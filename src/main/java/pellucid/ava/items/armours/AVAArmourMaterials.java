package pellucid.ava.items.armours;

import net.minecraft.Util;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.crafting.Ingredient;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.events.data.tags.AVAItemTagsProvider;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.HashMap;
import java.util.List;

public class AVAArmourMaterials
{
    public static final DeferredRegister<ArmorMaterial> ARMOUR_MATERIALS = DeferredRegister.create(Registries.ARMOR_MATERIAL, AVA.MODID);

    public static final DeferredHolder<ArmorMaterial, ArmorMaterial> STANDARD_EU = standard("standard_eu");
    public static final DeferredHolder<ArmorMaterial, ArmorMaterial> STANDARD_NRF = standard("standard_nrf");

    public static AVAWeaponUtil.TeamSide getSideFor(ArmorMaterial material)
    {
        if (material == STANDARD_EU.get())
            return AVAWeaponUtil.TeamSide.EU;
        if (material == STANDARD_NRF.get())
            return AVAWeaponUtil.TeamSide.NRF;
        return null;
    }


    private static DeferredHolder<ArmorMaterial, ArmorMaterial> standard(String name)
    {
        return ARMOUR_MATERIALS.register(name, () -> new ArmorMaterial(Util.make(new HashMap<>(), (map) -> {
            map.put(ArmorItem.Type.BOOTS, 3);
            map.put(ArmorItem.Type.LEGGINGS, 6);
            map.put(ArmorItem.Type.CHESTPLATE, 8);
            map.put(ArmorItem.Type.HELMET, 3);
            map.put(ArmorItem.Type.BODY, 11);
        }), 15, SoundEvents.ARMOR_EQUIP_LEATHER, () -> Ingredient.of(AVAItemTagsProvider.FIBRE), List.of(new ArmorMaterial.Layer(new ResourceLocation(AVA.MODID, name))), 2.0F, 100.0F));
    }
}
