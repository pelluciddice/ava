package pellucid.ava.items.armours;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.entity.living.LivingEquipmentChangeEvent;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static pellucid.ava.util.AVACommonUtil.isFullEquipped;

@EventBusSubscriber
public class AVAArmours extends ArmorItem implements IHasRecipe, ICustomTooltip
{
    protected static final AttributeModifier MOVEMENT_SPEED_MODIFIER = new AttributeModifier(UUID.fromString("D9AA8197-0727-4A85-80C4-E8823A26A765"), "Armour Speed Modifier", 0.01175F, AttributeModifier.Operation.ADD_VALUE);
    protected static final AttributeModifier MOVEMENT_SPEED_MODIFIER_SNEAK = new AttributeModifier(UUID.fromString("c602a846-dc79-42a2-b817-b4dbb761f91c"), "Armour Sneak Speed Modifier", 0.03825F, AttributeModifier.Operation.ADD_VALUE);
    protected final Recipe recipe;
    public static final List<AVAArmours> EU_ARMOURS = new ArrayList<>();
    public static final List<AVAArmours> NRF_ARMOURS = new ArrayList<>();

    public AVAArmours(DeferredHolder<ArmorMaterial, ArmorMaterial> materialIn, Type slot, Properties builder, Recipe recipe)
    {
        super(materialIn, slot, builder);
        if (materialIn == AVAArmourMaterials.STANDARD_EU || materialIn == AVAArmourMaterials.STANDARD_NRF)
            recipe.addDescription("standard_armour");
        if (materialIn == AVAArmourMaterials.STANDARD_EU)
            EU_ARMOURS.add(this);
        else
            NRF_ARMOURS.add(this);
        this.recipe = recipe;
    }

    @SubscribeEvent
    public static void armourChangeEvent(LivingEquipmentChangeEvent event)
    {
        if (event.getFrom().getItem() instanceof AVAArmours || event.getTo().getItem() instanceof AVAArmours)
        {
            if (event.getEntity() instanceof Player player)
            {
                AttributeInstance speed = player.getAttribute(Attributes.MOVEMENT_SPEED);
                if (speed != null)
                {
                    if (isFullEquipped(player))
                    {
                        if (!speed.hasModifier(MOVEMENT_SPEED_MODIFIER))
                            speed.addTransientModifier(MOVEMENT_SPEED_MODIFIER);
                    }
                    else
                    {
                        if (speed.hasModifier(MOVEMENT_SPEED_MODIFIER))
                            speed.removeModifier(MOVEMENT_SPEED_MODIFIER.id());
                    }
                }
            }
        }
    }

    @Override
    public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        AVAWeaponUtil.removeRepairCost(stack);
    }

    @Override
    public boolean addToolTips(ItemTooltipEvent event)
    {
        ICustomTooltip.super.addToolTips(event);
        return true;
    }

    @Override
    public void addAdditionalToolTips(ItemTooltipEvent event)
    {
        List<Component> tooltip = event.getToolTip();
        tooltip.add(Component.translatable("ava.item.tips.armour_full_equipped"));
        tooltip.add(Component.translatable("ava.item.tips.armour_night_vision").withStyle(ChatFormatting.GREEN));
        tooltip.add(Component.translatable("ava.item.tips.armour_knockback_resistance").withStyle(ChatFormatting.GREEN));
        tooltip.add(Component.translatable("ava.item.tips.armour_hurt_indicator").withStyle(ChatFormatting.GREEN));
        tooltip.add(Component.translatable("ava.item.tips.armour_projectile_indicator").withStyle(ChatFormatting.GREEN));
        tooltip.add(Component.translatable("ava.item.tips.armour_bio_indicator").withStyle(ChatFormatting.GREEN));
        tooltip.add(Component.translatable("ava.item.tips.armour_radio").withStyle(ChatFormatting.GREEN));
    }

    @Override
    public <T extends LivingEntity> int damageItem(ItemStack stack, int amount, @Nullable T entity, Runnable onBroken)
    {
        return AVAServerConfig.isCompetitiveModeActivated() ? 0 : amount;
    }

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    public static void giveTo(Player player, boolean eu, boolean equip)
    {
        List<AVAArmours> set = eu ? EU_ARMOURS : NRF_ARMOURS;
        for (AVAArmours item : set)
        {
            ItemStack stack = new ItemStack(item);
            if (!equip)
                player.addItem(stack);
            else
                player.setItemSlot(item.getEquipmentSlot(), stack);
        }
    }
}
