package pellucid.ava.items.armours.models;

// Made with Blockbench 3.9.3

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import pellucid.ava.util.AVAClientUtil;

public abstract class AVAArmourModel<E extends LivingEntity> extends HumanoidModel<E>
{
	public final ModelPart armorHead;
	public final ModelPart armorBody;
	public final ModelPart armorRightArm;
	public final ModelPart armorLeftArm;
	public final ModelPart armorRightLeg;
	public final ModelPart armorRightBoot;
	public final ModelPart armorLeftLeg;
	public final ModelPart armorLeftBoot;

	public AVAArmourModel(ModelPart root)
	{
		super(root, RenderType::entityCutoutNoCull);

		this.armorHead = getHead().getChild("armorHead");
		this.armorBody = body.getChild("armorBody");
		this.armorRightArm = rightArm.getChild("armorRightArm");
		this.armorLeftArm = leftArm.getChild("armorLeftArm");
		this.armorRightLeg = rightLeg.getChild("armorRightLeg");
		this.armorRightBoot = rightLeg.getChild("armorRightBoot");
		this.armorLeftLeg = leftLeg.getChild("armorLeftLeg");
		this.armorLeftBoot = leftLeg.getChild("armorLeftBoot");
	}

	// Shot https://gist.github.com/JTK222/b8bfa91836d529f9bf3850683675bd7d
	public AVAArmourModel applySlot(EquipmentSlot slot)
	{
		armorHead.visible = false;
		armorBody.visible = false;
		armorRightArm.visible = false;
		armorLeftArm.visible = false;
		armorRightLeg.visible = false;
		armorLeftLeg.visible = false;
		armorRightBoot.visible = false;
		armorLeftBoot.visible = false;

		switch (slot)
		{
			case HEAD:
				armorHead.visible = true;
				break;
			case CHEST:
				armorBody.visible = true;
				armorRightArm.visible = true;
				armorLeftArm.visible = true;
				break;
			case LEGS:
				armorRightLeg.visible = true;
				armorLeftLeg.visible = true;
				break;
			case FEET:
				armorRightBoot.visible = true;
				armorLeftBoot.visible = true;
				break;
			default:
				break;
		}
		return this;
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		AVAClientUtil.copyArmourRotationFromBody(this);
		armorHead.render(poseStack, buffer, packedLight, packedOverlay);
		armorBody.render(poseStack, buffer, packedLight, packedOverlay);
		armorRightArm.render(poseStack, buffer, packedLight, packedOverlay);
		armorLeftArm.render(poseStack, buffer, packedLight, packedOverlay);
		armorRightLeg.render(poseStack, buffer, packedLight, packedOverlay);
		armorRightBoot.render(poseStack, buffer, packedLight, packedOverlay);
		armorLeftLeg.render(poseStack, buffer, packedLight, packedOverlay);
		armorLeftBoot.render(poseStack, buffer, packedLight, packedOverlay);
	}
}