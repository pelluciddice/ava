package pellucid.ava.items.armours.models;// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports

import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.LivingEntity;

public class StandardNRFArmourModel extends AVAArmourModel<LivingEntity>
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor

	public StandardNRFArmourModel(ModelPart root) {
		super(root);
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition hat = partdefinition.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(0, 0), PartPose.offset(0.0F, 0.0F, 0.0F));
		
		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition armorHead = head.addOrReplaceChild("armorHead", CubeListBuilder.create().texOffs(85, 19).addBox(-2.5F, -1.0F, -5.0F, 5.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(92, 87).addBox(-4.1875F, -2.3244F, -4.6736F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(36, 64).addBox(0.5F, -4.0F, -5.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(90, 0).addBox(-4.0F, -5.5234F, -4.6953F, 8.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(57, 64).addBox(-3.5F, -4.0F, -5.0F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(91, 46).addBox(-3.5F, -6.0F, -5.0F, 7.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(79, 68).addBox(-2.5F, -11.7182F, -2.5F, 5.0F, 3.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(41, 0).addBox(-4.0F, -9.7416F, -4.0F, 8.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(66, 0).addBox(-5.0F, -5.0F, 2.0F, 10.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(62, 43).addBox(-5.0F, -6.0F, 1.0F, 10.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(0, 20).addBox(-5.0F, -8.0F, -5.0F, 10.0F, 2.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r1 = armorHead.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(77, 62).addBox(-5.1484F, -5.9481F, 4.1334F, 10.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r2 = armorHead.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(83, 28).addBox(8.2004F, -8.2336F, -3.1546F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r3 = armorHead.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(42, 92).addBox(10.4543F, -6.5917F, -1.6546F, 1.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, -0.8727F));

		PartDefinition cube_r4 = armorHead.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(29, 92).addBox(-11.6401F, -6.3703F, -1.6546F, 1.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, 0.8727F));

		PartDefinition cube_r5 = armorHead.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(66, 94).addBox(-2.6484F, -7.1256F, -11.0063F, 5.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -0.8727F, 0.0F, 0.0F));

		PartDefinition cube_r6 = armorHead.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(93, 3).addBox(-2.6484F, -5.8304F, 11.0931F, 5.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.8727F, 0.0F, 0.0F));

		PartDefinition cube_r7 = armorHead.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(55, 81).addBox(-9.4507F, -8.089F, -3.1546F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, 0.5236F));

		PartDefinition cube_r8 = armorHead.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(88, 77).addBox(-4.1484F, -7.7386F, 9.0577F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.5236F, 0.0F, 0.0F));

		PartDefinition cube_r9 = armorHead.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(13, 67).addBox(-1.1484F, -8.586F, -9.0236F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 33).addBox(-0.6484F, -7.8868F, -9.2072F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(87, 42).addBox(-4.1484F, -8.586F, -8.59F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -0.5236F, 0.0F, 0.0F));

		PartDefinition cube_r10 = armorHead.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(42, 92).addBox(-3.3255F, -6.4763F, -4.139F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, 0.3491F));

		PartDefinition cube_r11 = armorHead.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(11, 84).addBox(2.0466F, -6.5778F, -4.139F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, -0.3491F));

		PartDefinition cube_r12 = armorHead.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(66, 81).addBox(-4.3359F, -2.5937F, -7.5256F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 33).addBox(3.0391F, -2.5937F, -7.5256F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -1.5708F, 0.0F, 0.0F));

		PartDefinition cube_r13 = armorHead.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(50, 92).addBox(-4.3359F, -1.2865F, -5.4069F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -1.0472F, 0.0F, 0.0F));

		PartDefinition cube_r14 = armorHead.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(71, 57).addBox(-4.7943F, -0.943F, -3.1468F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, 0.3927F));

		PartDefinition cube_r15 = armorHead.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(55, 79).addBox(-4.457F, -8.6787F, -3.7471F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(79, 94).addBox(-4.6211F, -6.6787F, -3.751F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(31, 20).addBox(3.3242F, -6.6787F, -3.7588F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(25, 50).addBox(3.1602F, -8.6787F, -3.7471F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r16 = armorHead.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(0, 95).addBox(-4.3359F, -3.0454F, -5.7022F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(57, 31).addBox(3.0391F, -3.0454F, -5.7022F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -1.4835F, 0.0F, 0.0F));

		PartDefinition cube_r17 = armorHead.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(25, 38).addBox(2.52F, -1.0566F, -3.1468F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.0F, 0.0F, -0.3927F));

		PartDefinition headwear = partdefinition.addOrReplaceChild("headwear", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition armorBody = body.addOrReplaceChild("armorBody", CubeListBuilder.create().texOffs(33, 33).addBox(-4.5F, -0.4844F, -2.5F, 9.0F, 13.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-6.5F, -0.4844F, -3.5F, 13.0F, 12.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(34, 79).addBox(-4.5F, -0.4844F, 3.0195F, 9.0F, 11.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 16).addBox(-5.0F, -0.4844F, 3.2305F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 13).addBox(-5.0F, 1.5156F, 3.2305F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 39).addBox(-5.0F, 3.5156F, 3.2305F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(80, 10).addBox(-5.0F, 5.5156F, 3.2305F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(74, 7).addBox(-5.0F, 9.5156F, 3.2305F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 28).addBox(-5.0F, 7.5156F, 3.2305F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(25, 33).addBox(-5.5F, 1.5156F, -4.5F, 5.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-3.5F, 1.5156F, -4.9805F, 2.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 52).addBox(-6.5F, 5.5156F, -4.5F, 6.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(34, 0).addBox(0.5F, 5.5156F, -4.5F, 6.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(37, 92).addBox(0.5F, 1.5156F, -4.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(79, 68).addBox(1.9805F, 1.5156F, -4.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(41, 10).addBox(3.4336F, 1.5156F, -4.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(31, 20).addBox(-6.5F, -1.1914F, -3.0F, 13.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition body_r1 = armorBody.addOrReplaceChild("body_r1", CubeListBuilder.create().texOffs(62, 49).addBox(-6.6484F, -0.2624F, 3.8939F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, 0.6545F, 0.0F, 0.0F));

		PartDefinition body_r2 = armorBody.addOrReplaceChild("body_r2", CubeListBuilder.create().texOffs(70, 25).addBox(-6.6484F, -1.2917F, -3.5525F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1484F, 2.002F, -0.8454F, -0.6545F, 0.0F, 0.0F));

		PartDefinition rightArm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create(), PartPose.offset(-5.0F, 2.0F, 0.0F));

		PartDefinition armorRightArm = rightArm.addOrReplaceChild("armorRightArm", CubeListBuilder.create().texOffs(41, 64).addBox(-4.4727F, -2.4805F, -2.5F, 5.0F, 9.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(79, 52).addBox(-4.4727F, 6.5195F, -2.5F, 5.0F, 4.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(92, 81).addBox(-4.5273F, 1.5195F, 2.5F, 5.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-5.0F, 2.0F, 0.0F));

		PartDefinition leftArm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create(), PartPose.offset(5.0F, 2.0F, 0.0F));

		PartDefinition armorLeftArm = leftArm.addOrReplaceChild("armorLeftArm", CubeListBuilder.create().texOffs(20, 64).addBox(-0.4727F, -2.4805F, -2.5F, 5.0F, 9.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(88, 90).addBox(4.5273F, -0.4805F, -2.0F, 1.0F, 6.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(41, 10).addBox(-0.4727F, 6.5195F, -2.5F, 5.0F, 4.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(94, 28).addBox(-0.4727F, 1.5195F, 2.5F, 5.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(5.0F, 2.0F, 0.0F));

		PartDefinition rightLeg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create(), PartPose.offset(-1.9F, 12.0F, 0.0F));

		PartDefinition armorRightLeg = rightLeg.addOrReplaceChild("armorRightLeg", CubeListBuilder.create().texOffs(62, 28).addBox(-2.4297F, 0.0F, -2.5F, 5.0F, 9.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(78, 28).addBox(-2.4297F, 5.0F, -3.5F, 5.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 84).addBox(-4.8945F, 1.0F, -2.0F, 3.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.9F, 12.0F, 0.0F));

		PartDefinition armorRightBoot = rightLeg.addOrReplaceChild("armorRightBoot", CubeListBuilder.create().texOffs(52, 52).addBox(-2.4297F, 9.0F, -5.1523F, 5.0F, 3.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.9F, 12.0F, 0.0F));

		PartDefinition leftLeg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create(), PartPose.offset(1.9F, 12.0F, 0.0F));

		PartDefinition armorLeftLeg = leftLeg.addOrReplaceChild("armorLeftLeg", CubeListBuilder.create().texOffs(64, 10).addBox(-2.4297F, 0.0F, -2.5F, 5.0F, 9.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(71, 52).addBox(-2.4297F, 5.0F, -3.5F, 5.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(57, 10).addBox(-1.4297F, 1.0F, -3.9648F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(0, 20).addBox(-0.9297F, -1.0F, -3.4336F, 2.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(55, 92).addBox(2.5703F, 2.0F, -1.5F, 2.0F, 5.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(1.9F, 12.0F, 0.0F));

		PartDefinition armorLeftBoot = leftLeg.addOrReplaceChild("armorLeftBoot", CubeListBuilder.create().texOffs(25, 52).addBox(-2.4297F, 9.0F, -5.1523F, 5.0F, 3.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(1.9F, 12.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}
}