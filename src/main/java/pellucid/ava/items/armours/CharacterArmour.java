package pellucid.ava.items.armours;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions;
import net.neoforged.neoforge.registries.DeferredHolder;
import org.jetbrains.annotations.NotNull;
import pellucid.ava.AVA;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.items.armours.models.AVAArmourModel;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.recipes.Recipe;

import javax.annotation.Nullable;
import java.util.function.Consumer;

public class CharacterArmour extends AVAArmours
{
    private ResourceLocation texture;
    public CharacterArmour(DeferredHolder<ArmorMaterial, ArmorMaterial> materialIn, Type slot, Properties builder, Recipe recipe)
    {
        super(materialIn, slot, builder, recipe);
    }

    @OnlyIn(Dist.CLIENT)
    private AVAArmourModel<?> model;

    @OnlyIn(Dist.CLIENT)
    public void setModel(AVAArmourModel<?> model)
    {
        this.texture = new ResourceLocation(AVA.MODID, "textures/armours/" + material.unwrapKey().get().location().toString().split(":")[1] + ".png");
        this.model = model;
    }

    @OnlyIn(Dist.CLIENT)
    @Nullable
    @Override
    public @org.jetbrains.annotations.Nullable ResourceLocation getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, ArmorMaterial.Layer layer, boolean innerModel)
    {
        if (!AVAClientConfig.ENABLE_COMPLICATED_ARMOUR_MODEL.get())
            return null;
        return texture;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void initializeClient(Consumer<IClientItemExtensions> consumer)
    {
        consumer.accept(new IClientItemExtensions()
        {

            @Override
            public @NotNull HumanoidModel<?> getHumanoidArmorModel(LivingEntity livingEntity, ItemStack itemStack, EquipmentSlot equipmentSlot, HumanoidModel<?> original)
            {
                if (!AVAClientConfig.ENABLE_COMPLICATED_ARMOUR_MODEL.get())
                    return original;
                if (model == null)
                    MiscItems.armourModels();
                return model.applySlot(type.getSlot());
            }
        });
    }
}
