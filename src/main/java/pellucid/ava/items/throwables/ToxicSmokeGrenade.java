package pellucid.ava.items.throwables;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.entities.throwables.ToxicSmokeGrenadeEntity;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAWeaponUtil;

public class ToxicSmokeGrenade extends SmokeGrenade
{
    public ToxicSmokeGrenade(Recipe dyes, int colour, int releaseSmokeDuration, int releaseSmokeInterval, int growTime, int steadyTime, int shrinkTime)
    {
        super(0.75F, 30 * 20, releaseSmokeDuration, releaseSmokeInterval, growTime, steadyTime, shrinkTime, AVAEntities.M18_TOXIC, new Recipe().addItem(Items.IRON_NUGGET, 3).addItem(Items.SPIDER_EYE, 3).mergeIngredients(dyes), colour, AVAWeaponUtil.Classification.PROJECTILE);
    }

    @Override
    protected ProjectileEntity getEntity(Level world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss)
    {
        return new ToxicSmokeGrenadeEntity(type.get(), shooter, world, velocity, this);
    }
}
