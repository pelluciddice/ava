package pellucid.ava.items.throwables;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;
import java.util.function.Supplier;

public class HandGrenadeItem extends ThrowableItem
{
    private final boolean impact;
    private final int flashDuration;
    private final int damage;

    public HandGrenadeItem(Properties properties, boolean impact, int flashDuration, int damage, float power, int range, float radius, Supplier<EntityType<Entity>> type, SoundEvent sound, Recipe recipe, AVAWeaponUtil.Classification classification)
    {
        super(properties, power, range, radius, type, sound, recipe, classification);
        this.impact = impact;
        this.flashDuration = flashDuration;
        this.damage = damage / 5;
    }

    @Override
    public void addAdditionalToolTips(ItemTooltipEvent event)
    {
        super.addAdditionalToolTips(event);
        List<Component> tips = event.getToolTip();
        tips.add(Component.translatable("ava.item.tips.projectile_flash_duration", flashDuration));
        tips.add(Component.translatable("ava.item.tips.projectile_damage", damage));
        tips.add(Component.translatable("ava.item.tips.projectile_impact", impact));
    }

    @Override
    protected ProjectileEntity getEntity(Level world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss)
    {
        if (AVACommonUtil.isFullEquipped(shooter) && damage > 0 && !world.isClientSide())
            AVAWeaponUtil.playAttenuableSoundToClientMoving(AVAWeaponUtil.eitherSound(shooter, AVASounds.GRENADE_EU.get(), AVASounds.GRENADE_NRF.get()), shooter);
        return new HandGrenadeEntity(type.get(), shooter, world, velocity, this, this.impact, this.damage, this.flashDuration, this.range, this.radius, this.sound);
    }

    public int getDamage(boolean modified)
    {
        return modified ? this.damage : this.damage * 5;
    }
}
