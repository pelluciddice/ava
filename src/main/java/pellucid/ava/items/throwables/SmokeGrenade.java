package pellucid.ava.items.throwables;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.events.data.tags.AVAItemTagsProvider;
import pellucid.ava.items.init.Materials;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;
import java.util.function.Supplier;

public class SmokeGrenade extends ThrowableItem
{
    public final int colour;
    public int releaseSmokeDuration;
    public int releaseSmokeInterval;
    public int growTime;
    public int steadyTime;
    public int shrinkTime;

    public SmokeGrenade(Recipe dyes, int colour, int releaseSmokeDuration, int releaseSmokeInterval, int growTime, int steadyTime, int shrinkTime)
    {
        this(dyes, colour, 600, releaseSmokeDuration, releaseSmokeInterval, growTime, steadyTime, shrinkTime);
    }

    public SmokeGrenade(Recipe dyes, int colour, int range, int releaseSmokeDuration, int releaseSmokeInterval, int growTime, int steadyTime, int shrinkTime)
    {
        this(0.8F, range, releaseSmokeDuration, releaseSmokeInterval, growTime, steadyTime, shrinkTime, AVAEntities.M18, new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 2).addItem(Materials.FUSE, 2).addItem(Materials.SMOKE_POWDER).setResultCount(2).mergeIngredients(dyes), colour, AVAWeaponUtil.Classification.PROJECTILE);
    }

    public SmokeGrenade(float power, int range, int releaseSmokeDuration, int releaseSmokeInterval, int growTime, int steadyTime, int shrinkTime, Supplier<EntityType<Entity>> type, Recipe recipe, int colour, AVAWeaponUtil.Classification classification)
    {
        super(new Item.Properties(), power, range, 0, type, null, recipe, classification);
        this.releaseSmokeDuration = releaseSmokeDuration;
        this.releaseSmokeInterval = releaseSmokeInterval;
        this.growTime = growTime;
        this.steadyTime = steadyTime;
        this.shrinkTime = shrinkTime;
        this.colour = colour;
    }

    @Override
    public void addAdditionalToolTips(ItemTooltipEvent event)
    {
        List<Component> tips = event.getToolTip();
        tips.add(Component.translatable("ava.item.tips.smoke.colour").withStyle(Style.EMPTY.withColor(colour)));
        tips.add(Component.translatable("ava.item.tips.smoke.release_time", releaseSmokeDuration));
        tips.add(Component.translatable("ava.item.tips.smoke.release_interval", releaseSmokeInterval));
        tips.add(Component.translatable("ava.item.tips.smoke.grow_time", growTime));
        tips.add(Component.translatable("ava.item.tips.smoke.steady_time", steadyTime));
        tips.add(Component.translatable("ava.item.tips.smoke.shrink_time", shrinkTime));
    }

    @Override
    protected ProjectileEntity getEntity(Level world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss)
    {
        return new SmokeGrenadeEntity(type.get(), shooter, world, velocity, this);
    }
}
