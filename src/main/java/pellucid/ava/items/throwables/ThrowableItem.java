package pellucid.ava.items.throwables;

import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.player.status.ProjectileStatusManager;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Supplier;

public abstract class ThrowableItem extends AVAAnimatedItem<ThrowableItem> implements IHasRecipe, IClassification, ICustomTooltip
{
    protected final float power;
    protected final int range;
    protected final float radius;
    protected final SoundEvent sound;
    protected final Supplier<EntityType<Entity>> type;
    protected final Recipe recipe;
    private final AVAWeaponUtil.Classification classification;

    public ThrowableItem(Properties properties, float power, int range, float radius, Supplier<EntityType<Entity>> type, SoundEvent sound, Recipe recipe, AVAWeaponUtil.Classification classification)
    {
        super(properties.stacksTo(1), ProjectileStatusManager.INSTANCE);
        this.power = power;
        this.range = range;
        this.radius = radius;
        this.type = type;
        this.sound = sound;
        this.recipe = recipe;
        this.classification = classification;
        classification.addToList(this);
        AVAItemGroups.putItem(AVAItemGroups.MAIN, () -> this);
    }

    @Override
    public boolean addToolTips(ItemTooltipEvent event)
    {
        ICustomTooltip.super.addToolTips(event);
        return true;
    }

    @Override
    public void addAdditionalToolTips(ItemTooltipEvent event)
    {
        List<Component> tips = event.getToolTip();
        tips.add(Component.translatable("ava.item.tips.projectile_power", power));
        tips.add(Component.translatable("ava.item.tips.projectile_radius", radius));
        tips.add(Component.translatable("ava.item.tips.projectile_range", range));
    }

    public void toss(ServerLevel world, LivingEntity shooter, ItemStack stack, boolean toss)
    {
        toss(world, shooter, null, stack, toss);
    }

    public void toss(ServerLevel world, LivingEntity shooter, @Nullable LivingEntity target, ItemStack stack, boolean toss)
    {
        if (shooter instanceof Player)
            if (!classification.validate((Player) shooter, stack))
                return;

        stack.set(AVADataComponents.TAG_ITEM_FIRE, AVAConstants.PROJECTILE_THROW_TIME - 1);

        ProjectileEntity grenade = getEntity(world, shooter, stack, ((toss) ? 0.65F : 1.5F) * this.power, toss);
        if (target != null)
            grenade.fromMob(target);
        world.addFreshEntity(grenade);
    }

    protected abstract ProjectileEntity getEntity(Level world, LivingEntity shooter, ItemStack stack, double velocity, boolean toss);

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    public int getRange()
    {
        return this.range;
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return classification;
    }
}
