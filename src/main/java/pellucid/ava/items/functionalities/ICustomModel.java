package pellucid.ava.items.functionalities;

import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.ModelGetter;

public interface ICustomModel
{
    BakedModel getCustomModel(BakedModel originalBakedModel);

    void setCustomModel(ModelGetter getter);

    AVABakedItemModel getStaticModel(ItemStack stack);
}