package pellucid.ava.items.functionalities;


import net.minecraft.network.chat.Component;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;

public interface ICustomTooltip
{
    default boolean addToolTips(ItemTooltipEvent event)
    {
        event.getToolTip().add(Component.translatable(event.getItemStack().getDescriptionId()));
        return false;
    }

    default void addAdditionalToolTips(ItemTooltipEvent event)
    {

    }
}
