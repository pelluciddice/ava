package pellucid.ava.items.functionalities;

import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

public interface IListener
{
    void tick(Minecraft minecraft, Player player, ItemStack stack);

    boolean isItem(ItemStack stack);
}
