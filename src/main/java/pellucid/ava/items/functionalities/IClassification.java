package pellucid.ava.items.functionalities;

import pellucid.ava.util.AVAWeaponUtil;

public interface IClassification
{
    AVAWeaponUtil.Classification getClassification();
}
