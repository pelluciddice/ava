package pellucid.ava.items.functionalities;

import net.minecraft.world.item.ItemStack;
import pellucid.ava.items.guns.AVAItemGun;

public interface IScopedItem
{
    AVAItemGun.IScopeType getScopeType(ItemStack stack);
}
