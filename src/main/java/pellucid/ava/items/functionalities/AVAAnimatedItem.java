package pellucid.ava.items.functionalities;

import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.ModelGetter;
import pellucid.ava.client.renderers.models.BakedModelWrapper;
import pellucid.ava.player.status.ItemStatusManager;

public class AVAAnimatedItem<I extends Item> extends Item implements ICustomModel
{
    private final ItemStatusManager<I> manager;
    @OnlyIn(Dist.CLIENT)
    private ModelGetter modelGetter;
    @OnlyIn(Dist.CLIENT)
    private BakedModel originModel;
    @OnlyIn(Dist.CLIENT)
    private AVABakedItemModel<?, ?, ?, ?, ?> staticModel;

    public AVAAnimatedItem(Properties properties, ItemStatusManager<I> manager)
    {
        super(properties);
        this.manager = manager;
    }

    @Override
    public ItemAttributeModifiers getAttributeModifiers(ItemStack stack)
    {
        return ItemAttributeModifiers.builder()
                .add(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADD_VALUE), EquipmentSlotGroup.MAINHAND)
                .build();
    }

    @Override
    public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        manager.tickItem((I) this, stack, worldIn, entityIn, itemSlot, isSelected);
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public BakedModel getCustomModel(BakedModel originalBakedModel)
    {
        if (originModel == null)
            originModel = originalBakedModel;
        return modelGetter == null ? null : new BakedModelWrapper.Modded(originalBakedModel, modelGetter);
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public AVABakedItemModel<?, ?, ?, ?, ?> getStaticModel(ItemStack stack)
    {
        if (staticModel == null)
            staticModel = (AVABakedItemModel<?, ?, ?, ?, ?>) modelGetter.getModel(originModel, stack, null, null);
        return staticModel;
    }

    @OnlyIn(Dist.CLIENT)
    public void setCustomModel(ModelGetter getter)
    {
        modelGetter = getter;
    }


    @Override
    public boolean canAttackBlock(BlockState state, Level worldIn, BlockPos pos, Player player)
    {
        return false;
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity)
    {
        return true;
    }

    @Override
    public boolean onLeftClickEntity(ItemStack stack, Player player, Entity entity)
    {
        return true;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 72000;
    }

    @Override
    public UseAnim getUseAnimation(ItemStack stack)
    {
        return UseAnim.NONE;
    }
}
