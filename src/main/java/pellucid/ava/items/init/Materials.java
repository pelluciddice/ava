package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.util.AVAConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Materials
{
    public static final List<DeferredItem<Item>> CACHED_ITEMS = new ArrayList<>();
    public static final List<Item> MATERIALS = new ArrayList<>();

    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    public static final DeferredItem<Item> WORK_HARDENED_IRON = material("work_hardened_iron");
    public static final DeferredItem<Item> LENS = material("lens");
    public static final DeferredItem<Item> SPRING = material("spring");
    @Deprecated
    public static final DeferredItem<Item> PLASTIC = material("plastic");
    public static final DeferredItem<Item> PACKED_GUNPOWDER = material("packed_gunpowder");
    @Deprecated
    public static final DeferredItem<Item> SILICON = material("silicon");
    public static final DeferredItem<Item> COMPRESSED_WOOD = material("compressed_wood");
    public static final DeferredItem<Item> MECHANICAL_COMPONENTS = material("mechanical_components");
    public static final DeferredItem<Item> ACETONE_SOLUTION = material("acetone_solution", (properties) -> new Item(properties.durability(4)) {
        @Override
        public boolean hasCraftingRemainingItem(ItemStack stack)
        {
            return true;
        }

        @Override
        public ItemStack getCraftingRemainingItem(ItemStack stack)
        {
            stack.hurtAndBreak(1, AVAConstants.RAND, null, () -> {
                stack.shrink(1);
                stack.setDamageValue(0);
            });
            return stack;
        }
    });
    public static final DeferredItem<Item> FUSE = material("fuse");
    @Deprecated
    public static final DeferredItem<Item> FIBRE = material("fibre");
    public static final DeferredItem<Item> CERAMIC = material("ceramic");
    public static final DeferredItem<Item> SMOKE_POWDER = material("smoke_powder");

    private static DeferredItem<Item> material(String name)
    {
        return material(name, Item::new);
    }

    private static DeferredItem<Item> material(String name, Function<Item.Properties, Item> constructor)
    {
        DeferredItem<Item> item = ITEMS.register(name, () -> constructor.apply(new Item.Properties()));
        CACHED_ITEMS.add(item);
        AVAItemGroups.putItem(AVAItemGroups.SURVIVAL, item);
        return item;
    }

    public static void commonSetup()
    {
        if (MATERIALS.isEmpty())
        {
            for (DeferredItem<Item> item : CACHED_ITEMS)
                MATERIALS.add(item.get());
        }
    }
}
