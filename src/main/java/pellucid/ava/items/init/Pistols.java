package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.gun.stats.GunStatBuilder;
import pellucid.ava.items.guns.AVABulletFilledGun;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.AVASharedRecipes;
import pellucid.ava.sounds.AVASoundTracks;

import java.util.ArrayList;

import static pellucid.ava.items.init.Magazines.*;
import static pellucid.ava.util.AVAWeaponUtil.*;

public class Pistols
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */
    private static final AVAItemGun.Properties P226_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(32.5F).damageFloating(2.5F).penetration(0.0F).range(39.2F).fireRate(7.04F).capacity(15).mobility(93 + 6.0F)
            .reloadTime(38).drawSpeed(15).shakeTurnChance(0.2F).spreadFactor(3.25F).spreadRecoveryFactor(0.5F)
            .manual().optionalSilencer()
            .initialAccuracy(84.1F, 85.5F, 82.8F, 80.0F)
            .accuracy(84.1F, 85.5F, 82.8F, 80.0F)
            .stability(90.2F, 92.2F, 86.3F, 85.3F))
            .sounds(AVASoundTracks.P226_FIRE, AVASoundTracks.P226_RELOAD, AVASoundTracks.P226_DRAW);
    public static final DeferredItem<Item> P226 = createNormal(ITEMS, "p226", P226_P.magazineType(REGULAR_PISTOL_MAGAZINE), AVAGunRecipes.P226);


    private static final AVAItemGun.Properties SW1911_COLT_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(41.5F).damageFloating(3.5F).penetration(15.0F).range(46).fireRate(6.9F).capacity(8).mobility(86.7F + 6.0F)
            .reloadTime(40).drawSpeed(15).shakeTurnChance(0.5F).spreadFactor(3.25F).spreadRecoveryFactor(0.5F)
            .manual()
            .initialAccuracy(84.3F, 85.7F, 80.8F, 78.1F)
            .accuracy(84.3F, 85.7F, 80.8F, 78.1F)
            .stability(89.7F, 91.8F, 87.6F, 84.5F))
            .sounds(AVASoundTracks.SW1911_COLT_FIRE, AVASoundTracks.SW1911_COLT_RELOAD, AVASoundTracks.SW1911_COLT_DRAW);
    public static final DeferredItem<Item> SW1911_COLT = createNormal(ITEMS, "sw1911_colt", SW1911_COLT_P.magazineType(SMALL_PISTOL_MAGAZINE), AVAGunRecipes.SW1911_COLT);
    public static final DeferredItem<Item> SW1911_COLT_ARGENTO = createSkinnedNormal(ITEMS, "argento", SW1911_COLT, AVASharedRecipes.ARGENTO);


    private static final AVAItemGun.Properties PYTHON357_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(78.5F).damageFloating(3.5F).penetration(15.0F).range(39).fireRate(5.56F).capacity(6).mobility(86.7F + 6.0F)
            .reloadTime(80).drawSpeed(17).fireAnimationTime(2).shakeTurnChance(0.5F).spreadFactor(3.25F).spreadRecoveryFactor(0.5F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).manual()
            .initialAccuracy(84.3F, 85.7F, 80.8F, 78.1F, 90.5F, 90.4F, 80.8F, 78.2F)
            .accuracy(84.3F, 85.7F, 80.8F, 78.1F, 90.5F, 90.4F, 80.8F, 78.2F)
            .stability(59.9F, 67.9F, 59.9F, 59.8F, 68.7F, 75.0F, 60.5F, 59.8F))
            .sounds(AVASoundTracks.PYTHON357_FIRE, AVASoundTracks.PYTHON357_RELOAD, AVASoundTracks.PYTHON357_DRAW);
    public static final DeferredItem<Item> PYTHON357 = createNormal(ITEMS, "python357", PYTHON357_P.magazineType(AMMO_PISTOL), AVAGunRecipes.PYTHON357);
    public static final DeferredItem<Item> PYTHON357_OVERRIDER = createSkinned(ITEMS, "overrider", PYTHON357, AVASharedRecipes.OVERRIDER, AVABulletFilledGun::new);
    public static final DeferredItem<Item> PYTHON357_GOLD = createSkinned(ITEMS, "gold", PYTHON357, AVASharedRecipes.GOLD, AVABulletFilledGun::new);


    private static final AVAItemGun.Properties MAUSER_C96_P = new AVAItemGun.Properties(Classification.PISTOL_AUTO, GunStatBuilder.of()
            .damage(33.5F).damageFloating(3.5F).penetration(15.0F).range(35).fireRate(10F).capacity(15).mobility(91.9F + 6.0F)
            .reloadTime(40).drawSpeed(19).shakeTurnChance(0.1F)
            
            .initialAccuracy(79.3F, 81.9F, 74.7F, 60.1F)
            .accuracy(49.6F, 54.5F, 47.8F, 43.9F)
            .stability(81.4F, 85.6F, 77.1F, 70.6F))
            .sounds(AVASoundTracks.MAUSER_C96_FIRE, AVASoundTracks.MAUSER_C96_RELOAD, AVASoundTracks.MAUSER_C96_DRAW);
    public static final DeferredItem<Item> MAUSER_C96 = createNormal(ITEMS, "mauser_c96", MAUSER_C96_P.magazineType(REGULAR_PISTOL_MAGAZINE), AVAGunRecipes.MAUSER_C96);


    private static final AVAItemGun.Properties COLT_SAA_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(68.5F).damageFloating(3.5F).penetration(15.0F).range(45).fireRate(6.25F).capacity(6).mobility(86.7F + 6.0F)
            .reloadTime(90).drawSpeed(8).shakeTurnChance(0.5F).spreadFactor(3.25F).spreadRecoveryFactor(0.5F)
            .manual()
            .initialAccuracy(82.8F, 82.8F, 80.0F, 69.8F, 83.2F, 84.6F, 81.8F, 68.1F)
            .accuracy(82.8F, 82.8F, 80.0F, 69.8F, 83.2F, 84.6F, 81.8F, 68.1F)
            .stability(82.5F, 85.5F, 76.9F, 71.8F, 85.3F, 88.4F, 79.9F, 71.9F))
            .sounds(AVASoundTracks.COLT_SAA_FIRE, AVASoundTracks.COLT_SAA_RELOAD, AVASoundTracks.COLT_SAA_DRAW);
    public static final DeferredItem<Item> COLT_SAA = create(ITEMS, "colt_saa", COLT_SAA_P.magazineType(AMMO_PISTOL), AVAGunRecipes.COLT_SAA, AVABulletFilledGun::new);


    private static final AVAItemGun.Properties BERETTA_92FS_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(36.5F).damageFloating(2.5F).penetration(0.0F).range(40.2F).fireRate(8.14F).capacity(15).mobility(91.9F + 6.0F)
            .reloadTime(40).drawSpeed(9).shakeTurnChance(0.33F).spreadFactor(5F).spreadMax(2.5F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).manual().optionalSilencer()
            .initialAccuracy(86.5F, 87.6F, 85.3F, 83.4F, 89.7F, 90.7F, 78.9F, 76.8F)
            .accuracy(86.5F, 87.6F, 85.3F, 83.4F, 89.7F, 90.7F, 78.9F, 76.8F)
            .stability(81.4F, 84.5F, 75.7F, 74.4F, 81.4F, 84.5F, 75.7F, 74.4F))
            .sounds(AVASoundTracks.BERETTA_92FS_FIRE, AVASoundTracks.BERETTA_92FS_RELOAD, AVASoundTracks.BERETTA_92FS_DRAW);
    public static final DeferredItem<Item> BERETTA_92FS = createNormal(ITEMS, "beretta_92fs", BERETTA_92FS_P.magazineType(REGULAR_PISTOL_MAGAZINE), AVAGunRecipes.BERETTA_92FS);
    public static final DeferredItem<Item> BERETTA_92FS_SPORTS = createSkinnedNormal(ITEMS, "sports", BERETTA_92FS, AVASharedRecipes.SPORTS);
    public static final DeferredItem<Item> BERETTA_92FS_BARBATOS = createSkinnedNormal(ITEMS, "barbatos", BERETTA_92FS, AVASharedRecipes.BARBATOS);


    private static final AVAItemGun.Properties FN57_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(30.0F).damageFloating(1.0F).penetration(35.0F).range(40.2F).fireRate(10.20F).capacity(20).mobility(91.9F + 6.0F)
            .reloadTime(40).drawSpeed(10)
            .manual().shakeTurnChance(0.33F).spreadFactor(3.25F).spreadRecoveryFactor(0.5F).scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT)
            .initialAccuracy(84.3F, 85.7F, 80.8F, 78.1F, 90.5F, 90.4F, 80.8F, 78.2F)
            .accuracy(84.3F, 85.7F, 80.8F, 78.1F, 90.5F, 90.4F, 80.8F, 78.2F)
            .stability(88.5F, 90.8F, 86.3F, 82.8F, 88.5F, 90.8F, 86.3F, 82.8F))
            .sounds(AVASoundTracks.FN57_FIRE, AVASoundTracks.FN57_RELOAD, AVASoundTracks.FN57_DRAW);
    public static final DeferredItem<Item> FN57 = createNormal(ITEMS, "fn57", FN57_P.magazineType(LARGE_PISTOL_MAGAZINE), AVAGunRecipes.FN57);
    public static final DeferredItem<Item> FN57_SNOWFALL = createSkinnedNormal(ITEMS, "snowfall", FN57, AVASharedRecipes.SNOWFALL);
    public static final DeferredItem<Item> FN57_CHRISTMAS = createSkinnedNormal(ITEMS, "christmas", FN57, AVASharedRecipes.CHRISTMAS);


    private static final AVAItemGun.Properties DESERT_EAGLE_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(67.0F).damageFloating(4.0F).penetration(40.0F).range(42.4F).fireRate(4.17F).capacity(7).mobility(88.4F + 6.0F)
            .reloadTime(43).drawSpeed(13).scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT)
            .fireAnimationTime(4).manual()
            .initialAccuracy(78.0F, 79.8F, 76.4F, 68.1F, 83.2F, 84.6F, 81.8F, 68.1F)
            .accuracy(78.0F, 79.8F, 76.4F, 68.1F, 83.2F, 84.6F, 81.8F, 68.1F)
            .stability(59.1F, 67.3F, 51.0F, 38.7F, 59.1F, 67.3F, 51.0F, 38.7F))
            .sounds(AVASoundTracks.DESERT_EAGLE_FIRE, AVASoundTracks.DESERT_EAGLE_RELOAD, AVASoundTracks.DESERT_EAGLE_DRAW);
    public static final DeferredItem<Item> DESERT_EAGLE = createNormal(ITEMS, "desert_eagle", DESERT_EAGLE_P.magazineType(REGULAR_PISTOL_MAGAZINE), AVAGunRecipes.DESERT_EAGLE);
    public static final DeferredItem<Item> DESERT_EAGLE_SILVER = createSkinnedNormal(ITEMS, "silver", DESERT_EAGLE, AVASharedRecipes.SILVER);
    public static final DeferredItem<Item> DESERT_EAGLE_BLACK = createSkinnedNormal(ITEMS, "black", DESERT_EAGLE, AVASharedRecipes.BLACK);
    public static final DeferredItem<Item> DESERT_EAGLE_THE_ARGUS = createSkinnedNormal(ITEMS, "the_argus", DESERT_EAGLE, AVASharedRecipes.THE_ARGUS);


    private static final AVAItemGun.Properties GLOCK_P = new AVAItemGun.Properties(Classification.PISTOL, GunStatBuilder.of()
            .damage(37.5F).damageFloating(3.5F).penetration(15.0F).range(44.7F).fireRate(8.26F).capacity(13).mobility(92.9F + 6.0F)
            .reloadTime(40).drawSpeed(10).spreadFactor(2.0F).spreadRecoveryFactor(0.75F)
            .manual()
            .initialAccuracy(81.7F, 83.3F, 77.7F, 74.9F)
            .accuracy(81.7F, 83.3F, 77.7F, 74.9F)
            .stability(79.3F, 82.6F, 76.2F, 71.8F))
            .sounds(AVASoundTracks.GLOCK_FIRE, AVASoundTracks.GLOCK_RELOAD, AVASoundTracks.GLOCK_DRAW);
    public static final DeferredItem<Item> GLOCK = createNormal(ITEMS, "glock", GLOCK_P.magazineType(REGULAR_PISTOL_MAGAZINE), AVAGunRecipes.GLOCK);

    public static ArrayList<Item> ITEM_PISTOLS = new ArrayList<>();
}
