package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.Recipe;

public class Magazines
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    public static final DeferredItem<Item> REGULAR_PISTOL_MAGAZINE = mag("regular_pistol_magazine", 15, AVAGunRecipes.REGULAR_PISTOL_MAGAZINE);
    public static final DeferredItem<Item> SMALL_PISTOL_MAGAZINE = mag("small_pistol_magazine", 8, AVAGunRecipes.SMALL_PISTOL_MAGAZINE);
    public static final DeferredItem<Item> LARGE_PISTOL_MAGAZINE = mag("large_pistol_magazine", 20, AVAGunRecipes.LARGE_PISTOL_MAGAZINE);
    public static final DeferredItem<Item> AMMO_PISTOL = ammo("pistol_ammo", AVAGunRecipes.PISTOL_AMMO);

    public static final DeferredItem<Item> REGULAR_RIFLE_MAGAZINE = mag("regular_rifle_magazine", 30, AVAGunRecipes.REGULAR_RIFLE_MAGAZINE);
    public static final DeferredItem<Item> SMALL_RIFLE_MAGAZINE = mag("small_rifle_magazine", 20, AVAGunRecipes.SMALL_RIFLE_MAGAZINE);

    public static final DeferredItem<Item> AMMO_SNIPER = ammo("sniper_ammo", AVAGunRecipes.SNIPER_AMMO);
    public static final DeferredItem<Item> SMALL_SNIPER_MAGAZINE = mag("small_sniper_magazine", 5, AVAGunRecipes.SMALL_SNIPER_MAGAZINE);
    public static final DeferredItem<Item> REGULAR_SNIPER_MAGAZINE = mag("regular_sniper_magazine", 20, AVAGunRecipes.REGULAR_SNIPER_MAGAZINE);

    public static final DeferredItem<Item> REGULAR_SUB_MACHINEGUN_MAGAZINE = mag("regular_sub_machinegun_magazine", 30, AVAGunRecipes.REGULAR_SUB_MACHINEGUN_MAGAZINE);
    public static final DeferredItem<Item> SMALL_SUB_MACHINEGUN_MAGAZINE = mag("small_sub_machinegun_magazine", 20, AVAGunRecipes.SMALL_SUB_MACHINEGUN_MAGAZINE);
    public static final DeferredItem<Item> AMMO_SHOTGUN = ammo("shotgun_ammo", AVAGunRecipes.SHOTGUN_AMMO);
    public static final DeferredItem<Item> HEAVY_AP_MAGAZINE = ammo("heavy_ap_magazine", AVAGunRecipes.HEAVY_AP_MAGAZINE);

    private static DeferredItem<Item> mag(String name, int capacity, Recipe recipe)
    {
        return AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register(name, () -> new Ammo(new Item.Properties().durability(capacity), recipe, true)));
    }

    private static DeferredItem<Item> ammo(String name, Recipe recipe)
    {
        return ITEMS.register(name, () -> new Ammo(recipe));
    }
}
