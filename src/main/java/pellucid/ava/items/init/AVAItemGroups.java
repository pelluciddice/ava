package pellucid.ava.items.init;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD)
public class AVAItemGroups
{
    public static final DeferredRegister<CreativeModeTab> TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, AVA.MODID);

    private static final Map<DeferredHolder<CreativeModeTab, CreativeModeTab>, List<Supplier<ItemStack>>> ITEMS_MAP = new HashMap<>();
    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> MAIN = tab("main", () -> Rifles.M4A1.get());
    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> MAP_CREATION = tab("map_creation", () -> AVABlocks.AMMO_KIT_SUPPLIER.get());
    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> SURVIVAL =  tab("survival", () -> Materials.WORK_HARDENED_IRON.get());
    public static final DeferredHolder<CreativeModeTab, CreativeModeTab> ALPHA =  tab("alpha", () -> Items.BARRIER);

    private static <I extends ItemLike, S extends Supplier<I>> DeferredHolder<CreativeModeTab, CreativeModeTab> tab(String name, S icon)
    {
        return TABS.register(name, () -> CreativeModeTab.builder().icon(() -> new ItemStack(icon.get())).withTabsBefore(AVAConstants.VANILLA_TABS.toArray(new ResourceKey[]{})).title(Component.translatable(AVA.MODID + ".creative_tab." + name)).build());
    }

    public static <I extends Item, T extends Supplier<I>> T putItem(DeferredHolder<CreativeModeTab, CreativeModeTab> category, T item)
    {
        ITEMS_MAP.computeIfAbsent(category, (e) -> new ArrayList<>()).add(() -> new ItemStack(item.get()));
        return item;
    }

    public static void putItemStack(DeferredHolder<CreativeModeTab, CreativeModeTab> category, Supplier<ItemStack> item)
    {
        ITEMS_MAP.computeIfAbsent(category, (e) -> new ArrayList<>()).add(item);
    }

    @SubscribeEvent
    public static void registerCreativeTabs(BuildCreativeModeTabContentsEvent event)
    {
        ITEMS_MAP.keySet().stream().filter((e) -> e.getKey().equals(event.getTabKey())).findFirst().ifPresent((e) -> {
            event.acceptAll(AVACommonUtil.collect(ITEMS_MAP.get(e), Supplier::get));
        });
    }
}
