package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.stats.GunStatBuilder;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.gun.stats.IntegerGunStat;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.AVASharedRecipes;
import pellucid.ava.sounds.AVASoundTracks;

import java.util.ArrayList;

import static pellucid.ava.items.init.Magazines.REGULAR_RIFLE_MAGAZINE;
import static pellucid.ava.items.init.Magazines.SMALL_RIFLE_MAGAZINE;
import static pellucid.ava.util.AVAWeaponUtil.*;

public class Rifles
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */
    private static final AVAItemGun.Properties M4A1_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(35).damageFloating(3.0F).penetration(28.0F).range(70).fireRate(10.5F).capacity(30).mobility(95)
            .reloadTime(50).drawSpeed(9).aimTime(4)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail().shakeFactor(0.5F).shakeTurnChance(0.1F)
            .initialAccuracy(90.0F, 91.4F, 81.9F, 54.4F, 89.7F, 90.9F, 75.3F, 48.6F)
            .accuracy(85.6F, 86.9F, 72.6F, 46.1F, 88.7F, 90.0F, 70.4F, 44.1F)
            .stability(77.2F, 77.2F, 74.6F, 55.6F, 77.2F, 77.2F, 67.5F, 42.5F))
            .sounds(AVASoundTracks.M4A1_FIRE, AVASoundTracks.M4A1_RELOAD, AVASoundTracks.M4A1_DRAW);
    public static final DeferredItem<Item> M4A1 = createNormal(ITEMS, "m4a1", M4A1_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.M4A1);
    public static final DeferredItem<Item> M4A1_SUMIRE = createSkinnedNormal(ITEMS, "sumire", M4A1, AVASharedRecipes.SUMIRE);
    public static final DeferredItem<Item> M4A1_DREAMCATCHER = createSkinnedNormal(ITEMS, "dreamcatcher", M4A1, AVASharedRecipes.DREAMCATCHER);
    public static final DeferredItem<Item> M4A1_XPLORER = createSkinned(ITEMS, "xplorer", M4A1, AVASharedRecipes.XPLORER, (p, r) -> new AVAItemGun(p
            .getStats((stat) -> stat.put(GunStatTypes.DRAW_TIME, IntegerGunStat.integer(20))).getStats((stat) -> stat.put(GunStatTypes.CAPACITY, IntegerGunStat.integer(35)))
            .sounds(AVASoundTracks.M4A1_XPLORER_FIRE, AVASoundTracks.M4A1_RELOAD, AVASoundTracks.M4A1_XPLORER_DRAW), r));
    public static final DeferredItem<Item> M4A1_COTTON_CANDY = createSkinnedNormal(ITEMS, "cotton_candy", M4A1, AVASharedRecipes.COTTON_CANDY);


    private static final AVAItemGun.Properties MK20_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(50).damageFloating(4.0F).penetration(65.0F).range(63).fireRate(10.5F).capacity(20).mobility(94.3F)
            .reloadTime(52).drawSpeed(25).aimTime(4).shakeFactor(1.5F).shakeTurnChance(0.15F).fireAnimationTime(5)
            .scopeType(AVAItemGun.ScopeTypes.X4).trail()
            .initialAccuracy(88.0F, 89.1F, 81.1F, 56.4F, 95.4F, 95.3F, 71.1F, 50.1F)
            .accuracy(50.3F, 53.7F, 45.2F, 13.2F)
            .stability(77.0F, 82.6F, 76.8F, 64.7F, 77.2F, 79.4F, 73.9F, 45.9F))
            .sounds(AVASoundTracks.MK20_FIRE, AVASoundTracks.MK20_RELOAD, AVASoundTracks.MK20_DRAW)
            .stat(GunAttachmentTypes.HEAVY_BARREL, GunStatBuilder.of().range(3.5F).initialAccuracy(1.6F, 1.6F, 0.0F, 0.0F).accuracy(0.5F, 0.6F, 0.0F, 0.0F).stability(-0.2F, -0.1F, -0.2F, -0.2F).fireRate(-0.62F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(6.2F))
            .stat(GunAttachmentTypes.REINFORCED_BARREL, GunStatBuilder.of().damage(4.0F).fireRate(-0.62F))
            .stat(GunAttachmentTypes.SPETSNAZ_BARREL, GunStatBuilder.of().initialAccuracy(0.3F, 0.2F, 0.3F, 0.9F).accuracy(24.8F, 23.5F, 23.4F, 19.9F).stability(0.0F, 0.0F, 0.0F, 0.2F, 0.0F, 0.0F, 0.0F, 1.2F).mobility(-3.0F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(1.9F, 2.0F, 1.5F, -0.5F).accuracy(0.7F, 0.8F, 0.5F, 0.0F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.3F, 1.3F, 1.1F, 0.6F).accuracy(0.4F, 0.5F, 0.3F, 0.0F))
            .stat(GunAttachmentTypes.RECOIL_CONTROL_STOCK, GunStatBuilder.of().stability(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.6F))
            .stat(GunAttachmentTypes.SHOCK_ABSORBER, GunStatBuilder.of().initialAccuracy(2.8F, 3.1F, -1.6F, -4.2F).accuracy(1.1F, 1.3F, -0.4F, -0.2F));
    public static final DeferredItem<Item> MK20 = createNormal(ITEMS, "mk20", MK20_P.magazineType(SMALL_RIFLE_MAGAZINE), AVAGunRecipes.MK20);
	public static final DeferredItem<Item> MK20_BALD_EAGLE = createSkinnedNormal(ITEMS, "bald_eagle", MK20, AVASharedRecipes.BALD_EAGLE);


    private static final AVAItemGun.Properties FN_FNC_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(39).damageFloating(3.0F).penetration(28.0F).range(70).fireRate(10.2F).capacity(30).mobility(96.2F)
            .reloadTime(47).drawSpeed(15).shakeFactor(1.35F).shakeTurnChance(0.125F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(89.0F, 90.1F, 76.8F, 58.8F, 89.0F, 89.8F, 70.3F, 51.6F)
            .accuracy(81.7F, 83.4F, 66.2F, 46.9F, 87.1F, 88.2F, 64.6F, 45.2F)
            .stability(76.3F, 79.6F, 79.9F, 52.7F, 76.3F, 79.5F, 81.9F, 38.7F))
            .sounds(AVASoundTracks.FN_FNC_FIRE, AVASoundTracks.FN_FNC_RELOAD, AVASoundTracks.FN_FNC_DRAW);
    public static final DeferredItem<Item> FN_FNC = createNormal(ITEMS, "fn_fnc", FN_FNC_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.FN_FNC);
    public static final DeferredItem<Item> FN_FNC_DREAMCATCHER = createSkinnedNormal(ITEMS, "dreamcatcher", FN_FNC, AVASharedRecipes.DREAMCATCHER);
    public static final DeferredItem<Item> FN_FNC_FULLMOON = createSkinnedNormal(ITEMS, "fullmoon", FN_FNC, AVASharedRecipes.FULLMOON);


    private static final AVAItemGun.Properties XM8_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(39.2F).damageFloating(-4.2F, 1.8F).penetration(28.0F).range(69.6F).fireRate(10.00F).capacity(30).mobility(93.2F)
            .reloadTime(54).spreadRecoveryFactor(0.7F).drawSpeed(22).aimTime(5).shakeFactor(2.0F).shakeTurnChance(0.2F)
            .scopeType(AVAItemGun.ScopeTypes.X4).trail()
            .initialAccuracy(84.7F, 84.7F, 74.7F, 61.6F, 81.1F, 82.4F, 76.4F, 66.6F)
            .accuracy(75.4F, 75.4F, 65.5F, 54.6F, 78.2F, 80.0F, 70.9F, 61.1F)
            .stability(90.8F, 90.9F, 90.7F, 82.9F, 88.4F, 90.9F, 79.7F, 77.7F))
            .sounds(AVASoundTracks.XM8_FIRE, AVASoundTracks.XM8_RELOAD, AVASoundTracks.XM8_DRAW)
            .stat(GunAttachmentTypes.STABILITY_UPGRADE, GunStatBuilder.of().initialAccuracy(6.2F, 6.2F, 9.0F, 1.9F).accuracy(8.6F, 8.6F, 10.8F, 1.5F))
            .stat(GunAttachmentTypes.ADVANCED_TRIGGER, GunStatBuilder.of().spreadRecoveryFactor(3F));
    public static final DeferredItem<Item> XM8 = createNormal(ITEMS, "xm8", XM8_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.XM8);
    public static final DeferredItem<Item> XM8_FROST = createSkinnedNormal(ITEMS, "frost", XM8, AVASharedRecipes.FROST);
    public static final DeferredItem<Item> XM8_SNOWFALL = createSkinnedNormal(ITEMS, "snowfall", XM8, AVASharedRecipes.SNOWFALL);


    private static final AVAItemGun.Properties FG42_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(47).damageFloating(4.0F).penetration(65.0F).range(68).fireRate(9.09F).capacity(20).mobility(93.2F)
            .reloadTime(55).drawSpeed(26).shakeFactor(1.75F).shakeTurnChance(0.25F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(96.5F, 96.1F, 84.5F, 72.4F, 100.0F, 100.0F, 76.6F, 69.2F)
            .accuracy(61.3F, 67.0F, 28.9F, 17.8F, 83.5F, 83.7F, 55.4F, 47.4F)
            .stability(74.5F, 80.7F, 72.0F, 46.7F, 82.3F, 87.0F, 78.2F, 35.4F))
            .sounds(AVASoundTracks.FG42_FIRE, AVASoundTracks.FG42_RELOAD, AVASoundTracks.FG42_DRAW);
    public static final DeferredItem<Item> FG42 = createNormal(ITEMS, "fg42", FG42_P.magazineType(SMALL_RIFLE_MAGAZINE), AVAGunRecipes.FG42);
    public static final DeferredItem<Item> FG42_SUMIRE = createSkinnedNormal(ITEMS, "sumire", FG42, AVASharedRecipes.SUMIRE);
    public static final DeferredItem<Item> FG42_DREAMCATCHER = createSkinnedNormal(ITEMS, "dreamcatcher", FG42, AVASharedRecipes.DREAMCATCHER);


    private static final AVAItemGun.Properties SG556_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(39).damageFloating(3.0F).penetration(28.0F).range(63).fireRate(10.0F).capacity(30).mobility(94.3F)
            .reloadTime(55).drawSpeed(20).aimTime(4).shakeFactor(0.15F).shakeTurnChance(0.1F)
            .scopeType(AVAItemGun.ScopeTypes.X4).trail()
            .initialAccuracy(85.5F, 90.4F, 75.2F, 59.3F, 99.0F, 99.5F, 80.1F, 45.2F)
            .accuracy(71.2F, 84.8F, 52.1F, 29.0F, 92.8F, 96.3F, 67.8F, 33.0F)
            .stability(85.9F, 92.6F, 78.5F, 69.6F, 89.7F, 91.1F, 73.0F, 61.6F))
            .sounds(AVASoundTracks.SG556_FIRE, AVASoundTracks.SG556_RELOAD, AVASoundTracks.SG556_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-0.3F, -0.1F, -0.5F, -1.0F).accuracy(-0.1F, 0.0F, -0.2F, -0.2F).fireRate(0.99F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(8.1F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(1.8F, 1.9F, 0.0F, 0.0F).accuracy(1.2F, 1.7F, 0.0F, 0.0F).fireRate(-0.57F))
            .stat(GunAttachmentTypes.REINFORCED_BARREL, GunStatBuilder.of().damage(4.0F).fireRate(-0.74F))
            .stat(GunAttachmentTypes.ADVANCED_TRIGGER, GunStatBuilder.of().range(-3.3F).recoilCompensation(0.0F, 0.0F, 0.0F, 0.3F, 0.0F, 0.0F, 0.0F, 0.1F))
            .stat(GunAttachmentTypes.VETERAN_MECHANISM, GunStatBuilder.of().initialAccuracy(-1.6F, -1.9F, -1.2F, -0.8F).accuracy(-0.3F, -1.2F, 0.5F, 0.9F).spreadRecovery(-0.8F))
            .stat(GunAttachmentTypes.PROTOTYPE_GRIP, GunStatBuilder.of().stability(0.9F, 1.2F, 0.7F, 0.7F))
            .stat(GunAttachmentTypes.RECOIL_CONTROL_STOCK, GunStatBuilder.of().stability(0.1F, 0.0F, 0.0F, 0.0F, 1.2F, 0.0F, 0.0F, 0.0F));
    public static final DeferredItem<Item> SG556 = createNormal(ITEMS, "sg556", SG556_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.SG556);
    public static final DeferredItem<Item> SG556_BLACK_WIDOW = createSkinnedNormal(ITEMS, "black_widow", SG556, AVASharedRecipes.BLACK_WIDOW);


    private static final AVAItemGun.Properties AK12_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(49).damageFloating(4.0F).penetration(65.0F).range(63).fireRate(8.7F).capacity(30).mobility(92.4F)
            .reloadTime(56).drawSpeed(25).shakeFactor(1.5F).shakeTurnChance(0.3F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(96.1F, 96.8F, 100.0F, 69.4F, 96.1F, 95.8F, 73.3F, 53.0F)
            .accuracy(71.8F, 77.1F, 49.5F, 28.1F, 64.7F, 69.4F, 21.0F, 10.6F)
            .stability(64.9F, 71.8F, 62.2F, 50.9F, 71.1F, 68.4F, 54.3F, 26.0F))
            .sounds(AVASoundTracks.AK12_FIRE, AVASoundTracks.AK12_RELOAD, AVASoundTracks.AK12_DRAW)
            .stat(GunAttachmentTypes.HEAVY_BARREL, GunStatBuilder.of().range(3.5F).initialAccuracy(1.9F, 1.9F, 0.0F, 0.0F).accuracy(1.0F, 1.3F, 0.0F, 0.0F).stability(-2.1F, -4.2F, -1.7F, 0.1F).fireRate(-0.57F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(6.2F))
            .stat(GunAttachmentTypes.REINFORCED_BARREL, GunStatBuilder.of().damage(4.0F).stability(0.0F, 0.0F, 0.0F, -3.6F, 0.0F, 0.0F, 0.0F, -2.7F).fireRate(-0.57F))
            .stat(GunAttachmentTypes.SPETSNAZ_BARREL, GunStatBuilder.of().initialAccuracy(0.0F, 0.0F, 0.0F, 0.2F).accuracy(13.1F, 10.4F, 25.0F, 16.5F).stability(1.1F, 0.0F, 2.1F, 8.0F, 0.0F, 0.0F, 3.3F, 5.5F).mobility(-3.0F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(2.2F, 2.5F, 0.0F, -0.6F).accuracy(1.2F, 1.6F, 0.2F, -0.1F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.6F, 1.6F, 0.0F, 0.8F).accuracy(0.8F, 1.1F, 0.4F, 0.1F))
            .stat(GunAttachmentTypes.RECOIL_CONTROL_STOCK, GunStatBuilder.of().stability(0.0F, 0.0F, 0.2F, 3.9F))
            .stat(GunAttachmentTypes.SHOCK_ABSORBER, GunStatBuilder.of().initialAccuracy(3.3F, 3.2F, 0.0F, -5.6F).accuracy(1.8F, 2.6F, -1.5F, -0.9F));
    public static final DeferredItem<Item> AK12 = createNormal(ITEMS, "ak12", AK12_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.AK12);
    public static final DeferredItem<Item> AK12_UNIT_01 = createSkinnedNormal(ITEMS, "unit_01", AK12, AVASharedRecipes.UNIT_01);


    private static final AVAItemGun.Properties M16_VN_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(38.7F).damageFloating(-1.7F, 2.3F).penetration(28.0F).range(63).fireRate(10.0F).capacity(20).mobility(98.8F)
            .reloadTime(47).drawSpeed(22).shakeFactor(0.25F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(84.5F, 85.6F, 81.1F, 72.3F, 93.0F, 94.0F, 70.3F, 51.6F)
            .accuracy(80.6F, 81.9F, 77.1F, 64.8F, 91.1F, 92.2F, 64.6F, 45.2F)
            .stability(90.0F, 93.9F, 89.0F, 79.8F, 86.4F, 86.0F, 84.2F, 53.8F))
            .sounds(AVASoundTracks.M16_VN_FIRE, AVASoundTracks.M16_VN_RELOAD, AVASoundTracks.M16_VN_DRAW)
            .stat(GunAttachmentTypes.HEAVY_BARREL, GunStatBuilder.of().range(3.5F).initialAccuracy(1.5F, 1.5F, 1.4F, 1.4F).accuracy(1.4F, 1.4F, 1.2F, 0.0F).recoilCompensation(0.1F, 0.0F, -0.2F, -0.1F).fireRate(-0.74F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(6.2F).shakeTurnChance(0.05F))
            .stat(GunAttachmentTypes.SPETSNAZ_BARREL, GunStatBuilder.of().accuracy(1.3F, 1.2F, 1.3F, 2.3F).recoilCompensation(0.0F, 0.0F, 0.0F, 1.1F).mobility(-3.0F));
    public static final DeferredItem<Item> M16_VN = createNormal(ITEMS, "m16_vn", M16_VN_P.magazineType(SMALL_RIFLE_MAGAZINE), AVAGunRecipes.M16_VN);
    public static final DeferredItem<Item> M16_VN_FROST_SNOW = createSkinnedNormal(ITEMS, "frost_snow", M16_VN, AVASharedRecipes.FROST_SNOW);


    private static final AVAItemGun.Properties RK95_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(46.0F).damageFloating(4.0F).penetration(30.0F).range(61.7F).fireRate(9.17F).capacity(30).mobility(93.5F)
            .reloadTime(40).drawSpeed(22).shakeFactor(0.45F).shakeTurnChance(0.05F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(86.7F, 88.2F, 76.5F, 49.7F, 88.2F, 89.9F, 71.8F, 43.8F)
            .accuracy(81.5F, 83.4F, 67.1F, 38.5F, 86.1F, 87.8F, 64.7F, 36.1F)
            .stability(65.2F, 71.2F, 61.7F, 30.8F, 65.2F, 71.2F, 50.1F, 11.5F))
            .sounds(AVASoundTracks.RK95_FIRE, AVASoundTracks.RK95_RELOAD, AVASoundTracks.RK95_DRAW);
    public static final DeferredItem<Item> RK95 = ITEMS.register("rk95", () -> new AVAItemGun(RK95_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.RK95, AVAItemGroups.ALPHA));


    private static final AVAItemGun.Properties SA58_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(51.0F).damageFloating(4.0F).penetration(65.0F).range(66.9F).fireRate(10.1F).capacity(25).mobility(93.5F)
            .reloadTime(47).drawSpeed(20).shakeTurnChance(0.25F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(98.6F, 98.2F, 86.5F, 74.2F, 100.0F, 100.0F, 76.8F, 69.4F)
            .accuracy(71.5F, 76.0F, 38.7F, 25.1F, 82.5F, 82.6F, 54.3F, 46.3F)
            .stability(72.4F, 78.7F, 68.1F, 53.0F, 80.5F, 85.8F, 76.0F, 51.7F))
            .sounds(AVASoundTracks.SA58_FIRE, AVASoundTracks.SA58_RELOAD, AVASoundTracks.SA58_DRAW)
            .stat(GunAttachmentTypes.HEAVY_BARREL, GunStatBuilder.of().range(3.3F).initialAccuracy(1.9F, 1.9F, 0.0F, 0.0F).accuracy(0.5F, 0.7F, 0.0F, 0.0F).stability(-0.7F, -2.8F, -0.1F, -0.2F).fireRate(-0.74F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(5.9F))
            .stat(GunAttachmentTypes.REINFORCED_BARREL, GunStatBuilder.of().damage(4.0F).stability(0.0F, 0.0F, 0.0F, -3.1F).fireRate(-0.74F))
            .stat(GunAttachmentTypes.SPETSNAZ_BARREL, GunStatBuilder.of().initialAccuracy(0.4F, 0.3F, 0.9F, 1.2F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(28.9F, 24.7F, 31.7F, 24.2F, 0.0F, 0.0F, 0.0F, 0.0F).stability(1.4F, 0.0F, 2.5F, 6.1F, 0.0F, 0.0F, 3.2F, 6.2F).mobility(-3.0F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(2.3F, 2.5F, 1.0F, -0.5F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(0.7F, 1.0F, 0.2F, 0.0F, 0.0F, 0.0F, 0.0F,0.0F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.6F, 1.6F, 1.3F, 0.8F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(0.4F, 0.6F, 0.1F, 0.1F, 0.0F, 0.0F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.FAST_REACTION_STOCK, GunStatBuilder.of().stability(0.1F, 0.0F, 0.6F, 3.0F, 0.0F, 0.0F, 1.0F, 3.0F))
            .stat(GunAttachmentTypes.SHOCK_ABSORBER, GunStatBuilder.of().initialAccuracy(3.4F, 3.5F, -3.8F, -5.4F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(1.1F, 1.7F, -0.3F, -0.1F, 0.0F, 0.0F, 0.0F, 0.0F));
    public static final DeferredItem<Item> SA58 = ITEMS.register("sa58", () -> new AVAItemGun(SA58_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.SA58, AVAItemGroups.MAIN));
    public static final DeferredItem<Item> SA58_MILAD = createSkinnedNormal(ITEMS, "milad", SA58, AVASharedRecipes.CHRISTMAS);


    // Made-up detail stats
    private static final AVAItemGun.Properties KELTEC_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(47.2F).damageFloating(-2.1F, 1.9F).penetration(65.0F).range(63.4F).fireRate(10.0F).capacity(20).mobility(99.2F)
            .reloadTime(52).drawSpeed(20).shakeTurnChance(0.25F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(97.7F, 100.0F, 85.0F, 75.0F, 99.0F, 100.0F, 98.0F, 90.0F)
            .accuracy(61.8F, 70.0F, 55.0F, 50.0F, 51.8F, 65.0F, 50.0F, 45.0F)
            .stability(64.6F, 75.0F, 60.0F, 55.0F))
            .sounds(AVASoundTracks.KELTEC_FIRE, AVASoundTracks.KELTEC_RELOAD, AVASoundTracks.KELTEC_DRAW);
    public static final DeferredItem<Item> KELTEC = ITEMS.register("keltec", () -> new AVAItemGun(KELTEC_P.magazineType(SMALL_RIFLE_MAGAZINE), AVAGunRecipes.KELTEC, AVAItemGroups.MAIN));
    public static final DeferredItem<Item> KELTEC_COSMIC = createSkinnedNormal(ITEMS, "cosmic", KELTEC, AVASharedRecipes.COSMIC);


    private static final AVAItemGun.Properties AK47_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(44.0F).damageFloating(4.0F).penetration(30.0F).range(61.7F).fireRate(10.31F).capacity(30).mobility(93.5F)
            .reloadTime(40).drawSpeed(22).shakeTurnChance(0.15F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(91.5F, 96.4F, 80.5F, 58.2F, 92.1F, 94.8F, 66.0F, 42.3F)
            .accuracy(79.3F, 84.7F, 74.9F, 38.3F, 87.6F, 91.9F, 55.3F, 32.3F)
            .stability(77.0F, 82.2F, 76.5F, 68.9F, 79.5F, 83.6F, 76.6F, 70.3F))
            .sounds(AVASoundTracks.AK47_FIRE, AVASoundTracks.AK47_RELOAD, AVASoundTracks.AK47_DRAW);
    public static final DeferredItem<Item> AK47 = ITEMS.register("ak47", () -> new AVAItemGun(AK47_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.AK47, AVAItemGroups.MAIN));
    public static final DeferredItem<Item> AK47_PREDATOR = createSkinnedNormal(ITEMS, "predator", AK47, AVASharedRecipes.PREDATOR);
    public static final DeferredItem<Item> AK47_BLITZ = createSkinnedNormal(ITEMS, "blitz", AK47, AVASharedRecipes.BLITZ);



    private static final AVAItemGun.Properties SCAR_L_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(36).damageFloating(2.0F).penetration(28.0F).range(70.6F).fireRate(10.53F).capacity(30).mobility(96.2F)
            .reloadTime(52).drawSpeed(27).shakeFactor(2.25F).shakeTurnChance(0.01F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail()
            .initialAccuracy(88.8F, 89.6F, 87.6F, 64.2F, 83.6F, 91.4F, 83.6F, 51.1F)
            .accuracy(85.6F, 86.9F, 83.8F, 55.2F, 73.3F,83.6F,73.3F,38.0F)
            .stability(77.0F, 82.6F, 76.8F, 64.7F, 77.2F, 79.4F, 73.9F, 45.9F))
            .sounds(AVASoundTracks.SCAR_L_FIRE, AVASoundTracks.SCAR_L_RELOAD, AVASoundTracks.SCAR_L_DRAW);
    public static final DeferredItem<Item> SCAR_L = createNormal(ITEMS, "scar_l", SCAR_L_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.SCAR_L);


    private static final AVAItemGun.Properties M1_GARAND_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(70.4F).damageFloating(-2.5F, 1.5F).penetration(68.0F).range(55.4F).fireRate(9.09F).fireAnimationTime(5).capacity(8).mobility(93.5F)
            .reloadTime(50).drawSpeed(25).aimTime(3).shakeFactor(10.0F).shakeTurnChance(1.0F)
            .scopeType(AVAItemGun.ScopeTypes.GM94).trail().manual()
            .initialAccuracy(67.9F, 74.7F, 60.9F, 55.8F, 90.2F, 93.0F, 90.2F, 49.8F)
            .accuracy(67.9F, 74.7F, 60.9F, 55.8F, 90.2F, 93.0F, 90.2F, 49.8F)
            .stability(89.5F, 92.6F, 88.0F, 79.7F, 92.7F, 98.1F, 97.8F, 82.6F))
            .sounds(AVASoundTracks.M1_GARAND_FIRE, AVASoundTracks.M1_GARAND_RELOAD, AVASoundTracks.M1_GARAND_DRAW);
    public static final DeferredItem<Item> M1_GARAND = createNormal(ITEMS, "m1_garand", M1_GARAND_P.magazineType(SMALL_RIFLE_MAGAZINE), AVAGunRecipes.M1_GARAND);
    public static final DeferredItem<Item> M1_GARAND_R_LAB = createSkinnedNormal(ITEMS, "r_lab", M1_GARAND, AVASharedRecipes.R_LAB);


    private static final AVAItemGun.Properties G36KA1_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(37.0F).damageFloating(3.0F).penetration(28F).range(65.1F).fireRate(10.00F).capacity(30).mobility(93.5F)
            .reloadTime(45).drawSpeed(23).aimTime(3).shakeFactor(1.75F).shakeTurnChance(0.05F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail()
            .initialAccuracy(92.0F, 94.0F, 80.7F, 62.1F, 88.2F, 89.9F, 71.8F, 43.8F)
            .accuracy(84.9F, 86.6F, 73.4F, 46.5F, 85.9F, 87.7F, 64.3F, 35.6F)
            .stability(85.6F, 91.2F, 83.8F, 80.3F, 74.4F, 78.1F, 65.2F, 51.4F))
            .sounds(AVASoundTracks.G36KA1_FIRE, AVASoundTracks.G36KA1_RELOAD, AVASoundTracks.G36KA1_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-1.0F, -1.0F, -1.9F, -2.4F).accuracy(-0.6F, -0.6F, -1.0F, -1.1F).fireRate(0.54F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(1.8F, 1.8F, 0.0F, 0.0F).accuracy(1.6F, 1.6F, 0.0F, 0.0F).fireRate(-0.58F))
            .stat(GunAttachmentTypes.SHOCK_ABSORBER, GunStatBuilder.of().initialAccuracy(2.8F, 3.0F, -3.1F, -3.9F).accuracy(2.9F,3.0F,-1.7F,-1.7F))
            .stat(GunAttachmentTypes.SILICON_GRIP, GunStatBuilder.of().initialAccuracy(0.3F,0.3F,0.6F,0.9F).accuracy(0.9F,0.8F,1.4F,1.5F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().stability(0.6F, 0.1F, 0.0F, 0.0F).fireRate(-0.3F));
    public static final DeferredItem<Item> G36KA1 = createNormal(ITEMS, "g36ka1", G36KA1_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.G36KA1);


    private static final AVAItemGun.Properties XCR_P = new AVAItemGun.Properties(Classification.RIFLE, GunStatBuilder.of()
            .damage(37.0F).damageFloating(3.0F).penetration(45.0F).range(66.9F).fireRate(9.52F).capacity(30).mobility(96.2F)
            .reloadTime(46).drawSpeed(22).aimTime(5).shakeFactor(1.5F).spreadFactor(1.5F).spreadRecoveryFactor(1.1F).shakeTurnChance(0.025F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail()
            .initialAccuracy(89.0F, 90.1F, 86.5F, 58.8F, 87.5F, 88.5F, 75.6F, 58.1F)
            .accuracy(81.7F, 83.4F, 78.8F, 46.9F, 81.6F, 83.3F, 66.1F, 46.9F)
            .stability(81.1F, 84.0F, 82.6F, 65.0F, 79.6F, 82.1F, 83.2F, 60.6F))
            .sounds(AVASoundTracks.XCR_FIRE, AVASoundTracks.XCR_RELOAD, AVASoundTracks.XCR_DRAW)
            .stat(GunAttachmentTypes.MECHANISM_IMPROVEMENT, GunStatBuilder.of().fireRate(0.48F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().stability(1.7F,2.0F,0.0F,0.0F).fireRate(-0.26F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(2.1F,2.1F,1.9F,0.1F).accuracy(1.7F,1.9F,1.5F,0.1F));
    public static final DeferredItem<Item> XCR = createNormal(ITEMS, "xcr", XCR_P.magazineType(REGULAR_RIFLE_MAGAZINE), AVAGunRecipes.XCR);


    public static ArrayList<Item> ITEM_RIFLES = new ArrayList<>();
}
