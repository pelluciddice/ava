package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.items.throwables.HandGrenadeItem;
import pellucid.ava.items.throwables.SmokeGrenade;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.items.throwables.ToxicSmokeGrenade;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.ArrayList;
import java.util.List;

public class Projectiles
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);

    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * assault rifle (e.g. aks-74u): +5.5
     * pistols (e.g. p226): +6
     */
    public static final DeferredItem<Item> M67 = ITEMS.register("m67", () -> new HandGrenadeItem(new Item.Properties(), false, 0, 130, 1.0F, 60, 3.0F, AVAEntities.M67, AVASounds.GENERIC_GRENADE_EXPLODE.get(), AVAGunRecipes.M67, AVAWeaponUtil.Classification.PROJECTILE));
    public static final DeferredItem<Item> M67_SPORTS = ITEMS.register("m67_sports", () -> new HandGrenadeItem(new Item.Properties(), true, 0, 130, 1.0F, 60, 3.0F, AVAEntities.M67,
            AVASounds.GENERIC_GRENADE_EXPLODE.get(), AVAGunRecipes.M67_SPORTS, AVAWeaponUtil.Classification.PROJECTILE));
    public static final DeferredItem<Item> MK3A2 = ITEMS.register("mk3a2", () -> new HandGrenadeItem(new Item.Properties(), false, 60, 130, 0.75F, 30, 2.0F, AVAEntities.MK3A2,
            AVASounds.MK3A2_EXPLODE.get(), AVAGunRecipes.MK3A2, AVAWeaponUtil.Classification.PROJECTILE));
    public static final DeferredItem<Item> M116A1 = ITEMS.register("m116a1", () -> new HandGrenadeItem(new Item.Properties(), false, 80, 0, 0.75F, 30, 5.0F, AVAEntities.M116A1,
            AVASounds.FLASH_GRENADE_EXPLODE.get(), AVAGunRecipes.M116A1, AVAWeaponUtil.Classification.PROJECTILE));

    public static final DeferredItem<Item> M18_GREY = ITEMS.register("m18_grey", () -> new SmokeGrenade(AVAGunRecipes.M18_GREY, 0xFFFFFF, 150, 4, 100, 260, 60));
    public static final DeferredItem<Item> M18_GREY_II = ITEMS.register("m18_grey_2", () -> new SmokeGrenade(AVAGunRecipes.M18_GREY_II, 0xFFFFFF, 125, 3, 60, 220, 50));
    public static final DeferredItem<Item> M18_GREY_III = ITEMS.register("m18_grey_3", () -> new SmokeGrenade(AVAGunRecipes.M18_GREY_III, 0xFFFFFF, 100, 2, 20, 180, 40));
    public static final DeferredItem<Item> M18_RED = ITEMS.register("m18_red", () -> new SmokeGrenade(AVAGunRecipes.M18_RED, 0xCC4433, 150, 4, 100, 260, 60));
    public static final DeferredItem<Item> M18_RED_II = ITEMS.register("m18_red_2", () -> new SmokeGrenade(AVAGunRecipes.M18_RED_II, 0xCC4433, 125, 3, 60, 220, 50));
    public static final DeferredItem<Item> M18_RED_III = ITEMS.register("m18_red_3", () -> new SmokeGrenade(AVAGunRecipes.M18_RED_III, 0xCC4433, 100, 2, 20, 180, 40));
    public static final DeferredItem<Item> M18_PURPLE = ITEMS.register("m18_purple", () -> new SmokeGrenade(AVAGunRecipes.M18_PURPLE, 0x8800FF, 200, 5, 120, 300, 80));
    public static final DeferredItem<Item> M18_TOXIC = ITEMS.register("m18_toxic", () -> new ToxicSmokeGrenade(AVAGunRecipes.M18_TOXIC, 0x44EE44, 150, 5, 120, 300, 80));
    public static final DeferredItem<Item> M18_YELLOW = ITEMS.register("m18_yellow", () -> new SmokeGrenade(AVAGunRecipes.M18_YELLOW, 0xE0FF3A, 200, 5, 120, 300, 80));
    public static final DeferredItem<Item> M18_BLUE = ITEMS.register("m18_blue", () -> new SmokeGrenade(AVAGunRecipes.M18_BLUE, 0x04D7FF, 200, 5, 120, 300, 80));

    public static List<ThrowableItem> ITEM_PROJECTILES = new ArrayList<>();
}
