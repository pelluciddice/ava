package pellucid.ava.items.init;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Rarity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.fluids.AVAFluids;
import pellucid.ava.items.armours.AVAArmourMaterials;
import pellucid.ava.items.armours.CharacterArmour;
import pellucid.ava.items.armours.models.StandardEUArmourModel;
import pellucid.ava.items.armours.models.StandardNRFArmourModel;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.items.miscs.CraftableAmmoKit;
import pellucid.ava.items.miscs.MagicStickItem;
import pellucid.ava.items.miscs.ParachuteItem;
import pellucid.ava.items.miscs.TestItem;
import pellucid.ava.items.weapon_chest.WeaponChest;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.ArrayList;

public class MiscItems
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    public static final DeferredItem<Item> EU_STANDARD_BOOTS = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("eu_standard_boots", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_EU, ArmorItem.Type.BOOTS, new Item.Properties(), AVAGunRecipes.EU_STANDARD_BOOTS)));
    public static final DeferredItem<Item> EU_STANDARD_TROUSERS = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("eu_standard_trousers", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_EU, ArmorItem.Type.LEGGINGS, new Item.Properties(), AVAGunRecipes.EU_STANDARD_TROUSERS)));
    public static final DeferredItem<Item> EU_STANDARD_KEVLAR = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("eu_standard_kevlar", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_EU, ArmorItem.Type.CHESTPLATE, new Item.Properties(), AVAGunRecipes.EU_STANDARD_KEVLAR)));
    public static final DeferredItem<Item> EU_STANDARD_HELMET = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("eu_standard_helmet", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_EU, ArmorItem.Type.HELMET, new Item.Properties(), AVAGunRecipes.EU_STANDARD_HELMET)));

    public static final DeferredItem<Item> NRF_STANDARD_BOOTS = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("nrf_standard_boots", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_NRF, ArmorItem.Type.BOOTS, new Item.Properties(), AVAGunRecipes.NRF_STANDARD_BOOTS)));
    public static final DeferredItem<Item> NRF_STANDARD_TROUSERS = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("nrf_standard_trousers", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_NRF, ArmorItem.Type.LEGGINGS, new Item.Properties(), AVAGunRecipes.NRF_STANDARD_TROUSERS)));
    public static final DeferredItem<Item> NRF_STANDARD_KEVLAR = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("nrf_standard_kevlar", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_NRF, ArmorItem.Type.CHESTPLATE, new Item.Properties(), AVAGunRecipes.NRF_STANDARD_KEVLAR)));
    public static final DeferredItem<Item> NRF_STANDARD_HELMET = AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register("nrf_standard_helmet", () -> new CharacterArmour(AVAArmourMaterials.STANDARD_NRF, ArmorItem.Type.HELMET, new Item.Properties(), AVAGunRecipes.NRF_STANDARD_HELMET)));

//    public static final DeferredItem<Item> TEST_ARMOUR = ITEMS.register();

    public static final DeferredItem<Item> TEST_ITEM = ITEMS.register("test_item", () -> new TestItem());
    public static final DeferredItem<Item> TEST_ITEM_2 = ITEMS.register("test_item_2", () -> new TestItem());
    public static final DeferredItem<Item> AMMO_KIT = ITEMS.register("ammo_kit", () -> new CraftableAmmoKit(200));
    public static final DeferredItem<Item> AMMO_KIT_I = ITEMS.register("ammo_kit_i", () -> new CraftableAmmoKit(1000));
    public static final DeferredItem<Item> AMMO_KIT_II = ITEMS.register("ammo_kit_ii", () -> new AmmoKit(-1));

    public static final DeferredItem<Item> WEAPON_CHEST_MAIN = ITEMS.register("weapon_chest_main", () -> new WeaponChest(AVAWeaponUtil::getMainWeapons));
    public static final DeferredItem<Item> WEAPON_CHEST_SECONDARY = ITEMS.register("weapon_chest_secondary", () -> new WeaponChest(AVAWeaponUtil::getSecondaryWeapons));
    public static final DeferredItem<Item> WEAPON_CHEST_MELEE = ITEMS.register("weapon_chest_melee", () -> new WeaponChest(AVAWeaponUtil::getMeleeWeapons));
    public static final DeferredItem<Item> WEAPON_CHEST_PROJECTILE = ITEMS.register("weapon_chest_projectile", () -> new WeaponChest(AVAWeaponUtil::getProjectileWeapons));
    public static final DeferredItem<Item> WEAPON_CHEST_SPECIAL_WEAPON = ITEMS.register("weapon_chest_special_weapon", () -> new WeaponChest(AVAWeaponUtil::getSpecialWeapons));

    public static final DeferredItem<Item> C4 = ITEMS.register("c4", () -> new C4Item());

    public static final DeferredItem<Item> PARACHUTE = ITEMS.register("parachute", () -> new ParachuteItem());

    public static final DeferredItem<Item> VOID_WATER_BUCKET = AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register("void_water_bucket", () -> new BucketItem(AVAFluids.VOID_WATER.get(), new Item.Properties().rarity(Rarity.EPIC).craftRemainder(Items.BUCKET).stacksTo(1)) {
        @Override
        public boolean isFoil(ItemStack stack)
        {
            return true;
        }
    }));
    public static final DeferredItem<Item> MAGIC_STICK = ITEMS.register("magic_stick", () -> new MagicStickItem());

    public static final ArrayList<Item> ITEM_MISCS = new ArrayList<>();

    public static void commonSetup()
    {
        ITEM_MISCS.add(EU_STANDARD_BOOTS.get());
        ITEM_MISCS.add(EU_STANDARD_TROUSERS.get());
        ITEM_MISCS.add(EU_STANDARD_KEVLAR.get());
        ITEM_MISCS.add(EU_STANDARD_HELMET.get());
        ITEM_MISCS.add(NRF_STANDARD_BOOTS.get());
        ITEM_MISCS.add(NRF_STANDARD_TROUSERS.get());
        ITEM_MISCS.add(NRF_STANDARD_KEVLAR.get());
        ITEM_MISCS.add(NRF_STANDARD_HELMET.get());
        ITEM_MISCS.add(TEST_ITEM.get());
        ITEM_MISCS.add(AMMO_KIT.get());
        ITEM_MISCS.add(AMMO_KIT_I.get());
        ITEM_MISCS.add(AMMO_KIT_II.get());
        ITEM_MISCS.add(WEAPON_CHEST_MAIN.get());
        ITEM_MISCS.add(WEAPON_CHEST_SECONDARY.get());
        ITEM_MISCS.add(WEAPON_CHEST_MELEE.get());
        ITEM_MISCS.add(WEAPON_CHEST_PROJECTILE.get());
        ITEM_MISCS.add(WEAPON_CHEST_SPECIAL_WEAPON.get());
        ITEM_MISCS.add(C4.get());
        ITEM_MISCS.add(PARACHUTE.get());
        for (DeferredItem<Item> item : AVABlocks.CACHED_ITEMS)
            MiscItems.ITEM_MISCS.add(item.get());
    }

    @OnlyIn(Dist.CLIENT)
    public static void clientSetup()
    {
    }

    @OnlyIn(Dist.CLIENT)
    public static void armourModels()
    {
        standardArmour(EU_STANDARD_HELMET.get(), true);
        standardArmour(EU_STANDARD_KEVLAR.get(), true);
        standardArmour(EU_STANDARD_TROUSERS.get(), true);
        standardArmour(EU_STANDARD_BOOTS.get(), true);
        standardArmour(NRF_STANDARD_HELMET.get(), false);
        standardArmour(NRF_STANDARD_KEVLAR.get(), false);
        standardArmour(NRF_STANDARD_TROUSERS.get(), false);
        standardArmour(NRF_STANDARD_BOOTS.get(), false);
    }

    @OnlyIn(Dist.CLIENT)
    private static void standardArmour(Item armour, boolean eu)
    {
        EntityModelSet set = Minecraft.getInstance().getEntityModels();
        ((CharacterArmour) armour).setModel(eu ? new StandardEUArmourModel(set.bakeLayer(AVAModelLayers.STANDARD_EU_ARMOUR)) : new StandardNRFArmourModel(set.bakeLayer(AVAModelLayers.STANDARD_NRF_ARMOUR)));
    }
}
