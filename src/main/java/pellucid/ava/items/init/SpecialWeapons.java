package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.entities.shootables.M202RocketEntity;
import pellucid.ava.entities.throwables.GrenadeEntity;
import pellucid.ava.gun.stats.GunStatBuilder;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVASpecialWeapon;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.ArrayList;

public class SpecialWeapons
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    public static final DeferredItem<Item> M202_ROCKET = ITEMS.register("m202_rocket", () -> new Ammo(AVAGunRecipes.M202_ROCKET).setSpecial());
    public static final DeferredItem<Item> M202 = ITEMS.register("m202", () -> new AVASpecialWeapon(new AVAItemGun.Properties(AVAWeaponUtil.Classification.SPECIAL_WEAPON, GunStatBuilder.of().damage(140).range(91).initialAccuracy(100).accuracy(82).stability(80).fireRate(1).capacity(4).mobility(85.0F).reloadTime(50)
            .scopeType(AVAItemGun.ScopeTypes.M202).drawSpeed(13).penetration(0.0F).aimTime(4).noCrosshair().requireAim()).magazineType(M202_ROCKET).sounds(AVASoundTracks.M202_FIRE, AVASoundTracks.M202_RELOAD, AVASoundTracks.M4A1_DRAW), AVAGunRecipes.M202, (world, shooter, spread, weapon, stack) -> new M202RocketEntity(shooter, world, weapon, stack)));
    public static final DeferredItem<Item> GM94_GRENADE = ITEMS.register("gm94_grenade", () -> new Ammo(AVAGunRecipes.GM94_GRENADE).setSpecial());
    public static final DeferredItem<Item> GM94 = ITEMS.register("gm94", () -> new AVASpecialWeapon(new AVAItemGun.Properties(AVAWeaponUtil.Classification.SPECIAL_WEAPON, GunStatBuilder.of().damage(90).range(33).initialAccuracy(80).accuracy(80).stability(80).fireRate(1F).capacity(3).mobility(90.0F).reloadTime(43).aimTime(3)
            .scopeType(AVAItemGun.ScopeTypes.GM94).drawSpeed(20).penetration(0.0F)).magazineType(GM94_GRENADE).sounds(AVASoundTracks.GM94_FIRE, AVASoundTracks.GM94_RELOAD, AVASoundTracks.GM94_DRAW), AVAGunRecipes.GM94, (world, shooter, spread, weapon, stack) -> new GrenadeEntity(shooter, world, weapon)));
    public static final DeferredItem<Item> RPG7 = ITEMS.register("rpg7", () -> new AVASpecialWeapon(new AVAItemGun.Properties(AVAWeaponUtil.Classification.SPECIAL_WEAPON, GunStatBuilder.of().damage(250).range(100).initialAccuracy(100).accuracy(100).stability(50).fireRate(1F).fireAnimationTime(0).manual().capacity(1).mobility(85.0F).reloadTime(80).aimTime(7).exitAimOnFire()
            .scopeType(AVAItemGun.ScopeTypes.M202).drawSpeed(30).penetration(0.0F).noCrosshair().requireAim()).magazineType(M202_ROCKET).sounds(AVASoundTracks.RPG7_FIRE, AVASoundTracks.RPG7_RELOAD, AVASoundTracks.RPG7_DRAW), AVAGunRecipes.RPG7, (world, shooter, spread, weapon, stack) -> new M202RocketEntity(shooter, world, weapon, stack)));

    public static final DeferredItem<Item> BINOCULAR = ITEMS.register("binocular", () -> new BinocularItem());

    public static ArrayList<Item> ITEM_SPECIAL_WEAPONS = new ArrayList<>();
}
