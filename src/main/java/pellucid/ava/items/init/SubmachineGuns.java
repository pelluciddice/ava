package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.stats.GunStatBuilder;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVAMagazineShotgun;
import pellucid.ava.items.guns.AVAShotgun;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.AVASharedRecipes;
import pellucid.ava.sounds.AVASoundTracks;

import java.util.ArrayList;

import static pellucid.ava.items.init.Magazines.*;
import static pellucid.ava.util.AVAWeaponUtil.*;

public class SubmachineGuns
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */
    private static final AVAItemGun.Properties X95R_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(39).damageFloating(3.0F).penetration(35.0F).range(45).fireRate(11.76F).capacity(30).mobility(93 + 5.5F)
            .reloadTime(43).drawSpeed(18).aimTime(4).shakeFactor(1.45F).shakeTurnChance(0.125F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail()
            .initialAccuracy(80.5F, 82.6F, 76.3F, 68.0F, 84.5F, 86.5F, 81.7F, 76.3F)
            .accuracy(75.5F, 77.9F, 70.9F, 62.1F, 83.9F, 86.0F, 80.8F, 75.2F)
            .stability(82.9F, 84.8F, 80.2F, 75.8F, 83.3F, 85.8F, 80.8F, 76.2F))
            .sounds(AVASoundTracks.X95R_FIRE, AVASoundTracks.X95R_RELOAD, AVASoundTracks.X95R_DRAW)
            .stat(GunAttachmentTypes.REINFORCED_BARREL, GunStatBuilder.of().damage(3.0F).fireRate(-0.65F))
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-0.5F).accuracy(-0.4F, -0.3F, -0.4F, -0.4F).fireRate(0.74F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().recoilCompensation(0.5F, 0.6F, 0.6F, 0.5F).fireRate(-0.4F))
            .stat(GunAttachmentTypes.MECHANISM_IMPROVEMENT, GunStatBuilder.of().initialAccuracy(-0.1F).accuracy(-1.1F, -1.0F, -1.1F, -1.2F).fireRate(0.74F))
            .stat(GunAttachmentTypes.SILICON_GRIP, GunStatBuilder.of().initialAccuracy(0.5F, 0.4F, 0.5F, 0.6F).accuracy(-0.5F, -0.4F, -0.5F, -0.5F).recoilCompensation(1.2F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.1F, 1.1F, 1.0F, 0.8F).accuracy(0.9F, 1.0F, 0.8F, 0.7F));
    public static final DeferredItem<Item> X95R = createNormal(ITEMS, "x95r", X95R_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.X95R);
    public static final DeferredItem<Item> X95R_AUBE = createSkinnedNormal(ITEMS, "aube", X95R, AVASharedRecipes.AUBE);
    public static final DeferredItem<Item> X95R_CHRISTMAS = createSkinnedNormal(ITEMS, "christmas", X95R, AVASharedRecipes.CHRISTMAS);


    private static final AVAItemGun.Properties MP5SD5_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(34.0F).damageFloating(2.5F).penetration(35.0F).range(44.2F).fireRate(12.35F).capacity(30).mobility(90.1F + 5.5F)
            .reloadTime(42).drawSpeed(20).shakeFactor(1.65F).shakeTurnChance(0.05F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).silenced()
            .initialAccuracy(86.9F, 89.5F, 83.6F, 85.6F, 86.2F, 88.6F, 76.7F, 67.9F)
            .accuracy(80.8F, 84.2F, 76.5F, 68.1F, 85.5F, 87.9F, 69.9F, 62.6F)
            .stability(89.2F, 92.4F, 89.4F, 83.7F, 90.3F, 92.4F, 79.0F, 75.1F))
            .sounds(AVASoundTracks.MP5SD5_FIRE, AVASoundTracks.MP5SD5_RELOAD, AVASoundTracks.MP5SD5_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-0.7F, -0.6F, -3.5F, -2.7F).accuracy(-0.6F, -0.6F, -2.7F, -2.2F).fireRate(0.81F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(1.6F, 1.8F, 0.0F, 0.0F).accuracy(1.6F, 1.7F, 0.0F, 0.0F).fireRate(-0.86F))
            .stat(GunAttachmentTypes.SPETSNAZ_BARREL, GunStatBuilder.of().accuracy(0.7F, 0.6F, 2.8F, 2.2F).stability(0.1F).mobility(-2.9F))
            .stat(GunAttachmentTypes.MECHANISM_IMPROVEMENT, GunStatBuilder.of().fireRate(0.81F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(2.0F, 2.2F, 0.8F, 0.6F).accuracy(2.0F, 2.1F, 0.6F, 0.4F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.2F, 1.3F, 1.1F, 0.9F).accuracy(1.2F, 1.3F, 0.8F, 0.6F))
            .stat(GunAttachmentTypes.SILICON_GRIP, GunStatBuilder.of().initialAccuracy(0.2F, 0.3F, 1.3F, 1.0F).accuracy(0.6F, 0.5F, 2.5F, 2.0F));
    public static final DeferredItem<Item> MP5SD5 = createNormal(ITEMS, "mp5sd5", MP5SD5_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.MP5SD5);


    private static final AVAItemGun.Properties MK18_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(44.2F).damageFloating(3.0F).penetration(35.0F).range(36).fireRate(13.33F).capacity(30).mobility(90.1F + 5.5F)
            .reloadTime(45).drawSpeed(25).spreadMax(0.7F).shakeTurnChance(0.33F).shakeFactor(1.35F)
            .silenced()
            .initialAccuracy(86.9F, 89.5F, 83.6F, 85.6F)
            .accuracy(83.1F, 86.2F, 79.5F, 74.0F)
            .stability(90.3F, 93.1F, 90.4F, 85.1F))
            .sounds(AVASoundTracks.MK18_FIRE, AVASoundTracks.MK18_RELOAD, AVASoundTracks.MK18_DRAW);
    public static final DeferredItem<Item> MK18 = createNormal(ITEMS, "mk18", MK18_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.MK18);
    public static final DeferredItem<Item> MK18_AIR_WARFARE = createSkinnedNormal(ITEMS, "air_warfare", MK18, AVASharedRecipes.AIR_WARFARE);
    public static final DeferredItem<Item> MK18_KUYO_MON = createSkinnedNormal(ITEMS, "kuyo_mon", MK18, AVASharedRecipes.KUYO_MON);


    private static final AVAItemGun.Properties REMINGTON870_P = new AVAItemGun.Properties(Classification.SHOTGUN, GunStatBuilder.of()
            .damage(40.5F).damageFloating(1.5F).penetration(0.0F).range(30).fireRate(1.85F).fireAnimationTime(14) .capacity(7).mobility(81.5F + 5.5F)
            .reloadTime(13).preReloadTime(5).postReloadTime(13).drawSpeed(20).shakeFactor(0.35F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail().pellets(8).extraReloadSteps()
            .initialAccuracy(65.3F, 65.3F, 63.4F, 58.4F, 66.0F, 66.0F, 66.0F, 66.0F)
            .accuracy(65.3F, 65.3F, 63.4F, 58.4F, 66.0F, 66.0F, 66.0F, 66.0F)
            .stability(21.5F, 37.2F, 5.8F, 0.0F, 21.5F, 37.2F, 5.8F, 0.0F))
            .sounds(AVASoundTracks.REMINGTON870_FIRE, AVASoundTracks.REMINGTON870_RELOAD, AVASoundTracks.REMINGTON870_DRAW).postReloadSound(AVASoundTracks.REMINGTON870_POST_RELOAD)
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(1.2F));
    //shotguns -> .initialAccuracy(-10.0F).fireRate(gunProperties.getSpeed() * 2.0F).damage(gunProperties.getDamage() / 1.4F)
    public static final DeferredItem<Item> REMINGTON870 = ITEMS.register("remington870", () -> new AVAShotgun(REMINGTON870_P.magazineType(AMMO_SHOTGUN), AVAGunRecipes.REMINGTON870));
    public static final DeferredItem<Item> REMINGTON870_DREAMCATCHER = createSkinned(ITEMS, "dreamcatcher", REMINGTON870, AVASharedRecipes.DREAMCATCHER, AVAShotgun::new);


    private static final AVAItemGun.Properties MP5K_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(34.5F).damageFloating(2.5F).penetration(0.0F).range(43.6F).fireRate(12.5F).capacity(30).mobility(95.0F)
            .reloadTime(40).drawSpeed(17).aimTime(4).shakeFactor(5.0F).shakeTurnChance(0.1F).spreadRecoveryFactor(3.5F).spreadFactor(0.6F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail().optionalSilencer()
            .initialAccuracy(80.5F, 83.5F, 77.3F, 76.0F, 83.0F, 85.9F, 76.1F, 74.8F)
            .accuracy(68.9F, 70.3F, 61.6F, 58.1F, 75.7F, 76.7F, 57.2F, 53.4F)
            .stability(90.6F, 92.5F, 87.3F, 85.9F, 90.6F, 92.5F, 83.5F, 81.6F))
            .sounds(AVASoundTracks.MP5K_FIRE, AVASoundTracks.MP5K_RELOAD, AVASoundTracks.MP5K_DRAW)
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().stability(1.7F).fireRate(-0.42F))
            .stat(GunAttachmentTypes.MECHANISM_IMPROVEMENT, GunStatBuilder.of().accuracy(-0.1F, -0.1F, -0.2F, -0.2F).fireRate(0.78F))
            .stat(GunAttachmentTypes.SILICON_GRIP, GunStatBuilder.of().initialAccuracy(0.1F).accuracy(0.4F, 0.4F, 1.8F, 1.8F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.1F, 1.3F, 1.0F, 1.0F).accuracy(1.1F, 1.1F, 0.9F, 0.9F));
    public static final DeferredItem<Item> MP5K = createNormal(ITEMS, "mp5k", MP5K_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.MP5K);
    public static final DeferredItem<Item> MP5K_FROST = createSkinnedNormal(ITEMS, "frost", MP5K, AVASharedRecipes.FROST);

    private static final AVAItemGun.Properties SR_2M_VERESK_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(37F).damageFloating(4.0F).penetration(65.0F).range(41).fireRate(12.5F).capacity(20).mobility(93.6F + 5.5F)
            .reloadTime(40).drawSpeed(14).aimTime(4).shakeFactor(0.9F).shakeTurnChance(0.15F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail()
            .initialAccuracy(79.9F, 80.4F, 75.5F, 73.3F, 82.0F, 82.4F, 71.0F, 68.2F)
            .accuracy(68.9F, 70.3F, 61.6F, 58.1F, 75.5F, 76.7F, 57.2F, 53.4F)
            .stability(80.2F, 80.2F, 76.3F, 70.3F, 80.2F, 80.2F, 69.1F, 61.4F))
            .sounds(AVASoundTracks.SR_2M_VERESK_FIRE, AVASoundTracks.SR_2M_VERESK_RELOAD, AVASoundTracks.SR_2M_VERESK_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-0.8F, -0.8F, -1.2F, -1.3F).accuracy(-0.4F, -0.5F, -0.6F, -0.6F).fireRate(0.83F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(1.5F, 1.5F, 0.0F, 0.0F).accuracy(1.2F, 1.2F, 0.0F, 0.0F).fireRate(-0.87F))
            .stat(GunAttachmentTypes.MECHANISM_IMPROVEMENT, GunStatBuilder.of().accuracy(-0.5F, -0.5F, -0.6F, -0.7F).fireRate(0.83F))
            .stat(GunAttachmentTypes.EXTENDED_MAGAZINE, GunStatBuilder.of().capacity(10).mobility(-1.1F))
            .stat(GunAttachmentTypes.SILICON_GRIP, GunStatBuilder.of().initialAccuracy(0.3F, 0.2F, 0.4F, 0.5F).accuracy(2.1F, 1.9F, 2.6F, 2.9F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.1F, 1.1F, 0.9F, 0.9F).accuracy(0.9F, 0.9F, 0.8F, 0.7F));
    public static final DeferredItem<Item> SR_2M_VERESK = createNormal(ITEMS, "sr_2m_veresk", SR_2M_VERESK_P.magazineType(SMALL_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.SR_2M_VERESK);
    public static final DeferredItem<Item> SR_2M_VERESK_SUMIRE = createSkinnedNormal(ITEMS, "sumire", SR_2M_VERESK, AVASharedRecipes.SUMIRE);


    private static final AVAItemGun.Properties D_DEFENSE_10GA_P = new AVAItemGun.Properties(Classification.SHOTGUN, GunStatBuilder.of()
            .damage(71.5F).damageFloating(1.5F).range(23.8F).penetration(0.0F).fireRate(1F).capacity(2).mobility(81.5F + 5.5F)
            .reloadTime(40).drawSpeed(29).shakeFactor(0.5F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail().pellets(8)
            .initialAccuracy(55.8F, 59.5F, 50.9F, 41.4F, 61.6F, 66.2F, 55.8F, 46.9F)
            .accuracy(45.9F, 49.8F, 41.2F, 32.2F, 52.0F, 56.9F, 45.9F, 37.3F)
            .stability(25.0F, 11.9F, 11.9F, 11.9F, 35.0F, 11.9F, 11.9F, 11.9F))
            .sounds(AVASoundTracks.D_DEFENSE_10GA_FIRE, AVASoundTracks.D_DEFENSE_10GA_RELOAD, AVASoundTracks.D_DEFENSE_10GA_DRAW);
    public static final DeferredItem<Item> D_DEFENSE_10GA = ITEMS.register("d_defense_10ga", () -> new AVAMagazineShotgun(D_DEFENSE_10GA_P.magazineType(AMMO_SHOTGUN), AVAGunRecipes.D_DEFENSE_10GA, 8));


    private static final AVAItemGun.Properties KRISS_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(41.5F).damageFloating(4.5F).penetration(15.0F).range(48.5F).fireRate(12.5F).capacity(28).mobility(95.6F + 5.5F)
            .reloadTime(45).drawSpeed(16).aimTime(4).shakeFactor(1.25F).shakeTurnChance(0.5F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail().optionalSilencer()
            .initialAccuracy(77.5F, 79.3F, 72.0F, 69.6F, 79.0F, 80.6F, 69.9F, 66.7F)
            .accuracy(77.2F, 79.0F, 70.8F, 68.3F, 78.8F, 80.5F, 68.5F, 65.1F)
            .stability(91.3F, 91.7F, 86.0F, 83.1F, 88.3F, 90.8F, 80.1F, 77.4F))
            .sounds(AVASoundTracks.KRISS_SUPER_V_FIRE, AVASoundTracks.KRISS_SUPER_V_RELOAD, AVASoundTracks.KRISS_SUPER_V_DRAW)
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(2.5F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().stability(1.1F).fireRate(-0.45F))
            .stat(GunAttachmentTypes.SOFT_GRIP, GunStatBuilder.of().initialAccuracy(-1.3F, -1.4F, -1.2F, -1.1F).accuracy(-1.3F, -1.4F, -1.1F, -1.0F).recoilCompensation(0.31F))
            .stat(GunAttachmentTypes.CARBON_GRIP, GunStatBuilder.of().initialAccuracy(0.0F, 0.1F, 0.2F, 0.2F).accuracy(0.1F, 0.1F, 0.6F, 0.7F))
            .stat(GunAttachmentTypes.RECOIL_CONTROL_STOCK, GunStatBuilder.of().stability(4.4F));
    public static final DeferredItem<Item> KRISS = createNormal(ITEMS, "kriss_super_v", KRISS_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.KRISS);
    public static final DeferredItem<Item> KRISS_CHRISTMAS = createSkinnedNormal(ITEMS, "christmas", KRISS, AVASharedRecipes.CHRISTMAS);


    // AP 0.0F -> 10.0F
    private static final AVAItemGun.Properties CZ_EVO3_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(31.5F).damageFloating(2.5F).penetration(10.0F).range(44.7F).fireRate(14.29F).fireAnimationTime(2).capacity(30).mobility(96.8F + 5.5F)
            .reloadTime(47).drawSpeed(15).shakeFactor(3.5F).shakeTurnChance(0.25F).spreadRecoveryFactor(2.0F).spreadMax(0.85F)
            .trail()
            .initialAccuracy(81.8F, 83.6F, 77.9F, 75.0F)
            .accuracy(77.0F, 78.9F, 73.0F, 69.0F)
            .stability(88.6F, 89.5F, 86.0F, 81.2F))
            .sounds(AVASoundTracks.CZ_EVO3_FIRE, AVASoundTracks.CZ_EVO3_RELOAD, AVASoundTracks.CZ_EVO3_DRAW);
    public static final DeferredItem<Item> CZ_EVO3 = createNormal(ITEMS, "cz_evo3", CZ_EVO3_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.CZ_EVO3);
    public static final DeferredItem<Item> CZ_EVO3_COTTON_CANDY = createSkinnedNormal(ITEMS, "cotton_candy", CZ_EVO3, AVASharedRecipes.COTTON_CANDY);


    private static final AVAItemGun.Properties K1A1_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(36.0F).damageFloating(3.0F).penetration(28.0F).range(49.8F).fireRate(11.43F).capacity(30).mobility(93.2F + 5.5F)
            .reloadTime(40).drawSpeed(22).aimTime(3)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail()
            .initialAccuracy(81.9F, 81.9F, 79.0F, 75.0F, 84.1F, 84.1F, 72.9F, 68.3F)
            .accuracy(73.7F, 73.7F, 70.0F, 65.1F, 81.1F, 81.1F, 67.1F, 62.0F)
            .stability(85.1F, 87.6F, 80.9F, 78.3F, 85.1F, 87.6F, 75.9F, 72.9F))
            .sounds(AVASoundTracks.K1A1_FIRE, AVASoundTracks.K1A1_RELOAD, AVASoundTracks.K1A1_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-1.4F, -1.2F, -1.6F, -1.9F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(-0.8F, -0.6F, -0.8F, -1.0F, 0.0F, 0.0F, 0.0F, 0.0F).fireRate(0.74F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(1.6F, 1.7F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(1.4F, 1.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F).fireRate(-0.77F))
            .stat(GunAttachmentTypes.ADVANCED_TRIGGER, GunStatBuilder.of().range(-2.4F).spreadRecovery(0.25F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(1.8F, 1.9F, 1.6F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(1.7F, 1.9F, 1.6F, 1.1F, 0.0F, 0.0F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.SOFT_GRIP, GunStatBuilder.of().initialAccuracy(-3.0F, -3.1F, -3.0F, -2.4F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(-0.9F, 1.2F, -0.5F, 0.4F, 0.0F, 0.0F, 0.0F, 0.0F).spreadRecovery(0.65F))
            .stat(GunAttachmentTypes.SILICON_GRIP, GunStatBuilder.of().initialAccuracy(0.5F, 0.5F, 0.5F, 0.6F, -1.1F, -0.9F, -1.1F, -1.4F));
    public static final DeferredItem<Item> K1A1 = createNormal(ITEMS, "k1a1", K1A1_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.K1A1);
    public static final DeferredItem<Item> K1A1_RED_SECTOR = createSkinnedNormal(ITEMS, "red_sector", K1A1, AVASharedRecipes.RED_SECTOR);
    public static final DeferredItem<Item> K1A1_SKILLED = createSkinnedNormal(ITEMS, "skilled", K1A1, AVASharedRecipes.SKILLED);


    private static final AVAItemGun.Properties MP7A1_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(29.0F).damageFloating(3.0F).penetration(50.0F).range(42.4F).fireRate(12.99F).fireAnimationTime(2).capacity(40).mobility(93.6F + 5.5F)
            .reloadTime(38).drawSpeed(17).aimTime(3).spreadFactor(0.45F).shakeFactor(1.25F).shakeTurnChance(0.15F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail().optionalSilencer()
            .initialAccuracy(78.1F, 79.9F, 74.6F, 69.6F, 80.3F, 81.9F, 75.0F, 66.6F)
            .accuracy(72.7F, 74.8F, 70.1F, 63.3F, 77.8F, 79.5F, 70.2F, 61.1F)
            .stability(87.5F, 88.8F, 82.0F, 75.7F, 87.5F, 88.8F, 82.0F, 75.7F))
            .sounds(AVASoundTracks.MP7A1_FIRE, AVASoundTracks.MP7A1_RELOAD, AVASoundTracks.MP7A1_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-0.8F, -0.8F, -0.7F, -1.0F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(-0.5F, -0.5F, -0.4F, 0.6F).fireRate(0.9F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(1.4F, 1.4F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(1.2F, 1.2F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F).fireRate(-0.94F))
            .stat(GunAttachmentTypes.MECHANISM_IMPROVEMENT, GunStatBuilder.of().accuracy(-0.5F, -0.5F, -0.4F, -0.6F).fireRate(0.9F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(1.7F, 1.7F, 1.5F, 1.2F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(1.5F, 1.6F, 1.4F, 1.0F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().initialAccuracy(0.4F, 0.4F, 0.4F, 1.2F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(1.2F, 1.2F, 1.1F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F).spreadRecovery(-0.2F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.1F, 1.1F, 0.9F, 0.8F, 0.0F, 0.0F, 0.0F, 0.0F).accuracy(0.9F, 0.9F, 0.8F, 0.7F, 0.0F, 0.0F, 0.0F, 0.0F));
    public static final DeferredItem<Item> MP7A1 = createNormal(ITEMS, "mp7a1", MP7A1_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.MP7A1);
    public static final DeferredItem<Item> MP7A1_LIGHT = createSkinnedNormal(ITEMS, "light", MP7A1, AVASharedRecipes.LIGHT);
    public static final DeferredItem<Item> MP7A1_SUMIRE = createSkinnedNormal(ITEMS, "sumire", MP7A1, AVASharedRecipes.SUMIRE);
    public static final DeferredItem<Item> MP7A1_VALKYRIE = createSkinnedNormal(ITEMS, "valkyrie", MP7A1, AVASharedRecipes.VALKYRIE);


    private static final AVAItemGun.Properties P90_P = new AVAItemGun.Properties(Classification.SUB_MACHINEGUN, GunStatBuilder.of()
            .damage(34.0F).damageFloating(1.0F).penetration(35.0F).range(39.2F).fireRate(11.76F).fireAnimationTime(2).capacity(50).mobility(93.2F + 5.5F)
            .reloadTime(52).drawSpeed(16).aimTime(4).shakeTurnChance(0.05F)
            .scopeType(AVAItemGun.ScopeTypes.X2).trail().optionalSilencer()
            .initialAccuracy(87.4F, 87.5F, 86.2F, 70.9F, 86.2F, 88.6F, 76.7F, 67.9F)
            .accuracy(85.4F, 85.7F, 81.4F, 62.7F, 85.5F, 87.9F, 69.9F, 62.6F)
            .stability(90.9F, 92.9F, 84.6F, 81.2F, 90.7F, 92.7F, 79.8F, 76.2F))
            .sounds(AVASoundTracks.P90_FIRE, AVASoundTracks.P90_RELOAD, AVASoundTracks.P90_DRAW);
    public static final DeferredItem<Item> P90 = createNormal(ITEMS, "p90", P90_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.MP7A1);


    private static final AVAItemGun.Properties FN_TPS_P = new AVAItemGun.Properties(Classification.SHOTGUN, GunStatBuilder.of()
            .damage(44.0F).damageFloating(1.5F).penetration(0.0F).range(25.0F).fireRate(1.67F).fireAnimationTime(14).capacity(5).mobility(81.5F + 5.5F)
            .reloadTime(13).preReloadTime(5).postReloadTime(13).drawSpeed(20).shakeFactor(0.35F)
            .scopeType(AVAItemGun.ScopeTypes.IRON_SIGHT).trail().pellets(8).extraReloadSteps()
            .initialAccuracy(56.8F, 56.8F, 56.8F, 56.8F, 61.9F, 61.9F, 61.9F, 61.9F)
            .accuracy(56.8F, 56.8F, 56.8F, 56.8F, 61.9F, 61.9F, 61.9F, 61.9F)
            .stability(31.3F, 37.6F, 26.1F, 19.9F, 29.6F, 36.0F, 24.4F, 18.0F))
            .sounds(AVASoundTracks.FN_TPS_FIRE, AVASoundTracks.FN_TPS_RELOAD, AVASoundTracks.FN_TPS_DRAW).postReloadSound(AVASoundTracks.FN_TPS_POST_RELOAD)
            .stat(GunAttachmentTypes.EXTENDED_MAGAZINE, GunStatBuilder.of().capacity(2).initialAccuracy(0.0F,0.0F,0.0F,0.0F,-1.2F,-1.2F,0.0F,0.0F))
            .stat(GunAttachmentTypes.BIRDSHOT_BARREL, GunStatBuilder.of().damage(-10.0F).range(-13.4F).pellets(19).initialAccuracy(-28.7F, -28.7F, -28.7F, -28.7F, -4.0F,-4.0F,-4.0F,-4.0F));
    //shotguns -> .initialAccuracy(-10.0F).fireRate(gunProperties.getSpeed() * 2.0F).damage(gunProperties.getDamage() / 1.4F)
    public static final DeferredItem<Item> FN_TPS = ITEMS.register("fn_tps", () -> new AVAShotgun(FN_TPS_P.magazineType(AMMO_SHOTGUN), AVAGunRecipes.FN_TPS));


    private static final AVAItemGun.Properties AKS74U_P = new AVAItemGun.Properties(Classification.SHOTGUN, GunStatBuilder.of()
            .damage(41.0F).damageFloating(1.5F).penetration(35.0F).range(47.2F).fireRate(12.05F).capacity(30).mobility(94.3F + 5.5F)
            .reloadTime(48).drawSpeed(24)
            .trail()
            .initialAccuracy(80.0F, 81.5F, 77.8F, 67.6F)
            .accuracy(72.2F, 73.3F, 68.3F, 56.7F)
            .stability(87.0F, 88.5F, 86.0F, 78.5F))
            .sounds(AVASoundTracks.AKS74U_FIRE, AVASoundTracks.AKS74U_RELOAD, AVASoundTracks.AKS74U_DRAW)
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().initialAccuracy(-2.5F, -2.7F, -3.1F, -3.8F).accuracy(-1.3F, -1.4F, -1.5F, -1.5F).fireRate(0.6F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().accuracy(-1.0F, -1.0F, -1.1F, -1.2F).fireRate(0.6F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().stability(0.6F, 0.7F, 0.0F, 0.0F).fireRate(-0.33F))
            .stat(GunAttachmentTypes.ERGONOMIC_GRIP, GunStatBuilder.of().initialAccuracy(1.1F, 1.1F, 1.1F, 0.8F).accuracy(0.8F, 0.7F, 0.6F, 0.4F))
            ;
    public static final DeferredItem<Item> AKS74U = createNormal(ITEMS, "aks74u", AKS74U_P.magazineType(REGULAR_SUB_MACHINEGUN_MAGAZINE), AVAGunRecipes.AKS74U);


    public static ArrayList<Item> ITEM_SUBMACHINE_GUNS = new ArrayList<>();
}
