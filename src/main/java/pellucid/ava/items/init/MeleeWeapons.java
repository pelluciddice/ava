package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.sounds.AVASoundTracks;

import java.util.ArrayList;
import java.util.List;

public class MeleeWeapons
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    public static final DeferredItem<Item> FIELD_KNIFE = ITEMS.register("field_knife", () -> new AVAMeleeItem(AVAGunRecipes.FIELD_KNIFE, 35, 70, 11, 11, 20, 1.3F, 1.7F, AVASoundTracks.FIELD_KNIFE_DRAW, AVASoundTracks.FIELD_KNIFE_ATTACK_LIGHT, AVASoundTracks.FIELD_KNIFE_ATTACK_HEAVY, 0.0035F));
    public static final DeferredItem<Item> SCYTHE_IGNIS = ITEMS.register("scythe_ignis", () -> new AVAMeleeItem(AVAGunRecipes.SCYTHE_IGNIS, 35, 70, 11, 11, 20, 1.3F, 1.7F, AVASoundTracks.SCYTHE_IGNIS_DRAW, AVASoundTracks.SCYTHE_IGNIS_ATTACK_LIGHT, AVASoundTracks.SCYTHE_IGNIS_ATTACK_HEAVY, 0.0035F));

    public static final List<AVAMeleeItem> ITEM_MELEE_WEAPONS = new ArrayList<>();
}
