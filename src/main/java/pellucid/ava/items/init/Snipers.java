package pellucid.ava.items.init;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.stats.GunStatBuilder;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.gun.stats.ScopeGunStat;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVAManual;
import pellucid.ava.items.guns.AVASingle;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.AVASharedRecipes;
import pellucid.ava.sounds.AVASoundTracks;

import java.util.ArrayList;

import static pellucid.ava.items.init.Magazines.*;
import static pellucid.ava.util.AVAWeaponUtil.*;

public class Snipers
{
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);
    /***
     * mobility modification:
     * rifle (e.g. m4a1): +0
     * sniper (e.g. mosin-nagant): -6
     * submachine gun (e.g. x95r): +5.5
     * pistols (e.g. p226): +6
     */

    //stability -= 70
    private static final AVAItemGun.Properties MOSIN_NAGANT_P = new AVAItemGun.Properties(Classification.SNIPER, GunStatBuilder.of()
            .damage(125).damageFloating(4.0F).range(96).fireRate(0.8F).capacity(5).mobility(100.0F - 6.0F)
            .reloadTime(21).preReloadTime(13).postReloadTime(13).drawSpeed(17).aimTime(2).recoilCompensation(0.25F).penetration(68.0F)
            .scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT).noCrosshair().trail().extraReloadSteps()
            .initialAccuracy(24.7F, 42.2F, 19.3F, 15.9F, 88.7F, 90.8F, 78.7F, 43.8F)
            .accuracy(24.7F, 42.2F, 19.3F, 15.9F, 88.7F, 90.8F, 78.7F, 43.8F)
            .stability(77.7F, 82.2F, 66.6F, 66.6F, 77.7F, 82.2F, 71.0F, 66.6F))
            .sounds(AVASoundTracks.MOSIN_NAGANT_FIRE, AVASoundTracks.MOSIN_NAGANT_RELOAD, AVASoundTracks.MOSIN_NAGANT_DRAW).preReloadSound(AVASoundTracks.MOSIN_NAGANT_PRE_RELOAD).postReloadSound(AVASoundTracks.MOSIN_NAGANT_POST_RELOAD);
    public static final DeferredItem<Item> MOSIN_NAGANT = create(ITEMS, "mosin_nagant", MOSIN_NAGANT_P.magazineType(AMMO_SNIPER), AVAGunRecipes.MOSIN_NAGANT, AVAManual::new);
    public static final DeferredItem<Item> MOSIN_NAGANT_SUMIRE = createSkinned(ITEMS, "sumire", MOSIN_NAGANT, AVASharedRecipes.SUMIRE, AVAManual::new);
    public static final DeferredItem<Item> MOSIN_NAGANT_SPORTS = createSkinned(ITEMS, "sports", MOSIN_NAGANT, AVASharedRecipes.SPORTS, AVAManual::new);


    private static final AVAItemGun.Properties M24_P = new AVAItemGun.Properties(Classification.SNIPER, GunStatBuilder.of()
            .damage(126.7F).damageFloating(4.0F).penetration(65.0F).range(94).fireRate(1.1F).capacity(5).mobility(99.9F - 6.0F)
            .reloadTime(36).drawSpeed(10).aimTime(2).recoilCompensation(0.25F)
            .scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT).noCrosshair().trail()
            .initialAccuracy(17.5F, 17.7F, 16.8F, 13.1F, 87.8F, 90.0F, 77.3F, 41.7F)
            .accuracy(17.5F, 17.7F, 16.8F, 13.1F, 87.8F, 90.0F, 77.3F, 41.7F)
            .stability(69.9F, 76.0F, 59.9F, 59.9F, 69.9F, 76.0F, 59.9F, 59.9F))
            .sounds(AVASoundTracks.M24_FIRE, AVASoundTracks.M24_RELOAD, AVASoundTracks.M24_DRAW)
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(3.2F))
            .stat(GunAttachmentTypes.QUICK_SCOPE, GunStatBuilder.of().mobility(-1.0F).aimTime(-1))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_SCOPE, GunStatBuilder.of().mobility(-1.5F).aimTime(1).scopeType(AVAItemGun.ScopeTypes.FR_F2))
            .stat(GunAttachmentTypes.SHOCK_ABSORBER, GunStatBuilder.of().initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, 4.9F, 3.2F, -0.9F, -0.5F));
    public static final DeferredItem<Item> M24 = create(ITEMS, "m24", M24_P.magazineType(SMALL_SNIPER_MAGAZINE), AVAGunRecipes.M24, AVASingle::new);
    public static final DeferredItem<Item> M24_FLEUR_DE_LYS = createSkinned(ITEMS, "fleur_de_lys", M24, AVASharedRecipes.FLEUR_DE_LYS, AVASingle::new);


    private static final AVAItemGun.Properties SR_25_P = new AVAItemGun.Properties(Classification.SNIPER_SEMI, GunStatBuilder.of()
            .damage(126.7F).damageFloating(4.0F).penetration(65.0F).range(89).fireRate(2).fireAnimationTime(3).capacity(20).mobility(94.3F - 6.0F)
            .reloadTime(40).drawSpeed(16).aimTime(3).shakeTurnChance(0.2F)
            .scopeType(AVAItemGun.ScopeTypes.SR25).noCrosshair().manual()
            .initialAccuracy(17.7F, 18.8F, 16.9F, 15.4F, 87.1F, 87.8F, 70.5F, 50.3F)
            .accuracy(85.1F, 86.0F, 66.7F, 44.3F, 85.1F, 86.0F, 66.7F, 44.3F)
            .stability(57.0F, 57.0F, 69.5F, 38.9F, 38.9F, 69.5F, 38.9F, 38.9F))
            .sounds(AVASoundTracks.SR_25_FIRE, AVASoundTracks.SR_25_RELOAD, AVASoundTracks.SR_25_DRAW)
            .stat(GunAttachmentTypes.QUICK_SCOPE, GunStatBuilder.of().mobility(-1.0F).aimTime(-1).scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT))
            .stat(GunAttachmentTypes.BURST_BARREL, GunStatBuilder.of().damage(8.0F).stability(-1.0F, 0.0F, -3.3F, -3.3F))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, 2.7F, 2.8F, 2.3F, 2.4F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(7.5F).stability(0.0F, 0.0F, 0.0F, 0.F, -0.1F, 0.0F, -0.1F, -0.1F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().accuracy(0.0F, 0.0F, 0.0F, 0.0F, 1.4F, 1.5F, -0.2F, -1.7F).initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, 1.6F, 1.8F, -0.4F, -2.4F))
            .stat(GunAttachmentTypes.CUSTOM_TRIGGER, GunStatBuilder.of().stability(0.0F, 0.0F, 0.0F, 0.0F, 4.0F, 4.1F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.WEIGHTED_GRIP, GunStatBuilder.of().stability(0.0F, 0.0F, 0.0F, 0.0F, 5.0F, 4.0F, 0.0F, 0.0F).mobility(-0.8F))
            .stat(GunAttachmentTypes.LIGHT_STOCK, GunStatBuilder.of().stability(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.2F, -0.2F).mobility(2.1F))
            .stat(GunAttachmentTypes.HEAVY_BARREL, GunStatBuilder.of().stability(0.0F, 0.0F, 0.0F, 0.0F, 3.1F, 2.7F, 0.0F, 0.0F).mobility(-2.1F))
            .stat(GunAttachmentTypes.FAST_REACTION_STOCK, GunStatBuilder.of().range(-4.7F).initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, 2.7F, 2.5F, 5.5F, 10.0F).accuracy(0.0F, 0.0F, 0.0F, 0.0F, 2.3F, 2.2F, 4.5F, 7.1F).mobility(-2.1F));
    public static final DeferredItem<Item> SR_25 = createNormal(ITEMS, "sr_25", SR_25_P.magazineType(REGULAR_SNIPER_MAGAZINE), AVAGunRecipes.SR_25);
    public static final DeferredItem<Item> SR_25_KNUT = createSkinned(ITEMS, "knut", SR_25, AVASharedRecipes.KNUT, (properties, recipe) -> new AVAItemGun(properties.getStats((stats) -> stats.put(GunStatTypes.SCOPE_TYPE, ScopeGunStat.scope(AVAItemGun.ScopeTypes.KNUTS_M110))), recipe));


    private static final AVAItemGun.Properties FR_F2_P = new AVAItemGun.Properties(Classification.SNIPER, GunStatBuilder.of()
            .damage(122.0F).damageFloating(17.5F).penetration(97.0F).range(94.8F).fireRate(1).capacity(5).mobility(98.4F - 6.0F)
            .reloadTime(43).drawSpeed(20).aimTime(3)
            .scopeType(AVAItemGun.ScopeTypes.FR_F2).noCrosshair().manual().trail()
            .initialAccuracy(24.7F, 42.4F, 19.4F, 15.9F, 91.4F, 93.6F, 21.2F, 11.8F)
            .accuracy(24.7F, 42.4F, 19.4F, 15.9F, 91.4F, 93.6F, 21.2F, 11.8F)
            .stability(69.9F, 76.0F, 59.9F, 59.9F, 69.9F, 76.0F, 59.9F, 59.9F))
            .sounds(AVASoundTracks.FR_F2_FIRE, AVASoundTracks.FR_F2_RELOAD, AVASoundTracks.FR_F2_DRAW)
            .stat(GunAttachmentTypes.QUICK_SCOPE, GunStatBuilder.of().mobility(-1.0F).aimTime(-1).scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT))
            .stat(GunAttachmentTypes.SHARP_SHOOTER_BARREL, GunStatBuilder.of().initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, 2.8F, 2.2F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(7.1F))
            .stat(GunAttachmentTypes.EXTENDED_MAGAZINE, GunStatBuilder.of().capacity(5).stability(0.0F, 0.0F, 0.0F, 0.0F, -0.9F, -1.1F, -0.8F, -0.8F));
    public static final DeferredItem<Item> FR_F2 = create(ITEMS, "fr_f2", FR_F2_P.magazineType(SMALL_SNIPER_MAGAZINE), AVAGunRecipes.FR_F2, AVASingle::new);
    public static final DeferredItem<Item> FR_F2_SUMIRE = createSkinned(ITEMS, "sumire", FR_F2, AVASharedRecipes.SUMIRE, AVASingle::new);
    public static final DeferredItem<Item> FR_F2_CHRISTMAS = createSkinned(ITEMS, "christmas", FR_F2, AVASharedRecipes.CHRISTMAS, AVASingle::new);


    private static final AVAItemGun.Properties BARRETT_P = new AVAItemGun.Properties(Classification.SNIPER, GunStatBuilder.of()
            .damage(200.0F).damageFloating(6.0F).penetration(75.0F).range(99.7F).fireRate(1.4F).capacity(10).mobility(99.9F - 6.0F)
            .reloadTime(83).drawSpeed(33).aimTime(7).recoilCompensation(0.25F).fireAnimationTime(10)
            .scopeType(AVAItemGun.ScopeTypes.PGM).noCrosshair().trail()
            .initialAccuracy(24.7F, 42.4F, 19.4F, 15.9F, 93.0F, 95.0F, 90.0F, 13.1F)
            .accuracy(24.7F, 42.4F, 19.4F, 15.9F, 32.4F, 90.9F, 23.8F, 13.1F)
            .stability(40.5F, 76.0F, 54.9F, 54.9F, 40.5F, 57.1F, 23.3F, 11.6F))
            .sounds(AVASoundTracks.BARRETT_FIRE, AVASoundTracks.BARRETT_RELOAD, AVASoundTracks.BARRETT_DRAW);
    public static final DeferredItem<Item> BARRETT = createNormal(ITEMS, "barrett", BARRETT_P.magazineType(HEAVY_AP_MAGAZINE), AVAGunRecipes.BARRETT);


    private static final AVAItemGun.Properties DSR1_P = new AVAItemGun.Properties(Classification.SNIPER, GunStatBuilder.of()
            .damage(127.6F).damageFloating(2.5F).penetration(65.0F).range(94.8F).fireRate(0.9F).capacity(5).mobility(99.3F - 6.0F)
            .reloadTime(80).drawSpeed(13).aimTime(2).fireAnimationTime(25)
            .scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT).noCrosshair().manual().trail()
            .initialAccuracy(24.7F, 42.4F, 19.4F, 15.9F, 92.2F, 95.8F, 80.2F, 40.7F)
            .accuracy(24.7F, 42.4F, 19.4F, 15.9F, 92.2F, 95.8F, 80.2F, 40.7F)
            .stability(63.6F, 67.7F, 57.3F, 57.3F, 64.0F, 68.1F, 57.3F, 57.3F))
            .sounds(AVASoundTracks.DSR1_FIRE, AVASoundTracks.DSR1_RELOAD, AVASoundTracks.DSR1_DRAW)
            .stat(GunAttachmentTypes.SHARP_SHOOTER_SCOPE, GunStatBuilder.of().aimTime(1))
            .stat(GunAttachmentTypes.LONG_RANGE_BARREL, GunStatBuilder.of().range(20.9F).initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, -8.6F, -6.7F, -8.4F, 0.0F).stability(0.0F, 0.0F, 0.0F, 0.0F, -8.1F, -8.1F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.REINFORCED_BARREL, GunStatBuilder.of().damage(10.0F).initialAccuracy(3.0F, 2.1F, 2.2F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.WEIGHTED_GRIP, GunStatBuilder.of().stability(0.0F, 0.0F, 0.0F, 0.0F, 11.9F, 11.9F, 7.0F, 0.0F).mobility(-0.9F))
            .stat(GunAttachmentTypes.SHOCK_ABSORBER, GunStatBuilder.of().spreadRecovery(0.1F));
    public static final DeferredItem<Item> DSR1 = create(ITEMS, "dsr1", DSR1_P.magazineType(SMALL_SNIPER_MAGAZINE), AVAGunRecipes.DSR1, AVASingle::new);


    private static final AVAItemGun.Properties AWM_P = new AVAItemGun.Properties(Classification.SNIPER, GunStatBuilder.of()
            .damage(122.0F).damageFloating(4.0F).penetration(65.0F).range(97.4F).fireRate(1.0F).capacity(5).mobility(98.4F - 6.0F)
            .reloadTime(40).drawSpeed(17).aimTime(2).fireAnimationTime(20)
            .scopeType(AVAItemGun.ScopeTypes.MOSIN_NAGANT).noCrosshair().manual().trail()
            .initialAccuracy(24.7F, 42.4F, 19.4F, 15.9F, 94.5F, 97.8F, 21.5F, 11.9F)
            .accuracy(24.7F, 42.4F, 19.4F, 15.9F, 94.5F, 97.8F, 21.5F, 11.9F)
            .stability(63.3F, 67.5F, 54.5F, 54.5F, 63.7F, 67.9F, 55.0F, 55.0F))
            .sounds(AVASoundTracks.AWM_FIRE, AVASoundTracks.AWM_RELOAD, AVASoundTracks.AWM_DRAW)
            .stat(GunAttachmentTypes.SHARP_SHOOTER_SCOPE, GunStatBuilder.of().aimTime(1).scopeType(AVAItemGun.ScopeTypes.FR_F2))
            .stat(GunAttachmentTypes.HEAVY_BARREL, GunStatBuilder.of().damage(10.0F).stability(-7.9F, -8.0F, -8.0F, -8.0F))
            .stat(GunAttachmentTypes.SPETSNAZ_BARREL, GunStatBuilder.of().range(-12.2F).mobility(1.3F))
            .stat(GunAttachmentTypes.PRECISION_TRIGGER, GunStatBuilder.of().initialAccuracy(0.0F, 0.0F, 0.0F, 0.0F, 2.0F, 2.2F, 0.0F, 0.0F))
            .stat(GunAttachmentTypes.WEIGHTED_GRIP, GunStatBuilder.of().stability(6.1F, 6.0F, 0.0F, 0.0F).mobility(-0.8F))
            .stat(GunAttachmentTypes.RECOIL_CONTROL_STOCK, GunStatBuilder.of().stability(6.0F, 5.9F, 0.0F, 0.0F).mobility(-2.0F));
    public static final DeferredItem<Item> AWM = create(ITEMS, "awm", AWM_P.magazineType(SMALL_SNIPER_MAGAZINE), AVAGunRecipes.AWM, AVASingle::new);


    public static ArrayList<Item> ITEM_SNIPERS = new ArrayList<>();
}
