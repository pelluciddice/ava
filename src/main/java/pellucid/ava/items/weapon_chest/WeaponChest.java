package pellucid.ava.items.weapon_chest;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.Level;
import pellucid.ava.items.init.AVAItemGroups;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Supplier;

public class WeaponChest extends Item implements MenuProvider
{
    private final Supplier<List<Item>> contents;
    public WeaponChest(Supplier<List<Item>> contents)
    {
        super(new Properties().stacksTo(1).rarity(Rarity.EPIC));
        this.contents = contents;
        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, () -> this);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand hand)
    {
        ItemStack stack = player.getMainHandItem();
        if (player instanceof ServerPlayer)
        {
            player.openMenu(this, (buffer) -> buffer.writeUtf(BuiltInRegistries.ITEM.getKey(stack.getItem()).toString()));
            return InteractionResultHolder.success(stack);
        }
        return InteractionResultHolder.pass(stack);
    }

    public List<Item> getContents()
    {
        return contents.get();
    }

    @Override
    public Component getDisplayName()
    {
        return Component.empty();
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_createMenu_1_, Inventory p_createMenu_2_, Player p_createMenu_3_)
    {
        return new WeaponChestContainer(p_createMenu_1_, p_createMenu_3_.getMainHandItem().getItem());
    }
}
