package pellucid.ava.items.weapon_chest;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.Slot;
import pellucid.ava.packets.DataToServerMessage;

public class WeaponChestGUI extends AbstractContainerScreen<WeaponChestContainer>
{
    private static final ResourceLocation WEAPON_CHEST_GUI = new ResourceLocation("ava:textures/gui/weapon_chest.png");

    public WeaponChestGUI(WeaponChestContainer screenContainer, Inventory inv, Component title)
    {
        super(screenContainer, inv, Component.empty());
        this.imageWidth = 175;
        this.imageHeight = 193;
    }

    @Override
    public void render(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }

    @Override
    protected void renderBg(GuiGraphics matrixStack, float partialTicks, int x, int y)
    {
        if (minecraft == null)
            return;
        int i = (width - imageWidth) / 2;
        int j = (height - imageHeight) / 2;
        matrixStack.blit(WEAPON_CHEST_GUI, i, j, 0, 0, imageWidth, this.imageHeight);
    }

    @Override
    protected void renderLabels(GuiGraphics matrixStack, int x, int y)
    {
        matrixStack.drawString(font, this.title, this.titleLabelX, this.titleLabelY, 4210752);
    }

    @Override
    protected void slotClicked(Slot slotIn, int slotId, int mouseButton, ClickType type)
    {
        // If the user clickes outside the gui the slot will be null
        if (slotIn != null)
        {
            DataToServerMessage.pickWeaponChest(slotIn.getSlotIndex());
            onClose();
        }
    }
}
