package pellucid.ava.items.weapon_chest;

import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.neoforged.neoforge.items.ItemStackHandler;
import net.neoforged.neoforge.items.SlotItemHandler;
import pellucid.ava.blocks.AVATEContainers;

import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicInteger;

public class WeaponChestContainer extends AbstractContainerMenu
{
    private final Item chest;

    public WeaponChestContainer(int id, Inventory playerInventory)
    {
        this(id, (Item) null);
    }

    public WeaponChestContainer(int id, @Nullable Item chest)
    {
        super(AVATEContainers.WEAPON_CHEST_CONTAINER.get(), id);
        this.chest = chest == null ? Items.AIR : chest;
        ItemStackHandler itemHandler = new ItemStackHandler(81);
        int x = 8;
        int y = 18;
        AtomicInteger index = new AtomicInteger();
        for (int i=0;i<9;i++)
            for (int j=0;j<9;j++)
                addSlot(new SlotItemHandler(itemHandler, index.getAndIncrement(), x + 18 * j, y + 18 * i));
        index.set(0);
        if (this.chest instanceof WeaponChest)
            ((WeaponChest) this.chest).getContents().forEach((item) -> itemHandler.setStackInSlot(index.getAndIncrement(), new ItemStack(item)));
    }

    @Override
    public boolean canDragTo(Slot slotIn)
    {
        return false;
    }

    @Override
    public ItemStack quickMoveStack(Player p_38941_, int p_38942_)
    {
        return ItemStack.EMPTY;
    }

    @Override
    public boolean canTakeItemForPickAll(ItemStack stack, Slot slotIn)
    {
        return false;
    }

    @Override
    public boolean stillValid(Player playerIn)
    {
        return chest != Items.AIR && playerIn.getMainHandItem().getItem() == chest;
    }
}
