package pellucid.ava.items.miscs;


import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;

import javax.annotation.Nullable;
import java.util.List;

public class ParachuteItem extends Item implements IHasRecipe
{
    public ParachuteItem()
    {
        super(new Item.Properties().stacksTo(1).durability(10));
        AVAItemGroups.putItem(AVAItemGroups.MAIN, () -> this);
    }

    @Override
    public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        AVAWeaponUtil.removeRepairCost(stack);
    }

    @Override
    public boolean isValidRepairItem(ItemStack toRepair, ItemStack repair)
    {
        return repair.is(Tags.Items.STRINGS);
    }

    @Override
    public boolean isRepairable(ItemStack stack)
    {
        return true;
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand handIn)
    {
        ItemStack stack = player.getMainHandItem();
        if (!world.isClientSide() && stack.getDamageValue() < stack.getMaxDamage())
        {
            if (activateParachute(player))
                stack.setDamageValue(stack.getDamageValue() + 1);
        }
        return super.use(world, player, handIn);
    }

    public static boolean activateParachute(Player player)
    {
        if (player != null)
        {
            PlayerAction cap = PlayerAction.getCap(player);
            if (!cap.getUsingParachute())
            {
                if (!player.onGround())
                {
                    cap.setIsUsingParachute(player, true);
                    AVAWeaponUtil.playAttenuableSoundToClientMoving(AVASounds.PARACHUTE_OPEN.get(), player, 1.5F);
                    player.addEffect(new MobEffectInstance(MobEffects.LEVITATION, 2, (int) (player.fallDistance / 6.0F), false, false));
                    return true;
                }
            }
            else if (!AVAServerConfig.isCompetitiveModeActivated())
            {
                cap.setIsUsingParachute(player, false);
                return true;
            }
        }
        return false;
    }


    @Override
    public void appendHoverText(ItemStack stack, @Nullable TooltipContext worldIn, List<Component> tooltip, TooltipFlag flagIn)
    {
        tooltip.add(Component.translatable("ava.item.tips.parachute"));
    }

    @Override
    public Recipe getRecipe()
    {
        return AVAGunRecipes.PARACHUTE;
    }
}
