package pellucid.ava.items.miscs;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import pellucid.ava.gun.stats.GunStat;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.util.INBTSerializable;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class GunStatsMap extends HashMap<GunStatTypes, GunStat<?>> implements INBTSerializable<CompoundTag>
{
    public GunStatsMap(int initialCapacity, float loadFactor)
    {
        super(initialCapacity, loadFactor);
    }

    public GunStatsMap(int initialCapacity)
    {
        super(initialCapacity);
    }

    public GunStatsMap()
    {
    }

    public GunStatsMap(Map<? extends GunStatTypes, ? extends GunStat<?>> m)
    {
        m.forEach((k, v) -> {
            put(k, v.copy());
        });
    }

    @Override
    public GunStat<?> put(GunStatTypes key, GunStat<?> value)
    {
        value.type(key);
        return super.put(key, value);
    }

    @Override
    public GunStat<?> putIfAbsent(GunStatTypes key, GunStat<?> value)
    {
        value.type(key);
        return super.putIfAbsent(key, value);
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        ListTag list = new ListTag();
        keySet().stream().sorted(Comparator.comparingInt(Enum::ordinal)).forEach((k) -> {
            if (k.isMutable())
            {
                CompoundTag compound2 = new CompoundTag();
                compound2.putString("name", k.name());
                compound2.put("stat", get(k).serializeNBT());
                list.add(compound2);
            }
        });
        compound.put("list", list);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        ListTag list = nbt.getList("list", Tag.TAG_COMPOUND);
        for (Tag tag : list)
        {
            CompoundTag compound = (CompoundTag) tag;
            get(GunStatTypes.valueOf(compound.getString("name"))).deserializeNBT(compound.getCompound("stat"));
        }
    }
}
