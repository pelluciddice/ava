package pellucid.ava.items.miscs;


import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;

import java.util.ArrayList;
import java.util.List;

public class Ammo extends Item implements IHasRecipe, ICustomTooltip
{
    protected final Recipe recipe;
    public final boolean isMagazine;
    public static final List<Ammo> AMMO = new ArrayList<>();
    private final List<Item> guns = new ArrayList<>();
    private boolean deprecated = false;
    private boolean special = false;

    public Ammo(Recipe recipe)
    {
        this(AVAItemGroups.MAIN, recipe);
    }

    public Ammo(DeferredHolder<CreativeModeTab, CreativeModeTab> group, Recipe recipe)
    {
        this(new Item.Properties(), recipe, false);
        AVAItemGroups.putItem(group, () -> this);
    }

    public Ammo(Properties properties, Recipe recipe, boolean isMagazine)
    {
        super(properties);
        this.recipe = recipe;
        this.isMagazine = isMagazine;
        AMMO.add(this);
    }

    public Ammo setSpecial()
    {
        special = true;
        return this;
    }

    public boolean isSpecial()
    {
        return special;
    }

    public Ammo deprecated()
    {
        this.deprecated = true;
        return this;
    }

    public void add(Item gun)
    {
        guns.add(gun);
    }

    public ItemStack addToInventory(Player player, int times, boolean simulate)
    {
        ItemStack stack = new ItemStack(this, (this.isMagazine ? 1 : 5) * times);
        if (!simulate)
            player.getInventory().add(stack);
        return stack;
    }

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    public boolean isMagazine()
    {
        return this.isMagazine;
    }

    @Override
    public int getMaxStackSize(ItemStack stack)
    {
        return (!this.isMagazine || this.getDamage(stack) == 0) ? 64 : 1;
    }

    @Override
    public boolean addToolTips(ItemTooltipEvent event)
    {
        ICustomTooltip.super.addToolTips(event);
        if (deprecated)
            event.getToolTip().add(Component.translatable("ava.item.magazine_deprecation").setStyle(Style.EMPTY.applyFormat(ChatFormatting.RED)));
        return true;
    }

    @Override
    public void addAdditionalToolTips(ItemTooltipEvent event)
    {
        for (Item gun : guns)
            event.getToolTip().add(gun.getDescription());
    }
}
