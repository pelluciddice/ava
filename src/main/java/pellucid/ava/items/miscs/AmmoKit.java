package pellucid.ava.items.miscs;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;

public class AmmoKit extends Item
{
    public final int capacity;

    public AmmoKit(int capacity)
    {
        super(new Item.Properties().stacksTo(1).rarity(capacity > 200 ? Rarity.UNCOMMON : capacity > 0 ? Rarity.COMMON : Rarity.EPIC).durability(Math.max(capacity, 0)));
        this.capacity = capacity;
        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, () -> this);
    }

    @Override
    public boolean isValidRepairItem(ItemStack stack, ItemStack stack2)
    {
        return stack2.is(Tags.Items.GUNPOWDERS);
    }

    @Override
    public boolean isFoil(ItemStack stack)
    {
        return !requireRefill();
    }

    public boolean requireRefill()
    {
        return capacity != -1;
    }

    @Override
    public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (worldIn.isClientSide || !requireRefill())
            return;
        AVAWeaponUtil.removeRepairCost(stack);
        if (stack.getAllEnchantments().keySet().contains(Enchantments.MENDING) && stack.getDamageValue() > 0 && worldIn.getGameTime() % 20L == 0)
            stack.setDamageValue(stack.getDamageValue() - 1);
    }

    @Override
    public void appendHoverText(ItemStack stack, TooltipContext worldIn, List<Component> tooltip, TooltipFlag flagIn)
    {
        tooltip.add(Component.translatable("ava.item.tips.ammo_kit"));
        int max = stack.getMaxDamage();
        if (requireRefill())
        {
            tooltip.add(Component.translatable("ava.item.tips.ammo", max - stack.getDamageValue(), max));
            tooltip.add(Component.translatable("ava.item.tips.ammo_kit_2"));
            tooltip.add(Component.translatable("ava.item.tips.ammo_kit_3"));
        }
    }
}
