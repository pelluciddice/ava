package pellucid.ava.items.miscs;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class MagicStickItem extends Item
{
    public static BlockPos POS = null;
    public static BlockPos POS_2 = null;
    public MagicStickItem()
    {
        super(new Properties().stacksTo(1));
    }

    @Override
    public boolean canAttackBlock(BlockState state, Level world, BlockPos pos, Player player)
    {
        POS = pos;
        player.displayClientMessage(Component.literal("Set A: ").append(pos.toShortString()), true);
        return false;
    }

    @Override
    public InteractionResult useOn(UseOnContext context)
    {
        POS_2 = context.getClickedPos();
        if (context.getPlayer() != null)
            context.getPlayer().displayClientMessage(Component.literal("Set B: ").append(POS_2.toShortString()), true);
        return InteractionResult.CONSUME;
    }

    @Override
    public boolean isFoil(ItemStack stack)
    {
        return true;
    }
}
