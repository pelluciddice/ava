package pellucid.ava.items.miscs;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.apache.commons.lang3.StringUtils;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TestItem extends Item
{
    public TestItem()
    {
        super(new Item.Properties());
    }


    public InteractionResult useOn(UseOnContext p_41427_)
    {
//        if (!p_41427_.getLevel().isClientSide() && p_41427_.getHand() == InteractionHand.MAIN_HAND)
//        {
//            System.out.println(M4A1Model.RELOAD_ANIMATION);
//            System.out.println(Animations.of(M4A1Model.RELOAD_ANIMATION));
//            System.out.println(AVAModelTypes.GUNS.getModel(Rifles.M4A1.get()).reload.getA());
//        }
//        if (true)
//            return super.useOn(p_41427_);
//        if (p_41427_.getLevel().isClientSide)
//        {
//            JsonObject json = new JsonObject();
//            RegularGunModelProperty properties = new RegularGunModelProperty((AVAItemGun) SubmachineGuns.MP5SD5.get());
//            properties.putSubModel(GunSubModels.MAGAZINE);
//            properties.putSubModel(GunSubModels.HANDLE);
//
//            properties.handsIdle.setA(Mp5sd5Model.LEFT_HAND_IDLE);
//
//            properties.run.setA(Mp5sd5Model.RUN_ANIMATION);
//            properties.reload.setA(Mp5sd5Model.RELOAD_ANIMATION);
//            properties.reload.getB().setA(Mp5sd5Model.LEFT_HAND_RELOAD_ANIMATION_FP);
//            properties.draw.setA(Mp5sd5Model.DRAW_ANIMATION);
//            properties.draw.getB().setA(Mp5sd5Model.LEFT_HAND_RELOAD_ANIMATION_FP);
//
//            properties.display.put(ItemDisplayContext.THIRD_PERSON_LEFT_HAND, new Perspective(70, 0, 0, -1.75F, 2.25F, 3.75F, 0.95F, 0.95F, 0.95F));
//            properties.display.put(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND, new Perspective(70, 0, 0, -1.75F, 2.25F, 3.75F, 0.95F, 0.95F, 0.95F));
//            properties.display.put(ItemDisplayContext.FIRST_PERSON_LEFT_HAND, new Perspective(
//                    new Vector3f(-3, 3, 0),
//                    new Vector3f(-6F, 4.5F, 3.75F),
//                    new Vector3f(0.95F, 0.85F, 0.45F)));
//            properties.display.put(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new Perspective(
//                    new Vector3f(-3, 3, 0),
//                    new Vector3f(-6F, 4.5F, 3.75F),
//                    new Vector3f(0.95F, 0.85F, 0.45F)));
//            properties.display.put(ItemDisplayContext.GROUND, new Perspective(0, 0, -90, 0, -3, -0.5F, 0.55F, 0.55F, 0.55F));
//            properties.display.put(ItemDisplayContext.GUI, new Perspective(0, -90, 0, 0.75F, 0, -1.25F, 0.45F, 0.45F, 0.45F));
//            properties.display.put(ItemDisplayContext.FIXED, new Perspective(0, -90, 0, 1.25F, 0.75F, 0F, 1F, 1F, 1F));
//
//            properties.writeToJson(json);
//
//            GsonBuilder builder = new GsonBuilder().setLenient().setPrettyPrinting();

//            System.out.println(customPrettyPrint(json, 4));

//        }
//        if (!p_41427_.getLevel().isClientSide)
//        {
//            CompoundTag c = new CompoundTag();
//            CompoundTag c2 = new CompoundTag();
//            c.putInt("a", 2);
//            c2.putInt("a", 1);
//            System.out.println("------------------------------");
//        }
//        if (!p_41427_.getLevel().isClientSide && p_41427_.getLevel().getBlockEntity(p_41427_.getClickedPos()) instanceof BannerBlockEntity banner)
//        {
//            for (Pair<Holder<BannerPattern>, DyeColor> pattern : banner.getPatterns())
//            {
//                System.out.println(pattern.getFirst().get().getHashname() + " - - - " + pattern.getSecond().getName());
//            }
//        }
        return super.useOn(p_41427_);
    }



    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand handIn)
    {
        if (true)
            return super.use(world, player, handIn);

        if (true)
            return super.use(world, player, handIn);
        if (world.isClientSide() && handIn == InteractionHand.MAIN_HAND)
            for (int i = 0; i < player.getInventory().getContainerSize(); i++)
            {
                if (!player.getInventory().getItem(i).isEmpty())
                {
//                    System.out.println(player.getInventory().getItem(i));
//                    System.out.println(player.getInventory().getItem(i).getTag());
                }
            }

        if (!world.isClientSide())
        {
//            WeatherManager.Types.SNOW.fill(world, player.blockPosition());
//            List<AVAChunkGenerator.Layer> list = new ArrayList<>();
//            list.add(new AVAChunkGenerator.Layer(Blocks.STONE.defaultBlockState(), 1));
//            list.add(new AVAChunkGenerator.Layer(Blocks.WATER.defaultBlockState(), 2));
//            list.add(new AVAChunkGenerator.Layer(Blocks.LAVA.defaultBlockState(), 3));
//            list.add(new AVAChunkGenerator.Layer(Blocks.STONE.defaultBlockState(), 4));
//            System.out.println(AVAChunkGenerator.CODEC.encode(new AVAChunkGenerator(BuiltInRegistries.STRUCTURE_SETS, BuiltInRegistries.BIOME, list), JsonOps.INSTANCE, new JsonObject()));

            Vec3 start = player.getEyePosition();
            Vec3 end = start.add(player.getLookAngle().scale(2));

            System.out.println("-----------------------------------");
            long prev = System.nanoTime();

            AVAWeaponUtil.rayTraceAndPenetrateBlocks4(world, AVAWeaponUtil.getAllVectors2(start, end, 30), 100, (result, state) -> 1.0F);
            System.out.printf("Old ray tracing took: %d ns%n", System.nanoTime() - prev);
            prev = System.nanoTime();


            AVAWeaponUtil.rayTraceAndPenetrateBlocks4Efficiency(world, AVAWeaponUtil.getAllVectors2Efficiency(start, end, world.getMinBuildHeight(), world.getMaxBuildHeight(), 30), 100, (result, state) -> 1.0F);
            System.out.printf("New ray tracing took: %d ns%n", System.nanoTime() - prev);
            prev = System.nanoTime();

            if (true)
                return super.use(world, player, handIn);
            for (GunAttachmentTypes value : GunAttachmentTypes.values())
            {
                System.out.println("add(\"" + ((TranslatableContents) value.getTranslated().getContents()).getKey() + "\", \"" + getName(value.name()) + "\");");
            }
//            Animations list = Animations.of()
//                .append(Animation.of(0, new Perspective(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F)))
//                .append(Animation.of(5, new Perspective(1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F)))
//            }};
//            AVABakedModel.getPerspectiveInBetween(list, 0, 1.0F, false);
//            for (Animation animation : list)
//                System.out.println(animation);
//            System.out.println(AVABakedModel.getPerspectiveInBetween(list, 1, 0.25F, false));
//            System.out.println(AVABakedModel.getPerspectiveInBetween(list, 1, 0.5F, false));
//            System.out.println(AVABakedModel.getPerspectiveInBetween(list, 1, 0.75F, false));
//            System.out.println(AVABakedModel.getPerspectiveInBetween(list, 2, 0.25F, false));
//            System.out.println(AVABakedModel.getPerspectiveInBetween(list, 2, 0.5F, false));
//            System.out.println(AVABakedModel.getPerspectiveInBetween(list, 2, 0.75F, false));
        }
        if (!world.isClientSide())
        {

        }
        return super.use(world, player, handIn);
    }

    private String getName(String name)
    {
        name = name.replaceAll("_", " ");
        StringBuilder builder = new StringBuilder(name.length());
        String[] s = name.split(" ");
        for (String n : s)
        {
            n = StringUtils.capitalize(n.toLowerCase(Locale.ROOT));
            if (builder.length() != 0)
                builder.append(" ");
            builder.append(n);
        }
        return builder.toString();
    }

    static BlockHitResult rayTraceBlocks(Level world, ClipContext context) {
        return doRayTrace(context, (p_217297_1_, p_217297_2_) -> {
            BlockState blockstate = world.getBlockState(p_217297_2_);
            FluidState fluidstate = world.getFluidState(p_217297_2_);
            Vec3 vector3d = p_217297_1_.getFrom();
            Vec3 vector3d1 = p_217297_1_.getTo();
            VoxelShape voxelshape = p_217297_1_.getBlockShape(blockstate, world, p_217297_2_);
            BlockHitResult blockraytraceresult = world.clipWithInteractionOverride(vector3d, vector3d1, p_217297_2_, voxelshape, blockstate);
            VoxelShape voxelshape1 = p_217297_1_.getFluidShape(fluidstate, world, p_217297_2_);
            BlockHitResult blockraytraceresult1 = voxelshape1.clip(vector3d, vector3d1, p_217297_2_);
            double d0 = blockraytraceresult == null ? Double.MAX_VALUE : p_217297_1_.getFrom().distanceToSqr(blockraytraceresult.getLocation());
            double d1 = blockraytraceresult1 == null ? Double.MAX_VALUE : p_217297_1_.getFrom().distanceToSqr(blockraytraceresult1.getLocation());
            return d0 <= d1 ? blockraytraceresult : blockraytraceresult1;
        }, (p_217302_0_) -> {
            Vec3 vector3d = p_217302_0_.getFrom().subtract(p_217302_0_.getTo());
            return BlockHitResult.miss(p_217302_0_.getTo(), Direction.getNearest(vector3d.x, vector3d.y, vector3d.z), BlockPos.containing(p_217302_0_.getTo()));
        });
    }
    
    static <T> T doRayTrace(ClipContext context, BiFunction<ClipContext, BlockPos, T> rayTracer, Function<ClipContext, T> missFactory) 
    {
        Vec3 vector3d = context.getFrom();
        Vec3 vector3d1 = context.getTo();
        if (vector3d.equals(vector3d1)) {
            return missFactory.apply(context);
        } else {
            double d0 = Mth.lerp(-1.0E-7D, vector3d1.x, vector3d.x);
            double d1 = Mth.lerp(-1.0E-7D, vector3d1.y, vector3d.y);
            double d2 = Mth.lerp(-1.0E-7D, vector3d1.z, vector3d.z);
            double d3 = Mth.lerp(-1.0E-7D, vector3d.x, vector3d1.x);
            double d4 = Mth.lerp(-1.0E-7D, vector3d.y, vector3d1.y);
            double d5 = Mth.lerp(-1.0E-7D, vector3d.z, vector3d1.z);
            System.out.println("----1----");
            System.out.println(d0);
            System.out.println(d1);
            System.out.println(d2);
            System.out.println(d3);
            System.out.println(d4);
            System.out.println(d5);
            int i = Mth.floor(d3);
            int j = Mth.floor(d4);
            int k = Mth.floor(d5);
            System.out.println("----2----");
            System.out.println(i);
            System.out.println(j);
            System.out.println(k);
            BlockPos.MutableBlockPos blockpos$mutable = new BlockPos.MutableBlockPos(i, j, k);
            T t = rayTracer.apply(context, blockpos$mutable);
            if (t != null) {
                return t;
            } else {
                double d6 = d0 - d3;
                double d7 = d1 - d4;
                double d8 = d2 - d5;
                System.out.println("----3----");
                System.out.println(d6);
                System.out.println(d7);
                System.out.println(d8);
                int l = Mth.sign(d6);
                int i1 = Mth.sign(d7);
                int j1 = Mth.sign(d8);
                System.out.println("----4----");
                System.out.println(l);
                System.out.println(i1);
                System.out.println(j1);
                double d9 = l == 0 ? Double.MAX_VALUE : (double)l / d6;
                double d10 = i1 == 0 ? Double.MAX_VALUE : (double)i1 / d7;
                double d11 = j1 == 0 ? Double.MAX_VALUE : (double)j1 / d8;
                double d12 = d9 * (l > 0 ? 1.0D - Mth.frac(d3) : Mth.frac(d3));
                double d13 = d10 * (i1 > 0 ? 1.0D - Mth.frac(d4) : Mth.frac(d4));
                double d14 = d11 * (j1 > 0 ? 1.0D - Mth.frac(d5) : Mth.frac(d5));
                System.out.println("----4----");
                System.out.println(d9);
                System.out.println(d10);
                System.out.println(d11);
                System.out.println(d12);
                System.out.println(d13);
                System.out.println(d14);
                while(d12 <= 1.0D || d13 <= 1.0D || d14 <= 1.0D) {
                    if (d12 < d13) {
                        if (d12 < d14) {
                            i += l;
                            d12 += d9;
                        } else {
                            k += j1;
                            d14 += d11;
                        }
                    } else if (d13 < d14) {
                        j += i1;
                        d13 += d10;
                    } else {
                        k += j1;
                        d14 += d11;
                    }

                    T t1 = rayTracer.apply(context, blockpos$mutable.set(i, j, k));
                    if (t1 != null) {
                        return t1;
                    }
                }

                return missFactory.apply(context);
            }
        }
    }
}
