package pellucid.ava.items.miscs;

import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;

public class CraftableAmmoKit extends AmmoKit implements IHasRecipe
{
    public CraftableAmmoKit(int capacity)
    {
        super(capacity);
    }

    @Override
    public Recipe getRecipe()
    {
        return capacity > 200 ? AVAGunRecipes.AMMO_KIT_I : AVAGunRecipes.AMMO_KIT;
    }
}
