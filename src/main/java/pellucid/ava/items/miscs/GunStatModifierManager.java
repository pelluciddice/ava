package pellucid.ava.items.miscs;

import com.google.common.util.concurrent.AtomicDouble;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.INBTSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GunStatModifierManager implements INBTSerializable<CompoundTag>
{
    private final AVAItemGun gun;
    private final ItemStack stack;
    private final Map<GunStatTypes, List<Modifier>> modifiers = new HashMap<>();

    private GunStatModifierManager(AVAItemGun gun, ItemStack stack)
    {
        this.gun = gun;
        this.stack = stack;
        deserializeNBT(stack.getOrDefault(AVADataComponents.TAG_ITEM_GUN_STAT_MODIFIERS_MANAGER, new CompoundTag()));
    }

    public static GunStatModifierManager ofUnsafe(ItemStack stack)
    {
        return of(stack).get();
    }

    public static Optional<GunStatModifierManager> of(ItemStack stack)
    {
        return Optional.ofNullable(stack.getItem() instanceof AVAItemGun ? new GunStatModifierManager((AVAItemGun) stack.getItem(), stack) : null);
    }

    public double getModifiedValue(GunStatTypes type)
    {
        if (modifiers.containsKey(type))
        {
            AtomicDouble value = new AtomicDouble(0.0D);
            modifiers.get(type).forEach((modifier) -> value.set(value.get() + modifier.getValue()));
            return value.get();
        }
        return 0.0D;
    }

    public void addModifier(GunStatTypes type, Source source, double amount)
    {
        modifiers.computeIfAbsent(type, (e) -> new ArrayList<>()).add(new Modifier(type, source, amount));
        save();
    }

    public void removeModifier(GunStatTypes type, Source source)
    {
        if (modifiers.containsKey(type))
        {
            modifiers.get(type).removeIf((modifier) -> modifier.getSource() == source);
            if (modifiers.get(type).isEmpty())
                modifiers.remove(type);
            save();
        }
    }

    public void removeModifier(Source source)
    {
        new HashSet<>(modifiers.keySet()).forEach((k) -> removeModifier(k, source));
    }

    public void save()
    {
        stack.set(AVADataComponents.TAG_ITEM_GUN_STAT_MODIFIERS_MANAGER, serializeNBT());
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        ListTag list = new ListTag();
        this.modifiers.values().forEach((modifiers) -> {
            for (Modifier modifier : modifiers)
                list.add(modifier.serializeNBT());
        });
        compound.put("modifiers", list);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        modifiers.clear();
        ListTag list = nbt.getList("modifiers", Tag.TAG_COMPOUND);
        for (Tag tag : list)
        {
            CompoundTag compound = (CompoundTag) tag;
            modifiers.computeIfAbsent(GunStatTypes.valueOf(compound.getString("type")), (e) -> new ArrayList<>()).add(new Modifier(compound));
        }
    }

    public enum Source
    {
        GUN_MASTERY_BOOSTS,
        ;
    }

    private static class Modifier
    {
        private final GunStatTypes type;
        private final Source source;
        private final double value;

        private Modifier(CompoundTag compound)
        {
            this(GunStatTypes.valueOf(compound.getString("type")), Source.valueOf(compound.getString("source")), compound.getDouble("value"));
        }

        private Modifier(GunStatTypes type, Source source, double value)
        {
            this.type = type;
            this.source = source;
            this.value = value;
        }

        public GunStatTypes getType()
        {
            return type;
        }

        public Source getSource()
        {
            return source;
        }

        public double getValue()
        {
            return value;
        }

        public CompoundTag serializeNBT()
        {
            CompoundTag compound = new CompoundTag();
            compound.putString("type", type.name());
            compound.putString("source", source.name());
            compound.putDouble("value", value);
            return compound;
        }
    }
}
