package pellucid.ava.items.miscs;

import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.scanhits.MeleeAttackRaytraceEntity;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.player.status.MeleeStatusManager;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.sounds.SoundTrack;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.UUID;

public class AVAMeleeItem extends AVAAnimatedItem<AVAMeleeItem> implements IHasRecipe, IClassification
{
    protected static final UUID MOVEMENT_SPEED_MODIFIER = UUID.fromString("d1248141-2e53-432d-8a84-1b31d279163e");
    private final Recipe recipe;
    private final float damageL;
    private final float damageR;
    private final int drawSpeed;
    private final int speedL;
    private final int speedR;
    private final float rangeL;
    private final float rangeR;
    protected final SoundTrack drawSound;
    protected final SoundTrack attackLightSound;
    protected final SoundTrack attackHeavySound;

    private final float movementSpeed;

    public AVAMeleeItem(Recipe recipe, float damageL, float damageR, int drawSpeed, int speedL, int speedR, float rangeL, float rangeR, SoundTrack drawSound, SoundTrack attackLightSound, SoundTrack attackHeavySound, float movementSpeedBonus)
    {
        super(new Properties().stacksTo(1), MeleeStatusManager.INSTANCE);
        this.recipe = recipe;
        this.damageL = damageL / 5F;
        this.damageR = damageR / 5F;
        this.drawSpeed = drawSpeed;

        this.speedL = speedL;
        this.speedR = speedR;
        this.rangeL = rangeL * 1.7F;
        this.rangeR = rangeR * 1.7F;
        this.drawSound = drawSound;
        this.attackLightSound = attackLightSound;
        this.attackHeavySound = attackHeavySound;
        AVAWeaponUtil.Classification.MELEE_WEAPON.addToList(this);
        movementSpeed = movementSpeedBonus;
        AVAItemGroups.putItem(AVAItemGroups.MAIN, () -> this);
    }

    public SoundTrack getDrawSound()
    {
        return drawSound;
    }

    public SoundTrack getAttackLightSound()
    {
        return attackLightSound;
    }

    public SoundTrack getAttackHeavySound()
    {
        return attackHeavySound;
    }

    public int getSpeedL()
    {
        return speedL;
    }

    public int getSpeedR()
    {
        return speedR;
    }

    public int getDrawSpeed()
    {
        return drawSpeed;
    }

    public void startAttackLight(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_ATTACK_LIGHT, getSpeedL());
    }

    public void startAttackHeavy(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_ATTACK_HEAVY, getSpeedR());
    }

    public void startDraw(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_DRAW, getDrawSpeed());
    }

    public void meleeAttack(Player player, boolean left)
    {
        Level world = player.level();
        world.addFreshEntity(new MeleeAttackRaytraceEntity(world, player, left ? rangeL : rangeR, left ? damageL : damageR, this));
    }

    @Override
    public ItemAttributeModifiers getAttributeModifiers(ItemStack stack)
    {
        return ItemAttributeModifiers.builder()
                .add(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADD_VALUE), EquipmentSlotGroup.MAINHAND)
                .add(Attributes.MOVEMENT_SPEED, new AttributeModifier(MOVEMENT_SPEED_MODIFIER, "Movement Speed Modifier", AVAServerConfig.FIELD_KNIFE_MOVEMENT_SPEED_BONUS.get(), AttributeModifier.Operation.ADD_VALUE), EquipmentSlotGroup.MAINHAND)
                .build();
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return AVAWeaponUtil.Classification.MELEE_WEAPON;
    }

    @Override
    public Recipe getRecipe()
    {
        return recipe;
    }
}
