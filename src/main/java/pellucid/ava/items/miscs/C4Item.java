package pellucid.ava.items.miscs;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.player.status.C4StatusManager;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;

public class C4Item extends AVAAnimatedItem<C4Item> implements IHasRecipe, ICustomTooltip
{
    public C4Item()
    {
        super(new Properties().stacksTo(1), C4StatusManager.INSTANCE);
        AVAItemGroups.putItem(AVAItemGroups.MAIN, () -> this);
    }

    @Override
    public int getEntityLifespan(ItemStack itemStack, Level world)
    {
        return super.getEntityLifespan(itemStack, world) * (AVAServerConfig.isCompetitiveModeActivated() ? 4 : 1);
    }

    @Override
    public Recipe getRecipe()
    {
        return AVAGunRecipes.C4;
    }
}
