package pellucid.ava.items.miscs;

import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.items.functionalities.IScopedItem;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;

public class BinocularItem extends AVAAnimatedItem<BinocularItem> implements IHasRecipe, IClassification, ICustomTooltip, IScopedItem
{
    public BinocularItem()
    {
        super(new Item.Properties().stacksTo(1), BinocularStatusManager.INSTANCE);
        AVAWeaponUtil.Classification.SPECIAL_WEAPON.addToList(this);
        AVAItemGroups.putItem(AVAItemGroups.MAIN, () -> this);
    }

    @Override
    public ItemAttributeModifiers getAttributeModifiers(ItemStack stack)
    {
        return ItemAttributeModifiers.builder()
                .add(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADD_VALUE), EquipmentSlotGroup.MAINHAND).build();
    }

    @Override
    public boolean isBarVisible(ItemStack stack)
    {
        return true;
    }

    @Override
    public int getBarWidth(ItemStack stack)
    {
        return (int) Mth.lerp(1.0F - BinocularStatusManager.INSTANCE.fireInterval / (float) AVAConstants.BINOCULAR_CD, 0, 13);
    }

    public void fire(Level world, Player player, ItemStack stack)
    {
        if (firable(player, stack))
        {
            player.getCommandSenderWorld().playSound(null, player.getX(), player.getY(), player.getZ(), AVASounds.UAV_CAPTURES.get(), SoundSource.PLAYERS, 0.75F, 1.1F);
            List<LivingEntity> list = AVAWeaponUtil.getEntitiesInSight(LivingEntity.class, player, 20, 20, 100, (entity) -> AVAWeaponUtil.isValidEntity(entity) && (!AVAWeaponUtil.isSameSide(player, entity)) && !(entity instanceof LeopardEntity), false, false, true);
            for (LivingEntity entity : list)
            {
                AVAWorldData.getInstance(world).uavS.put(entity.getUUID(), 140);
                if (!AVAServerConfig.isCompetitiveModeActivated())
                    world.players().forEach((player2) -> {
                        if (AVAWeaponUtil.isSameSide(player, player2))
                            player2.sendSystemMessage(Component.translatable("ava.chat.binocular_captured", player.getDisplayName(), entity.getDisplayName(), entity.blockPosition().toString()));
                    });
            }
            if (!list.isEmpty() && world instanceof ServerLevel server)
                DataToClientMessage.uav(server);
        }
    }

    public boolean firable(Player player, ItemStack stack)
    {
        return AVAWeaponUtil.Classification.SPECIAL_WEAPON.validate(player, stack) && PlayerAction.getCap(player).isADS();
    }

    @Override
    public Recipe getRecipe()
    {
        return AVAGunRecipes.BINOCULAR;
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return AVAWeaponUtil.Classification.SPECIAL_WEAPON;
    }

    @Override
    public AVAItemGun.IScopeType getScopeType(ItemStack stack)
    {
        return AVAItemGun.ScopeTypes.X4;
    }
}
