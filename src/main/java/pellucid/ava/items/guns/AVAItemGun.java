package pellucid.ava.items.guns;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.gameevent.GameEvent;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredItem;
import org.apache.logging.log4j.util.BiConsumer;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.base.IAVARangedAttackMob;
import pellucid.ava.entities.functionalities.IDifficultyScaledHostile;
import pellucid.ava.entities.objects.item.GunItemEntity;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.gun.attachments.GunAttachmentCategory;
import pellucid.ava.gun.attachments.GunAttachmentManager;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.attachments.IAttachmentManager;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.gun.stats.FloatGunStat;
import pellucid.ava.gun.stats.FloatPairGunStat;
import pellucid.ava.gun.stats.GunStat;
import pellucid.ava.gun.stats.GunStatBuilder;
import pellucid.ava.gun.stats.GunStatFunctions;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.gun.stats.IntegerGunStat;
import pellucid.ava.gun.stats.ItemGunStat;
import pellucid.ava.gun.stats.NumberGunStat;
import pellucid.ava.gun.stats.ScopeGunStat;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.items.functionalities.IScopedItem;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.items.miscs.GunStatModifierManager;
import pellucid.ava.items.miscs.GunStatsMap;
import pellucid.ava.player.PlayerStances;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.sounds.SoundTrack;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;
import pellucid.ava.util.QuadConsumer;

import javax.annotation.Nullable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static pellucid.ava.gun.stats.BooleanGunStat.bool;
import static pellucid.ava.gun.stats.FloatGunStat.floating;
import static pellucid.ava.gun.stats.GunStatTypes.*;
import static pellucid.ava.gun.stats.IntegerGunStat.integer;
import static pellucid.ava.util.AVAConstants.DEFAULT_SPREAD_RECOVERY_FACTOR;
import static pellucid.ava.util.AVAConstants.SPACING;

public class AVAItemGun extends AVAAnimatedItem<AVAItemGun> implements IHasRecipe, IClassification, ICustomTooltip, IScopedItem
{
    protected static final RandomSource RANDOM = RandomSource.createThreadSafe();
    private final GunStatsMap stats;
    private final Properties properties;
    private final SoundTrack shootSound;
    private final SoundTrack reloadSound;
    private final SoundTrack preReloadSound;
    private final SoundTrack posteloadSound;
    private final Supplier<SoundEvent> scopeSound;
    protected final SoundTrack drawSound;
    private final Recipe recipe;
    private final AVAItemGun master;
    public List<AVAItemGun> subGuns = new ArrayList<>();

    private final AVAWeaponUtil.Classification classification;
    private final Map<GunAttachmentTypes, Map<GunStatTypes, GunStat<?>>> attachmentTypes;
    private final Map<GunAttachmentCategory, List<GunAttachmentTypes>> attachmentCategories;

    protected static final UUID MOVEMENT_SPEED_MODIFIER = UUID.fromString("2A52D429-3349-462E-9191-78783205BFBE");

    public AVAItemGun(Properties gunProperties, Recipe recipe)
    {
        this(gunProperties, recipe, AVAItemGroups.MAIN);
    }

    public AVAItemGun(Properties gunProperties, Recipe recipe, DeferredHolder<CreativeModeTab, CreativeModeTab> group)
    {
        super(new Item.Properties().stacksTo(1), GunStatusManager.INSTANCE);
        AVAItemGroups.putItem(group, () -> this);
        this.stats = gunProperties.stats;
        this.properties = gunProperties;
        this.shootSound = gunProperties.shootSound;
        this.reloadSound = gunProperties.reloadSound;
        this.preReloadSound = gunProperties.preReloadSound;
        this.posteloadSound = gunProperties.postReloadSound;
        this.scopeSound = gunProperties.scopeSound == null ? AVASounds.AIM : gunProperties.scopeSound;
        stats.putIfAbsent(AUTOMATIC, bool(true, true));
        stats.putIfAbsent(CROSSHAIR, bool(true, true));
        stats.putIfAbsent(REQUIRE_AIM, bool(false, false));
        stats.putIfAbsent(SILENCED, bool(false, false));
        stats.putIfAbsent(RELOAD_INTERACTABLE, bool(false, false));
        stats.putIfAbsent(EXTRA_RELOAD_STEPS, bool(false, false));
        stats.putIfAbsent(BULLET_TRAIL, bool(false, false));
        stats.putIfAbsent(FIRE_ANIMATION_TIME, integer(Math.max(3, Math.round(20.0F / (float) getStatValue(FIRE_RATE)))));
        stats.putIfAbsent(AIM_TIME, integer(-1));
        stats.putIfAbsent(OPTIONAL_SILENCER, bool(false, false));
        stats.putIfAbsent(SHAKE_FACTOR, floating(1.0F));
        stats.putIfAbsent(SPREAD_RECOVERY_FACTOR, floating(DEFAULT_SPREAD_RECOVERY_FACTOR));
        stats.putIfAbsent(SPREAD_RECOVERY, floating(GunStatFunctions.SPREAD_RECOVERY.apply(getAccuracy(ItemStack.EMPTY, null, false))));
        stats.putIfAbsent(RECOIL_COMPENSATION, floating(getStability(ItemStack.EMPTY, null, false)));
        stats.putIfAbsent(ATTACK_DAMAGE_FLOATING, FloatPairGunStat.pair(Pair.of(0.0F, 0.0F)));
        stats.putIfAbsent(SCOPE_TYPE, ScopeGunStat.scope(ScopeTypes.NONE));
        stats.putIfAbsent(SPREAD_MAX, FloatGunStat.floating(1.0F));
        stats.putIfAbsent(PELLETS, IntegerGunStat.integer(1));
        stats.putIfAbsent(SHAKE_TURN_CHANCE, FloatGunStat.floating(0.5F));
        stats.putIfAbsent(SPREAD_FACTOR, FloatGunStat.floating(1.0F));
        stats.putIfAbsent(EXIT_AIM_ON_FIRE, bool(false, false));

        //todo

        this.recipe = recipe;
        if (gunProperties.master != null)
            this.master = gunProperties.master;
        else
            this.master = this;
        master.subGuns.add(this);
        this.classification = gunProperties.classification.addToList(this);
        this.drawSound = gunProperties.drawSound;
        this.attachmentTypes = gunProperties.attachments;
        this.attachmentCategories = gunProperties.attachmentCategories;
    }

    public Properties getProperties()
    {
        return properties;
    }

    private final Lazy<Recipe> masterPaintRecipe = Lazy.of(() -> isMaster() ? new Recipe().addItem(this).addItem(Materials.ACETONE_SOLUTION.get()) : null);

    @Override
    public boolean isBarVisible(ItemStack stack)
    {
        return true;
    }

    @Override
    public int getBarWidth(ItemStack stack)
    {
        return (int) Math.round(13.0D * Math.min(1.0F, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) / (float) getCapacity(stack, true)));
    }

    @Override
    public int getBarColor(ItemStack stack)
    {
        float capacity = getCapacity(stack, true);
        float f = Math.max(0.0F, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) / capacity);
        return Mth.hsvToRgb(f / 3.0F, 1.0F, 1.0F);
    }

    public Recipe getMasterPaintRecipe()
    {
        return isMaster() ? masterPaintRecipe.get() : getMaster().getMasterPaintRecipe();
    }

    public boolean isSpecial()
    {
        return this instanceof AVASpecialWeapon;
    }

    public static GunAttachmentManager manager(ItemStack stack)
    {
        return GunAttachmentManager.of(stack);
    }

    public Map<GunAttachmentTypes, Map<GunStatTypes, GunStat<?>>> getAttachmentTypes()
    {
        return attachmentTypes;
    }

    public Map<GunAttachmentCategory, List<GunAttachmentTypes>> getAttachmentCategories()
    {
        return attachmentCategories;
    }

    @Override
    public boolean hasCustomEntity(ItemStack stack)
    {
        return true;
    }

    @Nullable
    @Override
    public Entity createEntity(Level world, Entity location, ItemStack itemstack)
    {
        return AVAServerConfig.isCompetitiveModeActivated() ? new GunItemEntity(location, AVAWeaponUtil.clearAnimationData(itemstack)) : super.createEntity(world, location, itemstack);
    }

    public void forceReload(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_AMMO, getCapacity(stack, true));
    }

    public void startReload(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_RELOAD, getReloadTime(stack));
    }

    public void startPreReload(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_PRE_RELOAD, getReloadTime(stack));
    }

    public void startPostReload(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_POST_RELOAD, getReloadTime(stack));
    }

    public void startDraw(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_DRAW, getDrawSpeed(stack, true));
    }

    public boolean reload(LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        int capacity = getCapacity(stack, true);
        if (competitive)
        {
            if (shooter instanceof Player && !((Player) shooter).getAbilities().instabuild)
            {
                int taken = Math.min(stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0), capacity - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0));
                stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) - taken);
                stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + taken);
            }
            else stack.set(AVADataComponents.TAG_ITEM_AMMO, capacity);
        }
        else
        {
            boolean isPlayer = shooter instanceof Player;
            while ((!isPlayer || (getSlotForMagazine((Player) shooter, stack) != -1 || ((Player) shooter).getAbilities().instabuild)) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < capacity)
            {
                int have = stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0);
                final int need = capacity - have;
                int provided = need;
                if (isPlayer && !((Player) shooter).getAbilities().instabuild)
                {
                    int slot = getSlotForMagazine((Player) shooter, stack);
                    if (slot == -1)
                        break;
                    ItemStack mag = ((Player) shooter).getInventory().getItem(slot);
                    boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                    if (isAmmoKit)
                    {
                        if (((AmmoKit) mag.getItem()).requireRefill())
                        {
                            provided = Math.min(need, mag.getMaxDamage() - mag.getDamageValue());
                            mag.setDamageValue(mag.getDamageValue() + provided);
                        }
                    }
                    else
                    {
                        provided = capacity - mag.getDamageValue();
                        if (mag.getCount() != 1)
                        {
                            mag.shrink(1);
                            ItemStack newMag = new ItemStack(getAmmoType(stack));
                            if (provided > need)
                            {
                                newMag.setDamageValue(need);
                                provided = need;
                                Level world = shooter.level();
                                if (!((Player) shooter).getInventory().add(newMag))
                                    world.addFreshEntity(new ItemEntity(world, shooter.getX(), shooter.getY(), shooter.getZ(), newMag));
                            }
                        }
                        else
                        {
                            if (provided > need)
                            {
                                mag.setDamageValue(mag.getDamageValue() + need);
                                provided = need;
                            }
                            else
                                mag.shrink(1);
                        }
                    }
                }
                stack.set(AVADataComponents.TAG_ITEM_AMMO, provided + have);
            }
        }
        return true;
    }

    public boolean fire(Level world, LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        return fire(world, shooter, null, stack, competitive);
    }

    public boolean fire(Level world, LivingEntity shooter, ItemStack stack, QuadConsumer<PlayerAction, Player, ItemStack, Float> recoil, QuadConsumer<PlayerAction, Player, ItemStack, Float> shake, boolean competitive)
    {
        return fire(world, shooter, null, stack, recoil, shake, competitive);
    }

    public boolean fire(Level world, LivingEntity shooter, @Nullable LivingEntity target, ItemStack stack, boolean competitive)
    {
        return fire(world, shooter, target, stack, this::recoil, this::shake, competitive);
    }

    public boolean fire(Level world, LivingEntity shooter, @Nullable LivingEntity target, ItemStack stack, QuadConsumer<PlayerAction, Player, ItemStack, Float> recoil, QuadConsumer<PlayerAction, Player, ItemStack, Float> shake, boolean competitive)
    {
        if (firable(shooter, stack))
        {
            boolean isPlayer = shooter instanceof Player;
            float spread = 1.5F;
            if (isPlayer)
            {
                Player player = (Player) shooter;
                PlayerAction capability = PlayerAction.getCap(player);
                float multiplier = getMultiplier(player);
                spread = spread(capability, player, stack, multiplier, true);
                if (exitAimOnFire(stack))
                    PlayerAction.getCap(player).setIsADS(world, false);
            }
            stack.set(AVADataComponents.TAG_ITEM_FIRE, getFireAnimation(stack));
            stack.set(AVADataComponents.TAG_ITEM_IDLE, 3);
            if (!world.isClientSide())
            {
                boolean silenced = isSilenced(stack);
                if (isPlayer)
                {
                    Player player = (Player) shooter;
                    if (!player.getAbilities().instabuild && !silenced && RANDOM.nextFloat() < 0.025F)
                        for (Monster monster : world.getEntitiesOfClass(Zombie.class, player.getBoundingBox().inflate(25.0F)))
                            monster.setTarget(player);
                }
                if (!silenced)
                    shooter.gameEvent(GameEvent.PROJECTILE_SHOOT, shooter);
                summonBullet(world, shooter, stack, target, spread);
                if (!isPlayer || !((Player) shooter).getAbilities().instabuild)
                    stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) - 1);
            }
            if (isPlayer)
            {
                Player player = (Player) shooter;
                PlayerAction capability = PlayerAction.getCap(player);
                float multiplier = getMultiplier(player);
                recoil.accept(capability, player, stack, multiplier);
                shake.accept(capability, player, stack, multiplier);
            }
            return true;
        }
        return false;
    }

    public boolean exitAimOnFire(ItemStack stack)
    {
        return getStat(stack, EXIT_AIM_ON_FIRE, false);
    }

    protected float getFireVolume(ItemStack stack)
    {
        return 1.0F;
    }

    protected float getFirePitch(ItemStack stack)
    {
        float range = 0.2F;
        return 1.0F - range / 2.0F + RANDOM.nextFloat() / (1.0F / range);
    }

    public float getMultiplier(Player player)
    {
        float multiplier = 1;
        if ((player.level().isClientSide ? AVACommonUtil.isMovingOnGroundClient(player) : AVACommonUtil.isMovingOnGroundServer(player)) && !player.isCrouching())
            multiplier *= player.level().getGameTime() % 8L < 4L ? 1.5F : 1.25F;
        return multiplier;
    }

    protected void summonBullet(Level world, LivingEntity shooter, ItemStack stack, float spread)
    {
        summonBullet(world, shooter, stack, null, spread);
    }

    protected void summonBullet(Level world, LivingEntity shooter, ItemStack stack, @Nullable LivingEntity target, float spread)
    {
        if (shooter instanceof IAVARangedAttackMob)
            spread = ((IAVARangedAttackMob) shooter).getSpread(spread);
        if (shooter instanceof IDifficultyScaledHostile)
            spread *= ((IDifficultyScaledHostile) shooter).getSpreadScale(shooter.level().getDifficulty());
        AVAWorldData data = AVAWorldData.getInstance(world);
        if (target != null && (data.uavS.containsKey(target.getUUID()) || data.uavEscortS.containsKey(target.getUUID())))
            spread *= 0.25F;
        Pair<Float, Float> damageFloating = getDamageFloating(stack, true);
        float range = getRange(stack, true);
        float penetration = getPenetration(stack);
        int pellets = getPellets(stack, true);
        for (int i = 0; i < pellets; i++)
        {
            float damage = (getBulletDamage(stack, true) + damageFloating.getA() + damageFloating.getB() * RANDOM.nextFloat()) / 5.0F;
            BulletEntity bullet = new BulletEntity(world, shooter, this, range, damage, spread, penetration);
            if (target != null)
                bullet.fromMob(target);
            world.addFreshEntity(bullet);
        }
    }

    public void postReload(ItemStack stack)
    {
        stack.set(AVADataComponents.TAG_ITEM_RELOAD, 0);
    }

    public float spread(PlayerAction capability, Player player, ItemStack stack, float multiplier, boolean modify)
    {
        final float staticAccuracy = getAccuracy(stack, player, true);
        final float initialSpread = GunStatFunctions.INITIAL_ACCURACY.apply(getInitialAccuracy(stack, player, true));
        float accuracy = GunStatFunctions.ACCURACY.apply(staticAccuracy);
        float spread = accuracy;
        if (capability.getSpread() <= 0)
        {
            spread = initialSpread * multiplier;
            if (spread <= 0.0F)
                spread = accuracy * 0.05F * multiplier;
        }
        else
            spread = capability.getSpread() + spread * multiplier * getSpreadFactor(stack, true);
        spread = Math.max(initialSpread * multiplier, Math.min(spread, (100.0F - staticAccuracy) / 30.0F * getSpreadMax(stack)));
        if (modify)
            capability.setSpread(spread);
        return spread;
    }

    public void recoil(PlayerAction capability, Player player, ItemStack stack, float multiplier)
    {
        recoil(capability, player, stack, multiplier, (p, r) -> p.setXRot(p.getXRot() + r));
    }

    public static void recoil(PlayerAction cap, Player player, ItemStack stack, float multiplier, BiConsumer<Player, Float> add)
    {
        if (!(stack.getItem() instanceof AVAItemGun gun))
            return;
        if (!gun.isAuto(stack))
            multiplier *= 1.5F;
        final float stability = 100.0F - gun.getStability(stack, player, true);
        float pitch = -1 * cap.getRecoil() * 0.05F + stability * -0.075F;
        if (cap.getRecoil() == 0)
            pitch *= 2.0F;
        boolean isSniper = gun.getClassification().isSniper();
        if (isSniper)
            multiplier *= 1.75F;
        pitch *= multiplier;
        float recoil = cap.getRecoil() + Math.abs(pitch);
        float limit = Math.min(stability * 0.65F, 23);
        if (gun.isAuto(stack) && recoil > limit)
        {
            recoil = limit;
            pitch = (recoil - cap.getRecoil()) * -1;
        }
        cap.setRecoil(recoil);
        add.accept(player, pitch);
    }

    public void shake(PlayerAction capability, Player player, ItemStack stack, float multiplier)
    {
        shake(capability, player, stack, multiplier, (p, y) -> p.setYRot(p.getYRot() + y));
    }

    public static void shake(PlayerAction cap, Player player, ItemStack stack, float multiplier, BiConsumer<Player, Float> add)
    {
        if (!(stack.getItem() instanceof AVAItemGun gun))
            return;
        final float stability = 100.0F - gun.getStability(stack, player, true);
        float yaw = stability * cap.getRecoil() * 0.0025F * gun.getShakeFactor(stack, true);
        boolean isSniper = gun.classification.isSniper();
        if (isSniper)
            multiplier *= 0.225F;
        yaw *= multiplier;

        boolean left = stack.getOrDefault(AVADataComponents.TAG_ITEM_SHAKE_TURN, false);
        if (RANDOM.nextFloat() <= gun.getShakeTurnChance(stack, true))
        {
            left = !left;
            stack.set(AVADataComponents.TAG_ITEM_SHAKE_TURN, left);
        }
        yaw *= left ? -1.0F : 1.0F;

        float shake = cap.getShake() + yaw;
        float limit = stability / 2.5F;
        if (Math.abs(shake) > limit)
        {
            if (cap.getShake() > 0)
                shake = limit;
            else
                shake = -limit;
            yaw = (shake - cap.getShake());
        }
        cap.setShake(shake);
        add.accept(player, yaw);
    }

    public boolean firable(LivingEntity shooter, ItemStack stack)
    {
        boolean isCreative = false;
        if (shooter instanceof Player)
        {
            if (!getClassification().validate((Player) shooter, stack))
                return false;
            if (shooter.isSprinting())
                return false;
            if (isRequireAim(stack) && !PlayerAction.getCap((Player) shooter).isADS())
                return false;
            isCreative = ((Player) shooter).getAbilities().instabuild;
        }
        return stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) > 0 || isCreative;
    }

    public boolean reloadable(LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        boolean hasMag = true;
        if (shooter instanceof Player)
        {
            if (!((Player) shooter).getAbilities().instabuild)
            {
                if (!competitive)
                    hasMag = getSlotForMagazine((Player) shooter, stack) != -1;
                else
                    hasMag = stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) > 0;
            }
            if (shooter.isSprinting())
                return false;
        }
        return hasMag && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < getCapacity(stack, true);
    }

    protected int getSlotForMagazine(Player player, ItemStack stack)
    {
        for (int slot=0;slot<player.getInventory().getContainerSize();slot++)
        {
            ItemStack slotStack = player.getInventory().getItem(slot);
            if (slotStack.getItem() == getAmmoType(stack))
                return slot;
            else if (slotStack.getItem() instanceof AmmoKit)
            {
                if (!((AmmoKit) slotStack.getItem()).requireRefill() || slotStack.getDamageValue() < slotStack.getMaxDamage())
                    return slot;
            }
        }
        return -1;
    }

    public static boolean hasAttachments(Item item)
    {
        return item instanceof AVAItemGun gun && !gun.getAttachmentCategories().isEmpty();
    }

    @Override
    public ItemAttributeModifiers getAttributeModifiers(ItemStack stack)
    {
        return ItemAttributeModifiers.builder()
                .add(Attributes.MOVEMENT_SPEED, new AttributeModifier(MOVEMENT_SPEED_MODIFIER, "Speed modifier", (-100 + getMobility(stack, true)) / 600.0D, AttributeModifier.Operation.ADD_VALUE), EquipmentSlotGroup.MAINHAND)
                .add(Attributes.ATTACK_SPEED, new AttributeModifier(BASE_ATTACK_SPEED_UUID, "Weapon modifier", Integer.MAX_VALUE, AttributeModifier.Operation.ADD_VALUE), EquipmentSlotGroup.MAINHAND)
                .build();
    }

    public SoundTrack getShootSound(ItemStack stack)
    {
        return stack.getOrDefault(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, false) ? AVASoundTracks.SILENCED_FIRE : this.shootSound;
    }

    public SoundTrack getReloadSound(ItemStack stack)
    {
        return this.reloadSound;
    }

    public SoundTrack getPreReloadSound(ItemStack stack)
    {
        return this.preReloadSound;
    }

    public SoundTrack getPostReloadSound(ItemStack stack)
    {
        return this.posteloadSound;
    }

    public SoundTrack getDrawSound(ItemStack stack)
    {
        return drawSound;
    }

    public SoundEvent getScopeSound(ItemStack stack)
    {
        return this.scopeSound.get();
    }

    public GunStatsMap getStats()
    {
        return stats;
    }

    public Map<GunStatTypes, GunStat<?>> getMutableStats()
    {
        return AVACommonUtil.removeIf(new HashMap<>(getStats()), (k, v) -> !k.isMutable());
    }

    private <T> T getStatValue(GunStatTypes stat)
    {
        return (T) stats.get(stat).getValue();
    }

    public <T> T getStat(ItemStack stack, GunStatTypes type, boolean modified)
    {
        GunStat<T> stat = (GunStat<T>) stats.get(type);
        Object value = stat.getValue();
        if (modified && stat instanceof NumberGunStat)
        {
            double modification = getStatModification(stack, type);
            if (modification != 0.0D)
                value = ((NumberGunStat) stat).calculateClamped(modification);
        }
        return (T) (modified && !stat.isEmpty() ? manager(stack).get(stack, type, value, stat.getDefaultValue()) : value);
    }

    protected double getStatModification(ItemStack stack, GunStatTypes type)
    {
        return GunStatModifierManager.ofUnsafe(stack).getModifiedValue(type);
    }

    public <T> T getStatWithoutBase(ItemStack stack, GunStatTypes type)
    {
        GunStat<T> stat = (GunStat<T>) stats.get(type);
        return manager(stack).get(stack, type, null, stat.getDefaultValue());
    }

    private Component getTip(ItemStack stack, GunStatTypes type, boolean beneficial)
    {
        float value = ((Number) getStat(stack, type, true)).floatValue();
        float originalValue = ((Number) getStat(stack, type, false)).floatValue();
        return Component.translatable(type.getKey()).append(": ").withStyle(ChatFormatting.AQUA).append(Component.literal(String.valueOf(AVACommonUtil.round(value, 3))).withStyle((AVACommonUtil.similar(value, originalValue) ? ChatFormatting.DARK_AQUA : ((beneficial && value > originalValue) || (!beneficial && value < originalValue)) ? ChatFormatting.GREEN : ChatFormatting.RED)));
    }

    public boolean hasTrail()
    {
        return (boolean) stats.get(BULLET_TRAIL).getValue();
    }

    public int getAimTime(ItemStack stack, boolean modified)
    {
        return getStat(stack, AIM_TIME, modified);
    }

    public boolean hasOptionalSilencer(ItemStack stack, boolean modified)
    {
        return getStat(stack, OPTIONAL_SILENCER, modified);
    }

    public float getAccuracy(ItemStack stack, @Nullable Player player, boolean modified)
    {
        return getStat(stack, player != null && PlayerAction.getCap(player).isADS() ? PlayerStances.aimAccuracyOf(player) : PlayerStances.accuracyOf(player), modified);
    }

    public float getInitialAccuracy(ItemStack stack, @Nullable Player player, boolean modified)
    {
        return player != null && PlayerAction.getCap(player).isADS() ? getStat(stack, PlayerStances.initialAimAccuracyOf(player), modified) : getStat(stack, PlayerStances.initialAccuracyOf(player), modified);
    }

    public float getStability(ItemStack stack, @Nullable Player player, boolean modified)
    {
        return getStat(stack, player != null && PlayerAction.getCap(player).isADS() ? PlayerStances.aimStabilityOf(player) : PlayerStances.stabilityOf(player), modified);
    }

    public float getShakeFactor(ItemStack stack, boolean modified)
    {
        return getStat(stack, SHAKE_FACTOR, modified);
    }

    public float getShakeTurnChance(ItemStack stack, boolean modified)
    {
        return getStat(stack, SHAKE_TURN_CHANCE, modified);
    }

    public Pair<Float, Float> getDamageFloating(ItemStack stack, boolean modified)
    {
        return getStat(stack, ATTACK_DAMAGE_FLOATING, modified);
    }

    public float getPenetration(ItemStack stack)
    {
        return getStat(stack, PENETRATION, false);
    }

    public int getPellets(ItemStack stack, boolean modified)
    {
        return getStat(stack, PELLETS, modified);
    }

    @Override
    public IScopeType getScopeType(ItemStack stack)
    {
        return getScopeType(stack, true);
    }

    public IScopeType getScopeType(ItemStack stack, boolean modified)
    {
        return getStat(stack, SCOPE_TYPE, modified);
    }

    public boolean isAuto(ItemStack stack)
    {
        return getStat(stack, AUTOMATIC, false);
    }

    public boolean hasCrosshair(ItemStack stack)
    {
        return getStat(stack, CROSSHAIR, false);
    }

    public boolean isReloadInteractable(ItemStack stack)
    {
        return (boolean) getStat(stack, RELOAD_INTERACTABLE, false) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) != 0;
    }

    public boolean hasExtraReloadSteps(ItemStack stack)
    {
        return getStat(stack, EXTRA_RELOAD_STEPS, false);
    }

    public int getCapacity(ItemStack stack, boolean modified)
    {
        return getStat(stack, CAPACITY, modified);
    }

    public float getBulletDamage(ItemStack stack, boolean modified)
    {
        return getStat(stack, ATTACK_DAMAGE, modified);
    }

    public float getRange(ItemStack stack, boolean modified)
    {
        return getStat(stack, RANGE, modified);
    }

    public float getFireRate(ItemStack stack, boolean modified)
    {
        return getStat(stack, FIRE_RATE, modified);
    }

    public int getFireAnimation(ItemStack stack)
    {
        return getStat(stack, FIRE_ANIMATION_TIME, false);
    }

    public int getReloadTime(ItemStack stack)
    {
        return getStat(stack, RELOAD_TIME, false);
    }

    public int getPreReloadTime(ItemStack stack)
    {
        return getStat(stack, PRE_RELOAD_TIME, false);
    }

    public int getPostReloadTime(ItemStack stack)
    {
        return getStat(stack, POST_RELOAD_TIME, false);
    }

    public Item getAmmoType(ItemStack stack)
    {
        return ((Supplier<Item>) getStat(stack, AMMO_TYPE, false)).get();
    }

    public boolean isRequireAim(ItemStack stack)
    {
        return getStat(stack, REQUIRE_AIM, false);
    }

    public float getMobility(ItemStack stack, boolean modified)
    {
        return getStat(stack, MOBILITY, modified);
    }

    public boolean isSilenced(ItemStack stack)
    {
        return (boolean) getStat(stack, SILENCED, false) || stack.getOrDefault(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, false);
    }

    public float getRecoilCompensation(Player player, ItemStack stack, boolean modified)
    {
        return player != null && PlayerAction.getCap(player).isADS() ? getStat(stack, PlayerStances.aimRecoilCompensationOf(player), modified) : getStat(stack, PlayerStances.recoilCompensationOf(player), modified);
    }

    public float getSpreadRecovery(ItemStack stack, boolean modified)
    {
        return getStat(stack, SPREAD_RECOVERY, modified);
    }

    public float getSpreadRecoveryFactor(ItemStack stack, boolean modified)
    {
        return getStat(stack, SPREAD_RECOVERY_FACTOR, modified);
    }

    public int getDrawSpeed(ItemStack stack, boolean modified)
    {
        return getStat(stack, DRAW_TIME, modified);
    }

    public float getSpreadMax(ItemStack stack)
    {
        return getStat(stack, SPREAD_MAX, false);
    }

    public float getSpreadFactor(ItemStack stack, boolean modified)
    {
        return getStat(stack, SPREAD_FACTOR, modified);
    }

    public List<AVAItemGun> getSubGuns()
    {
        return new ArrayList<>(this.subGuns);
    }

    public AVAItemGun getMaster()
    {
        return this.master;
    }

    public boolean isMaster()
    {
        return this == this.master;
    }

    @Override
    public Recipe getRecipe()
    {
        return this.recipe;
    }

    @Override
    public boolean addToolTips(ItemTooltipEvent event)
    {
        ICustomTooltip.super.addToolTips(event);
        List<Component> tips = event.getToolTip();
        ItemStack stack = event.getItemStack();
        tips.add(Component.translatable("ava.item.tips.ammo", stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0), getCapacity(stack, true)).withStyle(ChatFormatting.GOLD));
        tips.add(Component.translatable("ava.item.tips.ammo_type", getAmmoType(stack).getDescription()).withStyle(ChatFormatting.GREEN));
        GunMasteryManager manager = GunMasteryManager.ofUnsafe(stack);
        if (manager.hasMasteryUnlocked() && !AVAServerConfig.DISABLE_MASTERY_SYSTEM.get())
        {
            tips.add(Component.translatable("ava.item.tips.mastery", manager.mastery.getTitle(manager.masteryLevel)).withStyle(manager.mastery.getColour()));
            if (manager.hasOngoingTask())
                tips.add(Component.translatable("ava.item.tips.mastery_task", manager.task.getDescription(this, manager.taskProgress)).withStyle(manager.mastery.getColour()));
        }
        return true;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void addAdditionalToolTips(ItemTooltipEvent event)
    {
        List<Component> tips = event.getToolTip();
        ItemStack stack = event.getItemStack();


        float value = getBulletDamage(stack, true);
        float damage = getBulletDamage(stack, false);
        tips.add(ATTACK_DAMAGE.getTranslatedName().copy().append(": ").withStyle(ChatFormatting.AQUA).append(Component.literal(AVACommonUtil.round(value, 3) + " x" + getPellets(stack, true)).withStyle((AVACommonUtil.similar(value, damage) ? ChatFormatting.DARK_AQUA : value > damage ? ChatFormatting.GREEN : ChatFormatting.RED))));
        tips.add(ATTACK_DAMAGE_FLOATING.getTranslatedName().copy().append(": ").withStyle(ChatFormatting.AQUA).append(Component.literal(AVACommonUtil.getDamageFloatingString(getDamageFloating(stack, true))).withStyle(ChatFormatting.DARK_AQUA)));
        tips.add(getTip(stack, INITIAL_ACCURACY, true));
        tips.add(getTip(stack, ACCURACY, true));
        tips.add(getTip(stack, STABILITY, true));
        tips.add(getTip(stack, PENETRATION, true));
        tips.add(getTip(stack, FIRE_RATE, true));
        tips.add(getTip(stack, RANGE, true));
        tips.add(getTip(stack, MOBILITY, true));
        tips.add(getTip(stack, RELOAD_TIME, false));
        tips.add(getTip(stack, DRAW_TIME, false));
        tips.add(getTip(stack, AIM_TIME, false));
        tips.add(getTip(stack, RECOIL_COMPENSATION, true));
        tips.add(getTip(stack, SPREAD_RECOVERY, true));
        tips.add(getTip(stack, SHAKE_FACTOR, false));
        tips.add(AUTOMATIC.getTranslatedName().append(": ").withStyle(ChatFormatting.AQUA).append(Component.literal(String.valueOf(isAuto(stack))).withStyle(ChatFormatting.DARK_AQUA)));

        tips.add(SPACING);


        IAttachmentManager manager = manager(stack);
        for (GunAttachmentTypes type : manager.getTypes())
            tips.add(type.getTranslated().copy().withStyle(ChatFormatting.GOLD));

        tips.add(Component.literal(String.valueOf(stack.getOrDefault(AVADataComponents.TAG_ITEM_UUID, Util.NIL_UUID))).withStyle(ChatFormatting.DARK_AQUA));
    }

    @Override
    public AVAWeaponUtil.Classification getClassification()
    {
        return classification;
    }

    public static final ResourceLocation MOSIN_NAGANT_TEXTURE = new ResourceLocation(AVA.MODID + ":textures/overlay/mosin_nagant.png");
    public static final ResourceLocation PGM_TEXTURE = new ResourceLocation(AVA.MODID + ":textures/overlay/pgm.png");
    public static final ResourceLocation KNUTS_M110_TEXTURE = new ResourceLocation(AVA.MODID + ":textures/overlay/knuts_m110.png");
    public static final ResourceLocation KNUTS_M110_TEXTURE_2 = new ResourceLocation(AVA.MODID + ":textures/overlay/knuts_m110_2.png");
    public static final ResourceLocation M202_TEXTURE = new ResourceLocation(AVA.MODID + ":textures/overlay/m202.png");

    public enum ScopeTypes implements IScopeType
    {
        NONE(null, false, 0.0F),
        GM94(null, true, 0.1F),
        IRON_SIGHT(null, false, 0.2F),
        X2(null, true, 0.3F),
        X4(null, true, 0.4F),
        M202(M202_TEXTURE, true, 0.4F),
        SR25(MOSIN_NAGANT_TEXTURE, true, 0.5F, true),
        KNUTS_M110(KNUTS_M110_TEXTURE_2, true, 0.5F, true),
        MOSIN_NAGANT(MOSIN_NAGANT_TEXTURE, true, 0.6F),
        FR_F2(MOSIN_NAGANT_TEXTURE, true, 0.5F, true),
        PGM(PGM_TEXTURE, true, 0.5F, true),
        ;
        @Nullable
        private final ResourceLocation texture;
        private final boolean sound;
        private final float magnification;
        private final boolean doubleZoom;

        ScopeTypes(@Nullable ResourceLocation texture, boolean sound, float magnification)
        {
            this(texture, sound, magnification, false);
        }

        ScopeTypes(@Nullable ResourceLocation texture, boolean sound, float magnification, boolean doubleZoom)
        {
            this.texture = texture;
            this.sound = sound;
            this.magnification = magnification;
            this.doubleZoom = doubleZoom;
        }

        @Override
        public boolean isDoubleZoom()
        {
            return doubleZoom;
        }

        @Override
        public boolean isIronSight()
        {
            return this == IRON_SIGHT;
        }

        @Nullable
        @Override
        public ResourceLocation getTexture()
        {
            return texture;
        }

        @Override
        public float getMagnification(ItemStack stack)
        {
            return GunStatusManager.INSTANCE.zoomLevel.current() == 2 ? magnification + 0.3F : magnification;
        }

        @Override
        public boolean noAim(ItemStack stack)
        {
            return this == NONE;
        }

        @Override
        public boolean hasSound(ItemStack stack)
        {
            return sound;
        }
    }

    public interface IScopeType
    {
        default boolean isDoubleZoom()
        {
            return false;
        }

        boolean isIronSight();

        @Nullable
        ResourceLocation getTexture();

        float getMagnification(ItemStack stack);

        boolean noAim(ItemStack stack);

        boolean hasSound(ItemStack stack);

        default boolean animated(ItemStack stack)
        {
            return !noAim(stack) && hasSound(stack);
        }
    }

    public static class Properties
    {
        private final AVAWeaponUtil.Classification classification;
        private final GunStatsMap stats;
        private SoundTrack shootSound = SoundTrack.of();
        private SoundTrack reloadSound = SoundTrack.of();
        private SoundTrack preReloadSound = SoundTrack.of();
        private SoundTrack postReloadSound = SoundTrack.of();
        private SoundTrack drawSound = SoundTrack.of();
        private Supplier<SoundEvent> scopeSound = null;
        private AVAItemGun master = null;
        private final Map<GunAttachmentTypes, Map<GunStatTypes, GunStat<?>>> attachments = new HashMap<>();
        private final Map<GunAttachmentCategory, List<GunAttachmentTypes>> attachmentCategories = new HashMap<>();

        public Properties(Properties properties)
        {
            this.classification = properties.classification;
            this.stats = new GunStatsMap(properties.stats);
            this.shootSound = properties.shootSound;
            this.reloadSound = properties.reloadSound;
            this.preReloadSound = properties.preReloadSound;
            this.postReloadSound = properties.postReloadSound;
            this.drawSound = properties.drawSound;
            this.scopeSound = properties.scopeSound;
            this.master = properties.master;
            properties.attachments.forEach((k, v) -> {
                Map<GunStatTypes, GunStat<?>> map = new HashMap<>();
                v.forEach((k1, v1) -> map.put(k1, v1.copy()));
                this.attachments.put(k, map);
            });
            properties.attachmentCategories.forEach((k, v) -> this.attachmentCategories.put(k, new ArrayList<>(v)));
        }

        public Properties(AVAWeaponUtil.Classification classification, GunStatBuilder stats)
        {
            this(classification, stats.build());
        }

        public Properties(AVAWeaponUtil.Classification classification, Map<GunStatTypes, GunStat<?>> stats)
        {
            this.classification = classification;
            this.stats = new GunStatsMap(stats);
        }

        public Properties getStats(Consumer<Map<GunStatTypes, GunStat<?>>> action)
        {
            action.accept(stats);
            return this;
        }

        public Properties sounds(SoundTrack shoot, SoundTrack reload, SoundTrack draw)
        {
            this.drawSound = draw;
            return shootSound(shoot).reloadSound(reload);
        }

        public Properties shootSound(SoundTrack shoot)
        {
            this.shootSound = shoot;
            return this;
        }

        public Properties reloadSound(SoundTrack reload)
        {
            this.reloadSound = reload;
            return this;
        }

        public Properties preReloadSound(SoundTrack preReload)
        {
            this.preReloadSound = preReload;
            return this;
        }

        public Properties postReloadSound(SoundTrack postReload)
        {
            this.postReloadSound = postReload;
            return this;
        }

        public Properties skinned(AVAItemGun master)
        {
            this.master = master;
            return this;
        }

        public Properties magazineType(DeferredItem<Item> item)
        {
            stats.put(AMMO_TYPE, ItemGunStat.item(item));
            return this;
        }

        public Properties stat(GunAttachmentTypes type, GunStatBuilder stat)
        {
            return stat(type, stat.build());
        }

        public Properties stat(GunAttachmentTypes type, Map<GunStatTypes, GunStat<?>> stat)
        {
            attachments.put(type, stat);
            GunAttachmentCategory category = type.getCategory();
            if (!attachmentCategories.containsKey(category))
                attachmentCategories.put(category, new ArrayList<>());
            attachmentCategories.get(category).add(type);
            return this;
        }
    }

    public static class Serializer implements JsonSerializer<AVAItemGun>, JsonDeserializer<AVAItemGun>
    {
        public static final Serializer INSTANCE = new Serializer();
        public static final Map<String, JsonElement> GLOBAL_OVERRIDE_MAP = new HashMap<>();

        private Serializer()
        {
        }

        @Override
        public AVAItemGun deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
        {
            return deserialize(json);
        }

        @Override
        public JsonElement serialize(AVAItemGun gun, Type typeOfSrc, JsonSerializationContext context)
        {
            return serialize(gun);
        }

        public static JsonElement serialize(AVAItemGun gun)
        {
            JsonObject obj = new JsonObject();
            obj.addProperty("weapon", BuiltInRegistries.ITEM.getKey(gun).toString());
            AVACommonUtil.forEachSorted(gun.getMutableStats(), Comparator.comparingInt(Enum::ordinal), (type, stat) -> stat.writeToJson(obj));
            return obj;
        }

        public static AVAItemGun deserialize(JsonElement json)
        {
            JsonObject obj = json.getAsJsonObject();

            final String weapon = obj.get("weapon").getAsString();
            ResourceLocation gunName = new ResourceLocation(weapon);
            if (weapon.equals("global"))
            {
                GLOBAL_OVERRIDE_MAP.clear();
                for (String s : obj.keySet())
                {
                    if (s.equals("weapon"))
                        continue;
                    GLOBAL_OVERRIDE_MAP.put(s, obj.get(s));
                }
                return null;
            }
            if (BuiltInRegistries.ITEM.get(gunName) instanceof AVAItemGun gun)
            {
                Map<GunStatTypes, GunStat<?>> stats = gun.getMutableStats();
                for (String s : obj.keySet())
                {
                    if (s.equals("weapon"))
                        continue;
                    stats.get(GunStatTypes.valueOf(s.toUpperCase(Locale.ROOT))).readFromJson(GLOBAL_OVERRIDE_MAP.getOrDefault(s, obj.get(s)));
                }
                return gun;
            }
            throw new NullPointerException("Item " + gunName + " not found or is not a gun item.");
        }
    }
}
