package pellucid.ava.items.guns;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.entities.base.AVAEntity;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.recipes.Recipe;

import javax.annotation.Nullable;

public class AVASpecialWeapon extends AVAItemGun
{
    protected final ProjectileFactory projectileFactory;

    public AVASpecialWeapon(Properties gunProperties, Recipe recipe, ProjectileFactory projectileFactory)
    {
        super(gunProperties, recipe);
        this.projectileFactory = projectileFactory;
    }

    @Override
    public boolean reload(LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        int capacity = getCapacity(stack, true);
        if (competitive)
        {
            if (shooter instanceof Player && !((Player) shooter).getAbilities().instabuild)
            {
                int taken = Math.min(stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0), capacity - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0));
                stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) - taken);
                stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + taken);
            }
            else stack.set(AVADataComponents.TAG_ITEM_AMMO, capacity);
        }
        else
        {
            boolean isPlayer = shooter instanceof Player;
            while ((!isPlayer || (getSlotForMagazine((Player) shooter, stack) != -1 || ((Player) shooter).getAbilities().instabuild)) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < capacity)
            {
                int have = stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0);
                int need = capacity - have;
                int amount = need;
                if (isPlayer && !((Player) shooter).getAbilities().instabuild)
                {
                    int slot = getSlotForMagazine((Player) shooter, stack);
                    if (slot == -1)
                        break;
                    ItemStack mag = ((Player) shooter).getInventory().getItem(slot);
                    boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                    if (isAmmoKit)
                    {
                        if (((AmmoKit) mag.getItem()).requireRefill())
                        {
                            amount = mag.getMaxDamage() - need;
                            if (amount > need)
                            {
                                amount = need;
                                mag.setDamageValue(mag.getDamageValue() + amount);
                            }
                        }
                    }
                    else
                        mag.shrink(Math.min(mag.getCount(), need));
                }
                stack.set(AVADataComponents.TAG_ITEM_AMMO, amount + have);
            }
        }
        return true;
    }

    @Override
    protected void summonBullet(Level world, LivingEntity shooter, ItemStack stack, @Nullable LivingEntity target, float spread)
    {
        AVAEntity projectile = projectileFactory.create(world, shooter, spread, this, stack);
        if (target != null)
            projectile.fromMob(target);
        world.addFreshEntity(projectile);
    }

    @FunctionalInterface
    public interface ProjectileFactory
    {
        AVAEntity create(Level world, LivingEntity shooter, float spread, AVAItemGun weapon, ItemStack stack);
    }
}
