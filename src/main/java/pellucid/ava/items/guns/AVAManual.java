package pellucid.ava.items.guns;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.gun.stats.BooleanGunStat;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.recipes.Recipe;

public class AVAManual extends AVASingle
{
    public AVAManual(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties.getStats((stats) -> stats.put(GunStatTypes.RELOAD_INTERACTABLE, BooleanGunStat.bool(false, true))), recipe);
    }

    @Override
    public boolean reload(LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        int capacity = getCapacity(stack, true);
        if (competitive)
        {
            if (shooter instanceof Player && !((Player) shooter).getAbilities().instabuild)
            {
                int taken = Math.min(1, Math.min(stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0), capacity - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0)));
               stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) - taken);
               stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + taken);
            }
            else
               stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + 1);
        }
        else
        {
            boolean isPlayer = shooter instanceof Player;
            if ((!isPlayer || (getSlotForMagazine((Player) shooter, stack) != -1 || ((Player) shooter).getAbilities().instabuild)) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < capacity)
            {
                if (isPlayer && !((Player) shooter).getAbilities().instabuild)
                {
                    int slot = getSlotForMagazine((Player) shooter, stack);
                    if (slot != -1)
                    {
                        ItemStack mag = ((Player) shooter).getInventory().getItem(slot);
                        if (!(mag.getItem() instanceof AmmoKit))
                            mag.shrink(1);
                        else if (((AmmoKit) mag.getItem()).requireRefill())
                            mag.setDamageValue(mag.getDamageValue() + 1);
                    }
                    else
                    {
                        return false;
                    }
                }
               stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + 1);
            }
        }
        return true;
    }
}
