package pellucid.ava.items.guns;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.recipes.Recipe;

public class AVAMagazineShotgun extends AVAShotgun
{
    public AVAMagazineShotgun(Properties gunProperties, Recipe recipe, int bullets)
    {
        super(gunProperties, recipe);
    }

    @Override
    public boolean reload(LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        int capacity = getCapacity(stack, true);
        if (competitive)
        {
            if (shooter instanceof Player && !((Player) shooter).getAbilities().instabuild)
            {
                int taken = Math.min(stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0), capacity - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0));
                stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) - taken);
                stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + taken);
            }
            else stack.set(AVADataComponents.TAG_ITEM_AMMO, capacity);
        }
        else
        {
            boolean isPlayer = shooter instanceof Player;
            while ((!isPlayer || (getSlotForMagazine((Player) shooter, stack) != -1 || ((Player) shooter).getAbilities().instabuild)) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < capacity)
            {
                int have = stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0);
                final int need = capacity - have;
                int provided = need;
                if (isPlayer && !((Player) shooter).getAbilities().instabuild)
                {
                    int slot = getSlotForMagazine((Player) shooter, stack);
                    if (slot == -1)
                        break;
                    ItemStack mag = ((Player) shooter).getInventory().getItem(slot);
                    boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                    if (isAmmoKit)
                    {
                        if (((AmmoKit) mag.getItem()).requireRefill())
                        {
                            provided = Math.min(need, mag.getMaxDamage() - mag.getDamageValue());
                            mag.setDamageValue(mag.getDamageValue() + provided);
                        }
                    }
                    else
                        mag.shrink(Math.min(mag.getCount(), capacity - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0)));
                }
                stack.set(AVADataComponents.TAG_ITEM_AMMO, provided + have);
            }
        }
        return true;
    }
}
