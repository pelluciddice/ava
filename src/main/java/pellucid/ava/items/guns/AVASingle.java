package pellucid.ava.items.guns;

import pellucid.ava.gun.stats.BooleanGunStat;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.recipes.Recipe;

public class AVASingle extends AVAItemGun
{
    public AVASingle(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties.getStats((stats) -> {
            stats.put(GunStatTypes.AUTOMATIC, BooleanGunStat.bool(true, false));
            stats.put(GunStatTypes.EXIT_AIM_ON_FIRE, BooleanGunStat.bool(false, true));
        }), recipe);
    }

    /*@Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
        PlayerAction capability = PlayerAction.getCap((PlayerEntity) entityIn);
        if (this.initTags(stack).getInt(TAG_ITEM_FIRE) != 0)
            if (capability.isAiming())
                capability.setAiming(false);
    }*/
}
