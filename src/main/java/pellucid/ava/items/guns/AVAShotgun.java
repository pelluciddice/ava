package pellucid.ava.items.guns;

import net.minecraft.world.item.ItemStack;
import pellucid.ava.recipes.Recipe;

public class AVAShotgun extends AVAManual
{
    public AVAShotgun(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties, recipe);
    }

    @Override
    public boolean exitAimOnFire(ItemStack stack)
    {
        return false;
    }
}
