package pellucid.ava.items.guns;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.recipes.Recipe;

public class AVABulletFilledGun extends AVAItemGun
{
    public AVABulletFilledGun(Properties gunProperties, Recipe recipe)
    {
        super(gunProperties, recipe);
    }

    @Override
    public boolean reload(LivingEntity shooter, ItemStack stack, boolean competitive)
    {
        if (competitive)
        {
            if (shooter instanceof Player && !((Player) shooter).getAbilities().instabuild)
            {
                int taken = Math.min(stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0), getCapacity(stack, true) - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0));
                stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) - taken);
                stack.set(AVADataComponents.TAG_ITEM_AMMO, stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + taken);
            }
            else stack.set(AVADataComponents.TAG_ITEM_AMMO, getCapacity(stack, true));
        }
        else
        {
            boolean isPlayer = shooter instanceof Player;
            while ((!isPlayer || (getSlotForMagazine((Player) shooter, stack) != -1 || ((Player) shooter).getAbilities().instabuild)) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < getCapacity(stack, true))
            {
                int have = stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0);
                final int need = getCapacity(stack, true) - have;
                int provided = need;
                if (isPlayer && !((Player) shooter).getAbilities().instabuild)
                {
                    int slot = getSlotForMagazine((Player) shooter, stack);
                    if (slot == -1)
                        break;
                    ItemStack mag = ((Player) shooter).getInventory().getItem(slot);
                    boolean isAmmoKit = mag.getItem() instanceof AmmoKit;
                    if (isAmmoKit)
                    {
                        if (((AmmoKit) mag.getItem()).requireRefill())
                        {
                            provided = Math.min(need, mag.getMaxDamage() - mag.getDamageValue());
                            mag.setDamageValue(mag.getDamageValue() + provided);
                        }
                    }
                    else
                    {
                        provided = Math.min(mag.getCount(), need);
                        if (!((Player) shooter).isCreative())
                            mag.shrink(provided);
                    }
                }
                stack.set(AVADataComponents.TAG_ITEM_AMMO, provided + have);
            }
        }
        return true;
    }
}
