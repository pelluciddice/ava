package pellucid.ava.mixins.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.MouseHandler;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.items.functionalities.IScopedItem;

@Mixin(MouseHandler.class)
public class MouseHandlerMixin
{
    @ModifyVariable(method = "turnPlayer", at = @At("STORE"), ordinal = 2)
    public double modifySensitivity(double d4)
    {
        Player player = Minecraft.getInstance().player;
        if (player != null)
        {
            ItemStack stack = player.getMainHandItem();
            if (stack.getItem() instanceof IScopedItem)
                return (d4 - 0.2F) * AVAClientConfig.MAGNIFS_MAP.getOrDefault(((IScopedItem) stack.getItem()).getScopeType(stack).getMagnification(stack), () -> 1.0F).get() + 0.2F;
        }
        return d4;
    }
}
