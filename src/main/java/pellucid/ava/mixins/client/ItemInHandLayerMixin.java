package pellucid.ava.mixins.client;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.core.component.DataComponentType;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.AVATPRenderer;
import pellucid.ava.entities.smart.RoledSidedSmartAIEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.items.guns.AVAItemGun;

import java.util.function.Predicate;
import java.util.function.Supplier;

@Mixin(ItemInHandLayer.class)
public class ItemInHandLayerMixin
{
    @Inject(method = "renderArmWithItem", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/renderer/ItemInHandRenderer;renderItem(Lnet/minecraft/world/entity/LivingEntity;Lnet/minecraft/world/item/ItemStack;Lnet/minecraft/world/item/ItemDisplayContext;ZLcom/mojang/blaze3d/vertex/PoseStack;Lnet/minecraft/client/renderer/MultiBufferSource;I)V"))
    private void renderArmWithItem(LivingEntity entity, ItemStack item, ItemDisplayContext type, HumanoidArm arm, PoseStack stack, MultiBufferSource source, int packedLight, CallbackInfo ci)
    {
        ItemStack itemStack = entity.getMainHandItem();
        if (itemStack.getItem() instanceof AVAItemGun)
        {
            Predicate<Supplier<DataComponentType<Integer>>> isActive = (key) -> itemStack.getOrDefault(key, 0) != 0;
            boolean actioning = isActive.test(AVADataComponents.TAG_ITEM_RELOAD) || isActive.test(AVADataComponents.TAG_ITEM_DRAW) || isActive.test(AVADataComponents.TAG_ITEM_SILENCER_INSTALL);
            AVATPRenderer.transformThirdPersonItemMatrix(stack, entity.tickCount, entity.isSprinting(), actioning, entity instanceof SidedSmartAIEntity && !entity.getEntityData().get(RoledSidedSmartAIEntity.IS_ACTIVE), entity instanceof Player && PlayerAction.getCap((Player) entity).isADS());
        }
    }
}
