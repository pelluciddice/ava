package pellucid.ava.mixins.client;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.core.component.DataComponentType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.AVATPRenderer;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.entities.smart.RoledSidedSmartAIEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.items.guns.AVAItemGun;

import java.util.function.Predicate;
import java.util.function.Supplier;

@Mixin(HumanoidModel.class)
public abstract class HumanoidModelMixin
{

    @Inject(method = "poseLeftArm(Lnet/minecraft/world/entity/LivingEntity;)V", at = @At("TAIL"))
    private void poseLeftArm(LivingEntity entity, CallbackInfo ci)
    {
        if (entity.getMainHandItem().getItem() instanceof AVAItemGun && AVAClientConfig.ENABLE_ALTERNATED_THIRD_PERSON_MODEL.get())
        {
            ModelPart l = ((HumanoidModel) (Object) this).leftArm;
            ModelPart r = ((HumanoidModel) (Object) this).rightArm;
        }
    }

    @Inject(method = "poseRightArm(Lnet/minecraft/world/entity/LivingEntity;)V", at = @At("TAIL"))
    private void poseRightArm(LivingEntity entity, CallbackInfo ci)
    {
        HumanoidModel o = ((HumanoidModel) (Object) this);
        ItemStack itemStack = entity.getMainHandItem();
        if (itemStack.getItem() instanceof AVAItemGun && AVAClientConfig.ENABLE_ALTERNATED_THIRD_PERSON_MODEL.get() && o.rightArmPose == HumanoidModel.ArmPose.BOW_AND_ARROW)
        {
            ModelPart l = o.leftArm;
            ModelPart r = o.rightArm;
            Predicate<Supplier<DataComponentType<Integer>>> isActive = (key) -> itemStack.getOrDefault(key, 0) != 0;
            boolean actioning = isActive.test(AVADataComponents .TAG_ITEM_RELOAD) || isActive.test(AVADataComponents.TAG_ITEM_DRAW) || isActive.test(AVADataComponents.TAG_ITEM_SILENCER_INSTALL);
            AVATPRenderer.transformThirdPersonHandModel(l, r, entity.tickCount, entity.isSprinting(), actioning, entity instanceof SidedSmartAIEntity && !entity.getEntityData().get(RoledSidedSmartAIEntity.IS_ACTIVE), entity instanceof Player && PlayerAction.getCap((Player) entity).isADS());
        }
    }
}
