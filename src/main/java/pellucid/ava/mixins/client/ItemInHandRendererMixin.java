package pellucid.ava.mixins.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemInHandRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import pellucid.ava.items.functionalities.ICustomModel;

@Mixin(ItemInHandRenderer.class)
public abstract class ItemInHandRendererMixin
{
    @ModifyVariable(method = "tick()V", at = @At("STORE"), ordinal = 0)
    private boolean requipM(boolean original)
    {
        boolean isNoAnim = (Minecraft.getInstance().player.getMainHandItem().getItem() instanceof ICustomModel);
        return !isNoAnim && original;
    }
}
