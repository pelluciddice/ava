package pellucid.ava.mixins.client;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.world.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.util.AVACommonUtil;

@Mixin(GameRenderer.class)
public class GameRendererMixin
{
    @Inject(method = "bobHurt(Lcom/mojang/blaze3d/vertex/PoseStack;F)V", at = @At("HEAD"), cancellable = true)
    private void hurtCameraEffect(PoseStack matrixStackIn, float partialTicks, CallbackInfo ci)
    {
        LivingEntity player = Minecraft.getInstance().player;
        if (player != null && Minecraft.getInstance().getCameraEntity() == player && AVAClientConfig.REMOVE_HURT_CAMERA_TILT_ON_FULLY_EQUIPPED.get())
        {
            if (AVACommonUtil.isFullEquipped(player))
                ci.cancel();
        }
    }
}
