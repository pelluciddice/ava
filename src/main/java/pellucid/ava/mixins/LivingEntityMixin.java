package pellucid.ava.mixins;

import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.DistExecutor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import pellucid.ava.util.AVAClientUtil;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin
{
    @Inject(method = "setSprinting", at = @At("HEAD"), cancellable = true)
    private void setSprinting(boolean sprint, CallbackInfo ci)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            setSprinting2(sprint, ci);
        });
    }

    @OnlyIn(Dist.CLIENT)
    private void setSprinting2(boolean sprint, CallbackInfo ci)
    {
        LivingEntity player = Minecraft.getInstance().player;
        if (sprint && player != null && player.getUUID().equals(((LivingEntity) (Object) this).getUUID()))
        {
            if (AVAClientUtil.shouldStopSprinting())
                ci.cancel();
        }
    }
}
