package pellucid.ava.config;

import net.neoforged.neoforge.common.ModConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

public class AVAConfigs
{
    public static final AVAServerConfig SERVER;
    public static final ModConfigSpec SERVER_SPEC;

    static
    {
        final Pair<AVAServerConfig, ModConfigSpec> specPair = new ModConfigSpec.Builder().configure(AVAConfig.SERVER_INSTANCE::build);
        SERVER_SPEC = specPair.getRight();
        SERVER = specPair.getLeft();
    }

    public static final AVAClientConfig CLIENT;
    public static final ModConfigSpec CLIENT_SPEC;

    static
    {
        final Pair<AVAClientConfig, ModConfigSpec> specPair = new ModConfigSpec.Builder().configure(AVAConfig.CLIENT_INSTANCE::build);
        CLIENT_SPEC = specPair.getRight();
        CLIENT = specPair.getLeft();
    }
}
