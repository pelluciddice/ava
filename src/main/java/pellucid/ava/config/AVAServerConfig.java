package pellucid.ava.config;

import net.neoforged.neoforge.common.ModConfigSpec.BooleanValue;
import net.neoforged.neoforge.common.ModConfigSpec.Builder;
import net.neoforged.neoforge.common.ModConfigSpec.ConfigValue;
import net.neoforged.neoforge.common.ModConfigSpec.DoubleValue;
import net.neoforged.neoforge.common.ModConfigSpec.IntValue;

public class AVAServerConfig extends AVAConfig
{
    public static DoubleValue M67_EXPLOSIVE_DAMAGE;
    public static DoubleValue M67_EXPLOSIVE_RANGE;

    public static DoubleValue M116A1_FLASH_RANGE;
    public static IntValue M116A1_FLASH_DURATION;

    public static DoubleValue MK3A2_EXPLOSIVE_DAMAGE;
    public static DoubleValue MK3A2_EXPLOSIVE_RANGE;
    public static DoubleValue MK3A2_FLASH_RANGE;
    public static IntValue MK3A2_FLASH_DURATION;

    public static DoubleValue M202_ROCKET_EXPLOSIVE_RANGE;

    public static DoubleValue GM94_GRENADE_EXPLOSIVE_RANGE;

    public static BooleanValue IS_AMMO_KIT_SUPPLIER_CRAFTABLE;
    public static BooleanValue EXPLOSION_DESTROYS_ENTITIES;
    public static IntValue EXPLOSION_DESTRUCTION_TYPE;
    public static DoubleValue DAMAGE_MULTIPLIER_AGAINST_PLAYERS;
    public static DoubleValue DAMAGE_MULTIPLIER_AGAINST_OTHERS;
    public static BooleanValue SHOULD_GUARDS_SPAWN;
    public static IntValue GUARDS_SPAWN_CHANCE;
    public static ConfigValue<String> MELEE_DESTROYABLE_BLOCK;

    public static BooleanValue ENABLE_SNIPERS;
    public static BooleanValue ENABLE_SEMI_SNIPERS;
    public static BooleanValue ENABLE_RIFLES;
    public static BooleanValue ENABLE_SUB_MACHINEGUNS;
    public static BooleanValue ENABLE_SHOTGUNS;
    public static BooleanValue ENABLE_PISTOLS;
    public static BooleanValue ENABLE_AUTO_PISTOLS;
    public static BooleanValue ENABLE_PROJECTILES;
    public static BooleanValue ENABLE_SPECIAL_WEAPONS;

    public static BooleanValue COMPETITIVE_MODE;
    public static BooleanValue PRESET_WITH_PARACHUTE;

    public static ConfigValue<String> DISABLED_WEAPONS;

    public static DoubleValue FIELD_KNIFE_MOVEMENT_SPEED_BONUS;

    public static BooleanValue WEAPON_DRAWING_DELAY;

    public static BooleanValue SPAWN_RESTRICTION;

    public static IntValue A_SITE_RADIUS;
    public static IntValue A_SITE_HEIGHT;
    public static IntValue B_SITE_RADIUS;
    public static IntValue B_SITE_HEIGHT;

    public static BooleanValue DISABLE_MASTERY_SYSTEM;

    public static BooleanValue AVA_RESTRICTED_MOVEMENT;

    public static BooleanValue PRESET_SINGLE_PRIMARY_WEAPON;

    public static BooleanValue FREE_ATTACHMENTS;
    public static IntValue MAX_ENTITY_OF_SAME_TYPE;

    public static BooleanValue DO_STRUCTURES_GENERATE;

    public static IntValue EXPECTED_PLAYERS;

    public static DoubleValue HEADSHOT_DAMAGE_SCALE;

//    public static BooleanValue ARE_CLIENTS_TRUSTED;

    public static BooleanValue STABLE_FIRERATE;

    public static BooleanValue DO_BULLET_WALL_PENETRATION;

    public static IntValue CAN_CLIENT_SEE_ENTITY_STATUS;
    public static BooleanValue ANNOUNCE_KILL_TIPS;

    public AVAServerConfig build(Builder builder)
    {
        this.builder = builder;
        M67_EXPLOSIVE_DAMAGE = builder.defineInRange("m67_explosive_damage", 26.0D, 0.0D, MAX_EXPLOSIVE_DAMAGE);
        M67_EXPLOSIVE_RANGE = builder.defineInRange("m67_explosive_range", 3.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        makeSpace();
        M116A1_FLASH_RANGE = builder.defineInRange("m116a1_flash_range", 5.0D, 0.0D, MAX_FLASH_RANGE);
        M116A1_FLASH_DURATION = builder.defineInRange("m116a1_flash_duration", 80, 0, MAX_FLASH_DURATION);
        makeSpace();
        MK3A2_EXPLOSIVE_DAMAGE = builder.defineInRange("mk3a2_explosive_damage", 26.0D, 0.0D, MAX_EXPLOSIVE_DAMAGE);
        MK3A2_EXPLOSIVE_RANGE = builder.defineInRange("mk3a2_explosive_range", 2.0D, 0.0D, MAX_FLASH_RANGE);
        MK3A2_FLASH_RANGE = builder.defineInRange("mk3a2_flash_range", 2.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        MK3A2_FLASH_DURATION = builder.defineInRange("mk3a2_flash_duration", 60, 0, MAX_FLASH_DURATION);
        makeSpace();
        M202_ROCKET_EXPLOSIVE_RANGE = builder.defineInRange("m202_explosive_range", 6.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        makeSpace();
        GM94_GRENADE_EXPLOSIVE_RANGE = builder.defineInRange("gm94_explosive_range", 5.0D, 0.0D, MAX_EXPLOSIVE_RANGE);
        makeSpace();
        IS_AMMO_KIT_SUPPLIER_CRAFTABLE = builder.define("is_ammo_kit_supplier_craftable", false);
        EXPLOSION_DESTROYS_ENTITIES = builder.define("explosion_destroys_entities", false);
        builder.comment("0: None, 1: Glass Material Only, 2: All Blocks, 3: Glass Material Only By C4");
        EXPLOSION_DESTRUCTION_TYPE = builder.defineInRange("explosion_destruction_type", 0, 0, 3);
        builder.comment("Multiplier for the damage caused by all ava damage source");
        DAMAGE_MULTIPLIER_AGAINST_PLAYERS = builder.defineInRange("damage_multiplier_against_players", 1.0D, 0, 40.0D);
        DAMAGE_MULTIPLIER_AGAINST_OTHERS = builder.defineInRange("damage_multiplier_against_others", 1.0D, 0, 40.0D);
        SHOULD_GUARDS_SPAWN = builder.define("should_guards_spawn", false);
        GUARDS_SPAWN_CHANCE = builder.defineInRange("guards_spawn_chance", 50, 0, 100);
        makeSpace();
        MELEE_DESTROYABLE_BLOCK = builder.define("melee_destroyable_blocks", "");
        makeSpace(2, "https://docs.google.com/document/d/1950eqPMlePlyoeaFH1_MRRBRw5nishlXjR3h9FkDIhE/edit?usp=sharing", "Whether guns like m24 and mosin-nagant can be used");
        ENABLE_SNIPERS = builder.define("enable_snipers", true);
        builder.comment("Whether guns like sr-25 can be used (Deprecated, use command /ava setPlayMode instead!!!)");
        ENABLE_SEMI_SNIPERS = builder.define("enable_semi_snipers", true);
        builder.comment("Whether guns like mk20 can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_RIFLES = builder.define("enable_rifles", true);
        builder.comment("Whether guns like mp5k and x95r can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_SUB_MACHINEGUNS = builder.define("enable_sub_machineguns", true);
        builder.comment("Whether guns like remington870 can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_SHOTGUNS = builder.define("enable_shotguns", true);
        builder.comment("Whether guns like p226 can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_PISTOLS = builder.define("enable_pistols", true);
        builder.comment("Whether guns like Mauser C96 can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_AUTO_PISTOLS = builder.define("enable_auto_pistols", true);
        builder.comment("Whether projectiles like m67 and m18 can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_PROJECTILES = builder.define("enable_projectiles", true);
        builder.comment("Whether special weapons like gm94 and m202 can be used (Deprecated, use command /ava setPlayMode  instead!!!)");
        ENABLE_SPECIAL_WEAPONS = builder.define("enable_special_weapons", true);
        makeSpace(5, "Set both to true to enable competitive mode, please make sure you know what it is" +
                "before setting it to true");
        COMPETITIVE_MODE = builder.define("enable_competitive_mode", false);
        PRESET_WITH_PARACHUTE = builder.define("preset_with_parachute", false);
        makeSpace(3, "Disabled items. Format: m18_toxic, m202, gm94");
        DISABLED_WEAPONS = builder.define("disabled_weapons", "");
        makeSpace(3);
        FIELD_KNIFE_MOVEMENT_SPEED_BONUS = builder.defineInRange("field_knife_movement_speed_bonus", 0.0035D, 0.0D, 1.0D);
        makeSpace(2, "Whether there should be a delay while drawing a weapon");
        WEAPON_DRAWING_DELAY = builder.define("weapon_drawing_delay", true);

        makeSpace(2);
        SPAWN_RESTRICTION = builder.define("team_spawn_restriction", true);
        makeSpace(2);
        A_SITE_RADIUS = builder.defineInRange("a_site_plant_radius", 4, -1, 75);
        A_SITE_HEIGHT = builder.defineInRange("a_site_plant_height", 1, -1, 75);
        B_SITE_RADIUS = builder.defineInRange("b_site_plant_radius", 4, -1, 75);
        B_SITE_HEIGHT = builder.defineInRange("b_site_plant_height", 1, -1, 75);
        makeSpace(2);
        builder.comment("If the mastery system for guns is disabled, existing mastery will not be affected");
        DISABLE_MASTERY_SYSTEM = builder.define("disable_mastery_system", false);
        makeSpace(2);
        builder.comment("Whether restricted movement from AVA should be applied");
        AVA_RESTRICTED_MOVEMENT = builder.define("restricted_movement", false);
        builder.comment("Whether only 1 primary weapon is allowed for preset");
        PRESET_SINGLE_PRIMARY_WEAPON = builder.define("preset_single_primary_weapon", false);
        builder.comment("Whether the attachments are free to install");
        FREE_ATTACHMENTS = builder.define("free_attachments", false);
        builder.comment("The maximum number of the same type of entity can exist in a world, -1 indicates infinity as default");
        MAX_ENTITY_OF_SAME_TYPE = builder.defineInRange("max_entity_type", -1, -1, Integer.MAX_VALUE);
        builder.comment("Should A.V.A Structures generate");
        DO_STRUCTURES_GENERATE = builder.define("structure_generates", true);
        builder.comment("Expecting max player, this will effect the count of entity deployed by the ava deploy command, for example expecting 4 but there's only 2 players, then count is / 2. Set to -1 to disable difficulty scaling");
        EXPECTED_PLAYERS = builder.defineInRange("expecting_player_count", -1, 0, Integer.MAX_VALUE);
        builder.comment("Headshot damage multiplier");
        HEADSHOT_DAMAGE_SCALE = builder.defineInRange("headshot_damage_scale", 2.5D, 0.0D, 100.0D);
//        builder.comment("Are clients trusted... ignores server-side firerate checks, allows better experiences on clients");
//        ARE_CLIENTS_TRUSTED = builder.define("Are remote players trusted", true);
        builder.comment("Whether firerate is stable, if enabled the firerate is stabled but may feel slower on some weapons");
        STABLE_FIRERATE = builder.define("stable_firerate", false);
        builder.comment("Does bullet shoot through walls");
        DO_BULLET_WALL_PENETRATION = builder.define("do_bullet_penetrate_walls", true);
        builder.comment("If client can choose to show entity status, 0 for showing none, 1 for showing creatures, 2 for showing ally, 3 for showing all");
        CAN_CLIENT_SEE_ENTITY_STATUS = builder.defineInRange("can_client_see_entity_status", 3, 0, 3);
        builder.comment("Whether to announce kill tips");
        ANNOUNCE_KILL_TIPS = builder.define("announce_kill_tips", true);
        return this;
    }

    public static boolean isCompetitiveModeActivated()
    {
        try
        {
            return COMPETITIVE_MODE.get();
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
