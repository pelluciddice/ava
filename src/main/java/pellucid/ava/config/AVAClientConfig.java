package pellucid.ava.config;

import pellucid.ava.util.Lazy;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static net.neoforged.neoforge.common.ModConfigSpec.*;

public class AVAClientConfig extends AVAConfig
{
    public static BooleanValue FAST_ASSETS;
    public static BooleanValue AI_FAST_ASSETS;
    public static BooleanValue GUI_FAST_ASSETS;

    public static BooleanValue SHOULD_RENDER_CROSS_HAIR;
    public static IntValue RED;
    public static IntValue GREEN;
    public static IntValue BLUE;
    public static IntValue TRANSPARENCY;
    public static IntValue CENTRE_DOT_SIZE;
    public static IntValue LINE_THICKNESS;
    public static IntValue LINE_LENGTH;
    public static IntValue OFFSET_SCALE;

    public static BooleanValue FP_BOBBING;

    public static ConfigValue<String> PRIMARY_WEAPON_1_1;
    public static ConfigValue<String> PRIMARY_WEAPON_1_2;
    public static ConfigValue<String> SECONDARY_WEAPON_1;
    public static ConfigValue<String> MELEE_WEAPON_1;
    public static ConfigValue<String> PROJECTILE_1_1;
    public static ConfigValue<String> PROJECTILE_1_2;
    public static ConfigValue<String> PROJECTILE_1_3;
    public static ConfigValue<String> SPECIAL_WEAPON_1;

    public static ConfigValue<String> PRIMARY_WEAPON_2_1;
    public static ConfigValue<String> PRIMARY_WEAPON_2_2;
    public static ConfigValue<String> SECONDARY_WEAPON_2;
    public static ConfigValue<String> MELEE_WEAPON_2;
    public static ConfigValue<String> PROJECTILE_2_1;
    public static ConfigValue<String> PROJECTILE_2_2;
    public static ConfigValue<String> PROJECTILE_2_3;
    public static ConfigValue<String> SPECIAL_WEAPON_2;

    public static ConfigValue<String> PRIMARY_WEAPON_3_1;
    public static ConfigValue<String> PRIMARY_WEAPON_3_2;
    public static ConfigValue<String> SECONDARY_WEAPON_3;
    public static ConfigValue<String> MELEE_WEAPON_3;
    public static ConfigValue<String> PROJECTILE_3_1;
    public static ConfigValue<String> PROJECTILE_3_2;
    public static ConfigValue<String> PROJECTILE_3_3;
    public static ConfigValue<String> SPECIAL_WEAPON_3;

    public static BooleanValue ENABLE_QUICK_SWAP_HOTKEY;
    public static BooleanValue ENABLE_PRESET_HOTKEY;
    public static BooleanValue ENABLE_RADIO_HOTKEY;
    public static BooleanValue ENABLE_PING_HOTKEY;
    public static BooleanValue ENABLE_TAB_HOTKEY;

    public static BooleanValue ENABLE_PROJECTILE_INDICATOR;
    public static BooleanValue ENABLE_BIO_INDICATOR;

    public static BooleanValue ENABLE_COMPLICATED_ARMOUR_MODEL;

    public static BooleanValue ENABLE_KILL_TIP;
    public static BooleanValue ENABLE_BULLET_HOLE_EFFECT;
    public static BooleanValue ENABLE_BLOOD_EFFECT;
    public static BooleanValue ENABLE_BULLET_TRAIL_EFFECT;
    public static BooleanValue ENABLE_PROJECTILE_TRAIL_EFFECT;
    @Deprecated
    public static BooleanValue ENABLE_ALLY_STATUS_EFFECT;
    public static boolean showAllyStatus()
    {
        return ENABLE_ALLY_STATUS_EFFECT.get() && AVAServerConfig.CAN_CLIENT_SEE_ENTITY_STATUS.get() > 1;
    }

    public static BooleanValue ENABLE_KILL_EFFECT;
    public static BooleanValue ENABLE_HIT_EFFECT;
    public static BooleanValue ENABLE_LENS_TINT;
    @Deprecated
    public static BooleanValue ENABLE_CREATURE_STATUS;
    public static boolean showCreatureStatus()
    {
        return ENABLE_CREATURE_STATUS.get() && (AVAServerConfig.CAN_CLIENT_SEE_ENTITY_STATUS.get() == 1 || AVAServerConfig.CAN_CLIENT_SEE_ENTITY_STATUS.get() == 3);
    }

    public static BooleanValue ENABLE_PASSIVE_RADIO_VOICE;

    public static BooleanValue ENABLE_ALTERNATED_THIRD_PERSON_MODEL;

    public static BooleanValue REMOVE_HURT_CAMERA_TILT_ON_FULLY_EQUIPPED;

    public static BooleanValue REJECT_SERVER_DISPLAY_IMAGE;

    public static DoubleValue TEST;

    public static DoubleValue MAGNIF_1;
    public static DoubleValue MAGNIF_2;
    public static DoubleValue MAGNIF_3;
    public static DoubleValue MAGNIF_4;
    public static DoubleValue MAGNIF_5;
    public static DoubleValue MAGNIF_6;
    public static DoubleValue MAGNIF_7;
    public static DoubleValue MAGNIF_8;

    public static final Lazy<DoubleValue[]> MAGNIFS = Lazy.of(() -> new DoubleValue[]{
            MAGNIF_1, MAGNIF_2, MAGNIF_3, MAGNIF_4, MAGNIF_5, MAGNIF_6, MAGNIF_7, MAGNIF_8
    });

    public static final Map<Float, Supplier<Float>> MAGNIFS_MAP = new HashMap<>() {{
        put(0.1F, () -> MAGNIF_1.get().floatValue());
        put(0.2F, () -> MAGNIF_2.get().floatValue());
        put(0.3F, () -> MAGNIF_3.get().floatValue());
        put(0.4F, () -> MAGNIF_4.get().floatValue());
        put(0.5F, () -> MAGNIF_5.get().floatValue());
        put(0.6F, () -> MAGNIF_6.get().floatValue());
        put(0.7F, () -> MAGNIF_7.get().floatValue());
        put(0.8F, () -> MAGNIF_8.get().floatValue());
    }};

    public AVAClientConfig build(Builder builder)
    {
        this.builder = builder;

        this.builder.push("Fast Assets");
        FAST_ASSETS = builder.define("Fast or Fancy Assets (Fast -> Better Performance)", false);
        AI_FAST_ASSETS = builder.define("Should AI weapon models use fast assets", true);
        GUI_FAST_ASSETS = builder.define("Should GUI weapon models use image assets", true);
        this.builder.pop();

        this.builder.push("Crosshair");
        SHOULD_RENDER_CROSS_HAIR = builder.define("Enable", true);
        RED = builder.defineInRange("Red", 40, 0, 255);
        GREEN = builder.defineInRange("Green", 245, 0, 255);
        BLUE = builder.defineInRange("Blue", 15, 0, 255);
        TRANSPARENCY = builder.defineInRange("Transparency", 100, 0, 100);
        CENTRE_DOT_SIZE = builder.defineInRange("Centre Dot Size", 5, 0, 100);
        LINE_THICKNESS = builder.defineInRange("Line Thickness", 5, 0, 100);
        LINE_LENGTH = builder.defineInRange("Line Length", 40, 0, 100);
        OFFSET_SCALE = builder.defineInRange("Offset Scale", 10, 0, 100);
        this.builder.pop();

        this.builder.push("Controls");
        ENABLE_QUICK_SWAP_HOTKEY = builder.define("Quick Swap", false);
        ENABLE_PRESET_HOTKEY = builder.define("Preset Hotkey", false);
        ENABLE_RADIO_HOTKEY = builder.define("Radio Hotkey", true);
        ENABLE_PING_HOTKEY = builder.define("Ping Hotkey", true);
        ENABLE_TAB_HOTKEY = builder.define("Tab Hotkey", true);
        MAGNIF_1 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.1", 1.0F, 0.0F, 2.0F);
        MAGNIF_2 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.2", 1.0F, 0.0F, 2.0F);
        MAGNIF_3 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.3", 1.0F, 0.0F, 2.0F);
        MAGNIF_4 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.4", 1.0F, 0.0F, 2.0F);
        MAGNIF_5 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.5", 1.0F, 0.0F, 2.0F);
        MAGNIF_6 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.6", 1.0F, 0.0F, 2.0F);
        MAGNIF_7 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.7", 1.0F, 0.0F, 2.0F);
        MAGNIF_8 = builder.defineInRange("Mouse Sensitivity Scale of Magnification of x1.8", 1.0F, 0.0F, 2.0F);
        this.builder.pop();

        this.builder.push("HUD");
        ENABLE_PROJECTILE_INDICATOR = builder.define("Projectile Indicator", true);
        ENABLE_BIO_INDICATOR = builder.define("Biosensor (Zombie)", false);
        ENABLE_KILL_TIP = builder.define("Display Kill Tip", true);
        REMOVE_HURT_CAMERA_TILT_ON_FULLY_EQUIPPED = builder.define("Should the camera tile on-hit be removed when having AVA armour fully equipped", true);
        REJECT_SERVER_DISPLAY_IMAGE = builder.define("Rejects the image data sent from the server", false);
        FP_BOBBING = builder.define("First Person Bobbing", true);
        this.builder.pop();

        this.builder.push("Models");
        ENABLE_COMPLICATED_ARMOUR_MODEL = builder.define("Whether fancy ava armour model is used", true);
        ENABLE_ALTERNATED_THIRD_PERSON_MODEL = builder.define("Should player model be modified, turn off if bugged with other mods", true);
        this.builder.pop();

        this.builder.push("Environment Effects");
        ENABLE_BULLET_HOLE_EFFECT = builder.define("Use bullet hole effect", true);
        ENABLE_BLOOD_EFFECT = builder.define("Use blood effect", false);
        ENABLE_BULLET_TRAIL_EFFECT = builder.define("Use bullet trail effect", true);
        ENABLE_PROJECTILE_TRAIL_EFFECT = builder.define("Use projectile trail effect", true);
        ENABLE_ALLY_STATUS_EFFECT = builder.define("Show ally status", true);
        ENABLE_KILL_EFFECT = builder.define("Use kill effect", true);
        ENABLE_HIT_EFFECT = builder.define("Use hit effect", true);
        ENABLE_LENS_TINT = builder.define("Use lens tint", true);
        ENABLE_CREATURE_STATUS = builder.define("Show other creature status", false);
        ENABLE_PASSIVE_RADIO_VOICE = builder.define("Use passive radio sound effects", true);
        this.builder.pop();

        this.builder.push("No! :(");
        TEST = builder.defineInRange("Test value, should not be changed", 0.99F, -10000.0F, Double.MAX_VALUE);
        this.builder.pop();

        return this;
    }
}
