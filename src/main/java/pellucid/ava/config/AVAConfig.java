package pellucid.ava.config;

import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import pellucid.ava.AVA;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVACommonUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AVAConfig
{
    protected static final double MAX_EXPLOSIVE_DAMAGE = 100.0D;
    protected static final double MAX_EXPLOSIVE_RANGE = 30.0D;
    protected static final double MAX_FLASH_RANGE = 50.0D;
    protected static final int MAX_FLASH_DURATION = 200;
    protected ModConfigSpec.Builder builder;

    protected void makeSpace()
    {
        makeSpace(2);
    }

    protected void makeSpace(int lines, String... extra)
    {
        List<String> spaces = new ArrayList<>();
        for (int i=0;i<lines;i++)
            spaces.add("-");
        spaces.addAll(Arrays.asList(extra));
        builder.comment(spaces.toArray(new String[0]));
    }

    private static final List<ModConfigSpec.ConfigValue<?>> SERVER_FIELDS = new ArrayList<>();
    private static final List<ModConfigSpec.ConfigValue<?>> CLIENT_FIELDS = new ArrayList<>();

    public static final AVAServerConfig SERVER_INSTANCE = new AVAServerConfig();
    public static final AVAClientConfig CLIENT_INSTANCE = new AVAClientConfig();

    public static List<ModConfigSpec.ConfigValue<?>> getServerFields()
    {
        if (SERVER_FIELDS.isEmpty())
            SERVER_FIELDS.addAll(getherFieldsFor(AVAServerConfig.class, SERVER_INSTANCE));
        return SERVER_FIELDS;
    }

    public static AVAClientConfig getClientInstance()
    {
        if (CLIENT_FIELDS.isEmpty())
            CLIENT_FIELDS.addAll(getherFieldsFor(AVAClientConfig.class, CLIENT_INSTANCE));
        return CLIENT_INSTANCE;
    }

    public static void onConfigReload(ModConfigEvent.Reloading event)
    {
        ModConfig config = event.getConfig();
        if (config.getSpec() == AVAConfigs.SERVER_SPEC)
        {
            if (ServerLifecycleHooks.getCurrentServer() != null && !ServerLifecycleHooks.getCurrentServer().isSingleplayer())
            {
                AVA.LOGGER.info("Server config changed, notifying clients.");
                DataToClientMessage.serverConfig(null);
            }
        }
        else if (config.getSpec() == AVAConfigs.CLIENT_SPEC)
        {
        }
    }

    private static <I> List<ModConfigSpec.ConfigValue<?>> getherFieldsFor(Class<?> clazz, I instance)
    {
        Map<String, ModConfigSpec.ConfigValue<?>> map = new HashMap<>();
        try
        {
            for (Field field : clazz.getFields())
            {
                if (!Modifier.isFinal(field.getModifiers()))
                {
                    Object obj = field.get(instance);
                    if (ModConfigSpec.ConfigValue.class.isAssignableFrom(obj.getClass()))
                        map.put(field.getName(), (ModConfigSpec.ConfigValue<?>) field.get(instance));
                }
            }
        }
        catch (Exception e)
        {
            AVA.LOGGER.error("Error parsing config! " + Arrays.toString(e.getStackTrace()));
        }
        return AVACommonUtil.collect(map.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toList()), Map.Entry::getValue);
    }
}
