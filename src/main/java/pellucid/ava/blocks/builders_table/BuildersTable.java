package pellucid.ava.blocks.builders_table;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.DistExecutor;
import pellucid.ava.util.AVAClientUtil;

public class BuildersTable extends Block
{
    public BuildersTable(Properties properties)
    {
        super(properties);
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level world, BlockPos pos, Player player, BlockHitResult ray)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> openScreen(player.getInventory()));
        return InteractionResult.SUCCESS;
    }

    @OnlyIn(Dist.CLIENT)
    private void openScreen(Inventory inventory)
    {
        AVAClientUtil.setDeferredScreen(new BuildersGUI(inventory));
    }
}