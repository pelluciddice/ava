package pellucid.ava.blocks.builders_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.client.components.ItemWidgets;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.recipes.ItemRecipeHolder;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Loop;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class BuildersGUI extends Screen
{
    private static final ResourceLocation BUILDERS_GUI = new ResourceLocation("ava:textures/gui/builders_table.png");
    private final int imageWidth;
    private final int imageHeight;
    protected int leftPos;
    protected int topPos;
    private Loop selected_page;
    private ItemLike selected_item;
    private final Inventory inventory;
    private final List<ItemWidgets.ItemButton<BuildersGUI>> item_buttons = new ArrayList<>();
    private ItemWidgets.DisplayItemWidget<BuildersGUI> selected_item_widget;
    private final List<ItemWidgets.DisplayItemWidget<BuildersGUI>> ingredients_widgets = new ArrayList<>();
    private Recipe recipe;

    protected BuildersGUI(Inventory inventory)
    {
        super(Component.empty());
        this.inventory = inventory;
        this.imageWidth = 230;
        this.imageHeight = 219;
    }

    public void setSelectedPage(Consumer<Loop> alt)
    {
        alt.accept(this.selected_page);
        updateItems();
    }

    protected void clearAll()
    {
        clearWidgets();
        renderables.clear();
        children().clear();
        item_buttons.clear();
        ingredients_widgets.clear();
        recipe = null;
        selected_item_widget = null;
        selected_item = Items.AIR;
        selected_page = new Loop(blocks().size() / 110);
    }

    private List<ItemLike> blocks()
    {
        return AVACommonUtil.collect(AVABuildingBlocks.RECIPES, ItemRecipeHolder::getItem);
    }

    @Override
    protected void init()
    {
        clearAll();
        this.leftPos = (this.width - this.imageWidth) / 2;
        this.topPos = (this.height - this.imageHeight) / 2;
        int x = 13;
        int y = 9;
        for (int r = 0; r < 11; r++)
            for (int c = 0; c < 10; c++)
                item_buttons.add(addRenderableWidget(ItemWidgets.select(this, null, leftPos + x + c * 18, topPos + y + r * 18, (button) -> {
                    setSelectedItem(button.getItem());
                    item_buttons.forEach((b) -> b.setSelected(false));
                    button.setSelected(true);
                })));
        selected_item_widget = addRenderableWidget(ItemWidgets.display(this, null, leftPos + 198, topPos + 11));
        x = 200;
        y = 35;
        for (int i = 0; i < 4; i++)
            ingredients_widgets.add(addRenderableWidget(ItemWidgets.display(this, null, leftPos + x, topPos + y + i * 18)));
        setSelectedItem(Items.AIR);
        addRenderableWidget(new PageButton(leftPos + 196, topPos + 176, true));
        addRenderableWidget(new PageButton(leftPos + 196, topPos + 190, false));
        addRenderableWidget(new CraftButton(leftPos + 196, topPos + 110, 1));
        addRenderableWidget(new CraftButton(leftPos + 196, topPos + 121, 10));
        addRenderableWidget(new CraftButton(leftPos + 196, topPos + 132, 32));
        addRenderableWidget(new CraftButton(leftPos + 196, topPos + 143, 64));
        updateItems();
    }

    public void setSelectedItem(ItemLike item)
    {
        this.selected_item = item;
        Optional<ItemRecipeHolder> optional = AVABuildingBlocks.RECIPES.stream().filter((holder) -> holder.getItem() == item.asItem()).findFirst();
        recipe = optional.map(ItemRecipeHolder::getRecipe).orElse(null);
        selected_item_widget.setItem(recipe == null ? null : new ItemStack(item.asItem(), recipe.getResultCount()));
        List<Ingredient> ingredients = recipe == null ? null : recipe.getIngredients();
        for (int i = 0; i < 4; i++)
            ingredients_widgets.get(i).setItem(recipe == null || ingredients == null || ingredients.size() <= i ? ItemStack.EMPTY : new ItemStack(Recipe.pickFromIngredient(ingredients.get(i), Items.AIR), recipe.getCount(ingredients.get(i))));
    }

    private void updateItems()
    {
        List<ItemLike> blocks = blocks();
        int from = selected_page.current() * 110;
        for (int i = 0; i < 110; i++)
        {
            int index = from + i;
            ItemLike block = Blocks.AIR;
            if (index < blocks.size())
                block = blocks.get(index);
            item_buttons.get(i).setItem(block.asItem());
        }
    }

    @Override
    public void renderBackground(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.renderBackground(stack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        int x = (this.width - this.imageWidth) / 2;
        int y = (this.height - this.imageHeight) / 2;
        stack.blit(BUILDERS_GUI, x, y, 0, 0, this.imageWidth, this.imageHeight);
    }

    @Override
    public void render(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(stack, mouseX, mouseY, partialTicks);
        stack.drawCenteredString(font, selected_page.current() + 1 + "/" + selected_page.size(), leftPos + 206, topPos + 163, AVAConstants.AVA_HUD_TEXT_ORANGE);
    }

    private void craft(int count)
    {
        if (selected_item != null && recipe != null && recipe.canCraft(inventory.player, selected_item.asItem(), count))
            DataToServerMessage.builderCrafting(selected_item.asItem(), count);
    }

    class CraftButton extends AbstractButton
    {
        private boolean selected;
        private final int count;

        public CraftButton(int xIn, int yIn, int count)
        {
            super(xIn, yIn, 22, 9, Component.empty());
            this.count = count;
        }

        @Override
        public void onPress()
        {
            if (this.active)
            {
                BuildersGUI.this.craft(count);
                this.selected = true;
            }
        }

        @Override
        public void onRelease(double p_onRelease_1_, double p_onRelease_3_)
        {
            if (this.active)
                this.selected = false;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            this.active = recipe != null && selected_item != null && recipe.canCraft(inventory.player, selected_item.asItem(), count);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            int x = 0;
            if (this.active && this.isHoveredOrFocused())
                x += this.width;
            if (this.active && this.selected)
                x += this.width;
            if (!this.active)
                x += this.width * 2;
            stack.blit(BUILDERS_GUI, this.getX(), this.getY(), x, 219, this.width, this.height);
            stack.drawCenteredString(BuildersGUI.this.font, Component.literal("x" + count), this.getX() + this.width / 2, this.getY(), this.active ? 54783 : 16711680);
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {

        }
    }

    class PageButton extends AbstractButton
    {
        private final boolean up;

        public PageButton(int xIn, int yIn, boolean up)
        {
            super(xIn, yIn, 22, 11, Component.empty());
            this.up = up;
        }

        @Override
        public void onPress()
        {
            BuildersGUI.this.setSelectedPage((page) -> page.move(!up));
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            int x = this.up ? 132 : 66;
            if (this.isHoveredOrFocused())
                x += this.width;
            if (this.isHoveredOrFocused() && AVAClientUtil.leftMouseDown())
                x += this.width;
            stack.blit(BUILDERS_GUI, this.getX(), this.getY(), x, 219, this.width, this.height);
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {

        }
    }
}
