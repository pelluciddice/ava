package pellucid.ava.blocks;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.phys.shapes.VoxelShape;

import java.util.function.Supplier;

public class FloorParentBlock extends FloorBlock implements IParentedBlock
{
    private final Block parent;

    public FloorParentBlock(Properties properties, Block parent, Supplier<VoxelShape> topShape, Supplier<VoxelShape> bottomShape)
    {
        super(properties, topShape, bottomShape);
        this.parent = parent;
    }

    @Override
    public Block getParent()
    {
        return parent;
    }

    @Override
    public Block getBlock()
    {
        return this;
    }
}
