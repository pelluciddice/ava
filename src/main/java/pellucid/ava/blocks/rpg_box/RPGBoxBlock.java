package pellucid.ava.blocks.rpg_box;

import com.mojang.serialization.MapCodec;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.IInteractable;
import pellucid.ava.blocks.ITickableTileEntity;

public class RPGBoxBlock extends Block implements EntityBlock, IInteractable<BlockHitResult>
{
    public static final MapCodec<RPGBoxBlock> CODEC = simpleCodec((p) -> new RPGBoxBlock());

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final EnumProperty<DoubleBlockHalf> HALF = BlockStateProperties.DOUBLE_BLOCK_HALF;
    public static final BooleanProperty OPEN = BlockStateProperties.OPEN;

    public RPGBoxBlock()
    {
        super(Properties.of().sound(SoundType.METAL).mapColor(DyeColor.LIME));
        this.registerDefaultState(stateDefinition.any().setValue(HALF, DoubleBlockHalf.LOWER).setValue(FACING, Direction.NORTH).setValue(OPEN, false));
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context)
    {
        BlockPos pos = context.getClickedPos();
        Level world = context.getLevel();
        if (pos.getY() < world.getMaxBuildHeight() - 1 && world.getBlockState(pos.above()).canBeReplaced(context))
            return defaultBlockState().setValue(FACING, context.getHorizontalDirection());
        return null;
    }

    @Override
    public void setPlacedBy(Level world, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack)
    {
        world.setBlock(pos.above(), state.setValue(HALF, DoubleBlockHalf.UPPER), 3);
    }

    @Override
    public boolean canSurvive(BlockState state, LevelReader world, BlockPos pos)
    {
        if (state.getValue(HALF) == DoubleBlockHalf.LOWER)
            return super.canSurvive(state, world, pos);
        BlockState below = world.getBlockState(pos.below());
        return below.getBlock() == this && below.getValue(HALF) == DoubleBlockHalf.LOWER;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> p_49915_)
    {
        p_49915_.add(FACING, HALF, OPEN);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
    {
        return state.getValue(HALF) == DoubleBlockHalf.LOWER ? new RPGBoxTE(pos, state) : null;
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> p_153214_)
    {
        return ITickableTileEntity::ticker;
    }

    @Override
    public int getDuration()
    {
        return 80;
    }

    @Override
    public void interact(Level world, BlockHitResult result, @Nullable BlockPos pos, @Nullable Vec3 vector3d, LivingEntity interacter)
    {
        pos = result.getBlockPos();
        BlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof RPGBoxBlock && state.getValue(HALF) == DoubleBlockHalf.UPPER)
            pos = result.getBlockPos().below();
        BlockEntity te = world.getBlockEntity(pos);
        if (te instanceof RPGBoxTE box)
        {
            if (!box.isOpened())
                box.open();
        }
    }

    private static final VoxelShape SHAPE = box(0.01F, 0, 0.01F, 15.99F, 16, 15.99F);

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter p_60556_, BlockPos p_60557_, CollisionContext p_60558_)
    {
        return SHAPE;
    }

    @Override
    public boolean canInteract(BlockHitResult result, LivingEntity interactor, BlockPos pos)
    {
        return interactor.level().getBlockState(result.getBlockPos()).getBlock() instanceof RPGBoxBlock && !interactor.level().getBlockState(result.getBlockPos()).getValue(OPEN) && interactor.level().getBlockState(result.getBlockPos()).getValue(FACING).getOpposite() == result.getDirection();
    }

    @Override
    public String getInteractableTip()
    {
        return "ava.interaction.rpg_box";
    }

    @Override
    public boolean shouldDrawTipWrench()
    {
        return true;
    }
}
