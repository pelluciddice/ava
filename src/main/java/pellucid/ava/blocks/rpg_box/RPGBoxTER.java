package pellucid.ava.blocks.rpg_box;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.blocks.DirectionalShapedBlock;
import pellucid.ava.util.Lazy;

public class RPGBoxTER implements BlockEntityRenderer<RPGBoxTE>
{
    public static final Lazy<ItemStack> RPG_STACK = Lazy.of(RPGBoxTE::createRPGStack);
    private final ItemRenderer itemRenderer;
    public RPGBoxTER(BlockEntityRendererProvider.Context context)
    {
        this.itemRenderer = context.getItemRenderer();
    }

    @Override
    public void render(RPGBoxTE te, float p_112308_, PoseStack stack, MultiBufferSource p_112310_, int packedLightIn, int overlay)
    {
        if (!te.hasWeapon)
            return;
        Direction d = te.getBlockState().getValue(DirectionalShapedBlock.FACING);
        Vec3i vec = d.getOpposite().getNormal();
        stack.pushPose();
        stack.translate(vec.getX() * 0.075F, 0.0F, vec.getZ() * 0.075F);
        stack.translate(0.5F, 0.65F, 0.5F);
        if (d.getAxis() == Direction.Axis.X)
        {
            stack.mulPose(Axis.ZP.rotationDegrees(d == Direction.WEST ? 22.5F : -22.5F));
            stack.mulPose(Axis.XP.rotationDegrees(90));
        }
        else
        {
            stack.mulPose(Axis.XP.rotationDegrees(d == Direction.SOUTH ? 22.5F : -22.5F));
            stack.mulPose(Axis.ZP.rotationDegrees(90));
        }
        stack.mulPose(Axis.YP.rotationDegrees(d.toYRot()));
//        stack.translate(0.0F, 0.0F, d.getAxis() == Direction.Axis.X ? -0.6F : 0.6F);
        this.itemRenderer.renderStatic(RPG_STACK.get(), ItemDisplayContext.FIXED, packedLightIn, overlay, stack, p_112310_, Minecraft.getInstance().level, -1);
        stack.popPose();
    }
}
