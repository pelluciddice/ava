package pellucid.ava.blocks.rpg_box;

import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.DataTypes;

public class RPGBoxTE extends BlockEntity implements ITickableTileEntity
{
    public int openingTime = 0;
    public boolean hasWeapon = true;

    public RPGBoxTE(BlockPos pos, BlockState state)
    {
        super(AVATEContainers.RPG_BOX_TE.get(), pos, state);
    }

    @Override
    protected void saveAdditional(CompoundTag compound, HolderLookup.Provider lookup)
    {
        super.saveAdditional(compound, lookup);
        DataTypes.INT.write(compound, "openingTime", openingTime);
        DataTypes.BOOLEAN.write(compound, "hasWeapon", hasWeapon);
    }

    @Override
    public CompoundTag getUpdateTag(HolderLookup.Provider lookup)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "openingTime", openingTime);
        DataTypes.BOOLEAN.write(compound, "hasWeapon", hasWeapon);
        return compound;
    }

    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt, HolderLookup.Provider lookup)
    {
        handleUpdateTag(pkt.getTag(), lookup);
    }

    @Override
    public void handleUpdateTag(CompoundTag tag, HolderLookup.Provider lookup)
    {
        openingTime = DataTypes.INT.read(tag, "openingTime");
        hasWeapon = DataTypes.BOOLEAN.read(tag, "hasWeapon");
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket()
    {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    protected void notifyClient()
    {
        setChanged();
        getLevel().sendBlockUpdated(this.getBlockPos(), this.getBlockState(), this.getBlockState(), 3);
    }

    @Override
    public void loadAdditional(CompoundTag compound, HolderLookup.Provider lookup)
    {
        super.loadAdditional(compound, lookup);
        openingTime = DataTypes.INT.read(compound, "openingTime");
        hasWeapon = DataTypes.BOOLEAN.read(compound, "hasWeapon");
        notifyClient();
    }

    public static ItemStack createRPGStack()
    {
        ItemStack stack = new ItemStack(SpecialWeapons.RPG7.get());
        ((AVAItemGun) stack.getItem()).forceReload(stack);
        stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 2);
        return stack;
    }

    public void pickUp()
    {
        hasWeapon = false;
        notifyClient();
    }

    public void open()
    {
        hasWeapon = true;
        setState(true);
    }

    public void close()
    {
        setState(false);
    }

    public boolean isOpened()
    {
        return getBlockState().getValue(RPGBoxBlock.OPEN);
    }

    private void setState(boolean open)
    {
        if (isOpened() == open || level.isClientSide())
            return;
        level.setBlock(worldPosition, level.getBlockState(worldPosition).setValue(RPGBoxBlock.OPEN, open), 3);
        if (level.getBlockState(worldPosition.above()).getBlock() instanceof RPGBoxBlock)
            level.setBlock(worldPosition.above(), level.getBlockState(worldPosition.above()).setValue(RPGBoxBlock.OPEN, open), 3);
        if (open)
        {
            openingTime = 100;
            level.playSound(null, worldPosition, AVASounds.RPG_BOX_OPEN.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
        }
        else
            level.playSound(null, worldPosition, AVASounds.RPG_BOX_CLOSE.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
        notifyClient();
    }

    @Override
    public void tick()
    {
        if (level != null)
        {
            if (openingTime > 0)
                openingTime--;
            if (openingTime <= 0 && !level.isClientSide() && isOpened())
                close();
        }
    }
}
