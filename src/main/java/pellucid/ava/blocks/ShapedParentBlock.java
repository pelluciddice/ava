package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import pellucid.ava.util.AVACommonUtil;

public class ShapedParentBlock extends ParentBlock
{
    private final VoxelShape shape;
    public ShapedParentBlock(Properties properties, Block parent, VoxelShape... shapes)
    {
        super(properties, parent);
        this.shape = AVACommonUtil.mergeShapes(shapes);
    }

    @Override
    public VoxelShape getShape(BlockState p_60555_, BlockGetter p_60556_, BlockPos p_60557_, CollisionContext p_60558_)
    {
        return shape;
    }
}
