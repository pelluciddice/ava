package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;

import javax.annotation.Nullable;

public class TestBlock extends Block implements IInteractable
{
    public TestBlock()
    {
        super(Properties.of().sound(SoundType.METAL));
    }

    @Override
    public int getDuration()
    {
        return 200;
    }

    @Override
    public void interact(Level world, HitResult result, @Nullable BlockPos pos, @Nullable Vec3 vector3d, LivingEntity interacter)
    {

    }

    @Override
    public boolean canInteract(HitResult result, LivingEntity interactor, BlockPos pos)
    {
        return true;
    }
}
