package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import pellucid.ava.util.AVACommonUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class DirectionalShapedBlock extends Block
{
    public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;
    protected final Map<Direction, VoxelShape> shapes = new HashMap<>();

    public DirectionalShapedBlock(Properties properties, double x, double y, double z, double x2, double y2, double z2)
    {
        this(properties, () -> AVACommonUtil.boxShapeShapes(x, y, z, x2, y2, z2));
    }

    public DirectionalShapedBlock(Properties properties, Supplier<VoxelShape> shapeProvider)
    {
        this(properties);
        AVACommonUtil.putDirectionalShapes(shapes, shapeProvider.get());
    }

    public DirectionalShapedBlock(Properties properties)
    {
        super(properties);
        registerDefaultState(defaultBlockState().setValue(FACING, Direction.NORTH));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rotation)
    {
        return state.setValue(FACING, rotation.rotate(state.getValue(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror)
    {
        return state.rotate(mirror.getRotation(state.getValue(FACING)));
    }

    @Override
    public VoxelShape getCollisionShape(BlockState p_60572_, BlockGetter p_60573_, BlockPos p_60574_, CollisionContext p_60575_)
    {
        return getShape(p_60572_, p_60572_.getValue(FACING));
    }

    @Override
    public VoxelShape getVisualShape(BlockState p_60479_, BlockGetter p_60480_, BlockPos p_60481_, CollisionContext p_60482_)
    {
        return getShape(p_60479_, p_60479_.getValue(FACING));
    }

    @Override
    public VoxelShape getShape(BlockState p_60555_, BlockGetter p_60556_, BlockPos p_60557_, CollisionContext p_60558_)
    {
        return getShape(p_60555_, p_60555_.getValue(FACING));
    }

    public VoxelShape getShape(BlockState state, Direction direction)
    {
        return shapes.get(direction);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> p_49915_)
    {
        p_49915_.add(FACING);
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext p_49820_)
    {
        return defaultBlockState().setValue(FACING, p_49820_.getHorizontalDirection());
    }
}
