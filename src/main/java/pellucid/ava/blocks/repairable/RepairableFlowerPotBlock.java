package pellucid.ava.blocks.repairable;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.ITickableTileEntity;

import javax.annotation.Nullable;

import static pellucid.ava.blocks.repairable.RepairableTileEntity.DESTRUCTED;

public class RepairableFlowerPotBlock extends FlowerPotBlock implements EntityBlock
{
    public RepairableFlowerPotBlock()
    {
        super(null, () -> Blocks.AIR, BlockBehaviour.Properties.of().sound(SoundType.DECORATED_POT).instabreak().noOcclusion());
        registerDefaultState(defaultBlockState().setValue(DESTRUCTED, false));
        AVABlocks.REPAIRABLES.add(() -> this);
    }

    @Override
    public InteractionResult useWithoutItem(BlockState p_53540_, Level p_53541_, BlockPos p_53542_, Player p_53543_, BlockHitResult p_53545_)
    {
        return InteractionResult.CONSUME;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter reader, BlockPos pos, CollisionContext context)
    {
        return state.getValue(DESTRUCTED) ? Shapes.empty() : super.getShape(state, reader, pos, context);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        builder.add(DESTRUCTED);
    }

    @Override
    public RenderShape getRenderShape(BlockState state)
    {
        return state.getValue(DESTRUCTED) ? RenderShape.INVISIBLE : super.getRenderShape(state);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos p_153215_, BlockState p_153216_)
    {
        return new RepairableTileEntity(p_153215_, p_153216_);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> p_153214_)
    {
        return ITickableTileEntity::ticker;
    }
}
