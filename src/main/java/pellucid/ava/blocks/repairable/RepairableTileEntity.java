package pellucid.ava.blocks.repairable;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.cap.AVAWorldData;

import javax.annotation.Nullable;

public class RepairableTileEntity extends BlockEntity implements ITickableTileEntity
{
    public static final BooleanProperty DESTRUCTED = BooleanProperty.create("destructed");

    public RepairableTileEntity(BlockPos pos, BlockState state)
    {
        super(AVATEContainers.REPAIRABLE_TE.get(), pos, state);
    }

    public void revive()
    {
        set(false);
    }

    public void destruct(@Nullable Player player)
    {
        set(true);
        if (level instanceof ServerLevel)
        {
            level.playSound(null, worldPosition, getBlockState().getSoundType().getBreakSound(), SoundSource.BLOCKS, 1.0F, 1.0F);
            getBlockState().getBlock().playerWillDestroy(level, getBlockPos(), getBlockState(), player);
        }
    }

    private void set(boolean value)
    {
        if (level != null)
            level.setBlockAndUpdate(worldPosition, getBlockState().setValue(DESTRUCTED, value));
    }

    public boolean destructed()
    {
        return level == null || level.getBlockState(worldPosition).getValue(DESTRUCTED);
    }

    @Override
    public void tick()
    {
        if (level instanceof ServerLevel && level.getGameTime() % 600 == 0)
        {
            AVAWorldData data = AVAWorldData.getInstance(level);
            if (!data.repairablePositions.contains(worldPosition))
                data.repairablePositions.add(worldPosition);
//            if (!WorldData.getCap(level).getRepairablePositions().contains(worldPosition))
//                WorldData.getCap(level).addRepairablePosition(worldPosition);
        }
    }
}
