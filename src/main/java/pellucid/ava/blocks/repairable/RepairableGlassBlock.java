package pellucid.ava.blocks.repairable;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.TransparentBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.cap.AVAWorldData;

import javax.annotation.Nullable;

import static pellucid.ava.blocks.repairable.RepairableTileEntity.DESTRUCTED;

public class RepairableGlassBlock extends TransparentBlock implements EntityBlock
{
    public RepairableGlassBlock()
    {
        super(Properties.of().sound(SoundType.GLASS)
                .strength(0.3F).sound(SoundType.GLASS)
                .noOcclusion()
                .isValidSpawn((b, b2, b3, e) -> false)
                .isRedstoneConductor((b, b2, b3) -> false)
                .isSuffocating((b, b2, b3) -> false)
                .isViewBlocking((b, b2, b3) -> false));
        registerDefaultState(defaultBlockState().setValue(DESTRUCTED, false));
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving)
    {
        if (!worldIn.isClientSide && state.getBlock() != newState.getBlock())
        {
//            WorldData.getCap(worldIn).getRepairablePositions().remove(pos);
            AVAWorldData.getInstance(worldIn).repairablePositions.remove(pos);
            worldIn.removeBlockEntity(pos);
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter reader, BlockPos pos, CollisionContext context)
    {
        return state.getValue(DESTRUCTED) ? Shapes.empty() : super.getShape(state, reader, pos, context);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        builder.add(DESTRUCTED);
    }

    @Override
    public RenderShape getRenderShape(BlockState state)
    {
        return state.getValue(DESTRUCTED) ? RenderShape.INVISIBLE : super.getRenderShape(state);
    }

    @Override
    public boolean skipRendering(BlockState state, BlockState neightbor, Direction direction)
    {
        return neightbor.is(this) && !neightbor.getValue(DESTRUCTED) && super.skipRendering(state, neightbor, direction);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos p_153215_, BlockState p_153216_)
    {
        return new RepairableTileEntity(p_153215_, p_153216_);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> p_153214_)
    {
        return ITickableTileEntity::ticker;
    }
}
