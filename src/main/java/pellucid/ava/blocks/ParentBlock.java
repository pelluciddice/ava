package pellucid.ava.blocks;

import net.minecraft.world.level.block.Block;

public class ParentBlock extends Block implements IParentedBlock
{
    private final Block parent;

    public ParentBlock(Properties properties, Block parent)
    {
        super(properties);
        this.parent = parent;
    }

    @Override
    public Block getParent()
    {
        return parent;
    }

    @Override
    public Block getBlock()
    {
        return this;
    }
}
