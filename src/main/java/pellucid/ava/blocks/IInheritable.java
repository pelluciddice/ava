package pellucid.ava.blocks;

import net.minecraft.nbt.CompoundTag;

public interface IInheritable
{
    CompoundTag drain(CompoundTag compound);

    void inherit(CompoundTag compound);
}
