package pellucid.ava.blocks;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.neoforged.neoforge.network.IContainerFactory;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.blocks.boost_block.BoostTileEntity;
import pellucid.ava.blocks.colouring_table.GunColouringTableContainer;
import pellucid.ava.blocks.colouring_table.GunColouringTableTE;
import pellucid.ava.blocks.command_executor_block.CommandExecutorTE;
import pellucid.ava.blocks.crafting_table.GunCraftingTableContainer;
import pellucid.ava.blocks.crafting_table.GunCraftingTableTE;
import pellucid.ava.blocks.explosive_barrel.ExplosiveBarrelTE;
import pellucid.ava.blocks.modifying_table.GunModifyingTableContainer;
import pellucid.ava.blocks.modifying_table.GunModifyingTableTE;
import pellucid.ava.blocks.preset_table.PresetTableTE;
import pellucid.ava.blocks.repairable.RepairableTileEntity;
import pellucid.ava.blocks.rpg_box.RPGBoxTE;
import pellucid.ava.items.weapon_chest.WeaponChestContainer;
import pellucid.ava.util.AVACommonUtil;

import java.util.HashSet;
import java.util.List;
import java.util.function.Supplier;

public class AVATEContainers
{
    public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITIES = DeferredRegister.create(BuiltInRegistries.BLOCK_ENTITY_TYPE, AVA.MODID);
    public static final DeferredRegister<MenuType<?>> CONTAINERS = DeferredRegister.create(BuiltInRegistries.MENU, AVA.MODID);


    public static final Supplier<MenuType<GunCraftingTableContainer>> GUN_CRAFTING_TABLE_CONTAINER = container("gun_crafting_table_container", GunCraftingTableContainer::new);
    public static final Supplier<BlockEntityType<GunCraftingTableTE>> GUN_CRAFTING_TABLE_TE = tileEntity("gun_crafting_table_te", GunCraftingTableTE::new, AVABlocks.GUN_CRAFTING_TABLE);

    public static final Supplier<MenuType<GunColouringTableContainer>> GUN_COLOURING_TABLE_CONTAINER = container("gun_colouring_table_container", GunColouringTableContainer::new);
    public static final Supplier<BlockEntityType<GunColouringTableTE>> GUN_COLOURING_TABLE_TE = tileEntity("gun_colouring_table_te", GunColouringTableTE::new, AVABlocks.GUN_COLOURING_TABLE);

    public static final Supplier<MenuType<GunModifyingTableContainer>> GUN_MODIFYING_TABLE_CONTAINER = container("gun_modifying_table_container", GunModifyingTableContainer::new);
    public static final Supplier<BlockEntityType<GunModifyingTableTE>> GUN_MODIFYING_TABLE_TE = tileEntity("gun_modifying_table_te", GunModifyingTableTE::new, AVABlocks.GUN_MODIFYING_TABLE);

    public static final Supplier<BlockEntityType<BoostTileEntity>> BOOST_TE = tileEntity("boost_te", BoostTileEntity::new, AVABlocks.ATTACK_DAMAGE_BOOST_BLOCK, AVABlocks.HEALTH_BOOST_BLOCK);
    public static final Supplier<BlockEntityType<ExplosiveBarrelTE>> EXPLOSIVE_BARREL_TE = tileEntity("explosive_barrel", ExplosiveBarrelTE::new, AVABlocks.EXPLOSIVE_BARREL);
    public static final Supplier<BlockEntityType<RepairableTileEntity>> REPAIRABLE_TE = BLOCK_ENTITIES.register("repairable_te", () -> new BlockEntityType<>(RepairableTileEntity::new, new HashSet<>(AVACommonUtil.collect(AVABlocks.REPAIRABLES, Supplier::get)), null));
    public static final Supplier<BlockEntityType<CommandExecutorTE>> COMMAND_EXECUTOR_TE = BLOCK_ENTITIES.register("command_executor_te", () -> BlockEntityType.Builder.of(CommandExecutorTE::new, AVABlocks.COMMAND_EXECUTOR.get()).build(null));
    public static final Supplier<BlockEntityType<PresetTableTE>> PRESET_TABLE_TE = BLOCK_ENTITIES.register("preset_table_te", () -> BlockEntityType.Builder.of(PresetTableTE::new, AVABlocks.PRESET_TABLE.get()).build(null));
    public static final Supplier<BlockEntityType<RPGBoxTE>> RPG_BOX_TE = BLOCK_ENTITIES.register("rpg_box_te", () -> BlockEntityType.Builder.of(RPGBoxTE::new, AVABlocks.RPG_BOX.get()).build(null));

    public static final Supplier<MenuType<WeaponChestContainer>> WEAPON_CHEST_CONTAINER = CONTAINERS.register("weapon_chest_container", () -> new MenuType<>((IContainerFactory<WeaponChestContainer>) (id, inv, buffer) -> new WeaponChestContainer(id, BuiltInRegistries.ITEM.get(new ResourceLocation(buffer.readUtf(32767)))), FeatureFlags.DEFAULT_FLAGS));


    private static <T extends BlockEntity> Supplier<BlockEntityType<T>> tileEntity(String name, BlockEntityType.BlockEntitySupplier<T> constructor, Supplier<Block>... blocks)
    {
        return BLOCK_ENTITIES.register(name, () -> BlockEntityType.Builder.of(constructor, AVACommonUtil.collect(List.of(blocks), Supplier::get).toArray(new Block[blocks.length])).build(null));
    }

    private static <T extends AbstractContainerMenu> Supplier<MenuType<T>> container(String name, MenuType.MenuSupplier<T> constructor)
    {
        return CONTAINERS.register(name, () -> new MenuType<>(constructor, FeatureFlags.DEFAULT_FLAGS));
    }
}
