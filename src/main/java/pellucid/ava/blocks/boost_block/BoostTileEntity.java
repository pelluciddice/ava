package pellucid.ava.blocks.boost_block;

import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.DataTypes;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class BoostTileEntity extends BlockEntity implements ITickableTileEntity
{
    private boolean isAttackDamageBoost;
    private final HashMap<UUID, AtomicInteger> playerTriggerCD = new HashMap<>();

    public BoostTileEntity(BlockPos pos, BlockState state)
    {
        this(pos, state, false);
    }

    public BoostTileEntity(BlockPos pos, BlockState state, boolean isAttackDamageBoost)
    {
        super(AVATEContainers.BOOST_TE.get(), pos, state);
        this.isAttackDamageBoost = isAttackDamageBoost;
    }

    public void tick()
    {
        if (level != null && !level.isClientSide())
        {
            playerTriggerCD.forEach((id, cd) -> cd.getAndDecrement());
            playerTriggerCD.entrySet().removeIf((entry) -> entry.getValue().get() < 1);
            for (ServerPlayer player : level.getEntitiesOfClass(ServerPlayer.class, new AABB(worldPosition).expandTowards(0.0F, 0.5F, 0.0F), (player) -> !playerTriggerCD.containsKey(player.getUUID())))
            {
                if (player.experienceLevel >= 1)
                {
                    PlayerAction cap = PlayerAction.getCap(player);
                    ParticleOptions particle;
                    String type;
                    ChatFormatting colour;
                    boolean succeed = true;
                    int level;
                    if (isAttackDamageBoost)
                    {
                        int boost = cap.getAttackDamageBoost();
                        if (boost > 19)
                            succeed = false;
                        else
                            cap.setAttackDamageBoost(cap.getAttackDamageBoost() + 1);
                        particle = ParticleTypes.CRIT;
                        type = "attack damage";
                        colour = ChatFormatting.RED;
                        level = cap.getAttackDamageBoost();
                    }
                    else
                    {
                        int boost = cap.getHealthBoost();
                        if (boost > 19)
                            succeed = false;
                        else
                            cap.setHealthBoost(cap.getHealthBoost() + 1);
                        particle = ParticleTypes.HAPPY_VILLAGER;
                        type = "health";
                        colour = ChatFormatting.GREEN;
                        level = cap.getHealthBoost();
                    }
                    if (succeed)
                    {
                        playerTriggerCD.put(player.getUUID(), new AtomicInteger(25));
                        player.giveExperienceLevels(-1);
                        player.sendSystemMessage(Component.translatable("ava.chat.boost_changed", type, 1, level));
                        RandomSource rand = player.level().random;
                        for (int i = 0; i < 15; i++)
                            ((ServerLevel) this.level).sendParticles(particle, player.getX() + rand.nextFloat(), player.getY() + rand.nextFloat() * 2, player.getZ() + rand.nextFloat(), 1, 0.0F, 0.0F, 0.0F, 0.0F);
                        this.level.playSound(null, worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), AVASounds.BLOCK_BOOSTS_PLAYER.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
                    }
                }
            }
        }
    }

    @Override
    public void saveAdditional(CompoundTag compound, HolderLookup.Provider lookup)
    {
        DataTypes.BOOLEAN.write(compound, "isattackboost", isAttackDamageBoost);
    }

    @Override
    public void loadAdditional(CompoundTag nbt, HolderLookup.Provider lookup)
    {
        isAttackDamageBoost = nbt.getBoolean("isattackboost");
        super.loadAdditional(nbt, lookup);
    }
}
