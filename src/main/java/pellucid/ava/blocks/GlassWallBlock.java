package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class GlassWallBlock extends DirectionalShapedBlock
{
    public GlassWallBlock(Properties properties)
    {
        super(properties, 0, 0, 0, 16, 16, 1);
    }

    @Override
    public VoxelShape getVisualShape(BlockState p_60479_, BlockGetter p_60480_, BlockPos p_60481_, CollisionContext context)
    {
        return context.isHoldingItem(asItem()) ? Shapes.block() : super.getVisualShape(p_60479_, p_60480_, p_60481_, context);
    }

    @Override
    public boolean skipRendering(BlockState state, BlockState neighbor, Direction direction)
    {
        Direction facing = state.getValue(FACING);
        Direction.Axis axis = facing.getAxis();
        Direction.Axis axis2 = direction.getAxis();
        if (neighbor.is(this))
        {
            Direction facing2 = neighbor.getValue(FACING);
            if (axis2.isVertical())
                return facing == facing2;
            else if (facing == facing2 && axis != axis2)
                return true;
            else if (facing == facing2.getOpposite() && facing == direction)
                return true;
            else if (facing2 == direction.getOpposite() && axis != axis2)
                return true;
        }
        else if (neighbor.getBlock() instanceof GlassTrigWallBlock)
        {
            Direction facing2 = neighbor.getValue(FACING);
            if (axis2.isVertical() && facing == facing2 && direction == Direction.UP)
                return true;
            if (facing == facing2 && axis != axis2)
                return true;
        }
        return super.skipRendering(state, neighbor, direction);
    }
}
