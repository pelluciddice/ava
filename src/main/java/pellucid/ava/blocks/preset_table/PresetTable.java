package pellucid.ava.blocks.preset_table;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.Shapes;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.DistExecutor;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.DirectionalShapedBlock;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;

public class PresetTable extends DirectionalShapedBlock implements EntityBlock
{
    public PresetTable(Properties properties)
    {
        super(properties, () -> Shapes.or(AVACommonUtil.boxShapeShapes(0, 0, 0, 16, 1, 16), AVACommonUtil.boxShapeShapes(0, 1, 0, 16, 16, 2)));
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level world, BlockPos pos, Player player, BlockHitResult ray)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> this::openScreen);
        return InteractionResult.SUCCESS;
    }

    @OnlyIn(Dist.CLIENT)
    private void openScreen()
    {
        AVAClientUtil.setDeferredScreen(new PresetGUI());
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos p_153215_, BlockState p_153216_)
    {
        return new PresetTableTE(p_153215_, p_153216_);
    }
}