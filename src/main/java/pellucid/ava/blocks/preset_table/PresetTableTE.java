package pellucid.ava.blocks.preset_table;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import pellucid.ava.blocks.AVATEContainers;

public class PresetTableTE extends BlockEntity
{
    public PresetTableTE(BlockPos p_155229_, BlockState p_155230_)
    {
        super(AVATEContainers.PRESET_TABLE_TE.get(), p_155229_, p_155230_);
    }
}
