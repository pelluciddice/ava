package pellucid.ava.blocks.preset_table;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.blocks.DirectionalShapedBlock;
import pellucid.ava.competitive_mode.CompetitiveModeClient;

public class PresetTableTER implements BlockEntityRenderer<PresetTableTE>
{
    private final ItemRenderer itemRenderer;
    public PresetTableTER(BlockEntityRendererProvider.Context context)
    {
        this.itemRenderer = context.getItemRenderer();
    }

    @Override
    public void render(PresetTableTE te, float p_112308_, PoseStack stack, MultiBufferSource p_112310_, int packedLightIn, int overlay)
    {
        Direction d = te.getBlockState().getValue(DirectionalShapedBlock.FACING);
        stack.pushPose();
        stack.translate(0.5F, 0.65F, 0.5F);
        stack.scale(0.5F, 0.5F, 0.5F);
        stack.mulPose(Axis.YP.rotationDegrees(d.toYRot()));
        stack.translate(0.0F, 0.0F, d.getAxis() == Direction.Axis.X ? -0.6F : 0.6F);
        this.itemRenderer.renderStatic(new ItemStack(CompetitiveModeClient.getPreset().getPrimary()), ItemDisplayContext.FIXED, packedLightIn, overlay, stack, p_112310_, Minecraft.getInstance().level, -1);
        stack.popPose();
    }
}
