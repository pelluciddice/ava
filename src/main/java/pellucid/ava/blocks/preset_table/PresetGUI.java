package pellucid.ava.blocks.preset_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.client.components.ItemWidgets;
import pellucid.ava.client.renderers.HUDIndicators;
import pellucid.ava.competitive_mode.CompetitiveModeClient;
import pellucid.ava.competitive_mode.Preset;
import pellucid.ava.events.ClientModEventBus;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class PresetGUI extends Screen
{
    private static final ResourceLocation PRESET_GUI = new ResourceLocation("ava:textures/gui/preset_table.png");
    private final int imageWidth;
    private final int imageHeight;
    protected int leftPos;
    protected int topPos;
    private final List<ItemWidgets.ItemButton<PresetGUI>> presetOptions = new ArrayList<>();
    private final List<ItemWidgets.ItemButton<PresetGUI>> itemOptions = new ArrayList<>();
    private Preset preset = null;
    private Consumer<Item> selectionCallback = null;
    private Predicate<Item> selectionFilter = null;

    protected PresetGUI()
    {
        super(Component.empty());
        this.imageWidth = 256;
        this.imageHeight = 256;
    }

    public void setSelection(Preset preset, ItemWidgets.ItemButton<PresetGUI> button, Consumer<Item> selectionCallback, Predicate<Item> selectionFilter)
    {
        presetOptions.forEach((b) -> b.setSelected(false));
        button.setSelected(true);
        this.preset = preset;
        this.selectionCallback = selectionCallback;
        this.selectionFilter = (gun) -> selectionFilter.test(gun) && !AVAWeaponUtil.isWeaponDisabled(gun);
        updateItems();
    }

    public void applyCallback(Item item)
    {
        if (selectionCallback != null && preset != null)
        {
            selectionCallback.accept(item);
            CompetitiveModeClient.savePresetUnsafe(preset);
            CompetitiveModeClient.loadPresetUnsafe(preset);
        }
    }

    @Override
    protected void init()
    {
        this.leftPos = (this.width - this.imageWidth) / 2;
        this.topPos = (this.height - this.imageHeight) / 2;
        for (int i = 0; i < Preset.PRESETS.size(); i++)
        {
            Preset preset = Preset.PRESETS.get(i);
            int x = leftPos + 17 - 22;
            int y = topPos + 21 + 36 * i;
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamicItemStack(this, preset::getPrimaryStack, x += 22, y + 2, (button) -> setSelection(preset, button, preset::setPrimary, (gun) -> AVAWeaponUtil.isPrimaryWeapon(gun) && preset.getPrimaryCategoryPredicate().test(gun)))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamicItemStack(this, preset::getPrimaryStack2, x += 22, y + 2, (button) -> setSelection(preset, button, preset::setPrimary2, (gun) -> AVAWeaponUtil.isPrimaryWeapon(gun) && preset.getPrimaryCategoryPredicate().test(gun)))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamic(this, preset::getSecondary, x += 20, y, (button) -> setSelection(preset, button, preset::setSecondary, AVAWeaponUtil::isSecondaryWeapon))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamic(this, preset::getMelee, x += 18, y, (button) -> setSelection(preset, button, preset::setMelee, AVAWeaponUtil::isMeleeWeapon))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamic(this, preset::getProjectile, x += 18, y, (button) -> setSelection(preset, button, preset::setProjectile, AVAWeaponUtil::isProjectile))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamic(this, preset::getProjectile2, x += 18, y, (button) -> setSelection(preset, button, preset::setProjectile2, AVAWeaponUtil::isProjectile))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamic(this, preset::getProjectile3, x += 18, y, (button) -> setSelection(preset, button, preset::setProjectile3, AVAWeaponUtil::isProjectile))));
            presetOptions.add(addRenderableWidget(ItemWidgets.selectDynamic(this, preset::getSpecial, x += 18, y, (button) -> setSelection(preset, button, preset::setSpecial, AVAWeaponUtil::isSpecialWeapon))));
        }
        addRenderableWidget(ItemWidgets.selectEmpty(this, Component.translatable("ava.gui.client_config.select", ClientModEventBus.PRESET_F1.getTranslatedKeyMessage()), leftPos + 171, topPos + 22, 17, 17, (button) -> CompetitiveModeClient.forceChoosePreset(0)));
        addRenderableWidget(ItemWidgets.selectEmpty(this, Component.translatable("ava.gui.client_config.select", ClientModEventBus.PRESET_F2.getTranslatedKeyMessage()), leftPos + 171, topPos + 58, 17, 17, (button) -> CompetitiveModeClient.forceChoosePreset(1)));
        addRenderableWidget(ItemWidgets.selectEmpty(this, Component.translatable("ava.gui.client_config.select", ClientModEventBus.PRESET_F3.getTranslatedKeyMessage()), leftPos + 171, topPos + 94, 17, 17, (button) -> CompetitiveModeClient.forceChoosePreset(2)));

        addRenderableWidget(ItemWidgets.selectEmpty(this, Component.translatable("ava.gui.client_config.restore"), leftPos + 192, topPos + 25, 10, 10, (button) -> {
            Preset.F1.toDefault();
            CompetitiveModeClient.savePresetUnsafe(Preset.F1);
        }));
        addRenderableWidget(ItemWidgets.selectEmpty(this, Component.translatable("ava.gui.client_config.restore"), leftPos + 192, topPos + 61, 10, 10, (button) -> {
            Preset.F2.toDefault();
            CompetitiveModeClient.savePresetUnsafe(Preset.F2);
        }));
        addRenderableWidget(ItemWidgets.selectEmpty(this, Component.translatable("ava.gui.client_config.restore"), leftPos + 192, topPos + 97, 10, 10, (button) ->
        {
            Preset.F3.toDefault();
            CompetitiveModeClient.savePresetUnsafe(Preset.F3);
        }));

        addRenderableWidget(ItemWidgets.selectEmptyEither(this, (button) -> AVAItemGun.hasAttachments(Preset.F1.getPrimary()), Pair.of(Component.translatable("ava.gui.tooltip.valid_for_modify"), Component.translatable("ava.gui.tooltip.invalid_for_modify")), leftPos + 22, topPos + 11, 8, 8, (button) -> openModifyingScreen(Preset.F1.getPrimaryStack())));
        addRenderableWidget(ItemWidgets.selectEmptyEither(this, (button) -> AVAItemGun.hasAttachments(Preset.F2.getPrimary()), Pair.of(Component.translatable("ava.gui.tooltip.valid_for_modify"), Component.translatable("ava.gui.tooltip.invalid_for_modify")), leftPos + 22, topPos + 47, 8, 8, (button) -> openModifyingScreen(Preset.F2.getPrimaryStack())));
        addRenderableWidget(ItemWidgets.selectEmptyEither(this, (button) -> AVAItemGun.hasAttachments(Preset.F3.getPrimary()), Pair.of(Component.translatable("ava.gui.tooltip.valid_for_modify"), Component.translatable("ava.gui.tooltip.invalid_for_modify")), leftPos + 22, topPos + 83, 8, 8, (button) -> openModifyingScreen(Preset.F3.getPrimaryStack())));

        addRenderableWidget(ItemWidgets.selectEmptyEither(this, (button) -> AVAItemGun.hasAttachments(Preset.F1.getPrimary2()), Pair.of(Component.translatable("ava.gui.tooltip.valid_for_modify"), Component.translatable("ava.gui.tooltip.invalid_for_modify")), leftPos + 44, topPos + 11, 8, 8, (button) -> openModifyingScreen(Preset.F1.getPrimaryStack2())));
        addRenderableWidget(ItemWidgets.selectEmptyEither(this, (button) -> AVAItemGun.hasAttachments(Preset.F2.getPrimary2()), Pair.of(Component.translatable("ava.gui.tooltip.valid_for_modify"), Component.translatable("ava.gui.tooltip.invalid_for_modify")), leftPos + 44, topPos + 47, 8, 8, (button) -> openModifyingScreen(Preset.F2.getPrimaryStack2())));
        addRenderableWidget(ItemWidgets.selectEmptyEither(this, (button) -> AVAItemGun.hasAttachments(Preset.F3.getPrimary2()), Pair.of(Component.translatable("ava.gui.tooltip.valid_for_modify"), Component.translatable("ava.gui.tooltip.invalid_for_modify")), leftPos + 44, topPos + 83, 8, 8, (button) -> openModifyingScreen(Preset.F3.getPrimaryStack2())));

        updateItems();
    }

    protected static void openModifyingScreen(ItemStack stack)
    {
        if (AVAItemGun.hasAttachments(stack.getItem()))
            AVAClientUtil.setDeferredScreen(new PresetAttachmentsGUI(stack));
    }

    private void updateItems()
    {
        children().removeAll(itemOptions);
        renderables.removeAll(itemOptions);
        itemOptions.clear();
        if (selectionFilter != null)
        {
            int r = 0;
            int c = 0;
            for (Item item : BuiltInRegistries.ITEM.stream().filter((gun) -> selectionFilter.test(gun)).toList())
            {
                itemOptions.add(addRenderableWidget(ItemWidgets.select(this, item, leftPos + 11 + c * 18, topPos + 119 + r * 18, (button) -> applyCallback(button.getItem()))));
                c++;
                if (c >= 13)
                {
                    c = 0;
                    r++;
                }
            }
        }
    }

    @Override
    public void renderBackground(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.renderBackground(stack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        stack.blit(PRESET_GUI, (this.width - this.imageWidth) / 2, (this.height - this.imageHeight) / 2, 0, 0, this.imageWidth, this.imageHeight);

        int x = leftPos + 171;
        int y = topPos + 22;
        int size = 17;
        AVAClientUtil.drawTransparent(true);
        AVAClientUtil.blit(stack.pose(), HUDIndicators.Preset.F1.get(), x, y, x + size, y + size);
        AVAClientUtil.blit(stack.pose(), HUDIndicators.Preset.F2.get(), x, y + 36, x + size, y + 36 + size);
        AVAClientUtil.blit(stack.pose(), HUDIndicators.Preset.F3.get(), x, y + 72, x + size, y + 72 + size);
        AVAClientUtil.drawTransparent(false);
    }
}
