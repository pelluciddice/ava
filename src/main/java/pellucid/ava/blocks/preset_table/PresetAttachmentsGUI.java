package pellucid.ava.blocks.preset_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.ChatFormatting;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.client.components.ITickableButton;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.competitive_mode.CompetitiveModeClient;
import pellucid.ava.gun.attachments.GunAttachmentCategory;
import pellucid.ava.gun.attachments.GunAttachmentManager;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class PresetAttachmentsGUI extends Screen
{
    private static final ResourceLocation TEXTURE = new ResourceLocation("ava:textures/gui/preset_attachments.png");
    private final int imageWidth;
    private final int imageHeight;
    protected int leftPos;
    protected int topPos;
    private GunAttachmentCategory category = null;
    private final ItemStack weapon;

    public PresetAttachmentsGUI(ItemStack weapon)
    {
        super(Component.empty());
        this.weapon = weapon;
        this.imageWidth = 229;
        this.imageHeight = 218;
    }

    @Override
    public boolean isPauseScreen()
    {
        return false;
    }

    @Override
    protected void init()
    {
        this.leftPos = (this.width - this.imageWidth) / 2;
        this.topPos = (this.height - this.imageHeight) / 2;
        clearWidgets();
        super.init();
        addButtons();
    }

    protected void addButtons()
    {
        int x = leftPos + 124 - 16;
        int y = topPos + 82;
        for (GunAttachmentCategory value : GunAttachmentCategory.values())
            addRenderableWidget(new CategoryButton(x += 16, y, value));
        if (getStack().getItem() instanceof AVAItemGun gun)
        {
            if (category != null)
            {
                AtomicInteger y2 = new AtomicInteger(topPos + 30);
                int height = 32;
                for (GunAttachmentTypes type : gun.getAttachmentCategories().get(category))
                    addRenderableWidget(new InstallButton(leftPos + 52, y2.getAndAdd(height), type));
            }
        }
        addRenderableWidget(new UninstallButton(leftPos + 114, topPos + 8));
    }

    private ItemStack getStack()
    {
        return weapon;
    }

    private static final Perspective GUN_PERSPECTIVE = Perspective.scale(3.75F, 3.0F, 1.0F);
    @Override
    public void renderBackground(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.renderBackground(stack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        stack.blit(TEXTURE, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);

        if (this.minecraft == null)
            return;

        ItemStack item = getStack();

        if (item.getItem() instanceof AVAItemGun gun)
        {
            int fontHeight = 5;
            AVAClientUtil.scaleText(stack.pose(), leftPos + 126, topPos + 54, 0.85F, 0.775F, () -> stack.drawString(font, gun.getDescription(), 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE));

            if (category != null)
            {
                AtomicInteger y2 = new AtomicInteger(7);
                int width = 64;
                int height = 32;
                for (GunAttachmentTypes type : gun.getAttachmentCategories().get(category))
                {
                    int x3 = leftPos + 10;
                    int y3 = topPos + y2.get();
                    stack.blit(TEXTURE, x3, y3, 32, 219, width, height);


                    if (AVAClientUtil.inField(mouseX, mouseY, x3, y3, x3 + width, y3 + height))
                    {
                        AVAClientUtil.renderHoverEffect(stack, x3, y3, width, height);
                        drawStats(stack, GunStatTypes.getFullDisplayStrings(gun.getAttachmentTypes().get(type)), leftPos + 77, topPos + 35);
                    }

                    RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
                    drawAttachment(stack, type, gun, leftPos + 13,topPos + y2.get() + 1, topPos + y2.getAndAdd(height) + 2);
                }
                y2.set(7);
                AVAItemGun.manager(item).fromCategory(category).ifPresent((type) -> {
                    drawAttachment(stack, type, gun, leftPos + 78, topPos + y2.get() + 1, topPos + y2.getAndAdd(fontHeight) + 2);
                });
            }
            int x4 = leftPos + 154;
            AtomicInteger y4 = new AtomicInteger(topPos + 7);
            float damage = gun.getStatWithoutBase(item, GunStatTypes.ATTACK_DAMAGE);
            renderGunStat(stack, GunStatTypes.ATTACK_DAMAGE.getKey(), x4, y4.getAndAdd(fontHeight), gun.getBulletDamage(item, false), "(" + AVACommonUtil.getDamageFloatingString(gun.getDamageFloating(item, false)) + ")" + (!AVACommonUtil.similar(damage, 0.0F) ? ("[" + (damage > 0 ? "+" : "") + AVACommonUtil.round(damage, 2) + "]") : ""));
            renderGunStat(stack, GunStatTypes.RANGE.getKey(), x4, y4.getAndAdd(fontHeight), gun.getRange(item, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.RANGE));
            renderGunStat(stack, GunStatTypes.INITIAL_ACCURACY.getKey(), x4, y4.getAndAdd(fontHeight), gun.getInitialAccuracy(item, null, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.INITIAL_ACCURACY));
            renderGunStat(stack, GunStatTypes.ACCURACY.getKey(), x4, y4.getAndAdd(fontHeight), gun.getAccuracy(item, null, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.ACCURACY));
            renderGunStat(stack, GunStatTypes.STABILITY.getKey(), x4, y4.getAndAdd(fontHeight), gun.getStability(item, null, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.STABILITY));
            renderGunStat(stack, GunStatTypes.FIRE_RATE.getKey(), x4, y4.getAndAdd(fontHeight), gun.getFireRate(item, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.FIRE_RATE));
            renderGunStat(stack, GunStatTypes.CAPACITY.getKey(), x4, y4.getAndAdd(fontHeight), gun.getCapacity(item, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.CAPACITY));
            renderGunStat(stack, GunStatTypes.MOBILITY.getKey(), x4, y4.getAndAdd(fontHeight), gun.getMobility(item, false), (Number) gun.getStatWithoutBase(item, GunStatTypes.MOBILITY));


            AVAClientUtil.renderItemStack(stack, GUN_PERSPECTIVE, getStack(), leftPos + 171, topPos + 68, false);
        }
        else
        {
            if (category != null)
            {
                category = null;
                init();
            }
        }
    }

    private void drawAttachment(GuiGraphics stack, GunAttachmentTypes type, AVAItemGun gun, int x, int y, int y2)
    {
        AVAClientUtil.scaleText(stack.pose(), x, y, 0.65F, 0.6F, () -> {
            stack.drawString(font, type.getTranslated(), 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE);
        });
        drawStats(stack, GunStatTypes.getConcludedDisplayStrings(gun.getAttachmentTypes().get(type)), x, y2);
    }

    private void drawStats(GuiGraphics stack, List<? extends MutableComponent> tips, int x, int y)
    {
        int shortFontHeight = 5;
        AtomicInteger y2 = new AtomicInteger(y);
        for (MutableComponent text : tips)
        {
            AVAClientUtil.scaleText(stack.pose(), x, y2.addAndGet(shortFontHeight), 0.65F, 0.58F, () -> {
                stack.drawString(font, text.withStyle(ChatFormatting.GOLD), 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE);
            });
        }
    }

    private void renderGunStat(GuiGraphics stack, String translatable, int x, int y, float value, Number extra)
    {
        double extra2 = extra.doubleValue();
        renderGunStat(stack, translatable, x, y, value, !AVACommonUtil.similar(extra2, 0.0F) ? ("[" + (extra2 > 0 ? "+" : "") + AVACommonUtil.round(extra2, 2) + "]") : "");
    }

    private void renderGunStat(GuiGraphics stack, String translatable, int x, int y, float value, String extra)
    {
        AVAClientUtil.scaleText(stack.pose(), x, y, 0.7F, 0.65F, () -> {
            AVAClientUtil.renderRightAlignedText(stack, font, Component.translatable(translatable), 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE, true);
        });

        int x2 = (int) (46.0F * value / 100.0F);
        AVAClientUtil.fillGradient(stack.pose(), x, y +2, x + x2, y + 5, 0, AVAConstants.AVA_HUD_TEXT_YELLOW, AVAConstants.AVA_HUD_TEXT_ORANGE, Direction.EAST);

        x2 += 2;
        String text = String.valueOf(AVACommonUtil.round(value, 3));
        AVAClientUtil.scaleText(stack.pose(), x + x2, y, 0.7F, 0.65F, () -> {
            stack.drawString(font, text, 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE, true);
        });

        if (!extra.isEmpty())
            AVAClientUtil.scaleText(stack.pose(), (int) (x + x2 + font.width(text) * 0.8F), y, 0.7F, 0.65F, () -> {
                stack.drawString(font, extra, 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE, true);
        });
    }

    public void setCategory(GunAttachmentCategory category)
    {
        this.category = category;
        init();
    }

    @Override
    public void tick()
    {
        for (GuiEventListener child : children())
            if (child instanceof ITickableButton tickable)
                tickable.tick();
    }

    private boolean canCraft()
    {
        return minecraft != null && minecraft.player != null;
    }

    @Override
    public void onClose()
    {
        super.onClose();
        AVAClientUtil.setDeferredScreen(new PresetGUI());
    }

    class UninstallButton extends Button implements ITickableButton
    {
        public UninstallButton(int x, int y)
        {
            this(x, y, 8, 8);
        }

        public UninstallButton(int x, int y, int width, int height)
        {
            super(x, y, width, height, Component.empty(), (button) -> {}, (msg) -> {
                return Component.empty();
            });
            active = false;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_93747_, int p_93748_, float p_93749_)
        {
            if (active)
            {
                int x = 97;
                if (isHoveredOrFocused())
                {
                    x += width;
                    if (AVAClientUtil.leftMouseDown())
                        x += width;
                }
                stack.blit(TEXTURE, getX(), getY(), x, 227, width, height);
            }
        }

        private GunAttachmentTypes type = null;

        @Override
        public void onRelease(double p_93669_, double p_93670_)
        {
            if (active && type != null)
            {
                GunAttachmentManager manager = AVAItemGun.manager(getStack());
                manager.uninstall(type);
                CompetitiveModeClient.putAttachment(getStack().getItem(), manager.serializeNBT());
            }
        }

        @Override
        public void tick()
        {
            if (category != null && getStack().getItem() instanceof AVAItemGun)
            {
                GunAttachmentManager manager = AVAItemGun.manager(getStack());
                Optional<GunAttachmentTypes> type = manager.fromCategory(category);
                active = type.isPresent();
                if (active)
                    this.type = type.get();
            }
            else active = false;
        }
    }

    class InstallButton extends Button
    {
        private final GunAttachmentTypes type;

        public InstallButton(int x, int y, GunAttachmentTypes type)
        {
            this(x, y, 22, 8, type);
        }

        public InstallButton(int x, int y, int width, int height, GunAttachmentTypes type)
        {
            super(x, y, width, height, Component.empty(), (button) -> {}, (msg) -> {
                return Component.empty();
            });
            this.type = type;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_93747_, int p_93748_, float p_93749_)
        {
            int x = 97;
            if (isHoveredOrFocused())
            {
                x += 21;
                if (AVAClientUtil.leftMouseDown())
                    x += 21;
            }
            stack.blit(TEXTURE, getX(), getY(), x, 219, width, height);

            float scale = 0.8F;
            AVAClientUtil.scaleText(stack.pose(), (int) (getX() + width / 2.0F), getY(), scale, scale - 0.05F, () -> {
                stack.drawCenteredString(font, Component.translatable("ava.gui.widget.craft").withStyle(canCraft() ? ChatFormatting.WHITE : ChatFormatting.RED), 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE);
            });
        }

        @Override
        public void onRelease(double p_93669_, double p_93670_)
        {
            if (canCraft())
            {
                GunAttachmentManager manager = AVAItemGun.manager(getStack());
                manager.install(type);
                CompetitiveModeClient.putAttachment(getStack().getItem(), manager.serializeNBT());
            }
        }
    }

    class CategoryButton extends Button
    {
        private final GunAttachmentCategory category;

        public CategoryButton(int x, int y, GunAttachmentCategory category)
        {
            this(x, y, 16, 16, category);
        }

        public CategoryButton(int x, int y, int width, int height, GunAttachmentCategory category)
        {
            super(x, y, width, height, Component.empty(), (button) -> {}, (msg) -> {
                return Component.empty();
            });
            this.category = category;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_93747_, int p_93748_, float p_93749_)
        {
            Item item = getStack().getItem();
            this.active = item instanceof AVAItemGun gun && gun.getAttachmentCategories().containsKey(category);
            if (active)
            {
                int y = 219;
                if (AVAItemGun.manager(getStack()).fromCategory(category).isPresent())
                    y += height;
                stack.blit(TEXTURE, getX(), getY(), 159 + category.ordinal() * width, y, width, height);
                boolean selected = selected();
                if (isHoveredOrFocused() || selected)
                    AVAClientUtil.renderHoverEffect(stack, getX(), getY(), width, height);
                int x = 0;
                if (selected)
                    x += 16;
                stack.blit(TEXTURE, getX(), getY(), x, 219, width, height);
            }
        }

        @Override
        public void onPress()
        {
            if (active)
                setCategory(category);
        }

        public boolean selected()
        {
            return PresetAttachmentsGUI.this.category == this.category;
        }
    }
}
