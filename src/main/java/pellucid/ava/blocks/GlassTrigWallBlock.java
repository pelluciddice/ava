package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class GlassTrigWallBlock extends DirectionalShapedBlock
{
    private final boolean flipped;
    public GlassTrigWallBlock(Properties properties, boolean flipped)
    {
        super(properties, () -> {
            VoxelShape shape = Shapes.box(0, 0, 0, 16, 1, 1);
            for (int i = 1; i < 16; i++)
            {
                if (flipped)
                    shape = Shapes.or(shape, Shapes.box(i, i, 0, 16, i + 1, 1));
                else
                    shape = Shapes.or(shape, Shapes.box(0, i, 0, 16 - i, i + 1, 1));
            }
            return shape;
        });
        this.flipped = flipped;
    }

    public boolean isFlipped()
    {
        return flipped;
    }

    @Override
    public boolean hidesNeighborFace(BlockGetter level, BlockPos pos, BlockState state, BlockState neighbor, Direction direction)
    {
        if (neighbor.is(AVABuildingBlocks.GLASS_WALL.get()))
        {
            Direction facing = state.getValue(FACING);
            Direction neightborFacing = neighbor.getValue(FACING);
            boolean same = facing == neightborFacing;
            if (direction.getAxis().isHorizontal())
            {
                if (flipped)
                {
                    if (same && facing == direction.getCounterClockWise())
                        return true;
                }
                else
                {
                    if (same && facing == direction.getClockWise())
                        return true;
                }
            }
            else if (direction == Direction.DOWN)
            {
                if (same)
                    return true;
            }
        }
        return super.hidesNeighborFace(level, pos, state, neighbor, direction);
    }

    @Override
    public boolean skipRendering(BlockState state, BlockState neighbor, Direction direction)
    {
        if (direction.getAxis().isHorizontal())
        {
            if (neighbor.is(this))
            {
                Direction facing = state.getValue(FACING);
                Direction neightborFacing = neighbor.getValue(FACING);
                boolean same = facing == neightborFacing;
                if (flipped != ((GlassTrigWallBlock) neighbor.getBlock()).flipped)
                {
                    if (same && facing == direction.getCounterClockWise())
                        return true;
                }
                else if (neightborFacing == facing.getOpposite() && facing == direction)
                    return true;
            }
            if (neighbor.is(AVABuildingBlocks.GLASS_WALL.get()))
            {
                Direction facing = state.getValue(FACING);
                Direction neightborFacing = neighbor.getValue(FACING);
                boolean same = facing == neightborFacing;
                if (flipped)
                {
                    if (same && facing == direction.getCounterClockWise())
                        return true;
                }
                else
                {
                    if (same && facing == direction.getClockWise())
                        return true;
                }
            }
        }
        else if (direction == Direction.DOWN && neighbor.is(AVABuildingBlocks.GLASS_WALL.get()))
        {
            if (state.getValue(FACING) == neighbor.getValue(FACING))
                return true;
        }
        return super.skipRendering(state, neighbor, direction);
    }
}
