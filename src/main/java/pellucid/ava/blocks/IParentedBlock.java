package pellucid.ava.blocks;

import net.minecraft.world.level.block.Block;

public interface IParentedBlock
{
    Block getParent();

    Block getBlock();
}
