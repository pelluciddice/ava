package pellucid.ava.blocks.modifying_table;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.AVATEContainers;

public class GunModifyingTableTE extends BlockEntity implements MenuProvider
{
    public GunModifyingTableTE(BlockPos p_155229_, BlockState p_155230_)
    {
        super(AVATEContainers.GUN_MODIFYING_TABLE_TE.get(), p_155229_, p_155230_);
    }

    @Override
    public Component getDisplayName()
    {
        return Component.translatable("ava.container.gun_modifying_table");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_39954_, Inventory p_39955_, Player p_39956_)
    {
        return this.level == null ? null : new GunModifyingTableContainer(p_39954_, p_39955_, ContainerLevelAccess.create(this.level, this.getBlockPos()));
    }
}
