package pellucid.ava.blocks.modifying_table;

import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.neoforged.neoforge.items.ItemStackHandler;
import net.neoforged.neoforge.items.SlotItemHandler;
import org.jetbrains.annotations.NotNull;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVACommonUtil;

public class GunModifyingTableContainer extends AbstractContainerMenu
{
    private final ContainerLevelAccess worldPosCallable;
    public final ItemStackHandler handler = new ItemStackHandler(1);

    public GunModifyingTableContainer(int id, Inventory playerInventory)
    {
        this(id, playerInventory, ContainerLevelAccess.NULL);
    }

    public GunModifyingTableContainer(int id, Inventory playerInventory, ContainerLevelAccess worldPosCallable)
    {
        super(AVATEContainers.GUN_MODIFYING_TABLE_CONTAINER.get(), id);
        this.worldPosCallable = worldPosCallable;
        for (int r = 0; r < 3; r++)
            for(int c = 0; c < 9; c++)
                this.addSlot(new Slot(playerInventory, c + r * 9 + 9, 36 + c * 18, 137 + r * 18));
        for (int h = 0; h < 9; ++h)
            this.addSlot(new Slot(playerInventory, h, 36 + h * 18, 195));
        addSlot(new SlotItemHandler(handler, 0, 164, 102) {
            @Override
            public boolean mayPlace(@NotNull ItemStack stack)
            {
                return AVAItemGun.hasAttachments(stack.getItem());
            }
        });
    }

    @Override
    public void removed(Player player)
    {
        super.removed(player);
        this.worldPosCallable.execute((p_39371_, p_39372_) -> AVACommonUtil.clearContainer(player, handler));
    }

    @Override
    public boolean stillValid(Player playerIn)
    {
        return stillValid(this.worldPosCallable, playerIn, AVABlocks.GUN_MODIFYING_TABLE.get());
    }

    @Override
    public ItemStack quickMoveStack(Player playerIn, int index)
    {
        ItemStack stack = ItemStack.EMPTY;
        Slot slot = slots.get(index);
        if(slot.hasItem())
        {
            ItemStack stack1 = slot.getItem();
            stack = stack1.copy();
            //To Inventory
            final int inventorySlots = 36;
            if (index > inventorySlots - 1)
            {
                if (!this.moveItemStackTo(stack1, 0, 34, true))
                    return ItemStack.EMPTY;
            }
            //From Inventory
            else
            {
                if (moveItemStackTo(stack1, inventorySlots, inventorySlots + 1, false))
                    return ItemStack.EMPTY;
            }
            if (stack1.isEmpty())
                slot.set(ItemStack.EMPTY);
            else
                slot.setChanged();
            if (stack1.getCount() == stack.getCount())
                return ItemStack.EMPTY;
            slot.onTake(playerIn, stack1);
        }
        return stack;
    }
}
