package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;

public class ControllerBlock extends Block implements IInteractable<BlockHitResult>
{
    public static final VoxelShape NEGATIVE_Z = Block.box(0.0D, 3.0D, 0.0D, 16.0D, 13.0D, 3.0D);
    public static final VoxelShape POSITIVE_Z = Block.box(0.0D, 3.0D, 13.0D, 16.0D, 13.0D, 16.0D);
    public static final VoxelShape NEGATIVE_X = Block.box(0.0D, 3.0D, 0.0D, 3.0D, 13.0D, 16.0D);
    public static final VoxelShape POSITIVE_X = Block.box(13.0D, 3.0D, 0.0D, 16.0D, 13.0D, 16.0D);
    public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;
    public static final BooleanProperty ACTIVATE = BooleanProperty.create("activate");

    public ControllerBlock()
    {
        super(Properties.of().sound(SoundType.METAL).strength(5.0F).noOcclusion());
        registerDefaultState(defaultBlockState().setValue(FACING, Direction.NORTH).setValue(ACTIVATE, false));
    }

    @Override
    public boolean shouldDrawTipWrench()
    {
        return true;
    }

    @Override
    public String getInteractableTip()
    {
        return "ava.interaction.panel";
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rotation)
    {
        return state.setValue(FACING, rotation.rotate(state.getValue(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror)
    {
        return state.rotate(mirror.getRotation(state.getValue(FACING)));
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level worldIn, BlockPos pos, Player player, BlockHitResult hit)
    {
        return InteractionResult.CONSUME;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
    {
        switch (state.getValue(FACING))
        {
            case NORTH:
            default:
                return NEGATIVE_Z;
            case SOUTH:
                return POSITIVE_Z;
            case WEST:
                return NEGATIVE_X;
            case EAST:
                return POSITIVE_X;
        }
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context)
    {
        return this.defaultBlockState()
                .setValue(FACING, context.getHorizontalDirection());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        builder.add(FACING, ACTIVATE);
    }

    @Override
    public boolean isSignalSource(BlockState state)
    {
        return state.getValue(ACTIVATE);
    }

    @Override
    public int getSignal(BlockState state, BlockGetter blockAccess, BlockPos pos, Direction side)
    {
        return state.getValue(ACTIVATE) ? 15 : 0;
    }

    @Override
    public int getDirectSignal(BlockState state, BlockGetter blockAccess, BlockPos pos, Direction side)
    {
        return state.getValue(ACTIVATE) && state.getValue(FACING).getOpposite() == side ? 15 : 0;
    }

    @Override
    public int getDuration()
    {
        return 120;
    }

    @Override
    public double maxDistance()
    {
        return 1.5F;
    }

    @Override
    public void interact(Level world, BlockHitResult result, @Nullable BlockPos pos, @Nullable Vec3 vector3d, LivingEntity interacter)
    {
        if (!world.isClientSide() && pos != null)
        {
            BlockState state = world.getBlockState(pos);
            world.setBlockAndUpdate(pos, state.setValue(ACTIVATE, !state.getValue(ACTIVATE)));
            world.updateNeighborsAt(pos, this);
            world.updateNeighborsAt(pos.relative(world.getBlockState(pos).getValue(FACING)), this);
        }
    }
}
