package pellucid.ava.blocks.explosive_barrel;

import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.blocks.IInheritable;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.UUID;

public class ExplosiveBarrelTE extends BlockEntity implements ITickableTileEntity, IInheritable
{
    private int health = 180;
    private boolean exploded = false;
    private UUID lastAttacker;

    public ExplosiveBarrelTE(BlockPos pos, BlockState state)
    {
        super(AVATEContainers.EXPLOSIVE_BARREL_TE.get(), pos, state);
    }

    public void tick()
    {
        if (level != null && !level.isClientSide() && !exploded)
        {
            if (health >= 100)
                checkAndSet(0);
            else if (health >= 1)
                checkAndSet(1);
            else
            {
                checkAndSet(2);
                explode();
            }
        }
    }

    public void attack(@Nullable LivingEntity attacker, float damage)
    {
        health -= damage;
        health = Math.max(0, health);
        if (attacker != null)
            lastAttacker = attacker.getUUID();
    }

    public void heal(float amount)
    {
        health += amount;
        health = Math.min(180, health);
        exploded = false;
    }

    private void explode()
    {
        if (level != null)
        {
            exploded = true;
            RandomSource rand = level.random;
            AVAWeaponUtil.createExplosionBlock(level, worldPosition, getBlockState(), getLastAttacker(), 3, 60, (serverWorld, x, y, z) ->
            {
                for (int i = 0; i < 30; i++)
                {
                    serverWorld.sendParticles(ParticleTypes.LARGE_SMOKE, x - 0.5F + rand.nextFloat(), y + 0.1F, z - 0.5F + rand.nextFloat(), 1, 0.0F, 0.0F, 0.0F, 0.5F);
                    serverWorld.sendParticles(ParticleTypes.FLAME, x - 0.5F + rand.nextFloat(), y + 0.1F, z - 0.5F + rand.nextFloat(), 3, 0.0F, 0.0F, 0.0F, 0.5F);
                }
            }, AVASounds.EXPLOSIVE_BARREL_EXPLODE.get());
        }
    }

    private void checkAndSet(int health)
    {
        if (level != null)
        {
            BlockState state = getBlockState();
            if (state.getValue(ExplosiveBarrel.PHASE) != health)
                level.setBlockAndUpdate(worldPosition, state.setValue(ExplosiveBarrel.PHASE, health));
        }
    }

    @Nullable
    public LivingEntity getLastAttacker()
    {
        if (level != null && lastAttacker != null && !level.isClientSide())
        {
            Entity entity = ((ServerLevel) level).getEntity(lastAttacker);
            return entity instanceof LivingEntity ? (LivingEntity) entity : null;
        }
        return null;
    }

    @Override
    public void saveAdditional(CompoundTag compound, HolderLookup.Provider lookup)
    {
        drain(compound);
    }

    @Override
    public void loadAdditional(CompoundTag nbt, HolderLookup.Provider lookup)
    {
        inherit(nbt);
        super.loadAdditional(nbt, lookup);
    }

    @Override
    public CompoundTag drain(CompoundTag compound)
    {
        DataTypes.INT.write(compound, "health", health);
        DataTypes.BOOLEAN.write(compound, "exploded", exploded);
        if (lastAttacker != null)
            DataTypes.UUID.write(compound, "attacker", lastAttacker);
        return compound;
    }

    @Override
    public void inherit(CompoundTag compound)
    {
        health = DataTypes.INT.read(compound, "health");
        exploded = DataTypes.BOOLEAN.read(compound, "exploded");
        if (compound.contains("attacker"))
            lastAttacker = DataTypes.UUID.read(compound, "attacker");
    }
}
