package pellucid.ava.blocks.explosive_barrel;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.blocks.ITickableTileEntity;

import javax.annotation.Nullable;

public class ExplosiveBarrel extends Block implements EntityBlock
{
    public static final IntegerProperty PHASE = IntegerProperty.create("phase", 0, 2);
    public ExplosiveBarrel()
    {
        super(Properties.of().sound(SoundType.METAL).strength(1.75F).noOcclusion());
        registerDefaultState(defaultBlockState().setValue(PHASE, 0));
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level worldIn, BlockPos pos, Player player, BlockHitResult hit)
    {
        int phase = state.getValue(PHASE);
        ItemStack stack = player.getMainHandItem();
        if (phase > 0 && stack.is(Tags.Items.GUNPOWDERS))
        {
            if (!worldIn.isClientSide)
            {
                BlockEntity te = worldIn.getBlockEntity(pos);
                if (te instanceof ExplosiveBarrelTE)
                {
                    ((ExplosiveBarrelTE) te).heal(75.0F);
                    if (!player.getAbilities().instabuild)
                        stack.shrink(1);
                }
            }
            return InteractionResult.sidedSuccess(worldIn.isClientSide);
        }
        return InteractionResult.PASS;
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving)
    {
        if (!worldIn.isClientSide() && state.getBlock() != newState.getBlock())
            worldIn.removeBlockEntity(pos);
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void animateTick(BlockState stateIn, Level worldIn, BlockPos pos, RandomSource rand)
    {
        if (stateIn.getValue(PHASE) == 1)
            worldIn.addAlwaysVisibleParticle(ParticleTypes.SMOKE, pos.getX() + rand.nextFloat(), pos.getY() + 1.0F, pos.getZ() + rand.nextFloat(), 0.0F, 0.0F, 0.0F);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        builder.add(PHASE);
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
    {
        return new ExplosiveBarrelTE(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> p_153214_)
    {
        return ITickableTileEntity::ticker;
    }
}
