package pellucid.ava.blocks;

import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class DirectionalShapedParentBlock extends DirectionalShapedBlock implements IParentedBlock
{
    public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;
    protected final Map<Direction, VoxelShape> shapes = new HashMap<>();
    private final Block parent;

    public DirectionalShapedParentBlock(Properties properties, Block parent, double x, double y, double z, double x2, double y2, double z2)
    {
        super(properties, x, y, z, x2, y2, z2);
        this.parent = parent;
    }

    public DirectionalShapedParentBlock(Properties properties, Block parent, Supplier<VoxelShape> shapeProvider)
    {
        super(properties, shapeProvider);
        this.parent = parent;
    }

    public DirectionalShapedParentBlock(Properties properties, Block parent)
    {
        super(properties);
        this.parent = parent;
    }

    @Override
    public Block getParent()
    {
        return parent;
    }

    @Override
    public Block getBlock()
    {
        return this;
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext p_49820_)
    {
        return super.getStateForPlacement(p_49820_).setValue(FACING, p_49820_.getHorizontalDirection());
    }
}
