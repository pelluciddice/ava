package pellucid.ava.blocks;

import net.minecraft.world.item.Items;
import net.minecraft.world.level.material.MapColor;
import pellucid.ava.recipes.Recipe;

import java.util.Locale;

public enum AVABlockDyeColours
{
    BEIGE(MapColor.TERRACOTTA_WHITE, new Recipe().addItem(Items.WHITE_DYE).addItem(Items.ORANGE_DYE)),
    BRIGHT_GREEN(MapColor.TERRACOTTA_LIGHT_GREEN, new Recipe().addItem(Items.WHITE_DYE).addItem(Items.LIME_DYE)),
    BRIGHT_PINK(MapColor.TERRACOTTA_PINK, new Recipe().addItem(Items.WHITE_DYE).addItem(Items.PINK_DYE)),
    KHAKI(MapColor.SAND, new Recipe().addItem(Items.WHITE_DYE).addItem(Items.ORANGE_DYE)),
    WATER_BLUE(MapColor.COLOR_LIGHT_BLUE, new Recipe().addItem(Items.WHITE_DYE).addItem(Items.BLUE_DYE))
    ;
    private final String name;
    private final MapColor materialColor;
    private final Recipe recipe;

    AVABlockDyeColours(MapColor materialColor, Recipe recipe)
    {
        this.materialColor = materialColor;
        this.recipe = recipe;
        this.name = name().toLowerCase(Locale.ROOT);
    }

    public String getName()
    {
        return name;
    }

    public MapColor getMapColor()
    {
        return materialColor;
    }

    public Recipe getRecipe()
    {
        return recipe;
    }
}
