package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;


public class SiteBlock extends AVAHorizontalBlock
{
    public static final BooleanProperty POWERED = BlockStateProperties.POWERED;

    public SiteBlock()
    {
        super(BlockBehaviour.Properties.of().sound(SoundType.STONE).strength(-1.0F, 3600000.0F));
        registerDefaultState(defaultBlockState()
                .setValue(FACING, Direction.NORTH).setValue(POWERED, false));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        super.createBlockStateDefinition(builder);
        builder.add(POWERED);
    }

    @Override
    public void tick(BlockState state, ServerLevel world, BlockPos pos, RandomSource rand)
    {
        if (state.getValue(POWERED))
            world.setBlock(pos, state.setValue(POWERED, false), 2);
        else
        {
            world.setBlock(pos, state.setValue(POWERED, true), 2);
            world.scheduleTick(pos, this, 1);
        }
        updateNeightbors(world, pos);
    }

    public void trigger(LevelAccessor worldIn, BlockPos pos)
    {
        if (!worldIn.isClientSide() && !worldIn.getBlockTicks().hasScheduledTick(pos, this))
            worldIn.scheduleTick(pos, this, 1);
    }

    protected void updateNeightbors(Level world, BlockPos pos)
    {
        for (Direction direction : Direction.values())
        {
            BlockPos pos2 = pos.relative(direction);
            world.neighborChanged(pos2, this, pos);
            world.updateNeighborsAtExceptFromFacing(pos2, this, direction);
        }
    }

    @Override
    public boolean isSignalSource(BlockState state)
    {
        return true;
    }

    @Override
    public int getDirectSignal(BlockState state, BlockGetter blockAccess, BlockPos pos, Direction side)
    {
        return state.getValue(POWERED) ? 15 : 0;
    }

    @Override
    public int getSignal(BlockState state, BlockGetter blockAccess, BlockPos pos, Direction side)
    {
        return getDirectSignal(state, blockAccess, pos, side);
    }

    @Override
    public void onPlace(BlockState state, Level worldIn, BlockPos pos, BlockState oldState, boolean isMoving)
    {
        updateNeightbors(worldIn, pos);
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving)
    {
        updateNeightbors(worldIn, pos);
    }
}
