package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import pellucid.ava.util.AVACommonUtil;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class FloorBlock extends DirectionalShapedBlock
{
    protected final Map<Direction, VoxelShape> shapes2 = new HashMap<>();
    public static final BooleanProperty IS_TOP = BooleanProperty.create("is_top");
    public FloorBlock(Properties properties, Supplier<VoxelShape> topShape, Supplier<VoxelShape> bottomShape)
    {
        super(properties, topShape);
        AVACommonUtil.putDirectionalShapes(shapes2, bottomShape.get());
        registerDefaultState(defaultBlockState().setValue(IS_TOP, false));
    }

    @Nullable
    public BlockState getStateForPlacement(BlockPlaceContext context)
    {
        BlockPos pos = context.getClickedPos();
        BlockState state = super.getStateForPlacement(context);
        Direction direction = context.getClickedFace();
        return state.setValue(IS_TOP, !(direction != Direction.DOWN && (direction == Direction.UP || !(context.getClickLocation().y - (double) pos.getY() > 0.5D))));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> p_49915_)
    {
        super.createBlockStateDefinition(p_49915_);
        p_49915_.add(IS_TOP);
    }

    private static final Map<Boolean, VoxelShape> FRIENDLY_SHAPES = new HashMap<>() {{
        put(true, Block.box(0, 13, 0, 16, 16, 16));
        put(false, Block.box(0, 0, 0, 16, 3, 16));
    }};
    @Override
    public VoxelShape getShape(BlockState p_60555_, BlockGetter p_60556_, BlockPos p_60557_, CollisionContext p_60558_)
    {
        if (p_60558_.isHoldingItem(asItem()))
            return FRIENDLY_SHAPES.get(p_60555_.getValue(IS_TOP));
        return p_60555_.getValue(IS_TOP) ? super.getShape(p_60555_, p_60556_, p_60557_, p_60558_) : shapes2.get(p_60555_.getValue(FACING));
    }

    @Override
    public VoxelShape getShape(BlockState state, Direction direction)
    {
        return state.getValue(IS_TOP) ? super.getShape(state, direction) : shapes2.get(direction);
    }
}
