package pellucid.ava.blocks.mastery_table.builders_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.client.components.ItemWidgets;
import pellucid.ava.gun.gun_mastery.GunMastery;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Pair;

import java.util.List;

public class MasteryGUI extends Screen
{
    private static final ResourceLocation MASTERIES_GUI = new ResourceLocation("ava:textures/gui/mastery.png");
    private ItemStack item;
    private GunMasteryManager manager;
    private final int imageWidth;
    private final int imageHeight;
    protected int leftPos;
    protected int topPos;

    protected MasteryGUI(ItemStack item)
    {
        super(Component.empty());
        this.item = item;
        this.manager = GunMasteryManager.ofUnsafe(item);
        this.imageWidth = 230;
        this.imageHeight = 219;
    }

    @Override
    public void tick()
    {
        if (minecraft == null || minecraft.player == null)
            return;
        ItemStack stack = minecraft.player.getMainHandItem();
        if (stack.getItem() instanceof AVAItemGun)
            update(stack);
        else onClose();
    }

    private ItemWidgets.DisplayItemWidget<MasteryGUI> display;
    public void update(ItemStack item)
    {
        this.item = item;
        this.manager = GunMasteryManager.ofUnsafe(item);
        display.setItem(item);
    }

    @Override
    public boolean isPauseScreen()
    {
        return false;
    }

    @Override
    protected void init()
    {
        this.leftPos = (this.width - this.imageWidth) / 2;
        this.topPos = (this.height - this.imageHeight) / 2;
        display = addRenderableWidget(ItemWidgets.display(this, item, leftPos + 13, topPos + 10));
        addRenderableWidget(new RandomBoostButton());
    }

    @Override
    public void renderBackground(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.renderBackground(stack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        stack.blit(MASTERIES_GUI, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
    }

    @Override
    public void render(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(stack, mouseX, mouseY, partialTicks);
        stack.blit(MASTERIES_GUI, leftPos + 10, topPos + 32, GunMastery.MASTERIES_LIST.indexOf(manager.mastery) * 37, 219, 37, 37);
        stack.drawString(font, manager.mastery.getTitle(manager.masteryLevel), leftPos + 50, topPos + 32, AVAConstants.AVA_HUD_TEXT_ORANGE);
        stack.drawWordWrap(font, manager.mastery.getDescription(), leftPos + 50, topPos + 40, 180, AVAConstants.AVA_HUD_TEXT_WHITE);
        stack.drawWordWrap(font, Component.translatable("ava.gui.mastery.random_boosts", manager.masteryLevel), leftPos + 135, topPos + 78, 80, AVAConstants.AVA_HUD_TEXT_WHITE);

        int y = topPos + 78;
        for (GunStatTypes boost : GunMasteryManager.BOOSTS)
        {
            AVAClientUtil.renderRightAlignedText(stack, font, boost.getTranslatedName(), leftPos + 76, y, AVAConstants.AVA_HUD_TEXT_ORANGE, true);

            int x = leftPos + 80;
            for (int i = 0; i < manager.getBoost(boost); i++)
            {
                stack.fill(x, y + 2, x + 8, y + 8, AVAConstants.AVA_GUN_BOOST_GREEN.getRGB());
                x += 11;
            }
            y += 8;
        }

        stack.drawString(font, AVACommonUtil.round(manager.progressPercent() * 100.0F, 1) + "%", leftPos + 195, topPos + 139, AVAConstants.AVA_HUD_TEXT_YELLOW);
        AVAClientUtil.fillGradient(stack.pose(), leftPos + 15, topPos + 127, (int) (leftPos + 15 + 200 * manager.progressPercent()), topPos + 137, 0, AVAConstants.AVA_HUD_TEXT_YELLOW, AVAConstants.AVA_HUD_TEXT_ORANGE, Direction.EAST);

        int c = manager.mastery.getColour().getColor();
        y = 129;

        if (manager.hasOngoingTask())
        {
            stack.drawCenteredString(font, manager.task.getDescription((IClassification) item.getItem(), manager.taskProgress), width / 2, topPos + 128, c);
            stack.drawString(font, manager.task.getTitle(), leftPos + 14, topPos + (y += 8), c);
        }

        Pair<List<MutableComponent>, List<MutableComponent>> tips = manager.mastery.getPerkStrings(manager.masteryLevel);
        stack.drawString(font, Component.translatable("ava.gui.mastery.shooter_buffs"), leftPos + 14, topPos + (y += 14), c);
        for (MutableComponent tip : tips.getA())
            stack.drawString(font, tip, leftPos + 14, topPos + (y += 8), c);
        stack.drawString(font, Component.translatable("ava.gui.mastery.target_debuffs"), leftPos + 14, topPos + (y += 14), c);
        for (MutableComponent tip : tips.getB())
            stack.drawString(font, tip, leftPos + 14, topPos + (y += 8), c);
    }

    protected void rerollStats()
    {
        PlayerActionMessage.rollBoost();
    }

    protected boolean isValid()
    {
        return MasteryGUI.this.minecraft.player.experienceLevel > 0;
    }

    class RandomBoostButton extends Button
    {
        public RandomBoostButton()
        {
            super(leftPos + 196, topPos + 112, 21, 8, Component.empty(), (button) -> {
                if (isValid()) rerollStats();
            }, (msg) -> {
                return Component.empty();
            });
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_93747_, int p_93748_, float p_93749_)
        {
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            int x = 148;
            if (isValid())
            {
                if (isHoveredOrFocused())
                {
                    x += this.width;
                    if (AVAClientUtil.leftMouseDown())
                        x += this.width;
                }
            }
            else x += this.width * 2;
            stack.blit(MASTERIES_GUI, getX(), getY(), x, 219, width, height);
            stack.drawCenteredString(font, MasteryGUI.this.minecraft.player.experienceLevel + "/1", getX() + this.width / 2, getY(), isValid() ? AVAConstants.AVA_HUD_COLOUR_BLUE.getRGB() : AVAConstants.AVA_HUD_TEXT_RED);
        }
    }
}
