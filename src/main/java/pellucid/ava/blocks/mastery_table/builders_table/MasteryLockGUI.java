package pellucid.ava.blocks.mastery_table.builders_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.client.components.ItemWidgets;
import pellucid.ava.gun.gun_mastery.GunMastery;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.util.AVAConstants;

public class MasteryLockGUI extends Screen
{
    private static final ResourceLocation MASTERIES_GUI = new ResourceLocation("ava:textures/gui/mastery_lock.png");
    private final ItemStack item;
    private final int imageWidth;
    private final int imageHeight;
    protected int leftPos;
    protected int topPos;

    protected MasteryLockGUI(ItemStack item)
    {
        super(Component.empty());
        this.item = item;
        this.imageWidth = 230;
        this.imageHeight = 219;
    }

    @Override
    protected void init()
    {
        this.leftPos = (this.width - this.imageWidth) / 2;
        this.topPos = (this.height - this.imageHeight) / 2;
        addRenderableWidget(ItemWidgets.display(this, item, leftPos + 13, topPos + 10));
        addRenderableWidget(createSelectionButton(GunMasteryManager.MEDIC, 10, 32));
        addRenderableWidget(createSelectionButton(GunMasteryManager.SCOUT, 117, 32));
        addRenderableWidget(createSelectionButton(GunMasteryManager.SNIPER, 10, 122));
        addRenderableWidget(createSelectionButton(GunMasteryManager.WORRIER, 117, 122));
    }

    private Button createSelectionButton(GunMastery mastery, int x, int y)
    {
        return new Button(leftPos + x, topPos + y, 37, 37, Component.empty(), (button) -> lockMastery(mastery), (msg) -> {
            return Component.empty();
        }) {
            @Override
            public void renderWidget(GuiGraphics stack, int p_93747_, int p_93748_, float p_93749_)
            {
                if (isHoveredOrFocused())
                {
                    RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 0.45F);
                    stack.fill(getX(), getY(), getX() + width, getY() + height, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
                    RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
                }
                stack.drawString(font, mastery.getTitle(-1), getX() + width + 3, getY() + 1, AVAConstants.AVA_HUD_TEXT_ORANGE);
                stack.drawWordWrap(font, mastery.getDescription(), getX() + width + 3, getY() + 9, 64, AVAConstants.AVA_HUD_TEXT_WHITE);
            }
        };
    }

    private void lockMastery(GunMastery mastery)
    {
        DataToServerMessage.assignMastery(mastery.getName());
        onClose();
    }

    @Override
    public void renderBackground(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.renderBackground(stack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        int x = (this.width - this.imageWidth) / 2;
        int y = (this.height - this.imageHeight) / 2;
        stack.blit(MASTERIES_GUI, x, y, 0, 0, this.imageWidth, this.imageHeight);
    }

    @Override
    public void render(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(stack, mouseX, mouseY, partialTicks);
        stack.drawString(font, Component.translatable("ava.gui.mastery.pick_mastery"), leftPos + 34, topPos + 14, AVAConstants.AVA_HUD_TEXT_ORANGE);
    }
}
