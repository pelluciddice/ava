package pellucid.ava.blocks.mastery_table.builders_table;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.DistExecutor;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;

public class MasteryTable extends Block
{
    public MasteryTable(Properties properties)
    {
        super(properties);
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level world, BlockPos pos, Player player, BlockHitResult ray)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            if (AVAServerConfig.DISABLE_MASTERY_SYSTEM.get())
            {
                player.sendSystemMessage(Component.translatable("ava.chat.mastery_disabled"));
                return;
            }
            if (world.isClientSide())
            {
                ItemStack stack = player.getMainHandItem();
                if (stack.getItem() instanceof AVAItemGun)
                    openScreen(GunMasteryManager.ofUnsafe(stack).hasMasteryUnlocked(), stack);
                else
                    player.sendSystemMessage(Component.translatable("ava.chat.not_holding_gun"));
            }
        });
        return InteractionResult.SUCCESS;
    }

    @OnlyIn(Dist.CLIENT)
    private void openScreen(boolean unlocked, ItemStack mainHandItem)
    {
        AVAClientUtil.setDeferredScreen(unlocked ? new MasteryGUI(mainHandItem) : new MasteryLockGUI(mainHandItem));
    }
}