package pellucid.ava.blocks.colouring_table;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

import javax.annotation.Nullable;

public class GunColouringTable extends Block implements EntityBlock
{
    public GunColouringTable(Properties properties)
    {
        super(properties);
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level world, BlockPos pos, Player player, BlockHitResult ray)
    {
        if (!world.isClientSide)
        {
            BlockEntity tileentity = world.getBlockEntity(pos);
            if (tileentity instanceof GunColouringTableTE)
                player.openMenu((MenuProvider) tileentity);
        }
        return InteractionResult.SUCCESS;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos p_153215_, BlockState p_153216_)
    {
        return new GunColouringTableTE(p_153215_, p_153216_);
    }
}
