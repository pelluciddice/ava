package pellucid.ava.blocks.colouring_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import pellucid.ava.client.components.ItemWidgets;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAClientUtil;

import java.util.ArrayList;
import java.util.List;

@OnlyIn(Dist.CLIENT)
public class GunColouringGUI extends AbstractContainerScreen<GunColouringTableContainer>
{
    private static final ResourceLocation GUN_CRAFTING_GUI = new ResourceLocation("ava:textures/gui/gun_colouring_table.png");
    private int current_index = 0;
    private int current_shown_index = 0;
    private final List<AVAItemGun> gunsInv = new ArrayList<>();
    private final List<AVAItemGun> skins = new ArrayList<>();
    private final Inventory inventory;
    private final List<ItemWidgets.DisplayItemWidget<GunColouringGUI>> ingredients = new ArrayList<>();

    public GunColouringGUI(GunColouringTableContainer screenContainer, Inventory inv, Component titleIn)
    {
        super(screenContainer, inv, titleIn);
        this.imageWidth = 230;
        this.imageHeight = 219;
        this.inventory = inv;
    }

    protected void clearAll()
    {
        renderables.clear();
        children().clear();
        ingredients.clear();
    }

    @Override
    protected void init()
    {
        super.init();
        clearAll();
        this.addRenderableWidget(new SelectButton(this.leftPos + 60, this.topPos + 45, true, true));
        this.addRenderableWidget(new SelectButton(this.leftPos + 162, this.topPos + 45, false, true));
        this.addRenderableWidget(new SelectButton(this.leftPos + 46, this.topPos + 90, true, false));
        this.addRenderableWidget(new SelectButton(this.leftPos + 88, this.topPos + 90, false, false));
        this.addRenderableWidget(new PaintButton(this.leftPos + 99, this.topPos + 84));
        for (int i = 0; i < 5; i ++)
            ingredients.add(addRenderableWidget(ItemWidgets.display(this, null, leftPos + 123 + i * 18, topPos + 92)));
    }

    @Override
    public void render(GuiGraphics stack, int p_97796_, int p_97797_, float p_97798_)
    {
        super.render(stack, p_97796_, p_97797_, p_97798_);
        this.renderTooltip(stack, p_97796_, p_97797_);
    }

    @Override
    protected void renderLabels(GuiGraphics stack, int mouseX, int mouseY)
    {
        if (this.getShownGun(this.current_shown_index) != Items.AIR)
        {
            AVAItemGun gun = (AVAItemGun) this.getShownGun(this.current_shown_index);
            stack.pose().pushPose();
            stack.pose().scale(1.75F, 1.75F, 1.75F);
            stack.drawCenteredString(this.font, Component.translatable(gun.getDescriptionId()).getString(), 67, 9, 150);
            stack.pose().popPose();
        }
        if (this.gunsInv.size() <= this.current_index)
            return;
        AVAItemGun selectedGun = this.gunsInv.get(this.current_index);
        stack.renderFakeItem(new ItemStack(selectedGun), 64, 93);
        stack.pose().pushPose();
        stack.pose().scale(0.85F, 0.85F, 0.85F);
        stack.drawCenteredString(this.font, Component.translatable(selectedGun.getDescriptionId()).getString(), 85, 132, 150);
        stack.pose().popPose();
        if (this.getShownGun(this.current_shown_index - 1) != Items.AIR)
            renderItemShown(stack, this.getShownGun(this.current_shown_index - 1), 14, 44, false);
        if (this.getShownGun(this.current_shown_index) instanceof AVAItemGun)
            renderItemShown(stack, this.getShownGun(this.current_shown_index), 74, 12, true);
        if (this.getShownGun(this.current_shown_index + 1) != Items.AIR)
            renderItemShown(stack, this.getShownGun(this.current_shown_index + 1), 166, 44, false);
//        for (GuiEventListener listener : children())
//            if (listener instanceof AbstractWidget widget && widget.isHoveredOrFocused())
//            {
//                widget.renderToolTip(stack, mouseX - this.leftPos, mouseY - this.topPos);
//                break;
//            }
    }

    @Override
    protected void renderBg(GuiGraphics stack, float partialTicks, int mouseX, int mouseY)
    {
        if (this.minecraft == null)
            return;
        int x = (this.width - this.imageWidth) / 2;
        int y = (this.height - this.imageHeight) / 2;
        stack.blit(GUN_CRAFTING_GUI, x, y, 0, 0, this.imageWidth, this.imageHeight);
    }

    protected void renderItemShown(GuiGraphics stack, Item item, int x, int z, boolean primary)
    {
        ItemStack itemStack = new ItemStack(item);
        float f = primary ? 5.75F : 3.45F;
        x /= f;
        z /= f;

        int finalX = x;
        int finalZ = z;
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
            stack2.scale(f, f, 1.0F);
        }, (stack2) -> {
            AVAClientUtil.forceEnable3DModel(() -> stack.renderFakeItem(itemStack, finalX, finalZ));
        });

    }

    protected Item getShownGun(int index)
    {
        return index < 0 ? Items.AIR : this.skins.size() > index ? this.skins.get(index) : Items.AIR;
    }

    @Override
    protected void containerTick()
    {
        update();
    }

    protected void update()
    {
        this.gunsInv.clear();
        for (int slot=0;slot<inventory.getContainerSize();slot++)
            if (inventory.getItem(slot).getItem() instanceof AVAItemGun)
                this.gunsInv.add((AVAItemGun) inventory.getItem(slot).getItem());
        this.skins.clear();
        if (this.isIndexAvailable(this.current_index, false))
        {
            AVAItemGun gun = this.gunsInv.get(this.current_index);
            this.skins.addAll(getSkinsFromGun(gun));
            if (this.getShownGun(this.current_shown_index) != Items.AIR)
            {
                gun = (AVAItemGun) this.getShownGun(this.current_shown_index);
                this.ingredients.forEach((b) -> b.setItem(Items.AIR));
                Recipe recipe = gun.isMaster() ? gun.getMasterPaintRecipe() : gun.getRecipe();
                List<Ingredient> ingredients = recipe.getIngredients();
                for (int i = 0; i < recipe.getIngredients().size(); i++)
                    this.ingredients.get(i).setItem(new ItemStack(Recipe.pickFromIngredient(ingredients.get(i), Items.AIR), recipe.getCount(ingredients.get(i))));
            }
        }
    }

    protected List<AVAItemGun> getSkinsFromGun(AVAItemGun gun)
    {
        return gun.isMaster() ? gun.getSubGuns() : gun.getMaster().getSubGuns();
    }

    protected void setGunShown(int index)
    {
        //        index = index < 0 ? 0 : this.skins.size() <= index ? this.skins.size() - 1 : index;
        //        if (index >= 0)
        if (isIndexAvailable(index, true))
            this.current_shown_index = index;
    }

    protected void setGunSelected(int index)
    {
        //        index = index < 0 ? 0 : this.gunsInv.size() <= index ? this.gunsInv.size() - 1 : index;
        //        if (index >= 0)
        if (isIndexAvailable(index, false))
        {
            this.current_shown_index = 0;
            this.current_index = index;
        }
    }

    protected boolean isIndexAvailable(int index, boolean forSkin)
    {
        return (index >= 0) && (forSkin ? this.skins.size() > index : this.gunsInv.size() > index);
    }

    protected boolean canPaint()
    {
        if (isIndexAvailable(this.current_shown_index, true))
        {
            AVAItemGun selected = this.skins.get(this.current_shown_index);
            if (selected.isMaster() || inventory.player.isCreative())
                return true;
            else
                return selected.getRecipe().canCraft(inventory.player, selected);
        }
        return false;
    }

    protected void onPaint()
    {
        if (canPaint())
        {
            if (!this.isIndexAvailable(this.current_shown_index, true) || !this.isIndexAvailable(this.current_index, false))
                return;
            AVAItemGun from = this.gunsInv.get(this.current_index);
            AVAItemGun to = this.skins.get(this.current_shown_index);
            DataToServerMessage.gunPainting(from, to);
        }
    }

    @OnlyIn(Dist.CLIENT)
    class SelectButton extends AbstractButton
    {
        private final boolean left;
        private final boolean forSkin;
        private boolean isAvailable;

        public SelectButton(int xIn, int yIn, boolean left, boolean forSkin)
        {
            super(xIn, yIn, 10, 22, Component.empty());
            this.left = left;
            this.forSkin = forSkin;
        }

        @Override
        public void onPress()
        {
            GunColouringGUI gui = GunColouringGUI.this;
            if (this.isAvailable)
            {
                if (this.forSkin)
                {
                    if (this.left)
                        //                        if (gui.isIndexAvailable(gui.current_shown_index - 1, true))
                        gui.setGunShown(gui.current_shown_index - 1);
                    else
                        //                        if (gui.isIndexAvailable(gui.current_shown_index + 1, true))
                        gui.setGunShown(gui.current_shown_index + 1);
                }
                else
                {
                    if (this.left)
                        //                        if (gui.isIndexAvailable(gui.current_shown_index - 1, false))
                        gui.setGunSelected(gui.current_index - 1);
                    else
                        //                        if (gui.isIndexAvailable(gui.current_shown_index + 1, false))
                        gui.setGunSelected(gui.current_index + 1);
                }
            }
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            this.isAvailable = GunColouringGUI.this.isIndexAvailable(getIndex(), this.forSkin);
            int x = this.left ? 96 : 66;
            if (this.isAvailable)
            {
                x += this.width;
                if (this.isHoveredOrFocused())
                    x += this.width;
            }
            stack.blit(GUN_CRAFTING_GUI, this.getX(), this.getY(), x, 219, this.width, this.height);
        }

        private int getIndex()
        {
            GunColouringGUI gui = GunColouringGUI.this;
            return (this.forSkin ? gui.current_shown_index : gui.current_index) + (this.left ? -1 : + 1) ;
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput output)
        {

        }
    }

    @OnlyIn(Dist.CLIENT)
    class PaintButton extends AbstractButton
    {
        private boolean selected;

        public PaintButton(int xIn, int yIn)
        {
            super(xIn, yIn, 22, 9, Component.empty());
        }

        @Override
        public void onPress()
        {
            if (this.active)
            {
                GunColouringGUI.this.onPaint();
                this.selected = true;
            }
        }

        @Override
        public void onRelease(double p_onRelease_1_, double p_onRelease_3_)
        {
            if (this.active)
                this.selected = false;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            this.active = GunColouringGUI.this.canPaint();
            int x = 0;
            if (this.active && this.isHoveredOrFocused())
                x += this.width;
            if (this.active && this.selected)
                x += this.width;
            if (!this.active)
                x += this.width * 2;
            stack.blit(GUN_CRAFTING_GUI, this.getX(), this.getY(), x, 219, this.width, this.height);
            stack.drawCenteredString(GunColouringGUI.this.font, Component.translatable("ava.gui.widget.paint"), this.getX() + this.width / 2, this.getY(), this.active ? 54783 : 16711680);
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput output)
        {

        }
    }
}
