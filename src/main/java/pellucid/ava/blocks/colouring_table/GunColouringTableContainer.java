package pellucid.ava.blocks.colouring_table;

import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVATEContainers;

public class GunColouringTableContainer extends AbstractContainerMenu
{
    private final ContainerLevelAccess worldPosCallable;

    public GunColouringTableContainer(int id, Inventory playerInventory)
    {
        this(id, playerInventory, ContainerLevelAccess.NULL);
    }

    public GunColouringTableContainer(int id, Inventory playerInventory, ContainerLevelAccess worldPosCallable)
    {
        super(AVATEContainers.GUN_COLOURING_TABLE_CONTAINER.get(), id);
        this.worldPosCallable = worldPosCallable;
        for(int r = 0; r < 3; r++)
            for(int c = 0; c < 9; c++)
                this.addSlot(new Slot(playerInventory, c + r * 9 + 9, 36 + c * 18, 137 + r * 18));
        for(int h = 0; h < 9; ++h)
            this.addSlot(new Slot(playerInventory, h, 36 + h * 18, 195));
    }

    @Override
    public boolean stillValid(Player playerIn)
    {
        return stillValid(this.worldPosCallable, playerIn, AVABlocks.GUN_COLOURING_TABLE.get());
    }

    @Override
    public ItemStack quickMoveStack(Player playerIn, int index)
    {
        return ItemStack.EMPTY;
    }
}
