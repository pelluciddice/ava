package pellucid.ava.blocks.colouring_table;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import pellucid.ava.blocks.AVATEContainers;

import javax.annotation.Nullable;

public class GunColouringTableTE extends BlockEntity implements MenuProvider
{
    public GunColouringTableTE(BlockPos pos, BlockState state)
    {
        super(AVATEContainers.GUN_COLOURING_TABLE_TE.get(), pos, state);
    }

    @Override
    public Component getDisplayName()
    {
        return Component.translatable("ava.container.gun_colouring_table");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int p_createMenu_1_, Inventory p_createMenu_2_, Player p_createMenu_3_)
    {
        return this.level == null ? null : new GunColouringTableContainer(p_createMenu_1_, p_createMenu_2_, ContainerLevelAccess.create(this.level, this.getBlockPos()));
    }
}
