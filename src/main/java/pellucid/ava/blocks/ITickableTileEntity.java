package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public interface ITickableTileEntity
{
    static <T extends BlockEntity> void ticker(Level level, BlockPos blockPos, BlockState blockState, T t)
    {
        if (t instanceof ITickableTileEntity te)
            te.tick();
    }

    void tick();
}
