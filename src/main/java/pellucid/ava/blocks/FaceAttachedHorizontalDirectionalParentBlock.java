package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FaceAttachedHorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.AttachFace;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.EnumProperty;

import javax.annotation.Nullable;

public class FaceAttachedHorizontalDirectionalParentBlock extends DirectionalShapedParentBlock
{
    public static final EnumProperty<AttachFace> FACE = BlockStateProperties.ATTACH_FACE;

    public FaceAttachedHorizontalDirectionalParentBlock(Properties properties, Block parent, double width, double thickness)
    {
        super(properties, parent, (16 - width) / 2, (16 - width) / 2, 16, (16 + width) / 2, (16 + width) / 2, 16 - thickness);
        double i = (16 - width) / 2;
        double i2 = (16 + width) / 2;
        shapes.put(Direction.DOWN, Block.box(i, 0, i, i2, thickness, i2));
        shapes.put(Direction.UP, Block.box(i, 16 - thickness, i, i2, 16, i2));
    }

    @Override
    public boolean canSurvive(BlockState p_53186_, LevelReader p_53187_, BlockPos p_53188_)
    {
        return FaceAttachedHorizontalDirectionalBlock.canAttach(p_53187_, p_53188_, getConnectedDirection(p_53186_).getOpposite());
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext p_53184_)
    {
        for(Direction direction : p_53184_.getNearestLookingDirections()) {
            BlockState blockstate;
            if (direction.getAxis() == Direction.Axis.Y) {
                blockstate = this.defaultBlockState().setValue(FACE, direction == Direction.UP ? AttachFace.CEILING : AttachFace.FLOOR).setValue(FACING, p_53184_.getHorizontalDirection());
            } else {
                blockstate = this.defaultBlockState().setValue(FACE, AttachFace.WALL).setValue(FACING, direction.getOpposite());
            }

            if (blockstate.canSurvive(p_53184_.getLevel(), p_53184_.getClickedPos())) {
                return blockstate;
            }
        }

        return null;
    }

    @Override
    public BlockState updateShape(BlockState p_53190_, Direction p_53191_, BlockState p_53192_, LevelAccessor p_53193_, BlockPos p_53194_, BlockPos p_53195_)
    {
        return getConnectedDirection(p_53190_).getOpposite() == p_53191_ && !p_53190_.canSurvive(p_53193_, p_53194_) ? Blocks.AIR.defaultBlockState() : super.updateShape(p_53190_, p_53191_, p_53192_, p_53193_, p_53194_, p_53195_);
    }

    protected static Direction getConnectedDirection(BlockState p_53201_)
    {
        switch(p_53201_.getValue(FACE))
        {
            case CEILING:
                return Direction.DOWN;
            case FLOOR:
                return Direction.UP;
            default:
                return p_53201_.getValue(FACING);
        }
    }
}
