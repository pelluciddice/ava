package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;

import javax.annotation.Nullable;

public interface IInteractable<R extends HitResult>
{
    default int cd()
    {
        return 0;
    }

    int getDuration();

    default void whileInteract(Level world, LivingEntity interacter)
    {

    }

    void interact(Level world, R result, @Nullable BlockPos pos, @Nullable Vec3 vector3d, LivingEntity interacter);

    default double maxDistance()
    {
        return 0.85F;
    }

    default boolean sharedProgress()
    {
        return false;
    }

    default int getSharedProgress()
    {
        return 0;
    }

    default boolean canInteract(R result, LivingEntity interactor, BlockPos pos)
    {
        return true;
    }

    default String getInteractableTip()
    {
        return null;
    }

    default boolean shouldDrawTipWrench()
    {
        return false;
    }

    static <R extends HitResult> boolean canInteract(R result, LivingEntity interactor, IInteractable<R> interactable, BlockPos pos, Vec3 from, Vec3 to)
    {
        return interactable.canInteract(result, interactor, pos) && interactable.maxDistance() >= from.distanceTo(to);
    }
}
