package pellucid.ava.blocks.crafting_table;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import pellucid.ava.client.components.ItemWidgets;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.init.Pistols;
import pellucid.ava.items.init.Rifles;
import pellucid.ava.items.init.Snipers;
import pellucid.ava.items.init.SubmachineGuns;
import pellucid.ava.items.throwables.HandGrenadeItem;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@OnlyIn(Dist.CLIENT)
public class GunCraftingGUI extends AbstractContainerScreen<GunCraftingTableContainer>
{
    private static final ResourceLocation GUN_CRAFTING_GUI = new ResourceLocation("ava:textures/gui/gun_crafting_table.png");
    private static final ResourceLocation WIDGET_TEXTURE = new ResourceLocation("ava:textures/gui/widgets.png");
    private int selected_tab = 0;
    private int selected_page = 0;
    private Item selected_item = Snipers.MOSIN_NAGANT.get();
    private final Set<TabButton> tabButtons = new HashSet<>();
    private final Map<Integer, ItemButton> itemButtons0 = new HashMap<>();
    private final Map<Integer, ItemButton> itemButtons1 = new HashMap<>();
    private final Map<Integer, ItemButton> itemButtons2 = new HashMap<>();
    private final Map<Integer, ItemButton> itemButtons3 = new HashMap<>();
    private final Map<Integer, ItemButton> itemButtons4 = new HashMap<>();
    private final List<CraftButton> craftButton = new ArrayList<>();
    private final List<ItemWidgets.DisplayItemWidget<GunCraftingGUI>> mainIngredients = new ArrayList<>();
    private final List<ItemWidgets.DisplayItemWidget<GunCraftingGUI>> subIngredients = new ArrayList<>();
    private Pair<ItemWidgets.DisplayItemWidget<GunCraftingGUI>, ItemWidgets.DisplayItemWidget<GunCraftingGUI>> productDisplay = Pair.empty();
    private final Inventory inventory;
    private ItemWidgets.DisplayItemWidget<GunCraftingGUI> infoToolTip;

    public GunCraftingGUI(GunCraftingTableContainer screenContainer, Inventory inv, Component titleIn)
    {
        super(screenContainer, inv, titleIn);
        this.imageWidth = 247;
        this.imageHeight = 219;
        this.selected_tab = 0;
        this.setSelectedPage(0);
        this.inventory = inv;
    }

    protected void clearAll()
    {
        renderables.clear();
        children().clear();
        this.tabButtons.clear();
        this.itemButtons0.clear();
        this.itemButtons1.clear();
        this.itemButtons2.clear();
        this.itemButtons3.clear();
        this.itemButtons4.clear();
        this.craftButton.clear();
        mainIngredients.clear();
        subIngredients.clear();
    }

    @Override
    protected void init()
    {
        super.init();
        clearAll();
        for (int i = 0; i < 5; i++)
        {
            TabButton button = new TabButton(this.leftPos + 10 + 22 * i, this.topPos + 7, i);
            if (i==0)
                button.setSelected(true);
            this.tabButtons.add(button);
            this.addRenderableWidget(button);
        }
        for (int i=0;i<5;i++)
        {
            int c = 0;
            int r = 0;
            int f = 0;
            for (Item item : getItemListFromTab(i))
            {
                ItemButton button = new ItemButton(this.leftPos + c++ * 18 + 16, this.topPos + r * 18 + 48, item);
                this.getItemButtonGroup(i).put(f, button);
                button.active = false;
                button.visible = false;
                this.addRenderableWidget(button);
                if (c == 6)
                {
                    c = 0;
                    r++;
                }
                if (r >= 3)
                    r = 0;
                f++;
            }
        }
        this.addRenderableWidget(new PageButton(this.leftPos + 60, this.topPos + 36, true));
        this.addRenderableWidget(new PageButton(this.leftPos + 60, this.topPos + 106, false));
        craftButton.add(addRenderableWidget(new CraftButton(this.leftPos + 213, this.topPos + 83, true)));
        craftButton.add(addRenderableWidget(new CraftButton(this.leftPos + 213, this.topPos + 105, false)));
        this.updatePages();
        int h = 135;
        for (int i = 0; i < 4; i++)
            mainIngredients.add(addRenderableWidget(ItemWidgets.display(this, null, leftPos + h + i * 18, topPos + 79)));
        h = 157;
        for (int i = 0; i < 3; i++)
            subIngredients.add(addRenderableWidget(ItemWidgets.display(this, null, leftPos + h + i * 18, topPos + 99)));
        productDisplay = Pair.of(addRenderableWidget(ItemWidgets.display(this, null, leftPos + 141, topPos + 13)), addRenderableWidget(ItemWidgets.display(this, null, leftPos + 137, topPos + 101)));
        addRenderableWidget(infoToolTip = ItemWidgets.toolTipHover(this, new ItemStack(Items.REDSTONE_TORCH), () -> {
            return selected_item instanceof IHasRecipe recipe && recipe.getRecipe().hasDescription() ? recipe.getRecipe().getCombinedDescription() : Component.empty();
        }, leftPos + 160, topPos + 16));
        setSelectedItem(Items.AIR, null);
    }

    protected void updatePages()
    {
        this.resetItemButtons();
        Map<Integer, ItemButton> group = this.getItemButtonGroup(this.selected_tab);
        for (int from = this.selected_page * 18; from < Math.min(group.size(), (1 + this.selected_page) * 18); from++)
        {
            ItemButton button = group.get(from);
            button.visible = true;
            button.active = true;
            button.setSelected(false);
        }
    }

    protected void resetItemButtons()
    {
        for (int i=0;i<5;i++)
        {
            Map<Integer, ItemButton> group = this.getItemButtonGroup(i);
            for (int d = 0; d < group.size(); d++)
            {
                ItemButton button = group.get(d);
                button.visible = false;
                button.active = false;
            }
        }
    }

    protected Map<Integer, ItemButton> getItemButtonGroup(int tabIndex)
    {
        return switch (tabIndex)
                {
                    default -> this.itemButtons0;
                    case 1 -> this.itemButtons1;
                    case 2 -> this.itemButtons2;
                    case 3 -> this.itemButtons3;
                    case 4 -> this.itemButtons4;
                };
    }
    protected List<Item> getItemListFromTab()
    {
        return this.getItemListFromTab(this.selected_tab);
    }

    protected List<Item> getItemListFromTab(int tab)
    {
        return AVACommonUtil.getCraftableMap().get(tab);
    }

    protected void onCraft(boolean forGun)
    {
        if (this.selected_item instanceof IHasRecipe)
        {
            Recipe recipe;
            if (this.selected_item instanceof AVAItemGun && !forGun)
                recipe = ((IHasRecipe) ((AVAItemGun) this.selected_item).getAmmoType(inventory.player.getMainHandItem())).getRecipe();
            else
                recipe = ((IHasRecipe) this.selected_item).getRecipe();
            if (recipe.canCraft(inventory.player, selected_item))
                DataToServerMessage.gunCrafting(this.selected_item, forGun);
        }
    }

    @Override
    public void render(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(stack, mouseX, mouseY, partialTicks);
        this.renderTooltip(stack, mouseX, mouseY);
    }

    @Override
    protected void renderBg(GuiGraphics stack, float partialTicks, int mouseX, int mouseY)
    {
        if (this.minecraft == null)
            return;
        if (!(this.selected_item instanceof IHasRecipe))
            this.craftButton.get(0).setItem(Items.AIR);
        this.craftButton.get(0).setItem(this.selected_item);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        int x = (this.width - this.imageWidth) / 2;
        int y = (this.height - this.imageHeight) / 2;
        stack.blit(GUN_CRAFTING_GUI, x, y, 0, 0, this.imageWidth, this.imageHeight);
        if (this.selected_item instanceof AVAItemGun)
            stack.blit(WIDGET_TEXTURE, x + 137, y + 35, 8, 0, 61, 40);
        else if (this.selected_item instanceof HandGrenadeItem)
            stack.blit(WIDGET_TEXTURE, x + 137, y + 35, 69, 0, 14, 40);
    }

    @Override
    protected void renderTooltip(GuiGraphics stack, int mouseX, int mouseY)
    {
        super.renderTooltip(stack, mouseX, mouseY);
        GunStatTypes text = null;
        if (AVACommonUtil.between(mouseX, this.leftPos + 137, this.leftPos + 137 + 34) && AVACommonUtil.between(mouseY, this.topPos + 35, this.topPos + 35 + 40))
        {
            int y = (mouseY - this.topPos - 33) / 15;
            if (selected_item instanceof AVAItemGun)
                text = y == 0 ? GunStatTypes.ATTACK_DAMAGE : y == 1 ? GunStatTypes.RANGE : GunStatTypes.STABILITY;
            else if (selected_item instanceof HandGrenadeItem)
                text = y == 0 ? GunStatTypes.ATTACK_DAMAGE : y == 1 ? GunStatTypes.RANGE : null;
        }
        else if (AVACommonUtil.between(mouseX, this.leftPos + 137 + 34 + 6, this.leftPos + 137 + 34 + 6 + 34 + 10) && AVACommonUtil.between(mouseY, this.topPos + 35, this.topPos + 35 + 40))
        {
            int y = (mouseY - this.topPos - 33) / 15;
            text = y == 0 ? GunStatTypes.ACCURACY : y == 1 ? GunStatTypes.FIRE_RATE : GunStatTypes.CAPACITY;
        }
        if (text != null)
            stack.renderTooltip(font, text.getTranslatedName(), mouseX, mouseY);
    }

    @Override
    protected void renderLabels(GuiGraphics stack, int mouseX, int mouseY)
    {
        Item item = this.selected_item;
        if (this.minecraft == null || !(this.selected_item instanceof IHasRecipe))
            return;
        Recipe recipe = ((IHasRecipe) item).getRecipe();
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        stack.pose().pushPose();
        stack.pose().scale(0.90F, 0.90F, 0.90F);
        stack.drawString(this.font, Component.translatable(item.getDescriptionId()).getString(), 182, 12, 16762880);
        stack.pose().popPose();
//        int i = 22;
//        if (recipe.hasDescription())
//            for (String text : recipe.getDescription())
//            {
//                this.drawDescription(stack, text, i);
//                i += 8;
//            }
        int x = 193;
        int y = 46;
        if (item instanceof AVAItemGun gun)
        {
            this.drawAttribute(stack, GunStatTypes.ATTACK_DAMAGE.getKey(), gun.getBulletDamage(ItemStack.EMPTY, false),            x,        y);
            this.drawAttribute(stack, GunStatTypes.RANGE.getKey(), gun.getRange(ItemStack.EMPTY, false),                           x,     y + 19);
            this.drawAttribute(stack, GunStatTypes.STABILITY.getKey(), gun.getStability(ItemStack.EMPTY, null, false),      x,      y + 37);
            this.drawAttribute(stack, GunStatTypes.ACCURACY.getKey(), gun.getAccuracy(ItemStack.EMPTY, null, false),     x + 62,    y);
            this.drawAttribute(stack, GunStatTypes.FIRE_RATE.getKey(), gun.getFireRate(ItemStack.EMPTY, false),                 x + 62, y + 19);
            this.drawAttribute(stack, GunStatTypes.CAPACITY.getKey(), gun.getCapacity(ItemStack.EMPTY, false),                  x + 62, y + 37);
        }
        else if (item instanceof HandGrenadeItem handGrenadeItem)
        {
            this.drawAttribute(stack, GunStatTypes.ATTACK_DAMAGE.getKey(), handGrenadeItem.getDamage(false), x, y);
            this.drawAttribute(stack, GunStatTypes.RANGE.getKey(), handGrenadeItem.getRange(),                 x, y + 19);
        }
//        for (GuiEventListener listener : children())
//            if (listener instanceof AbstractWidget widget && widget.isHoveredOrFocused())
//            {
//                widget.renderToolTip(stack, mouseX - this.leftPos, mouseY - this.topPos);
//                break;
//            }
    }

    protected void drawDescription(GuiGraphics stack, String text, int y)
    {
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        stack.pose().pushPose();
        stack.pose().scale(0.80F, 0.80F, 0.80F);
        stack.drawString(this.font, Component.translatable(text).getString(), 205, y, 16120058);
        stack.pose().popPose();
    }

    protected void drawAttribute(GuiGraphics stack, String attribute, float value, int x, int y)
    {
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        stack.pose().pushPose();
        stack.pose().scale(0.80F, 0.80F, 0.80F);
//        stack.drawString(this.font, Component.translatable(attribute).getString() + ": " + AVACommonUtil.round(value, 2), x, y, 16762880);
        stack.drawString(this.font, "" + AVACommonUtil.round(value, 2), x, y, 16762880);
        stack.pose().popPose();
    }

    protected boolean isPageAvailable(int page)
    {
        return page >= 0 && page * 18 <= getItemButtonGroup(selected_tab).size();
    }

    protected void setSelectedPage(int index)
    {
        if (isPageAvailable(index))
        {
            this.selected_page = index;
            this.updatePages();
        }
    }

    protected void setSelectedTab(int index, TabButton button)
    {
        for (TabButton button1 : GunCraftingGUI.this.tabButtons)
            if (button1 != button && button1.isSelected())
                button1.setSelected(false);
        button.setSelected(true);
        this.selected_tab = index;
        this.setSelectedPage(0);
    }

    protected void setSelectedItem(Item item, ItemButton button)
    {
        if (button != null)
        {
            for (int i = 0; i < 5; i++)
            {
                Map<Integer, ItemButton> group = this.getItemButtonGroup(i);
                for (int d = 0; d < group.size(); d++)
                {
                    ItemButton button1 = group.get(d);
                    if (button1 != button && button1.isSelected())
                        button1.setSelected(false);
                }
            }
            button.setSelected(true);
        }
        this.craftButton.get(0).setItem(item);
        productDisplay.getA().setItem(craftButton.get(0).item instanceof IHasRecipe ? new ItemStack(craftButton.get(0).item, ((IHasRecipe) craftButton.get(0).item).getRecipe().getResultCount()) : ItemStack.EMPTY);
        if (item instanceof AVAItemGun)
        {
            Item mag = ((AVAItemGun) item).getAmmoType(ItemStack.EMPTY);
            this.craftButton.get(1).setItem(mag);
        }
        else
            this.craftButton.get(1).setItem(Items.AIR);
        productDisplay.getB().setItem(craftButton.get(1).item instanceof IHasRecipe ? new ItemStack(craftButton.get(1).item, ((IHasRecipe) craftButton.get(1).item).getRecipe().getResultCount()) : ItemStack.EMPTY);
        this.selected_item = item;

        mainIngredients.forEach((b) -> b.setItem(Items.AIR));
        if (craftButton.get(0).item instanceof IHasRecipe)
        {
            Recipe recipe = ((IHasRecipe) craftButton.get(0).item).getRecipe();
            List<Ingredient> ingredients = recipe.getIngredients();
            for (int i = 0; i < ingredients.size(); i++)
                mainIngredients.get(i).setItem(new ItemStack(Recipe.pickFromIngredient(ingredients.get(i), Items.AIR), recipe.getCount(ingredients.get(i))));
        }

        subIngredients.forEach((b) -> b.setItem(Items.AIR));
        if (craftButton.get(1).item instanceof IHasRecipe)
        {
            Recipe recipe = ((IHasRecipe) craftButton.get(1).item).getRecipe();
            List<Ingredient> ingredients = ((IHasRecipe) craftButton.get(1).item).getRecipe().getIngredients();
            for (int i = 0; i < ingredients.size(); i++)
                subIngredients.get(i).setItem(new ItemStack(Recipe.pickFromIngredient(ingredients.get(i), Items.AIR), recipe.getCount(ingredients.get(i))));
        }

        infoToolTip.setItem(selected_item instanceof IHasRecipe recipe && recipe.getRecipe().hasDescription() ? new ItemStack(Items.REDSTONE_TORCH) : null);

    }

    @OnlyIn(Dist.CLIENT)
    class TabButton extends AbstractButton
    {
        private boolean selected;
        private final String name;
        private final Item item;
        private final int type;

        public TabButton(int xIn, int yIn, int type)
        {
            super(xIn, yIn, 22, 22, Component.empty());
            this.type = type;
            switch (type)
            {
                default -> {
                    this.item = Snipers.MOSIN_NAGANT.get();
                    this.name = "snipers";
                }
                case 1 -> {
                    this.item = Rifles.M4A1.get();
                    this.name = "rifles";
                }
                case 2 -> {
                    this.item = SubmachineGuns.X95R.get();
                    this.name = "submachine_guns";
                }
                case 3 -> {
                    this.item = Pistols.P226.get();
                    this.name = "pistols";
                }
                case 4 -> {
                    this.item = MiscItems.NRF_STANDARD_KEVLAR.get();
                    this.name = "miscs";
                }
            }
            setTooltip(Tooltip.create(createNarrationMessage(), null));
        }

        @Override
        public void onPress()
        {
            GunCraftingGUI.this.setSelectedTab(this.type, this);
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            int x = 0;
            if (this.selected || this.isHoveredOrFocused())
                x += this.width;
            stack.blit(GUN_CRAFTING_GUI, getX(), getY(), x, 219, this.width, this.height);
            stack.renderFakeItem(new ItemStack(this.item), getX() + 3, getY() + 3);
        }

//        @Override
//        public void renderToolTip(GuiGraphics stack, int p_renderToolTip_1_, int p_renderToolTip_2_)
//        {
//            GunCraftingGUI.this.renderTooltip(stack, Component.literal(I18n.get("ava.gui.tab." + this.name)), p_renderToolTip_1_, p_renderToolTip_2_);
//        }

        @Override
        protected MutableComponent createNarrationMessage()
        {
            return Component.translatable("ava.gui.tab." + this.name);
        }

        public boolean isSelected()
        {
            return this.selected;
        }

        public void setSelected(boolean selectedIn)
        {
            this.selected = selectedIn;
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {

        }
    }

    @OnlyIn(Dist.CLIENT)
    class ItemButton extends AbstractButton
    {
        private boolean selected;
        private final Item item;

        public ItemButton(int xIn, int yIn, Item item)
        {
            super(xIn, yIn, 18, 18, Component.empty());
            this.item = item;
            setTooltip(Tooltip.create(createNarrationMessage(), null));
        }

        @Override
        public void onPress()
        {
            GunCraftingGUI.this.setSelectedItem(this.item, this);
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            if (this.isSelected())
            {
                stack.fill(getX() + 2, getY() + 2, getX() + width, getY() + height, AVAConstants.AVA_HUD_COLOUR_BLUE.getRGB());
                //                RenderSystem.setShaderTexture(0, GUN_CRAFTING_GUI);
                //                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
                //                stack.blit(this.x + 2, this.y + 2, 45, 229, this.width, this.height);
            }
            ItemRenderer renderer = Minecraft.getInstance().getItemRenderer();
            stack.renderFakeItem(new ItemStack(this.item), getX() + 2, getY() + 2);
        }

//        @Override
//        public void renderToolTip(GuiGraphics stack, int p_renderToolTip_1_, int p_renderToolTip_2_)
//        {
//            GunCraftingGUI.this.renderTooltip(stack, Component.literal(I18n.get(this.item.getDescriptionId())), p_renderToolTip_1_, p_renderToolTip_2_);
//        }

        @Override
        protected MutableComponent createNarrationMessage()
        {
            return Component.translatable(this.item.getDescriptionId());
        }

        public boolean isSelected()
        {
            return this.selected;
        }

        public void setSelected(boolean selectedIn)
        {
            this.selected = selectedIn;
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {

        }
    }

    @OnlyIn(Dist.CLIENT)
    class PageButton extends AbstractButton
    {
        private final boolean up;
        private boolean isAvailable;

        public PageButton(int xIn, int yIn, boolean up)
        {
            super(xIn, yIn, 22, 11, Component.empty());
            this.up = up;
        }

        @Override
        public void onPress()
        {
            if (this.isAvailable)
                GunCraftingGUI.this.setSelectedPage(getPage());
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            this.isAvailable = GunCraftingGUI.this.isPageAvailable(getPage());
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            int x = this.up ? 176 : 110;
            if (this.isAvailable)
            {
                x += this.width;
                if (this.isHoveredOrFocused())
                    x += this.width;
            }
            stack.blit(GUN_CRAFTING_GUI, getX(), getY(), x, 219, this.width, this.height);
        }

        private int getPage()
        {
            return GunCraftingGUI.this.selected_page + (this.up ? -1 : + 1);
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {

        }
    }

    @OnlyIn(Dist.CLIENT)
    class CraftButton extends AbstractButton
    {
        //for magazine/ammo
        private boolean forGun;
        private boolean selected;
        private Item item = Items.AIR;

        public CraftButton(int xIn, int yIn, boolean forGun)
        {
            super(xIn, yIn, 22, 9, Component.empty());
            this.forGun = forGun;
        }

        @Override
        public void onPress()
        {
            if (this.active)
            {
                GunCraftingGUI.this.onCraft(this.forGun);
                this.selected = true;
            }
        }

        @Override
        public void onRelease(double p_onRelease_1_, double p_onRelease_3_)
        {
            if (this.active)
                this.selected = false;
        }

        public void setItem(Item item)
        {
            this.item = item;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            this.active = (this.item != Items.AIR) && ((IHasRecipe) this.item).getRecipe().canCraft(inventory.player, GunCraftingGUI.this.selected_item);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            int x = 44;
            if (this.active && this.isHoveredOrFocused())
                x += this.width;
            if (this.active && this.selected)
                x += this.width;
            if (!this.active)
                x += this.width * 2;
            stack.blit(GUN_CRAFTING_GUI, getX(), getY(), x, 219, this.width, this.height);
            stack.drawCenteredString(GunCraftingGUI.this.font, Component.translatable("ava.gui.widget.craft"), getX() + this.width / 2, getY(), this.active ? 54783 : 16711680);
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {

        }
    }
}
