package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.AttachFace;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class WallLightBlock extends FaceAttachedHorizontalDirectionalParentBlock
{
    public static final BooleanProperty LIT = BooleanProperty.create("lit");
    public WallLightBlock(Properties properties, Block parent, double width, double thickness)
    {
        super(properties, parent, width, thickness);
        registerDefaultState(defaultBlockState().setValue(LIT, true));
    }

    @Override
    public VoxelShape getCollisionShape(BlockState p_60572_, BlockGetter p_60573_, BlockPos p_60574_, CollisionContext p_60575_)
    {
        return Shapes.empty();
    }

    @Override
    public VoxelShape getShape(BlockState state, Direction direction)
    {
        AttachFace face = state.getValue(FACE);
        if (face == AttachFace.WALL)
            return super.getShape(state, direction);
        return shapes.get(face == AttachFace.CEILING ? Direction.UP : Direction.DOWN);
    }

    @Override
    public InteractionResult useWithoutItem(BlockState p_60503_, Level p_60504_, BlockPos p_60505_, Player p_60506_, BlockHitResult p_60508_)
    {
        p_60504_.setBlock(p_60505_, p_60503_.setValue(LIT, !p_60503_.getValue(LIT)), 3);
        p_60504_.playSound(p_60506_, p_60505_, SoundEvents.STONE_BUTTON_CLICK_ON, SoundSource.BLOCKS, 0.3F, 0.5F);
        p_60504_.gameEvent(p_60506_, GameEvent.BLOCK_ACTIVATE, p_60505_);
        return InteractionResult.sidedSuccess(p_60504_.isClientSide);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> p_49915_)
    {
        super.createBlockStateDefinition(p_49915_);
        p_49915_.add(LIT);
        p_49915_.add(FACE);
    }
}
