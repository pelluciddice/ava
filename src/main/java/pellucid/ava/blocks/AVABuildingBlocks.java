package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.DyeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ConcretePowderBlock;
import net.minecraft.world.level.block.IronBarsBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.registries.DeferredBlock;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.ItemRecipeHolder;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class AVABuildingBlocks
{
    public static final DeferredRegister.Blocks BLOCKS = DeferredRegister.createBlocks(AVA.MODID);
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);

    public static final List<ItemRecipeHolder> RECIPES = new ArrayList<>();

    public static final List<DeferredBlock<Block>> CLASSIC_BLOCK_EXTENSIONS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> WALL_THIN_PILLAR_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> THIN_PILLAR_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> WALL_LIGHT_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> PLANKS_FLOOR_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> PLASTER_BLOCKS = new ArrayList<>();
    public static final List<Supplier<StairBlock>> PLASTER_STAIRS = new ArrayList<>();
    public static final List<Supplier<SlabBlock>> PLASTER_SLABS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> PLASTER_BLOCKS_2 = new ArrayList<>();
    public static final List<Supplier<StairBlock>> PLASTER_STAIRS_2 = new ArrayList<>();
    public static final List<Supplier<SlabBlock>> PLASTER_SLABS_2 = new ArrayList<>();

    public static final List<DeferredBlock<Block>> SLOPE_BLOCKS = new ArrayList<>();

    public static final List<DeferredBlock<Block>> VANILLA_SERIES_EXTENSIONS_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> CONCRETE_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> CONCRETE_POWDER_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> WOOL_BLOCKS = new ArrayList<>();

    public static final Map<DeferredBlock<Block>, Integer> SLOPE_ANGLES = new HashMap<>();

    public static DeferredBlock<Block> SMOOTH_STONE_STAIRS;


    public static DeferredBlock<Block> COBBLED_SANDSTONE_TILE;
    public static DeferredBlock<Block> COBBLED_SANDSTONE_TILE_SLAB;
    public static DeferredBlock<Block> COBBLED_SANDSTONE_TILE_STAIRS;

    public static DeferredBlock<Block> GLASS_FENCE;
    public static DeferredBlock<Block> GLASS_FENCE_TALL;
    public static DeferredBlock<Block> GLASS_WALL;
    public static DeferredBlock<Block> GLASS_TRIG_WALL;
    public static DeferredBlock<Block> GLASS_TRIG_WALL_FLIPPED;

    public static DeferredBlock<Block> IRON_GRID;
    public static DeferredBlock<Block> HARDENED_IRON_BARS;

    private static final Lazy<VoxelShape> SLOPE_90_SHAPE = Lazy.of(() -> {
        return AVACommonUtil.slopeShape(16, 16, 0, 0, 16);
    });
    private static final Lazy<VoxelShape> SLOPE_225_SHAPE = Lazy.of(() -> {
        return AVACommonUtil.slopeShape(6.65F, 16, 0, 0, 16);
    });
    private static final Lazy<VoxelShape> SLOPE_2252_SHAPE = Lazy.of(() -> {
        return Shapes.or(AVACommonUtil.boxShapeShapes(0, 0, 0, 16, 6.65F, 16), AVACommonUtil.slopeShape(6.65F, 16, 6.65F, 0, 16));
    });
    private static final Lazy<VoxelShape> SLOPE_2253_SHAPE = Lazy.of(() -> {
        return Shapes.or(Shapes.or(AVACommonUtil.boxShapeShapes(0, 0, 0, 16, 13.3F, 16), AVACommonUtil.boxShapeShapes(0, 13.3F, 6.5F, 16, 16F, 16)), AVACommonUtil.slopeShape(2.7F, 6.5F, 13.3F, 0, 6));
    });
    private static final Lazy<VoxelShape> SLOPE_675_SHAPE = Lazy.of(() -> {
        return Shapes.or(AVACommonUtil.boxShapeShapes(0, 0, 6.625F, 16, 16, 16), AVACommonUtil.slopeShape(16, 6.625F, 0, 0, 10));
    });
    private static final Lazy<VoxelShape> SLOPE_6752_SHAPE = Lazy.of(() -> {
        return Shapes.or(AVACommonUtil.boxShapeShapes(0, 0, 13.25F, 16, 16, 16), AVACommonUtil.slopeShape(16, 6.625F, 0, 6.625F, 10));
    });
    private static final Lazy<VoxelShape> SLOPE_6753_SHAPE = Lazy.of(() -> {
        return AVACommonUtil.slopeShape(6.625F, 2.75F, 0, 13.25F, 5);
    });

    public static void registerAllItems()
    {
        for (int i = 0; i < AVABlockDyeColours.values().length; i++)
        {
            Recipe recipe = AVABlockDyeColours.values()[i].getRecipe();
            createBlockItem(CONCRETE_BLOCKS.get(i), null);
            createBlockItem(CONCRETE_POWDER_BLOCKS.get(i), new Recipe().mergeIngredients(recipe).addItem(Blocks.SAND, 4).addItem(Blocks.GRAVEL, 3).setResultCount(8));
            createBlockItem(WOOL_BLOCKS.get(i), new Recipe().mergeIngredients(recipe).addItem(Blocks.WHITE_WOOL).setResultCount(8));
        }
        createBlockItem(SMOOTH_STONE_STAIRS, AVAGunRecipes.SMOOTH_STONE_STAIRS);
        createBlockItem(COBBLED_SANDSTONE_TILE, AVAGunRecipes.COBBLED_SANDSTONE_TILE);
        createBlockItem(COBBLED_SANDSTONE_TILE_SLAB, AVAGunRecipes.COBBLED_SANDSTONE_TILE_SLAB);
        createBlockItem(COBBLED_SANDSTONE_TILE_STAIRS, AVAGunRecipes.COBBLED_SANDSTONE_TILE_STAIRS);
        createBlockItem(GLASS_FENCE, AVAGunRecipes.GLASS_FENCE);
        createBlockItem(GLASS_FENCE_TALL, AVAGunRecipes.GLASS_FENCE_TALL);
        createBlockItem(GLASS_WALL, AVAGunRecipes.GLASS_WALL);
        createBlockItem(GLASS_TRIG_WALL, AVAGunRecipes.GLASS_TRIG_WALL);
        createBlockItem(GLASS_TRIG_WALL_FLIPPED, AVAGunRecipes.GLASS_TRIG_WALL);
        createBlockItem(IRON_GRID, AVAGunRecipes.IRON_GRID);
        createBlockItem(HARDENED_IRON_BARS, AVAGunRecipes.HARDENED_IRON_GRID);
        for (int i = 0; i < PLASTER_BLOCKS.size(); i++)
        {
            Supplier<Block> parent = PLASTER_BLOCKS.get(i);
            createBlockItem(parent, new Recipe().addItem(Tags.Items.BONES).addItem(Blocks.CLAY).addItem(DyeItem.byColor(DyeColor.byId(i))).setResultCount(4));
            createBlockItem(PLASTER_STAIRS.get(i), new Recipe().addItem(() -> parent.get().asItem(), 6).setResultCount(4));
            createBlockItem(PLASTER_SLABS.get(i), new Recipe().addItem(() -> parent.get().asItem(), 3).setResultCount(6));
        }
        for (int i = 0; i < PLASTER_BLOCKS_2.size(); i++)
        {
            Supplier<Block> parent = PLASTER_BLOCKS_2.get(i);
            createBlockItem(parent, new Recipe().addItem(Tags.Items.BONES).addItem(Blocks.CLAY).mergeIngredients(AVABlockDyeColours.values()[i].getRecipe()).setResultCount(4));
            createBlockItem(PLASTER_STAIRS_2.get(i), new Recipe().addItem(() -> parent.get().asItem(), 6).setResultCount(4));
            createBlockItem(PLASTER_SLABS_2.get(i), new Recipe().addItem(() -> parent.get().asItem(), 3).setResultCount(6));
        }
        for (DeferredBlock<Block> block : WALL_LIGHT_BLOCKS)
            createWallLightBlockItem(block);
        for (DeferredBlock<Block> block : THIN_PILLAR_BLOCKS)
            createThinPillarBlockItem(block);
        for (DeferredBlock<Block> block : WALL_THIN_PILLAR_BLOCKS)
            createWallThinPillarBlockItem(block);
        for (DeferredBlock<Block> block : PLANKS_FLOOR_BLOCKS)
            createPlanksFloorBlockItem(block);
        for (DeferredBlock<Block> block : SLOPE_BLOCKS)
            createSlopeBlockItem(block, SLOPE_ANGLES.get(block));
    }


    static
    {
        SMOOTH_STONE_STAIRS = BLOCKS.register("smooth_stone_stairs", () -> new StairBlock(Blocks.SMOOTH_STONE.defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(Blocks.SMOOTH_STONE)));
        COBBLED_SANDSTONE_TILE = BLOCKS.register("cobbled_sandstone_tile" , () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(MapColor.RAW_IRON).strength(2.0F, 2.0F)));
        COBBLED_SANDSTONE_TILE_SLAB = BLOCKS.register("cobbled_sandstone_tile_slab" , () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(MapColor.RAW_IRON).strength(2.0F, 2.0F)));
        COBBLED_SANDSTONE_TILE_STAIRS = BLOCKS.register("cobbled_sandstone_tile_stairs" , () -> new StairBlock(COBBLED_SANDSTONE_TILE.get().defaultBlockState(), BlockBehaviour.Properties.ofFullCopy(COBBLED_SANDSTONE_TILE.get())));
        GLASS_FENCE = BLOCKS.register("glass_fence" , () -> new GlassFenceBlock(BlockBehaviour.Properties.of().sound(SoundType.GLASS).strength(0.5F).sound(SoundType.GLASS).noOcclusion(), 16.0F));
        GLASS_FENCE_TALL = BLOCKS.register("glass_fence_tall" , () -> new GlassFenceBlock(BlockBehaviour.Properties.of().sound(SoundType.GLASS).strength(0.5F).sound(SoundType.GLASS).noOcclusion(), 24.0F));
        GLASS_WALL = BLOCKS.register("glass_wall" , () -> new GlassWallBlock(BlockBehaviour.Properties.of().sound(SoundType.GLASS).strength(0.3F).sound(SoundType.GLASS).noOcclusion()));
        GLASS_TRIG_WALL = BLOCKS.register("glass_trig_wall" , () -> new GlassTrigWallBlock(BlockBehaviour.Properties.of().sound(SoundType.GLASS).strength(0.3F).sound(SoundType.GLASS).noOcclusion(), false));
        GLASS_TRIG_WALL_FLIPPED = BLOCKS.register("glass_trig_wall_flipped" , () -> new GlassTrigWallBlock(BlockBehaviour.Properties.of().sound(SoundType.GLASS).strength(0.3F).sound(SoundType.GLASS).noOcclusion(), true));
        IRON_GRID = BLOCKS.register("iron_grid" , () -> new FloorBlock(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(5.0F).sound(SoundType.METAL).noOcclusion(), () -> {
            VoxelShape shape = Shapes.box(0, 15, 0, 16, 16, 0.5F);
            shape = Shapes.or(shape, Shapes.box(0, 15, 15.5F, 16, 16, 16));
            for (int i = 0; i < 8; i++)
                shape = Shapes.or(shape, Shapes.box(i * 2 + 0.5F, 15, 0.5F, i * 2 + 1.5F, 16, 15.5F));
            return shape;
        }, () -> {
            VoxelShape shape = Shapes.box(0, 0, 0, 16, 1, 0.5F);
            shape = Shapes.or(shape, Shapes.box(0, 0, 15.5F, 16, 1, 16));
            for (int i = 0; i < 8; i++)
                shape = Shapes.or(shape, Shapes.box(i * 2 + 0.5F, 0, 0.5F, i * 2 + 1.5F, 1, 15.5F));
            return shape;
        }));
        HARDENED_IRON_BARS = BLOCKS.register("hardened_iron_bars", () -> new IronBarsBlock(BlockBehaviour.Properties.of().requiresCorrectToolForDrops().strength(5.0F, 6.0F).sound(SoundType.METAL).noOcclusion()));
        for (DyeColor colour : DyeColor.values())
        {
            MapColor c = AVAConstants.dyeToMaterialColour(colour);
            DeferredBlock<Block> plaster = AVACommonUtil.append(PLASTER_BLOCKS, BLOCKS.register("plaster_" + colour.getName(), () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(c).strength(2.5F))));
            AVACommonUtil.append(PLASTER_STAIRS, BLOCKS.register("plaster_stairs_" + colour.getName(), () -> new StairBlock(plaster.get().defaultBlockState(), BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(c).strength(2.5F))));
            AVACommonUtil.append(PLASTER_SLABS, BLOCKS.register("plaster_slabs_" + colour.getName(), () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(c).strength(2.5F))));
        }
        for (AVABlockDyeColours colour : AVABlockDyeColours.values())
        {
            String name = colour.getName();
            MapColor material = colour.getMapColor();
            DeferredBlock<Block> concrete = AVACommonUtil.append(VANILLA_SERIES_EXTENSIONS_BLOCKS, AVACommonUtil.append(CONCRETE_BLOCKS, BLOCKS.register(name + "_concrete", () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(material).requiresCorrectToolForDrops().strength(1.8F)))));
            AVACommonUtil.append(VANILLA_SERIES_EXTENSIONS_BLOCKS, AVACommonUtil.append(CONCRETE_POWDER_BLOCKS, BLOCKS.register(name + "_concrete_powder", () -> new ConcretePowderBlock(concrete.get(), BlockBehaviour.Properties.of().sound(SoundType.SAND).mapColor(material).strength(0.5F).sound(SoundType.SAND)))));
            AVACommonUtil.append(VANILLA_SERIES_EXTENSIONS_BLOCKS, AVACommonUtil.append(WOOL_BLOCKS, BLOCKS.register(name + "_wool", () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.WOOL).mapColor(material).strength(0.8F).sound(SoundType.WOOL)))));
            DeferredBlock<Block> plaster = AVACommonUtil.append(PLASTER_BLOCKS_2, BLOCKS.register("plaster_" + name, () -> new Block(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(material).strength(2.5F))));
            AVACommonUtil.append(PLASTER_STAIRS_2, BLOCKS.register("plaster_stairs_" + name, () -> new StairBlock(plaster.get().defaultBlockState(), BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(material).strength(2.5F))));
            AVACommonUtil.append(PLASTER_SLABS_2, BLOCKS.register("plaster_slabs_" + name, () -> new SlabBlock(BlockBehaviour.Properties.of().sound(SoundType.STONE).mapColor(material).strength(2.5F))));
        }
        for (Supplier<Block> block : AVAConstants.getAllWallLightMaterials())
            WALL_LIGHT_BLOCKS.add(BLOCKS.register(AVACommonUtil.getRegistryNameBlock(block).getPath() + "_wall_light", () -> new WallLightBlock(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(1.0F).noOcclusion().lightLevel((state) -> state.getValue(WallLightBlock.LIT) ? 15 : 0), block.get(), 4, 1)));
        for (Supplier<Block> block : AVAConstants.getAllCommonBlocks())
        {
            createThinPillarBlock(block);
            createWallThinPillarBlock(block);
            createSlopeBlock(block, SLOPE_90_SHAPE, 90, false);
            createSlopeBlock(block, SLOPE_225_SHAPE, 225, false);
            createSlopeBlock(block, SLOPE_2252_SHAPE, 2252, false);
            createSlopeBlock(block, SLOPE_2253_SHAPE, 2253, false);
            createSlopeBlock(block, SLOPE_675_SHAPE, 675, true);
            createSlopeBlock(block, SLOPE_6752_SHAPE, 6752, true);
            createSlopeBlock(block, SLOPE_6753_SHAPE, 6753, true);
        }
        for (Supplier<Block> block : AVAConstants.getClassicStrippedWoods())
            createPlankFloor(block);
    }

    private static DeferredItem<Item> createParentedBlockItem(DeferredBlock<Block> block, Pair<String, String> additionNames, String remove, Recipe recipe)
    {
        RECIPES.add(new ItemRecipeHolder(() -> block.get().asItem(), recipe));
        return AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register(AVACommonUtil.getRegistryNameBlock(block).getPath(), () -> new BlockItem(block.get(), new Item.Properties())
        {
            @Override
            public Component getName(ItemStack stack)
            {
                String name = Component.translatable(getDescriptionId(stack).replace("ava.", BuiltInRegistries.BLOCK.getKey(block.get() instanceof IParentedBlock ? ((IParentedBlock) block.get()).getParent() : block.get()).getNamespace() + ".").replace(remove, "")).getString();
                if (additionNames.hasA())
                    name = Component.translatable(additionNames.getA()).getString() + " " + name;
                if (additionNames.hasB())
                    name = name + " " + Component.translatable(additionNames.getB()).getString();
                return Component.literal(name);
            }
        }));
    }

    private static DeferredItem<Item> createBlockItem(Supplier<? extends Block> block, @Nullable Recipe recipe)
    {
        if (recipe != null)
            RECIPES.add(new ItemRecipeHolder(() -> block.get().asItem(), recipe));
        return AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register(AVACommonUtil.getRegistryNameBlock(block).getPath(), () -> new BlockItem(block.get(), new Item.Properties())));
    }

    private static DeferredItem<Item> createThinPillarBlockItem(DeferredBlock<Block> block)
    {
        return createParentedBlockItem(block, Pair.of(null, "ava.block.pillar"), "thin_", new Recipe().addItem(() -> ((IParentedBlock) block.get()).getParent().asItem()).setResultCount(2));
    }

    private static DeferredItem<Item> createWallThinPillarBlockItem(DeferredBlock<Block> block)
    {
        return createParentedBlockItem(block, Pair.of(null, "ava.block.pillar_wall"), "wall_thin_", new Recipe().addItem(() -> ((IParentedBlock) block.get()).getParent().asItem()).setResultCount(2));
    }

    private static DeferredItem<Item> createWallLightBlockItem(DeferredBlock<Block> block)
    {
        return createParentedBlockItem(block, Pair.of(null, "ava.block.wall_light"), "_wall_light", new Recipe().addItem(() -> ((IParentedBlock) block.get()).getParent().asItem(), 2).addItem(Blocks.REDSTONE_LAMP).setResultCount(4));
    }

    private static DeferredItem<Item> createPlanksFloorBlockItem(DeferredBlock<Block> block)
    {
        return createParentedBlockItem(block, Pair.of(null, "ava.block.planks_floor"), "_planks_floor", new Recipe().addItem(() -> ((IParentedBlock) block.get()).getParent().asItem()).setResultCount(2));
    }

    private static DeferredItem<Item> createSlopeBlockItem(DeferredBlock<Block> block, int angle)
    {
        String slope = "slope_" + angle;
        return createParentedBlockItem(block, Pair.of(null, "ava.block." + slope), "_" + slope, new Recipe().addItem(() -> ((IParentedBlock) block.get()).getParent().asItem()).setResultCount(1));
    }

    private static DeferredBlock<Block> createPlankFloor(Supplier<Block> parent)
    {
        double third = 1 / 3.0D;
        double width = 4 + third;
        DeferredBlock<Block> block = BLOCKS.register(AVACommonUtil.getRegistryNameBlock(parent).getPath() + "_planks_floor", () -> new FloorParentBlock(BlockBehaviour.Properties.ofFullCopy(parent.get()), parent.get(), () -> AVACommonUtil.mergeShapes(
                Shapes.box(0, 14, 0, 16, 15, 0.5F),
                Shapes.box(0, 14, 15.5F, 16, 15, 16),
                Shapes.box(0.5F, 15, 0, 0.5F + width, 16, 16),
                Shapes.box(0.5F + width + 1, 15, 0, 0.5F + width + 1 + width, 16, 16),
                Shapes.box(0.5F + width + 1 + width + 1, 15, 0, 0.5F + width + 1 + width + 1 + width, 16, 16)), () -> AVACommonUtil.mergeShapes(
                Shapes.box(0, 0, 0, 16, 1, 0.5F),
                Shapes.box(0, 0, 15.5F, 16, 1, 16),
                Shapes.box(0.5F, 1, 0, 0.5F + width, 2, 16),
                Shapes.box(0.5F + width + 1, 1, 0, 0.5F + width + 1 + width, 2, 16),
                Shapes.box(0.5F + width + 1 + width + 1, 1, 0, 0.5F + width + 1 + width + 1 + width, 2, 16))));
        CLASSIC_BLOCK_EXTENSIONS.add(block);
        PLANKS_FLOOR_BLOCKS.add(block);
        return block;
    }

    private static DeferredBlock<Block> createThinPillarBlock(Supplier<Block> parent)
    {
        DeferredBlock<Block> block = BLOCKS.register("thin_" + AVACommonUtil.getRegistryNameBlock(parent).getPath(), () -> new DirectionalShapedParentBlock(BlockBehaviour.Properties.ofFullCopy(parent.get()), parent.get(), 4, 0, 0, 12, 16, 16) {
            @Override
            protected boolean isPathfindable(BlockState state, PathComputationType type)
            {
                return false;
            }
        });
        CLASSIC_BLOCK_EXTENSIONS.add(block);
        THIN_PILLAR_BLOCKS.add(block);
        return block;
    }

    private static DeferredBlock<Block> createWallThinPillarBlock(Supplier<Block> parent)
    {
        DeferredBlock<Block> block = BLOCKS.register("wall_thin_" + AVACommonUtil.getRegistryNameBlock(parent).getPath(), () -> new DirectionalShapedParentBlock(BlockBehaviour.Properties.ofFullCopy(parent.get()), parent.get(), 0, 0, 0, 16, 16, 8) {

            @Override
            protected boolean isPathfindable(BlockState state, PathComputationType type)
            {
                return false;
            }

            @org.jetbrains.annotations.Nullable
            @Override
            public BlockState getStateForPlacement(BlockPlaceContext context)
            {
                Direction direction = context.getClickedFace();
                if (direction.getAxis() != Direction.Axis.Y)
                    return defaultBlockState().setValue(FACING, direction.getOpposite());
                return super.getStateForPlacement(context);
            }
        });
        CLASSIC_BLOCK_EXTENSIONS.add(block);
        WALL_THIN_PILLAR_BLOCKS.add(block);
        return block;
    }

    private static DeferredBlock<Block> createSlopeBlock(Supplier<Block> parent, Lazy<VoxelShape> shape, int angle, boolean bumpFromEdge)
    {
        DeferredBlock<Block> block = BLOCKS.register(AVACommonUtil.getRegistryNameBlock(parent).getPath() + "_slope_" + angle, () -> new DirectionalShapedParentBlock(BlockBehaviour.Properties.ofFullCopy(parent.get()).noOcclusion().lightLevel((s) -> 1), parent.get(), shape) {

            @org.jetbrains.annotations.Nullable
            @Override
            public BlockState getStateForPlacement(BlockPlaceContext context)
            {
                return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
            }

            @Override
            public void entityInside(BlockState p_60495_, Level p_60496_, BlockPos p_60497_, Entity p_60498_)
            {
                if (bumpFromEdge)
                    p_60498_.setDeltaMovement(p_60498_.getDeltaMovement().add(new Vec3(p_60495_.getValue(DirectionalShapedBlock.FACING).step()).scale(0.1F)));
            }
        });
        CLASSIC_BLOCK_EXTENSIONS.add(block);
        SLOPE_BLOCKS.add(block);
        SLOPE_ANGLES.put(block, angle);
        return block;
    }
}
