package pellucid.ava.blocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.ticks.TickPriority;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;

public class AmmoKitSupplier extends AVAHorizontalBlock
{
    public static final VoxelShape NEGATIVE_Z = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 16.0D, 10.0D);
    public static final VoxelShape POSITIVE_Z = Block.box(0.0D, 0.0D, 6.0D, 16.0D, 16.0D, 16.0D);
    public static final VoxelShape NEGATIVE_X = Block.box(0.0D, 0.0D, 0.0D, 10.0D, 16.0D, 16.0D);
    public static final VoxelShape POSITIVE_X = Block.box(6.0D, 0.0D, 0.0D, 16.0D, 16.0D, 16.0D);
    public static final BooleanProperty ACTIVE = BooleanProperty.create("active");

    public AmmoKitSupplier()
    {
        super(BlockBehaviour.Properties.of().sound(SoundType.METAL).noCollission().strength(3.0F, 3.0F).noLootTable());
        registerDefaultState(defaultBlockState()
                .setValue(FACING, Direction.NORTH)
                .setValue(ACTIVE, true));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
    {
        super.createBlockStateDefinition(builder);
        builder.add(ACTIVE);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
    {
        return this.getShape(state, worldIn, pos, context);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
    {
        switch (state.getValue(FACING))
        {
            case NORTH:
            default:
                return NEGATIVE_Z;
            case SOUTH:
                return POSITIVE_Z;
            case WEST:
                return NEGATIVE_X;
            case EAST:
                return POSITIVE_X;
        }
    }

    @Override
    public void entityInside(BlockState state, Level worldIn, BlockPos pos, Entity entityIn)
    {
        if (!(entityIn instanceof Player player) || worldIn.isClientSide())
            return;
        ItemStack stack = player.getMainHandItem();
        if (stack.getItem() instanceof AVAItemGun)
        {
            if (state.getValue(ACTIVE))
            {
                AVAWeaponUtil.addAmmoToPlayer(player, stack, AVAServerConfig.isCompetitiveModeActivated() ? 3 : 6);
                worldIn.setBlockAndUpdate(pos, defaultBlockState().setValue(FACING, state.getValue(FACING)).setValue(ACTIVE, false));
                if (worldIn.getGameTime() % 8 == 0)
                    worldIn.playSound(null, pos.getX(), pos.getY(), pos.getZ(), AVASounds.AMMO_KIT_SUPPLIER_CONSUME.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
            }
            worldIn.scheduleTick(pos, this, AVAServerConfig.isCompetitiveModeActivated() ? 10 : 40, TickPriority.VERY_LOW);
        }
    }

    @Override
    public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, RandomSource rand)
    {
        worldIn.setBlockAndUpdate(pos, defaultBlockState().setValue(FACING, state.getValue(FACING)).setValue(ACTIVE, true));
    }
}
