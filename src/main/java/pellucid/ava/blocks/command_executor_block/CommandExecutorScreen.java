package pellucid.ava.blocks.command_executor_block;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractCommandBlockEditScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.BaseCommandBlock;
import net.neoforged.neoforge.network.PacketDistributor;
import pellucid.ava.client.components.IntTextField;
import pellucid.ava.packets.UpdateCommandExecutorMessage;
import pellucid.ava.util.AVAConstants;

public class CommandExecutorScreen extends AbstractCommandBlockEditScreen
{
    private final CommandExecutorTE commandExecutor;
    protected IntTextField<CommandExecutorScreen> constantEdit;
    protected IntTextField<CommandExecutorScreen> randFromEdit;
    protected IntTextField<CommandExecutorScreen> randToEdit;

    public CommandExecutorScreen(CommandExecutorTE commandExecutor)
    {
        this.commandExecutor = commandExecutor;
    }

    @Override
    public void render(GuiGraphics stack, int p_97673_, int p_97674_, float p_97675_)
    {
        super.render(stack, p_97673_, p_97674_, p_97675_);
        stack.drawCenteredString(font, "+", width / 2 - 39, height / 2 - 5, AVAConstants.AVA_HUD_TEXT_GRAY);
        stack.drawCenteredString(font, "~", width / 2 + 39, height / 2 - 3, AVAConstants.AVA_HUD_TEXT_GRAY);
    }

    @Override
    protected void init()
    {
        super.init();
        constantEdit = createEditBox(-100, commandExecutor.constantDelay, "ava.gui.description.command_executor_delay_constant");
        randFromEdit = createEditBox(-20, commandExecutor.minRandDelay, "ava.gui.description.command_executor_delay_rand_from");
        randToEdit = createEditBox(60, commandExecutor.maxRandDelay, "ava.gui.description.command_executor_delay_rand_to");

        updateGui();
    }

    public void updateGui()
    {
        BaseCommandBlock basecommandblock = getCommandBlock();
        commandEdit.setValue(basecommandblock.getCommand());
        boolean trackOutput = basecommandblock.isTrackOutput();
        outputButton.setValue(trackOutput);
        updatePreviousOutput(trackOutput);
    }

    private IntTextField<CommandExecutorScreen> createEditBox(int xOffset, int defaultValue, String name)
    {
        IntTextField<CommandExecutorScreen> box = new IntTextField<>(this, width / 2 + xOffset, height / 2 - 8, 40, 16, 0, 999999, defaultValue, Component.translatable(name))
        {
            @Override
            public void renderWidget(GuiGraphics stack, int p_94161_, int p_94162_, float p_94163_)
            {
                super.renderWidget(stack, p_94161_, p_94162_, p_94163_);
                stack.drawCenteredString(font, getMessage(), getX() + width / 2, getY() - 10, AVAConstants.AVA_HUD_TEXT_GRAY);
            }
        };
        box.setMaxLength(6);
        box.setEditable(true);
        return addRenderableWidget(box);
    }

    @Override
    public int getPreviousY()
    {
        return 165;
    }

    @Override
    protected void populateAndSendPacket(BaseCommandBlock command)
    {
        if (randFromEdit.getInt() > randToEdit.getInt())
        {
            randFromEdit.setValue("");
            randToEdit.setValue("");
        }
       PacketDistributor.sendToServer(new UpdateCommandExecutorMessage(BlockPos.containing(command.getPosition()), commandEdit.getValue(), command.isTrackOutput(), constantEdit.getInt(), randFromEdit.getInt(), randToEdit.getInt()));
    }

    @Override
    public BaseCommandBlock getCommandBlock()
    {
        return commandExecutor.getCommandBlock();
    }
}
