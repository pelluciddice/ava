package pellucid.ava.blocks.command_executor_block;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.BaseCommandBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec2;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.util.DataTypes;

import java.util.Random;

public class CommandExecutorTE extends BlockEntity implements ITickableTileEntity
{
    private final BaseCommandBlock commandBlock = new BaseCommandBlock()
    {
        @Override
        public ServerLevel getLevel()
        {
            return (ServerLevel) CommandExecutorTE.this.level;
        }

        @Override
        public void onUpdated()
        {
            BlockState blockstate = CommandExecutorTE.this.level.getBlockState(CommandExecutorTE.this.worldPosition);
            this.getLevel().sendBlockUpdated(CommandExecutorTE.this.worldPosition, blockstate, blockstate, 3);
        }

        @Override
        public Vec3 getPosition()
        {
            return Vec3.atCenterOf(CommandExecutorTE.this.worldPosition);
        }

        @Override
        public CommandSourceStack createCommandSourceStack()
        {
            return new CommandSourceStack(this, Vec3.atCenterOf(CommandExecutorTE.this.worldPosition), Vec2.ZERO, this.getLevel(), 2, this.getName().getString(), this.getName(), this.getLevel().getServer(), (Entity) null);
        }

        @Override
        public boolean isValid()
        {
            return !CommandExecutorTE.this.isRemoved();
        }
    };

    private final Random rand = new Random();
    public int constantDelay;
    public int minRandDelay;
    public int maxRandDelay;
    private int delay;

    public CommandExecutorTE(BlockPos pos, BlockState state)
    {
        super(AVATEContainers.COMMAND_EXECUTOR_TE.get(), pos, state);
    }

    @Override
    public void tick()
    {
        if (level != null && !level.isClientSide() && level.isLoaded(getBlockPos()))
        {
            if (delay <= 0)
            {
                commandBlock.performCommand(level);
                if (commandBlock.isTrackOutput())
                    level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), Block.UPDATE_CLIENTS);
                delay = constantDelay + rand.nextInt(maxRandDelay - minRandDelay + 1);
            }
            delay--;
        }
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket()
    {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag(HolderLookup.Provider lookup)
    {
        CompoundTag compound = new CompoundTag();
        commandBlock.save(compound, lookup);
        DataTypes.INT.write(compound, "constantDelay", constantDelay);
        DataTypes.INT.write(compound, "minRandDelay", minRandDelay);
        DataTypes.INT.write(compound, "maxRandDelay", maxRandDelay);
        DataTypes.INT.write(compound, "delay", delay);
        return compound;
    }

    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt, HolderLookup.Provider lookup)
    {
        handleUpdateTag(pkt.getTag(), lookup);
    }

    @Override
    public void handleUpdateTag(CompoundTag compound, HolderLookup.Provider lookup)
    {
        commandBlock.load(compound, lookup);
        constantDelay = DataTypes.INT.read(compound, "constantDelay");
        minRandDelay = DataTypes.INT.read(compound, "minRandDelay");
        maxRandDelay = DataTypes.INT.read(compound, "maxRandDelay");
        delay = DataTypes.INT.read(compound, "delay");
    }

    public BaseCommandBlock getCommandBlock()
    {
        return commandBlock;
    }

    @Override
    protected void saveAdditional(CompoundTag compound, HolderLookup.Provider lookup)
    {
        super.saveAdditional(compound, lookup);
        commandBlock.save(compound, lookup);
        DataTypes.INT.write(compound, "constantDelay", constantDelay);
        DataTypes.INT.write(compound, "minRandDelay", minRandDelay);
        DataTypes.INT.write(compound, "maxRandDelay", maxRandDelay);
        DataTypes.INT.write(compound, "delay", delay);
    }

    @Override
    public void loadAdditional(CompoundTag compound, HolderLookup.Provider lookup)
    {
        super.loadAdditional(compound, lookup);
        commandBlock.load(compound, lookup);
        constantDelay = DataTypes.INT.read(compound, "constantDelay");
        minRandDelay = DataTypes.INT.read(compound, "minRandDelay");
        maxRandDelay = DataTypes.INT.read(compound, "maxRandDelay");
        delay = DataTypes.INT.read(compound, "delay");
    }
}
