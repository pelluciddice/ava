package pellucid.ava.blocks.command_executor_block;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.ITickableTileEntity;
import pellucid.ava.packets.DataToClientMessage;

public class CommandExecutorBlock extends Block implements EntityBlock
{
    public CommandExecutorBlock()
    {
        super(Properties.of().sound(SoundType.METAL).strength(-1.0F, 3600000.0F).noLootTable());
    }

    @Override
    public InteractionResult useWithoutItem(BlockState state, Level world, BlockPos pos, Player player, BlockHitResult result)
    {
        BlockEntity blockentity = world.getBlockEntity(pos);
        if (blockentity instanceof CommandExecutorTE && player.canUseGameMasterBlocks() && player instanceof ServerPlayer)
        {
//            world.sendBlockUpdated(pos, state, state, Block.UPDATE_ALL);
            DataToClientMessage.openCommandExecutorScreen((ServerPlayer) player, pos);
            return InteractionResult.sidedSuccess(world.isClientSide);
        }
        return InteractionResult.PASS;
    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos p_153215_, BlockState p_153216_)
    {
        return new CommandExecutorTE(p_153215_, p_153216_);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> p_153214_)
    {
        return ITickableTileEntity::ticker;
    }
}
