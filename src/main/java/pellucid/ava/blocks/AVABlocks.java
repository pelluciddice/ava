package pellucid.ava.blocks;

import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.level.material.PushReaction;
import net.neoforged.neoforge.registries.DeferredBlock;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.blocks.boost_block.BoostBlock;
import pellucid.ava.blocks.builders_table.BuildersTable;
import pellucid.ava.blocks.colouring_table.GunColouringTable;
import pellucid.ava.blocks.command_executor_block.CommandExecutorBlock;
import pellucid.ava.blocks.crafting_table.GunCraftingTable;
import pellucid.ava.blocks.explosive_barrel.ExplosiveBarrel;
import pellucid.ava.blocks.mastery_table.builders_table.MasteryTable;
import pellucid.ava.blocks.modifying_table.GunModifyingTable;
import pellucid.ava.blocks.preset_table.PresetTable;
import pellucid.ava.blocks.repairable.RepairableFlowerPotBlock;
import pellucid.ava.blocks.repairable.RepairableGlassBlock;
import pellucid.ava.blocks.repairable.RepairableGlassPaneBlock;
import pellucid.ava.blocks.repairable.RepairableStainedGlassBlock;
import pellucid.ava.blocks.repairable.RepairableStainedGlassPaneBlock;
import pellucid.ava.blocks.rpg_box.RPGBoxBlock;
import pellucid.ava.fluids.AVAFluids;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.recipes.AVAGunRecipes;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class AVABlocks
{
    public static final DeferredRegister.Blocks BLOCKS = DeferredRegister.createBlocks(AVA.MODID);
    public static final DeferredRegister.Items ITEMS = DeferredRegister.createItems(AVA.MODID);

    public static final DeferredBlock<Block> GUN_CRAFTING_TABLE = BLOCKS.register("gun_crafting_table", () -> new GunCraftingTable(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(2.0F, 3.0F).noOcclusion()));
    public static final DeferredBlock<Block> GUN_COLOURING_TABLE = BLOCKS.register("gun_colouring_table", () -> new GunColouringTable(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(2.0F, 3.0F)));
    public static final DeferredBlock<Block> GUN_MODIFYING_TABLE = BLOCKS.register("gun_modifying_table", () -> new GunModifyingTable(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(2.0F, 3.0F)));
    public static final DeferredBlock<Block> BUILDERS_TABLE = BLOCKS.register("builders_table", () -> new BuildersTable(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(2.0F, 3.0F).lightLevel((state) -> 4).noOcclusion()));
    public static final DeferredBlock<Block> MASTERY_TABLE = BLOCKS.register("mastery_table", () -> new MasteryTable(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(2.0F, 3.0F).lightLevel((state) -> 7)));
    public static final DeferredBlock<Block> PRESET_TABLE = BLOCKS.register("preset_table", () -> new PresetTable(BlockBehaviour.Properties.of().sound(SoundType.METAL).strength(-1.0F, 3600000.0F).noLootTable().noOcclusion().lightLevel((state) -> 7)));

    public static final DeferredBlock<Block> AMMO_KIT_SUPPLIER = BLOCKS.register("ammo_kit_supplier", () -> new AmmoKitSupplier());

    public static final DeferredBlock<Block> ATTACK_DAMAGE_BOOST_BLOCK = BLOCKS.register("attack_damage_boost_block", () -> new BoostBlock());
    public static final DeferredBlock<Block> HEALTH_BOOST_BLOCK = BLOCKS.register("health_boost_block", () -> new BoostBlock());

    public static final DeferredBlock<Block> EXPLOSIVE_BARREL = BLOCKS.register("explosive_barrel", () -> new ExplosiveBarrel());

    public static final DeferredBlock<Block> TEST_BLOCK = BLOCKS.register("test_block", () -> new TestBlock());

    public static final DeferredBlock<Block> CONTROLLER = BLOCKS.register("controller", () -> new ControllerBlock());
    public static final DeferredBlock<Block> RPG_BOX = BLOCKS.register("rpg_box", () -> new RPGBoxBlock());

    public static final DeferredBlock<Block> SITE_A = BLOCKS.register("site_a", () -> new SiteBlock());
    public static final DeferredBlock<Block> SITE_B = BLOCKS.register("site_b", () -> new SiteBlock());

    public static final DeferredBlock<Block> VOID_WATER = BLOCKS.register("void_water", () -> new LiquidBlock(AVAFluids.VOID_WATER.get(), BlockBehaviour.Properties.of().mapColor(MapColor.WATER).replaceable().noCollission().strength(100.0F).pushReaction(PushReaction.DESTROY).noLootTable().liquid().sound(SoundType.EMPTY)) {
        @Override
        public void entityInside(BlockState state, Level worldIn, BlockPos pos, Entity entityIn)
        {
            if (!worldIn.isClientSide() && !(entityIn instanceof Player && (((Player) entityIn).isCreative() || entityIn.isSpectator())))
                entityIn.hurt(worldIn.damageSources().fellOutOfWorld(), Integer.MAX_VALUE);
        }
    });
    public static final DeferredBlock<Block> COMMAND_EXECUTOR = BLOCKS.register("command_executor", () -> new CommandExecutorBlock());

    public static final DeferredBlock<Block> REPAIRABLE_FLOWER_POT = BLOCKS.register("repairable_flower_pot", () -> new RepairableFlowerPotBlock());

    public static final List<Supplier<Block>> REPAIRABLES = new ArrayList<>();
    public static final List<DeferredBlock<Block>> GLASS_BLOCKS = new ArrayList<>();
    public static final List<DeferredBlock<Block>> GLASS_PANE_BLOCKS = new ArrayList<>();
    static
    {
        for (DyeColor colour : DyeColor.values())
        {
            createRepairableBlock(createRepairableStainedGlass(colour));
            createRepairableBlock(createRepairableStainedGlassPane(colour));
        }
        GLASS_BLOCKS.add(createRepairableBlock(BLOCKS.register("repairable_glass", () -> new RepairableGlassBlock())));
        GLASS_PANE_BLOCKS.add(createRepairableBlock(BLOCKS.register("repairable_glass_pane", () -> new RepairableGlassPaneBlock())));
    }

    private static DeferredItem<Item> itemBlock(DeferredBlock<Block> block)
    {
        return AVAItemGroups.putItem(AVAItemGroups.MAIN, ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties())));
    }

    private static DeferredBlock<Block>createRepairableBlock(DeferredBlock<Block>block)
    {
        REPAIRABLES.add(block);
        return block;
    }

    public static final DeferredItem[] CACHED_ITEMS = new DeferredItem[3];

    public static void registerAllItems()
    {
        CACHED_ITEMS[0] = AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, createBlockItemWithRecipe(AMMO_KIT_SUPPLIER, new Item.Properties().rarity(Rarity.EPIC), AVAGunRecipes.AMMO_KIT_SUPPLIER));
        CACHED_ITEMS[1] = AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, createBlockItemWithRecipe(EXPLOSIVE_BARREL, new Item.Properties(), AVAGunRecipes.EXPLOSIVE_BARREL));
        CACHED_ITEMS[2] = AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, createBlockItemWithRecipe(CONTROLLER, new Item.Properties(), AVAGunRecipes.CONTROLLER));
        itemBlock(GUN_CRAFTING_TABLE);
        itemBlock(GUN_COLOURING_TABLE);
        itemBlock(GUN_MODIFYING_TABLE);
        itemBlock(BUILDERS_TABLE);
        itemBlock(MASTERY_TABLE);
        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register("preset_table", () -> new BlockItem(PRESET_TABLE.get(), new Item.Properties().rarity(Rarity.EPIC))));
        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register("attack_damage_boost_block", () -> new BlockItem(ATTACK_DAMAGE_BOOST_BLOCK.get(), new Item.Properties().rarity(Rarity.EPIC))));
        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register("health_boost_block", () -> new BlockItem(HEALTH_BOOST_BLOCK.get(), new Item.Properties().rarity(Rarity.EPIC))));
        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register("rpg_box", () -> new BlockItem(RPG_BOX.get(), new Item.Properties().rarity(Rarity.EPIC))));
        ITEMS.register("test_block", () -> new BlockItem(TEST_BLOCK.get(), new Item.Properties()));
        createSiteBlockItem(SITE_A);
        createSiteBlockItem(SITE_B);

        AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register("command_executor", () -> new BlockItem(COMMAND_EXECUTOR.get(), new Item.Properties().rarity(Rarity.EPIC))));

        createRepairableBlockItem(REPAIRABLE_FLOWER_POT);
        for (DeferredBlock<Block> block : GLASS_BLOCKS)
            createRepairableBlockItem(block);
        for (DeferredBlock<Block> block : GLASS_PANE_BLOCKS)
            createRepairableBlockItem(block);
    }

    private static DeferredItem<Item> createParentedBlockItem(DeferredBlock<Block> block, Pair<String, String> additionNames, String remove)
    {
        return AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()) {
            @Override
            public Component getName(ItemStack stack)
            {
                String name = Component.translatable(getDescriptionId(stack).replace("ava.", "minecraft.").replace(remove, "")).getString();
                if (additionNames.hasA())
                    name = Component.translatable(additionNames.getA()).getString() + " " + name;
                if (additionNames.hasB())
                    name = name + " " + Component.translatable(additionNames.getB()).getString();
                return Component.literal(name);
            }
        }));
    }

    private static DeferredItem<Item> createSiteBlockItem(DeferredBlock<Block> block)
    {
        return AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()) {
            @Override
            public void appendHoverText(ItemStack stack, @Nullable TooltipContext worldIn, List<Component> tooltip, TooltipFlag flagIn)
            {
                tooltip.add(Component.translatable("ava.item.tips.site_block.info"));
            }
        }));
    }

    private static final Component REPAIRABLE_TIP = Component.translatable("ava.item.tips.repairable", Component.literal("/ava setRepairablesRepaired {true/false}").withStyle(ChatFormatting.AQUA)).withStyle(ChatFormatting.GRAY);
    private static DeferredItem<Item> createRepairableBlockItem(DeferredBlock<Block>block)
    {
        return AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties()) {
            @Override
            public void appendHoverText(ItemStack stack, @Nullable TooltipContext p_40573_, List<Component> p_40574_, TooltipFlag p_40575_)
            {
                p_40574_.add(REPAIRABLE_TIP);
            }
        }));
    }

    private static DeferredItem<Item> createMapCreationBlockItem(DeferredBlock<Block>block)
    {
        return AVAItemGroups.putItem(AVAItemGroups.MAP_CREATION, ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties())));
    }

    private static DeferredItem<Item> createBlockItemWithRecipe(DeferredBlock<Block>block, Item.Properties properties, Recipe recipe)
    {
        return ITEMS.register(block.getId().getPath(), () -> new BlockItemWithRecipe(block.get(), properties, recipe));
    }

    static class BlockItemWithRecipe extends BlockItem implements IHasRecipe
    {
        private final Recipe recipe;

        public BlockItemWithRecipe(Block blockIn, Properties builder, Recipe recipe)
        {
            super(blockIn, builder);
            this.recipe = recipe;
        }

        @Override
        public Recipe getRecipe()
        {
            return recipe;
        }
    }

    private static DeferredBlock<Block>createRepairableStainedGlass(DyeColor colour)
    {
        DeferredBlock<Block>block = BLOCKS.register("repairable_" + colour.getName() + "_stained_glass", () -> new RepairableStainedGlassBlock(colour));
        GLASS_BLOCKS.add(block);
        return block;
    }

    private static DeferredBlock<Block>createRepairableStainedGlassPane(DyeColor colour)
    {
        DeferredBlock<Block>block = BLOCKS.register("repairable_" + colour.getName() + "_stained_glass_pane", () -> new RepairableStainedGlassPaneBlock(colour));
        GLASS_PANE_BLOCKS.add(block);
        return block;
    }
}
