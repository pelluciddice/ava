package pellucid.ava.gamemodes.modes;

import net.minecraft.Util;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import pellucid.ava.cap.AVACrossWorldData;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

@EventBusSubscriber
public class GameModes
{
    public static final GameMode VANILLA = new GameMode("vanilla");
    public static final AnnihilationMode ANNIHILATION = new AnnihilationMode();
    public static final DemolishMode DEMOLISH = new DemolishMode();
    public static final EscortMode ESCORT = new EscortMode();

    public static final Map<String, GameMode> GAMEMODES = Util.make(new HashMap<>(), (map) -> {
        Consumer<GameMode> put = (mode) -> map.put(mode.getName(), mode);
        put.accept(VANILLA);
        put.accept(ANNIHILATION);
        put.accept(DEMOLISH);
        put.accept(ESCORT);
    });

    public static Optional<GameMode> getRunningAVAGamemode()
    {
        return GAMEMODES.values().stream().filter((m) -> m.isRunning() && m != VANILLA).findFirst();
    }

    @SubscribeEvent
    public static void onEntityDeath(LivingDeathEvent event)
    {
        AVACrossWorldData.getInstance().scoreboardManager.ifGamemodeRunning((mode) -> mode.onEntityDeath(event));
    }

    @SubscribeEvent
    public static void onEntitySpawn(EntityJoinLevelEvent event)
    {
        AVACrossWorldData.getInstance().scoreboardManager.ifGamemodeRunning((mode) -> mode.onEntitySpawn(event));
    }

    @SubscribeEvent
    public static void onPlayerSpawn(PlayerEvent.PlayerRespawnEvent event)
    {
        AVACrossWorldData.getInstance().scoreboardManager.ifGamemodeRunning((mode) -> mode.onPlayerSpawn(event));
    }
}
