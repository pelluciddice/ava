package pellucid.ava.gamemodes.modes;

import com.google.common.collect.ImmutableList;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.commands.BroadcastTextCommand;
import pellucid.ava.packets.PlaySoundToClientMessage;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;

public class AnnihilationMode extends ScoredTeamMode
{
    public AnnihilationMode()
    {
        super("annihilation");
    }

    @Override
    protected void onFinishPreparing(Level world)
    {
        super.onFinishPreparing(world);
        if (world instanceof ServerLevel)
        {
            AVAWorldData.getInstance(world).repairAll(world, true);
            PlayerList list = ((ServerLevel) world).getServer().getPlayerList();
            AVAWeaponUtil.forEachTeamMember(list, getTeamA(world), this::resetPlayer);
            AVAWeaponUtil.forEachTeamMember(list, getTeamB(world), this::resetPlayer);
            if (getTimer() != null)
                getTimer().reset(world);
        }
    }

    @Override
    public void tick(ServerLevel world)
    {
        super.tick(world);
        if (isRunning() && world instanceof ServerLevel && !isPreparing())
        {
            if (world.getGameTime() % 40 == 0)
            {
                addEUAIs((ServerLevel) world);
                addNRFAIs((ServerLevel) world);
            }
        }
    }

    @Override
    protected void end(Level world, GameMode.EndReason reason)
    {
        if (!isPreparing())
        {
            int soundDelay = 2;
            if (reason.isTimesUp())
            {
                task((w) ->
                {
                    if (w instanceof ServerLevel)
                        AVAWeaponUtil.playSoundToClients(w, (p) ->
                        {
                            BroadcastTextCommand.broadcastTo("ava.broadcast.times_up", true, 40, null);
                            return new PlaySoundToClientMessage(AVASounds.BROADCAST_MISSION_TIME_COMPLETE.get(), p).setMoving(p.getId());
                        });
                }, 2);
                soundDelay = 40;
            }
            if (!reason.isDraw() && getTeamA(world) != null && getTeamB(world) != null)
                task((w) ->
                {
                    if (w instanceof ServerLevel)
                        AVAWeaponUtil.playSoundToClients(w, (p) ->
                        {
                            boolean allyWin = ((reason.isAWin() && p.isAlliedTo(getTeamA(world))) || (reason.isBWin() && p.isAlliedTo(getTeamB(world))));
                            BroadcastTextCommand.broadcastTo(allyWin ? "ava.broadcast.friendly_troops_win" : "ava.broadcast.enemy_troops_win", true, 100, ImmutableList.of((ServerPlayer) p));
                            return new PlaySoundToClientMessage((allyWin ? AVASounds.BROADCAST_OPERATION_SUCCESS : AVASounds.BROADCAST_OPERATION_FAIL).get(), p).setMoving(p.getId());
                        });
                }, soundDelay);
        }
        super.end(world, reason);
    }

    @Override
    public void init(int targetScore, String triggerName, String triggerObj, Object... args)
    {
        super.init(targetScore, triggerName, triggerObj, args);
        this.maxAICount = (int) args[0];
    }

    @Override
    protected void onEntityDeath(LivingDeathEvent event)
    {
        LivingEntity entity = event.getEntity();
        Level world = entity.level();
        if (!world.isClientSide())
        {
            if (entity.isAlliedTo(getTeamA(world)))
                addScore(world, getScoreboard().teamB, 1);
            else if (entity.isAlliedTo(getTeamB(world)))
                addScore(world, getScoreboard().teamA, 1);
        }
    }
}
