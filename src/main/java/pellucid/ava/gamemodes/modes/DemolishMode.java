package pellucid.ava.gamemodes.modes;

import com.google.common.collect.ImmutableList;
import net.minecraft.Util;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.players.PlayerList;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GameType;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.scores.Team;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.commands.BroadcastTextCommand;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.smart.NRFSmartEntity;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.packets.PlaySoundToClientMessage;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.UUID;
import java.util.function.Supplier;

public class DemolishMode extends ScoredTeamMode
{
    private UUID c4UUID = Util.NIL_UUID;
    private C4Entity c4;
    private Vec3 c4Spawn = Vec3.ZERO;
    private boolean preparingNextRound = false;

    public DemolishMode()
    {
        super("demolish");
    }

    @Override
    public void init(int targetScore, String triggerName, String triggerObj, Object... args)
    {
        super.init(targetScore, triggerName, triggerObj, args);
        this.maxAICount = (int) args[0];
        this.c4Spawn = (Vec3) args[1];
    }

    @Override
    protected void onFinishPreparing(Level world)
    {
        super.onFinishPreparing(world);
        nextRound(world, DemolishEndReasons.FINISH_PREPARING);
    }

    public void setC4Spawn(Vec3 c4Spawn)
    {
        this.c4Spawn = c4Spawn;
    }

    @Override
    protected void onPlayerSpawn(PlayerEvent.PlayerRespawnEvent event)
    {
        Player player = event.getEntity();
        if (player instanceof ServerPlayer)
            ((ServerPlayer) player).setGameMode(GameType.SPECTATOR);
    }

    @Nullable
    public C4Entity getC4(Level world)
    {
        if (!world.isClientSide())
        {
            if ((c4 == null || !c4.isAlive()) && c4UUID != null)
            {
                Entity entity = ((ServerLevel) world).getEntity(c4UUID);
                if (entity instanceof C4Entity)
                    c4 = (C4Entity) entity;
                else
                    c4 = null;
            }
        }
        return c4;
    }

    public void setC4(C4Entity c4)
    {
        this.c4 = c4;
        this.c4UUID = c4 != null ? c4.getUUID() : Util.NIL_UUID;
    }

    @Override
    protected void onEntitySpawn(EntityJoinLevelEvent event)
    {
        super.onEntitySpawn(event);
        if (event.getEntity() instanceof C4Entity)
        {
            setC4((C4Entity) event.getEntity());
            Level w = event.getLevel();
            if (w instanceof ServerLevel)
            {
                AVAWeaponUtil.playSoundToClients(w, (p) -> {
                    if (p.isAlliedTo(getTeamA(w)))
                    {
                        try
                        {
                            BroadcastTextCommand.broadcastTo(Component.translatable("ava.broadcast.friendly_charge_set", getC4(w).getSetter().getDisplayName()).getString(), false, 100, ImmutableList.of((ServerPlayer) p));
                        }
                        catch (Exception ignored)
                        {

                        }
                    }
                    else
                        BroadcastTextCommand.broadcastTo("ava.broadcast.enemy_charge_set", true, 100, ImmutableList.of((ServerPlayer) p));
                    return new PlaySoundToClientMessage(AVASounds.BROADCAST_CHARGE_SET.get(), p).setMoving(p.getId());
                });
                ((ServerLevel) w).getAllEntities().forEach((e) -> {
                    if (e instanceof NRFSmartEntity)
                        ((NRFSmartEntity) e).setC4Entity(c4UUID);
                });
            }
        }
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = super.serializeNBT();
        DataTypes.UUID.write(compound, "c4", c4UUID);
        DataTypes.DOUBLE.write(compound, "c4x", c4Spawn.x);
        DataTypes.DOUBLE.write(compound, "c4y", c4Spawn.y);
        DataTypes.DOUBLE.write(compound, "c4z", c4Spawn.z);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        super.deserializeNBT(nbt);
        c4Spawn = new Vec3(DataTypes.DOUBLE.read(nbt, "c4x"), DataTypes.DOUBLE.read(nbt, "c4y"), DataTypes.DOUBLE.read(nbt, "c4z"));
        c4UUID = DataTypes.UUID.read(nbt, "c4");
    }

    @Override
    protected void end(Level world, GameMode.EndReason reason)
    {
        if (reason.isTimesUp())
        {
            if (getC4(world) == null)
                nextRound(world, DemolishEndReasons.TIMES_UP);
            return;
        }
        super.end(world, reason);
    }

    @Override
    public void tick(ServerLevel world)
    {
        super.tick(world);
        if (isRunning() && world instanceof ServerLevel && !preparingNextRound && !isPreparing())
        {
            if (world.getGameTime() % 10 == 0)
            {
                PlayerList list = ((ServerLevel) world).getServer().getPlayerList();
                if (isTeamEliminated(list, getTeamA(world)) && getC4(world) == null)
                    nextRound(world, DemolishEndReasons.TEAM_A_ELIMINATED);
                else if (isTeamEliminated(list, getTeamB(world)))
                    nextRound(world, DemolishEndReasons.TEAM_B_ELIMINATED);
            }
        }
    }

    @Override
    public boolean setRunning(Level world, boolean running)
    {
        boolean refresh = super.setRunning(world, running);
        if (refresh && running)
            startRound(world);
        return refresh;
    }

    public void startRound(Level world)
    {
        removeEntities(world);
        if (preparingNextRound)
        {
            preparingNextRound = false;
            checkScore(world);
        }
        if (isRunning())
        {
            if (world instanceof ServerLevel)
            {
                Timer timer = getTimer();
                if (timer != null)
                {
                    timer.reset(world);
                    timer.setPaused(false);
                }
                AVAWorldData.getInstance(world).repairAll(world, true);
                PlayerList list = ((ServerLevel) world).getServer().getPlayerList();
                AVAWeaponUtil.forEachTeamMember(list, getTeamA(world), this::resetPlayer);
                AVAWeaponUtil.forEachTeamMember(list, getTeamB(world), this::resetPlayer);

                if (!isPreparing())
                {
                    ItemStack c4Stack = new ItemStack(MiscItems.C4.get());
                    c4Stack.set(AVADataComponents.TAG_ITEM_SPECIAL, true);
                    world.addFreshEntity(new ItemEntity(world, c4Spawn.x, c4Spawn.y, c4Spawn.z, c4Stack));
                    addNRFAIs((ServerLevel) world);
                }
            }
        }
    }

    protected boolean isTeamEliminated(PlayerList list, Team team)
    {
        return !AVAWeaponUtil.forEachTeamMember(list, team, (p) -> {
            return p == null || (AVAWeaponUtil.isValidEntity(p) && p.isAlive());
        });
    }

    public void nextRound(Level world, DemolishEndReasons reason)
    {
        if (preparingNextRound)
            return;
        if (getTimer() != null)
            getTimer().setPaused(true);
        setC4(null);
        if (!reason.isFinishPreparing())
        {
            int soundDelay = 40;
            task((w) ->
            {
                if (w instanceof ServerLevel)
                    AVAWeaponUtil.playSoundToClients(w, (p) ->
                    {
                        boolean isAllyWin = (reason.euWin && p.isAlliedTo(getTeamA(world))) || (!reason.euWin && p.isAlliedTo(getTeamB(world)));
                        Supplier<SoundEvent> s = isAllyWin ? AVASounds.BROADCAST_ENEMY_ELIMINATED : AVASounds.BROADCAST_FRIENDLY_ELIMINATED;
                        boolean eu = p.isAlliedTo(getTeamA(world));
                        String broadcast = "ava.broadcast.";
                        if (reason == DemolishEndReasons.TIMES_UP)
                        {
                            s = AVASounds.BROADCAST_MISSION_TIME_COMPLETE;
                            broadcast += "times_up";
                        }
                        else if (reason == DemolishEndReasons.TARGET_DESTROYED)
                        {
                            s = AVASounds.BROADCAST_TARGET_DESTROYED;
                            broadcast += eu ? "enemy_target_destroyed" : "friendly_target_destroyed";
                        }
                        else if (reason == DemolishEndReasons.CHARGE_DEFUSED)
                        {
                            s = AVASounds.BROADCAST_CHARGE_DEFUSED;
                            broadcast += eu ? "friendly_charge_defused" : "enemy_charge_defused";
                        }
                        else
                            broadcast += isAllyWin ? "enemy_troops_eliminated" : "friendly_troops_eliminated";
                        BroadcastTextCommand.broadcastTo(broadcast, true, 40, ImmutableList.of((ServerPlayer) p));
                        return new PlaySoundToClientMessage(s.get(), p).setMoving(p.getId());
                    });
            }, 2);
            if (getTeamA(world) != null && getTeamB(world) != null)
                task((w) ->
                {
                    if (w instanceof ServerLevel)
                        AVAWeaponUtil.playSoundToClients(w, (p) ->
                        {
                            boolean isAllyWin = (reason.euWin && p.isAlliedTo(getTeamA(world))) || (!reason.euWin && p.isAlliedTo(getTeamB(world)));
                            BroadcastTextCommand.broadcastTo(isAllyWin ? "ava.broadcast.friendly_troops_win" : "ava.broadcast.enemy_troops_win", true, 100, ImmutableList.of((ServerPlayer) p));
                            return new PlaySoundToClientMessage((isAllyWin ? AVASounds.BROADCAST_OPERATION_SUCCESS : AVASounds.BROADCAST_OPERATION_FAIL).get(), p).setMoving(p.getId());
                        });
                }, soundDelay);
            addScore(world, reason.euWin ? getTeamA(world).getName() : getTeamB(world).getName(), 1);
        }
        task(this::startRound, 150);
        preparingNextRound = true;
    }

    @Override
    protected void checkScore(Level world)
    {
        if (!preparingNextRound)
            super.checkScore(world);
    }

    public enum DemolishEndReasons
    {
        TIMES_UP(false),
        TEAM_A_ELIMINATED(false),
        TEAM_B_ELIMINATED(true),
        TARGET_DESTROYED(true),
        CHARGE_DEFUSED(false),
        FINISH_PREPARING(true),
        ;

        private final boolean euWin;

        DemolishEndReasons(boolean enWin)
        {
            this.euWin = enWin;
        }

        public boolean isFinishPreparing()
        {
            return this == FINISH_PREPARING;
        }

        public boolean isEuWin()
        {
            return euWin;
        }
    }
}
