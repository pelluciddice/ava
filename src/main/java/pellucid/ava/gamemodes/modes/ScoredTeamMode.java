package pellucid.ava.gamemodes.modes;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.scores.Scoreboard;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

public class ScoredTeamMode extends TeamedMode
{
    protected int targetScore = -1;
    protected final Pair<Integer, Integer> score = Pair.of(0, 0);

    public ScoredTeamMode(String name)
    {
        super(name);
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = super.serializeNBT();
        DataTypes.INT.write(compound, "targetScore", targetScore);
        DataTypes.INT.write(compound, "scoreName", score.getA());
        DataTypes.INT.write(compound, "scoreObj", score.getB());
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        super.deserializeNBT(nbt);
        targetScore = DataTypes.INT.read(nbt, "targetScore");
        score.set(DataTypes.INT.read(nbt, "scoreName"), DataTypes.INT.read(nbt, "scoreObj"));
    }

    @Override
    public void tick(ServerLevel world)
    {
        super.tick(world);
        if (isRunning() && world instanceof ServerLevel && !isPreparing())
        {
            Timer timer = getTimer();
            if (timer != null)
            {
                if (!timer.isPaused() && timer.getTime(world) <= 0)
                    end(world, getWinningTeam(EndReason.timedScoreWins()));
            }
        }
    }

    @Override
    protected void end(Level world, EndReason reason)
    {
        super.end(world, reason);
        if (getTimer() != null)
            getTimer().reset(world);
    }

    public void init(int targetScore, String triggerName, String triggerObj, Object... args)
    {
        this.targetScore = targetScore;
        setTrigger(triggerName, triggerObj);
    }

    protected EndReason getWinningTeam(Pair<EndReason, EndReason> type)
    {
        return score.getA() > score.getB() ? type.getA() : score.getA() < score.getB() ? type.getB() : EndReason.DRAW;
    }

    @Override
    public boolean isValid(Level world)
    {
        Scoreboard scoreboard = world.getScoreboard();
        if (scoreboard.getObjective(getTrigger().getB()) == null)
            return false;
        return super.isValid(world) && targetScore > 0;
    }

    @Override
    public void restoreDefault(Level world)
    {
        super.restoreDefault(world);
        score.set(0, 0);
    }

    public int getTargetScore()
    {
        return targetScore;
    }

    public void setTargetScore(int targetScore)
    {
        this.targetScore = targetScore;
    }

    public int getScore(String team)
    {
        if (team.equals(getScoreboard().teamA))
            return score.getA();
        else if (team.equals(getScoreboard().teamB))
            return score.getB();
        return -1;
    }

    public void addScore(Level world, String team, int amount)
    {
        if (team.equals(getScoreboard().teamA))
            score.mapA((e) -> e + amount);
        else if (team.equals(getScoreboard().teamB))
            score.mapB((e) -> e + amount);

        checkScore(world);
        DataToClientMessage.gamemode();
    }

    protected void checkScore(Level world)
    {
        if (score.getA() >= targetScore)
            end(world, EndReason.TEAM_A_SCORE_WIN);
        else if (score.getB() >= targetScore)
            end(world, EndReason.TEAM_B_SCORE_WIN);
    }

    @Override
    public String toString()
    {
        return super.toString() + " - - - ScoreMode{" +
                "targetScore=" + targetScore +
                ", score=" + score +
                '}';
    }
}
