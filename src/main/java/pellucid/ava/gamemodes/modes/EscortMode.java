package pellucid.ava.gamemodes.modes;

import com.google.common.collect.ImmutableList;
import net.minecraft.Util;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.network.PacketDistributor;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.commands.BroadcastTextCommand;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.packets.PlaySoundToClientMessage;
import pellucid.ava.player.PositionWithRotation;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.EitherPair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.UUID;

public class EscortMode extends TeamedMode
{
    private UUID tank = Util.NIL_UUID;
    public int tankID = -1;
    public LeopardEntity.Variant tankVariant = LeopardEntity.Variant.FOREST;
    private List<Vec3> path = new ArrayList<>();
    public float tankSpeed = 0.1F;
    public int currentPathIndex = 0;
    private final Map<Integer, EitherPair<List<PositionWithRotation>>> defenseLines = new TreeMap<>();
    public final List<Integer> distBetweenDefenseLines = new ArrayList<>();
    public final List<Integer> distOfDefenseLines = new ArrayList<>();
    public final List<Integer> distBetweenPaths = new ArrayList<>();
    public int totalDistance = 0;

    public int nrfRPGCount = 0;

    public int uavSupportCD = 1200;
    public int uavSupportTimer = uavSupportCD;

    public EscortMode()
    {
        super("escort");
    }

    @Override
    protected int getMaxEUCount()
    {
        return (int) (maxAICount * 1.5F);
    }

    public void addPath(Vec3 path)
    {
        this.path.add(path);
        updateDistances();
    }

    public int getDefenseLineIndex()
    {
        for (int i = currentPathIndex; i >= 0; i--)
            if (defenseLines.containsKey(i))
                return i;
        return -1;
    }

    public int getNextDefenseLineIndex()
    {
        for (int i = currentPathIndex + 1; i < path.size(); i++)
            if (defenseLines.containsKey(i))
                return i;
        return -1;
    }

    public float distanceToNextDefenseLine(Level world)
    {
        Optional<LeopardEntity> tank = getTank(world);
        if (tank.isPresent())
        {
            int dist = 0;
            int defenseLineIndex = getNextDefenseLineIndex();
            for (int i = currentPathIndex + 1; i < defenseLineIndex; i++)
                dist += distBetweenPaths.get(i);
            return dist + (getNextPath() == null ? 0 : (float) tank.get().position().distanceTo(getNextPath()));
        }
        return -1;
    }

    public float distancePassed(Level world)
    {
        Optional<LeopardEntity> tank = getTank(world);
        if (tank.isPresent())
        {
            int dist = 0;
            for (int i = 0; i < currentPathIndex; i++)
                dist += distBetweenPaths.get(i);
            return dist + (getCurrentPath() == null ? 0 : (float) tank.get().position().distanceTo(getCurrentPath()));
        }
        return -1;
    }

    public Map<Integer, EitherPair<List<PositionWithRotation>>> getDefenseLines()
    {
        return defenseLines;
    }

    public void clearDefenseLines()
    {
        defenseLines.clear();
        distBetweenDefenseLines.clear();
        distOfDefenseLines.clear();
    }

    public void clearPath()
    {
        path.clear();
        distBetweenPaths.clear();
        totalDistance = 0;
    }

    public void addDefenseLine(int path, List<PositionWithRotation> spawn, boolean a)
    {
        defenseLines.computeIfAbsent(path, (e) -> EitherPair.eitherPair(new ArrayList<>(), new ArrayList<>())).accept(a, spawn);
        updateDistances();
    }

    public void addDefenseLine(int path, PositionWithRotation spawn, boolean a)
    {
        if (spawn == null)
            defenseLines.computeIfAbsent(path, (e) -> EitherPair.eitherPair(new ArrayList<>(), new ArrayList<>()));
        else
            defenseLines.computeIfAbsent(path, (e) -> EitherPair.eitherPair(new ArrayList<>(), new ArrayList<>())).apply(a).add(spawn);
        updateDistances();
    }

    public void updateDistances()
    {
        try
        {
            totalDistance = 0;
            distBetweenPaths.clear();
            for (int i = 0; i < this.path.size(); i++)
            {
                if (i == this.path.size() - 1)
                {
                    distBetweenPaths.add(0);
                    break;
                }
                int dist = (int) this.path.get(i).distanceTo(this.path.get(i + 1));
                distBetweenPaths.add(dist);
                totalDistance += dist;
            }

            distBetweenDefenseLines.clear();
            distOfDefenseLines.clear();
            int dist = 0;
            int distFromOrigin = 0;
            for (int i = 0; i < this.path.size(); i++)
            {
                if (defenseLines.containsKey(i))
                {
                    distBetweenDefenseLines.add(dist);
                    distOfDefenseLines.add(distFromOrigin);
                    dist = 0;
                }
                dist += distBetweenPaths.get(i);
                distFromOrigin += distBetweenPaths.get(i);
            }
        }
        catch (Exception e)
        {

        }
    }

    public boolean requireMoreRPGs(Level world)
    {
        Optional<LeopardEntity> tank = getTank(world);
        return GameModes.ESCORT.nrfRPGCount < (tank.isPresent() && !tank.get().requireRepair() ? GameModes.ESCORT.getMaxAICount() / 6 : 0) + 1;
    }

    @Override
    public void restoreDefault(Level world)
    {
        super.restoreDefault(world);
        getTank(world).ifPresent((tank) -> tank.remove(Entity.RemovalReason.DISCARDED));
        tank = Util.NIL_UUID;
        tankID = -1;
        currentPathIndex = -1;
        nrfRPGCount = 0;
    }

    @Nullable
    public Vec3 getNextPath()
    {
        if (currentPathIndex + 1 >= 0 && currentPathIndex + 1 < path.size())
            return path.get(currentPathIndex + 1);
        return null;
    }

    @Nullable
    public Vec3 getCurrentPath()
    {
        if (currentPathIndex >= 0 && currentPathIndex < path.size())
            return path.get(currentPathIndex);
        return null;
    }

    public void nextPath(ServerLevel world)
    {
        if (currentPathIndex < path.size())
        {
            currentPathIndex++;
            if (getCurrentPath() != null && defenseLines.containsKey(currentPathIndex) && !world.isClientSide())
            {
                if (!defenseLines.get(currentPathIndex).getA().isEmpty() && !defenseLines.get(currentPathIndex).getB().isEmpty())
                {
                    Map<String, List<PositionWithRotation>> spawns = AVAWorldData.getInstance(world).teamSpawns;
                    spawns.clear();
                    spawns.put(getTeamA(world).getName(), new ArrayList<>(defenseLines.get(currentPathIndex).getA()));
                    spawns.put(getTeamB(world).getName(), new ArrayList<>(defenseLines.get(currentPathIndex).getB()));
                    DataToClientMessage.world(world, (p) -> PacketDistributor.sendToPlayersInDimension(world, p));

                    BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.broke_defense_line_eu", 100, AVAWeaponUtil.TeamSide.EU);
                    BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.broke_defense_line_nrf", 100, AVAWeaponUtil.TeamSide.NRF);
                }
            }
        }
        else end(world, EndReason.TEAM_A_SCORE_WIN);
    }

    @Override
    public boolean setRunning(Level world, boolean running)
    {
        boolean refresh = super.setRunning(world, running);
        if (refresh && running)
        {
            if (defenseLines.containsKey(0))
            {
                Map<String, List<PositionWithRotation>> spawns = AVAWorldData.getInstance(world).teamSpawns;
                spawns.clear();
                spawns.put(getTeamA(world).getName(), new ArrayList<>(defenseLines.get(0).getA()));
                spawns.put(getTeamB(world).getName(), new ArrayList<>(defenseLines.get(0).getB()));
                PlayerList list = ((ServerLevel) world).getServer().getPlayerList();
                AVAWeaponUtil.forEachTeamMember(list, getTeamA(world), this::resetPlayer);
                AVAWeaponUtil.forEachTeamMember(list, getTeamB(world), this::resetPlayer);
            }
        }
        return refresh;
    }

    @Override
    protected void onFinishPreparing(Level world)
    {
        super.onFinishPreparing(world);
        if (world instanceof ServerLevel server)
        {
            AVAWorldData.getInstance(world).repairAll(world, true);
            PlayerList list = server.getServer().getPlayerList();
            AVAWeaponUtil.forEachTeamMember(list, getTeamA(world), this::resetPlayer);
            AVAWeaponUtil.forEachTeamMember(list, getTeamB(world), this::resetPlayer);
            if (getTimer() != null)
                getTimer().reset(world);
            currentPathIndex = -1;
            nrfRPGCount = 0;
            nextPath(server);
            EntityType<LeopardEntity> type = switch (tankVariant)
            {
                case FOREST -> AVAEntities.LEOPARD_FOREST.get();
                case DESERT -> AVAEntities.LEOPARD_DESERT.get();
                case SNOW -> AVAEntities.LEOPARD_SNOW.get();
            };
            LeopardEntity entity = type.create(world);
            if (entity != null)
            {
                world.addFreshEntity(entity);
                entity.moveTo(getCurrentPath().x, getCurrentPath().y, getCurrentPath().z, 0, 0);
                setTank(entity.getUUID());
                tankID = entity.getId();
            }
        }
    }

    public void setTank(UUID tank)
    {
        this.tank = tank;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = super.serializeNBT();
        DataTypes.UUID.write(compound, "tank", tank);
        DataTypes.INT.write(compound, "tankID", tankID);
        DataTypes.INT.write(compound, "tankVariant", tankVariant.ordinal());
        ListTag list = new ListTag();
        for (Vec3 pos : new ArrayList<>(path))
            list.add(AVAWeaponUtil.writeVec(pos));
        compound.put("path", list);
        DataTypes.FLOAT.write(compound, "tankSpeed", tankSpeed);
        DataTypes.INT.write(compound, "currentPathIndex", currentPathIndex);
        list = new ListTag();
        for (Map.Entry<Integer, EitherPair<List<PositionWithRotation>>> entry : new HashMap<>(defenseLines).entrySet())
        {
            CompoundTag tag = new CompoundTag();
            DataTypes.INT.write(tag, "pathIndex", entry.getKey());
            ListTag list1 = new ListTag();
            for (PositionWithRotation spawn : entry.getValue().getA())
                list1.add(spawn.serializeNBT());
            tag.put("attackSpawns", list1);
            list1 = new ListTag();
            for (PositionWithRotation spawn : entry.getValue().getB())
                list1.add(spawn.serializeNBT());
            tag.put("defenseSpawns", list1);
            list.add(tag);
        }
        compound.put("defenseLines", list);
        DataTypes.INT.write(compound, "nrfRPGCount", nrfRPGCount);
        compound.putIntArray("distBetweenDefenseLines", new ArrayList<>(distBetweenDefenseLines).stream().mapToInt((i) -> i).toArray());
        compound.putIntArray("distBetweenPaths", new ArrayList<>(distBetweenPaths).stream().mapToInt((i) -> i).toArray());
        compound.putIntArray("distOfDefenseLines", new ArrayList<>(distOfDefenseLines).stream().mapToInt((i) -> i).toArray());
        DataTypes.INT.write(compound, "totalDistance", totalDistance);
        DataTypes.INT.write(compound, "uavSupportCD", uavSupportCD);
        DataTypes.INT.write(compound, "uavSupportTimer", uavSupportTimer);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        super.deserializeNBT(nbt);
        setTank(DataTypes.UUID.read(nbt, "tank"));
        tankID = DataTypes.INT.read(nbt, "tankID");
        tankVariant = LeopardEntity.Variant.values()[DataTypes.INT.read(nbt, "tankVariant")];
        path.clear();
        for (Tag tag : nbt.getList("path", CompoundTag.TAG_COMPOUND))
            addPath(AVAWeaponUtil.readVec((CompoundTag) tag));
        tankSpeed = DataTypes.FLOAT.read(nbt, "tankSpeed");
        currentPathIndex = DataTypes.INT.read(nbt, "currentPathIndex");
        defenseLines.clear();
        for (Tag tag : nbt.getList("defenseLines", CompoundTag.TAG_COMPOUND))
        {
            CompoundTag tag1 = (CompoundTag) tag;
            int pathIndex = DataTypes.INT.read(tag1, "pathIndex");
            ListTag list1 = tag1.getList("attackSpawns", CompoundTag.TAG_COMPOUND);
            List<PositionWithRotation> attackSpawns = new ArrayList<>();
            for (Tag tag2 : list1)
                attackSpawns.add(new PositionWithRotation((CompoundTag) tag2));
            list1 = tag1.getList("defenseSpawns", CompoundTag.TAG_COMPOUND);
            List<PositionWithRotation> defenseSpawns = new ArrayList<>();
            for (Tag tag2 : list1)
                defenseSpawns.add(new PositionWithRotation((CompoundTag) tag2));
            addDefenseLine(pathIndex, attackSpawns, true);
            addDefenseLine(pathIndex, defenseSpawns, false);
        }
        DataTypes.INT.read(nbt, "nrfRPGCount");
        distBetweenDefenseLines.clear();
        for (int i : nbt.getIntArray("distBetweenDefenseLines"))
            distBetweenDefenseLines.add(i);
        distBetweenPaths.clear();
        for (int i : nbt.getIntArray("distBetweenPaths"))
            distBetweenPaths.add(i);
        distOfDefenseLines.clear();
        for (int i : nbt.getIntArray("distOfDefenseLines"))
            distOfDefenseLines.add(i);
        totalDistance = DataTypes.INT.read(nbt, "totalDistance");
        uavSupportCD = DataTypes.INT.read(nbt, "uavSupportCD");
        uavSupportTimer = DataTypes.INT.read(nbt, "uavSupportTimer");
    }

    @Override
    public void tick(ServerLevel world)
    {
        super.tick(world);
        if (isRunning() && !isPreparing())
        {
            if (world.getGameTime() % 200 == 0)
            {
                addEUAIs(world);
                addNRFAIs(world);
            }

            getTank(world).ifPresent((tank) -> {
                tank.updateFromEscort(this, world);
                if (tank.requireRepair())
                {
                    uavSupportTimer--;
                    if (uavSupportTimer <= 0)
                    {
                        uavSupportTimer = uavSupportCD / 2;
                        world.getAllEntities().forEach((e) -> {
                            if (e instanceof LivingEntity living && AVAWeaponUtil.TeamSide.NRF.isSameSide(living))
                                AVAWorldData.getInstance(world).uavEscortS.put(e.getUUID(), uavSupportCD);
                            if (e instanceof ServerPlayer player)
                            {
                                DataToClientMessage.screenShake(player, 2.0F);
                                task((w) -> DataToClientMessage.screenShake(player, 4.0F), 14);
                                task((w) -> DataToClientMessage.screenShake(player, 10.0F), 20);
                                task((w) -> DataToClientMessage.screenShake(player, 7.0F), 40);
                                task((w) -> DataToClientMessage.screenShake(player, 13.0F), 50);
                            }
                        });
                        DataToClientMessage.world(world, (p) -> PacketDistributor.sendToPlayersInDimension(world, p));
                        BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.uav_support_start_eu", 100, AVAWeaponUtil.TeamSide.EU);
                        BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.uav_support_start_nrf", 100, AVAWeaponUtil.TeamSide.NRF);
                        taskUAVSound(world, 4);
                    }
                }
                else uavSupportTimer = uavSupportCD;
                DataToClientMessage.gamemode();
            });
            Timer timer = getTimer();
            if (timer != null)
            {
                if (!timer.isPaused() && timer.getTime(world) <= 0)
                    end(world, EndReason.TIMES_UP_TEAM_B_SCORE_WIN);
            }
        }
    }

    protected void taskUAVSound(ServerLevel world, int times)
    {
        if (times > 0)
        {
            AVAWeaponUtil.playSoundToClients(world, (p) -> new PlaySoundToClientMessage(AVASounds.UAV_SUPPORT.get(), p).setMoving(p.getId()));
            task((w) -> taskUAVSound(world, times - 1), 30);
        }
    }

    public Optional<LeopardEntity> getTank(Level world)
    {
        return Optional.ofNullable(world instanceof ServerLevel ? (LeopardEntity) ((ServerLevel) world).getEntity(tank) : world.getEntity(tankID) instanceof LeopardEntity leopard ? leopard : null);
    }

    @Override
    protected void end(Level world, EndReason reason)
    {
        getTank(world).ifPresent((tank) -> tank.remove(Entity.RemovalReason.DISCARDED));
        if (!isPreparing())
        {
            int soundDelay = 2;
            if (reason.isTimesUp())
            {
                task((w) ->
                {
                    if (w instanceof ServerLevel)
                        AVAWeaponUtil.playSoundToClients(w, (p) ->
                        {
                            BroadcastTextCommand.broadcastTo("ava.broadcast.times_up", true, 40, null);
                            return new PlaySoundToClientMessage(AVASounds.BROADCAST_MISSION_TIME_COMPLETE.get(), p).setMoving(p.getId());
                        });
                }, 2);
                soundDelay = 40;
            }
            if (!reason.isDraw() && getTeamA(world) != null && getTeamB(world) != null)
                task((w) ->
                {
                    if (w instanceof ServerLevel)
                        AVAWeaponUtil.playSoundToClients(w, (p) ->
                        {
                            boolean allyWin = ((reason.isAWin() && p.isAlliedTo(getTeamA(world))) || (reason.isBWin() && p.isAlliedTo(getTeamB(world))));
                            BroadcastTextCommand.broadcastTo(allyWin ? "ava.broadcast.friendly_troops_win" : "ava.broadcast.enemy_troops_win", true, 100, ImmutableList.of((ServerPlayer) p));
                            return new PlaySoundToClientMessage((allyWin ? AVASounds.BROADCAST_OPERATION_SUCCESS : AVASounds.BROADCAST_OPERATION_FAIL).get(), p).setMoving(p.getId());
                        });
                }, soundDelay);
        }
        super.end(world, reason);
    }

    @Override
    public boolean isValid(Level world)
    {
        return super.isValid(world) && !path.isEmpty() && !defenseLines.isEmpty() && defenseLines.containsKey(0);
    }

    @Override
    public int getRespawnInvincibleTime()
    {
        return 100;
    }

    @Override
    public String toString()
    {
        return "EscortMode{" +
                "tankVariant=" + tankVariant +
                ", tankSpeed=" + tankSpeed +
                ", uavSupportCD=" + uavSupportCD +
                ", preparing=" + preparing +
                ", maxAICount=" + maxAICount +
                '}';
    }
}
