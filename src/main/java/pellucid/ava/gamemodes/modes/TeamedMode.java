package pellucid.ava.gamemodes.modes;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.scores.PlayerTeam;
import net.minecraft.world.scores.Scoreboard;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

public class TeamedMode extends GameMode
{
    public TeamedMode(String name)
    {
        super(name);
    }

    protected int getMaxEUCount()
    {
        return maxAICount;
    }

    protected int getMaxNRFCount()
    {
        return maxAICount;
    }

    public void addEUAIs(ServerLevel world)
    {
        PlayerTeam teamA = getTeamA(world);
        addAI(AVAEntities.EU_SOLDIER.get(), world, teamA, getMaxEUCount() - teamA.getPlayers().size());
    }

    public void addNRFAIs(ServerLevel world)
    {
        PlayerTeam teamB = getTeamB(world);
        addAI(AVAEntities.NRF_SOLDIER.get(), world, teamB, getMaxNRFCount() - teamB.getPlayers().size());
    }

    @Override
    protected void onEntitySpawn(EntityJoinLevelEvent event)
    {
        Entity entity = event.getEntity();

        if (entity instanceof SidedSmartAIEntity)
        {
            PlayerTeam team = entity.level().getScoreboard().getPlayerTeam(AVAWeaponUtil.TeamSide.getSideFor((LivingEntity) entity) == AVAWeaponUtil.TeamSide.EU ? getScoreboard().teamA : getScoreboard().teamB);
            if (team != null)
                entity.level().getScoreboard().addPlayerToTeam(entity.getScoreboardName(), team);
        }
    }


    @Override
    public boolean isValid(Level world)
    {
        Scoreboard scoreboard = world.getScoreboard();
        if (scoreboard.getPlayerTeam(getScoreboard().teamA) == null)
            return false;
        if (scoreboard.getPlayerTeam(getScoreboard().teamB) == null)
            return false;
        return true;
    }

    public PlayerTeam getTeamA(Level world)
    {
        return AVACommonUtil.notNullOr(world.getScoreboard().getPlayerTeam(getScoreboard().teamA), AVAConstants.EMPTY_TEAM);
    }

    public PlayerTeam getTeamB(Level world)
    {
        return AVACommonUtil.notNullOr(world.getScoreboard().getPlayerTeam(getScoreboard().teamB), AVAConstants.EMPTY_TEAM);
    }
}
