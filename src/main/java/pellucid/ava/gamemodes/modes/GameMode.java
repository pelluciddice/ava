package pellucid.ava.gamemodes.modes;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.GameType;
import net.minecraft.world.level.Level;
import net.minecraft.world.scores.PlayerTeam;
import net.minecraft.world.scores.ScoreAccess;
import net.minecraft.world.scores.ScoreHolder;
import net.minecraft.world.scores.Scoreboard;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.commands.BroadcastTextCommand;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.objects.item.GunItemEntity;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesServerManager;
import pellucid.ava.gamemodes.scoreboard.AVAScoreboardManager;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.INBTSerializable;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class GameMode implements INBTSerializable<CompoundTag>
{
    private final String name;
    public final Component displayName;
    public final String targetDisplayNameKey;
    private final Pair<String, String> timer = Pair.of("", "");
    private final Pair<String, String> trigger = Pair.of("", "");
    private boolean running = false;
    private boolean requireScoreboardTrack = true;
    private final List<Pair<Consumer<Level>, Integer>> pendingTasks = new ArrayList<>();
    protected int preparing = 0;
    protected int maxAICount = -1;

    public GameMode(String name)
    {
        this.name = name;
        this.displayName = Component.translatable("ava.hud." + name);
        this.targetDisplayNameKey = "ava.hud." + name + "_target";
    }

    protected <T extends Entity> void addAI(EntityType<T> type, ServerLevel world, PlayerTeam team, int count)
    {
        if (team != null)
        {
            CompoundTag compound = new CompoundTag();
            DataTypes.STRING.write(compound, "id", BuiltInRegistries.ENTITY_TYPE.getKey(type).toString());

            for (int i = 0; i < count; i++)
            {
                Entity entity = EntityType.loadEntityRecursive(compound.copy(), world, (entity2) ->
                {
                    world.getScoreboard().addPlayerToTeam(entity2.getScoreboardName(), team);
                    ;
                    return AVAWeaponUtil.setEntityPosByTeamSpawn(entity2, 3) ? entity2 : null;
                });
                if (entity == null)
                    return;

                if (entity instanceof Mob)
                    ((Mob) entity).finalizeSpawn(world, world.getCurrentDifficultyAt(entity.blockPosition()), MobSpawnType.COMMAND, null);

                if (!world.tryAddFreshEntityWithPassengers(entity))
                    return;

                if (entity instanceof SidedSmartAIEntity smart)
                {
                    if (!GameModes.DEMOLISH.isRunning() && smart.shouldFindFarTarget())
                        smart.findFarTarget();
                    smart.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, getRespawnInvincibleTime(), 999999, true, false, false));
                }
            }
        }
    }

    public boolean isPreparing()
    {
        return preparing > 0;
    }

    public void restoreDefault(Level world)
    {
        removeEntities(world);
    }

    public AVAScoreboardManager getScoreboard()
    {
        return AVACrossWorldData.getInstance().scoreboardManager;
    }

    public void tick(ServerLevel world)
    {
        List<Pair<Consumer<Level>, Integer>> toRemove = new ArrayList<>();
        for (Pair<Consumer<Level>, Integer> task : new ArrayList<>(pendingTasks))
        {
            if (task.mapB((b) -> b - 1).getB() <= 0)
            {
                task.getA().accept(world);
                toRemove.add(task);
            }
        }
        toRemove.forEach(pendingTasks::remove);
        if (!isValid(world))
            setRunning(world, false);
        else
        {
            if (world instanceof ServerLevel)
            {
                if (isPreparing())
                {
                    preparing--;
                    if (preparing == 1)
                        onFinishPreparing(world);
                }
                Timer timer = getTimer();
                if (timer != null)
                {
                    if (requireScoreboardTrack)
                    {
                        AVAWeaponUtil.trackScoreboard(world, timer.getName());
                        requireScoreboardTrack = false;
                    }
                }
            }
        }
    }

    protected void onFinishPreparing(Level world)
    {
        restoreDefault(world);
    }

    public boolean isValid(Level world)
    {
        return false;
    }

    public boolean isRunning()
    {
        return running;
    }

    public boolean setRunning(Level world, boolean running)
    {
        if (this.running != running && !world.isClientSide())
        {
            Timer timer = getTimer();
            if (timer != null)
            {
                if (running)
                    AVAWeaponUtil.trackScoreboard((ServerLevel) world, timer.getName());
                timer.setPaused(!running);
                timer.reset(world);
                DataToClientMessage.timer();
            }
            if (running)
            {
                preparing = 200;
                BroadcastTextCommand.broadcastTo("ava.broadcast.preparing", true, 200, null);
                AVAWeaponUtil.getPlayersForSide(world, AVAWeaponUtil.TeamSide.EU).forEach((p) -> p.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, getRespawnInvincibleTime(), 999, true, false, false)));
                AVAWeaponUtil.getPlayersForSide(world, AVAWeaponUtil.TeamSide.NRF).forEach((p) -> p.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, getRespawnInvincibleTime(), 999, true, false, false)));
                LoadingImagesServerManager.INSTANCE.deferredDisplay.ifPresent(Runnable::run);
            }
            this.running = running;
            return true;
        }
        this.running = running;
        return false;
    }

    public void setTimer(String timerName, String timerObj)
    {
        this.timer.set(timerName, timerObj);
    }

    public void setTrigger(String triggerName, String triggerObj)
    {
        this.trigger.set(triggerName, triggerObj);
    }

    @Nullable
    public Timer getTimer()
    {
        return AVACrossWorldData.getInstance().getTimer(timer.getA(), timer.getB());
    }

    public Pair<String, String> getTrigger()
    {
        return trigger;
    }

    public Optional<ScoreAccess> getTriggerScore(Level world)
    {
        Scoreboard s = world.getScoreboard();
        Pair<String, String> trigger = getTrigger();
        return Optional.ofNullable(s.getObjective(trigger.getB()) == null ? null : s.getOrCreatePlayerScore(ScoreHolder.forNameOnly(trigger.getA()), s.getObjective(trigger.getB())));
    }

    public String getName()
    {
        return name;
    }

    protected void onEntityDeath(LivingDeathEvent event)
    {

    }

    protected void onEntitySpawn(EntityJoinLevelEvent event)
    {

    }

    protected void onPlayerSpawn(PlayerEvent.PlayerRespawnEvent event)
    {

    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.STRING.write(compound, "timerName", timer.getA());
        DataTypes.STRING.write(compound, "timerObj", timer.getB());
        DataTypes.STRING.write(compound, "triggerName", trigger.getA());
        DataTypes.STRING.write(compound, "triggerObj", trigger.getB());
        DataTypes.BOOLEAN.write(compound, "running", running);
        DataTypes.INT.write(compound, "maxAICount", maxAICount);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        timer.set(DataTypes.STRING.read(nbt, "timerName"), DataTypes.STRING.read(nbt, "timerObj"));
        trigger.set(DataTypes.STRING.read(nbt, "triggerName"), DataTypes.STRING.read(nbt, "triggerObj"));
        running = DataTypes.BOOLEAN.read(nbt, "running");
        maxAICount = DataTypes.INT.read(nbt, "maxAICount");
    }

    protected void end(Level world, EndReason reason)
    {
        resetByEnd(world);
        getTriggerScore(world).ifPresent((score) -> score.set(reason.ordinal()));
        removeEntities(world);
    }

    protected void resetPlayer(ServerPlayer p)
    {
        DataToClientMessage.requestClientPreset(p);
        p.setGameMode(GameType.ADVENTURE);
        p.setHealth(p.getMaxHealth());
        PlayerAction cap = AVACommonUtil.cap(p);
        cap.setArmourValue(p, 10.0F);
        AVAWeaponUtil.setEntityPosByTeamSpawn(p, 3);
        p.removeAllEffects();
        p.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, getRespawnInvincibleTime(), 999, true, false, false));
        p.addEffect(new MobEffectInstance(MobEffects.SATURATION, Integer.MAX_VALUE, 999, true, false, false));
    }

    protected void removeEntities(Level world)
    {
        if (world instanceof ServerLevel)
        {
            List<Entity> toDiscard = new ArrayList<>();
            ((ServerLevel) world).getAllEntities().forEach((entity) ->
            {
                if (entity instanceof LeopardEntity || entity instanceof ProjectileEntity || entity instanceof SidedSmartAIEntity || entity instanceof C4Entity || entity instanceof GunItemEntity || (entity instanceof ItemEntity && ((ItemEntity) entity).getItem().getItem() == MiscItems.C4.get()))
                    toDiscard.add(entity);
            });
            toDiscard.forEach(Entity::discard);
        }
    }

    protected void resetByEnd(Level world)
    {
        restoreDefault(world);
        setRunning(world, false);
    }

    protected void task(Consumer<Level> action, int delay)
    {
        pendingTasks.add(Pair.of(action, delay));
    }

    enum EndReason
    {
        DRAW,
        TEAM_A_SCORE_WIN,
        TEAM_B_SCORE_WIN,
        TIMES_UP_TEAM_A_SCORE_WIN,
        TIMES_UP_TEAM_B_SCORE_WIN
        ;
        EndReason()
        {

        }

        public boolean isAWin()
        {
            return this == TEAM_A_SCORE_WIN || this == TIMES_UP_TEAM_A_SCORE_WIN;
        }

        public boolean isBWin()
        {
            return this == TEAM_B_SCORE_WIN || this == TIMES_UP_TEAM_B_SCORE_WIN;
        }

        public boolean isDraw()
        {
            return this == DRAW;
        }

        public boolean isScoreWin()
        {
            return scoreWins().contains(this);
        }

        public boolean isTimesUp()
        {
            return timedScoreWins().contains(this) || isDraw();
        }

        public static Pair<EndReason, EndReason> scoreWins()
        {
            return Pair.of(TEAM_A_SCORE_WIN, TEAM_B_SCORE_WIN);
        }

        public static Pair<EndReason, EndReason> timedScoreWins()
        {
            return Pair.of(TIMES_UP_TEAM_A_SCORE_WIN, TIMES_UP_TEAM_B_SCORE_WIN);
        }
    }

    public int getRespawnInvincibleTime()
    {
        return 60;
    }

    public int getMaxAICount()
    {
        return maxAICount;
    }

    public void setMaxAICount(int maxAICount)
    {
        this.maxAICount = maxAICount;
    }

    @Override
    public String toString()
    {
        return "GameMode{" +
                "name='" + name + '\'' +
                ", timer=" + timer +
                ", trigger=" + trigger +
                ", running=" + running +
                '}';
    }
}
