package pellucid.ava.gamemodes.loading_screen;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.packs.resources.PreparableReloadListener;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.storage.LevelResource;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import pellucid.ava.AVA;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.packets.DisplayLoadingImageMessage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class LoadingImagesServerManager extends LoadingImagesManager implements PreparableReloadListener
{
    public static final LoadingImagesServerManager INSTANCE = new LoadingImagesServerManager();
    public static final LevelResource RESOURCE = new LevelResource("");

    public final Map<Player, Boolean> readyPlayers = new HashMap<>();
    public int duration;
    public int maxWait;

    @Override
    public final CompletableFuture<Void> reload(PreparableReloadListener.PreparationBarrier p_10780_, ResourceManager p_10781_, ProfilerFiller p_10782_, ProfilerFiller p_10783_, Executor p_10784_, Executor p_10785_)
    {
        return CompletableFuture.supplyAsync(() -> (Void) null, p_10784_).thenCompose(p_10780_::wait).thenAcceptAsync((p_10792_) -> {
            try
            {
                serverInit(ServerLifecycleHooks.getCurrentServer());
            }
            catch (Exception e)
            {
                AVA.LOGGER.error("Failed to reload loading images, maybe the server is still starting?");
            }
        }, p_10785_);
    }

    protected Void prepare(ResourceManager p_10796_, ProfilerFiller p_10797_)
    {
        return null;
    }

    public void serverInit(MinecraftServer server) throws IOException
    {
        AVA.LOGGER.info("Loading loading images on server");
        super.init(server.getWorldPath(RESOURCE));
        AVA.LOGGER.info("Finish loading loading images on server");
    }

    @Override
    public void tick()
    {
        if (maxWait > 0)
        {
            maxWait--;
            if (maxWait <= 0)
                checkAndSendReadyToClients(null, true);
        }
    }

    public Optional<Runnable> deferredDisplay = Optional.empty();
    public int displayImageToClients(String image, int duration, int maxWait, boolean force)
    {
        if (images.containsKey(image))
        {
            if (!force && AVAServerConfig.isCompetitiveModeActivated())
            {
                AVA.LOGGER.info("Competitive mode is activated, delaying display image");
                deferredDisplay = Optional.of(() -> {
                    displayImageToClients(image, duration, maxWait, true);
                    deferredDisplay = Optional.empty();
                });
                return duration;
            }
            this.duration = duration;
            this.maxWait = maxWait;
            readyPlayers.clear();
            PacketDistributor.sendToAllPlayers(new DisplayLoadingImageMessage(DisplayLoadingImageMessage.SERVER_ASK_CLIENT, image, null, duration));
            for (ServerPlayer player : ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers())
                readyPlayers.put(player, false);
            return duration;
        }
        return 0;
    }

    public void checkAndSendReadyToClients(ServerPlayer player, boolean force)
    {
        if (player != null)
        {
            readyPlayers.put(player, true);
            if (readyPlayers.values().stream().allMatch(Boolean::booleanValue))
                force = true;
        }
        if (force)
        {
            PacketDistributor.sendToAllPlayers(new DisplayLoadingImageMessage(DisplayLoadingImageMessage.SERVER_ALL_READY, "", null, duration));
            readyPlayers.clear();
            maxWait = 0;
            duration = 0;
        }
    }

    public void sendImageToClient(ServerPlayer player, String image, int duration)
    {
        if (player != null && images.containsKey(image))
            PacketDistributor.sendToPlayer(player, new DisplayLoadingImageMessage(DisplayLoadingImageMessage.SERVER_IMAGE_TO_CLIENT, image, images.get(image).get(), duration));
    }

    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> registerCommand(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("displayLoadingScreen").requires(player -> player.hasPermission(2));
        builder.then(argument("name", StringArgumentType.string())
                .then(argument("duration", IntegerArgumentType.integer(1))
                        .then(argument("maxWait", IntegerArgumentType.integer(1))
                                .executes((context) ->
                                {
                                    String name = StringArgumentType.getString(context, "name").toLowerCase(Locale.ROOT);
                                    int duration = IntegerArgumentType.getInteger(context, "duration");
                                    int maxWait = IntegerArgumentType.getInteger(context, "maxWait");
                                    return INSTANCE.displayImageToClients(name, duration, maxWait, false);
                                }))));
        builder.then(argument("name", StringArgumentType.string())
                .then(argument("duration", IntegerArgumentType.integer(1))
                        .then(argument("maxWait", IntegerArgumentType.integer(1))
                                .then(argument("force", BoolArgumentType.bool())
                                        .executes((context) ->
                                        {
                                            String name = StringArgumentType.getString(context, "name").toLowerCase(Locale.ROOT);
                                            int duration = IntegerArgumentType.getInteger(context, "duration");
                                            int maxWait = IntegerArgumentType.getInteger(context, "maxWait");
                                            boolean force = BoolArgumentType.getBool(context, "force");
                                            return INSTANCE.displayImageToClients(name, duration, maxWait, force);
                                        })))));
        return builder;
    }

}
