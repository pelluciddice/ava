package pellucid.ava.gamemodes.loading_screen;

import com.mojang.blaze3d.platform.NativeImage;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.network.chat.Component;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ScreenEvent;
import org.joml.Matrix4f;
import pellucid.ava.AVA;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Lazy;

import java.awt.Color;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@EventBusSubscriber(Dist.CLIENT)
public class LoadingImagesClientManager extends LoadingImagesManager
{
    public static final LoadingImagesClientManager INSTANCE = new LoadingImagesClientManager();
    private static final Map<String, DynamicTexture> TEXTURES = new HashMap<>();
    private static final Map<String, Lazy<RenderType>> RENDER_TYPE = new HashMap<>();
    private String displayingImage = null;
    public int displayingDuration;

    @SubscribeEvent
    public static void onScreenOpen(ScreenEvent.Opening event)
    {
        if (event.getNewScreen() instanceof ReceivingLevelScreen && event.getCurrentScreen() instanceof LoadingScreen)
            event.setCanceled(true);
    }

    @Override
    public void init(Path directory) throws IOException
    {
        super.init(directory);
        TEXTURES.values().forEach(DynamicTexture::close);
        TEXTURES.clear();
        RENDER_TYPE.clear();
        displayingImage = null;
        displayingDuration = 0;
    }

    public void tick()
    {
        if (displayingDuration > 0)
        {
            displayingDuration--;
            if (displayingDuration <= 0)
            {
                displayingImage = null;
                if (Minecraft.getInstance().screen instanceof LoadingScreen)
                    Minecraft.getInstance().setScreen(null);
            }
        }
    }

    public DynamicTexture getTexture()
    {
        if (images.containsKey(displayingImage))
        {
            return TEXTURES.computeIfAbsent(displayingImage, (key) ->
            {
                int[][] image = images.get(displayingImage).get();
                if (image[0].length == 0 || image[0][0] == 0)
                    AVA.LOGGER.error("Image " + displayingImage + " is empty!");
                DynamicTexture dynamicTexture = new DynamicTexture(image[0].length, image.length, true);
                NativeImage pixels = dynamicTexture.getPixels();
                if (pixels != null)
                {
                    for (int y = 0; y < image.length; y++)
                        for (int x = 0; x < image[y].length; x++)
                        {
                            Color c = new Color(image[y][x]);
                            Color c2 = new Color(c.getBlue(), c.getGreen(), c.getRed());
                            pixels.setPixelRGBA(x, y, c2.getRGB());
                        }
                    dynamicTexture.upload();
                }
                return dynamicTexture;
            });
        }
        return null;
    }

    public void display(String name, int duration)
    {
        displayingImage = name;
        displayingDuration = duration;
        Minecraft.getInstance().setScreen(new LoadingScreen());
    }

    static class LoadingScreen extends Screen
    {
        public LoadingScreen()
        {
            super(Component.empty());
        }

        @Override
        public void render(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
        {
            if (LoadingImagesClientManager.INSTANCE.getTexture() != null)
            {
                Minecraft minecraft = Minecraft.getInstance();
                matrixStack.pose().pushPose();
                MultiBufferSource.BufferSource buffer = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
                VertexConsumer consumer = buffer.getBuffer(RENDER_TYPE.computeIfAbsent(LoadingImagesClientManager.INSTANCE.displayingImage, (image) -> Lazy.of(() -> RenderType.textSeeThrough(Minecraft.getInstance().textureManager.register(AVA.MODID + "_loading_screen_" + image, LoadingImagesClientManager.INSTANCE.getTexture())))).get());
                Matrix4f matrix4f = matrixStack.pose().last().pose();

                int w = minecraft.getWindow().getGuiScaledWidth();
                int h = minecraft.getWindow().getGuiScaledHeight();
                consumer.vertex(matrix4f, 0, 0, 0.0F).color(255, 255, 255, 255).uv(0.0F, 0.0F).uv2(AVAConstants.VANILLA_FULL_PACKED_LIGHT).endVertex();
                consumer.vertex(matrix4f, 0, h, 0.0F).color(255, 255, 255, 255).uv(1.0F, 0.0F).uv2(AVAConstants.VANILLA_FULL_PACKED_LIGHT).endVertex();
                consumer.vertex(matrix4f, w, h, 0.0F).color(255, 255, 255, 255).uv(1.0F, 1.0F).uv2(AVAConstants.VANILLA_FULL_PACKED_LIGHT).endVertex();
                consumer.vertex(matrix4f, w, 0, 0.0F).color(255, 255, 255, 255).uv(0.0F, 1.0F).uv2(AVAConstants.VANILLA_FULL_PACKED_LIGHT).endVertex();
                buffer.endBatch();
                matrixStack.pose().popPose();
            }
        }

        @Override
        public boolean keyPressed(int keyCode, int scanCode, int modifiers)
        {
            return super.keyPressed(keyCode, scanCode, modifiers);
        }

        @Override
        public boolean isPauseScreen()
        {
            return false;
        }
    }
}
