package pellucid.ava.gamemodes.loading_screen;

import pellucid.ava.AVA;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Lazy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LoadingImagesManager
{
    public final Map<String, Lazy<int[][]>> images = new HashMap<>();

    public void tick()
    {

    }

    public void init(Path directory) throws IOException
    {
        directory = loadingImagesDirectory(directory);
        AVA.LOGGER.info("Indexing existing loading images from " + directory);
        images.clear();
        File[] files = directory.toFile().listFiles();
        if (files != null)
        {
            for (File file : files)
            {
                if (file.isFile())
                {
                    String name = file.getName().toLowerCase(Locale.ROOT);
                    if (name.contains(" "))
                    {
                        AVA.LOGGER.error("File " + name + " contains spaces, skipping");
                        continue;
                    }
                    if (!name.endsWith(".png"))
                    {
                        AVA.LOGGER.error("File " + name + " is not a png, skipping");
                        continue;
                    }
                    // < 2 MB
                    if (Files.size(file.toPath()) / 1000000 > 2)
                    {
                        AVA.LOGGER.error("File " + name + " is too large, skipping");
                        continue;
                    }
                    images.put(name.replace(".png", ""), Lazy.of(() ->
                    {
                        try
                        {
                            int[][] array = AVACommonUtil.imageToArray(file, this instanceof LoadingImagesServerManager);
                            if (array == null)
                                throw new IOException("Image is too big, should not have more than" + AVAConstants.MAX_IMAGE_SIZE + "pixels");
                            AVA.LOGGER.info("Loaded image: " + name);
                            return array;
                        }
                        catch (IOException e)
                        {
                            AVA.LOGGER.error("Unable to load image: " + name + " !");
                            e.printStackTrace();
                            throw new RuntimeException(e);
                        }
                    }));
                }
            }
        }
    }

    public static Path loadingImagesDirectory(Path directory)
    {
        directory = directory.resolve("ava/loading_images");
        if (!Files.isDirectory(directory))
        {
            try
            {
                AVA.LOGGER.info("No existing preset directory found, creating at " + directory);
                Files.createDirectories(directory);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
        return directory;
    }
}
