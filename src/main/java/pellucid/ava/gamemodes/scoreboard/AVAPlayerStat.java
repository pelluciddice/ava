package pellucid.ava.gamemodes.scoreboard;

import net.minecraft.nbt.CompoundTag;
import pellucid.ava.util.DataTypes;

public class AVAPlayerStat
{
    private int score;
    private int death;
    private float damage;

    public AVAPlayerStat(CompoundTag compound)
    {
        this(DataTypes.INT.read(compound, "score"), DataTypes.INT.read(compound, "death"), DataTypes.FLOAT.read(compound, "damage"));
    }

    public AVAPlayerStat()
    {
        this(0, 0, 0);
    }

    public AVAPlayerStat(int score, int death, float damage)
    {
        this.score = score;
        this.death = death;
        this.damage = damage;
    }

    public CompoundTag write()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "score", score);
        DataTypes.INT.write(compound, "death", death);
        DataTypes.FLOAT.write(compound, "damage", damage);
        return compound;
    }

    public int getScore()
    {
        return score;
    }

    public AVAPlayerStat setScore(int score)
    {
        this.score = score;
        return this;
    }

    public int getDeath()
    {
        return death;
    }

    public AVAPlayerStat setDeath(int death)
    {
        this.death = death;
        return this;
    }

    public float getDamage()
    {
        return damage;
    }

    public AVAPlayerStat setDamage(float damage)
    {
        this.damage = damage;
        return this;
    }

    public void addScore(int score)
    {
        this.score += score;
    }

    public void addDeath()
    {
        this.death++;
    }

    public void addDamage(float damage)
    {
        this.damage += damage;
    }
}
