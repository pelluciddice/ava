package pellucid.ava.gamemodes.scoreboard;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.competitive_mode.CompetitiveUI;
import pellucid.ava.gamemodes.modes.EscortMode;
import pellucid.ava.gamemodes.modes.GameMode;
import pellucid.ava.gamemodes.modes.ScoredTeamMode;
import pellucid.ava.gamemodes.modes.TeamedMode;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Pair;

public class GameModeRenderer
{
    private static final Pair<ResourceLocation, ResourceLocation> EU = Pair.of(new ResourceLocation(AVA.MODID, "textures/overlay/eu.png"), new ResourceLocation(AVA.MODID, "textures/overlay/eu_enemy.png"));
    private static final Pair<ResourceLocation, ResourceLocation> NRF = Pair.of(new ResourceLocation(AVA.MODID, "textures/overlay/nrf.png"), new ResourceLocation(AVA.MODID, "textures/overlay/nrf_enemy.png"));
    private static final ResourceLocation DEFENSE_LINE = new ResourceLocation(AVA.MODID, "textures/overlay/escort_defense_line.png");
    private static final ResourceLocation ESCORT_LEOPARD_GREEN = new ResourceLocation(AVA.MODID, "textures/overlay/escort_leopard_green.png");
    private static final ResourceLocation ESCORT_LEOPARD_ORANGE = new ResourceLocation(AVA.MODID, "textures/overlay/escort_leopard_orange.png");
    private static final ResourceLocation ESCORT_LEOPARD_RED = new ResourceLocation(AVA.MODID, "textures/overlay/escort_leopard_red.png");

    public static void renderHUD(Minecraft minecraft, Level world, Player player, Font font, GuiGraphics graphics, float partialTicks, int screenWidth, int screenHeight)
    {
        AVAScoreboardManager scoreboard = AVACrossWorldData.getInstance().scoreboardManager;
        GameMode mode = scoreboard.getGamemode();
        PoseStack stack = graphics.pose();
        if (mode instanceof TeamedMode m)
        {
            stack.pushPose();
            stack.translate(screenWidth / 2.0F, 0, 0.0F);


            if (m instanceof ScoredTeamMode)
            {
                // TeamSide Background
                int offset00 = -1;
                if (player.isAlliedTo(m.getTeamA(world)))
                    offset00 = -36;
                else if (player.isAlliedTo(m.getTeamB(world)))
                    offset00 = 36;
                if (offset00 != -1)
                {
                    int wTop = 26;
                    int wBot = 22;
                    int wTop2 = 19;
                    int wBot2 = 15;
                    int y = 6;
                    int y2 = 17;
                    int h = 5;

                    AVAClientUtil.fillGradient(stack, offset00 - wTop, y, offset00 + wTop, y, offset00 - wBot - 2, y + h / 2.0F, offset00 + wBot + 2, y + h / 2.0F, 0, AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB(), Direction.SOUTH);
                    AVAClientUtil.fillGradient(stack, offset00 - wTop + 2, y + h / 2.0F, offset00 + wTop - 2, y + h / 2.0F, offset00 - wBot, y + h, offset00 + wBot, y + h, 0, AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG_TRANSPARENT.getRGB(), Direction.SOUTH);
                    AVAClientUtil.fillGradient(stack, offset00 - wTop2, y2, offset00 + wTop2, y2, offset00 - wBot2 - 2, y2 + h / 2.0F, offset00 + wBot2 + 2, y2 + h / 2.0F, 0, AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG_TRANSPARENT.getRGB(), Direction.NORTH);
                    AVAClientUtil.fillGradient(stack, offset00 - wTop2 + 2, y2 + h / 2.0F, offset00 + wTop2 - 2, y2 + h / 2.0F, offset00 - wBot2, y2 + h, offset00 + wBot2, y2 + h, 0, AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB(), Direction.NORTH);
                }
            }


            // Background
            int w = 70;
            CompetitiveUI.drawLine(graphics, -w, 6, w, 6.5F);
            AVAClientUtil.fillGradient(stack, -w, 6, w, 11, 0, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT.getRGB(), Direction.NORTH);
            AVAClientUtil.fillCoord(stack, -w, 11, w, 19, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB());
            AVAClientUtil.fillGradient(stack, -w, 19, w, 25, 0, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT.getRGB(), Direction.SOUTH);


            stack.pushPose();
            int offset0 = 76;
            float scale0 = 0.65F;
            stack.scale(scale0, scale0, scale0);
            graphics.drawCenteredString(font, m.displayName, -offset0, 0, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
            stack.popPose();

            if (mode instanceof ScoredTeamMode scored)
            {
                // Description
                stack.pushPose();
                stack.scale(scale0, scale0, scale0);
                graphics.drawCenteredString(font, Component.translatable(m.targetDisplayNameKey, scored.getTargetScore()), offset0, 0, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
                stack.popPose();


                // Scores
                stack.pushPose();
                int offset = 10;
                float scale = 1.7F;
                stack.scale(scale + 0.25F, scale, scale);
                String t = String.valueOf(scored.getScore(scoreboard.teamA));
                graphics.drawString(minecraft.font, t, -offset - font.width(t), 4, AVAConstants.AVA_HUD_TEXT_WHITE);
                graphics.drawString(minecraft.font, String.valueOf(scored.getScore(scoreboard.teamB)), +offset, 4, AVAConstants.AVA_HUD_TEXT_WHITE);
                stack.popPose();
            }


            // Team Names
            stack.pushPose();
            int offset2 = 65;
            float scale2 = 0.9F;
            stack.scale(scale2, scale2, scale2);
            Component t2 = m.getTeamA(world).getDisplayName();
            graphics.drawString(minecraft.font, t2, -offset2 - font.width(t2), 11, AVAConstants.AVA_HUD_TEXT_WHITE);
            graphics.drawString(minecraft.font, m.getTeamB(world).getDisplayName(), +offset2, 11, AVAConstants.AVA_HUD_TEXT_WHITE);
            stack.popPose();


            // Team player count
            int offset3 = 58;
            int w2 = 7;
            graphics.fill(-offset3 - w2, 18, -offset3, 23, AVAConstants.AVA_COMPETITIVE_UI_GRAY.getRGB());
            graphics.fill(offset3, 18, offset3 + w2, 23, AVAConstants.AVA_COMPETITIVE_UI_GRAY.getRGB());
            stack.pushPose();
            float scale3 = 0.75F;
            stack.scale(scale3, scale3, scale3);
            int offset4 = 82;
            graphics.drawCenteredString(font, String.valueOf(m.getTeamA(world).getPlayers().size()), -offset4, 23, AVAConstants.AVA_HUD_TEXT_WHITE);
            graphics.drawCenteredString(font, String.valueOf(m.getTeamB(world).getPlayers().size()), offset4, 23, AVAConstants.AVA_HUD_TEXT_WHITE);
            stack.popPose();


            // Team Icon
            int offset5 = 35;
            int s = 22;
            int y = 4;
            AVAClientUtil.drawTransparent(true);
            AVAClientUtil.blit(stack, Pair.or(EU, player.isAlliedTo(m.getTeamA(world))), -offset5 - s, y, -offset5, y + s);
            AVAClientUtil.blit(stack, Pair.or(NRF, player.isAlliedTo(m.getTeamB(world))), offset5, y, offset5 + s, y + s);
            AVAClientUtil.drawTransparent(false);


            if (mode instanceof EscortMode escort)
            {
                try
                {
                    // Escort Progress
                    int w3 = 36;
                    AVAClientUtil.fillCoord(stack, -w3, 14, w3, 17, AVAConstants.AVA_COMPETITIVE_UI_GRAY.getRGB());

                    escort.getTank(world).ifPresent((tank) ->
                    {
                        ResourceLocation texture = tank.getHealth() >= tank.getMaxHealth() / 2.0F ? ESCORT_LEOPARD_GREEN : tank.requireRepair() ? ESCORT_LEOPARD_RED : ESCORT_LEOPARD_ORANGE;
                        AVAClientUtil.drawTransparent(true);
                        float x = (w3 * 2 * (escort.distancePassed(world) / (float) escort.totalDistance)) - w3;
                        AVAClientUtil.blit(stack, texture, x - 11, 10, x + 11, 36);
                        AVAClientUtil.drawTransparent(false);
                    });
                    for (int i = 1; i < escort.distOfDefenseLines.size(); i++)
                    {
                        int dist = escort.distOfDefenseLines.get(i);
                        float x = (w3 * 2 * (dist / (float) escort.totalDistance)) - w3;
                        float y2 = 15.5F;
                        float s2 = 3.0F;
                        stack.pushPose();
                        AVAClientUtil.drawTransparent(true);
                        AVAClientUtil.blit(stack, DEFENSE_LINE, x - s2 * 1.25F, y2 - s2 - 2, x + s2 * 1.25F, y2 + s2);
                        AVAClientUtil.drawTransparent(false);
                        stack.popPose();

                        if (i == escort.getDefenseLines().keySet().stream().toList().indexOf(escort.getNextDefenseLineIndex()))
                        {
                            graphics.fill((int) x - 7, (int) y2 + 8, (int) x + 7, (int) y2 + 14, 0xFF000000);
                            AVAClientUtil.scaleText(stack, (int) x, (int) y2 + 8, 0.65F, 0.65F, () ->
                            {
                                graphics.drawCenteredString(font, Component.literal(AVACommonUtil.round(escort.distanceToNextDefenseLine(world), 1) + "m"), 0, 0, AVAConstants.AVA_HUD_TEXT_WHITE);
                            });
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }


            stack.popPose();


            if (mode instanceof EscortMode escort)
            {
                stack.pushPose();
                stack.translate(screenWidth, screenHeight, 0.0F);

                escort.getTank(world).ifPresent((tank) -> {
                    if (!tank.requireRepair())
                        return;
                    AVAClientUtil.fillGradient(stack, -60, -30, 0, -30, -40, 0, 0, 0, 0, 0x00000000, AVAConstants.AVA_COMPETITIVE_UI_DARK_BLUE.getRGB(), Direction.EAST);

                    AVAClientUtil.scaleText(stack, -38, -28, 0.925F, 0.95F, () -> {
                        graphics.drawCenteredString(font, Component.translatable("ava.hud.request_uav_support"), 0, 0, AVAConstants.AVA_HUD_TEXT_ORANGE);
                    });
                    AVAClientUtil.scaleText(stack, -20, -22, 2.0F, 2.0F, () -> {
                        graphics.drawCenteredString(font, AVACommonUtil.fillTenth(String.valueOf(escort.uavSupportTimer / 1200)) + ":" + AVACommonUtil.fillTenth(String.valueOf(escort.uavSupportTimer / 20 % 60)), 0, 0, AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG.getRGB());
                    });
                });
                stack.popPose();
            }
        }
    }
}
