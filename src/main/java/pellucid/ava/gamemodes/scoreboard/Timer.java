package pellucid.ava.gamemodes.scoreboard;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.level.Level;
import net.minecraft.world.scores.Objective;
import net.minecraft.world.scores.ScoreAccess;
import net.minecraft.world.scores.ScoreHolder;
import net.minecraft.world.scores.Scoreboard;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;

import java.util.ArrayList;
import java.util.List;

public class Timer
{
    private final String name;
    private final String storage;
    private int initial;
    private boolean paused;
    private boolean displaying;

    public Timer(CompoundTag compound)
    {
        this(compound.getString("name"), compound.getString("storage"), DataTypes.INT.read(compound, "initial"));
        this.paused = DataTypes.BOOLEAN.read(compound, "paused");
        this.displaying = DataTypes.BOOLEAN.read(compound, "displaying");
    }

    public Timer(String name, String storage, int initial)
    {
        this.name = name;
        this.storage = storage;
        this.initial = initial;
    }

    public void setInitial(int initial)
    {
        this.initial = initial;
    }

    public void reset(Level world)
    {
        try
        {
            world.getScoreboard().getOrCreatePlayerScore(ScoreHolder.forNameOnly(getName()), world.getScoreboard().getObjective(getStorage())).set(getInitial());
        }
        catch (Exception e)
        {
        }
    }

    public int getTime(Level world)
    {
        try
        {
            return world.getScoreboard().getOrCreatePlayerScore(ScoreHolder.forNameOnly(getName()), world.getScoreboard().getObjective(getStorage())).get();
        }
        catch (Exception e)
        {
            return 0;
        }
    }

    public String getMinuteString(Level world)
    {
        return AVACommonUtil.fillTenth(String.valueOf(getTime(world) / 1200));
    }

    public String getSecondsString(Level world)
    {
        return AVACommonUtil.fillTenth(String.valueOf(getTime(world) / 20 % 60));
    }

    public static void tick(Level world, List<Timer> timers)
    {
        for (Timer timer : new ArrayList<>(timers))
        {
            if (timer.isPaused())
                continue;
            Scoreboard board = world.getScoreboard();
            Objective object = board.getObjective(timer.getStorage());
            if (object != null && !object.getCriteria().isReadOnly())
            {
                ScoreAccess score = world.getScoreboard().getOrCreatePlayerScore(ScoreHolder.forNameOnly(timer.getName()), object);
                if (score.get() > 0)
                    score.add(-1);
            }
        }
    }

    public static ListTag write(List<Timer> timers)
    {
        ListTag list = new ListTag();
        for (Timer timer : timers)
            list.add(timer.write());
        return list;
    }

    public static List<Timer> read(ListTag list)
    {
        List<Timer> timers = new ArrayList<>();
        for (Tag nbt : list)
            timers.add(new Timer((CompoundTag) nbt));
        return timers;
    }

    public CompoundTag write()
    {
        CompoundTag compound = new CompoundTag();
        compound.putString("name", name);
        compound.putString("storage", storage);
        DataTypes.INT.write(compound, "initial", initial);
        DataTypes.BOOLEAN.write(compound, "paused", paused);
        DataTypes.BOOLEAN.write(compound, "displaying", displaying);
        return compound;
    }

    public String getName()
    {
        return name;
    }

    public int getInitial()
    {
        return initial;
    }

    public String getStorage()
    {
        return storage;
    }

    public void setPaused(boolean paused)
    {
        this.paused = paused;
    }

    public void setDisplaying(boolean displaying)
    {
        this.displaying = displaying;
    }

    public boolean isPaused()
    {
        return paused;
    }

    public boolean isDisplaying()
    {
        return displaying;
    }

    @Override
    public String toString()
    {
        return "Timer{" +
                "name='" + name + '\'' +
                ", storage='" + storage + '\'' +
                ", initial=" + initial +
                ", paused=" + paused +
                ", displaying=" + displaying +
                '}';
    }
}
