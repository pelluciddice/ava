package pellucid.ava.gamemodes.scoreboard;

import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.scores.PlayerTeam;
import pellucid.ava.AVA;
import pellucid.ava.gamemodes.modes.GameMode;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.INBTSerializable;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AVAScoreboardManager implements INBTSerializable<CompoundTag>
{
    private final Map<UUID, AVAPlayerStat> stats = new HashMap<>();
    public String teamA = "";
    public String teamB = "";
    private final PlayerTeam[] cachedTeams = new PlayerTeam[2];
    private int mapWidth = 0;
    private int mapHeight = 0;
    private float scale = 1.0F;
    private Direction direction = Direction.NORTH;
    private BlockPos from = BlockPos.ZERO;
    private BlockPos to = BlockPos.ZERO;
    private boolean heightSensitive;
    private List<Integer> mapColours = new ArrayList<>();
    private boolean requireMapUpdate = false;
    private boolean transparentAir = false;
    private GameMode gamemode = GameModes.VANILLA;

    public GameMode getGamemode()
    {
        return gamemode;
    }

    public void ifGamemodeRunning(Consumer<GameMode> action)
    {
        if (gamemode.isRunning())
            action.accept(gamemode);
    }

    public boolean setGamemode(GameMode gamemode)
    {
        if (this.gamemode != gamemode)
        {
            this.gamemode = gamemode;
            return true;
        }
        return false;
    }

    public void removeOfflinePlayersFromTeams(Level world)
    {
        PlayerTeam a = getTeamA(world);
        PlayerTeam b = getTeamB(world);
        if (a != null)
        {
            a.getPlayers().removeIf((name) -> world.getServer().getPlayerList().getPlayerByName(name) == null);
            world.getScoreboard().onTeamChanged(a);
        }
        if (b != null)
        {
            b.getPlayers().removeIf((name) -> world.getServer().getPlayerList().getPlayerByName(name) == null);
            world.getScoreboard().onTeamChanged(b);
        }
    }

    public CompoundTag writeMap()
    {
        CompoundTag compound = new CompoundTag();
        compound.putIntArray("map", mapColours);
        DataTypes.INT.write(compound, "width", mapWidth);
        DataTypes.INT.write(compound, "height", mapHeight);
        DataTypes.FLOAT.write(compound, "scale", scale);
        DataTypes.STRING.write(compound, "direction", direction.getName());
        DataTypes.BLOCKPOS.write(compound, "from", from);
        DataTypes.BLOCKPOS.write(compound, "to", to);
        DataTypes.BOOLEAN.write(compound, "heightSensitive", heightSensitive);
        DataTypes.BOOLEAN.write(compound, "transparentAir", transparentAir);
        return compound;
    }

    public BlockPos getFrom()
    {
        return from;
    }

    public BlockPos getTo()
    {
        return to;
    }

    public float getScale()
    {
        return scale;
    }

    public Direction getDirection()
    {
        return direction;
    }

    public List<Integer> getMapColours()
    {
        return mapColours;
    }

    public int getMapWidth()
    {
        return mapWidth;
    }

    public int getMapHeight()
    {
        return mapHeight;
    }

    public boolean hasMap()
    {
        return mapWidth > 0 && mapHeight > 0 && !mapColours.isEmpty();
    }

    public void updateMap(CompoundTag compound)
    {
        mapColours.clear();
        mapColours.addAll(Arrays.stream(compound.getIntArray("map")).boxed().collect(Collectors.toList()));
        mapWidth = DataTypes.INT.read(compound, "width");
        mapHeight = DataTypes.INT.read(compound, "height");
        scale = DataTypes.FLOAT.read(compound, "scale");
        direction = Direction.byName(compound.getString("direction"));
        if (direction == null)
            direction = Direction.NORTH;
        from = DataTypes.BLOCKPOS.read(compound, "from");
        to = DataTypes.BLOCKPOS.read(compound, "to");
        heightSensitive = DataTypes.BOOLEAN.read(compound, "heightSensitive");
        transparentAir = DataTypes.BOOLEAN.read(compound, "transparentAir");
    }

    private void updateMap(Level world, BlockPos from, BlockPos to, boolean heightSensitive, boolean transparentAir)
    {
        final int minX = Math.min(from.getX(), to.getX());
        final int minY = Math.min(from.getY(), to.getY());
        final int minZ = Math.min(from.getZ(), to.getZ());
        final int maxX = Math.max(from.getX(), to.getX());
        final int maxY = Math.max(from.getY(), to.getY());
        final int maxZ = Math.max(from.getZ(), to.getZ());


        for (int z = minZ; z <= maxZ; z++)
        {
            double d0 = 0.0D;

            for (int x = minX; x <= maxX; x++)
            {
                Multiset<MapColor> set = LinkedHashMultiset.create();
                LevelChunk chunk = world.getChunkAt(new BlockPos(x, 0, z));
                if (!chunk.isEmpty())
                {
                    ChunkPos chunkpos = chunk.getPos();
                    int chunkX = x & 15;
                    int chunkZ = z & 15;
                    int k3 = 0;
                    double d1 = 0.0D;

                    if (world.dimensionType().hasCeiling())
                    {
                        int l3 = x + z * 231871;
                        l3 = l3 * l3 * 31287121 + l3 * 11;
                        if ((l3 >> 20 & 1) == 0)
                            set.add(Blocks.DIRT.defaultBlockState().getMapColor(world, BlockPos.ZERO), 10);
                        else
                            set.add(Blocks.STONE.defaultBlockState().getMapColor(world, BlockPos.ZERO), 100);

                        d1 = 100.0D;
                    }
                    else
                    {
                        BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
                        BlockPos.MutableBlockPos pos2 = new BlockPos.MutableBlockPos();

                        int height = (heightSensitive ? maxY : chunk.getHeight(Heightmap.Types.WORLD_SURFACE, chunkX, chunkZ)) + 1;
                        BlockState state;
                        if (height <= world.getMinBuildHeight() + 1)
                            state = transparentAir ? Blocks.BEDROCK.defaultBlockState() : Blocks.AIR.defaultBlockState();
                        else
                        {
                            do
                            {
                                --height;
                                pos.set(chunkpos.getMinBlockX() + chunkX, height, chunkpos.getMinBlockZ() + chunkZ);
                                state = chunk.getBlockState(pos);
                            }
                            while (state.getMapColor(world, pos) == MapColor.NONE && height > world.getMinBuildHeight() && (!heightSensitive || height > minY));

                            if (height > world.getMinBuildHeight() && !state.getFluidState().isEmpty())
                            {
                                int y2 = height - 1;
                                pos2.set(pos);

                                BlockState state2;
                                do
                                {
                                    pos2.setY(y2--);
                                    state2 = chunk.getBlockState(pos2);
                                    ++k3;
                                }
                                while (y2 > world.getMinBuildHeight() && !state2.getFluidState().isEmpty());

                                state = AVACommonUtil.getCorrectStateForFluidBlock(world, state, pos);
                            }
                        }

                        d1 += height;
                        set.add(state.getMapColor(world, pos));
                    }
                    MapColor colour = Iterables.getFirst(Multisets.copyHighestCountFirst(set), MapColor.NONE);
                    MapColor.Brightness brightness;
                    if (colour == MapColor.WATER)
                    {
                        double d2 = (double) k3 * 0.1D + (double)(x + z & 1) * 0.2D;
                        if (d2 < 0.5D)
                            brightness = MapColor.Brightness.HIGH;
                        else if (d2 > 0.9D)
                            brightness = MapColor.Brightness.LOW;
                        else
                            brightness = MapColor.Brightness.NORMAL;
                    }
                    else
                    {
                        double d3 = (d1 - d0) * 4.0D / 5.0D + ((double)(x + z & 1) - 0.5D) * 0.4D;
                        if (d3 > 0.6D)
                            brightness = MapColor.Brightness.HIGH;
                        else if (d3 < -0.6D)
                            brightness = MapColor.Brightness.LOW;
                        else
                            brightness = MapColor.Brightness.NORMAL;
                    }

                    d0 = d1;
                    int index = x - minX + (z - minZ) * mapWidth;
                    if (direction.getAxis() == Direction.Axis.X)
                        index = z - minZ + (x - minX) * mapWidth;
                    mapColours.set(index, (int) colour.getPackedId(brightness));
                }
            }
        }
    }

    private void updateMap(Level world)
    {
        updateMap(world, from, to, direction, scale, heightSensitive, transparentAir);
    }

    public void updateMap(Level world, BlockPos from, BlockPos to, Direction up, float scale, boolean heightSensitive, boolean transparentAir)
    {
        final int minX = Math.min(from.getX(), to.getX());
        final int minZ = Math.min(from.getZ(), to.getZ());
        final int maxX = Math.max(from.getX(), to.getX());
        final int maxZ = Math.max(from.getZ(), to.getZ());
        this.from = new BlockPos(minX, world.getMinBuildHeight(), minZ);
        this.to = new BlockPos(maxX, world.getMaxBuildHeight(), maxZ);
        mapWidth = Math.max(minX, maxX) - Math.min(minX, maxX) + 1;
        mapHeight = Math.max(minZ, maxZ) - Math.min(minZ, maxZ) + 1;
        if (up.getAxis() == Direction.Axis.X)
        {
            int w = mapWidth;
            mapWidth = mapHeight;
            mapHeight = w;
        }

        direction = up;
        this.scale = scale;
        mapColours = new ArrayList<>() {{
            for (int i = 0; i < mapWidth * mapHeight; i ++)
                add(0);
        }};
        this.heightSensitive = heightSensitive;
        this.transparentAir = transparentAir;
        try
        {
            if (!world.hasChunksAt(from, to))
            {
                requireMapUpdate = true;
                return;
            }
            updateMap(world, from, to, heightSensitive, transparentAir);
            DataToClientMessage.scoreboardMap(null);
        }
        catch (Exception e)
        {
            AVA.LOGGER.error(world);
            AVA.LOGGER.error(from);
            AVA.LOGGER.error(to);
            AVA.LOGGER.error(up);
            AVA.LOGGER.error(scale);
            AVA.LOGGER.error(heightSensitive);
            AVA.LOGGER.error(transparentAir);
            e.printStackTrace();
        }
    }

    public AVAScoreboardManager setTeamA(String teamA)
    {
        this.teamA = teamA;
        return this;
    }

    public AVAScoreboardManager setTeamB(String teamB)
    {
        this.teamB = teamB;
        return this;
    }

    public Map<UUID, AVAPlayerStat> getStats()
    {
        return stats;
    }

    public void tick(ServerLevel world)
    {
        if (world.getGameTime() % 40 == 0)
        {
            List<ServerPlayer> players = getPlayers(world, teamA);
            players.addAll(getPlayers(world, teamB));
            for (ServerPlayer player : players)
            {
                UUID id = player.getUUID();
                if (!stats.containsKey(id))
                    stats.put(id, new AVAPlayerStat());
            }
            if (requireMapUpdate && world.hasChunksAt(from, to))
            {
                requireMapUpdate = false;
                updateMap(world);
            }
        }
        getGamemode().tick(world);
    }

    public List<ServerPlayer> getPlayers(ServerLevel world, String teamName)
    {
        PlayerTeam team = world.getScoreboard().getPlayerTeam(teamName);
        PlayerList players = world.getServer().getPlayerList();
        return team == null ? new ArrayList<>() : AVACommonUtil.collect(team.getPlayers(), players::getPlayerByName);
    }

    @Nullable
    public PlayerTeam getTeamA(Level world)
    {
        return getTeam(world, teamA, 0);
    }

    @Nullable
    public PlayerTeam getTeamB(Level world)
    {
        return getTeam(world, teamB, 1);
    }

    private PlayerTeam getTeam(Level world, String name, int index)
    {
        PlayerTeam team = cachedTeams[index];
        if (team == null || !team.getName().equals(name))
            cachedTeams[index] = world.getScoreboard().getPlayerTeam(name);
        return cachedTeams[index];
    }

    public AVAPlayerStat getPlayerStat(Player player)
    {
        return getPlayerStat(player.getUUID(), true);
    }

    public AVAPlayerStat getPlayerStat(UUID id)
    {
        return getPlayerStat(id, false);
    }

    public AVAPlayerStat getPlayerStat(UUID id, boolean putIfAbsent)
    {
        try
        {
            return putIfAbsent ? stats.computeIfAbsent(id, (id2) -> new AVAPlayerStat()) : stats.get(id);
        }
        catch (ConcurrentModificationException e)
        {
            return stats.get(id);
        }
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        ListTag list = new ListTag();
        new HashMap<>(stats).forEach((id, stat) -> {
            CompoundTag compound2 = new CompoundTag();
            compound2.putUUID("id", id);
            compound2.put("stat", stat.write());
            list.add(compound2);
        });
        compound.put("stats", list);
        compound.putString("teamA", teamA);
        compound.putString("teamB", teamB);
        compound.put("map", writeMap());
        DataTypes.BOOLEAN.write(compound, "requireMapUpdate", requireMapUpdate);
        DataTypes.STRING.write(compound, "gamemode", gamemode.getName());
        compound.put("gamemodeData", gamemode.serializeNBT());
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        stats.clear();
        for (Tag tag : nbt.getList("stats", Tag.TAG_COMPOUND))
        {
            CompoundTag compound = (CompoundTag) tag;
            stats.put(DataTypes.UUID.read(compound, "id"), new AVAPlayerStat(compound.getCompound("stat")));
        }
        teamA = nbt.getString("teamA");
        teamB = nbt.getString("teamB");
        updateMap(nbt.getCompound("map"));
        requireMapUpdate = nbt.getBoolean("requireMapUpdate");
        if (nbt.contains("gamemodeData"))
        {
            gamemode = GameModes.GAMEMODES.get(nbt.getString("gamemode"));
            gamemode.deserializeNBT(nbt.getCompound("gamemodeData"));
        }
    }

    @Override
    public String toString()
    {
        return "AVAScoreboardManager{" +
                "stats=" + stats +
                ", teamA='" + teamA + '\'' +
                ", teamB='" + teamB + '\'' +
                ", cachedTeams=" + Arrays.toString(cachedTeams) +
                ", gamemode=" + gamemode +
                '}';
    }
}
