package pellucid.ava.entities.livings;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import pellucid.ava.entities.functionalities.IDifficultyScaledHostile;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;

public abstract class AVABotEntity extends PathfinderMob implements IDifficultyScaledHostile
{
    public int blind;
    protected Item weapon;

    protected AVABotEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public boolean doHurtTarget(Entity entityIn)
    {
        float f = (float)this.getAttributeValue(Attributes.ATTACK_DAMAGE);
        float f1 = (float)this.getAttributeValue(Attributes.ATTACK_KNOCKBACK);
        if (entityIn instanceof LivingEntity) {
            f += EnchantmentHelper.getDamageBonus(this.getMainHandItem(), entityIn.getType());
            f1 += (float)EnchantmentHelper.getKnockbackBonus(this);
        }

        int i = EnchantmentHelper.getFireAspect(this);
        if (i > 0) {
            entityIn.setRemainingFireTicks(i * 4 * 20);
        }

        boolean flag = AVAWeaponUtil.attackEntity(entityIn, damageSources().mobAttack(this), f, 1.0F, 0.0F);
        if (flag) {
            if (f1 > 0.0F && entityIn instanceof LivingEntity) {
                ((LivingEntity)entityIn).knockback(f1 * 0.5F, (double) Mth.sin(this.getYRot() * ((float)Math.PI / 180F)), (double)(-Mth.cos(this.getYRot() * ((float)Math.PI / 180F))));
                this.setDeltaMovement(this.getDeltaMovement().multiply(0.6D, 1.0D, 0.6D));
            }

            if (entityIn instanceof Player) {
                Player playerentity = (Player)entityIn;
                this.maybeDisableShield(playerentity, this.getMainHandItem(), playerentity.isUsingItem() ? playerentity.getUseItem() : ItemStack.EMPTY);
            }

            this.doEnchantDamageEffects(this, entityIn);
            this.setLastHurtMob(entityIn);
        }

        return flag;
    }

    private void maybeDisableShield(Player p_233655_1_, ItemStack p_233655_2_, ItemStack p_233655_3_)
    {
        if (!p_233655_2_.isEmpty() && !p_233655_3_.isEmpty() && p_233655_2_.getItem() instanceof AxeItem && p_233655_3_.getItem() == Items.SHIELD)
        {
            float f = 0.25F + (float)EnchantmentHelper.getBlockEfficiency(this) * 0.05F;
            if (this.random.nextFloat() < f)
            {
                p_233655_1_.getCooldowns().addCooldown(Items.SHIELD, 100);
                this.level().broadcastEntityEvent(p_233655_1_, (byte)30);
            }
        }

    }

    @Override
    protected void populateDefaultEquipmentSlots(RandomSource random, DifficultyInstance difficulty)
    {
        ItemStack stack = getInitialMainWeapon();
        setItemSlot(EquipmentSlot.MAINHAND, stack);
        Item item = stack.getItem();
        if (item instanceof AVAItemGun)
            ((AVAItemGun) item).forceReload(stack);
        weapon = item;
    }

    @Override
    public void tick()
    {
        super.tick();
        if (blind > 0)
            blind--;
    }

    @Nullable
    @Override
    public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn)
    {
        populateDefaultEquipmentSlots(random, difficultyIn);
        AttributeInstance health = getAttribute(Attributes.MAX_HEALTH);
        health.setBaseValue(health.getBaseValue() * getHealthScale(difficultyIn.getDifficulty()));
        setHealth(getMaxHealth());
        return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn);
    }

    public abstract ItemStack getInitialMainWeapon();

    public boolean canAttackEntity(Entity entity)
    {
        return blind <= 0;
    }

    protected boolean isAttackablePlayer(Player player)
    {
        return player.isAlive() && !player.isCreative() && canAttackEntity(player);
    }

    @Override
    public boolean canBeLeashed(Player player)
    {
        return false;
    }

    @Override
    public boolean causeFallDamage(float distance, float damageMultiplier, DamageSource p_147189_)
    {
        return false;
    }

    @Override
    protected int decreaseAirSupply(int air)
    {
        return air;
    }

    @Override
    public boolean removeWhenFarAway(double distanceToClosestPlayer)
    {
        return false;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.INT.write(compound, "blind", blind);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        blind = DataTypes.INT.read(compound, "blind");
    }
}
