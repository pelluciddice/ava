package pellucid.ava.entities.livings.projectiles;

import com.google.common.collect.ImmutableMap;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.livings.guns.GunGuardEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.SpecialWeapons;

public class GrenadeLauncherGuardEntity extends GunGuardEntity
{
    public GrenadeLauncherGuardEntity(EntityType<? extends GrenadeLauncherGuardEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public int getRangeForAttack()
    {
        return 10;
    }

    @Override
    protected int getAttackInterval()
    {
        return 60;
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        ItemStack weapon = new ItemStack(SpecialWeapons.GM94.get());
        AVAItemGun gun = (AVAItemGun) weapon.getItem();
        gun.forceReload(weapon);
        return weapon;
    }

    private static final ImmutableMap<Item, Integer> DROPS = ImmutableMap.of(SpecialWeapons.GM94_GRENADE.get(), 7);
    @Override
    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return DROPS;
    }
}
