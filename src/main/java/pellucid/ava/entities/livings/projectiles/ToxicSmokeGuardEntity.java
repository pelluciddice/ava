package pellucid.ava.entities.livings.projectiles;

import com.google.common.collect.ImmutableMap;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.livings.RangedGuardEntity;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.throwables.ToxicSmokeGrenade;

public class ToxicSmokeGuardEntity extends RangedGuardEntity
{
    public ToxicSmokeGuardEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public int getRangeForAttack()
    {
        return 10;
    }

    @Override
    protected int getAttackInterval()
    {
        return 120;
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        return new ItemStack(Projectiles.M18_TOXIC.get());
    }

    @Override
    public void performRangedAttack(LivingEntity target, float distanceFactor)
    {
        if (!level().isClientSide() && level() instanceof ServerLevel)
        {
            ItemStack stack = getMainHandItem();
            if (stack.getItem() instanceof ToxicSmokeGrenade)
                ((ToxicSmokeGrenade) stack.getItem()).toss((ServerLevel) level(), this, target, stack, false);
        }
    }

    private static final ImmutableMap<Item, Integer> DROPS = ImmutableMap.of(Projectiles.M18_TOXIC.get(), 4);
    @Override
    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return DROPS;
    }

    @Override
    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.randomColourFrom(ColourTypes.YELLOW, ColourTypes.RED);
    }
}
