package pellucid.ava.entities.livings.melees;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.livings.MeleeGuardEntity;

public class BlueMeleeGuardEntity extends MeleeGuardEntity
{
    public BlueMeleeGuardEntity(EntityType<? extends AVAHostileEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        return new ItemStack(Items.STICK);
    }

    @Override
    protected float getMovementSpeedFactor()
    {
        return 0.45F;
    }

    @Override
    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.BLUE;
    }
}
