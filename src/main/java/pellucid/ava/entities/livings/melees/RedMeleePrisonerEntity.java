package pellucid.ava.entities.livings.melees;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.livings.MeleeGuardEntity;

public class RedMeleePrisonerEntity extends MeleeGuardEntity
{
    public RedMeleePrisonerEntity(EntityType<? extends AVAHostileEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        return new ItemStack(Items.IRON_AXE);
    }

    @Override
    protected float getMovementSpeedFactor()
    {
        return 0.65F;
    }

    @Override
    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.RED_PRISONER;
    }

    public static AttributeSupplier.Builder registerAttributes()
    {
        return MeleeGuardEntity.registerAttributes()
                .add(Attributes.ATTACK_DAMAGE, 6.5F)
                .add(Attributes.MAX_HEALTH, 45.0F);
    }
}
