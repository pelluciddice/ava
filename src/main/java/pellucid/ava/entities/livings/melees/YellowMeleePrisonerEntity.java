package pellucid.ava.entities.livings.melees;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.livings.MeleeGuardEntity;
import pellucid.ava.items.init.MeleeWeapons;

public class YellowMeleePrisonerEntity extends MeleeGuardEntity
{
    public YellowMeleePrisonerEntity(EntityType<? extends AVAHostileEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        return new ItemStack(random.nextBoolean() ? MeleeWeapons.FIELD_KNIFE.get() : Items.IRON_SHOVEL);
    }

    @Override
    protected float getMovementSpeedFactor()
    {
        return 0.775F;
    }

    @Override
    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.YELLOW_PRISONER;
    }
}
