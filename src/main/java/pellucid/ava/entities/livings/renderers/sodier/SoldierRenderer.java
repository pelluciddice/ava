package pellucid.ava.entities.livings.renderers.sodier;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.entities.livings.renderers.guard.GuardModel;
import pellucid.ava.entities.objects.parachute.renderers.ParachuteModel;
import pellucid.ava.entities.objects.parachute.renderers.ParachuteRenderer;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import java.util.function.Consumer;

public class SoldierRenderer extends HumanoidMobRenderer<SidedSmartAIEntity, GuardModel<SidedSmartAIEntity>>
{
    private final Consumer<PoseStack> transformer;
    private final Model parachute;

    public SoldierRenderer(EntityRendererProvider.Context context)
    {
        this(context, (stack) -> {});
    }

    public SoldierRenderer(EntityRendererProvider.Context context, Consumer<PoseStack> transfomer)
    {
        super(context, new GuardModel<>(context.bakeLayer(AVAModelLayers.HUMANOID)), 0.5F);
        this.transformer = transfomer;
//        layers.remove(layers.size() - 1);
//        addLayer(new AVATPRenderer.AVAArmourLayer(this, new GuardModel<>(context.bakeLayer(AVAModelLayers.HUMANOID)), new GuardModel<>(context.bakeLayer(AVAModelLayers.HUMANOID))));
//        addLayer(new AVATPRenderer.AVAHeldItemLayer(this));
        addLayer(new HumanoidArmorLayer<>(this, new GuardModel<>(context.bakeLayer(AVAModelLayers.HUMANOID)), new GuardModel<>(context.bakeLayer(AVAModelLayers.HUMANOID)), context.getModelManager()));
        addLayer(new ItemInHandLayer<>(this, context.getItemInHandRenderer()));
        this.parachute = new ParachuteModel(context.bakeLayer(AVAModelLayers.PARACHUTE));
        new ParachuteRenderer(context);
    }

    @Override
    public void render(SidedSmartAIEntity entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
        matrixStackIn.pushPose();
        matrixStackIn.scale(0.95F, 0.95F, 0.95F);
        transformer.accept(matrixStackIn);
        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
        matrixStackIn.popPose();
        if (entityIn.parachuted())
            ParachuteRenderer.renderParachute(parachute, matrixStackIn, bufferIn, packedLightIn);
    }

    private static final ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/prisoner_grey.png");
    @Override
    public ResourceLocation getTextureLocation(SidedSmartAIEntity entity)
    {
        return TEXTURE;
    }
}
