package pellucid.ava.entities.livings.renderers.guard;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.resources.ResourceLocation;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.entities.livings.AVAHostileEntity;

import java.util.function.Consumer;

public class GuardRenderer<E extends AVAHostileEntity> extends HumanoidMobRenderer<E, GuardModel<E>>
{
    private final Consumer<PoseStack> transformer;

    public GuardRenderer(EntityRendererProvider.Context context)
    {
        this(context, (stack) -> {});
    }

    public GuardRenderer(EntityRendererProvider.Context context, Consumer<PoseStack> transfomer)
    {
        //todo
        super(context, new GuardModel<>(context.bakeLayer(AVAModelLayers.HUMANOID)), 0.5F);
        this.transformer = transfomer;
    }

    @Override
    public void render(E entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
        matrixStackIn.pushPose();
        matrixStackIn.scale(0.9F, 0.9F, 0.9F);
        transformer.accept(matrixStackIn);
        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
        matrixStackIn.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(E entity)
    {
        return entity.getColour().texture;
    }
}
