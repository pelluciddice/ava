package pellucid.ava.entities.livings.renderers.guard;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.items.guns.AVAItemGun;

public class GuardModel<E extends LivingEntity> extends HumanoidModel<E>
{
    public GuardModel(ModelPart part)
    {
        super(part);
    }

    @Override
    public void prepareMobModel(E entityIn, float limbSwing, float limbSwingAmount, float partialTick)
    {
        super.prepareMobModel(entityIn, limbSwing, limbSwingAmount, partialTick);
        if (entityIn.getMainHandItem().getItem() instanceof AVAItemGun && !(entityIn instanceof SidedSmartAIEntity))
        {
            if (entityIn.getMainArm() == HumanoidArm.RIGHT)
                this.rightArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
            else
                this.leftArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
        }
    }
}
