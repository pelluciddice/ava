package pellucid.ava.entities.livings;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.RandomStrollGoal;
import net.minecraft.world.entity.ai.goal.RangedAttackGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.base.IAVARangedAttackMob;

public abstract class RangedGuardEntity extends AVAHostileEntity implements IAVARangedAttackMob
{
    public RangedGuardEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected void registerGoals()
    {
        goalSelector.addGoal(0, new RangedAttackGoal(this, 0.5F, getAttackInterval(), getRangeForAttack()));
        goalSelector.addGoal(1, new RandomStrollGoal(this, 0.35F, 5));
        targetSelector.addGoal(0, new AttackPlayerGoal(this, 2, false, false, (entity) -> entity instanceof Player && isAttackablePlayer((Player) entity), 70));
    }

    public int getRangeForAttack()
    {
        return 70;
    }

    public static AttributeSupplier.Builder registerAttributes()
    {
        return AVAHostileEntity.registerAttributes()
                .add(Attributes.ATTACK_DAMAGE, 5.0F);
    }

    protected int getAttackInterval()
    {
        return 1;
    }
}
