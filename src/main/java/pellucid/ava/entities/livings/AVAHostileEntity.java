package pellucid.ava.entities.livings;

import com.google.common.collect.ImmutableMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import pellucid.ava.AVA;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.functionalities.IDifficultyScaledHostile;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AVAHostileEntity extends AVABotEntity implements IEntityWithComplexSpawn, Enemy, IDifficultyScaledHostile
{
    private ColourTypes colour;
    private boolean isPrisoner;

    public AVAHostileEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
        colour = getDefaultColour();
        isPrisoner = false;
    }

    protected ColourTypes getDefaultColour()
    {
        return ColourTypes.randomGuardColourNotEmpty();
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        buffer.writeUtf(getColour().toString());
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        setColour(ColourTypes.valueOf(additionalData.readUtf(32767)));
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.putString("colour", getColour().name());
        DataTypes.BOOLEAN.write(compound, "prisoner", isPrisoner);
        compound.putString("weapon", BuiltInRegistries.ITEM.getKey(weapon).toString());
        DataTypes.INT.write(compound, "blind", blind);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        if (compound.contains("colour"))
            setColour(ColourTypes.valueOf(compound.getString("colour")));
        else
            setColour(getDefaultColour());
        isPrisoner = DataTypes.BOOLEAN.read(compound, "prisoner");
        weapon = BuiltInRegistries.ITEM.get(new ResourceLocation(compound.getString("weapon")));
        blind = DataTypes.INT.read(compound, "blind");
    }

    public void setColour(ColourTypes colour)
    {
        this.colour = colour;
    }

    public ColourTypes getColour()
    {
        return colour;
    }

    @Override
    protected void dropFromLootTable(DamageSource damageSourceIn, boolean attackedRecently)
    {
        super.dropFromLootTable(damageSourceIn, attackedRecently);
        getConstantDrops().forEach((item, count) -> {
            ItemStack stack = new ItemStack(item);
            stack.setCount(count);
            stack.grow(random.nextInt(3) - 1);
            if (!stack.isEmpty())
                spawnAtLocation(stack);
        });
    }

    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return ImmutableMap.of();
    }

    @Override
    protected void dropCustomDeathLoot(DamageSource source, int looting, boolean recentlyHitIn)
    {
        if (random.nextInt(10) == 0)
        {
            ItemStack stack = getMainHandItem();
            stack.setDamageValue(stack.getMaxDamage());
            spawnAtLocation(stack);
            setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
        }
    }

    public static AttributeSupplier.Builder registerAttributes()
    {
        return Monster.createMonsterAttributes()
                .add(Attributes.MAX_HEALTH, 20.0F)
                .add(Attributes.MOVEMENT_SPEED, 0.4D)
                .add(Attributes.ATTACK_KNOCKBACK, 0.0F)
                .add(Attributes.KNOCKBACK_RESISTANCE, 1.0F)
                .add(Attributes.FOLLOW_RANGE, 70.0F);
    }

    public enum ColourTypes
    {
        NONE("none"),
        BLUE("blue"),
        YELLOW("yellow"),
        RED("red"),

        GREY_PRISONER("grey", true),
        YELLOW_PRISONER("yellow", true),
        RED_PRISONER("red", true),
        BLACK_PRISONER("black", true);

        public final ResourceLocation texture;
        public final boolean isPrisoner;
        private static final Random RANDOM = new Random();

        ColourTypes(String colour)
        {
            this(colour, false);
        }

        ColourTypes(String colour, boolean isPrisoner)
        {
            this.texture = new ResourceLocation(AVA.MODID, "textures/entities/" + (isPrisoner ? "prisoner_" : "guard_") + colour + ".png");
            this.isPrisoner = isPrisoner;
        }

        public static ColourTypes randomGuardColourNotEmpty()
        {
            List<ColourTypes> values = Arrays.stream(values()).collect(Collectors.toList());
            values.removeIf((colour) -> colour.isPrisoner);
            return values.get(RANDOM.nextInt(values.size() - 1) + 1);
        }

        public static ColourTypes randomColourFrom(ColourTypes... types)
        {
            return types[RANDOM.nextInt(types.length)];
        }
    }

    public enum SpawningConditionChances implements SpawnPlacements.SpawnPredicate<AVAHostileEntity>
    {
        COMMON(50, 1, 5),
        UNCOMMON(20, 1, 4),
        NORMAL(10, 1, 3),
        RARE(5, 0, 2),
        EXTREMELY_RARE(3, 0, 1),
        BOSS(1, 0, 1);

        private final int weight;
        private final int min;
        private final int max;

        SpawningConditionChances(int weight, int min, int max)
        {
            this.weight = weight;
            this.min = min;
            this.max = max;
        }

        public int getWeight()
        {
            return weight;
        }

        public int getMin()
        {
            return min;
        }

        public int getMax()
        {
            return max;
        }

        @Override
        public boolean test(EntityType type, ServerLevelAccessor world, MobSpawnType spawnReason, BlockPos pos, RandomSource rand)
        {
//            return false;
            return world.getDifficulty() != Difficulty.PEACEFUL && world.getLevel().getGameTime() > 3600 * 20 && AVAServerConfig.SHOULD_GUARDS_SPAWN.get() && rand.nextInt(this == BOSS ? 100 : 10) == 0 && rand.nextInt(101) > 100 - AVAServerConfig.GUARDS_SPAWN_CHANCE.get();
        }
    }

    public static class AttackPlayerGoal extends NearestAttackableTargetGoal<Player>
    {
        private final double distance;
        private final Predicate<LivingEntity> canAttackTarget;
        public AttackPlayerGoal(Mob goalOwnerIn, int targetChanceIn, boolean checkSight, boolean nearbyOnlyIn, @Nullable Predicate<LivingEntity> targetPredicate, double distance)
        {
            super(goalOwnerIn, Player.class, targetChanceIn, checkSight, nearbyOnlyIn, targetPredicate);
            this.distance = distance;
            canAttackTarget = targetPredicate == null ? (entity) -> true : targetPredicate;
        }

        @Override
        public boolean canUse()
        {
            double distance = Double.MAX_VALUE;
            Player player = null;
            for(Player entity : mob.level().players())
            {
                if (targetConditions.test(targetMob, entity))
                {
                    double d = entity.position().distanceTo(mob.position());
                    if (d < distance && d <= this.distance)
                    {
                        distance = d;
                        player = entity;
                    }
                }
            }
            target = player;
            return target != null;
        }

        @Override
        public boolean canContinueToUse()
        {
            return super.canContinueToUse() && canAttackTarget.test(mob.getTarget());
        }
    }
}
