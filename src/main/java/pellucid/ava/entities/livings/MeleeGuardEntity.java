package pellucid.ava.entities.livings;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.ai.goal.RandomStrollGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.goals.AVAMeleeAttackGoal;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class MeleeGuardEntity extends AVAHostileEntity
{
    public MeleeGuardEntity(EntityType<? extends AVAHostileEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected void registerGoals()
    {
        goalSelector.addGoal(0, new AVAMeleeAttackGoal(this, null, getMovementSpeedFactor(), true, 0.075F, 15));
        goalSelector.addGoal(1, new MoveTowardsTargetGoal(this, getMovementSpeedFactor(), getFollowingDistance()));
        goalSelector.addGoal(2, new RandomStrollGoal(this, 0.25F, 30));
        targetSelector.addGoal(0, new AttackPlayerGoal(this, 0, false, false, (entity) -> entity instanceof Player && isAttackablePlayer((Player) entity), getFollowingDistance()));
    }

    @Override
    public boolean doHurtTarget(Entity entityIn)
    {
        AtomicBoolean attacked = new AtomicBoolean(false);
        if (entityIn instanceof Player)
            AVAWeaponUtil.penetrateArmour((Player) entityIn, damageSources().mobAttack(this), (float) getAttributeValue(Attributes.ATTACK_DAMAGE), 0.5F, () -> {
                attacked.set(super.doHurtTarget(entityIn));
                return attacked.get();
            });
        return attacked.get();
    }

    protected abstract float getMovementSpeedFactor();

    protected float getFollowingDistance()
    {
        return 100.0F;
    }

    public static AttributeSupplier.Builder registerAttributes()
    {
        return AVAHostileEntity.registerAttributes()
                .add(Attributes.MAX_HEALTH, 15.0F)
                .add(Attributes.ATTACK_DAMAGE, 8.5F)
                .add(Attributes.FOLLOW_RANGE, 100.0F);
    }
}
