package pellucid.ava.entities.livings.guns;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.SubmachineGuns;

public class ShotgunGuardEntity extends GunGuardEntity
{
    public ShotgunGuardEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public float getSpread(float spread)
    {
        return 3.25F;
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        ItemStack weapon = new ItemStack(SubmachineGuns.REMINGTON870.get());
        ((AVAItemGun) weapon.getItem()).forceReload(weapon);
        return weapon;
    }
}
