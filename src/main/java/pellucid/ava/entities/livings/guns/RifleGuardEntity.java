package pellucid.ava.entities.livings.guns;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.Rifles;

public class RifleGuardEntity extends GunGuardEntity
{
    public RifleGuardEntity(EntityType<? extends RifleGuardEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        ItemStack weapon = new ItemStack(Rifles.XM8.get());
        AVAItemGun gun = (AVAItemGun) weapon.getItem();
        gun.forceReload(weapon);
        return weapon;
    }
}
