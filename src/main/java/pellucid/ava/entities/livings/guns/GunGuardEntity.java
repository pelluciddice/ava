package pellucid.ava.entities.livings.guns;

import com.google.common.collect.ImmutableMap;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.goals.AIGunManager;
import pellucid.ava.entities.livings.RangedGuardEntity;
import pellucid.ava.items.guns.AVAItemGun;

public abstract class GunGuardEntity extends RangedGuardEntity
{
    public final AIGunManager manager = new AIGunManager(this);
    public GunGuardEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public void aiStep()
    {
        super.aiStep();
        manager.tick();
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.put("aiGunManager", manager.serializeNBT());
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        manager.deserializeNBT(compound.getCompound("aiGunManager"));
    }

    @Override
    public float getSpread(float spread)
    {
        return 2.75F;
    }

    @Override
    public void performRangedAttack(LivingEntity target, float distanceFactor)
    {
        manager.tryFire(target);
    }

//    private final ImmutableMap<Item, Integer> DROPS = ImmutableMap.of(((AVAItemGun) getWeapon().getItem()).getMagazineType(), 2);
    @Override
    protected ImmutableMap<Item, Integer> getConstantDrops()
    {
        return ImmutableMap.of(((AVAItemGun) weapon).getAmmoType(getMainHandItem()), 2);
    }
}
