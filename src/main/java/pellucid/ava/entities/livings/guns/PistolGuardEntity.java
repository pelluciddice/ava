package pellucid.ava.entities.livings.guns;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.Pistols;

public class PistolGuardEntity extends GunGuardEntity
{
    public PistolGuardEntity(EntityType<? extends PistolGuardEntity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public float getSpread(float spread)
    {
        return 2.25F;
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        ItemStack weapon = new ItemStack(Pistols.SW1911_COLT.get());
        AVAItemGun gun = (AVAItemGun) weapon.getItem();
        gun.forceReload(weapon);
        return weapon;
    }
}
