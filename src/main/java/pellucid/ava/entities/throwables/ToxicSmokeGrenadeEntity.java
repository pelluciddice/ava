package pellucid.ava.entities.throwables;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.items.throwables.SmokeGrenade;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

public class ToxicSmokeGrenadeEntity extends SmokeGrenadeEntity
{
    private int damageCD;
    public ToxicSmokeGrenadeEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public ToxicSmokeGrenadeEntity(EntityType<Entity> type, LivingEntity shooter, Level worldIn, double velocity, SmokeGrenade grenade)
    {
        super(type, shooter, worldIn, velocity, grenade);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (!level().isClientSide && fullyExpanded())
        {
            damageCD--;
            if (damageCD <= 0)
            {
                if (!level().isClientSide())
                    for (Entity entity : level().getEntities(this, getBoundingBox().inflate(3), (entity) -> entity instanceof LivingEntity && canAttack(entity)))
                        AVAWeaponUtil.attackEntityDependAllyDamage(entity, AVADamageSources.causeToxicGasDamage(level(), getShooter(), this), entity instanceof Player ? 2.0F : 5.0F, 1.0F);
                damageCD = 20;
            }
        }
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.INT.write(compound, "lastDamage", damageCD);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        damageCD = DataTypes.INT.read(compound, "lastDamage");
    }
}
