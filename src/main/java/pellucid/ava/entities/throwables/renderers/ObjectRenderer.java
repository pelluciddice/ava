package pellucid.ava.entities.throwables.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;

import java.util.function.Consumer;

public class ObjectRenderer<E extends Entity> extends EntityRenderer<E>
{
    protected final Model model;
    private final ResourceLocation texturePath;
    private final Consumer<PoseStack> modification;

    public ObjectRenderer(EntityRendererProvider.Context context, Model model, ResourceLocation texturePath)
    {
        this(context, model, texturePath, (stack) -> {});
    }

    public ObjectRenderer(EntityRendererProvider.Context context, Model model, ResourceLocation texturePath, Consumer<PoseStack> modification)
    {
        super(context);
        this.model = model;
        this.texturePath = texturePath;
        this.modification = modification;
    }

    @Override
    public void render(E entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
        matrixStackIn.pushPose();
        matrixStackIn.scale(0.5F, 0.5F, 0.5F);
        matrixStackIn.mulPose(Axis.YP.rotationDegrees(Mth.lerp(partialTicks, entityIn.yRotO, entityIn.getYRot()) - 180.0F));
        matrixStackIn.mulPose(Axis.XP.rotationDegrees(Mth.lerp(partialTicks, entityIn.xRotO, entityIn.getXRot())));
        modification.accept(matrixStackIn);
        model.renderToBuffer(matrixStackIn, bufferIn.getBuffer(model.renderType(getTextureLocation(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        matrixStackIn.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(E entity)
    {
        return texturePath;
    }
}
