package pellucid.ava.entities.throwables;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.base.BouncingEntity;
import pellucid.ava.entities.objects.plain_smoke.PlainSmokeEntity;
import pellucid.ava.items.throwables.SmokeGrenade;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.DataTypes;

public class SmokeGrenadeEntity extends BouncingEntity
{
    public SmokeGrenade grenade;

    public SmokeGrenadeEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public SmokeGrenadeEntity(EntityType<Entity> type, LivingEntity shooter, Level worldIn, double velocity, SmokeGrenade grenade)
    {
        super(type, shooter, worldIn, velocity, grenade.getRange(), AVASounds.GRENADE_HIT.get());
        this.grenade = grenade;
    }

    public SmokeGrenade getGrenade()
    {
        return grenade;
    }

    public boolean fullyExpanded()
    {
        return rangeTravelled > grenade.releaseSmokeDuration;
    }

    @Override
    public void tick()
    {
        if (grenade == null)
            remove(RemovalReason.DISCARDED);
        super.tick();
        if (rangeTravelled > 30 && rangeTravelled < grenade.releaseSmokeDuration)
        {
            if (this.rangeTravelled % 20 == 0)
                playSound(AVASounds.SMOKE_GRENADE_ACTIVE.get(), Mth.lerp((float) rangeTravelled / grenade.releaseSmokeDuration, 0.5F, 1.0F), 1.0F);
            if (!level().isClientSide && this.rangeTravelled % grenade.releaseSmokeInterval == 0)
                level().addFreshEntity(new PlainSmokeEntity(AVAEntities.PLAIN_SMOKE.get(), level(), grenade, position().add(new Vec3(random.nextFloat() - 0.5F, random.nextFloat() - 0.5F, random.nextFloat() - 0.5F).scale(2.0F))));
        }
        if (this.rangeTravelled >= this.range)
            remove(RemovalReason.DISCARDED);
    }

    @Override
    protected void move()
    {
        move(!landed);
    }

    @Override
    protected double getGravityVelocity()
    {
        return 0.041D;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.STRING.write(compound, "grenade", BuiltInRegistries.ITEM.getKey(grenade).toString());
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        grenade = (SmokeGrenade) BuiltInRegistries.ITEM.get(new ResourceLocation(DataTypes.STRING.read(compound, "grenade")));
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        super.writeSpawnData(buffer);
        DataTypes.STRING.write(buffer, BuiltInRegistries.ITEM.getKey(grenade).toString());
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        super.readSpawnData(additionalData);
        grenade = (SmokeGrenade) BuiltInRegistries.ITEM.get(new ResourceLocation(DataTypes.STRING.read(additionalData)));
    }
}
