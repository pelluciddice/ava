package pellucid.ava.entities.throwables;

import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.client.renderers.environment.EnvironmentObjectEffect;
import pellucid.ava.entities.base.BouncingEntity;
import pellucid.ava.entities.functionalities.IAwarableProjectile;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.QuadConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class HandGrenadeEntity extends BouncingEntity implements IAwarableProjectile
{
    protected boolean impact;
    protected int flash;
    public int damage;
    protected Item weapon = null;
    protected SoundEvent explosionSound;
    public double explosiveRadius;
    public double flashRadius;

    public HandGrenadeEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public HandGrenadeEntity(EntityType<Entity> type, LivingEntity shooter, Level worldIn, double velocity, Item weapon, boolean impact, int damage, int flash, int range, float radius, SoundEvent explosionSound)
    {
        this(type, shooter, worldIn, velocity, weapon, impact, damage, flash, range, radius, AVASounds.GRENADE_HIT.get(), explosionSound);
    }

    public HandGrenadeEntity(EntityType<Entity> type, LivingEntity shooter, Level worldIn, double velocity, Item weapon, boolean impact, int damage, int flash, int range, float radius, SoundEvent collideSound, SoundEvent explosionSound)
    {
        super(type, shooter, worldIn, velocity, range, collideSound);
        this.impact = impact;
        this.flash = flash;
        this.damage = damage;
        this.weapon = weapon;
        explosiveRadius = AVAWeaponUtil.getExplosiveRadiusForEntity(this);
        flashRadius = AVAWeaponUtil.getFlashRadiusForEntity(this);
        this.explosionSound = explosionSound;
    }

    private final List<Player> notifiedPlayers = new ArrayList<>();
    @Override
    public void tick()
    {
        super.tick();
        LivingEntity shooter = getShooter();
        if (damage > 0 && !level().isClientSide() && shooter != null)
        {
            AVAWeaponUtil.TeamSide team = AVAWeaponUtil.TeamSide.getSideFor(shooter);
            level().getEntitiesOfClass(Player.class, getBoundingBox().inflate(7.0F, 7.0F, 7.0F)).forEach((player) -> {
                if (!notifiedPlayers.contains(player) && shooter != player && (team == null || AVAWeaponUtil.TeamSide.getSideFor(player) != team) && AVAWeaponUtil.isInSight(player, this, true, true, true, true, true))
                {
                    if (AVACommonUtil.isFullEquipped(player))
                        AVAWeaponUtil.playAttenuableSoundToClientMoving(AVAWeaponUtil.eitherSound(player, AVASounds.ENEMY_GRENADE_EU.get(), AVASounds.ENEMY_GRENADE_NRF.get()), player);
                    notifiedPlayers.add(player);
                }
            });
        }
        if (this.rangeTravelled >= this.range)
            explode(false);
    }

    @Override
    protected void onImpact(HitResult result)
    {
        if (this.impact)
            this.explode(true);
        super.onImpact(result);
    }

    @Override
    public Item getIcon()
    {
        return getWeapon();
    }

    @Override
    public boolean isFromEnemy(Entity to)
    {
        return AVAWeaponUtil.isSameSide(to, getShooter());
    }

    @Override
    protected void move()
    {
        move(!landed);
    }

    protected void explode(boolean impact)
    {
        explode(
                (entity, distance) -> {},
                (serverWorld, x, y, z) -> {
                    for (int i = 0; i < 20; i++)
                    {
                        if (damage > 0)
                            serverWorld.sendParticles(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.5F);
                        if (flash > 0)
                            serverWorld.sendParticles(ParticleTypes.END_ROD, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.5F);
                    }
                }
        );
        if (flash > 0)
            flash((serverWorld, x, y, z) -> {
                for (int i = 0; i < 20; i++)
                    serverWorld.sendParticles(ParticleTypes.END_ROD, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.5F);
            });
    }

    protected void explode(BiConsumer<Entity, Double> extraAction, QuadConsumer<ServerLevel, Double, Double, Double> effect)
    {
        if (level().isClientSide() && (landed || impact))
            AVAWorldData.getInstance(level()).grenadeMarks.add(new EnvironmentObjectEffect(new BlockHitResult(position(), Direction.UP, blockPosition(), false), true));
        AVAWeaponUtil.createExplosionProjectile(this, getShooter(), weapon,
                extraAction,
                effect,
                explosionSound);
        if (isAlive())
            remove(RemovalReason.DISCARDED);
        if (level() instanceof ServerLevel && hasScreenShakeEffect())
        {
            level().getEntitiesOfClass(ServerPlayer.class, getBoundingBox().inflate(AVAClientUtil.CAMERA_VERTICAL_SHAKE_DISTANCE_MAX), AVAWeaponUtil::isValidEntity).forEach((e) -> {
                double d = position().distanceTo(e.position());
                if (d <= AVAClientUtil.CAMERA_VERTICAL_SHAKE_DISTANCE_MAX)
                    DataToClientMessage.screenShake(e, (float) Mth.lerp(1.0D - d / AVAClientUtil.CAMERA_VERTICAL_SHAKE_DISTANCE_MAX, AVAClientUtil.CAMERA_VERTICAL_SHAKE_WEAK, AVAClientUtil.CAMERA_VERTICAL_SHAKE_STRONG));
            });
        }
    }

    protected boolean hasScreenShakeEffect()
    {
        return damage > 0;
    }

    protected void flash(QuadConsumer<ServerLevel, Double, Double, Double> effect)
    {
        if (flash > 0)
            AVAWeaponUtil.createFlash(this, effect, explosionSound);
        if (isAlive())
            remove(RemovalReason.DISCARDED);
    }

    public Item getWeapon()
    {
        return weapon;
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        super.writeSpawnData(buffer);
        DataTypes.STRING.write(buffer, BuiltInRegistries.SOUND_EVENT.getKey(explosionSound).toString());
        DataTypes.ITEM.write(buffer, weapon == null ? Items.AIR : weapon);
        DataTypes.BOOLEAN.write(buffer, impact);
        DataTypes.DOUBLE.write(buffer, explosiveRadius);
        DataTypes.DOUBLE.write(buffer, flashRadius);
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        super.readSpawnData(additionalData);
        explosionSound = BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(DataTypes.STRING.read(additionalData)));
        weapon = DataTypes.ITEM.read(additionalData);
        impact = DataTypes.BOOLEAN.read(additionalData);
        explosiveRadius = DataTypes.DOUBLE.read(additionalData);
        flashRadius = DataTypes.DOUBLE.read(additionalData);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.BOOLEAN.write(compound, "impact", this.impact);
        DataTypes.INT.write(compound, "flash", this.flash);
        DataTypes.INT.write(compound, "damage", this.damage);
        compound.putString("explosionsound", BuiltInRegistries.SOUND_EVENT.getKey(explosionSound).toString());
        if (this.weapon != null)
            compound.putString("weapon", BuiltInRegistries.ITEM.getKey(weapon).toString());
        DataTypes.INT.write(compound, "rangetravelled", rangeTravelled);
        DataTypes.DOUBLE.write(compound, "er", explosiveRadius);
        DataTypes.DOUBLE.write(compound, "fr", flashRadius);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        impact = DataTypes.BOOLEAN.read(compound, "impact");
        flash = DataTypes.INT.read(compound, "flash");
        damage = DataTypes.INT.read(compound, "damage");
        explosionSound = BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(compound.getString("explosionsound")));
        if (compound.contains("weapon"))
        {
            Item item = BuiltInRegistries.ITEM.get(new ResourceLocation(compound.getString("weapon")));
            weapon = item == Items.AIR ? null : item;
        }
        explosiveRadius = DataTypes.DOUBLE.read(compound, "er");
        flashRadius = DataTypes.DOUBLE.read(compound, "fr");
    }
}
