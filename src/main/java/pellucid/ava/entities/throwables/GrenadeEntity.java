package pellucid.ava.entities.throwables;

import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.sounds.AVASounds;

public class GrenadeEntity extends HandGrenadeEntity
{
    public GrenadeEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public GrenadeEntity(LivingEntity shooter, Level worldIn, Item weapon)
    {
        super(AVAEntities.GRENADE.get(), shooter, worldIn, 1.15F, weapon, false, 160 / 5, 0, 70, 5, AVASounds.GENERIC_GRENADE_EXPLODE.get());
    }

    @Override
    protected void explode(boolean impact)
    {
        explode(
                (entity, distance) -> {},
                (serverWorld, x, y, z) -> {
                    for (int i = 0; i < 25; i++)
                    {
                        serverWorld.sendParticles(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 1, 0.0F, 0.0F, 0.0F, 0.5F);
                        serverWorld.sendParticles(ParticleTypes.FLAME, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 3, 0.0F, 0.0F, 0.0F, 0.5F);
                    }
                }
        );
    }

    @Override
    protected Vec3 getBouncingVelocityMultiplier(Direction direction, float factor)
    {
        return new Vec3(0.2F, 0.4F, 0.2F);
    }

    @Override
    protected double getGravityVelocity()
    {
        return 0.0235D;
    }
}
