package pellucid.ava.entities.base;

import net.minecraft.world.entity.monster.RangedAttackMob;

public interface IAVARangedAttackMob extends RangedAttackMob
{
    default float getSpread(float spread)
    {
        return 1.0F;
    }
}
