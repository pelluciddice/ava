package pellucid.ava.entities.base;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.registries.DeferredItem;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@OnlyIn(Dist.CLIENT)
public class EntityObjectRenderer<E extends Entity> extends EntityRenderer<E>
{
    private static final Map<Item, ItemStack> ITEM_STACKS = new HashMap<>();
    private final Function<E, Item> item;

    public EntityObjectRenderer(EntityRendererProvider.Context context, DeferredItem<Item> item)
    {
        super(context);
        this.item = (e) -> item.get();
    }

    public EntityObjectRenderer(EntityRendererProvider.Context context, Function<E, Item> item)
    {
        super(context);
        this.item = item;
    }

    @Override
    public void render(E entity, float entityYaw, float partialTicks, PoseStack stack, MultiBufferSource bufferIn, int packedLightIn)
    {
        stack.pushPose();
        float scale = 0.35F;
        stack.scale(scale, scale, scale);
        stack.mulPose(Axis.YP.rotationDegrees(Mth.lerp(partialTicks, entity.yRotO, entity.getYRot())));
        stack.mulPose(Axis.XP.rotationDegrees(Mth.lerp(partialTicks, entity.xRotO, entity.getXRot())));
        ItemStack model = ITEM_STACKS.computeIfAbsent(item.apply(entity), ItemStack::new);
        Minecraft.getInstance().getItemRenderer().renderStatic(model, ItemDisplayContext.HEAD, packedLightIn, OverlayTexture.NO_OVERLAY, stack, bufferIn, entity.level(), entity.getId());
        stack.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(E entity)
    {
        return null;
    }
}
