package pellucid.ava.entities.base;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ProjectileUtil;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.client.renderers.environment.GrenadeTrailEffect;
import pellucid.ava.entities.functionalities.IAwarableProjectile;
import pellucid.ava.entities.functionalities.IOwner;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.UUID;

public abstract class ProjectileEntity extends AVAEntity implements IOwner, IEntityWithComplexSpawn
{
    protected int range;
    protected UUID shooterUUID;
    protected int shooterID;
    protected double initialVelocity;

    public ProjectileEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public ProjectileEntity(EntityType<Entity> type, LivingEntity shooter, Level worldIn, double velocity, int range)
    {
        this(type, worldIn);
        this.range = range;
        this.shooterUUID = shooter.getUUID();
        this.shooterID = shooter.getId();
        velocity *= 0.825F;
        this.initialVelocity = velocity;
        float x = -Mth.sin(shooter.getYRot() * ((float)Math.PI / 180F)) * Mth.cos(shooter.getXRot() * ((float)Math.PI / 180F));
        float y = -Mth.sin((shooter.getXRot()) * ((float)Math.PI / 180F));
        float z = Mth.cos(shooter.getYRot() * ((float)Math.PI / 180F)) * Mth.cos(shooter.getXRot() * ((float)Math.PI / 180F));
        Vec3 vec3d = (new Vec3(x, y, z)).normalize().scale(velocity);
        this.setDeltaMovement(vec3d);
        this.setDirection();
        setPos(shooter.getX(), shooter.getEyeY(), shooter.getZ());
//        setPosition(shooter.getPosX(), shooter.getPosYEye() - (double)0.06F, shooter.getPosZ());
    }

    @Override
    public boolean canAttack(Entity entity)
    {
        if (fromMob && getShooter() != null && entity instanceof LivingEntity && !(entity instanceof Player))
        {
            AVAWeaponUtil.TeamSide side = AVAWeaponUtil.TeamSide.getSideFor(getShooter());
            if (side != null && side.isSameSide((LivingEntity) entity))
                return false;
        }
        return super.canAttack(entity);
    }

    @Override
    public boolean shouldRenderAtSqrDistance(double distance)
    {
        return distance <= 2048.0D;
    }

    @Override
    public void fromMob(LivingEntity target)
    {
        super.fromMob(target);
        double x = target.getX() - this.getX();
        double y = target.getY() + target.getBbHeight() / 2.0F - this.getEyeY();
        double z = target.getZ() - this.getZ();
        Vec3 vector3d = (new Vec3(x, y, z)).normalize().scale(initialVelocity);
        this.setDeltaMovement(vector3d);
        setDirection();
    }

    protected void setDirection()
    {
        Vec3 vec3d = getDeltaMovement();
        float f = Mth.sqrt((float) vec3d.horizontalDistanceSqr());
        this.setYRot((float)(Mth.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI)));
        this.setXRot((float)(Mth.atan2(vec3d.y, f) * (double)(180F / (float)Math.PI)));
        this.xRotO = this.getYRot();
        this.yRotO = this.getXRot();
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        DataTypes.INT.write(buffer, this.range);
        DataTypes.INT.write(buffer, getShooter() == null ? -1 : getShooter().getId());
        writePlainSpawnData(buffer);
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        range = additionalData.readInt();
        shooterID = additionalData.readInt();
        readPlainSpawnData(additionalData);
    }

    @Override
    public void tick()
    {
        setDirection();
        AABB axisalignedbb = this.getBoundingBox().expandTowards(this.getDeltaMovement());
        Vec3 from = this.position();
        Vec3 to = this.position().add(new Vec3(this.getDeltaMovement().x(), this.getDeltaMovement().y(), this.getDeltaMovement().z()));
        EntityHitResult entityResult = ProjectileUtil.getEntityHitResult(level(), this, from, to, axisalignedbb.inflate(1.0F), (entity) -> entity != getShooter() && !entity.isSpectator() && entity.isPickable() && canAttack(entity));
        BlockHitResult blockResult = AVAWeaponUtil.rayTraceBlocks(new ClipContext(from, to, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, this), level(), null, false);
        if (blockResult.getType() != HitResult.Type.MISS && entityResult != null)
            onImpact((from.distanceTo(blockResult.getLocation()) > from.distanceTo(entityResult.getLocation())) ? entityResult : blockResult);
        else if (entityResult != null)
            onImpact(entityResult);
        else if (blockResult.getType() != HitResult.Type.MISS)
            onImpact(blockResult);
        move();
        super.tick();
        if (level().isClientSide && (!(this instanceof IAwarableProjectile) || ((IAwarableProjectile) this).hasTrail()))
            AVAWorldData.getInstance(level()).grenadeTrails.computeIfAbsent(this, (e) -> new ArrayList<>()).add(new GrenadeTrailEffect(position()));
    }

    protected void move()
    {
        move(false);
    }

    protected void move(boolean rotates)
    {
        Vec3 vector3d = this.getDeltaMovement();
        double x = this.getX() + vector3d.x;
        double y = this.getY() + vector3d.y;
        double z = this.getZ() + vector3d.z;
        if (this.isInWater())
            for(int i = 0; i < 5; i++)
                this.level().addParticle(ParticleTypes.BUBBLE, x - vector3d.x * 0.25D, y - vector3d.y * 0.25D, z - vector3d.z * 0.25D, vector3d.x, vector3d.y, vector3d.z);
        if (!isNoGravity())
            this.setDeltaMovement(vector3d.x, vector3d.y - getGravityVelocity(), vector3d.z);
        this.setPos(x, y, z);
        if (rotates)
        {
            this.setXRot(getXRot() + 15.0F);
            this.setYRot(getYRot() + 15.0F);
        }
    }

    protected double getGravityVelocity()
    {
        return 0.0435D;
    }

    protected void onImpact(HitResult result)
    {

    }

    @Nullable
    @Override
    public LivingEntity getShooter()
    {
        Entity entity = null;
        if (this.level() instanceof ServerLevel)
        {
            if (this.shooterUUID != null)
                entity = ((ServerLevel) this.level()).getEntity(shooterUUID);
        }
        else
            entity = this.level().getEntity(this.shooterID);
        return entity == null ? null : (LivingEntity) entity;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.INT.write(compound, "range", this.range);
        DataTypes.DOUBLE.write(compound, "velocityi", this.initialVelocity);
        if (this.shooterUUID != null)
            DataTypes.UUID.write(compound, "ownerUUID", this.shooterUUID);
        DataTypes.INT.write(compound, "ownerID", this.shooterID);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        this.range = DataTypes.INT.read(compound, "range");
        this.initialVelocity = DataTypes.DOUBLE.read(compound, "velocityi");
        if (compound.contains("ownerUUID"))
            this.shooterUUID = DataTypes.UUID.read(compound, "ownerUUID");
        this.shooterID = DataTypes.INT.read(compound, "ownerID");
    }

    public int getRange()
    {
        return range;
    }
}
