package pellucid.ava.entities.base;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.network.PacketDistributor;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.robots.AVARobotEntity;
import pellucid.ava.packets.PlaySoundToClientMessage;
import pellucid.ava.util.DataTypes;

public abstract class AVAEntity extends Entity
{
    public int rangeTravelled = 0;
    public boolean fromMob;

    public AVAEntity(EntityType<?> entityTypeIn, Level worldIn)
    {
        super(entityTypeIn, worldIn);
    }

    @Override
    public void recreateFromPacket(ClientboundAddEntityPacket packet)
    {
        super.recreateFromPacket(packet);
        setDeltaMovement(packet.getXa(), packet.getYa(), packet.getZa());
    }

    @Override
    public void tick()
    {
        super.tick();
        rangeTravelled++;
    }

    public void writePlainSpawnData(RegistryFriendlyByteBuf buffer)
    {
        DataTypes.INT.write(buffer, rangeTravelled);
        DataTypes.BOOLEAN.write(buffer, fromMob);
        DataTypes.FLOAT.write(buffer, (float) getDeltaMovement().x);
        DataTypes.FLOAT.write(buffer, (float) getDeltaMovement().y);
        DataTypes.FLOAT.write(buffer, (float) getDeltaMovement().z);
    }

    public void readPlainSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        rangeTravelled = DataTypes.INT.read(additionalData);
        fromMob = DataTypes.BOOLEAN.read(additionalData);
        setDeltaMovement(DataTypes.FLOAT.read(additionalData), DataTypes.FLOAT.read(additionalData), DataTypes.FLOAT.read(additionalData));
    }

    protected void playSound(SoundEvent sound, SoundSource category, double x, double y, double z, float volume, float pitch)
    {
        if (level() instanceof ServerLevel world)
            PacketDistributor.sendToPlayersInDimension(world, new PlaySoundToClientMessage(BuiltInRegistries.SOUND_EVENT.getKey(sound).toString(), category, x, y, z, volume, pitch));
    }

    @Override
    public void playSound(SoundEvent soundIn, float volume, float pitch)
    {
        if (level() instanceof ServerLevel)
            playSound(soundIn, SoundSource.PLAYERS, getX(), getY(), getZ(), 1.0F, 1.0F);
    }

    public void fromMob(LivingEntity target)
    {
        this.fromMob = true;
    }

    public boolean canAttack(Entity entity)
    {
        if (fromMob)
            return !(entity instanceof AVAHostileEntity || entity instanceof AVARobotEntity);
        return true;
    }

    @Override
    protected void defineSynchedData(SynchedEntityData.Builder builder)
    {

    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        DataTypes.INT.write(compound, "rangetravelled", rangeTravelled);
        DataTypes.BOOLEAN.write(compound, "frommob", fromMob);
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        rangeTravelled = DataTypes.INT.read(compound, "rangetravelled");
        fromMob = DataTypes.BOOLEAN.read(compound, "frommob");
    }
}
