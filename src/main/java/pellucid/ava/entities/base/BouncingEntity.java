package pellucid.ava.entities.base;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

public abstract class BouncingEntity extends ProjectileEntity
{
    protected boolean landed;
    protected SoundEvent collideSound;

    public BouncingEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public BouncingEntity(EntityType<Entity> type, LivingEntity shooter, Level worldIn, double velocity, int range, SoundEvent collideSound)
    {
        super(type, shooter, worldIn, velocity, range);
        this.collideSound = collideSound;
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        super.writeSpawnData(buffer);
        DataTypes.BOOLEAN.write(buffer, landed);
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        super.readSpawnData(additionalData);
        landed = DataTypes.BOOLEAN.read(additionalData);
    }

    @Override
    protected void setDirection()
    {
    }

    @Override
    protected void onImpact(HitResult result)
    {
        if (result.getType() == HitResult.Type.BLOCK)
        {
            BlockHitResult blockResult = (BlockHitResult) result;
            BlockPos pos = blockResult.getBlockPos();
            if (!landed && !level().isClientSide())
            {
                level().playSound(null, this.getX(), this.getY(), this.getZ(), collideSound, SoundSource.PLAYERS, 2.0F, 1.0F);
                AVAWeaponUtil.checkForSpecialBlockHit(this, (BlockHitResult) result);
            }
            if (AVAWeaponUtil.destructRepairable(level(), pos, getShooter() instanceof Player ? (Player) getShooter() : null) || AVAWeaponUtil.destroyGlassOnHit(level(), ((BlockHitResult) result).getBlockPos()))
                return;
            Direction direction = blockResult.getDirection();
            Direction.Axis a = direction.getAxis();
            Vec3 m = this.getDeltaMovement();
            this.setDeltaMovement(new Vec3(m.x() * (a == Direction.Axis.X ? -1 : 1), m.y() * (a == Direction.Axis.Y ? -1 : 1), m.z() * (a == Direction.Axis.Z ? -1 : 1)).multiply(getBouncingVelocityMultiplier(direction, 0.3F)));
        }
        Vec3 m = this.getDeltaMovement();
        if (m.y() <= 0.1F)
        {
            this.setDeltaMovement(m.x(), 0.0D, m.z());
            this.landed = true;
        }
        else
            this.landed = false;
    }

    protected Vec3 getBouncingVelocityMultiplier(Direction direction, float factor)
    {
        return new Vec3(factor, factor * 1.25F, factor);
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.BOOLEAN.write(compound, "landed", landed);
        compound.putString("sound", BuiltInRegistries.SOUND_EVENT.getKey(collideSound).toString());
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        landed = DataTypes.BOOLEAN.read(compound, "landed");
        collideSound = BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(compound.getString("sound")));
    }
    public boolean isLanded()
    {
        return this.landed;
    }
}
