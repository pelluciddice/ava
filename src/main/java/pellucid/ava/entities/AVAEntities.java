package pellucid.ava.entities;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EntityType.Builder;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacementTypes;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.livings.MeleeGuardEntity;
import pellucid.ava.entities.livings.RangedGuardEntity;
import pellucid.ava.entities.livings.guns.PistolGuardEntity;
import pellucid.ava.entities.livings.guns.RifleGuardEntity;
import pellucid.ava.entities.livings.guns.ShotgunGuardEntity;
import pellucid.ava.entities.livings.guns.ShotgunPrisonerEntity;
import pellucid.ava.entities.livings.melees.BlueMeleeGuardEntity;
import pellucid.ava.entities.livings.melees.GreyMeleePrisonerEntity;
import pellucid.ava.entities.livings.melees.RedMeleePrisonerEntity;
import pellucid.ava.entities.livings.melees.YellowMeleePrisonerEntity;
import pellucid.ava.entities.livings.projectiles.GrenadeLauncherGuardEntity;
import pellucid.ava.entities.livings.projectiles.ToxicSmokeGuardEntity;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.objects.item.GunItemEntity;
import pellucid.ava.entities.objects.kits.AmmoKitEntity;
import pellucid.ava.entities.objects.kits.FirstAidKitEntity;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.objects.plain_smoke.PlainSmokeEntity;
import pellucid.ava.entities.robots.AVARobotEntity;
import pellucid.ava.entities.robots.DarkBlueRobotEntity;
import pellucid.ava.entities.robots.YellowRobotEntity;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.entities.scanhits.MeleeAttackRaytraceEntity;
import pellucid.ava.entities.shootables.M202RocketEntity;
import pellucid.ava.entities.smart.EUSmartEntity;
import pellucid.ava.entities.smart.NRFSmartEntity;
import pellucid.ava.entities.smart.RoledSidedSmartAIEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.entities.throwables.GrenadeEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.entities.throwables.ToxicSmokeGrenadeEntity;

import java.util.HashSet;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static pellucid.ava.entities.livings.AVAHostileEntity.SpawningConditionChances.*;

public class AVAEntities
{
    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(BuiltInRegistries.ENTITY_TYPE, AVA.MODID);
    private static final float PROJECTILE_SIZE = 0.175F;
    public static final Supplier<EntityType<Entity>>
            BULLET = entity("bullet", BulletEntity::new, MobCategory.MISC, 0.1F, 0.1F, -1, false),
            MELEE_RAYTRACING = entity("melee_raytracing", MeleeAttackRaytraceEntity::new, MobCategory.MISC, 0.1F, 0.1F, 20),
            M67 = entity("m67", HandGrenadeEntity::new, MobCategory.MISC, PROJECTILE_SIZE, PROJECTILE_SIZE, 20),
            MK3A2 = entity("mk3a2", HandGrenadeEntity::new, MobCategory.MISC, PROJECTILE_SIZE, PROJECTILE_SIZE, 20),
            M116A1 = entity("m116a1", HandGrenadeEntity::new, MobCategory.MISC, PROJECTILE_SIZE, PROJECTILE_SIZE, 20),
            M18 = entity("m18", SmokeGrenadeEntity::new, MobCategory.MISC, PROJECTILE_SIZE, PROJECTILE_SIZE, 20),
            M18_TOXIC = entity("m18_toxic", ToxicSmokeGrenadeEntity::new, MobCategory.MISC, PROJECTILE_SIZE, PROJECTILE_SIZE, 20),
            AMMO_KIT = entity("ammo_kit", AmmoKitEntity::new, MobCategory.MISC, 0.5F, 0.5F, 20),
            FIRST_AID_KIT = entity("first_aid_kit", FirstAidKitEntity::new, MobCategory.MISC, 0.5F, 0.5F, 20),
            ROCKET = entity("rocket", M202RocketEntity::new, MobCategory.MISC, 0.3F, 0.3F, 20),
            GRENADE = entity("grenade", GrenadeEntity::new, MobCategory.MISC, PROJECTILE_SIZE, PROJECTILE_SIZE, 20),
            C4 = entity("c4", C4Entity::new, MobCategory.MISC, 0.4F, 0.3F),
            PLAIN_SMOKE = entity("plain_smoke", PlainSmokeEntity::new, MobCategory.MISC, 0.1F, 0.1F, Integer.MAX_VALUE, false);

    public static final Supplier<EntityType<GunItemEntity>>
            GUN_ITEM = entity("gun_item", GunItemEntity::new, MobCategory.MISC, 0.4F, 0.3F);

    public static final Supplier<EntityType<LeopardEntity>>
            LEOPARD_DESERT = entity("leopard_desert", LeopardEntity::new, MobCategory.MISC, (builder) -> builder.sized(4F, 4F).fireImmune().canSpawnFarFromPlayer().clientTrackingRange(10).updateInterval(1)),
            LEOPARD_FOREST = entity("leopard_forest", LeopardEntity::new, MobCategory.MISC, (builder) -> builder.sized(4F, 4F).fireImmune().canSpawnFarFromPlayer().clientTrackingRange(10).updateInterval(1)),
            LEOPARD_SNOW = entity("leopard_snow", LeopardEntity::new, MobCategory.MISC, (builder) -> builder.sized(4F, 4F).fireImmune().canSpawnFarFromPlayer().clientTrackingRange(10).updateInterval(1));

    public static final Supplier<EntityType<BlueMeleeGuardEntity>>
            BLUE_MELEE_GUARD = entity("blue_melee_guard", BlueMeleeGuardEntity::new, MobCategory.MONSTER, 0.6F, 1.8F);

    public static final Supplier<EntityType<ShotgunGuardEntity>>
            SHOTGUN_GUARD = entity("shotgun_guard", ShotgunGuardEntity::new, MobCategory.MONSTER, 0.6F, 1.8F);

    public static final Supplier<EntityType<RifleGuardEntity>>
            RIFLE_GUARD = entity("rifle_guard", RifleGuardEntity::new, MobCategory.MONSTER, 0.6F, 1.8F);

    public static final Supplier<EntityType<GrenadeLauncherGuardEntity>>
            GRENADE_LAUNCHER_GUARD = entity("grenade_launcher_guard", GrenadeLauncherGuardEntity::new, MobCategory.MONSTER, 0.6F, 1.8F);

    public static final Supplier<EntityType<PistolGuardEntity>>
            PISTOL_GUARD = entity("pistol_guard", PistolGuardEntity::new, MobCategory.MONSTER, 0.6F, 1.8F);

    public static final Supplier<EntityType<ToxicSmokeGuardEntity>>
            TOXIC_SMOKE_GUARD = entity("toxic_smoke_guard", ToxicSmokeGuardEntity::new, MobCategory.MONSTER, 0.6F, 1.8F);

    public static final Supplier<EntityType<GreyMeleePrisonerEntity>>
            GREY_MELEE_PRISONER = entity("grey_prisoner", GreyMeleePrisonerEntity::new, MobCategory.MONSTER, 0.55F, 1.95F);

    public static final Supplier<EntityType<YellowMeleePrisonerEntity>>
            YELLOW_MELEE_PRISONER = entity("yellow_prisoner", YellowMeleePrisonerEntity::new, MobCategory.MONSTER, 0.55F, 1.95F);

    public static final Supplier<EntityType<RedMeleePrisonerEntity>>
            RED_MELEE_PRISONER = entity("red_prisoner", RedMeleePrisonerEntity::new, MobCategory.MONSTER, 0.65F, 2.225F);

    public static final Supplier<EntityType<ShotgunPrisonerEntity>>
            SHOTGUN_PRISONER = entity("shotgun_prisoner", ShotgunPrisonerEntity::new, MobCategory.MONSTER, 0.65F, 2.35F);

    public static final Supplier<EntityType<EUSmartEntity>>
            EU_SOLDIER = entity("eu_soldier", EUSmartEntity::new, MobCategory.CREATURE, 0.6F, 1.8F, 2);

    public static final Supplier<EntityType<NRFSmartEntity>>
            NRF_SOLDIER = entity("nrf_soldier", NRFSmartEntity::new, MobCategory.CREATURE, 0.6F, 1.8F, 2);

    public static final Supplier<EntityType<AVARobotEntity>>
            BLUE_ROBOT = entity("blue_robot", AVARobotEntity::new, MobCategory.CREATURE, 0.4F, 1.6F),
            YELLOW_ROBOT = entity("yellow_robot", YellowRobotEntity::new, MobCategory.CREATURE, 0.65F, 2.0F),
            DARK_BLUE_ROBOT = entity("dark_blue_robot", DarkBlueRobotEntity::new, MobCategory.CREATURE, 1.25F, 2.6F);

    public static final Supplier<EntityType<RoledSidedSmartAIEntity>>
            ROLED_SOLDIER = entity("roled_soldier", RoledSidedSmartAIEntity::new, MobCategory.CREATURE, 0.6F, 1.8F, 2);

    private static <T extends Entity> Supplier<EntityType<T>> entity(String name, EntityType.EntityFactory<T> constructor, MobCategory category, float width, float height)
    {
        return entity(name, constructor, category, width, height, -1);
    }

    private static <T extends Entity> Supplier<EntityType<T>> entity(String name, EntityType.EntityFactory<T> constructor, MobCategory category, float width, float height, int updateInterval)
    {
        return entity(name, constructor, category, width, height, updateInterval, true);
    }

    private static <T extends Entity> Supplier<EntityType<T>> entity(String name, EntityType.EntityFactory<T> constructor, MobCategory category, float width, float height, int updateInterval, boolean velocityUpdate)
    {
        return entity(name, constructor, category, (builder) -> {
            builder.sized(width, height);
            if (updateInterval != -1)
                builder.updateInterval(updateInterval);
            builder.setShouldReceiveVelocityUpdates(velocityUpdate);
        });
    }

    private static <T extends Entity> Supplier<EntityType<T>> entity(String name, EntityType.EntityFactory<T> constructor, MobCategory category, Consumer<Builder<T>> build)
    {
        return ENTITIES.register(name, () -> {
            Builder<T> builder = Builder.of(constructor, category);
            build.accept(builder);
            return builder.build(name);
        });
    }

    public static void setEntityAttributes(EntityAttributeCreationEvent event)
    {
        event.put(BLUE_MELEE_GUARD.get(), MeleeGuardEntity.registerAttributes().build());
        event.put(SHOTGUN_GUARD.get(), RangedGuardEntity.registerAttributes().build());
        event.put(RIFLE_GUARD.get(), RangedGuardEntity.registerAttributes().build());
        event.put(GRENADE_LAUNCHER_GUARD.get(), RangedGuardEntity.registerAttributes().build());
        event.put(PISTOL_GUARD.get(), RangedGuardEntity.registerAttributes().build());
        event.put(TOXIC_SMOKE_GUARD.get(), RangedGuardEntity.registerAttributes().build());

        event.put(GREY_MELEE_PRISONER.get(), MeleeGuardEntity.registerAttributes().build());
        event.put(YELLOW_MELEE_PRISONER.get(), MeleeGuardEntity.registerAttributes().build());
        event.put(RED_MELEE_PRISONER.get(), RedMeleePrisonerEntity.registerAttributes().build());
        event.put(SHOTGUN_PRISONER.get(), ShotgunPrisonerEntity.registerAttributes().build());

        event.put(EU_SOLDIER.get(), SidedSmartAIEntity.registerAttributes().build());
        event.put(NRF_SOLDIER.get(), SidedSmartAIEntity.registerAttributes().build());
        event.put(ROLED_SOLDIER.get(), SidedSmartAIEntity.registerAttributes().build());

        event.put(BLUE_ROBOT.get(), AVARobotEntity.registerAttributes(14.0F, 6.0F).build());
        event.put(YELLOW_ROBOT.get(), AVARobotEntity.registerAttributes(24.0F, 8.0F).build());
        event.put(DARK_BLUE_ROBOT.get(), AVARobotEntity.registerAttributes(44.0F, 12.0F).build());

        event.put(LEOPARD_DESERT.get(), LeopardEntity.registerAttributes().build());
        event.put(LEOPARD_FOREST.get(), LeopardEntity.registerAttributes().build());
        event.put(LEOPARD_SNOW.get(), LeopardEntity.registerAttributes().build());
    }

    public static void addSpawns(SpawnPlacementRegisterEvent event)
    {
        registerSpawnPlacement(event, BLUE_MELEE_GUARD.get(), COMMON);
        registerSpawnPlacement(event, RIFLE_GUARD.get(), UNCOMMON);
        registerSpawnPlacement(event, GRENADE_LAUNCHER_GUARD.get(), RARE);
        registerSpawnPlacement(event, PISTOL_GUARD.get(), NORMAL);
        registerSpawnPlacement(event, TOXIC_SMOKE_GUARD.get(), RARE);
    }

    public static HashSet<EntityType<?>> ALL_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> UNCOMMON_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> COMMON_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> NORMAL_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> RARE_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> EXTREMELY_RARE_HOSTILES = new HashSet<>();
    public static HashSet<EntityType<?>> BOSS_HOSTILES = new HashSet<>();

    private static void registerSpawnPlacement(SpawnPlacementRegisterEvent event, EntityType<? extends AVAHostileEntity> type, AVAHostileEntity.SpawningConditionChances chance)
    {
        switch (chance)
        {
            case UNCOMMON:
                UNCOMMON_HOSTILES.add(type);
                break;
            case COMMON:
                COMMON_HOSTILES.add(type);
                break;
            case NORMAL:
                NORMAL_HOSTILES.add(type);
                break;
            case RARE:
                RARE_HOSTILES.add(type);
                break;
            case EXTREMELY_RARE:
                EXTREMELY_RARE_HOSTILES.add(type);
                break;
            case BOSS:
                BOSS_HOSTILES.add(type);
                break;
        }
        ALL_HOSTILES.add(type);
        event.register(type, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING, chance::test, SpawnPlacementRegisterEvent.Operation.AND);
    }

    public static AVAHostileEntity.SpawningConditionChances getRarityFor(EntityType<?> type)
    {
        if (COMMON_HOSTILES.contains(type))
            return COMMON;
        else if (UNCOMMON_HOSTILES.contains(type))
            return UNCOMMON;
        else if (NORMAL_HOSTILES.contains(type))
            return NORMAL;
        else if (RARE_HOSTILES.contains(type))
            return RARE;
        else if (EXTREMELY_RARE_HOSTILES.contains(type))
            return EXTREMELY_RARE;
//        else if (BOSS_HOSTILES.contains(type))
        else
            return BOSS;
    }
}
