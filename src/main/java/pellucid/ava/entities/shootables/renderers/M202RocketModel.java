package pellucid.ava.entities.shootables.renderers;// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.Entity;

public class M202RocketModel<T extends Entity> extends EntityModel<T>
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	private final ModelPart bone;

	public M202RocketModel(ModelPart root) {
		this.bone = root.getChild("bone");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition bone = partdefinition.addOrReplaceChild("bone", CubeListBuilder.create().texOffs(6, 11).addBox(-1.4373F, -6.0528F, -46.0591F, 3.0F, 16.0F, 42.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-8.0286F, 0.5385F, -46.0591F, 16.0F, 3.0F, 42.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.6362F, -5.0528F, -55.0591F, 3.0F, 14.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-7.0286F, 0.3396F, -55.0591F, 14.0F, 3.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-0.8351F, -4.0528F, 4.9409F, 2.0F, 12.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-6.0286F, 1.1407F, 4.9409F, 12.0F, 2.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-13.5286F, 0.9526F, 31.7742F, 27.0F, 1.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.0231F, -11.5528F, 31.7742F, 1.0F, 27.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0286F, -0.7472F, 11.2258F));

		PartDefinition hexadecagon_r1 = bone.addOrReplaceChild("hexadecagon_r1", CubeListBuilder.create().texOffs(0, 0).addBox(-0.9946F, -13.5F, -5.0F, 1.0F, 27.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0286F, 1.9472F, 36.7742F, 0.0F, 0.0F, -0.7854F));

		PartDefinition hexadecagon_r2 = bone.addOrReplaceChild("hexadecagon_r2", CubeListBuilder.create().texOffs(0, 0).addBox(-0.9946F, -5.0F, -5.0F, 1.0F, 10.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-5.0F, -0.9946F, -5.0F, 10.0F, 1.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0286F, 1.9472F, 36.7742F, 0.0F, 0.0F, -0.3927F));

		PartDefinition hexadecagon_r3 = bone.addOrReplaceChild("hexadecagon_r3", CubeListBuilder.create().texOffs(0, 0).addBox(-0.9946F, -5.0F, -5.0F, 1.0F, 10.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0286F, 1.9472F, 36.7742F, 0.0F, 0.0F, 0.3927F));

		PartDefinition hexadecagon_r4 = bone.addOrReplaceChild("hexadecagon_r4", CubeListBuilder.create().texOffs(0, 0).addBox(-0.9946F, -13.5F, -5.0F, 1.0F, 27.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.0286F, 1.9472F, 36.7742F, 0.0F, 0.0F, 0.7854F));

		PartDefinition hexadecagon_r12_r1 = bone.addOrReplaceChild("hexadecagon_r12_r1", CubeListBuilder.create().texOffs(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(16.0935F, 18.0692F, -11.2258F, 0.0F, 0.0F, -0.7854F));

		PartDefinition hexadecagon_r11_r1 = bone.addOrReplaceChild("hexadecagon_r11_r1", CubeListBuilder.create().texOffs(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-16.1506F, 18.0692F, -11.2258F, 0.0F, 0.0F, 0.7854F));

		PartDefinition hexadecagon_r10_r1 = bone.addOrReplaceChild("hexadecagon_r10_r1", CubeListBuilder.create().texOffs(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-6.0F, -23.6065F, 16.1666F, 12.0F, 2.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-7.0F, -24.4076F, -43.8334F, 14.0F, 3.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-8.0F, -24.2087F, -34.8334F, 16.0F, 3.0F, 42.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(8.6966F, 23.0116F, -11.2258F, 0.0F, 0.0F, -0.3927F));

		PartDefinition hexadecagon_r9_r1 = bone.addOrReplaceChild("hexadecagon_r9_r1", CubeListBuilder.create().texOffs(6, 11).addBox(-0.8065F, -28.8F, 16.1666F, 2.0F, 12.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-6.0F, -23.6065F, 16.1666F, 12.0F, 2.0F, 26.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.6076F, -29.8F, -43.8334F, 3.0F, 14.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-7.0F, -24.4076F, -43.8334F, 14.0F, 3.0F, 60.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-1.4087F, -30.8F, -34.8334F, 3.0F, 16.0F, 42.0F, new CubeDeformation(0.0F))
		.texOffs(6, 11).addBox(-8.0F, -24.2087F, -34.8334F, 16.0F, 3.0F, 42.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-8.7538F, 23.0116F, -11.2258F, 0.0F, 0.0F, 0.3927F));

		return LayerDefinition.create(meshdefinition, 512, 512);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		bone.render(poseStack, buffer, packedLight, packedOverlay);
	}
}