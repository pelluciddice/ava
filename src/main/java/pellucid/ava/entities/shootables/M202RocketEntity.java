package pellucid.ava.entities.shootables;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;

public class M202RocketEntity extends HandGrenadeEntity
{
    public M202RocketEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public M202RocketEntity(LivingEntity shooter, Level worldIn, AVAItemGun weapon, ItemStack stack)
    {
        super(AVAEntities.ROCKET.get(), shooter, worldIn, weapon == SpecialWeapons.RPG7.get() ? 1.5F : 1.375F, weapon, true, (int) weapon.getBulletDamage(stack, true), 0, (int) weapon.getRange(stack, true), 6, AVASounds.ROCKET_TRAVEL.get(), AVASounds.ROCKET_EXPLODE.get());
    }

    @Override
    public boolean hasTrail()
    {
        return false;
    }

    @Override
    protected void setDirection()
    {
        Vec3 vec3d = getDeltaMovement();
        float f = Mth.sqrt((float) vec3d.horizontalDistanceSqr());
        setYRot((float)(Mth.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI)));
        setXRot((float)(Mth.atan2(vec3d.y, f) * (double)(180F / (float)Math.PI)));
        this.yRotO = this.getYRot();
        this.xRotO = this.getXRot();
    }

    @Override
    protected void move()
    {
        move(false);
    }

    @Override
    protected void move(boolean rotates)
    {
        super.move(rotates);
        if (level() instanceof ServerLevel && level().getGameTime() % 2L == 0)
        {
            AVAWeaponUtil.playAttenuableSoundToClientMoving(AVASounds.ROCKET_TRAVEL.get(), this);
            ((ServerLevel) level()).sendParticles(ParticleTypes.FLAME, getX(), getY(), getZ(), 2, (this.random.nextFloat() - 0.5F) / 2, (this.random.nextFloat() - 0.5F) / 2, (this.random.nextFloat() - 0.5F) / 2, 0.015F);
            ((ServerLevel) level()).sendParticles(ParticleTypes.SMOKE, getX(), getY(), getZ(), 2, (this.random.nextFloat() - 0.5F) / 2, (this.random.nextFloat() - 0.5F) / 2, (this.random.nextFloat() - 0.5F) / 2, 0.005F);
        }
    }

    @Override
    protected void explode(boolean impact)
    {
        explode(
                (entity, distance) -> {},
                (serverWorld, x, y, z) -> {
                    for (int i = 0; i < 30; i++)
                    {
                        serverWorld.sendParticles(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.5F);
                        serverWorld.sendParticles(ParticleTypes.FLAME, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 2, 0.0F, 0.0F, 0.0F, 0.5F);
                    }
                }
        );
    }

    @Override
    public boolean isNoGravity()
    {
        return true;
    }

    @Override
    public boolean isAwarable()
    {
        return false;
    }
}
