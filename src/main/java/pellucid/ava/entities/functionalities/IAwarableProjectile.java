package pellucid.ava.entities.functionalities;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import pellucid.ava.util.AVAConstants;

public interface IAwarableProjectile
{
    Item getIcon();

    default int getTrailColour(Entity to)
    {
        return isFromEnemy(to) ? AVAConstants.AVA_FRIENDLY_COLOUR : AVAConstants.AVA_HOSTILE_COLOUR;
    }

    boolean isFromEnemy(Entity to);

    default boolean hasTrail()
    {
        return true;
    }

    default boolean isAwarable()
    {
        return true;
    }
}
