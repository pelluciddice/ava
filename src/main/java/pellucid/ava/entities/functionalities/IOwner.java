package pellucid.ava.entities.functionalities;

import net.minecraft.world.entity.LivingEntity;

import javax.annotation.Nullable;

public interface IOwner
{
    @Nullable
    LivingEntity getShooter();
}
