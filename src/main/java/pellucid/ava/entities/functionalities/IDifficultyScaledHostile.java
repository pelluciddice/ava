package pellucid.ava.entities.functionalities;

import net.minecraft.world.Difficulty;
import pellucid.ava.gamemodes.modes.GameModes;

public interface IDifficultyScaledHostile
{
    default float getDamageScale(Difficulty difficulty)
    {
        float multiplier = 1.0F;
        switch (difficulty)
        {
            case PEACEFUL -> multiplier *= 0.25F;
            case EASY -> multiplier *= 0.5F;
            case NORMAL -> multiplier *= 0.75F;
            case HARD -> multiplier *= 1.0F;
        }
        return multiplier;
    }

    default float getSpreadScale(Difficulty difficulty)
    {
        float multiplier = 1.0F;
        switch (difficulty)
        {
            case PEACEFUL -> multiplier *= 1.6F;
            case EASY -> multiplier *= 1.3F;
            case NORMAL -> multiplier *= 1.0F;
            case HARD -> multiplier *= 0.2F;
        }
        return multiplier;
    }

    default float getHealthScale(Difficulty difficulty)
    {
        float multiplier = 1.0F;
        switch (difficulty)
        {
            case PEACEFUL -> multiplier *= 0.75F;
            case EASY -> multiplier *= 0.95F;
            case NORMAL -> multiplier *= 1.15F;
            case HARD -> multiplier *= 1.35F;
        }
        return multiplier;
    }

    default float getSpeedScale(Difficulty difficulty)
    {
        float multiplier = 1.0F;
        switch (difficulty)
        {
            case PEACEFUL -> multiplier *= 0.6F;
            case EASY -> multiplier *= 0.65F;
            case NORMAL -> multiplier *= 0.7F;
            case HARD -> multiplier *= 0.75F;
        }
        if (GameModes.DEMOLISH.isRunning())
            multiplier *= 1.2F;
        else if (GameModes.ESCORT.isRunning())
            multiplier *= 1.1F;
        else if (GameModes.ANNIHILATION.isRunning())
            multiplier *= 1.0F;
        return multiplier;
    }
}
