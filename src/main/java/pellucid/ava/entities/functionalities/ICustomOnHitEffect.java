package pellucid.ava.entities.functionalities;

import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;

import java.awt.Color;

public interface ICustomOnHitEffect
{
    ICustomOnHitEffect DEFAULT = new ICustomOnHitEffect() {};

    static ICustomOnHitEffect orDefault(Object obj)
    {
        return obj instanceof ICustomOnHitEffect effect ? effect : DEFAULT;
    }

    default Color getBulletHoleColour(Level world, BlockHitResult result)
    {
        return null;
    }

    default BlockState blockParticle(Level world, BlockHitResult result)
    {
        return Blocks.AIR.defaultBlockState();
    }

    default boolean playsHeadShotSound()
    {
        return isLiving();
    }

    default Block bloodParticle()
    {
        return isLiving() ? Blocks.REDSTONE_BLOCK : Blocks.AIR;
    }

    default boolean bloodMark()
    {
        return isLiving();
    }

    default boolean onHitEffect()
    {
        return isLiving();
    }

    default boolean onKillEffect()
    {
        return isLiving();
    }

    default boolean onKillTip()
    {
        return isLiving();
    }
    default void onHit(Level world, Vec3 hit)
    {

    }

    default boolean isLiving()
    {
        return true;
    }
}
