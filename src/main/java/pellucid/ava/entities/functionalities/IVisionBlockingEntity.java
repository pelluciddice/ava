package pellucid.ava.entities.functionalities;

import net.minecraft.world.phys.AABB;

public interface IVisionBlockingEntity
{
    AABB getVisionBlockingBox();

    boolean shouldBeBlocking();
}
