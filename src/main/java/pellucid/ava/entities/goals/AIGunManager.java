package pellucid.ava.entities.goals;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVAShotgun;
import pellucid.ava.sounds.SoundTrack;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.INBTSerializable;

public class AIGunManager implements INBTSerializable<CompoundTag>
{
    public final LivingEntity owner;
    private boolean reloading = false;

    private int reload = 0;
    private float fireTicks = 0;
    private boolean firing = false;
    private int fireAnimTicks = 0;

    public AIGunManager(LivingEntity owner)
    {
        this.owner = owner;
    }

    public void tryFire(LivingEntity target)
    {
        ItemStack stack = owner.getMainHandItem();
        if (stack.getItem() instanceof AVAItemGun)
        {
            AVAItemGun gun = (AVAItemGun) stack.getItem();
            if (gun.firable(owner, stack) && !reloading && fireTicks <= 0)
            {
                gun.fire(owner.level(), owner, target, stack, AVAServerConfig.isCompetitiveModeActivated());
                fireTicks += 20.0F / gun.getFireRate(stack, true);
                firing = true;
                fireAnimTicks = 0;
            }
        }
    }

    public void tick()
    {
        ItemStack stack = owner.getMainHandItem();
        if (stack.getItem() instanceof AVAItemGun)
        {
            AVAItemGun gun = (AVAItemGun) stack.getItem();
            boolean competitive = AVAServerConfig.isCompetitiveModeActivated();
            if (fireTicks > 0)
                fireTicks--;
            if (stack.getItem() instanceof AVAShotgun)
            {
                if (reloading && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) >= gun.getCapacity(stack, true))
                    reloading = false;
                if (stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < 1 && gun.reloadable(owner, stack, competitive))
                    reloading = true;
            }
            else if (stack.getItem() instanceof AVAItemGun)
            {
                if (stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < 1 && gun.reloadable(owner, stack, competitive))
                    reloading = true;
            }
            if (reloading)
            {
                if (reload < gun.getReloadTime(stack))
                {
                    SoundTrack.Track sound = gun.getReloadSound(stack).get(reload);
                    if (sound != null)
                        owner.playSound(sound.sound.get(), sound.volume, sound.pitch);
                    reload++;
                }
                else
                {
                    reload = 0;
                    reloading = false;
                    gun.reload(owner, stack, competitive);
                }
            }
            if (firing)
            {
                if (fireAnimTicks < gun.getFireAnimation(stack))
                {
                    SoundTrack.Track sound = gun.getShootSound(stack).get(fireAnimTicks);
                    if (sound != null)
                        AVAWeaponUtil.playAttenuableSoundToClient(sound.sound.get(), owner, 0.75F, 0.85F + owner.getRandom().nextFloat() * 0.3F, true, (p) -> false);
                    fireAnimTicks++;
                }
                else
                {
                    fireAnimTicks = 0;
                    firing = false;
                }
            }
        }
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.BOOLEAN.write(compound, "reloading", reloading);
        DataTypes.INT.write(compound, "reload", reload);
        DataTypes.FLOAT.write(compound, "fireTicks", fireTicks);
        DataTypes.BOOLEAN.write(compound, "firing", firing);
        DataTypes.INT.write(compound, "fireAnimTicks", fireAnimTicks);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        reloading = DataTypes.BOOLEAN.read(nbt, "reloading");
        reload = DataTypes.INT.read(nbt, "reload");
        fireTicks = DataTypes.FLOAT.read(nbt, "fireTicks");
        firing = DataTypes.BOOLEAN.read(nbt, "firing");
        fireAnimTicks = DataTypes.INT.read(nbt, "fireAnimTicks");
    }
}
