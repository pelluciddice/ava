package pellucid.ava.entities.goals;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.util.LandRandomPos;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.util.AVAConstants;

import javax.annotation.Nullable;
import java.util.EnumSet;

public class AVAMeleeAttackGoal extends Goal
{
    protected final PathfinderMob mob;
    @Nullable
    private final SoundEvent attackSound;
    private final double speedModifier;
    private final boolean followingTargetEvenIfNotSeen;
    private Path path;
    private double pathedTargetX;
    private double pathedTargetY;
    private double pathedTargetZ;
    private int ticksUntilNextPathRecalculation;
    private int ticksUntilNextAttack;
    private long lastCanUseCheck;
    private int failedPathFindingPenalty = 0;
    private boolean canPenalize = false;
    private final float distraction;
    private final int randomMoveDuration;

    public AVAMeleeAttackGoal(PathfinderMob creature, @Nullable SoundEvent attackSound, double speed, boolean useLongMemory, float distraction, int randomMoveDuration)
    {
        this.mob = creature;
        this.attackSound = attackSound;
        this.speedModifier = speed;
        this.followingTargetEvenIfNotSeen = useLongMemory;
        this.randomMoveDuration = randomMoveDuration;
        this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
        this.distraction = distraction;
    }

    @Override
    public boolean canUse()
    {
        long i = this.mob.level().getGameTime();
        if (i - this.lastCanUseCheck < 20L)
            return false;
        else
        {
            this.lastCanUseCheck = i;
            LivingEntity livingentity = this.mob.getTarget();
            if (livingentity == null)
                return false;
            else if (!livingentity.isAlive())
                return false;
            else
            {
                if (canPenalize)
                {
                    if (--this.ticksUntilNextPathRecalculation <= 0)
                    {
                        this.path = this.mob.getNavigation().createPath(livingentity, 0);
                        this.ticksUntilNextPathRecalculation = 4 + this.mob.getRandom().nextInt(7);
                        return this.path != null;
                    }
                    else
                        return true;
                }
                this.path = this.mob.getNavigation().createPath(livingentity, 0);
                if (this.path != null)
                    return true;
                else
                    return this.mob.isWithinMeleeAttackRange(livingentity);
            }
        }
    }

    @Override
    public boolean canContinueToUse()
    {
        LivingEntity livingentity = this.mob.getTarget();
        if (livingentity == null)
            return false;
        else if (!livingentity.isAlive())
            return false;
        else if (!this.followingTargetEvenIfNotSeen)
            return !this.mob.getNavigation().isDone();
        else if (!this.mob.isWithinRestriction(livingentity.blockPosition()))
            return false;
        return !(livingentity instanceof Player) || !livingentity.isSpectator() && !((Player) livingentity).isCreative();
    }

    @Override
    public void start()
    {
        this.mob.getNavigation().moveTo(this.path, this.speedModifier);
        this.mob.setAggressive(true);
        this.ticksUntilNextPathRecalculation = 0;
        this.ticksUntilNextAttack = 0;
        resetAttackCooldown();
    }

    @Override
    public void stop()
    {
        LivingEntity livingentity = this.mob.getTarget();
        if (!EntitySelector.NO_CREATIVE_OR_SPECTATOR.test(livingentity))
            this.mob.setTarget(null);

        this.mob.setAggressive(false);
        this.mob.getNavigation().stop();
    }

    @Override
    public boolean requiresUpdateEveryTick()
    {
        return true;
    }

    private int randomMove = 0;
    @Override
    public void tick()
    {
        LivingEntity target = this.mob.getTarget();
        if (target != null)
        {
            this.mob.getLookControl().setLookAt(target, 30.0F, 30.0F);
            this.ticksUntilNextPathRecalculation = Math.max(this.ticksUntilNextPathRecalculation - 1, 0);
            if (randomMove > 0)
                randomMove--;
            else
            {
                if (target.position().distanceTo(mob.position()) > 3.0F && mob.getRandom().nextFloat() < distraction)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        Vec3 vec = LandRandomPos.getPos(mob, 5, 2);
                        if (vec != null && (vec.distanceTo(target.position()) < target.distanceTo(mob)))
                        {
                            mob.getNavigation().moveTo(vec.x, vec.y, vec.z, speedModifier);
                            randomMove = randomMoveDuration;
                            break;
                        }
                    }
                }
                else if ((this.followingTargetEvenIfNotSeen || this.mob.getSensing().hasLineOfSight(target))
                        && this.ticksUntilNextPathRecalculation <= 0
                        && (
                        this.pathedTargetX == 0.0 && this.pathedTargetY == 0.0 && this.pathedTargetZ == 0.0
                                || target.distanceToSqr(this.pathedTargetX, this.pathedTargetY, this.pathedTargetZ) >= 1.0
                                || this.mob.getRandom().nextFloat() < 0.05F
                ))
                {
                    this.pathedTargetX = target.getX();
                    this.pathedTargetY = target.getY();
                    this.pathedTargetZ = target.getZ();
                    this.ticksUntilNextPathRecalculation = 4 + this.mob.getRandom().nextInt(7);
                    double d0 = this.mob.distanceToSqr(target);
                    if (this.canPenalize)
                    {
                        this.ticksUntilNextPathRecalculation += failedPathFindingPenalty;
                        if (this.mob.getNavigation().getPath() != null)
                        {
                            net.minecraft.world.level.pathfinder.Node finalPathPoint = this.mob.getNavigation().getPath().getEndNode();
                            if (finalPathPoint != null && target.distanceToSqr(finalPathPoint.x, finalPathPoint.y, finalPathPoint.z) < 1)
                                failedPathFindingPenalty = 0;
                            else
                                failedPathFindingPenalty += 10;
                        }
                        else
                            failedPathFindingPenalty += 10;
                    }
                    if (d0 > 1024.0)
                        this.ticksUntilNextPathRecalculation += 10;
                    else if (d0 > 256.0)
                        this.ticksUntilNextPathRecalculation += 5;

                    if (!this.mob.getNavigation().moveTo(target, this.speedModifier))
                        this.ticksUntilNextPathRecalculation += 15;

                    this.ticksUntilNextPathRecalculation = this.adjustedTickDelay(this.ticksUntilNextPathRecalculation);
                }
            }

            this.ticksUntilNextAttack = Math.max(this.ticksUntilNextAttack - 1, 0);
            this.checkAndPerformAttack(target);
        }
        if (getTicksUntilNextAttack() == 6 && attacked)
        {
            mob.swing(InteractionHand.MAIN_HAND);
            if (attackSound != null)
                mob.playSound(attackSound, 1.0F, 0.9F + AVAConstants.RAND.nextFloat() * 0.2F);
        }
    }

    protected void resetAttackCooldown()
    {

        this.ticksUntilNextAttack = this.adjustedTickDelay(20);
    }

    protected boolean isTimeToAttack()
    {
        return this.ticksUntilNextAttack <= 0;
    }

    protected boolean canPerformAttack(LivingEntity p_301299_)
    {
        return this.isTimeToAttack() && this.mob.isWithinMeleeAttackRange(p_301299_) && this.mob.getSensing().hasLineOfSight(p_301299_);
    }

    protected int getTicksUntilNextAttack()
    {
        return this.ticksUntilNextAttack;
    }

    protected int getAttackInterval()
    {
        return this.adjustedTickDelay(20);
    }

    private boolean attacked = false;
    protected void checkAndPerformAttack(LivingEntity enemy)
    {
        if (canPerformAttack(enemy))
        {
            resetAttackCooldown();
            mob.doHurtTarget(enemy);
            attacked = true;
        }
    }
}
