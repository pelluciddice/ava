package pellucid.ava.entities.robots;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.ai.goal.RandomStrollGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.entities.goals.AVAMeleeAttackGoal;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.sounds.AVASounds;

public class DarkBlueRobotEntity extends AVARobotEntity
{
    public DarkBlueRobotEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public int getAmbientSoundInterval()
    {
        return 50;
    }

    @Override
    protected @Nullable SoundEvent getHurtSound(DamageSource p_21239_)
    {
        return AVASounds.DARK_BLUE_ROBOT_ONHIT.get();
    }

    @Override
    protected @Nullable SoundEvent getAmbientSound()
    {
        return AVASounds.DARK_BLUE_ROBOT_AMBIENT.get();
    }

    @Override
    protected void registerGoals()
    {
        goalSelector.addGoal(0, new AVAMeleeAttackGoal(this, getAttackSound(), getMovementSpeedFactor(), true, 0.01F, 15));
        goalSelector.addGoal(1, new MoveTowardsTargetGoal(this, getMovementSpeedFactor(), getFollowingDistance()));
        goalSelector.addGoal(2, new RandomStrollGoal(this, 0.25F, 30));
        targetSelector.addGoal(0, new AVAHostileEntity.AttackPlayerGoal(this, 0, false, false, (entity) -> entity instanceof Player && isAttackablePlayer((Player) entity), getFollowingDistance()));
    }

    @Override
    protected float getMovementSpeedFactor()
    {
        return super.getMovementSpeedFactor() / 1.5F;
    }
}
