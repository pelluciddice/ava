package pellucid.ava.entities.robots;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.RandomStrollGoal;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.entities.functionalities.ICustomOnHitEffect;
import pellucid.ava.entities.goals.AVAMeleeAttackGoal;
import pellucid.ava.entities.livings.AVABotEntity;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.sounds.AVASounds;

public class AVARobotEntity extends AVABotEntity implements ICustomOnHitEffect, Enemy
{
    public AVARobotEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected void registerGoals()
    {
        goalSelector.addGoal(0, new AVAMeleeAttackGoal(this, getAttackSound(), getMovementSpeedFactor(), true, 0.035F, 15));
        goalSelector.addGoal(2, new RandomStrollGoal(this, 0.25F, 30));
        targetSelector.addGoal(0, new AVAHostileEntity.AttackPlayerGoal(this, 0, false, false, (entity) -> entity instanceof Player && isAttackablePlayer((Player) entity), getFollowingDistance()));
    }

    @Nullable
    @Override
    protected SoundEvent getDeathSound()
    {
        return AVASounds.COMMON_ROBOT_DEATH.get();
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource p_21239_)
    {
        return AVASounds.COMMON_ROBOT_ONHIT.get();
    }

    @Nullable
    @Override
    protected SoundEvent getAmbientSound()
    {
        return AVASounds.BLUE_ROBOT_AMBIENT.get();
    }

    @Override
    protected float getSoundVolume()
    {
        return 1.5F;
    }

    protected SoundEvent getAttackSound()
    {
        return AVASounds.COMMON_ROBOT_ATTACK.get();
    }

    @Override
    public boolean playsHeadShotSound()
    {
        return false;
    }

    @Override
    public Block bloodParticle()
    {
        return Blocks.LAPIS_BLOCK;
    }

    @Override
    public boolean bloodMark()
    {
        return false;
    }

    @Override
    public void onHit(Level world, Vec3 hit)
    {
        if (level() instanceof ServerLevel)
            ((ServerLevel) level()).sendParticles(ParticleTypes.ENCHANTED_HIT, hit.x, hit.y, hit.z, 4, 0, 0, 0, 0.25F);
    }

    @Override
    public void aiStep()
    {
        updateSwingTime();
        super.aiStep();
    }

    @Override
    public int getAmbientSoundInterval()
    {
        return 20;
    }

    protected float getMovementSpeedFactor()
    {
        return 0.65F;
    }

    protected float getFollowingDistance()
    {
        return 100.0F;
    }

    public static AttributeSupplier.Builder registerAttributes(float health, float attackDamage)
    {
        return Monster.createMonsterAttributes()
                .add(Attributes.ATTACK_DAMAGE, attackDamage)
                .add(Attributes.MAX_HEALTH, health)
                .add(Attributes.MOVEMENT_SPEED, 0.4D)
                .add(Attributes.ATTACK_KNOCKBACK, 0.0F)
                .add(Attributes.KNOCKBACK_RESISTANCE, 1.0F)
                .add(Attributes.FOLLOW_RANGE, 50.0F);
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        return ItemStack.EMPTY;
    }
}
