package pellucid.ava.entities.robots.renderers;// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;

public class YellowRobotModel extends BaseRobotModel
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor

	public YellowRobotModel(ModelPart root) {
		super(root, 0.5F);
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition hat = partdefinition.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(0, 0), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(16, 85).addBox(-3.0F, -8.5F, -10.0F, 6.0F, 11.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(66, 22).addBox(-4.0F, -9.5F, -8.0F, 8.0F, 13.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(32, 12).addBox(-3.0F, -2.5F, -2.0F, 6.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -33.5F, 1.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 0).addBox(-15.0F, -2.4984F, 0.2871F, 30.0F, 5.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(0, 56).addBox(-6.0F, 2.5016F, -0.6738F, 12.0F, 21.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(72, 41).addBox(-2.0F, 23.5016F, -0.6738F, 4.0F, 5.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -33.5016F, 0.6738F));

		PartDefinition cube_r1 = body.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(0, 12).addBox(-10.176F, -56.4906F, 0.0F, 12.0F, 22.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 57.5016F, -0.6738F, 0.0F, 0.0F, 0.1222F));

		PartDefinition cube_r2 = body.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(68, 57).addBox(-33.2772F, -49.4641F, 0.9648F, 5.0F, 11.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 57.5016F, -0.6738F, 0.0F, 0.0F, 0.7854F));

		PartDefinition cube_r3 = body.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(74, 0).addBox(28.2772F, -49.4641F, 0.9648F, 5.0F, 11.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 57.5016F, -0.6738F, 0.0F, 0.0F, -0.7854F));

		PartDefinition cube_r4 = body.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(32, 34).addBox(-1.824F, -56.4906F, 0.0F, 12.0F, 22.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 57.5016F, -0.6738F, 0.0F, 0.0F, -0.1222F));

		PartDefinition left_arm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(35, 83).mirror().addBox(-0.5F, -5.3201F, -1.4163F, 5.0F, 10.0F, 5.0F, new CubeDeformation(0.0F)).mirror(false)
		.texOffs(86, 75).addBox(0.5F, 4.6799F, -0.4163F, 3.0F, 12.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(15.5F, -33.6799F, 3.4163F));

		PartDefinition cube_r5 = left_arm.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(86, 90).addBox(17.0F, 27.0835F, -40.7932F, 1.0F, 17.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(92, 54).addBox(17.0F, 27.0835F, -36.7932F, 1.0F, 11.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(55, 86).addBox(17.0F, 22.0835F, -40.7932F, 1.0F, 5.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(0, 85).addBox(15.5F, 13.2793F, -39.2059F, 4.0F, 9.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.5F, 57.6799F, -3.4163F, -2.0508F, 0.0F, 0.0F));

		PartDefinition cube_r6 = left_arm.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(92, 90).addBox(17.0F, 50.7714F, -5.6132F, 1.0F, 9.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.5F, 57.6799F, -3.4163F, -2.7053F, 0.0F, 0.0F));

		PartDefinition right_arm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(35, 83).addBox(-4.5F, -5.3201F, -2.4163F, 5.0F, 10.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(86, 75).addBox(-3.5F, 4.6799F, -1.4163F, 3.0F, 12.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-15.5F, -33.6799F, 4.4163F));

		PartDefinition cube_r7 = right_arm.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(86, 90).addBox(-18.0F, 27.0183F, -40.7848F, 1.0F, 17.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(92, 54).addBox(-18.0F, 27.0183F, -36.7848F, 1.0F, 11.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(55, 86).addBox(-18.0F, 22.0183F, -40.7848F, 1.0F, 5.0F, 7.0F, new CubeDeformation(0.0F))
		.texOffs(0, 85).addBox(-19.5F, 13.2141F, -39.1976F, 4.0F, 9.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.5F, 57.6799F, -4.4163F, -2.0508F, 0.0F, 0.0F));

		PartDefinition cube_r8 = right_arm.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(92, 90).addBox(-18.0F, 50.6977F, -5.6281F, 1.0F, 9.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.5F, 57.6799F, -4.4163F, -2.7053F, 0.0F, 0.0F));

		PartDefinition left_leg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(40, 12).mirror().addBox(-4.0F, 0.0407F, -2.39F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)).mirror(false)
		.texOffs(0, 42).addBox(-3.0F, 30.9626F, -6.5033F, 6.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, -10.0407F, 2.39F));

		PartDefinition cube_r9 = left_leg.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(68, 75).addBox(-8.5F, -14.8802F, 0.6693F, 5.0F, 14.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(6.0F, 34.0407F, -2.39F, 0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r10 = left_leg.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(40, 64).addBox(-9.5F, -26.7437F, -3.0016F, 7.0F, 12.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(6.0F, 34.0407F, -2.39F, -0.1309F, 0.0F, 0.0F));

		PartDefinition right_leg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(40, 12).addBox(-4.0F, 0.0407F, -2.39F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(0, 42).addBox(-3.0F, 30.9626F, -6.5033F, 6.0F, 3.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(6.0F, -10.0407F, 2.39F));

		PartDefinition cube_r11 = right_leg.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(68, 75).addBox(-2.5F, -0.104F, -3.0211F, 5.0F, 14.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 19.0F, 0.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition cube_r12 = right_leg.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(40, 64).mirror().addBox(-3.5F, -11.521F, -3.4103F, 7.0F, 12.0F, 7.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.0F, 19.0F, 0.0F, -0.1309F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack stack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		super.renderToBuffer(stack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
//				head.render(poseStack, buffer, packedLight, packedOverlay);
//				body.render(poseStack, buffer, packedLight, packedOverlay);
//				leftArm.render(poseStack, buffer, packedLight, packedOverlay);
//				rightArm.render(poseStack, buffer, packedLight, packedOverlay);
//				leftLeg.render(poseStack, buffer, packedLight, packedOverlay);
//				rightLeg.render(poseStack, buffer, packedLight, packedOverlay);
	}

	@Override
	protected void translateScaleOffset(PoseStack stack)
	{
		stack.translate(0.0F, 1.55F, 0.0F);
	}
}