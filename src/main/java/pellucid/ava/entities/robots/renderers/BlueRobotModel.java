package pellucid.ava.entities.robots.renderers;
// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;

public class BlueRobotModel extends BaseRobotModel
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor

	public BlueRobotModel(ModelPart root) {
		super(root, 0.825F);
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition hat = partdefinition.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(0, 0), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(24, 0).addBox(-3.0F, -3.4707F, -2.0397F, 6.0F, 3.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(42, 8).addBox(-1.5F, -3.4707F, -1.0397F, 3.0F, 5.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(12, 41).addBox(-2.5F, -6.6465F, -0.0397F, 5.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(44, 36).addBox(-1.0F, -6.4707F, -2.9421F, 2.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(19, 0).addBox(-1.5F, -3.4707F, -3.3991F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(32, 27).addBox(-2.0F, -5.4707F, -1.0397F, 4.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.5293F, -0.9603F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, 1.1667F, -2.4167F, 7.0F, 11.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(44, 16).addBox(-1.5F, 12.1667F, -2.4167F, 3.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(22, 10).addBox(-3.0F, 3.1667F, -2.9167F, 6.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(0, 16).addBox(-4.0F, 1.1667F, -2.9167F, 8.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(12, 48).addBox(-3.5F, -0.3333F, -2.4167F, 7.0F, 2.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(28, 20).addBox(-2.5F, 7.1667F, -2.9167F, 5.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.1667F, -0.5833F));

		PartDefinition left_arm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(24, 12).addBox(-0.5F, 9.25F, -1.75F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(12, 24).addBox(0.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-1.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(44, 49).addBox(-1.5F, 5.25F, -1.75F, 3.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(32, 40).addBox(-1.0F, 2.25F, -1.75F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(32, 46).addBox(-1.5F, -1.75F, -1.75F, 3.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(5.0F, 2.75F, -0.25F));

		PartDefinition right_arm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(44, 49).addBox(-1.5F, 5.25F, -1.75F, 3.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(32, 46).addBox(-1.5F, -1.75F, -1.75F, 3.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(32, 40).addBox(-2.0F, 2.25F, -1.75F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 24).addBox(-0.5F, 9.25F, -1.75F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(24, 8).addBox(-1.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 16).addBox(0.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-5.0F, 2.75F, -0.25F));

		PartDefinition left_leg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(16, 24).addBox(-2.0F, -0.5F, -2.0F, 4.0F, 13.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(3.5F, 11.5F, -0.5F));

		PartDefinition right_leg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(0, 24).addBox(-2.0F, -0.5F, -2.0F, 4.0F, 13.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.5F, 11.5F, -0.5F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void renderToBuffer(PoseStack stack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		super.renderToBuffer(stack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		//		head.render(poseStack, buffer, packedLight, packedOverlay);
		//		body.render(poseStack, buffer, packedLight, packedOverlay);
		//		leftArm.render(poseStack, buffer, packedLight, packedOverlay);
		//		rightArm.render(poseStack, buffer, packedLight, packedOverlay);
		//		leftLeg.render(poseStack, buffer, packedLight, packedOverlay);
		//		rightLeg.render(poseStack, buffer, packedLight, packedOverlay);
	}

	@Override
	protected void translateScaleOffset(PoseStack stack)
	{
		stack.translate(0.0F, 0.25F, 0.0F);
	}
}