package pellucid.ava.entities.robots.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.util.Mth;
import pellucid.ava.entities.robots.AVARobotEntity;

import static pellucid.ava.util.AVACommonUtil.toRad;

public abstract class BaseRobotModel extends HumanoidModel<AVARobotEntity>
{
    private final float size;

    public BaseRobotModel(ModelPart part)
    {
        this(part, 1.0F);
    }

    public BaseRobotModel(ModelPart part, float size)
    {
        super(part);
        this.size = size;
    }

    @Override
    public void renderToBuffer(PoseStack stack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
    {
        stack.pushPose();
        stack.scale(size, size, size);
        translateScaleOffset(stack);
        head.render(stack, buffer, packedLight, packedOverlay);
        body.render(stack, buffer, packedLight, packedOverlay);
        leftArm.render(stack, buffer, packedLight, packedOverlay);
        rightArm.render(stack, buffer, packedLight, packedOverlay);
        leftLeg.render(stack, buffer, packedLight, packedOverlay);
        rightLeg.render(stack, buffer, packedLight, packedOverlay);
        stack.popPose();
    }

    @Override
    public void setupAnim(AVARobotEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
    {
        this.head.yRot = netHeadYaw * ((float)Math.PI / 180F);
        this.head.xRot = headPitch * ((float)Math.PI / 180F);

        setPreRotationPoints();
        this.rightArm.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 2.0F * limbSwingAmount * 0.5F;
        this.leftArm.xRot = Mth.cos(limbSwing * 0.6662F) * 2.0F * limbSwingAmount * 0.5F;
        this.rightArm.zRot = 0.0F;
        this.leftArm.zRot = 0.0F;
        this.rightLeg.xRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        this.leftLeg.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        this.rightLeg.yRot = 0.0F;
        this.leftLeg.yRot = 0.0F;
        this.rightLeg.zRot = 0.0F;
        this.leftLeg.zRot = 0.0F;
        this.rightArm.yRot = 0.0F;
        this.leftArm.yRot = 0.0F;
        this.poseRightArm(entityIn);
        this.poseLeftArm(entityIn);
        this.setupAttackAnimation(entityIn, ageInTicks);
        this.body.xRot = 0.0F;
        setPostRotationPoints();

        AnimationUtils.bobArms(this.rightArm, this.leftArm, ageInTicks);
    }

    protected void setPreRotationPoints()
    {
//        this.bipedBody.rotateAngleY = 0.0F;
//        this.bipedRightArm.rotationPointZ = 0.0F;
//        this.bipedRightArm.rotationPointX = -5.0F;
//        this.bipedLeftArm.rotationPointZ = 0.0F;
//        this.bipedLeftArm.rotationPointX = 5.0F;
    }

    protected void setPostRotationPoints()
    {
//        this.bipedRightLeg.rotationPointZ = 0.1F;
//        this.bipedLeftLeg.rotationPointZ = 0.1F;
//        this.bipedRightLeg.rotationPointY = 12.0F;
//        this.bipedLeftLeg.rotationPointY = 12.0F;
//        this.bipedHead.rotationPointY = 0.0F;
//        this.bipedBody.rotationPointY = 0.0F;
//        this.bipedLeftArm.rotationPointY = 2.0F;
//        this.bipedRightArm.rotationPointY = 2.0F;
    }

    public void poseRightArm(AVARobotEntity p_241654_1_)
    {
        switch (this.rightArmPose)
        {
            case EMPTY -> this.rightArm.yRot = 0.0F;
            case BLOCK -> {
                this.rightArm.xRot = this.rightArm.xRot * 0.5F - 0.9424779F;
                this.rightArm.yRot = (-(float) Math.PI / 6F);
            }
            case ITEM -> {
                this.rightArm.xRot = this.rightArm.xRot * 0.5F - ((float) Math.PI / 10F);
                this.rightArm.yRot = 0.0F;
            }
            case THROW_SPEAR -> {
                this.rightArm.xRot = this.rightArm.xRot * 0.5F - (float) Math.PI;
                this.rightArm.yRot = 0.0F;
            }
            case BOW_AND_ARROW -> {
                this.rightArm.yRot = -0.1F + this.head.yRot;
                this.leftArm.yRot = 0.1F + this.head.yRot + 0.4F;
                this.rightArm.xRot = (-(float) Math.PI / 2F) + this.head.xRot;
                this.leftArm.xRot = (-(float) Math.PI / 2F) + this.head.xRot;
            }
            case CROSSBOW_CHARGE -> AnimationUtils.animateCrossbowCharge(this.rightArm, this.leftArm, p_241654_1_, true);
            case CROSSBOW_HOLD -> AnimationUtils.animateCrossbowHold(this.rightArm, this.leftArm, this.head, true);
        }

    }

    public void poseLeftArm(AVARobotEntity p_241655_1_)
    {
        switch (this.leftArmPose)
        {
            case EMPTY -> this.leftArm.yRot = 0.0F;
            case BLOCK -> {
                this.leftArm.xRot = this.leftArm.xRot * 0.5F - 0.9424779F;
                this.leftArm.yRot = ((float) Math.PI / 6F);
            }
            case ITEM -> {
                this.leftArm.xRot = this.leftArm.xRot * 0.5F - ((float) Math.PI / 10F);
                this.leftArm.yRot = 0.0F;
            }
            case THROW_SPEAR -> {
                this.leftArm.xRot = this.leftArm.xRot * 0.5F - (float) Math.PI;
                this.leftArm.yRot = 0.0F;
            }
            case BOW_AND_ARROW -> {
                this.rightArm.yRot = -0.1F + this.head.yRot - 0.4F;
                this.leftArm.yRot = 0.1F + this.head.yRot;
                this.rightArm.xRot = (-(float) Math.PI / 2F) + this.head.xRot;
                this.leftArm.xRot = (-(float) Math.PI / 2F) + this.head.xRot;
            }
            case CROSSBOW_CHARGE -> AnimationUtils.animateCrossbowCharge(this.rightArm, this.leftArm, p_241655_1_, false);
            case CROSSBOW_HOLD -> AnimationUtils.animateCrossbowHold(this.rightArm, this.leftArm, this.head, false);
        }

    }

    @Override
    protected void setupAttackAnimation(AVARobotEntity entity, float ageInTicks)
    {
        if (attackTime > 0.0F)
        {
            setRotationsRad(rightArm, radLerpSwing(120.0F, 60.0F), radLerpSwing(-70.0F, 30.0F), radLerpSwing(10.0F, 30.0F));
            setRotationsRad(leftArm, radLerpSwing(25.0F, -25.0F), radLerpSwing(-3.0F, 18.0F), radLerpSwing(-23.0F, 27.0F));
        }
        else
        {
            setRotationsDeg(rightArm, 25.0F, -3.0F, 23.0F);
            setRotationsDeg(leftArm, 25.0F, -3.0F, -23.0F);
        }
        AnimationUtils.bobArms(rightArm, rightArm, ageInTicks);
    }

    abstract protected void translateScaleOffset(PoseStack stack);

    protected void setRotationsDeg(ModelPart model, float x, float y, float z)
    {
        setRotationsRad(model, toRad(x), toRad(y), toRad(z));
    }

    protected void setRotationsRad(ModelPart model, float x, float y, float z)
    {
        setRotationAngle(model, (float) (Math.PI * 2.0F - x), (float) (Math.PI * 2.0F - y), z);
    }

    protected void setRotationAngle(ModelPart model, float x, float y, float z)
    {
        model.xRot = x;
        model.yRot = y;
        model.zRot = z;
    }

    protected void setPivot(ModelPart model, float x, float y, float z)
    {
        model.x = x;
        model.y = y;
        model.z = z;
    }

    protected float radLerpSwing(float from, float to)
    {
        return Mth.lerp(attackTime, toRad(from), toRad(to));
    }
}
