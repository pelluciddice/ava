package pellucid.ava.entities.robots.renderers;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.resources.ResourceLocation;
import pellucid.ava.AVA;
import pellucid.ava.entities.robots.AVARobotEntity;

public class RobotRenderer<M extends HumanoidModel<AVARobotEntity>> extends HumanoidMobRenderer<AVARobotEntity, M>
{
    public static final ResourceLocation BLUE = new ResourceLocation(AVA.MODID, "textures/entities/robot_blue.png");
    public static final ResourceLocation YELLOW = new ResourceLocation(AVA.MODID, "textures/entities/robot_yellow.png");
    public static final ResourceLocation YELLOW_INVIS = new ResourceLocation(AVA.MODID, "textures/entities/robot_yellow_invis.png");
    public static final ResourceLocation DARK_BLUE = new ResourceLocation(AVA.MODID, "textures/entities/robot_dark_blue.png");

    private final ResourceLocation texture;
    public RobotRenderer(EntityRendererProvider.Context context, M model, ResourceLocation texture)
    {
        super(context, model, 0.5F);
        this.texture = texture;
    }

    @Override
    public ResourceLocation getTextureLocation(AVARobotEntity entity)
    {
        return texture;
    }
}
