package pellucid.ava.entities.robots.renderers;// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.Entity;
import pellucid.ava.entities.robots.AVARobotEntity;

public class DarkBlueRobotModel<T extends Entity> extends BaseRobotModel
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	private final ModelPart rotatable;
	private final ModelPart rotatable2;

	public DarkBlueRobotModel(ModelPart root) {
		super(root, 0.425F);
		rotatable = rightArm.getChild("rotatable");
		rotatable2 = leftArm.getChild("rotatable2");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition hat = partdefinition.addOrReplaceChild("hat", CubeListBuilder.create().texOffs(0, 0), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(129, 79).addBox(-7.0F, -14.0F, -7.0F, 14.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -61.0F, -2.3516F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 0).addBox(-22.0F, -19.5833F, -1.7473F, 44.0F, 19.0F, 17.0F, new CubeDeformation(0.0F))
		.texOffs(0, 36).addBox(-14.0F, -0.5833F, -6.7473F, 28.0F, 23.0F, 18.0F, new CubeDeformation(0.0F))
		.texOffs(0, 110).addBox(-7.0F, 22.4167F, -4.7473F, 14.0F, 14.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(108, 22).addBox(-7.5F, -19.5833F, -14.0989F, 15.0F, 19.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -41.4167F, -5.2527F));

		PartDefinition cube_r1 = body.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(0, 77).mirror().addBox(-10.5F, -7.5F, -8.2227F, 19.0F, 19.0F, 14.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(10.2016F, -12.0833F, -1.0002F, 0.0F, -0.6981F, 0.0F));

		PartDefinition cube_r2 = body.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(0, 77).addBox(-8.5F, -7.5F, -8.2227F, 19.0F, 19.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.2016F, -12.0833F, -1.0002F, 0.0F, 0.6981F, 0.0F));

		PartDefinition left_arm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(111, 118).addBox(-0.5F, 7.4846F, -3.6002F, 13.0F, 24.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(75, 60).addBox(-2.5F, -8.5154F, -5.6002F, 17.0F, 16.0F, 17.0F, new CubeDeformation(0.0F)), PartPose.offset(19.5F, -57.4846F, -1.3998F));

		PartDefinition cube_r8 = left_arm.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(105, 0).addBox(-3.5F, -4.5F, -3.5F, 7.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(6.0F, 26.9846F, -6.1002F, -0.3054F, 0.0F, 0.0F));

		PartDefinition rotatable2 = left_arm.addOrReplaceChild("rotatable2", CubeListBuilder.create().texOffs(47, 131).addBox(-5.5F, -5.7679F, 2.5F, 11.0F, 11.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(126, 55).addBox(-3.5F, -3.7679F, -10.5F, 7.0F, 7.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(153, 42).addBox(-2.0F, -10.3929F, -7.5F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(6.0F, 20.382F, -23.7382F, -0.3054F, 0.0F, 0.0F));

		PartDefinition cube_r9 = rotatable2.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(74, 36).addBox(-2.1337F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, -1.2566F));

		PartDefinition cube_r10 = rotatable2.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(82, 142).addBox(-2.0827F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, -2.5133F));

		PartDefinition cube_r11 = rotatable2.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(150, 107).addBox(-1.9173F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, 2.5133F));

		PartDefinition cube_r12 = rotatable2.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(150, 142).addBox(-1.8663F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, 1.2566F));

		PartDefinition right_arm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(75, 60).addBox(-14.5F, -8.5154F, -5.6002F, 17.0F, 16.0F, 17.0F, new CubeDeformation(0.0F))
		.texOffs(111, 118).addBox(-12.5F, 7.4846F, -3.6002F, 13.0F, 24.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offset(-19.5F, -57.4846F, -1.3998F));

		PartDefinition cube_r3 = right_arm.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(105, 0).addBox(-3.5F, -4.5F, -3.5F, 7.0F, 7.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-6.0F, 26.9846F, -6.1002F, -0.3054F, 0.0F, 0.0F));

		PartDefinition rotatable = right_arm.addOrReplaceChild("rotatable", CubeListBuilder.create().texOffs(47, 131).addBox(-5.5F, -5.7679F, 2.5F, 11.0F, 11.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(126, 55).addBox(-3.5F, -3.7679F, -10.5F, 7.0F, 7.0F, 13.0F, new CubeDeformation(0.0F))
		.texOffs(153, 42).addBox(-2.0F, -10.3929F, -7.5F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-6.0F, 20.382F, -23.7382F, -0.3054F, 0.0F, 0.0F));

		PartDefinition cube_r4 = rotatable.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(74, 36).addBox(-2.1337F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, -1.2566F));

		PartDefinition cube_r5 = rotatable.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(82, 142).addBox(-2.0827F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, -2.5133F));

		PartDefinition cube_r6 = rotatable.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(150, 107).addBox(-1.9173F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, 2.5133F));

		PartDefinition cube_r7 = rotatable.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(150, 142).addBox(-1.8663F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.0335F, 2.25F, 0.0F, 0.0F, 1.2566F));

		PartDefinition left_leg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(122, 0).addBox(-5.5F, 36.8132F, -8.3384F, 11.0F, 5.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(13.5F, -17.6296F, -3.9585F));

		PartDefinition cube_r13 = left_leg.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(0, 140).addBox(-5.0F, -15.8164F, -6.2969F, 9.0F, 23.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 31.6296F, 1.9585F, 0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r14 = left_leg.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(66, 93).addBox(-6.5F, -12.2539F, -9.7969F, 13.0F, 22.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 9.6296F, 1.9585F, -0.1745F, 0.0F, 0.0F));

		PartDefinition right_leg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(122, 0).addBox(-5.5F, 36.8132F, -8.3384F, 11.0F, 5.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(-13.5F, -17.6296F, -3.9585F));

		PartDefinition cube_r15 = right_leg.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(0, 140).addBox(-5.0F, -15.8164F, -6.2969F, 9.0F, 23.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 31.6296F, 1.9585F, 0.1745F, 0.0F, 0.0F));

		PartDefinition cube_r16 = right_leg.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(66, 93).addBox(-6.5F, -12.2539F, -9.7969F, 13.0F, 22.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 9.6296F, 1.9585F, -0.1745F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void setupAnim(AVARobotEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		setRotationsDeg(rotatable, rotatable.xRot, 0, ageInTicks % 360.0F * 7.0F);
		setRotationsDeg(rotatable2, rotatable2.xRot, 0, ageInTicks % 360.0F * 7.0F);
		super.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
	}

	@Override
	public void renderToBuffer(PoseStack stack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		super.renderToBuffer(stack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
//		head.render(stack, buffer, packedLight, packedOverlay);
//		body.render(stack, buffer, packedLight, packedOverlay);
//		leftArm.render(stack, buffer, packedLight, packedOverlay);
//		rightArm.render(stack, buffer, packedLight, packedOverlay);
//		leftLeg.render(stack, buffer, packedLight, packedOverlay);
//		rightLeg.render(stack, buffer, packedLight, packedOverlay);
	}

	@Override
	protected void translateScaleOffset(PoseStack stack)
	{
		stack.translate(0.0F, 2.0F, 0.0F);
	}
}