package pellucid.ava.entities.robots.renderers;


import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.entities.robots.AVARobotEntity;

import javax.annotation.Nullable;

public class YellowRobotRenderer extends RobotRenderer<YellowRobotModel>
{
    public YellowRobotRenderer(EntityRendererProvider.Context context)
    {
        super(context, new YellowRobotModel(context.bakeLayer(AVAModelLayers.YELLOW_ROBOT)), RobotRenderer.YELLOW);
    }

    @Override
    public ResourceLocation getTextureLocation(AVARobotEntity entity)
    {
//        System.out.println(entity.isInvisible());
        return entity.isInvisible() ? RobotRenderer.YELLOW_INVIS : RobotRenderer.YELLOW;
    }

    @Nullable
    @Override
    protected RenderType getRenderType(AVARobotEntity entity, boolean visible, boolean visibleToLocal, boolean glowing)
    {
        return RenderType.itemEntityTranslucentCull(getTextureLocation(entity));
    }
}
