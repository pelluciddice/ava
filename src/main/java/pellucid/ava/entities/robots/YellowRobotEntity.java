package pellucid.ava.entities.robots;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.DataTypes;

public class YellowRobotEntity extends AVARobotEntity
{
    public YellowRobotEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public int getAmbientSoundInterval()
    {
        return 50;
    }

    @Override
    protected @Nullable SoundEvent getAmbientSound()
    {
        return AVASounds.YELLOW_ROBOT_AMBIENT.get();
    }

    private boolean invisSet = false;
    @Override
    public void tick()
    {
        super.tick();
        if (!invisSet)
        {
            if (random.nextFloat() <= 0.4F)
                setInvisible(true);
            invisSet = true;
        }
        if (swinging && isInvisible())
            setInvisible(false);
    }

    @Override
    protected float getMovementSpeedFactor()
    {
        return super.getMovementSpeedFactor() / 1.2F;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.BOOLEAN.write(compound, "invisSet", invisSet);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        invisSet = DataTypes.BOOLEAN.read(compound, "invisSet");
    }
}
