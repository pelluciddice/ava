package pellucid.ava.entities.objects.c4;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.joml.Matrix4f;
import pellucid.ava.AVA;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

public class C4Renderer extends EntityRenderer<C4Entity>
{
    private static final ResourceLocation RED_MARK = new ResourceLocation(AVA.MODID, "textures/entities/c4_mark_red.png");
    public static final ResourceLocation BLUE_MARK = new ResourceLocation(AVA.MODID, "textures/entities/c4_mark_blue.png");

    public C4Renderer(EntityRendererProvider.Context context)
    {
        super(context);
    }

    private static ItemStack C4_STACK;
    @Override
    public void render(C4Entity entity, float entityYaw, float partialTicks, PoseStack stack, MultiBufferSource bufferIn, int packedLightIn)
    {
        if (C4_STACK == null)
            C4_STACK = new ItemStack(MiscItems.C4.get());
        Minecraft minecraft = Minecraft.getInstance();
        stack.pushPose();
        stack.scale(0.5F, 0.5F, 0.5F);
        stack.mulPose(Axis.YP.rotationDegrees(Mth.lerp(partialTicks, entity.yRotO, entity.getYRot()) - 180.0F));
        stack.mulPose(Axis.XP.rotationDegrees(Mth.lerp(partialTicks, entity.xRotO, entity.getXRot())));
        stack.translate(0.0F, 0.15F, 0.0F);
        minecraft.getItemRenderer().renderStatic(C4_STACK, ItemDisplayContext.HEAD, packedLightIn, OverlayTexture.NO_OVERLAY, stack, bufferIn, entity.level(), entity.getId());
        stack.popPose();
        stack.pushPose();
        float y = entity.getBbHeight() + 0.75F;
        stack.translate(0.0D, y, 0.0D);
        stack.mulPose(minecraft.getEntityRenderDispatcher().camera.rotation());
        stack.scale(-0.025F, -0.025F, 0.025F);
        Matrix4f matrix4f = stack.last().pose();
        Font font = minecraft.font;
        MutableComponent text;
        if (entity.defused())
            text = Component.translatable("ava.entity.nicknames.c4_defused");
        else
            text = Component.literal(entity.getDisplayTime());
        float x = (float) (-font.width(text) / 2);
        font.drawInBatch(text, x, 0, AVAConstants.AVA_HUD_TEXT_ORANGE, false, matrix4f, bufferIn, Font.DisplayMode.SEE_THROUGH, 0, packedLightIn);
        stack.popPose();

        Player player = minecraft.player;
        if (player != null && AVAClientUtil.isCompetitiveModeEnabled() && !entity.defused() && (AVAWeaponUtil.TeamSide.getSideFor(player) == AVAWeaponUtil.TeamSide.EU || entity.exposed()))
            drawMarkAndDistance(player, entity, stack, bufferIn, packedLightIn, RED_MARK, 0.0F);
    }

    public static void drawMarkAndDistance(Player player, Entity entity, PoseStack stack, MultiBufferSource bufferIn, int packedLightIn, ResourceLocation texture, float extraTranslation)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Font font = minecraft.font;
        stack.pushPose();
        stack.mulPose(minecraft.getEntityRenderDispatcher().camera.rotation());
        stack.mulPose(Axis.ZP.rotationDegrees(180));
        stack.translate(0.0F, -0.6F + extraTranslation, 0.0F);
        AVAClientUtil.drawTransparent(true);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, texture);
        draw(Tesselator.getInstance().getBuilder(), Tesselator.getInstance(), stack);

        stack.translate(0.0F, 0.3F, 0.0F);
        stack.scale(-0.015F, -0.015F, 0.015F);
        stack.mulPose(Axis.ZP.rotationDegrees(180));
        Component text = Component.literal(AVACommonUtil.roundEntityDistance(player, entity) + "m");
        float x = (float)(-font.width(text) / 2);
        font.drawInBatch(text, x, 0, AVAConstants.AVA_HUD_TEXT_GRAY, true, stack.last().pose(), bufferIn, Font.DisplayMode.SEE_THROUGH, 0, packedLightIn);

        AVAClientUtil.drawTransparent(false);
        stack.popPose();
    }

    private static void draw(BufferBuilder bufferbuilder, Tesselator tessellator, PoseStack stack)
    {
        Matrix4f stack4f = stack.last().pose();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        float size = 0.25F;
        bufferbuilder.vertex(stack4f, -size, +size, 0.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(stack4f, +size, +size, 0.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(stack4f, +size, -size, 0.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(stack4f, -size, -size, 0.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
    }

    @Override
    public ResourceLocation getTextureLocation(C4Entity entity)
    {
        return null;
    }
}
