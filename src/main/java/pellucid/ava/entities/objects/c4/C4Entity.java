package pellucid.ava.entities.objects.c4;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.blocks.IInteractable;
import pellucid.ava.blocks.SiteBlock;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.objects.ObjectEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.modes.DemolishMode;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class C4Entity extends ObjectEntity implements IInteractable<EntityHitResult>
{
    private static final EntityDataAccessor<Integer> DEFUSED = SynchedEntityData.defineId(C4Entity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Boolean> EXPOSED = SynchedEntityData.defineId(C4Entity.class, EntityDataSerializers.BOOLEAN);
    private Player setter = null;
    public UUID disarmingEntity;

    public C4Entity(EntityType<? extends Entity> entityTypeIn, Level worldIn)
    {
        super(AVAEntities.C4.get(), worldIn);
    }

    public C4Entity(Level worldIn, Player setter)
    {
        this(AVAEntities.C4.get(), worldIn);
        this.setter = setter;
        setPos(setter.getX(), setter.getY() + 0.5F, setter.getZ());
    }

    public Player getSetter()
    {
        return setter;
    }

    @Override
    protected void defineSynchedData(SynchedEntityData.Builder builder)
    {
        builder.define(DEFUSED, 0);
        builder.define(EXPOSED, false);
    }

    @Nullable
    public SidedSmartAIEntity getDisarmingEntity()
    {
        Entity entity;
        if (level() instanceof ServerLevel)
        {
            entity = ((ServerLevel) level()).getEntity(disarmingEntity);
            if (entity instanceof SidedSmartAIEntity)
                return (SidedSmartAIEntity) entity;
        }
        return null;
    }

    public boolean flash()
    {
        int i;
        if (rangeTravelled <= 200)
            i = 40;
        else if (rangeTravelled <= 400)
            i = 25;
        else if (rangeTravelled <= 600)
            i = 15;
        else if (rangeTravelled <= 700)
            i = 10;
        else
            i = 5;
        return rangeTravelled % i == 0;
    }

    @Override
    public boolean shouldRenderAtSqrDistance(double p_19883_)
    {
        return AVAServerConfig.isCompetitiveModeActivated() || super.shouldRenderAtSqrDistance(p_19883_);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (!level().isClientSide())
        {
            if (!defused())
            {
                if (flash())
                    playSound(AVASounds.C4_BEEPS.get(), 0.75F, 1.0F);
                if (rangeTravelled >= 800)
                {
                    explode();
                    remove(RemovalReason.DISCARDED);
                }
                if (!exposed() && AVAServerConfig.isCompetitiveModeActivated())
                {
                    List<LivingEntity> nrfs = level().getEntitiesOfClass(LivingEntity.class, getBoundingBox().inflate(5.0F), (entity) -> AVAWeaponUtil.TeamSide.getSideFor(entity) == AVAWeaponUtil.TeamSide.NRF);
                    Optional<LivingEntity> founderOpt = nrfs.stream().filter((player) -> AVAWeaponUtil.isInSight(player, this, true, true, true, true, true)).findFirst();
                    if (!nrfs.isEmpty() && founderOpt.isPresent())
                    {
                        getEntityData().set(EXPOSED, true);
                        LivingEntity founder = founderOpt.get();
                        for (LivingEntity nrf : nrfs)
                            if (nrf instanceof Player)
                                nrf.sendSystemMessage(Component.translatable("ava.chat.c4_found", founder.getDisplayName(), blockPosition().toString()));
                    }
                }
            }
            else
            {
                int defused = getEntityData().get(DEFUSED);
                getEntityData().set(DEFUSED, defused + 1);
                if (defused >= 400)
                    remove(RemovalReason.DISCARDED);
            }
        }
    }

    public float getTimeLeft()
    {
        return AVACommonUtil.round((800 - rangeTravelled) / 20.0F, 2);
    }

    public String getDisplayTime()
    {
        String[] time = String.valueOf(AVACommonUtil.round((800 - rangeTravelled) / 20.0F, 2)).split("\\.");
        return AVACommonUtil.fillTenth(time[0]) + ":" + AVACommonUtil.fillTenth(time[1]);
    }

    public boolean defused()
    {
        return getEntityData().get(DEFUSED) > 0;
    }

    public boolean exposed()
    {
        return getEntityData().get(EXPOSED);
    }

    private void explode()
    {
        AVAWeaponUtil.createExplosionEntity(this, 6.0F, 500.0F, setter, MiscItems.C4.get(),
                (entity, distance) -> {},
                (serverWorld, x, y, z) -> {
                    for (int i = 0; i < 15; i++)
                    {
                        serverWorld.sendParticles(ParticleTypes.LARGE_SMOKE, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 3, 0.0F, 0.0F, 0.0F, 0.35F);
                        serverWorld.sendParticles(ParticleTypes.FLAME, x - 0.5F + this.random.nextFloat(), this.getY() + 0.1F, z - 0.5F + this.random.nextFloat(), 3, 0.0F, 0.0F, 0.0F, 0.35F);
                    }
                }, AVASounds.C4_EXPLODE.get()
        );
        AVAWeaponUtil.forEachBlockPos(blockPosition(), 3, 2, (pos) -> {
            Block block = level().getBlockState(pos).getBlock();
            if (block instanceof SiteBlock)
                ((SiteBlock) block).trigger(level(), pos);
            return false;
        });
        updateDemolishMode(DemolishMode.DemolishEndReasons.TARGET_DESTROYED);
    }

    protected void updateDemolishMode(DemolishMode.DemolishEndReasons reason)
    {
        if (!level().isClientSide() && GameModes.DEMOLISH.isRunning())
            GameModes.DEMOLISH.nextRound(level(), reason);
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        if (setter != null)
            DataTypes.UUID.write(compound, "setter", setter.getUUID());
        DataTypes.INT.write(compound, "defused", getEntityData().get(DEFUSED));
        if (disarmingEntity != null)
            DataTypes.UUID.write(compound, "disarmingEntity", disarmingEntity);
        DataTypes.BOOLEAN.write(compound, "expodes", getEntityData().get(EXPOSED));
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        if (compound.contains("setter"))
            setter = level().getPlayerByUUID(DataTypes.UUID.read(compound, "setter"));
        getEntityData().set(DEFUSED, DataTypes.INT.read(compound, "defused"));
        if (compound.contains("disarmingEntity"))
            disarmingEntity = DataTypes.UUID.read(compound, "disarmingEntity");
        getEntityData().set(EXPOSED, DataTypes.BOOLEAN.read(compound, "exposed"));
    }

    @Override
    public int getDuration()
    {
        return 144;
    }

    @Override
    public boolean canInteract(EntityHitResult result, LivingEntity interactor, BlockPos pos)
    {
        return !defused() && (!AVAServerConfig.isCompetitiveModeActivated() || AVAWeaponUtil.TeamSide.getSideFor(interactor) == AVAWeaponUtil.TeamSide.NRF);
    }

    @Override
    public void interact(Level world, EntityHitResult result, @Nullable BlockPos pos, @Nullable Vec3 vector3d, LivingEntity interacter)
    {
        if (!world.isClientSide())
        {
            getEntityData().set(DEFUSED, 1);
            updateDemolishMode(DemolishMode.DemolishEndReasons.CHARGE_DEFUSED);
        }
    }

    @Override
    public double maxDistance()
    {
        return 1.75F;
    }

    @Override
    public boolean isPickable() {
        return true;
    }
}
