package pellucid.ava.entities.objects;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.base.AVAEntity;

public class ObjectEntity extends AVAEntity
{
    public ObjectEntity(EntityType<?> entityTypeIn, Level worldIn)
    {
        super(entityTypeIn, worldIn);
    }

    @Override
    public void tick()
    {
        super.tick();
        Vec3 vec3d = this.getDeltaMovement();
        this.setDeltaMovement(this.getDeltaMovement().add(0.0D, -0.04D, 0.0D));
        if (this.level().isClientSide)
            this.noPhysics = false;
        else
        {
            this.noPhysics = !this.level().noCollision(this);
            if (this.noPhysics)
                this.moveTowardsClosestSpace(this.getX(), (this.getBoundingBox().minY + this.getBoundingBox().maxY) / 2.0D, this.getZ());
        }
        if (!this.onGround() || getDeltaMovement().horizontalDistanceSqr() > (double)1.0E-5F || (this.tickCount + this.getId()) % 4 == 0)
        {
            this.move(MoverType.SELF, this.getDeltaMovement());
            if (this.onGround())
                this.setDeltaMovement(this.getDeltaMovement().multiply(1.0D, -0.5D, 1.0D));
        }
        if (!this.level().isClientSide) {
            double d0 = this.getDeltaMovement().subtract(vec3d).lengthSqr();
            if (d0 > 0.01D)
                this.hasImpulse = true;
        }
    }
}
