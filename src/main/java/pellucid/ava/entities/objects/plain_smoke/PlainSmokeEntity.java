package pellucid.ava.entities.objects.plain_smoke;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import pellucid.ava.client.renderers.environment.SmokeEffect;
import pellucid.ava.entities.base.AVAEntity;
import pellucid.ava.entities.functionalities.IVisionBlockingEntity;
import pellucid.ava.items.throwables.SmokeGrenade;
import pellucid.ava.util.DataTypes;

public class PlainSmokeEntity extends AVAEntity implements IEntityWithComplexSpawn, IVisionBlockingEntity
{
    public SmokeGrenade grenade;
    public SmokeEffect effect;

    public PlainSmokeEntity(EntityType<?> type, Level world)
    {
        super(type, world);
    }

    public PlainSmokeEntity(EntityType<?> type, Level world, SmokeGrenade grenade, Vec3 pos)
    {
        super(type, world);
        this.grenade = grenade;
        setPos(pos);
        effect = new SmokeEffect(getBoundingBox().getCenter(), grenade);
    }

    @Override
    public boolean shouldBeBlocking()
    {
        return effect.lerpLive(1.0F) >= 0.75F;
    }

    protected float getSize()
    {
        return (effect.getSize(1.0F) + effect.random) * 2.0F;
    }

    @Override
    public AABB getVisionBlockingBox()
    {
        float s = getSize();
        return getBoundingBox().inflate(s);
    }

    @Override
    public EntityDimensions getDimensions(Pose p_19975_)
    {
        return EntityDimensions.fixed(0, 0);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (effect != null)
            effect.tick();
        refreshDimensions();
        if (effect.shouldBeDead())
            discard();
    }

    @Override
    public boolean isNoGravity()
    {
        return true;
    }

    @Override
    public boolean isPickable()
    {
        return true;
    }

    @Override
    public boolean canBeCollidedWith()
    {
        return false;
    }

    @Override
    public boolean isPushable()
    {
        return false;
    }

    @Override
    public boolean isAttackable()
    {
        return false;
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.ITEM.write(compound, "grenade", grenade);
        compound.put("effect", effect.serializeNBT());
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        Item item = DataTypes.ITEM.read(compound, "grenade");
        grenade = item instanceof SmokeGrenade ? (SmokeGrenade) item : null;
        effect = new SmokeEffect();
        effect.deserializeNBT(DataTypes.COMPOUND.read(compound, "effect"));
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        writePlainSpawnData(buffer);
        DataTypes.ITEM.write(buffer, grenade);
        DataTypes.COMPOUND.write(buffer, effect.serializeNBT());
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        readPlainSpawnData(additionalData);
        Item item = DataTypes.ITEM.read(additionalData);
        grenade = item instanceof SmokeGrenade ? (SmokeGrenade) item : null;
        effect = new SmokeEffect();
        effect.deserializeNBT(DataTypes.COMPOUND.read(additionalData));
    }

    public enum Phase
    {
        GROW,
        STEADY,
        SHRINK,
        ;

        public Phase next()
        {
            return values()[(ordinal() + 1) % values().length];
        }
    }
}
