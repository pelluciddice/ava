package pellucid.ava.entities.objects.item;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.util.AVAClientUtil;

public class GunItemEntityRenderer extends EntityRenderer<GunItemEntity>
{
    public GunItemEntityRenderer(EntityRendererProvider.Context context)
    {
        super(context);
    }

    @Override
    public void render(GunItemEntity entity, float entityYaw, float partialTicks, PoseStack stack, MultiBufferSource bufferIn, int packedLightIn)
    {
        Minecraft minecraft = Minecraft.getInstance();
        stack.pushPose();
        stack.translate(0.0F, 0.035F, 0.0F);
        float scale = 0.6F;
        stack.scale(scale, scale, scale);
        stack.mulPose(Axis.YP.rotationDegrees(Mth.lerp(partialTicks, entity.yRotO, entity.getYRot())));
        stack.mulPose(Axis.XP.rotationDegrees(90.0F));
        AVAClientUtil.forceEnableFastAssets(() -> minecraft.getItemRenderer().renderStatic(entity.getStack(), ItemDisplayContext.FIXED, packedLightIn, OverlayTexture.NO_OVERLAY, stack, bufferIn, entity.level(), entity.getId()));
        stack.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(GunItemEntity entity)
    {
        return null;
    }
}
