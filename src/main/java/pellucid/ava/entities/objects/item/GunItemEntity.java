package pellucid.ava.entities.objects.item;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.objects.ObjectEntity;
import pellucid.ava.util.DataTypes;

public class GunItemEntity extends ObjectEntity implements IEntityWithComplexSpawn
{
    private ItemStack stack = ItemStack.EMPTY;
    public GunItemEntity(EntityType<?> type, Level world)
    {
        super(AVAEntities.GUN_ITEM.get(), world);
    }

    public GunItemEntity(Entity entity, ItemStack stack)
    {
        super(AVAEntities.GUN_ITEM.get(), entity.level());
        setYRot(entity.getYRot());
        setPos(entity.getX(), entity.getY() + entity.getBbHeight() / 2.0F, entity.getZ());
        this.stack = stack.copy();
    }

    @Override
    public boolean shouldRenderAtSqrDistance(double distance)
    {
        return Math.sqrt(distance) <= 20.0D;
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.COMPOUND.write(compound, "gun", (CompoundTag) stack.saveOptional(level().registryAccess()));
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        stack = ItemStack.parseOptional(level().registryAccess(), DataTypes.COMPOUND.read(compound, "gun"));
    }

    @Override
    public void tick()
    {
        super.tick();
        if (this.rangeTravelled > 400)
            this.remove(RemovalReason.DISCARDED);
    }

    @Override
    public boolean isPickable() {
        return false;
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        DataTypes.ITEMSTACK.write(buffer, stack);
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        stack = DataTypes.ITEMSTACK.read(additionalData);
    }

    public ItemStack getStack()
    {
        return stack;
    }
}
