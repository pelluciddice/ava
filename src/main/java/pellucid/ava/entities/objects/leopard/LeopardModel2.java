//// Made with Blockbench 4.9.3
//// Exported for Minecraft version 1.17 or later with Mojang mappings
//// Paste this class into your mod and generate all required imports
//
//
//import net.minecraft.client.model.geom.ModelPart;
//import net.minecraft.client.model.geom.builders.LayerDefinition;
//
//public class leopard<T extends Entity> extends EntityModel<T> {
//	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
//	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "leopard"), "main");
//	private final ModelPart wheel_left;
//	private final ModelPart wheel_right;
//	private final ModelPart wheel2_left;
//	private final ModelPart wheel2_right;
//	private final ModelPart wheel3_left;
//	private final ModelPart wheel3_right;
//	private final ModelPart wheel4_left;
//	private final ModelPart wheel4_right;
//	private final ModelPart wheel5_left;
//	private final ModelPart wheel5_right;
//	private final ModelPart wheel6_left;
//	private final ModelPart wheel6_right;
//	private final ModelPart wheel7_left;
//	private final ModelPart wheel7_right;
//	private final ModelPart wheel8_left;
//	private final ModelPart wheel8_right;
//	private final ModelPart wheel9_left;
//	private final ModelPart wheel9_right;
//	private final ModelPart belt_left;
//	private final ModelPart belt_right;
//	private final ModelPart body_left;
//	private final ModelPart body_right;
//	private final ModelPart body;
//	private final ModelPart turret;
//
//	public leopard(ModelPart root) {
//		this.wheel_left = root.getChild("wheel_left");
//		this.wheel_right = root.getChild("wheel_right");
//		this.wheel2_left = root.getChild("wheel2_left");
//		this.wheel2_right = root.getChild("wheel2_right");
//		this.wheel3_left = root.getChild("wheel3_left");
//		this.wheel3_right = root.getChild("wheel3_right");
//		this.wheel4_left = root.getChild("wheel4_left");
//		this.wheel4_right = root.getChild("wheel4_right");
//		this.wheel5_left = root.getChild("wheel5_left");
//		this.wheel5_right = root.getChild("wheel5_right");
//		this.wheel6_left = root.getChild("wheel6_left");
//		this.wheel6_right = root.getChild("wheel6_right");
//		this.wheel7_left = root.getChild("wheel7_left");
//		this.wheel7_right = root.getChild("wheel7_right");
//		this.wheel8_left = root.getChild("wheel8_left");
//		this.wheel8_right = root.getChild("wheel8_right");
//		this.wheel9_left = root.getChild("wheel9_left");
//		this.wheel9_right = root.getChild("wheel9_right");
//		this.belt_left = root.getChild("belt_left");
//		this.belt_right = root.getChild("belt_right");
//		this.body_left = root.getChild("body_left");
//		this.body_right = root.getChild("body_right");
//		this.body = root.getChild("body");
//		this.turret = root.getChild("turret");
//	}
//
//	public static LayerDefinition createBodyLayer() {
//		MeshDefinition meshdefinition = new MeshDefinition();
//		PartDefinition partdefinition = meshdefinition.getRoot();
//
//		PartDefinition wheel_left = partdefinition.addOrReplaceChild("wheel_left", CubeListBuilder.create().texOffs(35, 139).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(144, 126).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));
//
//		PartDefinition hexadecagon_r1 = wheel_left.addOrReplaceChild("hexadecagon_r1", CubeListBuilder.create().texOffs(44, 144).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(25, 139).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r2 = wheel_left.addOrReplaceChild("hexadecagon_r2", CubeListBuilder.create().texOffs(131, 144).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(139, 60).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r3 = wheel_left.addOrReplaceChild("hexadecagon_r3", CubeListBuilder.create().texOffs(138, 113).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r4 = wheel_left.addOrReplaceChild("hexadecagon_r4", CubeListBuilder.create().texOffs(107, 139).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel_right = partdefinition.addOrReplaceChild("wheel_right", CubeListBuilder.create().texOffs(137, 133).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(63, 132).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));
//
//		PartDefinition hexadecagon_r5 = wheel_right.addOrReplaceChild("hexadecagon_r5", CubeListBuilder.create().texOffs(132, 50).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(137, 129).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r6 = wheel_right.addOrReplaceChild("hexadecagon_r6", CubeListBuilder.create().texOffs(0, 133).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(138, 10).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r7 = wheel_right.addOrReplaceChild("hexadecagon_r7", CubeListBuilder.create().texOffs(118, 137).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r8 = wheel_right.addOrReplaceChild("hexadecagon_r8", CubeListBuilder.create().texOffs(74, 138).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel2_left = partdefinition.addOrReplaceChild("wheel2_left", CubeListBuilder.create().texOffs(27, 143).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(147, 71).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 4.0F));
//
//		PartDefinition hexadecagon_r9 = wheel2_left.addOrReplaceChild("hexadecagon_r9", CubeListBuilder.create().texOffs(147, 28).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(17, 143).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r10 = wheel2_left.addOrReplaceChild("hexadecagon_r10", CubeListBuilder.create().texOffs(74, 147).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(143, 35).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r11 = wheel2_left.addOrReplaceChild("hexadecagon_r11", CubeListBuilder.create().texOffs(142, 141).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r12 = wheel2_left.addOrReplaceChild("hexadecagon_r12", CubeListBuilder.create().texOffs(143, 39).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel2_right = partdefinition.addOrReplaceChild("wheel2_right", CubeListBuilder.create().texOffs(111, 135).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(30, 119).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 4.0F));
//
//		PartDefinition hexadecagon_r13 = wheel2_right.addOrReplaceChild("hexadecagon_r13", CubeListBuilder.create().texOffs(65, 115).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(135, 70).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r14 = wheel2_right.addOrReplaceChild("hexadecagon_r14", CubeListBuilder.create().texOffs(128, 131).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(136, 103).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r15 = wheel2_right.addOrReplaceChild("hexadecagon_r15", CubeListBuilder.create().texOffs(135, 66).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r16 = wheel2_right.addOrReplaceChild("hexadecagon_r16", CubeListBuilder.create().texOffs(136, 107).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel3_left = partdefinition.addOrReplaceChild("wheel3_left", CubeListBuilder.create().texOffs(66, 142).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(18, 147).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 8.0F));
//
//		PartDefinition hexadecagon_r17 = wheel3_left.addOrReplaceChild("hexadecagon_r17", CubeListBuilder.create().texOffs(147, 9).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(142, 14).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r18 = wheel3_left.addOrReplaceChild("hexadecagon_r18", CubeListBuilder.create().texOffs(24, 147).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(142, 67).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r19 = wheel3_left.addOrReplaceChild("hexadecagon_r19", CubeListBuilder.create().texOffs(10, 142).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r20 = wheel3_left.addOrReplaceChild("hexadecagon_r20", CubeListBuilder.create().texOffs(142, 137).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel3_right = partdefinition.addOrReplaceChild("wheel3_right", CubeListBuilder.create().texOffs(135, 16).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(112, 79).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 8.0F));
//
//		PartDefinition hexadecagon_r21 = wheel3_right.addOrReplaceChild("hexadecagon_r21", CubeListBuilder.create().texOffs(18, 105).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(134, 121).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r22 = wheel3_right.addOrReplaceChild("hexadecagon_r22", CubeListBuilder.create().texOffs(56, 115).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(29, 135).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r23 = wheel3_right.addOrReplaceChild("hexadecagon_r23", CubeListBuilder.create().texOffs(134, 117).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r24 = wheel3_right.addOrReplaceChild("hexadecagon_r24", CubeListBuilder.create().texOffs(39, 135).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel4_left = partdefinition.addOrReplaceChild("wheel4_left", CubeListBuilder.create().texOffs(141, 118).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(146, 78).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 12.0F));
//
//		PartDefinition hexadecagon_r25 = wheel4_left.addOrReplaceChild("hexadecagon_r25", CubeListBuilder.create().texOffs(68, 146).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(114, 141).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r26 = wheel4_left.addOrReplaceChild("hexadecagon_r26", CubeListBuilder.create().texOffs(146, 132).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(141, 122).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r27 = wheel4_left.addOrReplaceChild("hexadecagon_r27", CubeListBuilder.create().texOffs(98, 141).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r28 = wheel4_left.addOrReplaceChild("hexadecagon_r28", CubeListBuilder.create().texOffs(0, 142).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel4_right = partdefinition.addOrReplaceChild("wheel4_right", CubeListBuilder.create().texOffs(90, 133).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(33, 98).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 12.0F));
//
//		PartDefinition hexadecagon_r29 = wheel4_right.addOrReplaceChild("hexadecagon_r29", CubeListBuilder.create().texOffs(52, 97).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(78, 133).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r30 = wheel4_right.addOrReplaceChild("hexadecagon_r30", CubeListBuilder.create().texOffs(66, 100).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(22, 134).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r31 = wheel4_right.addOrReplaceChild("hexadecagon_r31", CubeListBuilder.create().texOffs(133, 30).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r32 = wheel4_right.addOrReplaceChild("hexadecagon_r32", CubeListBuilder.create().texOffs(104, 134).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel5_left = partdefinition.addOrReplaceChild("wheel5_left", CubeListBuilder.create().texOffs(59, 141).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(12, 146).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 16.0F));
//
//		PartDefinition hexadecagon_r33 = wheel5_left.addOrReplaceChild("hexadecagon_r33", CubeListBuilder.create().texOffs(6, 146).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(49, 141).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r34 = wheel5_left.addOrReplaceChild("hexadecagon_r34", CubeListBuilder.create().texOffs(62, 146).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(88, 141).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r35 = wheel5_left.addOrReplaceChild("hexadecagon_r35", CubeListBuilder.create().texOffs(141, 24).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r36 = wheel5_left.addOrReplaceChild("hexadecagon_r36", CubeListBuilder.create().texOffs(141, 95).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel5_right = partdefinition.addOrReplaceChild("wheel5_right", CubeListBuilder.create().texOffs(132, 59).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 93).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 16.0F));
//
//		PartDefinition hexadecagon_r37 = wheel5_right.addOrReplaceChild("hexadecagon_r37", CubeListBuilder.create().texOffs(19, 92).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(125, 116).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r38 = wheel5_right.addOrReplaceChild("hexadecagon_r38", CubeListBuilder.create().texOffs(66, 93).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(71, 132).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r39 = wheel5_right.addOrReplaceChild("hexadecagon_r39", CubeListBuilder.create().texOffs(64, 125).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r40 = wheel5_right.addOrReplaceChild("hexadecagon_r40", CubeListBuilder.create().texOffs(8, 133).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel6_left = partdefinition.addOrReplaceChild("wheel6_left", CubeListBuilder.create().texOffs(125, 140).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(143, 145).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 20.0F));
//
//		PartDefinition hexadecagon_r41 = wheel6_left.addOrReplaceChild("hexadecagon_r41", CubeListBuilder.create().texOffs(115, 145).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(81, 140).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r42 = wheel6_left.addOrReplaceChild("hexadecagon_r42", CubeListBuilder.create().texOffs(0, 146).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(135, 140).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r43 = wheel6_left.addOrReplaceChild("hexadecagon_r43", CubeListBuilder.create().texOffs(140, 74).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r44 = wheel6_left.addOrReplaceChild("hexadecagon_r44", CubeListBuilder.create().texOffs(141, 20).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel6_right = partdefinition.addOrReplaceChild("wheel6_right", CubeListBuilder.create().texOffs(90, 115).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(66, 89).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 20.0F));
//
//		PartDefinition hexadecagon_r45 = wheel6_right.addOrReplaceChild("hexadecagon_r45", CubeListBuilder.create().texOffs(33, 89).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(112, 28).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r46 = wheel6_right.addOrReplaceChild("hexadecagon_r46", CubeListBuilder.create().texOffs(92, 2).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(124, 95).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r47 = wheel6_right.addOrReplaceChild("hexadecagon_r47", CubeListBuilder.create().texOffs(91, 111).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r48 = wheel6_right.addOrReplaceChild("hexadecagon_r48", CubeListBuilder.create().texOffs(47, 125).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel7_left = partdefinition.addOrReplaceChild("wheel7_left", CubeListBuilder.create().texOffs(140, 31).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(96, 145).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 24.0F));
//
//		PartDefinition hexadecagon_r49 = wheel7_left.addOrReplaceChild("hexadecagon_r49", CubeListBuilder.create().texOffs(90, 145).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(140, 4).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r50 = wheel7_left.addOrReplaceChild("hexadecagon_r50", CubeListBuilder.create().texOffs(145, 112).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(42, 140).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r51 = wheel7_left.addOrReplaceChild("hexadecagon_r51", CubeListBuilder.create().texOffs(140, 0).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r52 = wheel7_left.addOrReplaceChild("hexadecagon_r52", CubeListBuilder.create().texOffs(140, 50).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel7_right = partdefinition.addOrReplaceChild("wheel7_right", CubeListBuilder.create().texOffs(48, 104).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(86, 54).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 24.0F));
//
//		PartDefinition hexadecagon_r53 = wheel7_right.addOrReplaceChild("hexadecagon_r53", CubeListBuilder.create().texOffs(86, 50).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(102, 79).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r54 = wheel7_right.addOrReplaceChild("hexadecagon_r54", CubeListBuilder.create().texOffs(86, 58).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(33, 110).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r55 = wheel7_right.addOrReplaceChild("hexadecagon_r55", CubeListBuilder.create().texOffs(102, 28).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r56 = wheel7_right.addOrReplaceChild("hexadecagon_r56", CubeListBuilder.create().texOffs(43, 110).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel8_left = partdefinition.addOrReplaceChild("wheel8_left", CubeListBuilder.create().texOffs(11, 93).addBox(8.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(50, 145).addBox(8.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 22.0F, -3.8F));
//
//		PartDefinition hexadecagon_r57 = wheel8_left.addOrReplaceChild("hexadecagon_r57", CubeListBuilder.create().texOffs(137, 144).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(92, 28).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r58 = wheel8_left.addOrReplaceChild("hexadecagon_r58", CubeListBuilder.create().texOffs(56, 145).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(44, 98).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r59 = wheel8_left.addOrReplaceChild("hexadecagon_r59", CubeListBuilder.create().texOffs(46, 89).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r60 = wheel8_left.addOrReplaceChild("hexadecagon_r60", CubeListBuilder.create().texOffs(15, 99).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel8_right = partdefinition.addOrReplaceChild("wheel8_right", CubeListBuilder.create().texOffs(69, 22).addBox(-10.0F, -1.2984F, -13.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(70, 51).addBox(-10.0F, -2.5F, -12.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 22.0F, -3.8F));
//
//		PartDefinition hexadecagon_r61 = wheel8_right.addOrReplaceChild("hexadecagon_r61", CubeListBuilder.create().texOffs(11, 51).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(7, 63).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r62 = wheel8_right.addOrReplaceChild("hexadecagon_r62", CubeListBuilder.create().texOffs(70, 62).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(80, 28).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r63 = wheel8_right.addOrReplaceChild("hexadecagon_r63", CubeListBuilder.create().texOffs(0, 43).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r64 = wheel8_right.addOrReplaceChild("hexadecagon_r64", CubeListBuilder.create().texOffs(86, 41).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -1.0F, -12.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel9_left = partdefinition.addOrReplaceChild("wheel9_left", CubeListBuilder.create().texOffs(80, 144).addBox(1.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(30, 148).addBox(1.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(7.0F, 20.9F, 15.1F));
//
//		PartDefinition hexadecagon_r65 = wheel9_left.addOrReplaceChild("hexadecagon_r65", CubeListBuilder.create().texOffs(148, 18).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(34, 144).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r66 = wheel9_left.addOrReplaceChild("hexadecagon_r66", CubeListBuilder.create().texOffs(36, 148).addBox(6.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(144, 100).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r67 = wheel9_left.addOrReplaceChild("hexadecagon_r67", CubeListBuilder.create().texOffs(143, 108).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r68 = wheel9_left.addOrReplaceChild("hexadecagon_r68", CubeListBuilder.create().texOffs(121, 144).addBox(6.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition wheel9_right = partdefinition.addOrReplaceChild("wheel9_right", CubeListBuilder.create().texOffs(73, 143).addBox(-17.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(102, 147).addBox(-17.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F)), PartPose.offset(7.0F, 20.9F, 15.1F));
//
//		PartDefinition hexadecagon_r69 = wheel9_right.addOrReplaceChild("hexadecagon_r69", CubeListBuilder.create().texOffs(147, 89).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(143, 56).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, 0.0F, 0.0F, -0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r70 = wheel9_right.addOrReplaceChild("hexadecagon_r70", CubeListBuilder.create().texOffs(108, 147).addBox(-8.0F, -1.5F, -0.2984F, 2.0F, 3.0F, 0.5967F, new CubeDeformation(0.0F))
//		.texOffs(143, 104).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, 0.0F, 0.0F, 0.3927F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r71 = wheel9_right.addOrReplaceChild("hexadecagon_r71", CubeListBuilder.create().texOffs(143, 43).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, 0.0F, 0.0F, -0.7854F, 0.0F, 0.0F));
//
//		PartDefinition hexadecagon_r72 = wheel9_right.addOrReplaceChild("hexadecagon_r72", CubeListBuilder.create().texOffs(105, 143).addBox(-8.0F, -0.2984F, -1.5F, 2.0F, 0.5967F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, 0.0F, 0.0F, 0.7854F, 0.0F, 0.0F));
//
//		PartDefinition belt_left = partdefinition.addOrReplaceChild("belt_left", CubeListBuilder.create().texOffs(33, 89).addBox(7.0F, 0.5F, -12.6F, 4.0F, 1.0F, 25.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));
//
//		PartDefinition cube_r1 = belt_left.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(107, 67).addBox(-2.0F, -0.3242F, -1.2656F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -3.665F, -17.7243F, -2.2253F, 0.0F, 0.0F));
//
//		PartDefinition cube_r2 = belt_left.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(66, 111).addBox(-2.0F, -0.3711F, -1.3125F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -2.8439F, -17.7743F, -1.789F, 0.0F, 0.0F));
//
//		PartDefinition cube_r3 = belt_left.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(76, 111).addBox(-2.0F, -0.3477F, -1.2734F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -2.0867F, -17.5551F, -1.4835F, 0.0F, 0.0F));
//
//		PartDefinition cube_r4 = belt_left.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(115, 95).addBox(-2.0F, -0.3516F, -1.2461F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -1.4934F, -17.0789F, -1.0908F, 0.0F, 0.0F));
//
//		PartDefinition cube_r5 = belt_left.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(116, 66).addBox(-2.0F, -0.0742F, -3.3477F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -0.0977F, -14.5605F, -0.6545F, 0.0F, 0.0F));
//
//		PartDefinition cube_r6 = belt_left.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(108, 128).addBox(-2.0F, -0.1484F, -1.6406F, 4.0F, 1.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -0.8063F, -15.1605F, -0.48F, 0.0F, 0.0F));
//
//		PartDefinition cube_r7 = belt_left.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(90, 119).addBox(-2.0F, -0.5391F, -0.4609F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -4.8038F, 16.3464F, 2.5307F, 0.0F, 0.0F));
//
//		PartDefinition cube_r8 = belt_left.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(125, 67).addBox(-2.0F, -0.3438F, 0.2852F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -3.4802F, 16.8768F, 1.9635F, 0.0F, 0.0F));
//
//		PartDefinition cube_r9 = belt_left.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(64, 129).addBox(-2.0F, -0.5352F, -0.5F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -3.4802F, 16.9119F, 1.5708F, 0.0F, 0.0F));
//
//		PartDefinition cube_r10 = belt_left.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(128, 129).addBox(-2.0F, -0.8984F, -0.832F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -2.8311F, 17.2481F, 1.3526F, 0.0F, 0.0F));
//
//		PartDefinition cube_r11 = belt_left.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(8, 137).addBox(-2.0F, -0.6406F, -0.625F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -1.8922F, 16.65F, 0.9599F, 0.0F, 0.0F));
//
//		PartDefinition cube_r12 = belt_left.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(95, 127).addBox(-2.0F, -0.3945F, 2.2305F, 4.0F, 1.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, 2.1813F, 10.18F, 0.5672F, 0.0F, 0.0F));
//
//		PartDefinition belt_right = partdefinition.addOrReplaceChild("belt_right", CubeListBuilder.create().texOffs(0, 84).addBox(-11.0F, 0.5F, -12.6F, 4.0F, 1.0F, 25.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));
//
//		PartDefinition cube_r13 = belt_right.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(43, 84).addBox(-2.0F, -0.3242F, -1.2656F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -3.665F, -17.7243F, -2.2253F, 0.0F, 0.0F));
//
//		PartDefinition cube_r14 = belt_right.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(86, 45).addBox(-2.0F, -0.3711F, -1.3125F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -2.8439F, -17.7743F, -1.789F, 0.0F, 0.0F));
//
//		PartDefinition cube_r15 = belt_right.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(88, 0).addBox(-2.0F, -0.3477F, -1.2734F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -2.0867F, -17.5551F, -1.4835F, 0.0F, 0.0F));
//
//		PartDefinition cube_r16 = belt_right.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(88, 68).addBox(-2.0F, -0.3516F, -1.2461F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -1.4934F, -17.0789F, -1.0908F, 0.0F, 0.0F));
//
//		PartDefinition cube_r17 = belt_right.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(13, 89).addBox(-2.0F, -0.0742F, -3.3477F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -0.0977F, -14.5605F, -0.6545F, 0.0F, 0.0F));
//
//		PartDefinition cube_r18 = belt_right.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(57, 62).addBox(-2.0F, -0.1484F, -1.6406F, 4.0F, 1.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -0.8063F, -15.1605F, -0.48F, 0.0F, 0.0F));
//
//		PartDefinition cube_r19 = belt_right.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(46, 93).addBox(-2.0F, -0.5391F, -0.4609F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -4.8038F, 16.3464F, 2.5307F, 0.0F, 0.0F));
//
//		PartDefinition cube_r20 = belt_right.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(11, 97).addBox(-2.0F, -0.3438F, 0.2852F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -3.4802F, 16.8768F, 1.9635F, 0.0F, 0.0F));
//
//		PartDefinition cube_r21 = belt_right.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(44, 102).addBox(-2.0F, -0.5352F, -0.5F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -3.4802F, 16.9119F, 1.5708F, 0.0F, 0.0F));
//
//		PartDefinition cube_r22 = belt_right.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(102, 83).addBox(-2.0F, -0.8984F, -0.832F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -2.8311F, 17.2481F, 1.3526F, 0.0F, 0.0F));
//
//		PartDefinition cube_r23 = belt_right.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(15, 103).addBox(-2.0F, -0.6406F, -0.625F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -1.8922F, 16.65F, 0.9599F, 0.0F, 0.0F));
//
//		PartDefinition cube_r24 = belt_right.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(82, 125).addBox(-2.0F, -0.3945F, 2.2305F, 4.0F, 1.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, 2.1813F, 10.18F, 0.5672F, 0.0F, 0.0F));
//
//		PartDefinition body_left = partdefinition.addOrReplaceChild("body_left", CubeListBuilder.create().texOffs(61, 3).addBox(11.0F, -7.0F, -16.0F, 1.0F, 4.0F, 29.0F, new CubeDeformation(0.0F))
//		.texOffs(14, 134).addBox(10.9961F, -7.0F, 12.7266F, 1.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
//		.texOffs(33, 107).addBox(7.0F, -7.0F, 17.7266F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(12, 23).addBox(6.9844F, -7.9352F, -18.2766F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));
//
//		PartDefinition cube_r25 = body_left.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(33, 84).addBox(-4.5F, -1.6094F, -2.4492F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(84, 22).addBox(-0.5F, -1.6094F, -2.4492F, 1.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(11.5F, -5.211F, -17.1996F, -0.6109F, 0.0F, 0.0F));
//
//		PartDefinition cube_r26 = body_left.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(17, 11).addBox(-0.5078F, -2.0234F, -1.4336F, 1.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(11.5F, -5.211F, -17.1996F, -0.1745F, 0.0F, 0.0F));
//
//		PartDefinition cube_r27 = body_left.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(133, 22).addBox(-0.5039F, -0.8516F, -3.0F, 1.0F, 2.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(11.4961F, -5.0F, 15.5F, 0.3054F, 0.0F, 0.0F));
//
//		PartDefinition cube_r28 = body_left.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(86, 36).addBox(-2.0F, -1.5F, -0.5F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(9.0F, -4.5F, 18.5266F, 0.1745F, 0.0F, 0.0F));
//
//		PartDefinition body_right = partdefinition.addOrReplaceChild("body_right", CubeListBuilder.create().texOffs(47, 41).addBox(-12.0F, -7.0F, -16.0F, 1.0F, 4.0F, 29.0F, new CubeDeformation(0.0F))
//		.texOffs(96, 133).addBox(-11.9961F, -7.0F, 12.7266F, 1.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
//		.texOffs(78, 68).addBox(-11.0F, -7.0F, 17.7266F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 22).addBox(-11.9922F, -7.9352F, -18.2766F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 0).addBox(-6.9711F, -6.5091F, -12.7983F, 14.0F, 6.0F, 26.0F, new CubeDeformation(0.0F))
//		.texOffs(66, 100).addBox(-6.9672F, -6.5091F, 11.3017F, 7.0F, 4.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(66, 100).mirror().addBox(-0.0328F, -6.5091F, 11.3017F, 7.0F, 4.0F, 7.0F, new CubeDeformation(0.0F)).mirror(false)
//		.texOffs(123, 120).addBox(-1.0F, -5.5091F, 11.8017F, 2.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(33, 98).addBox(-6.8F, -5.5091F, 11.8017F, 2.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 32).addBox(-4.3F, -5.5091F, 11.8017F, 3.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 32).addBox(1.3F, -5.5091F, 11.8017F, 3.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 93).addBox(4.8F, -5.5091F, 11.8017F, 2.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(119, 129).addBox(3.9F, -2.8091F, 11.5017F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(56, 115).addBox(-4.9F, -2.8091F, 11.5017F, 1.0F, 1.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 110).addBox(0.025F, -4.8661F, 12.1572F, 7.0F, 4.0F, 5.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 110).addBox(-6.9672F, -4.8661F, 12.1572F, 7.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));
//
//		PartDefinition cube_r29 = body_right.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(115, 89).addBox(-7.0F, 0.0117F, -1.6328F, 14.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0289F, -4.5091F, 14.8017F, 0.9599F, 0.0F, 0.0F));
//
//		PartDefinition cube_r30 = body_right.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(114, 79).addBox(-7.0F, -6.3961F, -3.4617F, 14.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -3.4567F, -18.5029F, -1.9635F, 0.0F, 0.0F));
//
//		PartDefinition cube_r31 = body_right.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(101, 100).addBox(-7.0F, -2.1375F, -1.475F, 14.0F, 2.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -6.009F, -19.0204F, -2.0508F, 0.0F, 0.0F));
//
//		PartDefinition cube_r32 = body_right.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(78, 66).addBox(4.5F, -1.6094F, -2.4453F, 14.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 119).addBox(4.5F, -1.1094F, -2.4492F, 14.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 61).addBox(0.5F, -1.6094F, -2.4492F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 84).addBox(-0.5F, -1.6094F, -2.4492F, 1.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-11.5F, -5.211F, -17.1996F, -0.6109F, 0.0F, 0.0F));
//
//		PartDefinition cube_r33 = body_right.addOrReplaceChild("cube_r33", CubeListBuilder.create().texOffs(0, 11).addBox(-0.4922F, -2.0234F, -1.4336F, 1.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-11.5F, -5.211F, -17.1996F, -0.1745F, 0.0F, 0.0F));
//
//		PartDefinition cube_r34 = body_right.addOrReplaceChild("cube_r34", CubeListBuilder.create().texOffs(133, 22).addBox(-0.4922F, -0.8516F, -3.0F, 1.0F, 2.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-11.5F, -5.0F, 15.5F, 0.3054F, 0.0F, 0.0F));
//
//		PartDefinition cube_r35 = body_right.addOrReplaceChild("cube_r35", CubeListBuilder.create().texOffs(13, 84).addBox(-2.0F, -1.5F, -0.5F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-9.0F, -4.5F, 18.5266F, 0.1745F, 0.0F, 0.0F));
//
//		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(44, 74).addBox(-11.4961F, -1.425F, 2.5852F, 4.0F, 3.0F, 12.0F, new CubeDeformation(0.0F))
//		.texOffs(44, 74).mirror().addBox(7.4094F, -1.425F, 2.5852F, 4.0F, 3.0F, 12.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.1F, 16.0648F, -9.9766F));
//
//		PartDefinition cube_r36 = body.addOrReplaceChild("cube_r36", CubeListBuilder.create().texOffs(86, 36).addBox(-11.5F, -1.1328F, -5.5F, 23.0F, 3.0F, 11.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.4023F, -2.7148F, 0.1309F, 0.0F, 0.0F));
//
//		PartDefinition cube_r37 = body.addOrReplaceChild("cube_r37", CubeListBuilder.create().texOffs(0, 32).addBox(-11.5F, -1.2305F, 5.1172F, 23.0F, 4.0F, 15.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.075F, 8.5852F, 0.0436F, 0.0F, 0.0F));
//
//		PartDefinition turret = partdefinition.addOrReplaceChild("turret", CubeListBuilder.create().texOffs(92, 19).addBox(-10.0F, -5.4275F, 7.9432F, 20.0F, 6.0F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 0).addBox(-6.5F, -6.673F, 1.0828F, 6.0F, 4.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(92, 10).addBox(-10.0F, -5.4275F, -0.0568F, 20.0F, 6.0F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(57, 51).addBox(6.0F, -5.4275F, 2.9432F, 4.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
//		.texOffs(98, 116).addBox(-10.0F, -5.4275F, 2.9432F, 11.0F, 6.0F, 5.0F, new CubeDeformation(0.0F))
//		.texOffs(102, 69).addBox(-7.0F, -4.4666F, 11.4354F, 14.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
//		.texOffs(69, 36).addBox(-7.0F, -4.4666F, 16.4378F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(61, 36).addBox(-4.6F, -4.4666F, 16.4378F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(69, 9).addBox(-2.2F, -4.4666F, 16.4378F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(8, 32).addBox(0.2F, -4.4666F, 16.4378F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(69, 0).addBox(2.6F, -4.4666F, 16.4378F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 32).addBox(5.0F, -4.4666F, 16.4378F, 2.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 51).addBox(-0.0078F, -1.4275F, -5.7911F, 10.0F, 2.0F, 17.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 51).mirror().addBox(-9.9961F, -1.4275F, -5.7911F, 10.0F, 2.0F, 17.0F, new CubeDeformation(0.0F)).mirror(false)
//		.texOffs(78, 50).addBox(2.9922F, -3.4275F, -2.7911F, 7.0F, 2.0F, 14.0F, new CubeDeformation(0.0F))
//		.texOffs(78, 50).addBox(-9.9922F, -3.4275F, -2.7911F, 7.0F, 2.0F, 14.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 70).addBox(1.9961F, -4.4705F, 10.2089F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 70).mirror().addBox(-9.9961F, -4.4705F, 10.2089F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false)
//		.texOffs(49, 132).addBox(-2.0F, -5.451F, -2.7911F, 4.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 13.6355F, -2.2929F));
//
//		PartDefinition cube_r38 = turret.addOrReplaceChild("cube_r38", CubeListBuilder.create().texOffs(30, 115).addBox(-8.5F, -1.0F, -4.9F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.6791F, -6.4763F, 0.4363F, 0.5236F, 0.0F));
//
//		PartDefinition cube_r39 = turret.addOrReplaceChild("cube_r39", CubeListBuilder.create().texOffs(0, 32).addBox(-0.5F, -3.0F, -2.6602F, 1.0F, 5.0F, 6.0F, new CubeDeformation(0.0F))
//		.texOffs(19, 0).addBox(-1.5F, -3.0F, 1.3398F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 0).addBox(-1.5F, -3.0F, -0.8602F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-8.2546F, -1.4603F, 13.2787F, 0.0F, 0.5236F, 0.0F));
//
//		PartDefinition cube_r40 = turret.addOrReplaceChild("cube_r40", CubeListBuilder.create().texOffs(54, 0).addBox(0.5F, -3.0F, -0.8602F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
//		.texOffs(54, 9).addBox(0.5F, -3.0F, 1.3398F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
//		.texOffs(61, 36).addBox(-0.5F, -3.0F, -2.6602F, 1.0F, 5.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(8.2546F, -1.4603F, 13.2787F, 0.0F, -0.5236F, 0.0F));
//
//		PartDefinition cube_r41 = turret.addOrReplaceChild("cube_r41", CubeListBuilder.create().texOffs(0, 124).addBox(0.8008F, -2.9961F, -2.2F, 6.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.4275F, -1.2911F, 0.0F, -0.5236F, 0.0F));
//
//		PartDefinition cube_r42 = turret.addOrReplaceChild("cube_r42", CubeListBuilder.create().texOffs(18, 125).addBox(-6.8008F, -2.9961F, -2.2F, 6.0F, 6.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.4275F, -1.2911F, 0.0F, 0.5236F, 0.0F));
//
//		PartDefinition cube_r43 = turret.addOrReplaceChild("cube_r43", CubeListBuilder.create().texOffs(54, 0).addBox(0.0039F, -1.75F, -4.8477F, 4.0F, 2.0F, 7.0F, new CubeDeformation(0.0F))
//		.texOffs(54, 9).addBox(15.9961F, -1.75F, -4.8477F, 4.0F, 2.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-10.0F, -2.7309F, -0.8149F, 0.6109F, 0.0F, 0.0F));
//
//		PartDefinition cube_r44 = turret.addOrReplaceChild("cube_r44", CubeListBuilder.create().texOffs(66, 89).mirror().addBox(-9.9961F, -1.0F, -4.0F, 5.0F, 2.0F, 9.0F, new CubeDeformation(0.0F)).mirror(false)
//		.texOffs(66, 89).addBox(4.9961F, -1.0F, -4.0F, 5.0F, 2.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.2011F, 6.0335F, 0.0436F, 0.0F, 0.0F));
//
//		PartDefinition cube_r45 = turret.addOrReplaceChild("cube_r45", CubeListBuilder.create().texOffs(92, 0).addBox(-10.0117F, -1.0F, -8.5F, 20.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 1.0256F, 2.8026F, -0.1745F, 0.0F, 0.0F));
//
//		PartDefinition cube_r46 = turret.addOrReplaceChild("cube_r46", CubeListBuilder.create().texOffs(0, 51).addBox(-7.0F, -3.0F, -2.5F, 3.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.3734F, -1.9431F, 9.0245F, -0.2182F, 0.5236F, 0.0F));
//
//		PartDefinition cube_r47 = turret.addOrReplaceChild("cube_r47", CubeListBuilder.create().texOffs(36, 125).addBox(4.0F, -3.0F, -2.5F, 3.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.3734F, -1.9431F, 9.0245F, -0.2182F, -0.5236F, 0.0F));
//
//		PartDefinition cube_r48 = turret.addOrReplaceChild("cube_r48", CubeListBuilder.create().texOffs(103, 106).addBox(-7.0F, -3.0F, -2.5F, 14.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -1.9744F, 12.7245F, -0.2182F, 0.0F, 0.0F));
//
//		PartDefinition cube_r49 = turret.addOrReplaceChild("cube_r49", CubeListBuilder.create().texOffs(54, 18).addBox(-3.0F, -0.5F, -1.5F, 6.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 102).addBox(4.0F, -0.5F, -1.5F, 6.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.5F, -5.9275F, 9.4432F, -0.1745F, 0.0F, 0.0F));
//
//		PartDefinition cube_r50 = turret.addOrReplaceChild("cube_r50", CubeListBuilder.create().texOffs(64, 115).addBox(-0.5F, -1.0F, -4.9F, 9.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.6791F, -6.4763F, 0.4363F, -0.5236F, 0.0F));
//
//		PartDefinition cube_r51 = turret.addOrReplaceChild("cube_r51", CubeListBuilder.create().texOffs(0, 11).addBox(-2.0F, -1.0F, -4.5F, 4.0F, 2.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.6791F, -6.4763F, 0.4363F, 0.0F, 0.0F));
//
//		PartDefinition barrel = turret.addOrReplaceChild("barrel", CubeListBuilder.create().texOffs(132, 50).addBox(-0.2984F, -1.5F, -8.0F, 0.5967F, 3.0F, 6.0F, new CubeDeformation(0.0F))
//		.texOffs(52, 125).addBox(-1.5F, -0.2984F, -8.0F, 3.0F, 0.5967F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.0355F, -10.7071F));
//
//		PartDefinition hexadecagon_r73 = barrel.addOrReplaceChild("hexadecagon_r73", CubeListBuilder.create().texOffs(121, 28).addBox(-1.5F, -0.2984F, -8.0F, 3.0F, 0.5967F, 6.0F, new CubeDeformation(0.0F))
//		.texOffs(129, 131).addBox(-0.2984F, -1.5F, -8.0F, 0.5967F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.3927F));
//
//		PartDefinition hexadecagon_r74 = barrel.addOrReplaceChild("hexadecagon_r74", CubeListBuilder.create().texOffs(70, 125).addBox(-1.5F, -0.2984F, -8.0F, 3.0F, 0.5967F, 6.0F, new CubeDeformation(0.0F))
//		.texOffs(63, 132).addBox(-0.2984F, -1.5F, -8.0F, 0.5967F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.3927F));
//
//		PartDefinition hexadecagon_r75 = barrel.addOrReplaceChild("hexadecagon_r75", CubeListBuilder.create().texOffs(82, 131).addBox(-0.2984F, -1.5F, -8.0F, 0.5967F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.7854F));
//
//		PartDefinition hexadecagon_r76 = barrel.addOrReplaceChild("hexadecagon_r76", CubeListBuilder.create().texOffs(0, 133).addBox(-0.2984F, -1.5F, -8.0F, 0.5967F, 3.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7854F));
//
//		PartDefinition barrel2 = turret.addOrReplaceChild("barrel2", CubeListBuilder.create().texOffs(0, 50).addBox(-0.1989F, -1.0F, -8.0F, 0.3978F, 2.0F, 28.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 52).addBox(-1.0F, -0.1989F, -8.0F, 2.0F, 0.3978F, 28.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.9355F, -26.7071F));
//
//		PartDefinition hexadecagon_r77 = barrel2.addOrReplaceChild("hexadecagon_r77", CubeListBuilder.create().texOffs(0, 52).addBox(-1.0F, -0.1989F, -8.0F, 2.0F, 0.3978F, 28.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 48).addBox(-0.1989F, -1.0F, -8.0F, 0.3978F, 2.0F, 28.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.3927F));
//
//		PartDefinition hexadecagon_r78 = barrel2.addOrReplaceChild("hexadecagon_r78", CubeListBuilder.create().texOffs(0, 52).addBox(-1.0F, -0.1989F, -8.0F, 2.0F, 0.3978F, 28.0F, new CubeDeformation(0.0F))
//		.texOffs(0, 52).addBox(-0.1989F, -1.0F, -8.0F, 0.3978F, 2.0F, 28.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.3927F));
//
//		PartDefinition hexadecagon_r79 = barrel2.addOrReplaceChild("hexadecagon_r79", CubeListBuilder.create().texOffs(0, 46).addBox(-0.1989F, -1.0F, -8.0F, 0.3978F, 2.0F, 28.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.7854F));
//
//		PartDefinition hexadecagon_r80 = barrel2.addOrReplaceChild("hexadecagon_r80", CubeListBuilder.create().texOffs(0, 54).addBox(-0.1989F, -1.0F, -8.0F, 0.3978F, 2.0F, 28.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7854F));
//
//		return LayerDefinition.create(meshdefinition, 256, 256);
//	}
//
//	@Override
//	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
//
//	}
//
//	@Override
//	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
//		wheel_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel2_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel2_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel3_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel3_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel4_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel4_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel5_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel5_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel6_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel6_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel7_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel7_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel8_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel8_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel9_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		wheel9_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		belt_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		belt_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		body_left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		body_right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//		turret.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
//	}
//}