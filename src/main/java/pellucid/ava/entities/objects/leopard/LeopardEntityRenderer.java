package pellucid.ava.entities.objects.leopard;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.AVAModelLayers;

public class LeopardEntityRenderer extends MobRenderer<LeopardEntity, LeopardModel>
{
    private final ResourceLocation texture;
    public LeopardEntityRenderer(EntityRendererProvider.Context context, String variant)
    {
        super(context, new LeopardModel(context.bakeLayer(AVAModelLayers.LEOPARD)), 1.5F);
        this.texture = new ResourceLocation(AVA.MODID, "textures/entities/leopard_" + variant + ".png");
    }

    @Override
    public void render(LeopardEntity entity, float p_115456_, float p_115457_, PoseStack stack, MultiBufferSource p_115459_, int p_115460_)
    {
        entity.hurtTime = 0;
        stack.pushPose();
        float scale = 2.75F;
        stack.scale(scale, scale - 0.1F, scale + 0.3F);
        stack.translate(0, 0.09F, 0);
        super.render(entity, p_115456_, p_115457_, stack, p_115459_, p_115460_);
        stack.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(LeopardEntity p_114482_)
    {
        return texture;
    }
}
