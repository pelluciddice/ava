package pellucid.ava.entities.objects.leopard;

import net.minecraft.commands.arguments.EntityAnchorArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.blocks.IInteractable;
import pellucid.ava.commands.BroadcastTextCommand;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.entities.functionalities.ICustomOnHitEffect;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.gamemodes.modes.EscortMode;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import java.util.List;

public class LeopardEntity extends Mob implements ICustomOnHitEffect, IInteractable<EntityHitResult>, IEntityWithComplexSpawn
{
    private static final EntityDataAccessor<Integer> DATA_REPAIR = SynchedEntityData.defineId(LeopardEntity.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Boolean> DATA_MOVING = SynchedEntityData.defineId(LeopardEntity.class, EntityDataSerializers.BOOLEAN);

    private int lastEscort = 0;
    private int lastRepaired = 60;
    private int engineSound = 0;

    public LeopardEntity(EntityType<? extends LeopardEntity> type, Level worldIn)
    {
        super(type, worldIn);
        refreshDimensions();
    }

    @Override
    public double maxDistance()
    {
        return 1.0F;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.INT.write(compound, "repair", getRepair());
        DataTypes.INT.write(compound, "lastEscort", lastEscort);
        DataTypes.INT.write(compound, "lastRepaired", lastRepaired);
        DataTypes.INT.write(compound, "engineSound", engineSound);
        DataTypes.BOOLEAN.write(compound, "moving", isMoving());
        DataTypes.INT.write(compound, "brokenTicks", brokenTicks);
        DataTypes.INT.write(compound, "stoppedTicks", stoppedTicks);
        DataTypes.INT.write(compound, "escortedTicks", escortedTicks);
        DataTypes.STRING.write(compound, "lastRepairedEntity", lastRepairedEntity);
        DataTypes.STRING.write(compound, "lastDestroyedEntity", lastDestroyedEntity);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        setRepair(DataTypes.INT.read(compound, "repair"));
        lastEscort = DataTypes.INT.read(compound, "lastEscort");
        lastRepaired = DataTypes.INT.read(compound, "lastRepaired");
        engineSound = DataTypes.INT.read(compound, "engineSound");
        setMoving(DataTypes.BOOLEAN.read(compound, "moving"));
        brokenTicks = DataTypes.INT.read(compound, "brokenTicks");
        stoppedTicks = DataTypes.INT.read(compound, "stoppedTicks");
        escortedTicks = DataTypes.INT.read(compound, "escortedTicks");
        lastRepairedEntity = DataTypes.STRING.read(compound, "lastRepairedEntity");
        lastDestroyedEntity = DataTypes.STRING.read(compound, "lastDestroyedEntity");
    }

    @Override
    protected void defineSynchedData(SynchedEntityData.Builder builder)
    {
        super.defineSynchedData(builder);
        builder.define(DATA_REPAIR, 0);
        builder.define(DATA_MOVING, false);
    }

    public void setRepair(int repair)
    {
        entityData.set(DATA_REPAIR, repair);
    }

    public int getRepair()
    {
        return entityData.get(DATA_REPAIR);
    }

    public void setMoving(boolean moving)
    {
        entityData.set(DATA_MOVING, moving);
    }

    public boolean isMoving()
    {
        return entityData.get(DATA_MOVING);
    }

    public static AttributeSupplier.Builder registerAttributes()
    {
        return AVAHostileEntity.registerAttributes()
                .add(Attributes.MAX_HEALTH, 400.0F)
                .add(Attributes.ATTACK_DAMAGE, 0F)
                .add(Attributes.FOLLOW_RANGE, 100.0F);
    }

    @Override
    public double getX(double offset)
    {
        return this.position().x + (double) (getDirection().getAxis() == Direction.Axis.Z ? AVAConstants.LEOPARD_WIDTH : AVAConstants.LEOPARD_LENGTH) * offset;
    }

    @Override
    public double getZ(double offset)
    {
        return this.position().z + (double) (getDirection().getAxis() == Direction.Axis.Z ? AVAConstants.LEOPARD_LENGTH : AVAConstants.LEOPARD_WIDTH) * offset;
    }

    @Override
    protected AABB makeBoundingBox()
    {
        double x = getX();
        double y = getY();
        double z = getZ();
        float w;
        float l;
        float h = AVAConstants.LEOPARD_HEIGHT;
        if (getDirection().getAxis() == Direction.Axis.Z)
        {
            w = AVAConstants.LEOPARD_WIDTH;
            l = AVAConstants.LEOPARD_LENGTH;
        }
        else
        {
            w = AVAConstants.LEOPARD_LENGTH;
            l = AVAConstants.LEOPARD_WIDTH;
        }
        return new AABB(x - (double) w / 2.0D, y, z - (double) l / 2.0D, x + (double) w / 2.0D, y + (double) h, z + (double) l / 2.0D);
    }

//    @Override
//    public EntityDimensions getDimensions(Pose pose)
//    {
//        return getDirection().getAxis() == Direction.Axis.Z ? new AVAEntityDimensions(AVAConstants.LEOPARD_WIDTH, AVAConstants.LEOPARD_LENGTH, AVAConstants.LEOPARD_HEIGHT) : new AVAEntityDimensions(AVAConstants.LEOPARD_LENGTH, AVAConstants.LEOPARD_WIDTH, AVAConstants.LEOPARD_HEIGHT);
//    }

    @Override
    public void tick()
    {
        super.tick();
        invulnerableTime = 0;
        if (!level().isClientSide() && getHealth() < 1.0F)
            setHealth(1.0F);
        refreshDimensions();
    }

    public void repair(String name)
    {
        if (!requireRepair() || !(level() instanceof ServerLevel world))
            return;
        this.lastRepairedEntity = name;
        setRepair(getRepair() + 1);
        if (getRepair() >= getDuration())
        {
            setRepair(0);
            setHealth(getMaxHealth());
            lastRepaired = 100;

            BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.tank_repaired_eu", 100, AVAWeaponUtil.TeamSide.EU, lastRepairedEntity);
            BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.tank_repaired_nrf", 100, AVAWeaponUtil.TeamSide.NRF);
            lastRepairedEntity = "";
        }
    }

    public boolean requireRepair()
    {
        return getHealth() <= 1.0F;
    }

    @Override
    public boolean isDeadOrDying()
    {
        return false;
    }

    private int stoppedTicks = 0;
    private int brokenTicks = 0;
    private int aliveTicks = 0;
    private int escortedTicks = 0;
    private String lastRepairedEntity = "";
    private String lastDestroyedEntity = "";

    public void updateFromEscort(EscortMode mode, ServerLevel world)
    {
        mode.tankID = getId();
        if (engineSound <= 0 && !requireRepair())
        {
            if (isMoving())
                AVAWeaponUtil.playAttenuableSoundToClientMoving(AVASounds.LEOPARD_MOVING.get(), this, 1.5F, 0.85F + random.nextFloat() * 0.3F);
            else
                AVAWeaponUtil.playAttenuableSoundToClientMoving(AVASounds.LEOPARD_AMBIENT.get(), this, 1.5F, 0.85F + random.nextFloat() * 0.3F);
            engineSound = 2;
        }
        else if (engineSound > 0)
            engineSound--;
        if (lastEscort > 0)
            lastEscort--;
        if (lastRepaired > 0)
            lastRepaired--;
        Vec3 next = mode.getNextPath();
        if (next != null)
        {
            if (position().distanceTo(next) < 0.5F)
                mode.nextPath(world);
            if (requireRepair())
            {
                setDeltaMovement(0, 0, 0);
                setMoving(false);
                if (brokenTicks % 600 == 0)
                {
                    if (brokenTicks == 0)
                    {
                        BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.tank_damaged_eu", 100, AVAWeaponUtil.TeamSide.EU);
                        BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.tank_damaged_nrf", 100, AVAWeaponUtil.TeamSide.NRF, lastDestroyedEntity);
                    }
                    else
                        BroadcastTextCommand.broadcastToTeam(world, random.nextBoolean() ? "ava.broadcast.tank_damaged_eu" : "ava.broadcast.tank_stopped", 100, AVAWeaponUtil.TeamSide.EU);
                }
                brokenTicks++;
                aliveTicks = 0;
                if (level() instanceof ServerLevel server && random.nextBoolean())
                    server.sendParticles(ParticleTypes.LARGE_SMOKE, getRandomX(0.25F), position().y + 3F, getRandomZ(0.25F), 3, 0, 0, 0, 0F);
                return;
            }
            else
            {
                brokenTicks = 0;
                aliveTicks++;
                if (aliveTicks % 600 == 0)
                    BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.tank_destroy", 100, AVAWeaponUtil.TeamSide.NRF);
            }
            if (!level().getEntitiesOfClass(LivingEntity.class, getBoundingBox().inflate(2.0F), (e) -> AVAWeaponUtil.isValidEntity(e) && e != this && AVAWeaponUtil.TeamSide.EU.isSameSide(e)).isEmpty())
            {
                lastEscort = 20;
                stoppedTicks = 0;
            }
            if (lastEscort > 0)
            {
                setDeltaMovement(next.subtract(position()).normalize().scale(mode.tankSpeed));
                List<Entity> excluded = level().getEntitiesOfClass(Entity.class, getBoundingBox(), (e) -> AVAWeaponUtil.isValidEntity(e) && e != this);
                lookAt(EntityAnchorArgument.Anchor.EYES, next);
                Vec3i normal = getDirection().getNormal();
                Vec3 vec = new Vec3(normal.getX(), normal.getY(), normal.getZ());
                lookAt(EntityAnchorArgument.Anchor.EYES, position().add(vec));
                level().getEntitiesOfClass(Entity.class, getBoundingBox().expandTowards(vec.scale(mode.tankSpeed)), (e) -> AVAWeaponUtil.isValidEntity(e) && e != this && !excluded.contains(e)).forEach((e) -> e.hurt(level().damageSources().fellOutOfWorld(), 100));
                setMoving(true);
            }
            else
            {
                setMoving(false);
                setDeltaMovement(0, 0, 0);
                stoppedTicks++;
                if (stoppedTicks % 200 == 0)
                    BroadcastTextCommand.broadcastToTeam(world, "ava.broadcast.tank_stopped", 100, AVAWeaponUtil.TeamSide.EU);
            }
        }
        else mode.nextPath(world);
    }

    @Override
    public boolean shouldDrawTipWrench()
    {
        return true;
    }

    @Override
    public String getInteractableTip()
    {
        return "ava.interaction.leopard";
    }

    @Override
    public boolean isPersistenceRequired()
    {
        return true;
    }

    @Override
    public boolean hurt(DamageSource source, float amount)
    {
        if (source instanceof AVADamageSources.AVAIndirectDamageSource avaSource && avaSource.getWeapon() == SpecialWeapons.RPG7.get() && lastRepaired <= 0)
        {
            lastDestroyedEntity = source.getEntity() != null ? source.getEntity().getName().getString() : lastDestroyedEntity;
            return super.hurt(source, amount);
        }
        if (source.is(DamageTypeTags.BYPASSES_INVULNERABILITY))
            remove(RemovalReason.DISCARDED);
        return false;
    }

    @Override
    public void onHit(Level world, Vec3 hit)
    {
        if (level() instanceof ServerLevel)
        {
            ((ServerLevel) level()).sendParticles(ParticleTypes.ENCHANTED_HIT, hit.x, hit.y, hit.z, 4, 0, 0, 0, 0.25F);
            playSound(AVASounds.BULLET_BLOCK_METAL_THIN.get(), 1.0F, getVoicePitch());
        }
    }

    @Override
    public boolean shouldRenderAtSqrDistance(double dist)
    {
        return true;
    }

    @Nullable
    @Override
    protected SoundEvent getHurtSound(DamageSource p_21239_)
    {
        return AVASounds.BULLET_BLOCK_METAL_THIN.get();
    }

    @Override
    public float getVoicePitch()
    {
        return 1.0F + random.nextFloat() / 2.0F;
    }

    @Override
    public void knockback(double p_147241_, double p_147242_, double p_147243_)
    {

    }

    @Override
    public void push(double p_20286_, double p_20287_, double p_20288_)
    {
    }

    @Override
    public void push(Entity p_21294_)
    {
    }

    @Override
    public boolean onGround()
    {
        return true;
    }

    @Override
    public boolean canBeCollidedWith()
    {
        return true;
    }

    @Override
    public boolean isNoGravity()
    {
        return false;
    }

    @Override
    public boolean isLiving()
    {
        return false;
    }

    @Override
    public int getDuration()
    {
        return 200;
    }

    @Override
    public void whileInteract(Level world, LivingEntity interacter)
    {
        if (world.isClientSide())
            PlayerActionMessage.repairLeopard();
    }

    @Override
    public void interact(Level world, EntityHitResult result, @Nullable BlockPos pos, @Nullable Vec3 vector3d, LivingEntity interacter)
    {

    }

    @Override
    public boolean canInteract(EntityHitResult result, LivingEntity interactor, BlockPos pos)
    {
        return requireRepair() && AVAWeaponUtil.TeamSide.EU.isSameSide(interactor);
    }

    @Override
    public boolean sharedProgress()
    {
        return true;
    }

    @Override
    public int getSharedProgress()
    {
        return getRepair();
    }

    @Override
    public int getAmbientSoundInterval()
    {
        return 70;
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        DataTypes.FLOAT.write(buffer, getHealth());
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        setHealth(DataTypes.FLOAT.read(additionalData));
    }

    public enum Variant
    {
        DESERT,
        FOREST,
        SNOW,
        ;
    }
}
