package pellucid.ava.entities.objects.kits;

import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.functionalities.IObjectPickable;
import pellucid.ava.entities.objects.ObjectEntity;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;

public abstract class KitEntity extends ObjectEntity
{
    public KitEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public KitEntity(EntityType<Entity> type, Level worldIn, double x, double y, double z)
    {
        super(type, worldIn);
        this.absMoveTo(x, y, z, this.random.nextInt(12) * 30, this.random.nextInt(12) * 30);
    }

    @Override
    public void tick()
    {
        super.tick();
        if (this.rangeTravelled > 2500)
            this.remove(RemovalReason.DISCARDED);
        if (level().getGameTime() % 2 == 0 && !level().isClientSide())
            level().getEntitiesOfClass(LivingEntity.class, getBoundingBox().inflate(0.225F)).stream().filter((entity) -> AVAWeaponUtil.isValidEntity(entity) && (entity instanceof Player || entity instanceof IObjectPickable)).findFirst().ifPresent((entity) -> {
                if (onCollide(entity))
                    remove(RemovalReason.DISCARDED);
            });
    }

    protected boolean onCollide(LivingEntity entity)
    {
        level().playSound(null, this.getX(), this.getY(), this.getZ(), AVASounds.COLLECTS_PICKABLE.get(), SoundSource.NEUTRAL, 1.0F, 1.0F);
        return true;
    }

    @Override
    public boolean isPickable() {
        return true;
    }
}
