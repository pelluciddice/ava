package pellucid.ava.entities.objects.kits.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.entities.objects.kits.AmmoKitEntity;
import pellucid.ava.entities.objects.kits.KitEntity;

@OnlyIn(Dist.CLIENT)
public class KitRenderer extends EntityRenderer<KitEntity>
{
    private static final ResourceLocation AMMO_KIT = new ResourceLocation(AVA.MODID, "textures/entities/ammo_kit.png");
    private static final ResourceLocation FIRST_AID_KIT = new ResourceLocation(AVA.MODID, "textures/entities/first_aid_kit.png");

    private final KitModel kitModel;

    public KitRenderer(EntityRendererProvider.Context context)
    {
        super(context);
        this.kitModel = new KitModel(context.bakeLayer(AVAModelLayers.KIT));
    }

    @Override
    public void render(KitEntity entityIn, float entityYaw, float partialTicks, PoseStack stack, MultiBufferSource bufferIn, int packedLightIn)
    {
        stack.pushPose();
        stack.mulPose(Axis.YP.rotationDegrees(Mth.lerp(partialTicks, entityIn.yRotO, entityIn.getYRot()) - 90.0F));
        stack.mulPose(Axis.ZP.rotationDegrees(Mth.lerp(partialTicks, entityIn.xRotO, entityIn.getXRot())));
        kitModel.renderToBuffer(stack, bufferIn.getBuffer(this.kitModel.renderType(this.getTextureLocation(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        stack.popPose();
    }

    @Override
    public ResourceLocation getTextureLocation(KitEntity entity)
    {
        return entity instanceof AmmoKitEntity ? AMMO_KIT : FIRST_AID_KIT;
    }
}
