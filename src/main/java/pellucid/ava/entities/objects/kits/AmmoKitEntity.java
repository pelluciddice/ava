package pellucid.ava.entities.objects.kits;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAWeaponUtil;

public class AmmoKitEntity extends KitEntity
{
    public AmmoKitEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public AmmoKitEntity(Level worldIn, double x, double y, double z)
    {
        super(AVAEntities.AMMO_KIT.get(), worldIn, x, y, z);
    }

    @Override
    protected boolean onCollide(LivingEntity entity)
    {
        ItemStack stack = entity.getMainHandItem();
        if (stack.getItem() instanceof AVAItemGun)
        {
            super.onCollide(entity);
            if (entity instanceof Player)
            AVAWeaponUtil.addAmmoToPlayer((Player) entity, stack, 3);
            return true;
        }
        return false;
    }
}
