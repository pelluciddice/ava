package pellucid.ava.entities.objects.kits;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.util.AVAWeaponUtil;

public class FirstAidKitEntity extends KitEntity
{
    public FirstAidKitEntity(EntityType<Entity> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public FirstAidKitEntity(Level worldIn, double x, double y, double z)
    {
        super(AVAEntities.FIRST_AID_KIT.get(), worldIn, x, y, z);
    }

    @Override
    protected boolean onCollide(LivingEntity entity)
    {
        if (AVAWeaponUtil.canReceiveHealing(entity))
        {
            super.onCollide(entity);
            entity.heal(10.0F);
            return true;
        }
        return false;
    }
}
