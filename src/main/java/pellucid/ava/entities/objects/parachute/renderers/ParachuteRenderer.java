package pellucid.ava.entities.objects.parachute.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.Model;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.entities.objects.parachute.ParachuteEntity;
import pellucid.ava.entities.throwables.renderers.ObjectRenderer;

public class ParachuteRenderer extends ObjectRenderer<ParachuteEntity>
{
    public static final ResourceLocation PARACHUTE_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/parachute.png");
    private static net.minecraft.client.model.Model PARACHUTE_MODEL = null;

    public ParachuteRenderer(EntityRendererProvider.Context context)
    {
        super(context, new ParachuteModel(context.bakeLayer(AVAModelLayers.PARACHUTE)), PARACHUTE_TEXTURE);
        if (PARACHUTE_MODEL == null)
            PARACHUTE_MODEL = new ParachuteModel(context.bakeLayer(AVAModelLayers.PARACHUTE));
    }

    @Override
    public void render(ParachuteEntity entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if ((player == null || player == entityIn.getOwner()) && minecraft.options.getCameraType().isFirstPerson())
            return;
        renderParachute(model, matrixStackIn, bufferIn, packedLightIn);
    }

    public static void renderParachute(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
        renderParachute(PARACHUTE_MODEL, matrixStackIn, bufferIn, packedLightIn);
    }

    public static void renderParachute(Model model, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
        matrixStackIn.pushPose();
        matrixStackIn.translate(0.0F, 0.75F, 0.0F);
        matrixStackIn.scale(0.75F, 0.75F, 0.75F);
        matrixStackIn.mulPose(Axis.XP.rotationDegrees(180.0F));
        model.renderToBuffer(matrixStackIn, bufferIn.getBuffer(model.renderType(PARACHUTE_TEXTURE)), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        matrixStackIn.popPose();
    }
}
