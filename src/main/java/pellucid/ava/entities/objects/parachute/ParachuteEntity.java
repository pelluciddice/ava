package pellucid.ava.entities.objects.parachute;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.material.PushReaction;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.entities.base.AVAEntity;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.UUID;

@Deprecated
public class ParachuteEntity extends AVAEntity implements IEntityWithComplexSpawn
{
    protected UUID ownerUUID;
    protected int ownerID = -1;

    public ParachuteEntity(EntityType<?> entityTypeIn, Level worldIn)
    {
        super(entityTypeIn, worldIn);
    }

    public ParachuteEntity(Level worldIn, LivingEntity owner)
    {
        super(null, worldIn);
        //        super(AVAEntities.PARACHUTE, worldIn);
//        ownerUUID = owner.getUUID();
//        ownerID = owner.getId();
//        setPositionWithOwner();
    }

    private void setPositionWithOwner()
    {
        LivingEntity owner = getOwner();
        if (AVAWeaponUtil.isValidEntity(owner))
        {
            setPos(owner.getX(), owner.getY(), owner.getZ());
            xOld = owner.xOld;
            yOld = owner.yOld;
            zOld = owner.zOld;
            xo = owner.xo;
            yo = owner.yo;
            zo = owner.zo;
        }
    }

    @Override
    public void tick()
    {
        super.tick();
        LivingEntity owner = getOwner();
        if (AVAWeaponUtil.isValidEntity(owner) && (!owner.onGround() || level().isClientSide()))
        {
            setPositionWithOwner();
            AttributeInstance attribute = owner.getAttribute(Attributes.GRAVITY);
            if (attribute != null && !attribute.hasModifier(AVAConstants.SLOW_FALLING_MODIFIER))
                attribute.addTransientModifier(AVAConstants.SLOW_FALLING_MODIFIER);
        }
        else if (!level().isClientSide())
            remove(RemovalReason.DISCARDED);
    }

    @Override
    public void remove(RemovalReason reason)
    {
        LivingEntity owner = getOwner();
        if (owner != null)
        {
            AttributeInstance attribute = owner.getAttribute(Attributes.GRAVITY);
            if (attribute != null && attribute.hasModifier(AVAConstants.SLOW_FALLING_MODIFIER))
                attribute.removeModifier(AVAConstants.SLOW_FALLING_MODIFIER.id());
            if (owner instanceof Player)
                PlayerAction.getCap((Player) owner).setIsUsingParachute((Player) owner, false);
        }
        super.remove(reason);
    }

    @Nullable
    public LivingEntity getOwner()
    {
        if (level() instanceof ServerLevel)
        {
            if (ownerUUID != null)
            {
                Entity entity = ((ServerLevel) level()).getEntity(ownerUUID);
                return entity == null ? null : (LivingEntity) entity;
            }
        }
        else
        {
            if (ownerID != -1)
            {
                Entity entity = level().getEntity(ownerID);
                return entity == null ? null : (LivingEntity) entity;
            }
        }
        return null;
    }

    @Override
    public boolean isPickable()
    {
        return false;
    }

    @Override
    public PushReaction getPistonPushReaction()
    {
        return PushReaction.IGNORE;
    }

    @Override
    public boolean isPushable()
    {
        return false;
    }

    @Override
    public boolean canCollideWith(Entity entity)
    {
        return false;
    }

    @Override
    public void push(Entity entityIn)
    {

    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        if (ownerUUID != null)
            DataTypes.UUID.write(compound, "owner", ownerUUID);
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        if (compound.contains("owner"))
            ownerUUID = DataTypes.UUID.read(compound, "owner");
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        buffer.writeInt(ownerID);
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        ownerID = additionalData.readInt();
    }
}
