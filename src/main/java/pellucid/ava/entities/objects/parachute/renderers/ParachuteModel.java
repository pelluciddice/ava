package pellucid.ava.entities.objects.parachute.renderers;// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.Entity;

public class ParachuteModel<T extends Entity> extends EntityModel<T>
{
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	private final ModelPart bone;
	private final ModelPart bone2;
	private final ModelPart bone3;
	private final ModelPart bone4;
	private final ModelPart bone5;
	private final ModelPart bone6;
	private final ModelPart bone7;
	private final ModelPart bone8;
	private final ModelPart bone9;
	private final ModelPart bone10;
	private final ModelPart bone11;
	private final ModelPart bone12;
	private final ModelPart bone13;
	private final ModelPart bone14;
	private final ModelPart bone15;
	private final ModelPart bone16;
	private final ModelPart bone17;
	private final ModelPart bone18;
	private final ModelPart bone19;
	private final ModelPart bone20;
	private final ModelPart bone21;
	private final ModelPart bone22;
	private final ModelPart bone23;
	private final ModelPart bone24;
	private final ModelPart bone25;
	private final ModelPart bone26;
	private final ModelPart bone27;
	private final ModelPart bone28;
	private final ModelPart bone29;
	private final ModelPart bone30;

	public ParachuteModel(ModelPart root) {
		this.bone = root.getChild("bone");
		this.bone2 = root.getChild("bone2");
		this.bone3 = root.getChild("bone3");
		this.bone4 = root.getChild("bone4");
		this.bone5 = root.getChild("bone5");
		this.bone6 = root.getChild("bone6");
		this.bone7 = root.getChild("bone7");
		this.bone8 = root.getChild("bone8");
		this.bone9 = root.getChild("bone9");
		this.bone10 = root.getChild("bone10");
		this.bone11 = root.getChild("bone11");
		this.bone12 = root.getChild("bone12");
		this.bone13 = root.getChild("bone13");
		this.bone14 = root.getChild("bone14");
		this.bone15 = root.getChild("bone15");
		this.bone16 = root.getChild("bone16");
		this.bone17 = root.getChild("bone17");
		this.bone18 = root.getChild("bone18");
		this.bone19 = root.getChild("bone19");
		this.bone20 = root.getChild("bone20");
		this.bone21 = root.getChild("bone21");
		this.bone22 = root.getChild("bone22");
		this.bone23 = root.getChild("bone23");
		this.bone24 = root.getChild("bone24");
		this.bone25 = root.getChild("bone25");
		this.bone26 = root.getChild("bone26");
		this.bone27 = root.getChild("bone27");
		this.bone28 = root.getChild("bone28");
		this.bone29 = root.getChild("bone29");
		this.bone30 = root.getChild("bone30");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition bone = partdefinition.addOrReplaceChild("bone", CubeListBuilder.create(), PartPose.offset(0.0059F, -21.7941F, -15.3412F));

		PartDefinition cube_r1 = bone.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(163, 255).addBox(-8.0F, -0.5F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -92.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r2 = bone.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(256, 90).addBox(-8.0F, -0.5F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -92.0F, 30.6875F, -0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r3 = bone.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(256, 108).addBox(-22.4166F, -5.7472F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -91.9989F, 15.3412F, 0.0F, 0.0F, -0.3491F));

		PartDefinition cube_r4 = bone.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(256, 126).addBox(6.4166F, -5.7472F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -91.9989F, 15.3412F, 0.0F, 0.0F, 0.3491F));

		PartDefinition bone2 = partdefinition.addOrReplaceChild("bone2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -24.4171F, 0.0013F, 0.0F, -0.3927F, 0.0F));

		PartDefinition cube_r5 = bone2.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(212, 239).addBox(-8.0F, -86.9517F, 23.4659F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.623F, -15.3425F, 0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r6 = bone2.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(0, 251).addBox(-8.0F, -86.9517F, -39.4659F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.623F, 15.345F, -0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r7 = bone2.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(49, 253).addBox(9.0493F, -92.1989F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.6242F, -0.0013F, 0.0F, 0.0F, -0.3491F));

		PartDefinition cube_r8 = bone2.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(98, 255).addBox(-25.0493F, -92.1989F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.6242F, -0.0013F, 0.0F, 0.0F, 0.3491F));

		PartDefinition bone3 = partdefinition.addOrReplaceChild("bone3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -24.4171F, 0.0013F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r9 = bone3.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(49, 235).addBox(-8.0F, -86.9517F, 23.4659F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.623F, -15.3425F, 0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r10 = bone3.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(237, 2).addBox(-8.0F, -86.9517F, -39.4659F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.623F, 15.345F, -0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r11 = bone3.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(98, 237).addBox(9.0493F, -92.1989F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.6242F, -0.0013F, 0.0F, 0.0F, -0.3491F));

		PartDefinition cube_r12 = bone3.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(163, 237).addBox(-25.0493F, -92.1989F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.6242F, -0.0013F, 0.0F, 0.0F, 0.3491F));

		PartDefinition bone4 = partdefinition.addOrReplaceChild("bone4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -24.4171F, 0.0013F, 0.0F, -1.1781F, 0.0F));

		PartDefinition cube_r13 = bone4.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(221, 54).addBox(-8.0F, -86.9517F, 23.4659F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.623F, -15.3425F, 0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r14 = bone4.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(221, 72).addBox(-8.0F, -86.9517F, -39.4659F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.623F, 15.345F, -0.3491F, 0.0F, 0.0F));

		PartDefinition cube_r15 = bone4.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(212, 221).addBox(9.0493F, -92.1989F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.6242F, -0.0013F, 0.0F, 0.0F, -0.3491F));

		PartDefinition cube_r16 = bone4.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(0, 233).addBox(-25.0493F, -92.1989F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 2.6242F, -0.0013F, 0.0F, 0.0F, 0.3491F));

		PartDefinition bone5 = partdefinition.addOrReplaceChild("bone5", CubeListBuilder.create(), PartPose.offset(0.0059F, -32.5365F, 0.0064F));

		PartDefinition cube_r17 = bone5.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(98, 219).addBox(-8.0F, -19.0354F, 14.0896F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -73.4646F, 0.0F, -0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r18 = bone5.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(49, 217).addBox(-8.0F, -19.0354F, -30.0896F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -73.4646F, 0.0F, 0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r19 = bone5.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(163, 219).addBox(-8.0F, -19.0354F, 14.0896F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -73.4646F, 0.0F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r20 = bone5.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(221, 36).addBox(-8.0F, -19.0354F, -30.0896F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -73.4646F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition bone6 = partdefinition.addOrReplaceChild("bone6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -32.5365F, 0.0064F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r21 = bone6.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(212, 167).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r22 = bone6.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(212, 185).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r23 = bone6.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(212, 203).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r24 = bone6.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(0, 215).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition bone7 = partdefinition.addOrReplaceChild("bone7", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -32.5365F, 0.0064F, 0.0F, -0.2618F, 0.0F));

		PartDefinition cube_r25 = bone7.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(207, 95).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r26 = bone7.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(207, 113).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r27 = bone7.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(207, 131).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r28 = bone7.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(207, 149).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition bone8 = partdefinition.addOrReplaceChild("bone8", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -32.5365F, 0.0064F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r29 = bone8.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(0, 197).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r30 = bone8.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(49, 199).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r31 = bone8.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(98, 201).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r32 = bone8.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(163, 201).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition bone9 = partdefinition.addOrReplaceChild("bone9", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -32.5365F, 0.0064F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r33 = bone9.addOrReplaceChild("cube_r33", CubeListBuilder.create().texOffs(98, 183).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r34 = bone9.addOrReplaceChild("cube_r34", CubeListBuilder.create().texOffs(163, 183).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r35 = bone9.addOrReplaceChild("cube_r35", CubeListBuilder.create().texOffs(188, 0).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r36 = bone9.addOrReplaceChild("cube_r36", CubeListBuilder.create().texOffs(188, 18).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition bone10 = partdefinition.addOrReplaceChild("bone10", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0059F, -32.5365F, 0.0064F, 0.0F, -1.309F, 0.0F));

		PartDefinition cube_r37 = bone10.addOrReplaceChild("cube_r37", CubeListBuilder.create().texOffs(172, 56).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r38 = bone10.addOrReplaceChild("cube_r38", CubeListBuilder.create().texOffs(172, 75).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, -1.5708F, 0.0F));

		PartDefinition cube_r39 = bone10.addOrReplaceChild("cube_r39", CubeListBuilder.create().texOffs(0, 179).addBox(-8.0F, -89.5115F, -45.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, -0.6981F, 0.0F, 0.0F));

		PartDefinition cube_r40 = bone10.addOrReplaceChild("cube_r40", CubeListBuilder.create().texOffs(49, 181).addBox(-8.0F, -89.5115F, 29.0469F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 18.5354F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition bone11 = partdefinition.addOrReplaceChild("bone11", CubeListBuilder.create(), PartPose.offset(0.0078F, -35.7095F, 0.0F));

		PartDefinition cube_r41 = bone11.addOrReplaceChild("cube_r41", CubeListBuilder.create().texOffs(93, 163).addBox(-27.4238F, -34.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -58.3569F, 0.0F, 1.5708F, -0.5236F, -1.5708F));

		PartDefinition cube_r42 = bone11.addOrReplaceChild("cube_r42", CubeListBuilder.create().texOffs(44, 161).addBox(11.4238F, -34.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -58.3569F, 0.0F, -1.5708F, -0.5236F, 1.5708F));

		PartDefinition cube_r43 = bone11.addOrReplaceChild("cube_r43", CubeListBuilder.create().texOffs(142, 165).addBox(-27.4238F, -34.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -58.3569F, 0.0F, 0.0F, 0.0F, -1.0472F));

		PartDefinition cube_r44 = bone11.addOrReplaceChild("cube_r44", CubeListBuilder.create().texOffs(172, 38).addBox(11.4238F, -34.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -58.3569F, 0.0F, 0.0F, 0.0F, 1.0472F));

		PartDefinition bone12 = partdefinition.addOrReplaceChild("bone12", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0078F, -35.7095F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r45 = bone12.addOrReplaceChild("cube_r45", CubeListBuilder.create().texOffs(158, 93).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 1.5708F, -0.5236F, -1.5708F));

		PartDefinition cube_r46 = bone12.addOrReplaceChild("cube_r46", CubeListBuilder.create().texOffs(158, 111).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, -1.5708F, -0.5236F, 1.5708F));

		PartDefinition cube_r47 = bone12.addOrReplaceChild("cube_r47", CubeListBuilder.create().texOffs(158, 129).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, -1.0472F));

		PartDefinition cube_r48 = bone12.addOrReplaceChild("cube_r48", CubeListBuilder.create().texOffs(158, 147).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, 1.0472F));

		PartDefinition bone13 = partdefinition.addOrReplaceChild("bone13", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0078F, -35.7095F, 0.0F, 0.0F, -0.2618F, 0.0F));

		PartDefinition cube_r49 = bone13.addOrReplaceChild("cube_r49", CubeListBuilder.create().texOffs(139, 2).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 1.5708F, -0.5236F, -1.5708F));

		PartDefinition cube_r50 = bone13.addOrReplaceChild("cube_r50", CubeListBuilder.create().texOffs(139, 20).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, -1.5708F, -0.5236F, 1.5708F));

		PartDefinition cube_r51 = bone13.addOrReplaceChild("cube_r51", CubeListBuilder.create().texOffs(60, 143).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, -1.0472F));

		PartDefinition cube_r52 = bone13.addOrReplaceChild("cube_r52", CubeListBuilder.create().texOffs(109, 145).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, 1.0472F));

		PartDefinition bone14 = partdefinition.addOrReplaceChild("bone14", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0078F, -35.7095F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r53 = bone14.addOrReplaceChild("cube_r53", CubeListBuilder.create().texOffs(123, 54).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 1.5708F, -0.5236F, -1.5708F));

		PartDefinition cube_r54 = bone14.addOrReplaceChild("cube_r54", CubeListBuilder.create().texOffs(123, 73).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, -1.5708F, -0.5236F, 1.5708F));

		PartDefinition cube_r55 = bone14.addOrReplaceChild("cube_r55", CubeListBuilder.create().texOffs(60, 125).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, -1.0472F));

		PartDefinition cube_r56 = bone14.addOrReplaceChild("cube_r56", CubeListBuilder.create().texOffs(109, 127).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, 1.0472F));

		PartDefinition bone15 = partdefinition.addOrReplaceChild("bone15", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0078F, -35.7095F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r57 = bone15.addOrReplaceChild("cube_r57", CubeListBuilder.create().texOffs(90, 36).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 1.5708F, -0.5236F, -1.5708F));

		PartDefinition cube_r58 = bone15.addOrReplaceChild("cube_r58", CubeListBuilder.create().texOffs(60, 107).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, -1.5708F, -0.5236F, 1.5708F));

		PartDefinition cube_r59 = bone15.addOrReplaceChild("cube_r59", CubeListBuilder.create().texOffs(109, 91).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, -1.0472F));

		PartDefinition cube_r60 = bone15.addOrReplaceChild("cube_r60", CubeListBuilder.create().texOffs(109, 109).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, 1.0472F));

		PartDefinition bone16 = partdefinition.addOrReplaceChild("bone16", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0078F, -35.7095F, 0.0F, 0.0F, -1.309F, 0.0F));

		PartDefinition cube_r61 = bone16.addOrReplaceChild("cube_r61", CubeListBuilder.create().texOffs(74, 71).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 1.5708F, -0.5236F, -1.5708F));

		PartDefinition cube_r62 = bone16.addOrReplaceChild("cube_r62", CubeListBuilder.create().texOffs(60, 89).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, -1.5708F, -0.5236F, 1.5708F));

		PartDefinition cube_r63 = bone16.addOrReplaceChild("cube_r63", CubeListBuilder.create().texOffs(90, 0).addBox(52.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, -1.0472F));

		PartDefinition cube_r64 = bone16.addOrReplaceChild("cube_r64", CubeListBuilder.create().texOffs(90, 18).addBox(-68.2505F, -80.1431F, -8.0F, 16.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 33.6431F, 0.0F, 0.0F, 0.0F, 1.0472F));

		PartDefinition bone17 = partdefinition.addOrReplaceChild("bone17", CubeListBuilder.create(), PartPose.offset(0.0089F, 8.6119F, 0.0F));

		PartDefinition cube_r65 = bone17.addOrReplaceChild("cube_r65", CubeListBuilder.create().texOffs(0, 287).addBox(-4.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(286, 0).addBox(-4.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -92.0F, 0.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r66 = bone17.addOrReplaceChild("cube_r66", CubeListBuilder.create().texOffs(263, 288).addBox(-4.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(33, 289).addBox(-4.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -92.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition bone18 = partdefinition.addOrReplaceChild("bone18", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0089F, 8.6119F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition cube_r67 = bone18.addOrReplaceChild("cube_r67", CubeListBuilder.create().texOffs(197, 275).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(230, 277).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r68 = bone18.addOrReplaceChild("cube_r68", CubeListBuilder.create().texOffs(278, 252).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(278, 270).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition bone19 = partdefinition.addOrReplaceChild("bone19", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0089F, 8.6119F, 0.0F, 0.0F, -0.2618F, 0.0F));

		PartDefinition cube_r69 = bone19.addOrReplaceChild("cube_r69", CubeListBuilder.create().texOffs(33, 271).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(66, 273).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r70 = bone19.addOrReplaceChild("cube_r70", CubeListBuilder.create().texOffs(115, 273).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(164, 273).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition bone20 = partdefinition.addOrReplaceChild("bone20", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0089F, 8.6119F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r71 = bone20.addOrReplaceChild("cube_r71", CubeListBuilder.create().texOffs(0, 269).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(270, 20).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r72 = bone20.addOrReplaceChild("cube_r72", CubeListBuilder.create().texOffs(270, 38).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(270, 56).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition bone21 = partdefinition.addOrReplaceChild("bone21", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0089F, 8.6119F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r73 = bone21.addOrReplaceChild("cube_r73", CubeListBuilder.create().texOffs(261, 180).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(261, 198).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r74 = bone21.addOrReplaceChild("cube_r74", CubeListBuilder.create().texOffs(261, 216).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(261, 234).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition bone22 = partdefinition.addOrReplaceChild("bone22", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0089F, 8.6119F, 0.0F, 0.0F, -1.309F, 0.0F));

		PartDefinition cube_r75 = bone22.addOrReplaceChild("cube_r75", CubeListBuilder.create().texOffs(256, 144).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(212, 257).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r76 = bone22.addOrReplaceChild("cube_r76", CubeListBuilder.create().texOffs(245, 259).addBox(-96.0F, 42.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(261, 162).addBox(-96.0F, -43.2754F, -8.0F, 8.0F, 1.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition bone23 = partdefinition.addOrReplaceChild("bone23", CubeListBuilder.create(), PartPose.offset(0.0137F, 31.1182F, 0.5F));

		PartDefinition cube_r77 = bone23.addOrReplaceChild("cube_r77", CubeListBuilder.create().texOffs(55, 87).addBox(-22.0136F, -48.4209F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-22.0136F, -48.4209F, -0.5F, 1.0F, 100.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -79.5791F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r78 = bone23.addOrReplaceChild("cube_r78", CubeListBuilder.create().texOffs(50, 87).addBox(21.0136F, -48.4209F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(5, 0).addBox(21.0136F, -48.4209F, -0.5F, 1.0F, 100.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -79.5791F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone24 = partdefinition.addOrReplaceChild("bone24", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0137F, 31.1182F, 0.5F, 0.0F, -1.5708F, 0.0F));

		PartDefinition cube_r79 = bone24.addOrReplaceChild("cube_r79", CubeListBuilder.create().texOffs(30, 87).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(45, 87).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r80 = bone24.addOrReplaceChild("cube_r80", CubeListBuilder.create().texOffs(35, 87).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(40, 87).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone25 = partdefinition.addOrReplaceChild("bone25", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0137F, 31.1182F, 0.5F, 0.0F, -0.5236F, 0.0F));

		PartDefinition cube_r81 = bone25.addOrReplaceChild("cube_r81", CubeListBuilder.create().texOffs(10, 87).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(25, 87).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r82 = bone25.addOrReplaceChild("cube_r82", CubeListBuilder.create().texOffs(15, 87).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 87).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone26 = partdefinition.addOrReplaceChild("bone26", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0137F, 31.1182F, 0.5F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r83 = bone26.addOrReplaceChild("cube_r83", CubeListBuilder.create().texOffs(70, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r84 = bone26.addOrReplaceChild("cube_r84", CubeListBuilder.create().texOffs(75, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(80, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone27 = partdefinition.addOrReplaceChild("bone27", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0137F, 31.1182F, 0.5F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r85 = bone27.addOrReplaceChild("cube_r85", CubeListBuilder.create().texOffs(50, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(65, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r86 = bone27.addOrReplaceChild("cube_r86", CubeListBuilder.create().texOffs(55, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(60, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone28 = partdefinition.addOrReplaceChild("bone28", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0137F, 31.1182F, 0.5F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r87 = bone28.addOrReplaceChild("cube_r87", CubeListBuilder.create().texOffs(30, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(45, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r88 = bone28.addOrReplaceChild("cube_r88", CubeListBuilder.create().texOffs(35, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(40, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone29 = partdefinition.addOrReplaceChild("bone29", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0137F, 31.1182F, 0.5F, 0.0F, -2.618F, 0.0F));

		PartDefinition cube_r89 = bone29.addOrReplaceChild("cube_r89", CubeListBuilder.create().texOffs(10, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(25, 0).addBox(23.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, -0.5236F));

		PartDefinition cube_r90 = bone29.addOrReplaceChild("cube_r90", CubeListBuilder.create().texOffs(15, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(20, 0).addBox(-24.9864F, -128.0952F, -0.5F, 1.0F, 85.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 12.4209F, 0.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition bone30 = partdefinition.addOrReplaceChild("bone30", CubeListBuilder.create().texOffs(82, 291).addBox(-3.0F, -93.0F, -1.0F, 18.0F, 18.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, 99.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 512, 512);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		bone.render(poseStack, buffer, packedLight, packedOverlay);
		bone2.render(poseStack, buffer, packedLight, packedOverlay);
		bone3.render(poseStack, buffer, packedLight, packedOverlay);
		bone4.render(poseStack, buffer, packedLight, packedOverlay);
		bone5.render(poseStack, buffer, packedLight, packedOverlay);
		bone6.render(poseStack, buffer, packedLight, packedOverlay);
		bone7.render(poseStack, buffer, packedLight, packedOverlay);
		bone8.render(poseStack, buffer, packedLight, packedOverlay);
		bone9.render(poseStack, buffer, packedLight, packedOverlay);
		bone10.render(poseStack, buffer, packedLight, packedOverlay);
		bone11.render(poseStack, buffer, packedLight, packedOverlay);
		bone12.render(poseStack, buffer, packedLight, packedOverlay);
		bone13.render(poseStack, buffer, packedLight, packedOverlay);
		bone14.render(poseStack, buffer, packedLight, packedOverlay);
		bone15.render(poseStack, buffer, packedLight, packedOverlay);
		bone16.render(poseStack, buffer, packedLight, packedOverlay);
		bone17.render(poseStack, buffer, packedLight, packedOverlay);
		bone18.render(poseStack, buffer, packedLight, packedOverlay);
		bone19.render(poseStack, buffer, packedLight, packedOverlay);
		bone20.render(poseStack, buffer, packedLight, packedOverlay);
		bone21.render(poseStack, buffer, packedLight, packedOverlay);
		bone22.render(poseStack, buffer, packedLight, packedOverlay);
		bone23.render(poseStack, buffer, packedLight, packedOverlay);
		bone24.render(poseStack, buffer, packedLight, packedOverlay);
		bone25.render(poseStack, buffer, packedLight, packedOverlay);
		bone26.render(poseStack, buffer, packedLight, packedOverlay);
		bone27.render(poseStack, buffer, packedLight, packedOverlay);
		bone28.render(poseStack, buffer, packedLight, packedOverlay);
		bone29.render(poseStack, buffer, packedLight, packedOverlay);
		bone30.render(poseStack, buffer, packedLight, packedOverlay);
	}
}