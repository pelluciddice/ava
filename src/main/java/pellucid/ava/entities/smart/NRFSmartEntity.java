package pellucid.ava.entities.smart;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.smart.goals.AVADisarmC4Goal;
import pellucid.ava.entities.smart.goals.AVAFindC4Goal;
import pellucid.ava.entities.smart.goals.AVALookRandomlyGoal;
import pellucid.ava.entities.smart.goals.AVAMoveToPosGoal;
import pellucid.ava.entities.smart.goals.AVANearestOpponentTargetGoal;
import pellucid.ava.entities.smart.goals.AVARandomWalkingGoal;
import pellucid.ava.entities.smart.goals.AVARangedAttackGoal;
import pellucid.ava.entities.smart.goals.MoveToLeaderGoal;
import pellucid.ava.entities.smart.goals.MoveToPingGoal;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;

import java.util.List;

public class NRFSmartEntity extends SidedSmartAIEntity
{
    public boolean holdsRPG = false;

    public final TargetingConditions entityLeopardPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        if (!entityPredicate.test(entity) || !holdsRPG)
            return false;
        return entity instanceof LeopardEntity;
    });

    public final TargetingConditions entityAttackLeopardPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        return entityLeopardPredicate.test(this, entity) && canSee(entity) && !((LeopardEntity) entity).requireRepair();
    });

    public NRFSmartEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected boolean inSight(Entity entity)
    {
        return AVAWeaponUtil.isInSight(this, entity, true, !holdsRPG, 70, 50, true, false, false);
    }

    @Override
    public double getMaxAttackDistance()
    {
        return super.getMaxAttackDistance() * (holdsRPG ? 1.5F : 1.0F);
    }

    @Override
    public void findFarTarget()
    {
        if (holdsRPG)
        {
            if (canFindFarTarget())
            {
                double r = findFarTargetRadius();
                if (r > 0)
                {
                    LivingEntity target = level().getNearestEntity(LeopardEntity.class, entityLeopardPredicate, this, getX(), getY(), getZ(), getBoundingBox().inflate(r, r / 2.0D, r));
                    if (target != null)
                        setTarget(target);
                }
            }
        }
        else
            super.findFarTarget();
    }

    @Override
    protected void registerGoals()
    {
        if (GameModes.ESCORT.isRunning())
        {
            targetSelector.addGoal(0, new AVANearestOpponentTargetGoal(this, () -> entityAttackLeopardPredicate, () -> entityLeopardPredicate) {
                @Override
                public boolean canUse()
                {
                    return holdsRPG && super.canUse();
                }
            });
            targetSelector.addGoal(1, new AVANearestOpponentTargetGoal(this, () -> entityAttackOpponentTargetPredicate, () -> entityOpponentTargetPredicate));
            goalSelector.addGoal(0, new AvoidEntityGoal<>(this, LeopardEntity.class, (e) -> true, 3, 1.0F, 1.0F, (e) -> e instanceof LeopardEntity));
            goalSelector.addGoal(1, new AVARangedAttackGoal(this, () -> holdsRPG ? 0 : 0.75F));
            goalSelector.addGoal(2, new MoveToPingGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(3, new MoveToLeaderGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(5, new MoveTowardsTargetGoal(this, 0.7F, (float) (getMaxAttackDistance() * 3.0F)) {
                @Override
                public boolean canUse()
                {
                    return getTarget() != null && !canSee(getTarget()) && super.canUse();
                }
            });
            goalSelector.addGoal(6, new AVAMoveToPosGoal(this, () -> holdsRPG || random.nextBoolean() ? null : lastTargetPos, () -> lastTargetPos = null));
            goalSelector.addGoal(9, new AVALookRandomlyGoal(this));
            goalSelector.addGoal(9, new AVARandomWalkingGoal(this));
            goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 20.0F, 0.125F));
        }
        else
        {
            targetSelector.addGoal(0, new AVANearestOpponentTargetGoal(this, () -> entityAttackOpponentTargetPredicate, () -> entityOpponentTargetPredicate));
            targetSelector.addGoal(1, new AVANearestOpponentTargetGoal(this, () -> entityAttackTargetPredicate, () -> entityTargetPredicate));
            goalSelector.addGoal(0, new AVARangedAttackGoal(this, () -> 0.15F));
            goalSelector.addGoal(0, new AVADisarmC4Goal(this));
            goalSelector.addGoal(1, new AVAFindC4Goal(this));
            goalSelector.addGoal(2, new MoveToPingGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(3, new MoveToLeaderGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(5, new MoveTowardsTargetGoal(this, 0.7F, (float) (getMaxAttackDistance() * 3.0F)));
            sitePos = Lazy.of(() ->
            {
                if (GameModes.DEMOLISH.isRunning())
                    return random.nextBoolean() ? null : Pair.or(AVAWorldData.getInstance(level()).sitesMark, random.nextBoolean());
                return null;
            });
            goalSelector.addGoal(6, new AVAMoveToPosGoal(this, () -> lastTargetPos, () -> lastTargetPos = null));
            goalSelector.addGoal(8, new AVAMoveToPosGoal(this, sitePos, () -> sitePos = null));
            goalSelector.addGoal(9, new AVALookRandomlyGoal(this));
            goalSelector.addGoal(9, new AVARandomWalkingGoal(this));
            goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 20.0F, 0.125F));
        }
    }

    @Override
    public ItemStack getInitialMainWeapon()
    {
        if (GameModes.ESCORT.isRunning())
        {
            if (GameModes.ESCORT.requireMoreRPGs(level()))
            {
                holdsRPG = true;
                GameModes.ESCORT.nrfRPGCount++;
                return new ItemStack(SpecialWeapons.RPG7.get());
            }
        }
        List<Item> weapons = getSide().getWeapons();
        return weapons.isEmpty() ? ItemStack.EMPTY : new ItemStack(weapons.get(random.nextInt(weapons.size())));
    }

    @Override
    public void remove(RemovalReason reason)
    {
        super.remove(reason);
        if (GameModes.ESCORT.isRunning() && reason.shouldDestroy() && holdsRPG)
            GameModes.ESCORT.nrfRPGCount--;
    }

    @Override
    public Class<?> getThis()
    {
        return NRFSmartEntity.class;
    }

    @Override
    public AVAWeaponUtil.TeamSide getSide()
    {
        return AVAWeaponUtil.TeamSide.NRF;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.BOOLEAN.write(compound, "holdsRPG", holdsRPG);
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        holdsRPG = DataTypes.BOOLEAN.read(compound, "holdsRPG");
    }
}
