package pellucid.ava.entities.smart;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.pathfinder.Path;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.blocks.IInteractable;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.base.IAVARangedAttackMob;
import pellucid.ava.entities.functionalities.IDifficultyScaledHostile;
import pellucid.ava.entities.functionalities.IObjectPickable;
import pellucid.ava.entities.goals.AIGunManager;
import pellucid.ava.entities.livings.AVABotEntity;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.items.armours.AVAArmours;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Lazy;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

public abstract class SidedSmartAIEntity extends AVABotEntity implements AVAWeaponUtil.ISidedEntity, IAVARangedAttackMob, IObjectPickable, IDifficultyScaledHostile, IInteractable<EntityHitResult>
{
    public BlockPos lastTargetPos;
    protected UUID c4Entity;
    private final AIGunManager manager = new AIGunManager(this);
    private static final EntityDataAccessor<Boolean> PARACHUTED = SynchedEntityData.defineId(SidedSmartAIEntity.class, EntityDataSerializers.BOOLEAN);
    public static final EntityDataAccessor<Boolean> IS_ACTIVE = SynchedEntityData.defineId(SidedSmartAIEntity.class, EntityDataSerializers.BOOLEAN);
    public static final EntityDataAccessor<Optional<UUID>> LEADER = SynchedEntityData.defineId(SidedSmartAIEntity.class, EntityDataSerializers.OPTIONAL_UUID);
    public static final EntityDataAccessor<Integer> LEADER_ID = SynchedEntityData.defineId(SidedSmartAIEntity.class, EntityDataSerializers.INT);

    protected final Predicate<Entity> entityPredicate = (entity) -> {
        if (entity instanceof Player && (((Player) entity).getAbilities().instabuild))
            return false;
        return AVAWeaponUtil.isValidEntity(entity) && distanceTo(entity) <= getMaxAttackDistance() * 5.0F && !(entity instanceof ArmorStand) && entity != this;
    };

    public final TargetingConditions entityTargetPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        if (!entityPredicate.test(entity))
            return false;
        return entity instanceof Enemy && !(entity instanceof AVABotEntity);
    });

    public final TargetingConditions entityAttackTargetPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        return entityTargetPredicate.test(this, entity) && canSee(entity);
    });

    public final TargetingConditions entityOpponentTargetPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        if (!entityPredicate.test(entity))
            return false;
        return isAggressive(entity);
    });

    public final TargetingConditions entityAttackOpponentTargetPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        return entityOpponentTargetPredicate.test(this, entity) && canSee(entity);
    });

    public final TargetingConditions entityAttackPredicate = TargetingConditions.forNonCombat().ignoreLineOfSight().selector((entity) -> {
        if (!entityPredicate.test(entity))
            return false;
        return !(entity instanceof Player) || !((Player) entity).getAbilities().instabuild;
    });

    protected int activeTicks;


    protected final Map<UUID, Boolean> visibilities = new HashMap<>();
    protected final Map<UUID, Integer> visibilityTolerance = new HashMap<>();

    public SidedSmartAIEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    public String getInteractableTip()
    {
        return "ava.interaction.smart_entity";
    }

    @Override
    public int getDuration()
    {
        return 0;
    }

    @Override
    public int cd()
    {
        return 40;
    }

    @Override
    public double maxDistance()
    {
        return IInteractable.super.maxDistance() * 4.0F;
    }

    @Override
    public boolean canInteract(EntityHitResult result, LivingEntity interactor, BlockPos pos)
    {
        return getSide().isSameSide(interactor);
    }

    @Override
    public void interact(Level world, EntityHitResult result, @org.jetbrains.annotations.Nullable BlockPos pos, @org.jetbrains.annotations.Nullable Vec3 vector3d, LivingEntity interacter)
    {
        if (!level().isClientSide())
        {
            LivingEntity leader = getLeader();
            if (leader == interacter)
                setLeader(null);
            else
                setLeader(interacter);
            interacter.sendSystemMessage(Component.translatable("ava.chat.entity_now_following_leader", hasCustomName() ? getCustomName() : getDisplayName(), getLeader() == null ? "[]" : interacter.getDisplayName()));
            playSound(AVAWeaponUtil.RadioMessage.X1.getSound(this), 1.0F, 1.0F);
        }
    }

    public Vec3 pingLoc = null;

    public void moveToPing(Vec3 pingLoc)
    {
        this.pingLoc = pingLoc;
    }

    public Vec3 getPingLoc()
    {
        return pingLoc;
    }

    public boolean isAggressive(LivingEntity target)
    {
        return getSide().isOppositeSide(target);
    }

    @Override
    public boolean isPersistenceRequired()
    {
        return AVAServerConfig.isCompetitiveModeActivated();
    }

    @Override
    protected void defineSynchedData(SynchedEntityData.Builder builder)
    {
        super.defineSynchedData(builder);
        builder.define(PARACHUTED, false);
        builder.define(IS_ACTIVE, false);
        builder.define(LEADER, Optional.empty());
        builder.define(LEADER_ID, -1);
    }

    protected boolean hasLeader()
    {
        if (getLeader() == null)
        {
            if (!level().isClientSide())
                setLeader(null);
            return false;
        }
        return true;
    }

    protected void setLeader(Entity leader)
    {
        getEntityData().set(LEADER, Optional.ofNullable(leader == null ? null : leader.getUUID()));
        setLeaderID(leader == null ? -1 : leader.getId());
    }

    protected int getLeaderID()
    {
        return getEntityData().get(LEADER_ID);
    }

    protected void setLeaderID(int id)
    {
        getEntityData().set(LEADER_ID, id);
    }

    @Nullable
    public LivingEntity getLeader()
    {
        Optional<UUID> leader = getEntityData().get(LEADER);
        Entity entity = null;
        if (level() instanceof ServerLevel)
            entity = leader.map((value) -> ((ServerLevel) this.level()).getEntity(value)).orElse(null);
        else if (getLeaderID() != -1)
            entity = level().getEntity(getLeaderID());
        return entity == null ? null : (LivingEntity) entity;
    }

    @Override
    public Component getDisplayName()
    {
        return hasLeader() ? super.getDisplayName().copy().append(" ").append(Component.translatable("ava.chat.entity_following_leader", getLeader().getDisplayName().getString())) : super.getDisplayName();
    }

    @Override
    public boolean shouldShowName()
    {
        return false;
    }

    public void rotateTowardsEntity(Entity entity)
    {
        lookAtEntity(entity);
        lookAt(entity, 360, 360);
    }

    public void lookAtEntity(Entity entity)
    {
        getLookControl().setLookAt(entity, 30, 30);
    }

    @Override
    public boolean hurt(DamageSource source, float amount)
    {
        boolean attacked = super.hurt(source, amount);
        if (!level().isClientSide())
        {
            Entity attacker = source.getEntity();
            if (attacker instanceof LivingEntity)
                warnNearbyAllies((LivingEntity) attacker);
        }
        return attacked;
    }

    public void warnNearbyAllies(@Nullable LivingEntity attacker)
    {
        if (attacker == null)
            return;
        boolean isPlayer = attacker instanceof Player;
        boolean competitive = AVAServerConfig.isCompetitiveModeActivated();

        if (isPlayer && competitive && getSide().isSameSide(attacker))
            return;
        if (isPlayer && ((Player) attacker).getAbilities().instabuild)
            return;
        double r = getWarningRadius();
        setCommonAttackTarget(attacker);
        AABB bb = AABB.unitCubeFromLowerCorner(position()).inflate(r, r / 2.0F, r);

        AVAWeaponUtil.warnNearbySmarts(getClass(), this, bb, attacker);
    }

    protected double getWarningRadius()
    {
        return GameModes.ESCORT.isRunning() ? 2.0F : 5.0D;
    }

    public void setCommonAttackTarget(LivingEntity target)
    {
        if (getTarget() == null)
            setTarget(target);
    }

    protected Lazy<BlockPos> sitePos;

    @Override
    public ItemStack getInitialMainWeapon()
    {
        List<Item> weapons = getSide().getWeapons();
        return weapons.isEmpty() ? ItemStack.EMPTY : new ItemStack(weapons.get(random.nextInt(weapons.size())));
    }

    public abstract Class<?> getThis();

    public double getTrackingRange()
    {
        AttributeInstance attribute = getAttribute(Attributes.FOLLOW_RANGE);
        if (attribute == null)
            return 0.0F;
        return attribute.getValue();
    }

    public double getMaxAttackDistance()
    {
        return getMainHandItem().getItem() instanceof AVAItemGun ? ((AVAItemGun) getMainHandItem().getItem()).getRange(getMainHandItem(), true) * 0.7F : getTrackingRange();
    }

    public final boolean canSee(Entity entity)
    {
        if (entity == null)
            return false;
        return visibilities.computeIfAbsent(entity.getUUID(), (id) -> {
            boolean inSight = inSight(entity);
            if (inSight)
                visibilityTolerance.put(id, 40);
            return visibilityTolerance.containsKey(id);
        });
    }

    protected boolean inSight(Entity entity)
    {
        return AVAWeaponUtil.isInSight(this, entity, true, true, 70, 50, true, false, false);
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn)
    {
        SoundEvent sound = AVACommonUtil.getStepSoundFor(this, true);
        if (sound != null)
            playSound(sound, 0.85F, 1.0F);
    }

    @Override
    public float getSpread(float spread)
    {
        return 2.0F;
    }

    protected boolean isActive()
    {
        return getEntityData().get(IS_ACTIVE);
    }

    protected void setActive(boolean active)
    {
        getEntityData().set(IS_ACTIVE, active);
    }

    protected int pathFindDuration = 0;

    @Override
    public void aiStep()
    {
        manager.tick();
        visibilities.clear();
        AVACommonUtil.decreaseAndRemove(visibilityTolerance);
        super.aiStep();
        if (!level().isClientSide())
        {
            if (getTarget() == null)
            {
                if (isActive() && activeTicks-- <= 0)
                    setActive(false);
            }
            else
            {
                activeTicks = 60;
                if (!isActive())
                    setActive(true);
            }
            if (getLeader() != null && getSide().isOppositeSide(getLeader()))
                setLeader(null);
        }
        ItemStack stack = getMainHandItem();
        if (stack.getItem() instanceof AVAItemGun && !level().isClientSide())
        {
            AVAItemGun gun = (AVAItemGun) stack.getItem();

            if (stack.getOrDefault(AVADataComponents.TAG_ITEM_TICKS, 0).floatValue() > 0)
                stack.set(AVADataComponents.TAG_ITEM_TICKS, stack.getOrDefault(AVADataComponents.TAG_ITEM_TICKS, 0).floatValue() - 1);
            if (stack.getOrDefault(AVADataComponents.TAG_ITEM_FIRE, 0) > 0)
            {
                stack.set(AVADataComponents.TAG_ITEM_FIRE, stack.getOrDefault(AVADataComponents.TAG_ITEM_FIRE, 0) + 1);
                if (stack.getOrDefault(AVADataComponents.TAG_ITEM_FIRE, 0) >= gun.getFireAnimation(stack) + 2)
                    stack.set(AVADataComponents.TAG_ITEM_FIRE, 0);
            }
            if (stack.getOrDefault(AVADataComponents.TAG_ITEM_RELOAD, 0) > 0)
            {
                stack.getOrDefault(AVADataComponents.TAG_ITEM_RELOAD, stack.getOrDefault(AVADataComponents.TAG_ITEM_RELOAD, 0) + 1);
                if (stack.getOrDefault(AVADataComponents.TAG_ITEM_RELOAD, 0) >= gun.getReloadTime(stack) && gun.reload(this, stack, AVAServerConfig.isCompetitiveModeActivated()))
                    gun.postReload(stack);
            }
        }
        if (getTarget() != null && (!AVAWeaponUtil.isValidEntity(getTarget()) || (getTarget().distanceTo(this) >= 150.0F) || !entityPredicate.test(getTarget())))
            setTarget(null);
        if (!level().isClientSide())
        {
            if (getNavigation().isInProgress())
            {
                if (pathFindDuration++ >= 900 && !canSee(getTarget()))
                {
                    getNavigation().moveTo((Path) null, 0.7F);
                    if (lastTargetPos != null)
                        lastTargetPos = null;
                }
            }
            else pathFindDuration = 0;

            if (shouldFindFarTarget())
                findFarTarget();
            if (!onGround())
            {
                int ground = level().getHeight(Heightmap.Types.MOTION_BLOCKING, blockPosition().getX(), blockPosition().getZ());
                if (ground > level().getMinBuildHeight())
                {
                    double height = getY() - ground;
                    if (height > 4 && height <= 180 && !parachuted())
                    {
                        setParachuted(true);
                        AVAWeaponUtil.playAttenuableSoundToClientMoving(AVASounds.PARACHUTE_OPEN.get(), this, 1.5F);
                    }
                }
            }
            else if (parachuted())
                setParachuted(false);
        }
    }

    public boolean shouldFindFarTarget()
    {
        return AVAServerConfig.isCompetitiveModeActivated() && tickCount % (GameModes.ANNIHILATION.isRunning() ? 160 : GameModes.DEMOLISH.isRunning() ? 800 : GameModes.ESCORT.isRunning() ? 200 : 10000) == 0 && getTarget() == null && getC4Entity() == null;
    }

    public boolean parachuted()
    {
        return getEntityData().get(PARACHUTED);
    }

    public void setParachuted(boolean value)
    {
        getEntityData().set(PARACHUTED, value);
        AttributeInstance attribute = getAttribute(Attributes.GRAVITY);
        if (value && attribute != null && !attribute.hasModifier(AVAConstants.SLOW_FALLING_MODIFIER))
            attribute.addTransientModifier(AVAConstants.SLOW_FALLING_MODIFIER);
        else if (!value && attribute != null && attribute.hasModifier(AVAConstants.SLOW_FALLING_MODIFIER))
            attribute.removeModifier(AVAConstants.SLOW_FALLING_MODIFIER.id());
    }

    public boolean canFindFarTarget()
    {
        return getTarget() == null && AVAServerConfig.isCompetitiveModeActivated();
    }

    public void findFarTarget()
    {
        if (canFindFarTarget())
        {
            double r = findFarTargetRadius();
            if (r > 0)
            {
                LivingEntity target = level().getNearestEntity(LivingEntity.class, entityOpponentTargetPredicate, this, getX(), getY(), getZ(), getBoundingBox().inflate(r, r / 2.0D, r));
                if (target != null)
                    setTarget(target);
            }
        }
    }

    protected double findFarTargetRadius()
    {
        return getTrackingRange() * 2.5F;
    }

    @Override
    protected void populateDefaultEquipmentSlots(RandomSource random, DifficultyInstance difficulty)
    {
        super.populateDefaultEquipmentSlots(random, difficulty);
        List<AVAArmours> armourSet = getSide() == AVAWeaponUtil.TeamSide.EU ? AVAArmours.EU_ARMOURS : getSide() == AVAWeaponUtil.TeamSide.NRF ? AVAArmours.NRF_ARMOURS : Collections.emptyList();
        if (!armourSet.isEmpty())
            for (int i=0;i<4;i++)
                setItemSlot(EquipmentSlot.byTypeAndIndex(EquipmentSlot.Type.ARMOR, i), new ItemStack(armourSet.get(i)));
        setCustomName(getSide().randomName());
    }

    public static AttributeSupplier.Builder registerAttributes()
    {
        return Monster.createMonsterAttributes()
                .add(Attributes.MAX_HEALTH, 18.0F)
                .add(Attributes.MOVEMENT_SPEED, 0.4D)
                .add(Attributes.ATTACK_KNOCKBACK, 0.0F)
                .add(Attributes.KNOCKBACK_RESISTANCE, 1.0F)
                .add(Attributes.FOLLOW_RANGE, 50.0F);
    }

    @Override
    public void performRangedAttack(LivingEntity target, float distanceFactor)
    {
        manager.tryFire(target);
    }

    @Nullable
    public C4Entity getC4Entity()
    {
        Entity entity;
        if (level() instanceof ServerLevel)
        {
            entity = ((ServerLevel) level()).getEntity(c4Entity);
            if (entity instanceof C4Entity)
                return (C4Entity) entity;
        }
        return null;
    }

    public void setC4Entity(UUID c4Entity)
    {
        this.c4Entity = c4Entity;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.put("aiGunManager", manager.serializeNBT());
        if (lastTargetPos != null)
            DataTypes.BLOCKPOS.write(compound, "lastTargetPos", lastTargetPos);
        if (c4Entity != null)
            DataTypes.UUID.write(compound, "c4", c4Entity);
        DataTypes.INT.write(compound, "pathFindDuration", pathFindDuration);
        DataTypes.INT.write(compound, "tickExisted", tickCount);
        DataTypes.BOOLEAN.write(compound, "parachute", parachuted());
        DataTypes.BOOLEAN.write(compound, "active", isActive());
        LivingEntity leader = getLeader();
        if (leader != null)
            DataTypes.UUID.write(compound, "leader", leader.getUUID());
        if (getPingLoc() != null)
            DataTypes.COMPOUND.write(compound, "pingLoc", AVAWeaponUtil.writeVec(new CompoundTag(), getPingLoc()));
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        manager.deserializeNBT(compound.getCompound("aiGunManager"));
        if (compound.contains("lastTargetPos"))
            lastTargetPos = DataTypes.BLOCKPOS.read(compound, "lastTargetPos");
        if (compound.contains("c4"))
            c4Entity = DataTypes.UUID.read(compound, "c4");
        pathFindDuration = DataTypes.INT.read(compound, "pathFindDuration");
        tickCount = DataTypes.INT.read(compound, "tickExisted");
        setParachuted(DataTypes.BOOLEAN.read(compound, "parachute"));
        setActive(DataTypes.BOOLEAN.read(compound, "active"));
        getEntityData().set(LEADER, Optional.ofNullable(DataTypes.UUID.readSafe(compound, "leader", null)));
        if (compound.contains("pingLoc"))
            moveToPing(AVAWeaponUtil.readVec(DataTypes.COMPOUND.read(compound, "pingLoc")));
    }

    @Override
    public boolean shouldRenderAtSqrDistance(double dist)
    {
        return true;
    }

    @Override
    protected void dropCustomDeathLoot(DamageSource source, int looting, boolean recentlyHitIn)
    {
        if (!AVAServerConfig.isCompetitiveModeActivated())
            super.dropCustomDeathLoot(source, looting, recentlyHitIn);
        else
        {
            ItemStack stack = getMainHandItem();
            if (AVAWeaponUtil.isPrimaryWeapon(stack.getItem()))
            {
                spawnAtLocation(stack);
                int max = ((AVAItemGun) stack.getItem()).getCapacity(stack, true) * 3;
                stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, max / 2 + random.nextInt(max / 2));
                setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
            }
        }
    }
}
