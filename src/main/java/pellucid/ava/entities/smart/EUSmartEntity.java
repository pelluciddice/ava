package pellucid.ava.entities.smart;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.smart.goals.AVALookRandomlyGoal;
import pellucid.ava.entities.smart.goals.AVAMoveToPosGoal;
import pellucid.ava.entities.smart.goals.AVANearestOpponentTargetGoal;
import pellucid.ava.entities.smart.goals.AVARandomWalkingGoal;
import pellucid.ava.entities.smart.goals.AVARangedAttackGoal;
import pellucid.ava.entities.smart.goals.EscortLeopardGoal;
import pellucid.ava.entities.smart.goals.MoveToLeaderGoal;
import pellucid.ava.entities.smart.goals.MoveToPingGoal;
import pellucid.ava.entities.smart.goals.RepairLeopardGoal;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.util.AVAWeaponUtil;

public class EUSmartEntity extends SidedSmartAIEntity
{
    public EUSmartEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(type, worldIn);
    }

    @Override
    protected void registerGoals()
    {
        if (GameModes.ESCORT.isRunning())
        {
            targetSelector.addGoal(0, new AVANearestOpponentTargetGoal(this, () -> entityAttackOpponentTargetPredicate, () -> entityOpponentTargetPredicate));
            goalSelector.addGoal(0, new AVARangedAttackGoal(this, () -> 0.75F));
            goalSelector.addGoal(0, new RepairLeopardGoal(this));
            goalSelector.addGoal(0, new EscortLeopardGoal(this, 0.7F, (float) (getMaxAttackDistance() * 3.0F)));
            goalSelector.addGoal(2, new MoveToPingGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(3, new MoveToLeaderGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(5, new MoveTowardsTargetGoal(this, 0.7F, (float) (getMaxAttackDistance() * 3.0F)));
            goalSelector.addGoal(6, new AVAMoveToPosGoal(this, () -> lastTargetPos, () -> lastTargetPos = null));
            goalSelector.addGoal(9, new AVALookRandomlyGoal(this));
            goalSelector.addGoal(9, new AVARandomWalkingGoal(this));
            goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 20.0F, 0.125F));
        }
        else
        {
            targetSelector.addGoal(0, new AVANearestOpponentTargetGoal(this, () -> entityAttackOpponentTargetPredicate, () -> entityOpponentTargetPredicate));
            targetSelector.addGoal(1, new AVANearestOpponentTargetGoal(this, () -> entityAttackTargetPredicate, () -> entityTargetPredicate));
            goalSelector.addGoal(0, new AVARangedAttackGoal(this, () -> 0.15F));
            goalSelector.addGoal(2, new MoveToPingGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(3, new MoveToLeaderGoal(this, 0.8F, 70.0F));
            goalSelector.addGoal(5, new MoveTowardsTargetGoal(this, 0.7F, (float) (getMaxAttackDistance() * 3.0F)));
            goalSelector.addGoal(6, new AVAMoveToPosGoal(this, () -> lastTargetPos, () -> lastTargetPos = null));
            goalSelector.addGoal(9, new AVALookRandomlyGoal(this));
            goalSelector.addGoal(9, new AVARandomWalkingGoal(this));
            goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 20.0F, 0.125F));
        }
    }

    @Override
    public boolean shouldFindFarTarget()
    {
        if (GameModes.ESCORT.isRunning())
            return random.nextFloat() <= 0.01F;
        return super.shouldFindFarTarget();
    }

    @Override
    public Class<?> getThis()
    {
        return EUSmartEntity.class;
    }

    @Override
    public AVAWeaponUtil.TeamSide getSide()
    {
        return AVAWeaponUtil.TeamSide.EU;
    }
}
