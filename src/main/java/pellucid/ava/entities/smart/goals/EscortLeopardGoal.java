package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.ai.util.DefaultRandomPos;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.modes.GameModes;

import java.util.EnumSet;

public class EscortLeopardGoal extends MoveTowardsTargetGoal
{
    private final SidedSmartAIEntity smart;
    private LeopardEntity tank;
    private double wantedX;
    private double wantedY;
    private double wantedZ;
    private final double speedModifier;
    private final float within;

    public EscortLeopardGoal(SidedSmartAIEntity smart, double speed, float range)
    {
        super(smart, speed, range);
        this.smart = smart;
        this.speedModifier = speed;
        this.within = range;
        this.setFlags(EnumSet.of(Flag.MOVE));
    }

    @Override
    public boolean canUse()
    {
        if (!GameModes.ESCORT.isRunning() || GameModes.ESCORT.getTank(smart.level()).isEmpty())
            return false;
        LeopardEntity tank = GameModes.ESCORT.getTank(smart.level()).get();
        this.tank = tank;
        if (tank.distanceToSqr(smart) > (double) (this.within * this.within))
            return false;
        else
        {
            Vec3 vec3 = DefaultRandomPos.getPosTowards(smart, 6, 4, tank.position(), (float) Math.PI / 2F);
            if (vec3 == null)
                return false;
            else
            {
                this.wantedX = vec3.x;
                this.wantedY = vec3.y;
                this.wantedZ = vec3.z;
                return true;
            }
        }
    }

    @Override
    public boolean canContinueToUse()
    {
        return !this.smart.getNavigation().isDone() && tank.distanceToSqr(this.smart) < (double)(this.within * this.within);
    }

    @Override
    public void stop()
    {
        this.tank = null;
    }

    @Override
    public void start()
    {
        this.smart.getNavigation().moveTo(this.wantedX, this.wantedY, this.wantedZ, this.speedModifier);
    }
}
