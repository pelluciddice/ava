package pellucid.ava.entities.smart.goals;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.pathfinder.Path;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import java.util.EnumSet;
import java.util.function.Supplier;

public class AVAMoveToPosGoal extends AVASmartEntityGoal
{
    private final Supplier<BlockPos> targetPos;
    private final Runnable onReach;
    private boolean arrived;

    public AVAMoveToPosGoal(SidedSmartAIEntity smartEntity, Supplier<BlockPos> targetPos, Runnable onReach)
    {
        super(smartEntity);
        this.targetPos = targetPos;
        this.onReach = onReach;
        setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.JUMP));
    }

    @Override
    public boolean canUse()
    {
        return getTargetPos() != null && smartEntity.getTarget() == null && !arrived && smartEntity.getC4Entity() == null;
    }

    @Override
    public boolean canContinueToUse() 
    {
        return canUse() && !arrived;
    }

    @Override
    public void start() 
    {
        BlockPos pos = getTargetPos();
        smartEntity.getNavigation().moveTo(pos.getX() + 0.5D, pos.getY() + 1.0D, pos.getZ() + 0.5D, getSpeedFactor());
    }

    public BlockPos getTargetPos()
    {
        return targetPos.get();
    }

    @Override
    public void stop()
    {
//        arrived = false;
        smartEntity.getNavigation().moveTo((Path) null, getSpeedFactor());
    }

    @Override
    public boolean requiresUpdateEveryTick()
    {
        return true;
    }

    private final float range = smartEntity.getRandom().nextFloat() * 7.0F;
    @Override
    public void tick()
    {
        BlockPos pos = getTargetPos().above();
        smartEntity.getNavigation().moveTo(pos.getX() + 0.5D, pos.getY() + 1.0D, pos.getZ() + 0.5D, getSpeedFactor());
        if (pos.closerThan(smartEntity.blockPosition(), range))
        {
            arrived = true;
            onReach.run();
        }
    }
}
