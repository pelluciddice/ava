package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.ai.util.DefaultRandomPos;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import javax.annotation.Nullable;
import java.util.EnumSet;

public class MoveToLeaderGoal extends MoveTowardsTargetGoal
{
    private final SidedSmartAIEntity smart;
    @Nullable
    private LivingEntity target;
    private double wantedX;
    private double wantedY;
    private double wantedZ;
    private final double speedModifier;
    private final float within;

    public MoveToLeaderGoal(SidedSmartAIEntity smart, double speed, float range)
    {
        super(smart, speed, range);
        this.smart = smart;
        this.speedModifier = speed;
        this.within = range;
        this.setFlags(EnumSet.of(Goal.Flag.MOVE));
    }

    @Override
    public boolean canUse()
    {
        this.target = smart.getLeader();
        if (this.target == null)
            return false;
        else if (this.target.distanceToSqr(smart) > (double) (this.within * this.within))
            return false;
        else
        {
            Vec3 vec3 = DefaultRandomPos.getPosTowards(smart, 6, 4, this.target.position(), (float) Math.PI / 2F);
            if (vec3 == null)
                return false;
            else
            {
                this.wantedX = vec3.x;
                this.wantedY = vec3.y;
                this.wantedZ = vec3.z;
                return true;
            }
        }
    }

    @Override
    public boolean canContinueToUse()
    {
        return !this.smart.getNavigation().isDone() && this.target.isAlive() && this.target.distanceToSqr(this.smart) < (double)(this.within * this.within);
    }

    @Override
    public void stop()
    {
        this.target = null;
    }

    @Override
    public void start()
    {
        this.smart.getNavigation().moveTo(this.wantedX, this.wantedY, this.wantedZ, this.speedModifier);
    }
}
