package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.goal.target.TargetGoal;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import java.util.Random;

public abstract class AVASmartEntityTargetGoal extends TargetGoal
{
    protected static final Random RANDOM = new Random();
    protected final SidedSmartAIEntity smartEntity;
    protected Entity target;
    protected final Level world;

    public AVASmartEntityTargetGoal(SidedSmartAIEntity smartEntity)
    {
        super(smartEntity, false, false);
        this.smartEntity = smartEntity;
        this.world = smartEntity.level();
    }
}

