package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

public class AVARandomWalkingGoal extends WaterAvoidingRandomStrollGoal
{
    private final SidedSmartAIEntity smartEntity;
    public AVARandomWalkingGoal(SidedSmartAIEntity smartEntity)
    {
        super(smartEntity, 0.7F, 10);
        this.smartEntity = smartEntity;
    }

    @Override
    public boolean canUse()
    {
        return super.canUse() && smartEntity.getC4Entity() == null;
    }

    @Override
    public boolean canContinueToUse()
    {
        return super.canContinueToUse() && smartEntity.getC4Entity() == null;
    }
}
