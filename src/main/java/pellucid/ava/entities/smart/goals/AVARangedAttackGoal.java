package pellucid.ava.entities.smart.goals;

import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import java.util.EnumSet;
import java.util.function.Supplier;

public class AVARangedAttackGoal extends AVASmartEntityGoal
{
    private final Supplier<Float> moveToChance;

    public AVARangedAttackGoal(SidedSmartAIEntity attacker, Supplier<Float> moveToChance)
    {
        super(attacker);
        this.moveToChance = moveToChance;
        setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
    }

    private int lastStrafe = 0;
    @Override
    public void tick()
    {
        double distance = smartEntity.distanceTo(target);
        double range = smartEntity.getMaxAttackDistance();
        if (distance > range)
            smartEntity.getNavigation().moveTo(target, getSpeedFactor());
        else
        {
//            smartEntity.rotateTowardsEntity(target);
            smartEntity.performRangedAttack((LivingEntity) target, (float) Mth.clamp((distance / range), 0.1F, 1.0F));
            if (lastStrafe <= 0)
            {
                lastStrafe = 20;
                smartEntity.getMoveControl().strafe(smartEntity.getRandom().nextFloat() * 2.0F - 1.0F, smartEntity.getRandom().nextFloat() * 2.0F - 1.0F);
            }
            else
                lastStrafe--;
        }
    }

    @Override
    public boolean requiresUpdateEveryTick()
    {
        return true;
    }

    private int lastTryToMove = 0;
    @Override
    public boolean canUse()
    {
        LivingEntity entity = smartEntity.getTarget();
        if (entity != null && smartEntity.entityAttackPredicate.test(smartEntity, entity))
        {
            smartEntity.rotateTowardsEntity(entity);
            if (lastTryToMove > 0)
                lastTryToMove--;
            if (smartEntity.canSee(entity))
            {
                target = entity;
                return true;
            }
            else if (lastTryToMove <= 0 && smartEntity.getRandom().nextFloat() < moveToChance.get())
            {
                smartEntity.getNavigation().moveTo(entity, getSpeedFactor());
                lastTryToMove = 20;
            }
        }
        return false;
    }

    @Override
    public boolean canContinueToUse()
    {
        return canUse() && !smartEntity.getNavigation().isDone();
    }

    @Override
    public void stop()
    {
        target = null;
    }
}
