package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.Goal;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.EnumSet;

public class AVADisarmC4Goal extends AVASmartEntityGoal
{
    private C4Entity c4;
    private int disarmTicks = 0;

    public AVADisarmC4Goal(SidedSmartAIEntity smartEntity)
    {
        super(smartEntity);
        setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.JUMP));
    }

    @Override
    public boolean canUse()
    {
        if (disarmTicks == -1)
            return false;
        c4 = smartEntity.getC4Entity();
        if (!AVAWeaponUtil.isValidEntity(c4) || c4.defused() || c4.distanceTo(smartEntity) > 1.5F)
            return false;
//        SidedSmartAIEntity disarming = c4.getDisarmingEntity();
//        if (disarming != null && disarming != smartEntity)
//            return false;
//        return smartEntity.canSee(c4);
        return true;
    }

    @Override
    public void start()
    {
        c4.disarmingEntity = smartEntity.getUUID();
        disarmTicks = 0;
    }

    @Override
    public void tick()
    {
        smartEntity.lookAtEntity(c4);
        smartEntity.rotateTowardsEntity(c4);
        if (++disarmTicks >= c4.getDuration())
            c4.interact(world, null, null, null, smartEntity);
    }

    @Override
    public boolean requiresUpdateEveryTick()
    {
        return true;
    }

    @Override
    public void stop()
    {
        if (c4 != null)
        {
            c4.disarmingEntity = null;
            c4 = null;
        }
        smartEntity.setC4Entity(null);
        disarmTicks = 0;
    }
}
