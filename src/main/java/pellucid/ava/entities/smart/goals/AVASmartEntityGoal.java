package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.Level;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import java.util.Random;

public abstract class AVASmartEntityGoal extends Goal
{
    protected static final Random RANDOM = new Random();
    protected final SidedSmartAIEntity smartEntity;
    protected Entity target;
    protected final Level world;

    public AVASmartEntityGoal(SidedSmartAIEntity smartEntity)
    {
        this.smartEntity = smartEntity;
        this.world = smartEntity.level();
    }

    public float getSpeedFactor()
    {
        return smartEntity.getSpeedScale(world.getDifficulty());
    }
}

