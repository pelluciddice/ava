package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.pathfinder.Path;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.modes.GameModes;

import java.util.EnumSet;
import java.util.List;

public class AVAFindC4Goal extends AVASmartEntityGoal
{
    public AVAFindC4Goal(SidedSmartAIEntity smartEntity)
    {
        super(smartEntity);
        setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.JUMP));
    }

    @Override
    public void start()
    {
        if (GameModes.DEMOLISH.isRunning())
            target = smartEntity.getC4Entity();
        else if (smartEntity.getC4Entity() == null)
        {
            List<C4Entity> entities = world.getEntitiesOfClass(C4Entity.class, smartEntity.getBoundingBox().inflate(smartEntity.getMaxAttackDistance(), 10.0D, smartEntity.getMaxAttackDistance()));
            int distance = Integer.MAX_VALUE;
            for (C4Entity c4 : entities)
            {
                if (c4.defused())
                    continue;
                Path path2 = smartEntity.getNavigation().createPath(c4, 0);
                if (path2 == null)
                    continue;
                int pathDistance = path2.getNodeCount();
                if (pathDistance > distance)
                    continue;
                distance = pathDistance;
                target = c4;
            }
        }
    }

    @Override
    public void tick()
    {
        if (target != null)
            smartEntity.getNavigation().moveTo(target, getSpeedFactor());
    }

    @Override
    public boolean requiresUpdateEveryTick()
    {
        return true;
    }

    @Override
    public boolean canUse()
    {
        if (GameModes.DEMOLISH.isRunning() && smartEntity.getC4Entity() != null)
        {
            target = smartEntity.getC4Entity();
            return target.distanceTo(smartEntity) > 1.25F;
        }
        return smartEntity.tickCount % 400 == 0;
    }

    @Override
    public boolean canContinueToUse()
    {
        if (target == null)
            return false;
        if (target.distanceTo(smartEntity) <= 1.25F)
            return false;
        return canUse() && smartEntity.getNavigation().moveTo(target, getSpeedFactor());
    }

    @Override
    public void stop()
    {
        smartEntity.getNavigation().moveTo((Path) null, getSpeedFactor());
//        if (target != null && !((C4Entity) target).defused())
//        {
//            smartEntity.setC4Entity(target.getUUID());
//            smartEntity.lookAtEntity(target);
//        }
        target = null;
    }
}
