package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

public class AVALookRandomlyGoal extends RandomLookAroundGoal
{
    private final SidedSmartAIEntity smartEntity;
    public AVALookRandomlyGoal(SidedSmartAIEntity smartEntity)
    {
        super(smartEntity);
        this.smartEntity = smartEntity;
    }

    @Override
    public boolean canUse()
    {
        return super.canUse() && smartEntity.getC4Entity() == null;
    }

    @Override
    public boolean canContinueToUse()
    {
        return super.canContinueToUse() && smartEntity.getC4Entity() == null;
    }
}
