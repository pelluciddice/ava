package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.entity.player.Player;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.EnumSet;
import java.util.function.Supplier;

public class AVANearestOpponentTargetGoal extends AVASmartEntityTargetGoal
{
    protected LivingEntity nearestTarget;
    protected final Supplier<TargetingConditions> pickPredicate;
    protected final Supplier<TargetingConditions> continueousPredicate;

    public AVANearestOpponentTargetGoal(SidedSmartAIEntity goalOwnerIn, Supplier<TargetingConditions> pickPredicate, Supplier<TargetingConditions> continueousPredicate)
    {
        super(goalOwnerIn);
        setFlags(EnumSet.of(Goal.Flag.TARGET));
        this.pickPredicate = pickPredicate;
        this.continueousPredicate = continueousPredicate;
    }

    protected int lastSeenTicks = 0;
    @Override
    public boolean canContinueToUse()
    {
        if (!AVAWeaponUtil.isValidEntity(target))
            return false;
        LivingEntity entity = mob.getTarget();
        if (entity == null)
            entity = (LivingEntity) target;

        if (!continueousPredicate.get().test(smartEntity, entity))
            return false;
        if (!smartEntity.canAttackEntity(entity))
            return false;
        smartEntity.lastTargetPos = entity.blockPosition();
        if (smartEntity.canSee(entity))
            lastSeenTicks = 0;
        else if (++lastSeenTicks > (entity instanceof Player && AVAServerConfig.isCompetitiveModeActivated() ? 200 : 600))
            return false;
        mob.setTarget(entity);
        return true;
    }

    @Override
    public boolean canUse()
    {
        if (!AVAServerConfig.isCompetitiveModeActivated() && RANDOM.nextFloat() <= 0.2F)
            return false;
        double r = smartEntity.getMaxAttackDistance() * 1.5F;
        nearestTarget = world.getNearestEntity(LivingEntity.class, pickPredicate.get(), mob, mob.getX(), mob.getY(), mob.getZ(), mob.getBoundingBox().inflate(r, 10.0D, r));
        return nearestTarget != null;
    }

    @Override
    public void start()
    {
        super.start();
        mob.setTarget(nearestTarget);
    }
}
