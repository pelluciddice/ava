package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.smart.SidedSmartAIEntity;

import javax.annotation.Nullable;
import java.util.EnumSet;

public class MoveToPingGoal extends MoveTowardsTargetGoal
{
    private final SidedSmartAIEntity smart;
    @Nullable
    private Vec3 target;
    private final double speedModifier;
    private final float within;
    private int idle;

    public MoveToPingGoal(SidedSmartAIEntity smart, double speed, float range)
    {
        super(smart, speed, range);
        this.smart = smart;
        this.speedModifier = speed;
        this.within = range;
        this.setFlags(EnumSet.of(Flag.MOVE));
    }

    @Override
    public boolean canUse()
    {
        this.target = smart.getPingLoc();
        if (this.target == null)
            return false;
        else
            return !(this.target.distanceToSqr(smart.position()) > (double) (this.within * this.within));
    }

    @Override
    public boolean canContinueToUse()
    {
        if (target == null)
            return false;
        if (!this.smart.getNavigation().isDone() && this.target.distanceToSqr(smart.position()) < (double)(this.within * this.within))
        {
            idle = 0;
            return true;
        }
        return idle++ < 100;
    }

    @Override
    public void stop()
    {
        this.target = null;
        smart.moveToPing(null);
        idle = 0;
    }

    @Override
    public void start()
    {
        this.smart.getNavigation().moveTo(target.x, target.y, target.z, this.speedModifier);
    }
}
