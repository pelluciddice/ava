package pellucid.ava.entities.smart.goals;

import net.minecraft.world.entity.ai.goal.Goal;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.modes.GameModes;

import java.util.EnumSet;

public class RepairLeopardGoal extends Goal
{
    private final SidedSmartAIEntity smart;
    private LeopardEntity tank;

    public RepairLeopardGoal(SidedSmartAIEntity smart)
    {
        this.smart = smart;
        this.setFlags(EnumSet.of(Flag.LOOK));
    }

    @Override
    public boolean requiresUpdateEveryTick()
    {
        return true;
    }

    @Override
    public void tick()
    {
        super.tick();
        smart.lookAtEntity(tank);
        tank.repair(smart.getName().getString());
    }

    @Override
    public boolean canUse()
    {
        if (!GameModes.ESCORT.isRunning() || GameModes.ESCORT.getTank(smart.level()).isEmpty())
            return false;
        LeopardEntity tank = GameModes.ESCORT.getTank(smart.level()).get();
        this.tank = tank;
        return tank.getBoundingBox().inflate(tank.maxDistance()).intersects(smart.getBoundingBox()) && tank.requireRepair();
    }

    @Override
    public boolean canContinueToUse()
    {
        return canUse();
    }

    @Override
    public void stop()
    {
        this.tank = null;
    }
}
