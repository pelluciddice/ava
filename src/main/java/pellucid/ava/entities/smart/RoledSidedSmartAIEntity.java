package pellucid.ava.entities.smart;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.tags.GameEventTags;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.VanillaGameEvent;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.smart.goals.AVADisarmC4Goal;
import pellucid.ava.entities.smart.goals.AVAFindC4Goal;
import pellucid.ava.entities.smart.goals.AVALookRandomlyGoal;
import pellucid.ava.entities.smart.goals.AVAMoveToPosGoal;
import pellucid.ava.entities.smart.goals.AVANearestOpponentTargetGoal;
import pellucid.ava.entities.smart.goals.AVARandomWalkingGoal;
import pellucid.ava.entities.smart.goals.AVARangedAttackGoal;
import pellucid.ava.entities.smart.goals.MoveToLeaderGoal;
import pellucid.ava.entities.smart.goals.MoveToPingGoal;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.INBTSerializable;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;

@EventBusSubscriber
public class RoledSidedSmartAIEntity extends SidedSmartAIEntity
{
    public final RoledProperties role = new RoledProperties();
    public RoledSidedSmartAIEntity(Level worldIn)
    {
        this(AVAEntities.ROLED_SOLDIER.get(), worldIn);
    }

    public RoledSidedSmartAIEntity(EntityType<? extends PathfinderMob> type, Level worldIn)
    {
        super(AVAEntities.ROLED_SOLDIER.get(), worldIn);
    }

    public static final int MAX_HEARING_RANGE = 30;
    @SubscribeEvent
    public static void onVanillaGameEvent(VanillaGameEvent event)
    {
        if (event.getVanillaEvent().is(GameEventTags.VIBRATIONS))
        {
            Entity cause = event.getCause();
            if (cause instanceof Player player && (!player.isSteppingCarefully() || !event.getVanillaEvent().is(GameEventTags.IGNORE_VIBRATIONS_SNEAKING)))
            {
                Level world = cause.level();
                world.getEntitiesOfClass(RoledSidedSmartAIEntity.class, cause.getBoundingBox().inflate(MAX_HEARING_RANGE), (e) -> e.distanceTo(cause) <= e.role.hearingRange).forEach((e) -> {
                    if (e.getTarget() == null && e.entityPredicate.test(cause) && e.getSide().isOppositeSide(player))
                        e.setTarget(player);
                });
            }
        }
    }

    @Override
    public void registerGoals()
    {
        if (role != null && role.set)
        {
            targetSelector.addGoal(0, new AVANearestOpponentTargetGoal(this, () -> entityAttackOpponentTargetPredicate, () -> entityOpponentTargetPredicate));
            targetSelector.addGoal(1, new AVANearestOpponentTargetGoal(this, () -> entityAttackTargetPredicate, () -> entityTargetPredicate));

            goalSelector.addGoal(0, new AVARangedAttackGoal(this, () -> 0F));
            goalSelector.addGoal(2, new MoveToPingGoal(this, 0.8F, (float) (getMaxAttackDistance() * 2.0F)));
            goalSelector.addGoal(3, new MoveToLeaderGoal(this, 0.8F, (float) (getMaxAttackDistance() * 3.0F)));

            if (!role.stationary)
            {
                if (getSide() == AVAWeaponUtil.TeamSide.NRF)
                {
                    goalSelector.addGoal(4, new AVAFindC4Goal(this));
                    goalSelector.addGoal(5, new AVADisarmC4Goal(this));

                    sitePos = Lazy.of(() -> {
                        if (GameModes.DEMOLISH.isRunning())
                            return random.nextBoolean() ? null : Pair.or(AVAWorldData.getInstance(level()).sitesMark, random.nextBoolean());
                        return null;
                    });
                    goalSelector.addGoal(8, new AVAMoveToPosGoal(this, sitePos, () -> sitePos = null));
                }

                goalSelector.addGoal(6, new MoveTowardsTargetGoal(this, 0.7F, (float) (getMaxAttackDistance() * 3.0F)));
                goalSelector.addGoal(7, new AVAMoveToPosGoal(this, () -> lastTargetPos, () -> lastTargetPos = null));
                goalSelector.addGoal(9, new AVARandomWalkingGoal(this));
            }
            if (role.lookAround)
            {
                goalSelector.addGoal(9, new AVALookRandomlyGoal(this));
                goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 20.0F, 0.25F));
            }
        }
    }

    @Override
    public boolean shouldFindFarTarget()
    {
        return role.sensingRange != 0 && super.shouldFindFarTarget();
    }

    @Override
    public double getTrackingRange()
    {
        return role.sensingRange;
    }

    @Override
    public Class<?> getThis()
    {
        return RoledSidedSmartAIEntity.class;
    }

    @Override
    public AVAWeaponUtil.TeamSide getSide()
    {
        return level().isClientSide() ? AVAWeaponUtil.TeamSide.getSideFor(this) : role.side;
    }

    @Override
    protected double findFarTargetRadius()
    {
        return role.sensingRange;
    }

    @Override
    protected boolean inSight(Entity entity)
    {
        return entity.distanceTo(this) <= getMaxAttackDistance() && super.inSight(entity);
    }

    @Override
    public double getMaxAttackDistance()
    {
        return role.sightRange;
    }

    @Override
    protected double getWarningRadius()
    {
        return role.notifyNearby;
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        compound.put("properties", role.serializeNBT());
    }

    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        role.deserializeNBT(compound.getCompound("properties"));
        removeAllGoals((g) -> true);
        registerGoals();
    }

    @Override
    public boolean dynamicSide()
    {
        return level().isClientSide();
    }

    public static class RoledProperties implements INBTSerializable<CompoundTag>
    {
        private boolean set;

        private AVAWeaponUtil.TeamSide side;
        private int sightRange;
        private int sensingRange;
        private int hearingRange;
        private boolean stationary;
        private boolean lookAround;
        @Deprecated
        private boolean isLeader;
        @Deprecated
        private boolean followLeader;
        @Deprecated
        private boolean followPlayer;
        private int notifyNearby;

        public RoledProperties()
        {
            this.set = false;
        }

        public RoledProperties(CompoundTag compound)
        {
            deserializeNBT(compound);
        }

        public void updateAll(AVAWeaponUtil.TeamSide side, int sightRange, int sensingRange, int hearingRange, boolean stationary, boolean lookAround, boolean isLeader, boolean followLeader, boolean followPlayer, int notifyNearby)
        {
            this.set = true;

            this.side = side;
            this.sightRange = sightRange;
            this.sensingRange = sensingRange;
            this.hearingRange = hearingRange;
            this.stationary = stationary;
            this.lookAround = lookAround;
            this.isLeader = isLeader;
            this.followLeader = followLeader;
            this.followPlayer = followPlayer;
            this.notifyNearby = notifyNearby;
        }

        @Override
        public CompoundTag serializeNBT()
        {
            CompoundTag compound = new CompoundTag();
            compound.putString("side", side.name());
            compound.putInt("sightRange", sightRange);
            compound.putInt("sensingRange", sensingRange);
            compound.putInt("hasEars", hearingRange);
            compound.putBoolean("stationary", stationary);
            compound.putBoolean("lookAround", lookAround);
            compound.putBoolean("isLeader", isLeader);
            compound.putBoolean("followLeader", followLeader);
            compound.putBoolean("followPlayer", followPlayer);
            compound.putInt("notifyNearby", notifyNearby);
            return compound;
        }

        @Override
        public void deserializeNBT(CompoundTag nbt)
        {
            this.set = true;

            side = AVAWeaponUtil.TeamSide.fromName(nbt.getString("side")).get();
            sightRange = nbt.getInt("sightRange");
            sensingRange = nbt.getInt("sensingRange");
            hearingRange = nbt.getInt("hearingRange");
            stationary = nbt.getBoolean("stationary");
            lookAround = nbt.getBoolean("lookAround");
            isLeader = nbt.getBoolean("isLeader");
            followLeader = nbt.getBoolean("followLeader");
            followPlayer = nbt.getBoolean("followPlayer");
            notifyNearby = nbt.getInt("notifyNearby");
        }

        @Override
        public String toString()
        {
            return "RoledProperties{" +
                    "set=" + set +
                    ", side=" + side +
                    ", sightRange=" + sightRange +
                    ", sensingRange=" + sensingRange +
                    ", hearingRange=" + hearingRange +
                    ", stationary=" + stationary +
                    ", lookAround=" + lookAround +
                    ", isLeader=" + isLeader +
                    ", followLeader=" + followLeader +
                    ", followPlayer=" + followPlayer +
                    ", notifyNearby=" + notifyNearby +
                    '}';
        }
    }
}
