package pellucid.ava.entities;

import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.items.init.Projectiles;

import javax.annotation.Nullable;

public class AVADamageSources
{
    public static final ResourceKey<DamageType> DIRECT = ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "direct"));
    public static final ResourceKey<DamageType> BULLET = ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "bullet"));
    public static final ResourceKey<DamageType> EXPLOSION = ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "explosion"));
    public static final ResourceKey<DamageType> TOXIC_GAS = ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "toxic_gas"));
    public static final ResourceKey<DamageType> SYSTEM_REMOVE = ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "system_remove"));

    public static AVADamageSource causeDamageDirect(Level world, Entity attacker, Item weapon)
    {
        return new AVADamageSource(newSource(world, DIRECT), attacker, weapon);
    }

    public static AVAIndirectDamageSource causeBulletDamageIndirect(Level world, Entity attacker, Entity bullet, Item weapon)
    {
        return new AVAIndirectDamageSource(newSource(world, BULLET), bullet, attacker, weapon);
    }

    public static AVAIndirectDamageSource causeProjectileExplodeDamage(Level world, Entity attacker, Entity projectile, Item weapon)
    {
        return new AVAIndirectDamageSource(newSource(world, EXPLOSION), projectile, attacker, weapon);
    }

    public static AVADamageSource causeExplodeDamage(Level world, Entity attacker)
    {
        return new AVADamageSource(newSource(world, EXPLOSION), attacker, AVABlocks.EXPLOSIVE_BARREL.get().asItem());
    }

    public static AVAIndirectDamageSource causeToxicGasDamage(Level world, Entity attacker, Entity projectile)
    {
        return new AVAIndirectDamageSource(newSource(world, TOXIC_GAS), projectile, attacker, Projectiles.M18_TOXIC.get());
    }

    public static DamageSource causeSystemRemoveDamage(Level world, Entity attacker)
    {
        return new DamageSource(newSource(world, SYSTEM_REMOVE), attacker, null);
    }

    private static Holder.Reference<DamageType> newSource(Level world, ResourceKey<DamageType> key)
    {
        return world.registryAccess().registryOrThrow(Registries.DAMAGE_TYPE).getHolderOrThrow(key);
    }

    public static boolean isAVADamageSource(DamageSource source)
    {
        return source instanceof AVADamageSource || source instanceof AVAIndirectDamageSource;
    }

    public static class AVADamageSource extends DamageSource implements IAVAWeaponDamageSource
    {
        private final Item weapon;
        public AVADamageSource(Holder<DamageType> type, Entity damageSourceEntityIn, @Nullable Item weapon)
        {
            super(type, damageSourceEntityIn);
            this.weapon = weapon;
        }

        @Override
        public Component getLocalizedDeathMessage(LivingEntity entity)
        {
            String s = "death.attack.";
            if (getEntity() == null)
            {
                s += "ava.killed";
                if (this.weapon != null)
                    return Component.translatable(s + ".weapon", entity.getDisplayName(), this.weapon.getDescriptionId());
                return Component.translatable(s, entity.getDisplayName());
            }
            s += this.getMsgId();
            return this.weapon != null ? Component.translatable(s + ".weapon", entity.getDisplayName(), getEntity().getDisplayName(), Component.translatable(this.weapon.getDescriptionId())) : Component.translatable(s, entity.getDisplayName(), getEntity().getDisplayName());
        }

        @Override
        public Item getWeapon()
        {
            return this.weapon;
        }
    }

    public static class AVAIndirectDamageSource extends DamageSource implements IAVAWeaponDamageSource
    {
        private final Item weapon;
        public AVAIndirectDamageSource(Holder<DamageType> type, Entity source, @Nullable Entity indirectEntityIn, @Nullable Item weapon)
        {
            super(type, source, indirectEntityIn);
            this.weapon = weapon;
        }

        @Override
        public Component getLocalizedDeathMessage(LivingEntity entity)
        {
            String s = "death.attack.";
            if (getEntity() == null)
            {
                s += "ava.killed";
                if (this.weapon != null)
                    return Component.translatable(s + ".weapon", entity.getDisplayName(), this.weapon.getDescriptionId());
                return Component.translatable(s, entity.getDisplayName());
            }
            s += this.getMsgId();
            return this.weapon != null ? Component.translatable(s + ".weapon", entity.getDisplayName(), getEntity().getDisplayName(), Component.translatable(this.weapon.getDescriptionId())) : Component.translatable(s, entity.getDisplayName(), getEntity().getDisplayName());
        }

        @Override
        public Item getWeapon()
        {
            return this.weapon;
        }
    }

    public interface IAVAWeaponDamageSource
    {
        Item getWeapon();
    }
}
