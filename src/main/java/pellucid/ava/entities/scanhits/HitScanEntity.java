package pellucid.ava.entities.scanhits;

import net.minecraft.Util;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.entity.IEntityWithComplexSpawn;
import pellucid.ava.entities.base.AVAEntity;
import pellucid.ava.entities.functionalities.IOwner;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.util.UUID;

public class HitScanEntity extends AVAEntity implements IOwner, IEntityWithComplexSpawn
{
    protected LivingEntity shooter;
    protected UUID shooterUUID;
    protected int shooterID;
    protected float range;
    protected boolean travelled = false;

    public HitScanEntity(EntityType<?> type, Level worldIn)
    {
        super(type, worldIn);
    }

    public HitScanEntity(EntityType<? extends Entity> type, Level worldIn, LivingEntity shooter, float range)
    {
        this(type, worldIn);
        this.setPos(shooter.getX(), shooter.getY() + shooter.getEyeHeight(), shooter.getZ());
        float f = -Mth.sin(shooter.getYRot() * ((float)Math.PI / 180F)) * Mth.cos(shooter.getXRot() * ((float)Math.PI / 180F));
        float f1 = -Mth.sin((shooter.getXRot()) * ((float)Math.PI / 180F));
        float f2 = Mth.cos(shooter.getYRot() * ((float)Math.PI / 180F)) * Mth.cos(shooter.getXRot() * ((float)Math.PI / 180F));
        this.setDeltaMovement(new Vec3(f, f1, f2).normalize());
        this.shooter = shooter;
        this.shooterUUID = shooter.getUUID();
        this.shooterID = shooter.getId();
        this.range = range;
    }

    @Override
    public void writePlainSpawnData(RegistryFriendlyByteBuf buffer)
    {
        super.writePlainSpawnData(buffer);
        DataTypes.UUID.write(buffer, shooterUUID == null ? Util.NIL_UUID : shooterUUID);
        DataTypes.INT.write(buffer, shooterID);
        DataTypes.FLOAT.write(buffer, range);
    }

    @Override
    public void readPlainSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        super.readPlainSpawnData(additionalData);
        shooterUUID = DataTypes.UUID.read(additionalData);
        if (shooterUUID.equals(Util.NIL_UUID))
            shooterUUID = null;
        shooterID = DataTypes.INT.read(additionalData);
        range = DataTypes.FLOAT.read(additionalData);
    }

    @Override
    public void fromMob(LivingEntity target)
    {
        super.fromMob(target);
        Vec3 vector3d = AVAWeaponUtil.canBeSeen(this, false, target, true, false, true, false, true);
        if (vector3d != null)
            setDeltaMovement(vector3d.subtract(position()).normalize());
        else
            setDeltaMovement(target.position().add(0, target.getBbHeight() / 2.0F, 0).subtract(position()).normalize());
    }

    protected void setDirection(Vec3 vec3d)
    {
        float f = Mth.sqrt((float) vec3d.horizontalDistanceSqr());
        this.setYRot((float)(Mth.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI)));
        this.setXRot((float)(Mth.atan2(vec3d.y, f) * (double)(180F / (float)Math.PI)));
        this.yRotO = this.getYRot();
        this.xRotO = this.getXRot();
    }

    @Override
    public void tick()
    {
        super.tick();
        setDirection(this.getDeltaMovement());
        if (getShooter() == null || travelled)
        {
            remove(RemovalReason.DISCARDED);
            return;
        }
        Vec3 from = this.position();
        Vec3 to = this.position().add(getDeltaMovement().scale(range));
        EntityHitResult entityResult = AVAWeaponUtil.rayTraceEntity(level(), this, from, to, (entity) -> entity != getShooter() && !entity.isSpectator() && entity.isPickable() && entity.isAlive() && canAttack(entity), true);
        BlockHitResult blockResult = level().clip(new ClipContext(from, to, ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, this));
        if (blockResult.getType() != HitResult.Type.MISS && entityResult != null)
            onImpact((from.distanceTo(blockResult.getLocation()) > from.distanceTo(entityResult.getLocation())) ? entityResult : blockResult);
        else if (entityResult != null)
            onImpact(entityResult);
        else if (blockResult.getType() != HitResult.Type.MISS)
            onImpact(blockResult);
        else
            onImpact(null);
        travelled = true;
    }

    @Override
    public boolean isNoGravity()
    {
        return true;
    }

    protected void onImpact(@Nullable HitResult result)
    {

    }

    @Nullable
    @Override
    public LivingEntity getShooter()
    {
        if (!level().isClientSide())
        {
            if ((this.shooter == null || !this.shooter.isAlive()) && this.shooterUUID != null)
            {
                Entity entity = ((ServerLevel) this.level()).getEntity(this.shooterUUID);
                if (entity instanceof LivingEntity)
                    this.shooter = (LivingEntity) entity;
                else
                    this.shooter = null;
            }
        }
        else
        {
            Entity entity = this.level().getEntity(this.shooterID);
            if (entity instanceof LivingEntity)
                this.shooter = (LivingEntity) entity;
            else
                this.shooter = null;
        }
        return this.shooter;
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        if (this.shooterUUID != null)
            DataTypes.UUID.write(compound, "owner", this.shooterUUID);
        DataTypes.FLOAT.write(compound, "range", range);
        DataTypes.BOOLEAN.write(compound, "travalled", travelled);
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        if (compound.contains("owner"))
            this.shooterUUID = DataTypes.UUID.read(compound, "owner");
        range = DataTypes.FLOAT.read(compound, "range");
        this.travelled = DataTypes.BOOLEAN.read(compound, "travelled");
    }

    @Override
    public void writeSpawnData(RegistryFriendlyByteBuf buffer)
    {
        writePlainSpawnData(buffer);
    }

    @Override
    public void readSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        readPlainSpawnData(additionalData);
    }
}
