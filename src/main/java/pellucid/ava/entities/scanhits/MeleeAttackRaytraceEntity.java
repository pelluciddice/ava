package pellucid.ava.entities.scanhits;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.client.renderers.environment.EnvironmentObjectEffect;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;

public class MeleeAttackRaytraceEntity extends HitScanEntity
{
    private float damage;
    private Item weapon;
    public MeleeAttackRaytraceEntity(EntityType<Entity> type, Level worldIn)
    {
        super(AVAEntities.MELEE_RAYTRACING.get(), worldIn);
    }

    public MeleeAttackRaytraceEntity(Level worldIn, LivingEntity shooter, float range, float damage, Item weapon)
    {
        super(AVAEntities.MELEE_RAYTRACING.get(), worldIn, shooter, range);
        this.damage = damage;
        this.weapon = weapon;
    }

    @Override
    protected void onImpact(@Nullable HitResult result)
    {
        if (result != null)
        {
            Vec3 vec = result.getLocation();
            if (result.getType() == HitResult.Type.ENTITY)
            {
                if (!level().isClientSide())
                {
                    Entity target = ((EntityHitResult) result).getEntity();
                    if (target instanceof LivingEntity)
                    {
                        float multiplier = 1;
                        if (Math.abs(target.getEyeY() - vec.y()) <= 0.3125F)
                            multiplier *= 2.0F;
                        if (!(target instanceof Player))
                            multiplier *= 1.35F;
                        AVAWeaponUtil.attackEntityDependAllyDamage(target, AVADamageSources.causeDamageDirect(level(), getShooter(), weapon), damage * multiplier, 1.0F);
                    }
                }
            }
            else if (result.getType() == HitResult.Type.BLOCK)
            {
                BlockPos pos = ((BlockHitResult) result).getBlockPos();
                if (!AVAWeaponUtil.destructRepairable(level(), pos, getShooter() instanceof Player ? (Player) getShooter() : null) && !AVAWeaponUtil.destroyGlassOnHit(level(), pos) && !AVAWeaponUtil.destroyBlockOnMeleeHit(level(), pos))
                {
                    if (!level().isClientSide())
                    {
                        AVAConstants.getMeleeOnHitBlockSound(level(), level().getBlockState(pos), pos).ifPresent((s) -> {
                            playSound(s, SoundSource.BLOCKS, vec.x(), vec.y(), vec.z(), 1.25F, 1.25F);
                        });
                    }
                    else
                        AVAWorldData.getInstance(level()).knifeHoles.add(new EnvironmentObjectEffect((BlockHitResult) result, true));
                }
            }
        }
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.FLOAT.write(compound, "damage", damage);
        compound.putString("weapon", BuiltInRegistries.ITEM.getKey(weapon).toString());
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        damage = DataTypes.FLOAT.read(compound, "damage");
        weapon = BuiltInRegistries.ITEM.get(new ResourceLocation(compound.getString("weapon")));
    }
}
