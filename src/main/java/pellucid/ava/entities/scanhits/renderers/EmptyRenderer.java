package pellucid.ava.entities.scanhits.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import pellucid.ava.AVA;

@OnlyIn(Dist.CLIENT)
public class EmptyRenderer<T extends Entity> extends EntityRenderer<T>
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/bullet.png");
//    private final BulletModel bulletModel = new BulletModel();

    public EmptyRenderer(EntityRendererProvider.Context context)
    {
        super(context);
    }

    @Override
    public void render(T entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
    {
//        float f = MathHelper.rotLerp(entityIn.prevRotationYaw, entityIn.rotationYaw, partialTicks);
//        float f1 = MathHelper.lerp(partialTicks, entityIn.prevRotationPitch, entityIn.rotationPitch);
//        this.bulletModel.setupAnim(0.0F, f, f1);
//        bulletModel.render(matrixStackIn, bufferIn.getBuffer(this.bulletModel.getRenderType(this.getEntityTexture(entityIn))), packedLightIn, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
    }

    @Override
    public ResourceLocation getTextureLocation(T entity)
    {
        return TEXTURE;
    }
}
