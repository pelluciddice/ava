package pellucid.ava.entities.scanhits;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.boss.EnderDragonPart;
import net.minecraft.world.entity.boss.enderdragon.EndCrystal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.environment.EnvironmentObjectEffect;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.functionalities.ICustomOnHitEffect;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.gun.gun_mastery.tasks.DamageMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HeadshotMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HitMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillTypeMasteryTask;
import pellucid.ava.items.armours.AVAArmours;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BulletEntity extends HitScanEntity
{
    public float damage;
    private Item weapon = Items.AIR;
    protected float spread;
    protected float penetration;

    public BulletEntity(EntityType<? extends Entity> type, Level worldIn)
    {
        super(AVAEntities.BULLET.get(), worldIn);
    }

    public BulletEntity(Level worldIn, LivingEntity shooter, AVAItemGun weapon, float range, float damage, float spread, float penetration)
    {
        super(AVAEntities.BULLET.get(), worldIn, shooter, range * 2.0F);
        this.spread = spread;
        this.damage = damage;
        this.weapon = weapon;
        this.penetration = penetration / 100.0F;
        spread();
    }

    public Item getWeapon()
    {
        return weapon;
    }

    public float getBulletDamage()
    {
        return damage;
    }

    @Override
    public void writePlainSpawnData(RegistryFriendlyByteBuf buffer)
    {
        super.writePlainSpawnData(buffer);
        DataTypes.FLOAT.write(buffer, damage);
        DataTypes.ITEM.write(buffer, weapon);
        DataTypes.FLOAT.write(buffer, spread);
        DataTypes.FLOAT.write(buffer, penetration);
    }

    @Override
    public void readPlainSpawnData(RegistryFriendlyByteBuf additionalData)
    {
        super.readPlainSpawnData(additionalData);
        damage = DataTypes.FLOAT.read(additionalData);
        weapon = DataTypes.ITEM.read(additionalData);
        spread = DataTypes.FLOAT.read(additionalData);
        penetration = DataTypes.FLOAT.read(additionalData);
    }

    @Override
    public void fromMob(LivingEntity target)
    {
        super.fromMob(target);
        spread();
    }

    protected void spread()
    {
        double x = (-spread + this.random.nextFloat() * spread * 2.0F);
        double y = (-spread + this.random.nextFloat() * spread * 2.0F);
        double z = (-spread + this.random.nextFloat() * spread * 2.0F);
        this.setDeltaMovement(this.getDeltaMovement().add(new Vec3(x, y, z).scale(0.05F)).normalize());
    }

    protected boolean hasTrail()
    {
        return AVAClientConfig.ENABLE_BULLET_TRAIL_EFFECT.get() && weapon instanceof AVAItemGun && ((AVAItemGun) weapon).hasTrail();
    }

    protected float blockPenCalculator(BlockHitResult result, BlockState state)
    {
        return AVAWeaponUtil.getBlockHardness(result, this, state) * Math.max(0.15F, 1.0F - penetration) * 5.0F;
    }

    protected float entityPenCalculator(EntityHitResult result, Entity entity)
    {
        return AVAWeaponUtil.getEntityHardness(result, this, entity) * Math.max(0.05F, 1.0F - penetration);
    }

    @Override
    public void tick()
    {
        setDirection(getDeltaMovement());
        if (getShooter() == null || travelled)
        {
            remove(RemovalReason.DISCARDED);
            return;
        }
        if (!fromMob || random.nextFloat() <= 0.15F)
        {
            AABB axisalignedbb = getBoundingBox().expandTowards(getDeltaMovement().scale(range));
            Vec3 from = position();
            Vec3 to = position().add(getDeltaMovement().scale(range));
            List<Vec3> vecs = AVAWeaponUtil.getAllVectors2Efficiency(from, to, level().getMinBuildHeight(), level().getMaxBuildHeight(), Math.round(range));
            if (!vecs.isEmpty())
            {
                Vec3 endVec = vecs.get(vecs.size() - 1);
                List<Pair<BlockHitResult, Float>> blockResults = AVAWeaponUtil.rayTraceAndPenetrateBlocks4Efficiency(level(), vecs, damage, this::blockPenCalculator);
                List<Pair<EntityHitResult, Float>> entityResults = AVAWeaponUtil.rayTraceAndPenetrateEntities(level(), this, vecs, axisalignedbb, (entity) -> entity != getShooter() && !entity.isSpectator() && entity.isPickable() && entity.isAlive() && (!(entity instanceof LivingEntity) || !((LivingEntity) entity).isDeadOrDying()) && canAttack(entity), damage, this::entityPenCalculator);
                if (!blockResults.isEmpty() || !entityResults.isEmpty())
                {
                    int blockIndex = 0;
                    int entityIndex = 0;
                    while (true)
                    {
                        Pair<BlockHitResult, Float> blockResult = null;
                        Pair<EntityHitResult, Float> entityResult = null;
                        double blockDistance = Double.MAX_VALUE;
                        double entityDistance = Double.MAX_VALUE;
                        if (blockResults.size() > blockIndex)
                        {
                            blockResult = blockResults.get(blockIndex);
                            blockDistance = blockResult.getB();
                        }
                        if (entityResults.size() > entityIndex)
                        {
                            entityResult = entityResults.get(entityIndex);
                            entityDistance = entityResult.getB();
                        }
                        if (blockResult == null && entityResult == null)
                            break;
                        if (blockDistance < entityDistance)
                        {
                            if (blockResult != null)
                            {
                                blockIndex++;
                                onImpact(blockResult.getA());
                            }
                        }
                        else
                        {
                            if (entityResult != null)
                            {
                                entityIndex++;
                                onImpact(entityResult.getA());
                            }
                        }
                        if ((blockResults.size() <= blockIndex && entityResults.size() <= entityIndex) || damage <= AVAConstants.MIN_BULLET_DAMAGE)
                        {
                            if (blockResult != null)
                                endVec = blockResult.getA().getLocation();
                            else if (entityResult != null)
                                endVec = entityResult.getA().getLocation();
                        }
                        if ((blockDistance == Double.MAX_VALUE && entityDistance == Double.MAX_VALUE) || damage <= AVAConstants.MIN_BULLET_DAMAGE)
                            break;
                    }
                }
                if (!level().isClientSide())
                {
                    LivingEntity shooter = getShooter();
                    if (shooter != null)
                    {
                        final double x = getX() - endVec.x;
                        final double y = getY() - endVec.y;
                        final double z = getZ() - endVec.z;
                        int count = (int) (Math.round((Math.max(Math.max(Math.abs(x), Math.abs(y)), Math.abs(z)))) / 6F);
                        for (int i = 1; i < count; i++)
                            if (random.nextFloat() <= 0.25F)
                                AVAWeaponUtil.playAttenuableSoundToClient(AVASounds.BULLET_FLY_BY.get(), level(), new Vec3(shooter.getX() + x / count * i, shooter.getY() + y / count * i, shooter.getZ() + z / count * i), (player) -> player == shooter);
                    }
                }
                else if (hasTrail() && getShooter() != null && shooter.isAlive() && vecs.size() > 20 && random.nextFloat() <= 0.75F)
                {
                    Vec3 offset = AVAWeaponUtil.getVectorForRotation(shooter.getXRot(), shooter.getYRot() + 90.0F).scale(0.0375F);
                    Vec3 vec = vecs.get(20).add(0.0F, -0.035F, 0.0F);
                    if (shooter instanceof Player && !PlayerAction.getCap((Player) shooter).isADS())
                        vec = vec.add(offset.x, 0.0, offset.z);
                    AVAWorldData.getInstance(level()).bulletTrails.put(Pair.of(vec, endVec), 2);
//                    WorldData.getCap(level).addBulletTrail(vec, endVec);
                }
            }
        }
        travelled = true;
        baseTick();
    }

    @Override
    public boolean canAttack(Entity entity)
    {
        if (fromMob && getShooter() != null && entity instanceof LivingEntity && !(entity instanceof Player))
        {
            AVAWeaponUtil.TeamSide side = AVAWeaponUtil.TeamSide.getSideFor(getShooter());
            if (side != null && side.isSameSide((LivingEntity) entity))
                return false;
        }
        return super.canAttack(entity);
    }

    private final Set<BlockPos> hitBlockPos = new HashSet<>();
    private final Set<Entity> hitEntities = new HashSet<>();
    private float blockBreakVolume = 1.25F;
    private boolean blooded = false;

    @Override
    protected void onImpact(@Nullable HitResult result)
    {
        LivingEntity shooter = getShooter();
        if (result == null || shooter == null)
            return;
        Level world = level();
        Vec3 vec = result.getLocation();
        if (result.getType() == HitResult.Type.BLOCK)
        {
            BlockHitResult blockResult = (BlockHitResult) result;
            BlockPos pos = blockResult.getBlockPos();
            BlockState state = world.getBlockState(pos);
            if (!AVAWeaponUtil.canBulletHitBlock(blockResult, this) ||
                    AVAWeaponUtil.isRayTraceIgnorableBlock(state, null) ||
                    AVAWeaponUtil.destructRepairable(world, pos, getShooter() instanceof Player ? (Player) getShooter() : null)
            )
                return;
            AVAWeaponUtil.checkHitTargetBlock(world, this, shooter, blockResult);
            ICustomOnHitEffect customEffect = state.getBlock() instanceof ICustomOnHitEffect ? (ICustomOnHitEffect) state.getBlock() : null;
            if (!AVAWeaponUtil.destroyGlassOnHit(world, pos) && world.isClientSide())
            {
                if (!blockResult.isInside() && AVAClientConfig.ENABLE_BULLET_HOLE_EFFECT.get())
                    AVAWorldData.getInstance(world).bulletHoles.add((EnvironmentObjectEffect) new EnvironmentObjectEffect(blockResult, true).setColour(customEffect != null ? customEffect.getBulletHoleColour(world, blockResult) : null));
                if (blooded)
                {
                    blooded = false;
                    if (AVAClientConfig.ENABLE_BLOOD_EFFECT.get())
                        AVAWorldData.getInstance(world).bloods.add(new EnvironmentObjectEffect(blockResult, true));
                }
            }
            if (world instanceof ServerLevel server)
            {
                boolean repeated = hitBlockPos.contains(pos);
                if (!repeated)
                    hitBlockPos.add(pos);
                boolean soundEffect = !repeated && !AVAWeaponUtil.checkForSpecialBlockHit(this, blockResult) && !AVAWeaponUtil.igniteTNTOnHit(world, state, pos, shooter) && !AVAWeaponUtil.attackExplosiveBarrelOnHit(world, pos, shooter, damage);
                if (!repeated)
                {
                    if (random.nextFloat() <= 0.25F)
                        shooter.gameEvent(GameEvent.PROJECTILE_LAND, shooter);
                    if (soundEffect)
                    {
                        AVAConstants.getBulletOnHitBlockSound(world, state, pos).ifPresent((s) -> {
                            world.playSound(null, vec.x(), vec.y(), vec.z(), s, SoundSource.BLOCKS, blockBreakVolume, 0.875F + random.nextFloat() / 4.0F);
                        });
                        blockBreakVolume *= 0.5F;
                    }
                    server.sendParticles(ParticleTypes.SMOKE, vec.x(), vec.y(), vec.z(), 4, 0.0F, 0.0F, 0.0F, 0.1F);
                    server.sendParticles(new BlockParticleOption(ParticleTypes.BLOCK, customEffect == null ? state : customEffect.blockParticle(world, blockResult)), vec.x(), vec.y(), vec.z(), 4, 0.0F, 0.0F, 0.0F, 0.2F);
                }
            }
            AVAWeaponUtil.onBulletHitBlock(blockResult, this);
            damage -= blockPenCalculator(blockResult, state);
            GunMasteryManager.of(shooter.getMainHandItem()).ifPresent((manager) -> {
                if (manager.hasMasteryUnlocked())
                    manager.mastery.onHitBlock(this, blockResult, manager.masteryLevel);
            });
        }
        else if (result.getType() == HitResult.Type.ENTITY)
        {
            Entity target = ((EntityHitResult) result).getEntity();
            if (!AVAWeaponUtil.canBulletHitEntity(((EntityHitResult) result), this) || !target.isAlive() || target == shooter)
                return;
            boolean isEnderCrystal = target instanceof EndCrystal && !fromMob;
            boolean isDragonParts = target instanceof EnderDragonPart && !fromMob;
            if (isEnderCrystal)
                target.hurt(AVADamageSources.causeBulletDamageIndirect(world, shooter, this, weapon), damage);
            else if (target instanceof LivingEntity || isDragonParts)
            {
                ICustomOnHitEffect customEffect = ICustomOnHitEffect.orDefault(target);
                float multiplier = Math.max(0.25F, 1.0F - (float) vec.distanceTo(position()) * 0.5F / range);
                target.invulnerableTime = 0;
                boolean repeated = hitEntities.contains(target);
                boolean targetLiving = target instanceof LivingEntity;
                if (!repeated)
                {
                    hitEntities.add(target);
                    if (!world.isClientSide())
                    {
                        Block block = customEffect.bloodParticle();
                        customEffect.onHit(world, vec);
                        if (block != Blocks.AIR)
                            ((ServerLevel) world).sendParticles(new BlockParticleOption(ParticleTypes.BLOCK, block.defaultBlockState()), vec.x, vec.y, vec.z, 4, 0F, 0F, 0F, 0.0F);
                        if (Math.abs(target.getEyeY() - vec.y()) <= 0.3125F)
                        {
                            multiplier *= AVAServerConfig.HEADSHOT_DAMAGE_SCALE.get();
                            if (targetLiving && customEffect.playsHeadShotSound())
                                world.playSound(null, target.getX(), target.getY(), target.getZ(), ((LivingEntity) target).getItemBySlot(EquipmentSlot.HEAD).getItem() instanceof AVAArmours ? AVASounds.HEADSHOT_HELMET.get() : AVASounds.HEADSHOT.get(), SoundSource.PLAYERS, 1.0F, 1.0F);
                            if (GunMasteryManager.isTaskRunning(shooter.getMainHandItem(), HeadshotMasteryTask.class))
                                GunMasteryManager.ofUnsafe(shooter.getMainHandItem()).progress(world, shooter, 1);
                        }
                    }
                }
                if (targetLiving)
                {
                    LivingEntity living = (LivingEntity) target;
                    if (living.isBlocking())
                        living.stopUsingItem();
                    if (!world.isClientSide() && AVAWeaponUtil.TeamSide.getSideFor(living) != null && random.nextFloat() <= 0.15F)
                        AVAWeaponUtil.playAttenuableSoundToClientMoving(AVAWeaponUtil.eitherSound(living, AVASounds.ON_HIT_EU.get(), AVASounds.ON_HIT_NRF.get()), living);
                }
                if (AVAWeaponUtil.attackEntityDependAllyDamage(target, AVADamageSources.causeBulletDamageIndirect(world, shooter, this, weapon), damage, multiplier, Math.max(0.15F, Math.min(1.0F, 1.0F - penetration))))
                {
                    if (!world.isClientSide() && GunMasteryManager.isTaskRunning(shooter.getMainHandItem(), DamageMasteryTask.class))
                        GunMasteryManager.ofUnsafe(shooter.getMainHandItem()).progress(world, shooter, (int) damage);
                    if (targetLiving && customEffect.bloodMark())
                        blooded = true;
                }
                damage *= (1.0F - AVAWeaponUtil.getEntityHardness((EntityHitResult) result, this, target)) * (1.0F - penetration);
                if (targetLiving && !world.isClientSide())
                {
                    LivingEntity living = (LivingEntity) target;
                    if (living.isDeadOrDying())
                    {
                        if (GunMasteryManager.isTaskRunning(shooter.getMainHandItem(), KillMasteryTask.class) || GunMasteryManager.isTaskRunning(shooter.getMainHandItem(), KillTypeMasteryTask.class, target.getType()))
                            GunMasteryManager.ofUnsafe(shooter.getMainHandItem()).progress(world, shooter, 1);
                        if (random.nextFloat() <= (living instanceof Player ? 0.75F : 0.075F) && AVAWeaponUtil.TeamSide.getSideFor(shooter) != null)
                            AVAWeaponUtil.playAttenuableSoundToClientMoving(AVAWeaponUtil.eitherSound(shooter, AVASounds.ENEMY_DOWN_EU.get(), AVASounds.ENEMY_DOWN_NRF.get()), shooter);
                        AVAWeaponUtil.TeamSide side = AVAWeaponUtil.TeamSide.getSideFor(living);
                        if (side != null && random.nextFloat() <= (living instanceof Player ? 0.75F : 0.25F))
                            world.getEntitiesOfClass(LivingEntity.class, living.getBoundingBox().inflate(7.0F), (entity) -> entity.isAlive() && AVAWeaponUtil.TeamSide.getSideFor(entity) == side).stream().findFirst()
                                .ifPresent((entity) -> AVAWeaponUtil.playAttenuableSoundToClientMoving(AVAWeaponUtil.eitherSound(entity, AVASounds.ALLY_DOWN_EU.get(), AVASounds.ALLY_DOWN_NRF.get()), entity));
                    }
                    if (GunMasteryManager.isTaskRunning(shooter.getMainHandItem(), HitMasteryTask.class))
                        GunMasteryManager.ofUnsafe(shooter.getMainHandItem()).progress(world, shooter, 1);
                }
            }
            AVAWeaponUtil.onBulletHitEntity((EntityHitResult) result, this);
            GunMasteryManager.of(shooter.getMainHandItem()).ifPresent((manager) ->
            {
                if (manager.hasMasteryUnlocked())
                    manager.mastery.onHitEntity(this, (EntityHitResult) result, manager.masteryLevel);
            });
        }
    }

    @Override
    public void addAdditionalSaveData(CompoundTag compound)
    {
        super.addAdditionalSaveData(compound);
        DataTypes.FLOAT.write(compound, "damage", this.damage);
        if (this.weapon != null)
            compound.putString("weapon", BuiltInRegistries.ITEM.getKey(weapon).toString());
        DataTypes.FLOAT.write(compound, "spread", spread);
        DataTypes.FLOAT.write(compound, "penetration", penetration);
    }


    @Override
    public void readAdditionalSaveData(CompoundTag compound)
    {
        super.readAdditionalSaveData(compound);
        this.damage = DataTypes.INT.read(compound, "damage");
        if (compound.contains("weapon"))
        {
            Item item = BuiltInRegistries.ITEM.get(new ResourceLocation(compound.getString("weapon")));
            this.weapon = item == Items.AIR ? null : (AVAItemGun) item;
        }
        this.spread = DataTypes.FLOAT.read(compound, "spread");
        this.penetration = DataTypes.FLOAT.read(compound, "penetration");
    }
}
