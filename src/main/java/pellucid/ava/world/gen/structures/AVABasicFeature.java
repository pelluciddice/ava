package pellucid.ava.world.gen.structures;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.PiecesContainer;
import net.minecraft.world.level.levelgen.structure.pools.JigsawPlacement;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.level.levelgen.structure.pools.alias.PoolAliasLookup;
import pellucid.ava.config.AVAServerConfig;

import java.util.List;
import java.util.Optional;

/***
 * Reference: https://github.com/TelepathicGrunt/StructureTutorialMod/blob/1.19.0-Forge-Jigsaw/src/main/java/com/telepathicgrunt/structuretutorial/structures/SkyStructures.java
 */
public class AVABasicFeature extends Structure implements StructureType<AVABasicFeature>
{
    // A custom codec that changes the size limit for our code_structure_sky_fan.json's config to not be capped at 7.
    // With this, we can have a structure with a size limit up to 30 if we want to have extremely long branches of pieces in the structure.
    public static final MapCodec<AVABasicFeature> CODEC = RecordCodecBuilder.mapCodec((instance) ->
            instance.group(Codec.STRING.fieldOf("type").forGetter((structure) -> structure.name),
                    AVABasicFeature.settingsCodec(instance),
                    StructureTemplatePool.CODEC.fieldOf("start_pool").forGetter((structure) -> structure.startPool),
                    ResourceLocation.CODEC.optionalFieldOf("start_jigsaw_name").forGetter((structure) -> structure.startJigsawName),
                    Codec.intRange(0, 30).fieldOf("size").forGetter((structure) -> structure.size)
            ).apply(instance, AVABasicFeature::new));

    private final String name;
    private final Holder<StructureTemplatePool> startPool;
    private final Optional<ResourceLocation> startJigsawName;
    private final int size;

    public AVABasicFeature(String name, Structure.StructureSettings config, Holder<StructureTemplatePool> pool, Optional<ResourceLocation> jigsaw, int size)
    {
        super(config);
        this.name = name.split(":")[1];
        this.startPool = pool;
        this.startJigsawName = jigsaw;
        this.size = size;
    }

    private static boolean extraSpawningChecks(Structure.GenerationContext context)
    {
        ChunkPos chunkpos = context.chunkPos();
        return context.chunkGenerator().getFirstOccupiedHeight(
                chunkpos.getMinBlockX(),
                chunkpos.getMinBlockZ(),
                Heightmap.Types.MOTION_BLOCKING_NO_LEAVES,
                context.heightAccessor(),
                context.randomState()) < 150;
    }

    @Override
    public HolderSet<Biome> biomes()
    {
        if (!AVAServerConfig.DO_STRUCTURES_GENERATE.get())
            return HolderSet.direct();
        return super.biomes();
    }

    @Override
    public Optional<Structure.GenerationStub> findGenerationPoint(Structure.GenerationContext context)
    {
        if (!AVABasicFeature.extraSpawningChecks(context) || !AVAServerConfig.DO_STRUCTURES_GENERATE.get())
            return Optional.empty();
        int startY = 0;
        ChunkPos chunkPos = context.chunkPos();
        BlockPos blockPos = new BlockPos(chunkPos.getMinBlockX(), startY, chunkPos.getMinBlockZ());
        return JigsawPlacement.addPieces(context, this.startPool, this.startJigsawName, this.size, blockPos, false, Optional.of(Heightmap.Types.WORLD_SURFACE_WG), 15, PoolAliasLookup.create(List.of(), blockPos, context.seed()));
    }

    @Override
    public void afterPlace(WorldGenLevel world, StructureManager manager, ChunkGenerator generator, RandomSource random, BoundingBox boundingBox, ChunkPos chunkPos, PiecesContainer container)
    {
        BlockPos.betweenClosedStream(boundingBox).forEach((pos) -> {
            if (world.getBlockEntity(pos) instanceof ChestBlockEntity chest)
                chest.setLootTable(AVAFeatures.LOOT_TABLES.get(name), random.nextLong());
        });
    }

    @Override
    public StructureType<?> type()
    {
        return AVAFeatures.TYPES.get(name).get();
    }

    @Override
    public GenerationStep.Decoration step()
    {
        return GenerationStep.Decoration.SURFACE_STRUCTURES;
    }

    @Override
    public MapCodec codec()
    {
        return CODEC;
    }
}
