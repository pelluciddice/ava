package pellucid.ava.world.gen.structures;

import com.mojang.serialization.MapCodec;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.storage.loot.LootTable;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.events.data.LootTableDataProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AVAFeatures
{
    public static final AVAFeatures INSTANCE = new AVAFeatures();
    public static final DeferredRegister<StructureType<?>> STRUCTURES = DeferredRegister.create(BuiltInRegistries.STRUCTURE_TYPE, AVA.MODID);
    public static final Map<String, ResourceKey<LootTable>> LOOT_TABLES = new HashMap<>();
    public static final Map<String, List<ResourceKey<Biome>>> BIOMES = new HashMap<>();
    public static final Map<String, DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>>> TYPES = new HashMap<>();
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> BLUE_ROBOT_CONTAINER = structure("blue_robot_container", LootTableDataProvider.AVAChestLoot.BLUE_ROBOT_CONTAINER, Biomes.BEACH, Biomes.SNOWY_BEACH, Biomes.RIVER, Biomes.FROZEN_RIVER, Biomes.SWAMP, Biomes.DRIPSTONE_CAVES);
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> YELLOW_ROBOT_CONTAINER = structure("yellow_robot_container", LootTableDataProvider.AVAChestLoot.YELLOW_ROBOT_CONTAINER, Biomes.BEACH, Biomes.SNOWY_BEACH, Biomes.RIVER, Biomes.FROZEN_RIVER, Biomes.SWAMP, Biomes.DRIPSTONE_CAVES);
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> DARK_BLUE_ROBOT_CONTAINER = structure("dark_blue_robot_container", LootTableDataProvider.AVAChestLoot.DARK_BLUE_ROBOT_CONTAINER, Biomes.BEACH, Biomes.SNOWY_BEACH, Biomes.RIVER, Biomes.FROZEN_RIVER, Biomes.SWAMP, Biomes.DRIPSTONE_CAVES);

    //EU
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> OUTPOST_DESERT = structure("outpost_desert", LootTableDataProvider.AVAChestLoot.OUTPOST_DESERT, Biomes.DESERT);
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> OUTPOST_FOREST = structure("outpost_forest", LootTableDataProvider.AVAChestLoot.OUTPOST_FOREST, Biomes.FOREST);

    //NRF
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> OUTPOST_SNOW = structure("outpost_snow", LootTableDataProvider.AVAChestLoot.OUTPOST_SNOW, Biomes.SNOWY_PLAINS);
    public static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> OUTPOST_OCEAN = structure("outpost_ocean", LootTableDataProvider.AVAChestLoot.OUTPOST_OCEAN, Biomes.OCEAN);

    private static DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> structure(String name, ResourceKey<LootTable> lootTable, ResourceKey<Biome>... biomes)
    {
        LOOT_TABLES.put(name, lootTable);
        BIOMES.put(name, List.of(biomes));
        DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>> obj = STRUCTURES.register(name, () -> new StructureType<AVABasicFeature>()
        {
            @Override
            public MapCodec<AVABasicFeature> codec()
            {
                return AVABasicFeature.CODEC;
            }
        });
        TYPES.put(name, obj);
        return obj;
    }
}
