package pellucid.ava;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.DistExecutor;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.neoforge.client.gui.IConfigScreenFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.cap.AVACapabilities;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.guis.ClientConfigGUI;
import pellucid.ava.config.AVAConfigs;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.fluids.AVAFluids;
import pellucid.ava.items.armours.AVAArmourMaterials;
import pellucid.ava.items.init.AVAItemGroups;
import pellucid.ava.items.init.Magazines;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.init.MeleeWeapons;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.init.Pistols;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.init.Rifles;
import pellucid.ava.items.init.Snipers;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.items.init.SubmachineGuns;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.world.gen.structures.AVAFeatures;

@Mod("ava")
public class AVA
{
    public static final String MODID = "ava";
    public static final Logger LOGGER = LogManager.getLogger(MODID);

    //Test
    public AVA(IEventBus bus)
    {
        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, AVAConfigs.SERVER_SPEC);
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, AVAConfigs.CLIENT_SPEC);
        DistExecutor.runWhenOn(Dist.CLIENT, () -> AVA::registerClientConfigScreenButton);
        AVADataComponents.DATA_COMPONENT_TYPES.register(bus);
        AVAArmourMaterials.ARMOUR_MATERIALS.register(bus);
        AVASounds.registerAll();
        AVASounds.SOUNDS.register(bus);
        AVASoundTracks.registerAll();
        AVABlocks.BLOCKS.register(bus);
        AVABuildingBlocks.BLOCKS.register(bus);
        AVAFluids.FLUIDS.register(bus);
        Magazines.ITEMS.register(bus);
        Projectiles.ITEMS.register(bus);
        Pistols.ITEMS.register(bus);
        Rifles.ITEMS.register(bus);
        Snipers.ITEMS.register(bus);
        SubmachineGuns.ITEMS.register(bus);
        MiscItems.ITEMS.register(bus);
        Materials.ITEMS.register(bus);
        SpecialWeapons.ITEMS.register(bus);
        MeleeWeapons.ITEMS.register(bus);
        AVABlocks.ITEMS.register(bus);
        AVABlocks.registerAllItems();
        AVABuildingBlocks.ITEMS.register(bus);
        AVABuildingBlocks.registerAllItems();
        AVAEntities.ENTITIES.register(bus);
        AVATEContainers.CONTAINERS.register(bus);
        AVATEContainers.BLOCK_ENTITIES.register(bus);
        AVAFeatures.STRUCTURES.register(bus);
        AVACapabilities.CAPABILITIES.register(bus);
        AVAItemGroups.TABS.register(bus);
    }

    @OnlyIn(Dist.CLIENT)
    private static void registerClientConfigScreenButton()
    {
        ModLoadingContext.get().registerExtensionPoint(IConfigScreenFactory.class, () -> (client, parent) -> new ClientConfigGUI());
    }
}
