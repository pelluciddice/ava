package pellucid.ava.events;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.AddReloadListenerEvent;
import net.neoforged.neoforge.event.RegisterCommandsEvent;
import net.neoforged.neoforge.event.entity.living.LivingChangeTargetEvent;
import net.neoforged.neoforge.event.entity.living.LivingDamageEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import net.neoforged.neoforge.event.server.ServerStartedEvent;
import net.neoforged.neoforge.event.tick.EntityTickEvent;
import net.neoforged.neoforge.event.tick.ServerTickEvent;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.commands.AVACommands;
import pellucid.ava.entities.objects.kits.AmmoKitEntity;
import pellucid.ava.entities.objects.kits.FirstAidKitEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.events.data.custom.GunStatDataReloadListener;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesServerManager;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

@EventBusSubscriber(modid = AVA.MODID)
public class CommonForgeEventBus
{
    @SubscribeEvent
    public static void onReloadListenersAdded(AddReloadListenerEvent event)
    {
        event.addListener(new GunStatDataReloadListener());
        event.addListener(LoadingImagesServerManager.INSTANCE);
    }

    @SubscribeEvent
    public static void registerCommands(RegisterCommandsEvent event)
    {
        AVACommands.registerAll(event.getDispatcher(), event.getBuildContext());
    }

    @SubscribeEvent
    public static void onServerStart(ServerStartedEvent event)
    {
        try
        {
            AVAWorldData.clearServerInstances();
            AVACrossWorldData.clearServerInstance();
            LoadingImagesServerManager.INSTANCE.serverInit(event.getServer());
        }
        catch (Exception e)
        {
            AVA.LOGGER.error(e.getMessage());
        }
    }

    @SubscribeEvent
    public static void onServerTick(ServerTickEvent.Pre event)
    {
        MinecraftServer server = event.getServer();
        AVACrossWorldData.getInstance().tick(server.overworld());
        server.getAllLevels().forEach((world) -> AVAWorldData.getInstance(world).tick(world));
        LoadingImagesServerManager.INSTANCE.tick();
    }

    @SubscribeEvent
    public static void onLivingDeath(LivingDeathEvent event)
    {
        LivingEntity entity = event.getEntity();
        Level world = entity.getCommandSenderWorld();
        AVACrossWorldData crossData = AVACrossWorldData.getInstance();
        AVAWorldData data = AVAWorldData.getInstance(world);
        if (!world.isClientSide())
        {
            if (entity instanceof Enemy)
            {
                if (AVAConstants.RAND.nextFloat() <= crossData.mobDropsKitsChance)
                    world.addFreshEntity(AVAConstants.RAND.nextBoolean() ? new AmmoKitEntity(world, entity.getX(), entity.getY(), entity.getZ()) : new FirstAidKitEntity(world, entity.getX(), entity.getY(), entity.getZ()));
            }
            data.uavEscortS.remove(entity.getUUID());
            data.uavS.remove(entity.getUUID());
            data.x9S.remove(entity.getUUID());
        }
        else
        {
            data.uavEscortC.remove(entity.getId());
            data.uavC.remove(entity.getId());
            data.x9C.remove(entity.getId());
        }
    }

    @SubscribeEvent
    public static void onLivingSetsTarge(LivingChangeTargetEvent event)
    {
        LivingEntity entity = event.getEntity();
        CompoundTag compound = entity.getPersistentData();
        if (entity instanceof Mob && compound.contains(AVAConstants.TAG_ENTITY_BLINDED) && event.getNewTarget() != null)
            ((Mob) entity).setTarget(null);
    }

    @SubscribeEvent
    public static void onLivingUpdate(EntityTickEvent.Post event)
    {
        Entity entity = event.getEntity();
        CompoundTag compound = entity.getPersistentData();
        if (entity instanceof Mob && compound.contains(AVAConstants.TAG_ENTITY_BLINDED))
        {
            int duration = DataTypes.INT.read(compound, AVAConstants.TAG_ENTITY_BLINDED);
            if (duration - 1 <= 0)
                compound.remove(AVAConstants.TAG_ENTITY_BLINDED);
            else
                DataTypes.INT.write(compound, AVAConstants.TAG_ENTITY_BLINDED, duration - 1);
        }
    }

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent event)
    {
        if (!event.isEndConquered())
        {
            Player player = event.getEntity();
            Level world = player.level();
            if (!world.isClientSide())
                AVAWeaponUtil.setEntityPosByTeamSpawn(player, 3);
        }
    }

    @SubscribeEvent
    public static void onEntityHurt(LivingDamageEvent event)
    {
        LivingEntity attacked = event.getEntity();
        Entity attacker = event.getSource().getEntity();
        if (!(attacker instanceof LivingEntity))
            return;
        if (attacked instanceof SidedSmartAIEntity)
            ((SidedSmartAIEntity) attacked).warnNearbyAllies((LivingEntity) attacker);
        else if (attacked instanceof Player)
            AVAWeaponUtil.warnNearbySmarts((Player) attacked, (LivingEntity) attacker);
    }
}
