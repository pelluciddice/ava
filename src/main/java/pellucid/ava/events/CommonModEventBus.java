package pellucid.ava.events;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.minecraft.world.item.ItemStack;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent;
import net.neoforged.neoforge.network.event.RegisterPayloadHandlersEvent;
import pellucid.ava.config.AVAConfig;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.events.data.BlockStateDataProvider;
import pellucid.ava.events.data.ItemModelDataProvider;
import pellucid.ava.events.data.LootTableDataProvider;
import pellucid.ava.events.data.RecipeDataProvider;
import pellucid.ava.events.data.SoundProvider;
import pellucid.ava.events.data.custom.GunStatDataProvider;
import pellucid.ava.events.data.lang.LangDataProviderENUS;
import pellucid.ava.events.data.tags.AVABlockTagsProvider;
import pellucid.ava.events.data.tags.AVADamageSourceTagsProvider;
import pellucid.ava.events.data.tags.AVAFluidTagsProvider;
import pellucid.ava.events.data.tags.AVAItemTagsProvider;
import pellucid.ava.events.data.tags.BiomeTagsProvider;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.packets.AVAPackets;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.concurrent.CompletableFuture;

@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD)
public class CommonModEventBus
{
    @SubscribeEvent
    public static void addEntitySpawns(SpawnPlacementRegisterEvent event)
    {
        AVAEntities.addSpawns(event);
    }

    @SubscribeEvent
    public static void registerMessages(RegisterPayloadHandlersEvent event)
    {
        AVAPackets.registerMessages(event);
    }

    @SubscribeEvent
    public static void onConfigReload(ModConfigEvent.Reloading event)
    {
        AVAConfig.onConfigReload(event);
    }

    @SubscribeEvent
    public static void onSetup(FMLCommonSetupEvent event)
    {

        MiscItems.commonSetup();
        Materials.commonSetup();

        for (AVAItemGun gun : AVAWeaponUtil.getAllGuns())
            ((Ammo) gun.getAmmoType(new ItemStack(gun))).add(gun);
    }

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event)
    {
        DataGenerator gen = event.getGenerator();
        CompletableFuture<HolderLookup.Provider> provider = event.getLookupProvider();
        PackOutput output = gen.getPackOutput();
        ExistingFileHelper helper = event.getExistingFileHelper();
        boolean client = event.includeClient();
        boolean server = event.includeServer();
        gen.addProvider(client, new LangDataProviderENUS(output));
        gen.addProvider(client, new ItemModelDataProvider(output, helper));
        gen.addProvider(client, new BlockStateDataProvider(output, helper));
        gen.addProvider(client, new SoundProvider(output, helper));
//        AVAModelTypes.TYPES.forEach((t) -> gen.addProvider(client, new ItemModelResourcesProvider(output, t)));
        gen.addProvider(server, new BiomeTagsProvider(output, provider, helper));
        AVABlockTagsProvider blocks = new AVABlockTagsProvider(output, provider, helper);
        gen.addProvider(server, blocks);
        gen.addProvider(server, new AVAItemTagsProvider(output, provider, blocks.contentsGetter(), helper));
        gen.addProvider(server, new RecipeDataProvider(output, provider));
        gen.addProvider(server, new LootTableDataProvider(output, provider));
        gen.addProvider(server, new GunStatDataProvider(output));
        gen.addProvider(server, new AVAFluidTagsProvider(output, provider, helper));
        gen.addProvider(server, new AVADamageSourceTagsProvider(output, provider, helper));
    }

    @SubscribeEvent
    public static void registerEntityTypeAttributes(EntityAttributeCreationEvent event)
    {
        AVAEntities.setEntityAttributes(event);
    }
}
