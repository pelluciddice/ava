package pellucid.ava.events.data;

import net.minecraft.client.renderer.block.model.BlockModel;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.IronBarsBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.AttachFace;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.neoforge.client.model.generators.BlockModelBuilder;
import net.neoforged.neoforge.client.model.generators.BlockStateProvider;
import net.neoforged.neoforge.client.model.generators.ConfiguredModel;
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder;
import net.neoforged.neoforge.client.model.generators.ModelFile;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.registries.DeferredBlock;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.blocks.DirectionalShapedBlock;
import pellucid.ava.blocks.FloorBlock;
import pellucid.ava.blocks.IParentedBlock;
import pellucid.ava.blocks.WallLightBlock;
import pellucid.ava.blocks.repairable.RepairableTileEntity;
import pellucid.ava.blocks.rpg_box.RPGBoxBlock;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class BlockStateDataProvider extends BlockStateProvider
{
    public BlockStateDataProvider(PackOutput gen, ExistingFileHelper exFileHelper)
    {
        super(gen, AVA.MODID, exFileHelper);
    }

    @Override
    protected void registerStatesAndModels()
    {
        for (DeferredBlock<Block> block : AVABlocks.GLASS_BLOCKS)
        {
            String path = block.getId().getPath().replace("repairable_", "");
            simpleBlockItem(block.get(), cubeAll(BuiltInRegistries.BLOCK.get(new ResourceLocation(path))));
            ConfiguredModel model = new ConfiguredModel(new ModelFile.UncheckedModelFile(mcLoc("block/" + path)));
            getVariantBuilder(block.get()).partialState()
                    .with(RepairableTileEntity.DESTRUCTED, true).addModels(model);
            getVariantBuilder(block.get()).partialState()
                    .with(RepairableTileEntity.DESTRUCTED, false).addModels(model);
        }
        for (DeferredBlock<Block> block : AVABlocks.GLASS_PANE_BLOCKS)
        {
            String path = block.getId().getPath();
            String name = path.replace("repairable_", "");
            String name2 = path.replace("repairable_", "").replace("_pane", "");
            paneBlock((IronBarsBlock) block.get(), mcLoc("block/" + name2), mcLoc("block/" + name + "_top"));
            itemModels().getBuilder(path).parent(new ModelFile.UncheckedModelFile(mcLoc("item/generated"))).texture("layer0", mcLoc("block/"+ name2));
        }
        for (DeferredBlock<Block> b : AVABuildingBlocks.WALL_LIGHT_BLOCKS)
        {
            Block block = ((IParentedBlock) b.get()).getBlock();
            Block parent = ((IParentedBlock) b.get()).getParent();

            ResourceLocation texture = new ResourceLocation(BuiltInRegistries.BLOCK.getKey(parent).getNamespace() + ":block/" + AVAClientUtil.toValidTexture(BuiltInRegistries.BLOCK.getKey(parent).getPath()));
            BlockModelBuilder model = models().withExistingParent("block/wall_lights/" + b.getId().getPath(), "ava:block/parents/wall_light")
                    .texture("edge", texture);
            BlockModelBuilder litModel = models().withExistingParent("block/wall_lights/" + b.getId().getPath() + "_lit", "ava:block/parents/wall_light_lit")
                    .texture("edge", texture);

            getVariantBuilder(block).forAllStates((state) -> {
                Direction facing = state.getValue(WallLightBlock.FACING);
                AttachFace face = state.getValue(WallLightBlock.FACE);
                boolean lit = state.getValue(WallLightBlock.LIT);

                return ConfiguredModel.builder()
                        .modelFile(lit ? litModel : model)
                        .rotationX(face == AttachFace.FLOOR ? 0 : (face == AttachFace.WALL ? 90 : 180))
                        .rotationY((int) (face == AttachFace.CEILING ? facing : facing.getOpposite()).toYRot())
                        .build();
            });
            blockItem(block, "/wall_lights/").transforms()
                    .transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).translation(0, 8, 0).end()
                    .transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).translation(0, 8, 0).end()
                    .transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).translation(0, 8, 0).end()
                    .transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).translation(0, 8, 0).end()
                    .transform(ItemDisplayContext.GROUND).translation(0, 4, 0).end()
                    .transform(ItemDisplayContext.FIXED).rotation(-90, 0, 0).translation(0, 0, -8).end()
                    .transform(ItemDisplayContext.GUI).rotation(90, 0, 0).end().end()
                    .guiLight(BlockModel.GuiLight.FRONT);
        }

        for (int i = 0; i < AVABuildingBlocks.PLASTER_BLOCKS.size(); i++)
        {
            DeferredBlock<Block> parent = AVABuildingBlocks.PLASTER_BLOCKS.get(i);
            blockWithItem(parent.get(), this::simpleBlock);
            blockWithItem(AVABuildingBlocks.PLASTER_STAIRS.get(i).get(), (b) -> stairsBlock(b, parent.get()));
            blockWithItem(AVABuildingBlocks.PLASTER_SLABS.get(i).get(), (b) -> slabBlock(b, parent.get()));
        }

        for (int i = 0; i < AVABuildingBlocks.PLASTER_BLOCKS_2.size(); i++)
        {
            Supplier<Block> parent = AVABuildingBlocks.PLASTER_BLOCKS_2.get(i);
            blockWithItem(parent.get(), this::simpleBlock);
            blockWithItem(AVABuildingBlocks.PLASTER_STAIRS_2.get(i).get(), (b) -> stairsBlock(b, parent.get()));
            blockWithItem(AVABuildingBlocks.PLASTER_SLABS_2.get(i).get(), (b) -> slabBlock(b, parent.get()));
        }

        for (DeferredBlock<Block> block : AVABuildingBlocks.VANILLA_SERIES_EXTENSIONS_BLOCKS)
            blockWithItem(block.get(), this::simpleBlock);

        buildDirectionalParentBlocks("thin_pillar", AVABuildingBlocks.THIN_PILLAR_BLOCKS, (state) -> ((DirectionalShapedBlock) state.getBlock()).getShape(state, Direction.NORTH), null, null);
        buildDirectionalParentBlocks("wall_thin_pillar", AVABuildingBlocks.WALL_THIN_PILLAR_BLOCKS, (state) -> ((DirectionalShapedBlock) state.getBlock()).getShape(state, Direction.NORTH), null, null);

        for (DeferredBlock<Block> block : AVABuildingBlocks.SLOPE_BLOCKS)
            createHorizontalBlockWithOutAutoModel("slope_" + AVABuildingBlocks.SLOPE_ANGLES.get(block), ((IParentedBlock) block.get()).getBlock(), blockTexture(((IParentedBlock) block.get()).getParent()));

        BiFunction<BlockState, String, String> floorBlockModelNameFunction = (state, name) -> name + (state.getValue(FloorBlock.IS_TOP) ? "_top" : "_bottom");

        for (DeferredBlock<Block> block : AVABuildingBlocks.PLANKS_FLOOR_BLOCKS)
            floorItemTransform(blockWithItem(block.get(), (b) -> "planks_floors/" + block.getId().getPath() + "_top", (b) -> createHorizontalBlockWithModel("planks_floor", b, blockTexture(((IParentedBlock) b).getParent()), (state) -> ((DirectionalShapedBlock) state.getBlock()).getShape(state, Direction.NORTH), floorBlockModelNameFunction, floorBlockModelNameFunction)));

        stairsBlock((StairBlock) AVABuildingBlocks.SMOOTH_STONE_STAIRS.get(), new ResourceLocation("block/smooth_stone"));
        identicalModel(AVABlocks.REPAIRABLE_FLOWER_POT.get(), Blocks.FLOWER_POT);

        simpleBlockItem(AVABuildingBlocks.COBBLED_SANDSTONE_TILE.get());
        blockWithItem(AVABuildingBlocks.COBBLED_SANDSTONE_TILE_STAIRS.get(), (b) -> stairsBlock(b, AVABuildingBlocks.COBBLED_SANDSTONE_TILE.get()));

        blockWithItem(AVABuildingBlocks.COBBLED_SANDSTONE_TILE_SLAB.get(), (b) -> slabBlock(b, AVABuildingBlocks.COBBLED_SANDSTONE_TILE.get()));

        floorItemTransform(blockWithItem(AVABuildingBlocks.IRON_GRID.get(), (b) -> BuiltInRegistries.BLOCK.getKey(b).getPath() + "_top", (b) -> horizontalBlock(b, (state) -> {
            ResourceLocation texture = blockTexture(b);
            return createModel(floorBlockModelNameFunction.apply(state, BuiltInRegistries.BLOCK.getKey(b).getPath()), ((DirectionalShapedBlock) state.getBlock()).getShape(state, Direction.NORTH))
                    .texture("texture", texture)
                    .texture("particle", texture);
        })));

        simpleBlockItem(AVABlocks.COMMAND_EXECUTOR.get());
        getVariantBuilder(AVABlocks.RPG_BOX.get()).forAllStates((state) -> {
            Direction facing = state.getValue(RPGBoxBlock.FACING);
            boolean open = state.getValue(RPGBoxBlock.OPEN);
            boolean top = state.getValue(RPGBoxBlock.HALF) == DoubleBlockHalf.UPPER;

            ModelFile topModel = models().getExistingFile(new ResourceLocation(AVA.MODID, "block/rpg_box_top" + (open ? "" : "_closed")));
            ModelFile bottomModel = models().getExistingFile(new ResourceLocation(AVA.MODID, "block/rpg_box_bottom" + (open ? "" : "_closed")));

            return ConfiguredModel.builder()
                    .modelFile(top ? topModel : bottomModel)
                    .rotationY((int) facing.toYRot())
                    .build();
        });
        paneBlock((IronBarsBlock) AVABuildingBlocks.HARDENED_IRON_BARS.get(), mcLoc("block/iron_bars"), mcLoc("block/iron_bars"));
        itemModels().getBuilder("item/hardened_iron_bars").parent(new ModelFile.UncheckedModelFile(mcLoc("item/generated"))).texture("layer0", mcLoc("block/iron_bars"));
    }

    private void floorItemTransform(ItemModelBuilder builder)
    {
        builder.transforms()
                .transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).translation(0, -6.75F, 0).end()
                .transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).translation(0, -6.75F, 0).end()
                .transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).translation(0, -6.75F, 0).end()
                .transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).translation(0, -6.75F, 0).end()
                .transform(ItemDisplayContext.GROUND).translation(0, -10, 0).end()
                .transform(ItemDisplayContext.FIXED).rotation(-90, 0, 0).translation(0, 0, -7.25F).end()
                .transform(ItemDisplayContext.GUI).rotation(-90, 0, 0).end();
    }

    private void slabBlock(Block block, Block parent)
    {
        ResourceLocation name = BuiltInRegistries.BLOCK.getKey(parent);
        ResourceLocation rel = new ResourceLocation(name.getNamespace() + ":block/" + name.getPath());;
        slabBlock((SlabBlock) block, rel, rel);
    }

    private void stairsBlock(Block block, Block parent)
    {
        ResourceLocation name = BuiltInRegistries.BLOCK.getKey(parent);
        stairsBlock((StairBlock) block, new ResourceLocation(name.getNamespace() + ":block/" + name.getPath()));
    }

    private void simpleBlockItem(Block block)
    {
        blockWithItem(block, this::simpleBlock);
    }

    private ItemModelBuilder blockWithItem(Block block, Consumer<Block> blockModel)
    {
        blockModel.accept(block);
        return blockItem(block);
    }

    private ItemModelBuilder blockWithItem(Block block, Function<Block, String> blockModelFileName, Consumer<Block> blockModel)
    {
        blockModel.accept(block);
        return blockItem(block, blockModelFileName);
    }

    private ItemModelBuilder blockItem(Block block, String folder)
    {
        return blockItem(block, folder, (b) -> AVACommonUtil.getRegistryNameBlock(block).getPath());
    }

    private ItemModelBuilder blockItem(Block block, Function<Block, String> fileName)
    {
        return blockItem(block, "", fileName);
    }

    private ItemModelBuilder blockItem(Block block, String folder, Function<Block, String> fileName)
    {
        if (folder.isEmpty())
            folder = "/";
        return itemModels().getBuilder(AVACommonUtil.getRegistryNameBlock(block).getPath()).parent(itemModels().getExistingFile(new ResourceLocation(AVA.MODID, "block" + folder + fileName.apply(block))));
    }

    @Override
    public ResourceLocation blockTexture(Block block)
    {
        return AVAClientUtil.toValidTexture(super.blockTexture(block));
    }

    private ItemModelBuilder blockItem(Block block)
    {
        return blockItem(block, "");
    }

    private void identicalModel(Block block, Block parent)
    {
        String name = AVACommonUtil.getRegistryNameBlock(block).getPath();
        String name2 = BuiltInRegistries.BLOCK.getKey(parent).getPath();
        BlockModelBuilder model = models().withExistingParent("block/" + name, "block/" + name2);
        getVariantBuilder(block).partialState().modelForState().modelFile(model).addModel();
        itemModels().withExistingParent("item/" + name, "item/" + name2);
    }

    private void buildDirectionalParentBlocks(String name, List<DeferredBlock<Block>> blocks, Function<BlockState, VoxelShape> shape, BiFunction<BlockState, String, String> modelFileName, BiFunction<BlockState, String, String> parentModelName)
    {
        for (DeferredBlock<Block> b : blocks)
            createHorizontalBlockWithModel(name, ((IParentedBlock) b.get()).getBlock(), blockTexture(((IParentedBlock) b.get()).getParent()), shape, modelFileName, parentModelName);
    }

    private void createHorizontalBlockWithOutAutoModel(String name, Block block, ResourceLocation texture)
    {
        horizontalBlock(block, (state) -> {
            return models().withExistingParent("block/"+ name + "s/" + AVACommonUtil.getRegistryNameBlock(block).getPath(), "ava:block/parents/" + name)
                    .texture("texture", texture)
                    .texture("particle", texture);
        });
    }

    private void createHorizontalBlockWithModel(String name, Block block, ResourceLocation texture, Function<BlockState, VoxelShape> shape, BiFunction<BlockState, String, String> modelFileName, BiFunction<BlockState, String, String> parentModelName)
    {
        horizontalBlock(block, (state) -> {
            String modelName = "block/" + name + "s/" + AVACommonUtil.getRegistryNameBlock(block).getPath();
            if (modelFileName != null)
                modelName = modelFileName.apply(state, modelName);

            return models().getBuilder(modelName)
                    .parent(createParentModel(parentModelName == null ? name : parentModelName.apply(state, name), shape.apply(state)))
                    .texture("texture", texture)
                    .texture("particle", texture);
        });
    }

    private BlockModelBuilder createParentModel(String name, VoxelShape shape)
    {
        return createModel("parents/" + name + "_parent", shape);
    }

    private final Map<String, BlockModelBuilder> models = new HashMap<>();
    private BlockModelBuilder createModel(String name, VoxelShape shape)
    {
        String k = "block/" + name;
        return models.computeIfAbsent(k, (key) -> {
            BlockModelBuilder model = models().withExistingParent(key, "block/block");
            for (AABB aabb : shape.toAabbs())
                model.element().from((float) (aabb.minX * 16.0F), (float) (aabb.minY * 16.0F), (float) (aabb.minZ * 16.0F)).to((float) (aabb.maxX * 16.0F), (float) (aabb.maxY * 16.0F), (float) (aabb.maxZ * 16.0F)).allFaces((direction, builder) -> builder.texture("#texture").texture("#particle")).end();
            return model;
        });
    }
}
