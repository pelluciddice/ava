package pellucid.ava.events.data.tags;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.registries.DeferredHolder;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.AVA;
import pellucid.ava.world.gen.structures.AVABasicFeature;
import pellucid.ava.world.gen.structures.AVAFeatures;

import java.lang.reflect.Field;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;

public class BiomeTagsProvider extends TagsProvider<Biome>
{
    public BiomeTagsProvider(PackOutput generator, CompletableFuture<HolderLookup.Provider> provider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(generator, Registries.BIOME, provider, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider)
    {
        for (Field field : AVAFeatures.class.getFields())
        {
            try
            {
                Object obj = field.get(AVAFeatures.INSTANCE);
                if (obj.getClass().isAssignableFrom(DeferredHolder.class))
                {
                    TagAppender<Biome> tag = tag(TagKey.create(Registries.BIOME, new ResourceLocation(AVA.MODID, "has_structure/" + field.getName().toLowerCase(Locale.ROOT))));
                    for (ResourceKey<Biome> acceptableBiome : AVAFeatures.BIOMES.get(((DeferredHolder<StructureType<?>, StructureType<AVABasicFeature>>) obj).getId().getPath()))
                        tag.add(acceptableBiome);
                }
            }
            catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getName()
    {
        return "AVA Structures Tags";
    }
}
