package pellucid.ava.events.data.tags;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.AVA;
import pellucid.ava.world.gen.structures.AVAFeatures;

import java.lang.reflect.Field;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;

public class StructureTagsProvider extends TagsProvider<StructureSet>
{
    public static final ResourceLocation AVA_STRUCTURES_SET_RL = new ResourceLocation(AVA.MODID, "ava_structures");
    public static final TagKey<StructureSet> AVA_STRUCTURES_SET = TagKey.create(Registries.STRUCTURE_SET, AVA_STRUCTURES_SET_RL);

    public static final ResourceLocation AFTER_PLACE_SET_RL = new ResourceLocation(AVA.MODID, "ava_dimension_structure_set");
    public static final TagKey<StructureSet> AFTER_PLACE_STRUCTURES_SET = TagKey.create(Registries.STRUCTURE_SET, AFTER_PLACE_SET_RL);

    
    public StructureTagsProvider(PackOutput generator, CompletableFuture<HolderLookup.Provider> provider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(generator, Registries.STRUCTURE_SET, provider, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider)
    {
        TagAppender<StructureSet> tag = tag(AVA_STRUCTURES_SET);
        for (Field field : AVAFeatures.class.getFields())
            tag.add(ResourceKey.create(Registries.STRUCTURE_SET, new ResourceLocation(AVA.MODID, field.getName().toLowerCase(Locale.ROOT))));
    }

    @Override
    public String getName()
    {
        return "AVA Structures Tags";
    }
}
