package pellucid.ava.events.data.tags;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.FluidTagsProvider;
import net.minecraft.tags.FluidTags;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.AVA;
import pellucid.ava.fluids.AVAFluids;

import java.util.concurrent.CompletableFuture;

public class AVAFluidTagsProvider extends FluidTagsProvider
{
    public AVAFluidTagsProvider(PackOutput generator, CompletableFuture<HolderLookup.Provider> provider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(generator, provider, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider)
    {
        tag(FluidTags.WATER).add(AVAFluids.VOID_WATER.get(), AVAFluids.FLOWING_VOID_WATER.get());
    }
}
