package pellucid.ava.events.data.tags;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.internal.versions.neoforge.NeoForgeVersion;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.AVA;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.init.MiscItems;

import java.util.concurrent.CompletableFuture;

public class AVAItemTagsProvider extends ItemTagsProvider
{
    public static final TagKey<Item> WORK_HARDENED_IRON = key("ingots/work_hardened_iron");
    public static final TagKey<Item> SPRING = key("spring");
    public static final TagKey<Item> PLASTIC = key("plastic");
    public static final TagKey<Item> PACKED_GUNPOWDER = key("packed_gunpowder");
    public static final TagKey<Item> SILICON = key("silicon");
    public static final TagKey<Item> COMPRESSED_WOOD = key("compressed_wood");
    public static final TagKey<Item> FIBRE = key("fibre");
    public static final TagKey<Item> CERAMIC = key("ceramic");

    public AVAItemTagsProvider(PackOutput gen, CompletableFuture<HolderLookup.Provider> lookupProvider, CompletableFuture<TagsProvider.TagLookup<Block>> provider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(gen, lookupProvider, provider, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider)
    {
        tag(WORK_HARDENED_IRON).add(Materials.WORK_HARDENED_IRON.get());
        tag(SPRING).add(Materials.SPRING.get());
        tag(PLASTIC).add(Materials.PLASTIC.get());
        tag(PACKED_GUNPOWDER).add(Materials.PACKED_GUNPOWDER.get());
        tag(SILICON).add(Materials.SILICON.get());
        tag(COMPRESSED_WOOD).add(Materials.COMPRESSED_WOOD.get());
        tag(FIBRE).add(Materials.FIBRE.get());
        tag(CERAMIC).add(Materials.CERAMIC.get());

        tag(ItemTags.FREEZE_IMMUNE_WEARABLES).add(MiscItems.EU_STANDARD_BOOTS.get(), MiscItems.EU_STANDARD_TROUSERS.get(), MiscItems.EU_STANDARD_KEVLAR.get(), MiscItems.EU_STANDARD_HELMET.get());
        tag(ItemTags.FREEZE_IMMUNE_WEARABLES).add(MiscItems.NRF_STANDARD_BOOTS.get(), MiscItems.NRF_STANDARD_TROUSERS.get(), MiscItems.NRF_STANDARD_KEVLAR.get(), MiscItems.NRF_STANDARD_HELMET.get());
    }

    private static TagKey<Item> key(String name)
    {
        return ItemTags.create(new ResourceLocation(NeoForgeVersion.MOD_ID, name));
    }
}
