package pellucid.ava.events.data.tags;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.util.AVACommonUtil;

import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class AVABlockTagsProvider extends BlockTagsProvider
{
    public AVABlockTagsProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(output, lookupProvider, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider p_256380_)
    {
        tag(BlockTags.STAIRS).add(AVABuildingBlocks.SMOOTH_STONE_STAIRS.get());
        tag(BlockTags.WALLS).add(AVABuildingBlocks.GLASS_FENCE.get(), AVABuildingBlocks.GLASS_FENCE_TALL.get());
        tag(BlockTags.MINEABLE_WITH_PICKAXE).add(AVACommonUtil.collect(AVABuildingBlocks.CONCRETE_BLOCKS, Supplier::get).toArray(new Block[0]));
        tag(BlockTags.MINEABLE_WITH_SHOVEL).add(AVACommonUtil.collect(AVABuildingBlocks.CONCRETE_POWDER_BLOCKS, Supplier::get).toArray(new Block[0]));
    }
}
