package pellucid.ava.events.data.tags;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageType;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;
import pellucid.ava.AVA;
import pellucid.ava.entities.AVADamageSources;

import java.util.concurrent.CompletableFuture;

public class AVADamageSourceTagsProvider extends TagsProvider<DamageType>
{
    public static final TagKey<DamageType> DIRECT = TagKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "direct"));
    public static final TagKey<DamageType> BULLET = TagKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "bullet"));
    public static final TagKey<DamageType> EXPLOSION = TagKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "explosion"));
    public static final TagKey<DamageType> TOXIC_GAS = TagKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "toxic_gas"));
    public static final TagKey<DamageType> SYSTEM_REMOVE = TagKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(AVA.MODID, "system_remove"));

    public AVADamageSourceTagsProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider, @Nullable ExistingFileHelper existingFileHelper)
    {
        super(output, Registries.DAMAGE_TYPE, lookupProvider, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider provider)
    {
        tag(DamageTypeTags.IS_EXPLOSION).add(AVADamageSources.EXPLOSION);
        tag(DamageTypeTags.BYPASSES_ARMOR).add(AVADamageSources.TOXIC_GAS, AVADamageSources.SYSTEM_REMOVE);
        tag(DamageTypeTags.BYPASSES_INVULNERABILITY).add(AVADamageSources.SYSTEM_REMOVE);

        tag(DIRECT).add(AVADamageSources.DIRECT);
        tag(BULLET).add(AVADamageSources.BULLET);
        tag(EXPLOSION).add(AVADamageSources.EXPLOSION);
        tag(TOXIC_GAS).add(AVADamageSources.TOXIC_GAS);
        tag(SYSTEM_REMOVE).add(AVADamageSources.SYSTEM_REMOVE);
    }
}
