package pellucid.ava.events.data.custom.models;

import com.google.common.hash.Hashing;
import com.google.common.hash.HashingOutputStream;
import com.google.gson.JsonElement;
import net.minecraft.Util;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.events.data.BaseDataProvider;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Deprecated
public class ItemModelResourcesProvider extends BaseDataProvider
{
    public static final boolean FORCE_OVERRIDE = true;

    private final AVAModelTypes<?, ?, ?, ?, ?, ?> type;
    public ItemModelResourcesProvider(PackOutput generator, AVAModelTypes<?, ?, ?, ?, ?, ?> type)
    {
        super(generator);
        this.type = type;
    }

    @Override
    public CompletableFuture<?> run(CachedOutput output)
    {
        List<CompletableFuture<?>> list = new ArrayList<>();
        Path path = dataGenerator.getOutputFolder();
        type.getModelManager().getModelProperties().forEach((gun, property) -> {
            Path outputFile = path.resolve("assets/ava/models/" + type.name + "/" + BuiltInRegistries.ITEM.getKey(gun).getPath() + ".json");
            list.add(saveStableCustomLines(output, property.writeToJsonR(), outputFile));
        });
        return CompletableFuture.allOf(list.toArray(CompletableFuture[]::new));
    }

    @Override
    public String getName()
    {
        return "AVA " + AVACommonUtil.toDisplayString(type.name) + " Models";
    }

    public static CompletableFuture<?> saveStableCustomLines(CachedOutput output, JsonElement json, Path path)
    {
        return CompletableFuture.runAsync(() -> {
            try
            {
                if (FORCE_OVERRIDE)
                    Files.deleteIfExists(path);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                HashingOutputStream hashedStream = new HashingOutputStream(Hashing.sha1(), stream);
                stream.write(AVAClientUtil.avaPrettyPrint(json, 0).getBytes());
                output.writeIfNeeded(path, stream.toByteArray(), hashedStream.hash());
            }
            catch (IOException ioexception)
            {
                LOGGER.error("Failed to save file to {}", path, ioexception);
            }

        }, Util.backgroundExecutor());
    }
}
