package pellucid.ava.events.data.custom;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimpleJsonResourceReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.neoforged.neoforge.network.PacketDistributor;
import pellucid.ava.AVA;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.GunStatsMap;
import pellucid.ava.packets.DataToClientMessage;

import java.util.HashMap;
import java.util.Map;

public class GunStatDataReloadListener extends SimpleJsonResourceReloadListener
{
    public static Gson GSON = new GsonBuilder().registerTypeAdapter(GunStatsMap.class, AVAItemGun.Serializer.INSTANCE).create();
    public GunStatDataReloadListener()
    {
        super(GSON, "stats/guns");
    }

    @Override
    protected void apply(Map<ResourceLocation, JsonElement> map, ResourceManager manager, ProfilerFiller profile)
    {
        Map<ResourceLocation, JsonElement> defaultsMap = new HashMap<>();
        Map<ResourceLocation, JsonElement> othersMap = new HashMap<>();
        map.forEach((k, v) -> (k.getNamespace().equals(AVA.MODID) ? defaultsMap : othersMap).put(k, v));
        toJson(defaultsMap);
        toJson(othersMap);
        try
        {
            AVA.LOGGER.debug("Syncing gun stats to clients.");
            DataToClientMessage.weaponStats(PacketDistributor::sendToAllPlayers);
        }
        catch (Exception e)
        {
            AVA.LOGGER.error("Failed to sync! The server is not up yet.");
        }
    }

    private static void toJson(Map<ResourceLocation, JsonElement> map)
    {
        map.entrySet().stream().filter((e) -> e.getKey().getPath().equals("global")).findFirst().ifPresent((g) -> AVAItemGun.Serializer.deserialize(g.getValue()));
        map.forEach((k, v) -> {
            try
            {
                AVAItemGun.Serializer.deserialize(v);
            }
            catch (Exception e)
            {
                AVA.LOGGER.error("Error trying to parse gun stat from data file " + k);
                e.printStackTrace();
            }
        });
    }
}
