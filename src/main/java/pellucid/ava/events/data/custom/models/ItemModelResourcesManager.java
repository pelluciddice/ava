package pellucid.ava.events.data.custom.models;

import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.ResourceManagerReloadListener;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.util.QuadFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import static net.minecraft.world.item.ItemDisplayContext.FIRST_PERSON_RIGHT_HAND;

public abstract class ItemModelResourcesManager<I extends Item, S extends ISubModels<I>, P extends RegularModelProperty<I, ?, S, P>> implements ResourceManagerReloadListener
{
    public final AVAModelTypes<I, ?, ?, P, ?, ?> type;
    protected final Map<I, P> modelProperties;

    public ItemModelResourcesManager(AVAModelTypes<I, ?, ?, P, ?, ?> type)
    {
        this.type = type;
        this.modelProperties = new HashMap<>();
        generate();
    }

    public Map<I, P> getModelProperties()
    {
        return modelProperties;
    }

    public abstract void generate();

    public void put(DeferredItem<Item> item, Perspective fpRight, Perspective leftHandIdle, Perspective rightHandIdle, QuadFunction<P, Perspective, Perspective, Perspective, P> property)
    {
        P p = type.constructProperty((I) item.get());
        p.display2(FIRST_PERSON_RIGHT_HAND, fpRight);
        p.leftHandsIdle(leftHandIdle);
        p.rightHandsIdle(rightHandIdle);
        modelProperties.put((I) item.get(), property.apply(p, fpRight, leftHandIdle, rightHandIdle));
    }

    protected void copyFrom(Supplier<Item> item, Supplier<Item> master)
    {
        modelProperties.put((I) item.get(), modelProperties.get((I) master.get()).copyFor((I) item.get()));
    }

    protected void copyFrom(Supplier<Item> item, Supplier<Item> master, S... subModels)
    {
        modelProperties.put((I) item.get(), modelProperties.get((I) master.get()).copyFor((I) item.get()).subModels(subModels));
    }

    @Override
    public void onResourceManagerReload(ResourceManager manager)
    {
        AVA.LOGGER.info("Reloading " + type.name + " model resources");
        generate();
//        manager.listPacks().forEach((resource) -> {
//            resource.listResources(PackType.CLIENT_RESOURCES, AVA.MODID, "models/" + type.name + "s", (a, b) -> {
//                try
//                {
//                    JsonObject json = JsonParser.parseString(IOUtils.toString(b.get(), StandardCharsets.UTF_8)).getAsJsonObject();
//                    @Nullable Item item = BuiltInRegistries.ITEM.get(new ResourceLocation(a.getNamespace(), a.getPath().replace("models/" + type.name + "s/", "").replace(".json", "")));
//                    modelProperties.get(item).readFromJson(json);
//                }
//                catch (IOException e)
//                {
//                    throw new RuntimeException(e);
//                }
//            });
//        });
    }
}
