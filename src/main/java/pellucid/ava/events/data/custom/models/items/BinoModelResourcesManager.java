package pellucid.ava.events.data.custom.models.items;

import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.binocular.BinoSubModels;
import pellucid.ava.client.renderers.models.binocular.RegularBinoModelProperty;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.items.miscs.BinocularItem;

public class BinoModelResourcesManager extends ItemModelResourcesManager<BinocularItem, BinoSubModels, RegularBinoModelProperty>
{
    public static final BinoModelResourcesManager INSTANCE = new BinoModelResourcesManager();

    public BinoModelResourcesManager()
    {
        super(AVAModelTypes.BINOCULAR);
    }

    @Override
    public void generate()
    {
        put(
                SpecialWeapons.BINOCULAR,
                new Perspective(-44.0F, 0.0F, 3.0F, -9.0F, 3.625F, 2.05F, 1.0F, 1.0F, 1.0F),
                new Perspective(-28.0F, 0.0F, 5.0F, -0.1F, -0.35F, 0.225F, 0.75F, 1.0F, 0.825F),
                new Perspective(-31.0F, 1.0F, -14.0F, -0.025F, -0.35F, 0.25F, 0.75F, 1.0F, 0.7F),
                (p, o, l, r) -> p
                        .run(new Perspective(-73.0F, -8.0F, -1.0F, -8.775F, 1.4F, 2.875F, 1.0F, 1.0F, 1.0F))
                        .runLeft(new Perspective(-10.0F, 0.0F, 11.0F, -0.125F, -0.5F, 0.175F, 1.0F, 1.0F, 1.0F))
                        .runRight(new Perspective(-12.0F, 1.0F, -16.0F, 0.05F, -0.5F, 0.175F, 1.0F, 1.0F, 1.0F))
                        .draw(Animations.of()
                                .append(Animation.of(0, new Perspective(-56.0F, -2.0F, 5.0F, -9.0F, -0.375F, 2.225F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(6, o))
                                .append(Animation.of(13, new Perspective(-46.0F, -2.0F, 4.0F, -9.075F, 3.225F, 2.05F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(20, o)))
                        .drawLeft(Animations.of()
                                .append(Animation.of(0, new Perspective(-15.0F, 0.0F, 10.0F, 0.0F, -0.475F, 0.3F, 0.75F, 1.0F, 0.825F)))
                                .append(Animation.of(6, new Perspective(-28.0F, 0.0F, -6.0F, -0.225F, -0.275F, 0.2F, 0.75F, 1.0F, 0.825F)))
                                .append(Animation.of(13, new Perspective(-28.0F, 0.0F, 15.0F, 0.05F, -0.375F, 0.275F, 0.75F, 1.0F, 0.825F)))
                                .append(Animation.of(20, l)))
                        .drawRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-31.0F, 1.0F, -14.0F, -0.05F, -0.35F, 0.475F, 0.75F, 1.0F, 0.7F)))
                                .append(Animation.of(6, new Perspective(-31.0F, 1.0F, 1.0F, 0.175F, -0.3F, 0.225F, 0.75F, 1.0F, 0.7F)))
                                .append(Animation.of(13, new Perspective(-31.0F, 1.0F, -25.0F, -0.125F, -0.35F, 0.275F, 0.75F, 1.0F, 0.7F)))
                                .append(Animation.of(20, r)))
                        .aimingPos(new Perspective(0.0F, 0.0F, 0.0F, -8.95F, 8.225F, 7.275F, 1.0F, 1.0F, 1.0F))
                        .display2(ItemDisplayContext.THIRD_PERSON_LEFT_HAND, new Perspective(0, 0, 0, 0, 1, 0.5F, 1.0F, 1.0F, 1.0F))
                        .display(ItemDisplayContext.FIXED, new Perspective(-90, 0, 0, 0, 0, 0, 1.5F, 1.5F, 1.5F))
                        .display(ItemDisplayContext.GUI, new Perspective(-109, 28, -50, 0, 0, 0, 2.5F, 2.5F, 2.5F))
        );
    }
}
