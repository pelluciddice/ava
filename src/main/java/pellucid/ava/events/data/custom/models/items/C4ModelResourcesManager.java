package pellucid.ava.events.data.custom.models.items;

import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemDisplayContext;
import org.joml.Vector3f;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.QuadAnimator;
import pellucid.ava.client.renderers.models.c4.C4ModelVariables;
import pellucid.ava.client.renderers.models.c4.C4SubModels;
import pellucid.ava.client.renderers.models.c4.RegularC4ModelProperty;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.util.AVAConstants;

public class C4ModelResourcesManager extends ItemModelResourcesManager<C4Item, C4SubModels, RegularC4ModelProperty>
{
    public static final C4ModelResourcesManager INSTANCE = new C4ModelResourcesManager();

    public C4ModelResourcesManager()
    {
        super(AVAModelTypes.C4);
    }

    @Override
    public void generate()
    {
        put(
                MiscItems.C4,
                new Perspective(25.0F, 11.0F, 2.0F, -8.6F, 1.125F, -2.1F, 1.15F, 1.0F, 1.0F),
                new Perspective(-13.0F, -3.0F, -12.0F, -0.275F, -0.175F, 0.15F, 1.15F, 1.075F, 1.075F),
                Perspective.EMPTY,
                (p, o, l, r) -> p
                        .subModels(C4SubModels.SCREEN_UNSET, C4SubModels.SCREEN_SET_TIMER, C4SubModels.SCREEN_SET_15, C4SubModels.SCREEN_SET_30, C4SubModels.SCREEN_SET_40, C4SubModels.SCREEN_SET_OK, C4SubModels.LEVER)
                        .run(new Perspective(32.0F, -2.0F, -2.0F, -13.125F, -2.45F, -1.8F, 1.225F, 1.0F, 1.0F))
                        .runLeft(new Perspective(-21.0F, -3.0F, 3.0F, 0.225F, -0.2F, 0.45F, 1.15F, 1.075F, 1.075F))
                        .set(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(3, new Perspective(57.0F, 15.0F, 2.0F, -8.6F, 2.65F, -1.275F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(8, new Perspective(66.0F, 12.0F, 2.0F, -8.975F, 2.4F, -0.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(65.0F, 13.0F, 2.0F, -8.975F, 2.55F, -0.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(13, new Perspective(66.0F, 12.0F, 2.0F, -8.975F, 2.4F, -0.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(17, new Perspective(66.0F, 11.0F, 2.0F, -8.975F, 2.05F, -0.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(20, new Perspective(66.0F, 9.0F, 0.0F, -8.975F, 2.05F, -0.225F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(23, new Perspective(66.0F, 11.0F, 2.0F, -8.975F, 2.05F, -0.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(30, new Perspective(67.0F, 10.0F, 4.0F, -8.975F, 2.5F, -0.15F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(32, new Perspective(67.0F, 9.0F, 3.0F, -8.975F, 2.5F, -0.25F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(34, new Perspective(67.0F, 10.0F, 4.0F, -8.975F, 2.5F, -0.15F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(40, new Perspective(67.0F, 10.0F, 4.0F, -8.975F, 2.5F, -0.15F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(46, new Perspective(67.0F, 8.0F, 2.0F, -8.975F, 2.5F, -0.15F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(49, new Perspective(68.0F, 5.0F, -2.0F, -8.975F, 2.5F, -0.35F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(52, new Perspective(67.0F, 7.0F, 2.0F, -8.975F, 2.375F, -0.2F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(59, new Perspective(73.0F, 6.0F, 2.0F, -8.975F, 2.35F, -0.05F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(66, new Perspective(73.0F, 4.0F, 2.0F, -8.825F, 2.2F, -0.125F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(70, new Perspective(38.0F, 2.0F, 2.0F, -8.825F, -5.975F, -0.125F, 1.15F, 1.0F, 1.0F))))
                        .setLeft(Animations.of()
                                .append(Animation.of(0, l))
                                .append(Animation.of(3, new Perspective(-50.0F, -3.0F, -9.0F, -0.225F, 0.0F, 0.45F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(8, new Perspective(-58.0F, -3.0F, -10.0F, -0.2F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(9, new Perspective(-58.0F, -2.0F, -10.0F, -0.2F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(13, new Perspective(-58.0F, -3.0F, -10.0F, -0.2F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(17, new Perspective(-58.0F, -3.0F, -10.0F, -0.2F, 0.025F, 0.5F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(20, new Perspective(-58.0F, -2.0F, -9.0F, -0.2F, 0.025F, 0.5F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(23, new Perspective(-58.0F, -3.0F, -10.0F, -0.2F, 0.025F, 0.5F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(30, new Perspective(-57.0F, -3.0F, -9.0F, -0.175F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(32, new Perspective(-56.0F, -1.0F, -8.0F, -0.175F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(35, new Perspective(-57.0F, -3.0F, -9.0F, -0.175F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(40, new Perspective(-57.0F, -3.0F, -9.0F, -0.175F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(46, new Perspective(-58.0F, -3.0F, -8.0F, -0.175F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(49, new Perspective(-60.0F, -3.0F, -7.0F, -0.175F, 0.05F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(52, new Perspective(-58.0F, -4.0F, -8.0F, -0.175F, 0.025F, 0.475F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(59, new Perspective(-65.0F, -4.0F, -6.0F, -0.15F, 0.075F, 0.525F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(66, new Perspective(-66.0F, -4.0F, -5.0F, -0.15F, 0.1F, 0.55F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(70, new Perspective(-70.0F, -4.0F, -3.0F, -0.15F, 0.1F, 1.1F, 1.15F, 1.075F, 1.075F))))
                        .setRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-8.0F, -2.0F, 7.0F, -0.525F, -0.225F, 0.725F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(8, new Perspective(-49.0F, -2.0F, -28.0F, -0.4F, -0.225F, 0.25F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(9, new Perspective(-56.0F, -2.0F, -28.0F, -0.375F, -0.175F, 0.25F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(13, new Perspective(-56.0F, -2.0F, -25.0F, -0.4F, -0.225F, 0.225F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(17, new Perspective(-42.0F, -2.0F, -23.0F, -0.35F, -0.3F, 0.25F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(20, new Perspective(-40.0F, -2.0F, -23.0F, -0.35F, -0.3F, 0.25F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(23, new Perspective(-42.0F, -2.0F, -23.0F, -0.35F, -0.3F, 0.25F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(27, new Perspective(-47.0F, -2.0F, -23.0F, -0.35F, -0.325F, 0.275F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(30, new Perspective(-42.0F, -2.0F, -23.0F, -0.35F, -0.325F, 0.325F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(33, new Perspective(-46.0F, -2.0F, -19.0F, -0.375F, -0.375F, 0.325F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(35, new Perspective(-42.0F, -2.0F, -23.0F, -0.35F, -0.325F, 0.325F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(38, new Perspective(-43.0F, -16.0F, -4.0F, -0.125F, -0.325F, 0.3F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(40, new Perspective(-42.0F, -2.0F, -23.0F, -0.35F, -0.325F, 0.325F, 0.725F, 1.0F, 0.675F)))
                                .append(Animation.of(46, new Perspective(-41.0F, -4.0F, -16.0F, -0.15F, -0.35F, 0.35F, 0.8F, 1.0F, 0.725F)))
                                .append(Animation.of(49, new Perspective(-39.0F, -4.0F, -15.0F, -0.15F, -0.325F, 0.375F, 0.8F, 1.0F, 0.725F)))
                                .append(Animation.of(52, new Perspective(-44.0F, -4.0F, -2.0F, -0.05F, -0.35F, 0.375F, 0.8F, 1.0F, 0.725F)))
                                .append(Animation.of(59, new Perspective(-30.0F, -4.0F, -2.0F, -0.125F, -0.45F, 0.375F, 0.8F, 1.0F, 0.725F)))
                                .append(Animation.of(66, new Perspective(-30.0F, -4.0F, -2.0F, -0.125F, -0.45F, 0.375F, 0.8F, 1.0F, 0.725F)))
                                .append(Animation.of(70, new Perspective(-30.0F, -4.0F, -2.0F, -0.125F, -0.45F, 0.375F, 0.8F, 1.0F, 0.725F))))
                        .draw(Animations.of()
                                .append(Animation.of(0, new Perspective(4.0F, -3.0F, 2.0F, -7.525F, -6.5F, -2.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(4, new Perspective(24.0F, 1.0F, 2.0F, -7.55F, 0.15F, -2.1F, 1.15F, 1.0F, 1.0F)))
                                .append(Animation.of(7, o)))
                        .drawLeft(Animations.of()
                                .append(Animation.of(0, new Perspective(-3.0F, -3.0F, 6.0F, -0.2F, -0.175F, 0.5F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(4, new Perspective(-17.0F, -3.0F, 0.0F, -0.175F, -0.175F, 0.275F, 1.15F, 1.075F, 1.075F)))
                                .append(Animation.of(7, l)))
                        .quadAnim(C4SubModels.SCREEN_UNSET, new QuadAnimator.Animator.Vanisher(QuadAnimator.Motion.TO, C4ModelVariables.SET, 9, 20))
                        .quadAnim(C4SubModels.SCREEN_SET_TIMER, new QuadAnimator.Animator.Vanisher(QuadAnimator.Motion.TO, C4ModelVariables.SET, 19, 30))
                        .quadAnim(C4SubModels.SCREEN_SET_15, new QuadAnimator.Animator.Vanisher(QuadAnimator.Motion.TO, C4ModelVariables.SET, 29, 35))
                        .quadAnim(C4SubModels.SCREEN_SET_30, new QuadAnimator.Animator.Vanisher(QuadAnimator.Motion.TO, C4ModelVariables.SET, 34, 40))
                        .quadAnim(C4SubModels.SCREEN_SET_40, new QuadAnimator.Animator.Vanisher(QuadAnimator.Motion.TO, C4ModelVariables.SET, 39, 45))
                        .quadAnim(C4SubModels.SCREEN_SET_OK, new QuadAnimator.Animator.Vanisher(QuadAnimator.Motion.TO, C4ModelVariables.SET, 44, AVAConstants.C4_SET_TIME))
                        .quadAnim(C4SubModels.LEVER, new QuadAnimator.Animator.Rotator(QuadAnimator.Motion.TO, C4ModelVariables.SET, -55.0F, 8, 10, Direction.Axis.X, new Vector3f(12.2625F, 10.7625F, 6.0625F)))
                        .quadAnim(C4SubModels.LEVER, new QuadAnimator.Animator.Rotator(QuadAnimator.Motion.CONSTANT, C4ModelVariables.SET, -55.0F, 10, AVAConstants.C4_SET_TIME, Direction.Axis.X, new Vector3f(12.2625F, 10.7625F, 6.0625F)))
                        .display2(ItemDisplayContext.THIRD_PERSON_LEFT_HAND, new Perspective(90, 0, 0, -6.25F, 0, 0, 0.75F, 0.75F, 0.75F))
                        .display(ItemDisplayContext.GROUND, new Perspective(0, 0, 0, 0, -1.25F, 0, 0.75F, 0.75F, 0.75F))
                        .display(ItemDisplayContext.FIXED, new Perspective(-90, -180, 0, 0, -1, -2.5F, 1, 1, 1))
                        .display(ItemDisplayContext.GUI, new Perspective(63, 0, 0, 0, 0, 0, 0.75F, 0.75F, 0.75F))

        );
    }
}
