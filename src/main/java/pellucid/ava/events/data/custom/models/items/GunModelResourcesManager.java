package pellucid.ava.events.data.custom.models.items;

import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.guns.GunSubModels;
import pellucid.ava.client.renderers.models.guns.RegularGunModelProperty;
import pellucid.ava.client.renderers.models.init.PistolModels;
import pellucid.ava.client.renderers.models.init.RiflesModels;
import pellucid.ava.client.renderers.models.init.SniperModels;
import pellucid.ava.client.renderers.models.init.SpecialWeaponModels;
import pellucid.ava.client.renderers.models.init.SubMachinegunModels;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.items.guns.AVAItemGun;

import java.util.function.Function;

public class GunModelResourcesManager extends ItemModelResourcesManager<AVAItemGun, GunSubModels, RegularGunModelProperty>
{
    public static final GunModelResourcesManager INSTANCE = new GunModelResourcesManager();

    public GunModelResourcesManager()
    {
        super(AVAModelTypes.GUNS);
    }

    @Override
    public void generate()
    {
        SubMachinegunModels.put(this);
        RiflesModels.put(this);
        PistolModels.put(this);
        SniperModels.put(this);
        SpecialWeaponModels.put(this);
    }

    public void putSkinned(DeferredItem<Item> gun, GunSubModels... models)
    {
        putSkinned(gun, (p) -> p.subModels(models));
    }

    public void putSkinned(DeferredItem<Item> gun, Function<RegularGunModelProperty, RegularGunModelProperty> property)
    {
        RegularGunModelProperty p = modelProperties.get(((AVAItemGun) gun.get()).getMaster()).copyFor((AVAItemGun) gun.get());
        modelProperties.put((AVAItemGun) gun.get(), property.apply(p));
    }
}
