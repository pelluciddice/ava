package pellucid.ava.events.data.custom.models.items;

import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.melee.MeleeSubModels;
import pellucid.ava.client.renderers.models.melee.RegularMeleeModelProperty;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.items.init.MeleeWeapons;
import pellucid.ava.items.miscs.AVAMeleeItem;

import static pellucid.ava.client.renderers.Perspective.EMPTY;
import static pellucid.ava.items.init.MeleeWeapons.FIELD_KNIFE;

public class MeleeModelResourcesManager extends ItemModelResourcesManager<AVAMeleeItem, MeleeSubModels, RegularMeleeModelProperty>
{
    public static final MeleeModelResourcesManager INSTANCE = new MeleeModelResourcesManager();

    public MeleeModelResourcesManager()
    {
        super(AVAModelTypes.MELEES);
    }

    @Override
    public void generate()
    {
        put(
                FIELD_KNIFE,
                new Perspective(-4.0F, 18.0F, 22.0F, -0.075F, 4.425F, 0.4F, 1.0F, 1.0F, 1.0F),
                EMPTY,
                new Perspective(-2.0F, -12.0F, -47.0F, -0.8F, -0.35F, -0.05F, 1.125F, 1.0F, 0.7F),
                (p, o, l, r) -> p
                        .run(new Perspective(-4.0F, -24.0F, 62.0F, 2.25F, 2.15F, 0.4F, 1.0F, 1.0F, 1.0F))
                        .runRight(new Perspective(-2.0F, -49.0F, -19.0F, -0.725F, -0.375F, 0.25F, 1.125F, 1.0F, 0.7F))
                        .draw(Animations.of()
                                .append(Animation.of(0, new Perspective(12.0F, 79.0F, 104.0F, -0.475F, 0.775F, 6.725F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(12.0F, 79.0F, 104.0F, 0.65F, 4.725F, 2.6F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, o)))
                        .drawRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-96.0F, -38.0F, -82.0F, -1.025F, -0.15F, 0.55F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(2, new Perspective(-111.333F, -25.0F, -95.0F, -1.25F, 0.158F, 0.292F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(3, new Perspective(-111.333F, -22.0F, -95.0F, -1.275F, 0.308F, 0.192F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(4, new Perspective(-101.333F, -22.5F, -86.0F, -1.2F, 0.283F, 0.292F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(5, new Perspective(-91.333F, -21.0F, -77.0F, -1.125F, 0.258F, 0.392F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(6, new Perspective(-69.167F, -23.0F, -65.25F, -1.013F, 0.048F, 0.427F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(7, new Perspective(-47.0F, -25.0F, -53.5F, -0.9F, -0.162F, 0.363F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(8, new Perspective(-33.75F, -21.75F, -51.875F, -0.875F, -0.241F, 0.279F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(9, new Perspective(-24.5F, -18.5F, -50.25F, -0.85F, -0.319F, 0.194F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(11, new Perspective(-2.0F, -12.0F, -47.0F, -0.8F, -0.35F, -0.05F, 1.125F, 1.0F, 0.7F))))
                        .attackLight(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(1, new Perspective(89.0F, 68.0F, -40.0F, -16.475F, 7.275F, 2.0F, 1.0F, 0.7F, 1.0F)))
                                .append(Animation.of(2, new Perspective(131.0F, 68.0F, -51.0F, -16.025F, 7.125F, 3.725F, 1.0F, 0.7F, 1.0F)))
                                .append(Animation.of(3, new Perspective(15.0F, 14.0F, 78.0F, -8.5F, 7.95F, 0.325F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(4, new Perspective(5.0F, -17.0F, 61.0F, 13.15F, 4.125F, 0.4F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(6, new Perspective(-1.0F, -17.0F, 17.333F, 8.35F, -2.325F, 0.567F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(7, new Perspective(-4.0F, -5.0F, 39.0F, 5.95F, -5.55F, 0.65F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, o)))
                        .attackLightRight(Animations.of()
                                .append(Animation.of(0, r))
                                .append(Animation.of(1, new Perspective(-66.0F, -4.0F, -74.0F, -0.55F, 0.075F, 0.375F, 1.125F, 1.725F, 0.7F)))
                                .append(Animation.of(2, new Perspective(-79.0F, -4.0F, -77.0F, -0.625F, 0.15F, 0.35F, 1.125F, 1.725F, 0.7F)))
                                .append(Animation.of(3, new Perspective(-114.0F, -49.0F, -87.0F, -0.65F, 0.225F, 0.225F, 0.975F, 1.1F, 0.6F)))
                                .append(Animation.of(4, new Perspective(-33.0F, -71.0F, -7.0F, -0.825F, -0.35F, 0.225F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(7, new Perspective(-9.0F, -12.0F, -20.0F, -0.95F, -0.35F, 0.875F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(11, r)))
                        .attackHeavy(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(3, new Perspective(6.0F, -47.0F, 70.0F, -1.6F, 4.7F, 6.825F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-6.0F, -86.0F, 73.0F, 3.925F, 3.35F, 3.7F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(6, new Perspective(-10.667F, -60.667F, 64.5F, 6.008F, 3.104F, 3.15F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(7, new Perspective(12.667F, -44.933F, 84.0F, -3.733F, 6.693F, -1.425F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(10, new Perspective(12.667F, -34.933F, 84.0F, -4.258F, 6.443F, -1.2F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(13, new Perspective(1.0F, 5.0F, -5.0F, 3.95F, 5.5F, -0.275F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(20, o)))
                        .attackHeavyLeft(Animations.of()
                                .append(Animation.of(0, new Perspective(-11.0F, 0.0F, 6.0F, 0.05F, -0.525F, 0.35F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-48.0F, 0.0F, 28.0F, -0.05F, -0.1F, 0.225F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-30.0F, 0.0F, 4.0F, 0.125F, -0.35F, 0.275F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(6, new Perspective(-11.0F, 0.0F, 6.0F, 0.05F, -0.525F, 0.35F, 1.0F, 1.0F, 1.0F))))
                        .attackHeavyRight(Animations.of()
                                .append(Animation.of(0, r))
                                .append(Animation.of(3, new Perspective(41.0F, -71.0F, 52.0F, -0.55F, -0.4F, 0.4F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(5, new Perspective(41.0F, -71.0F, 52.0F, -0.55F, -0.4F, 0.4F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(6, new Perspective(33.833F, -61.167F, 35.5F, -0.917F, -0.392F, 0.7F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(7, new Perspective(44.0F, -67.0F, 48.0F, -0.175F, -0.125F, 0.1F, 0.875F, 1.0F, 0.55F)))
                                .append(Animation.of(10, new Perspective(42.0F, -68.0F, 46.0F, -0.15F, -0.125F, 0.125F, 0.875F, 1.0F, 0.55F)))
                                .append(Animation.of(11, new Perspective(3.0F, -45.667F, 11.0F, -0.208F, -0.142F, 0.142F, 0.958F, 1.0F, 0.6F)))
                                .append(Animation.of(12, new Perspective(-2.0F, -23.333F, -12.0F, -0.467F, -0.258F, 0.008F, 1.042F, 1.0F, 0.65F)))
                                .append(Animation.of(13, new Perspective(-3.0F, -1.0F, -17.0F, -0.625F, -0.325F, -0.05F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(16, new Perspective(0.429F, -5.714F, -24.857F, -0.6F, -0.361F, -0.05F, 1.125F, 1.0F, 0.7F)))
                                .append(Animation.of(20, r)))
                        .display2(ItemDisplayContext.THIRD_PERSON_LEFT_HAND, Perspective.translation(0, 1, 2.5F))
                        .display(ItemDisplayContext.GROUND, Perspective.rotation(0, 0, 90))
                        .display(ItemDisplayContext.FIXED, new Perspective(93, 47, -93, -0.25F, -0.75F, -0.5F, 1, 1, 1))
                        .display(ItemDisplayContext.GUI, new Perspective(79, 47, -93, -0.75F, -0.75F, 0, 1, 1, 1))
                );

        put(
                MeleeWeapons.SCYTHE_IGNIS,
                new Perspective(6.0F, 14.0F, -7.0F, -1.4F, -0.325F, 2.6F, 1.0F, 1.0F, 1.0F),
                EMPTY,
                new Perspective(-1.0F, 14.0F, -7.0F, -0.2F, -0.5F, -0.05F, 1.0F, 1.0F, 1.0F),
                (p, o, l, r) -> p
                        .run(new Perspective(20.0F, 55.0F, -12.0F, -2.775F, 0.325F, 3.25F, 1.0F, 1.0F, 1.0F))
                        .attackLight(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(1, new Perspective(25.0F, -6.0F, -32.0F, -2.725F, 6.175F, 2.85F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(2, new Perspective(33.0F, -1.0F, -33.0F, -4.825F, 4.925F, 1.825F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-1.0F, 48.0F, -31.0F, -12.225F, -0.225F, 5.325F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(4, new Perspective(-149.0F, 43.0F, 86.0F, -12.425F, -1.375F, 6.0F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-166.0F, 19.0F, 78.0F, -12.425F, -1.375F, 6.0F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(7, new Perspective(-54.0F, 82.0F, -31.0F, -9.1F, -9.725F, 8.225F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(-45.0F, 0.0F, -7.0F, 0.425F, -4.625F, 8.225F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, o)))
                        .attackLightLeft(Animations.of()
                                .append(Animation.of(0, new Perspective(-9.0F, 20.0F, 59.0F, 0.25F, -0.275F, 0.3F, 0.8F, 1.1F, 0.7F)))
                                .append(Animation.of(1, new Perspective(-39.0F, 12.0F, 72.0F, 0.425F, -0.05F, 0.3F, 0.8F, 1.1F, 0.7F)))
                                .append(Animation.of(2, new Perspective(-38.0F, 12.0F, 70.0F, 0.525F, -0.025F, 0.325F, 0.8F, 1.1F, 0.7F)))
                                .append(Animation.of(3, new Perspective(-13.0F, 7.0F, 10.0F, -0.05F, -0.45F, 0.35F, 1.0F, 1.0F, 1.0F))))
                        .attackLightRight(Animations.of()
                                .append(Animation.of(0, r))
                                .append(Animation.of(1, new Perspective(-50.0F, 27.0F, 16.0F, -0.1F, -0.325F, -0.075F, 0.625F, 1.025F, 0.55F)))
                                .append(Animation.of(2, new Perspective(-38.0F, 27.0F, -2.0F, -0.125F, -0.35F, 0.075F, 0.625F, 1.025F, 0.55F)))
                                .append(Animation.of(3, new Perspective(54.0F, 24.0F, -75.0F, -0.45F, -0.325F, -0.275F, 0.625F, 1.025F, 0.55F)))
                                .append(Animation.of(4, new Perspective(78.0F, 24.0F, -92.0F, -0.35F, -0.325F, -0.225F, 0.625F, 1.025F, 0.55F)))
                                .append(Animation.of(7, new Perspective(20.0F, 14.0F, -1.0F, -0.2F, -0.575F, 0.025F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(20.0F, 14.0F, -1.0F, -0.2F, -0.575F, 0.025F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, r)))
                        .attackHeavy(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(2, new Perspective(-26.0F, -27.0F, -25.0F, -0.3F, -1.45F, 2.875F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-32.222F, -40.722F, -24.0F, 4.739F, -5.813F, 2.86F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(7, new Perspective(25.0F, -11.0F, -76.0F, -1.75F, 1.0F, 3.2F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(-4.923F, 72.846F, -75.385F, -10.796F, 4.221F, 8.558F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(10, new Perspective(26.077F, 176.846F, -102.385F, -8.496F, 4.771F, 7.783F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(-77.962F, 61.923F, -54.692F, -6.173F, 0.873F, 8.716F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(12, new Perspective(-110.0F, 23.0F, 39.0F, -3.85F, -3.025F, 9.65F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(16, new Perspective(-49.0F, 42.0F, 9.0F, 0.475F, -3.025F, 9.65F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(20, o)))
                        .attackHeavyRight(Animations.of()
                                .append(Animation.of(0, r))
                                .append(Animation.of(2, new Perspective(1.0F, 11.0F, 3.0F, -0.25F, -0.4F, 0.0F, 0.85F, 1.0F, 0.95F)))
                                .append(Animation.of(3, new Perspective(2.889F, 11.167F, 9.444F, -0.572F, -0.406F, 0.047F, 0.858F, 1.0F, 0.953F)))
                                .append(Animation.of(7, new Perspective(-1.0F, 30.0F, 17.0F, -0.125F, -0.5F, -0.05F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(63.0F, 51.0F, -68.0F, -0.225F, -0.225F, -0.45F, 0.525F, 0.75F, 0.45F)))
                                .append(Animation.of(10, new Perspective(63.0F, 6.0F, -73.0F, -0.225F, -0.225F, -0.4F, 0.525F, 0.75F, 0.45F)))
                                .append(Animation.of(12, new Perspective(63.0F, 6.0F, -73.0F, -0.225F, -0.225F, -0.4F, 0.525F, 0.75F, 0.45F)))
                                .append(Animation.of(19, new Perspective(12.0F, 13.0F, -18.25F, -0.378F, -0.591F, -0.169F, 0.941F, 0.969F, 0.931F)))
                                .append(Animation.of(20, r)))
                        .draw(Animations.of()
                                .append(Animation.of(0, new Perspective(-80.0F, -1.0F, 92.0F, -2.3F, 0.125F, 2.6F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-64.545F, 41.091F, 63.0F, -1.155F, 1.802F, 3.575F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(3.0F, 57.0F, -6.0F, 0.125F, 3.55F, 3.075F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, o)))
                        .drawRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-1.0F, 14.0F, -15.0F, 0.65F, -0.5F, 0.375F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(22.0F, -1.0F, -45.818F, -0.482F, -0.35F, -0.316F, 0.875F, 0.725F, 0.625F)))
                                .append(Animation.of(5, new Perspective(3.0F, 2.0F, -2.0F, -0.25F, -0.475F, -0.275F, 0.875F, 1.0F, 0.65F)))
                                .append(Animation.of(11, r)))
                        .display2(ItemDisplayContext.THIRD_PERSON_LEFT_HAND, Perspective.translation(0, 0, 1))
                        .display(ItemDisplayContext.GROUND, Perspective.rotation(0, 0, 90))
                        .display(ItemDisplayContext.FIXED, Perspective.rotation(-70, 50, 75))
                        .display(ItemDisplayContext.GUI, new Perspective(70, 50, -75, 0, -1, 0, 0.45F, 0.45F, 0.45F))

        );
    }
}
