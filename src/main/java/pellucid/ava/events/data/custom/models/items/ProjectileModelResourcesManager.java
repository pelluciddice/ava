package pellucid.ava.events.data.custom.models.items;

import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.projectile.ProjectileModelVariables;
import pellucid.ava.client.renderers.models.projectile.ProjectileSubModels;
import pellucid.ava.client.renderers.models.projectile.RegularProjectileModelProperty;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.throwables.ThrowableItem;

import static pellucid.ava.client.renderers.models.projectile.ProjectileModelVariables.*;
import static pellucid.ava.client.renderers.models.projectile.ProjectileSubModels.*;
import static pellucid.ava.items.init.Projectiles.*;

public class ProjectileModelResourcesManager extends ItemModelResourcesManager<ThrowableItem, ProjectileSubModels, RegularProjectileModelProperty>
{
    public static final ProjectileModelResourcesManager INSTANCE = new ProjectileModelResourcesManager();

    public ProjectileModelResourcesManager()
    {
        super(AVAModelTypes.PROJECTILE);
    }

    @Override
    public void generate()
    {
        put(
                M18_GREY,
                new Perspective(30.0F, 39.0F, -5.0F, -3.85F, 2.2F, -2.825F, 1.0F, 1.0F, 1.0F),
                Perspective.EMPTY,
                new Perspective(-32.0F, 0.0F, -31.0F, -0.375F, -0.325F, 0.525F, 1.4F, 1.375F, 0.975F),
                (p, o, l, r) -> p.subModels(PIN)
                        .run(new Perspective(-6.0F, 31.0F, 51.0F, 4.425F, 0.075F, -1.7F, 1.0F, 1.0F, 1.0F))
                        .runRight(new Perspective(-35.0F, -39.0F, -36.0F, -0.9F, -0.325F, 0.6F, 1.4F, 1.3F, 0.975F))
                        .draw(Animations.of()
                                .append(Animation.of(0, new Perspective(6.0F, 78.0F, 56.0F, -8.2F, -3.25F, 0.125F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(6, new Perspective(24.0F, 69.0F, 5.0F, -4.675F, 1.525F, -2.65F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, o)))
                        .drawRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-18.0F, -48.0F, -80.0F, -0.65F, -0.325F, 0.25F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(6, new Perspective(-15.0F, -6.0F, -54.0F, -0.75F, -0.375F, 0.225F, 1.4F, 1.425F, 0.975F)))
                                .append(Animation.of(9, r)))
                        .pinOff(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(1, new Perspective(18.0F, 38.0F, -3.0F, -4.3F, 1.8F, -2.375F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(5.0F, 26.0F, 11.0F, -6.725F, 0.975F, -2.075F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(5.0F, 22.0F, 15.0F, -7.675F, 0.425F, -2.075F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(5.0F, -43.0F, -10.0F, 11.825F, 2.225F, -2.85F, 0.55F, 0.725F, 0.675F))))
                        .pinOffLeft(Animations.of()
                                .append(Animation.of(0, new Perspective(-19.0F, 0.0F, 1.0F, -0.025F, -0.325F, 0.55F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(1, new Perspective(-29.0F, 0.0F, 28.0F, 0.075F, -0.325F, 0.2F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-23.0F, 0.0F, 48.0F, 0.225F, -0.225F, 0.225F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(-16.0F, 0.0F, 40.0F, 0.175F, -0.3F, 0.175F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(-25.0F, 0.0F, 27.0F, 0.575F, -0.5F, 0.45F, 1.0F, 1.0F, 1.0F))))
                        .pinOffRight(Animations.of()
                                .append(Animation.of(0, r))
                                .append(Animation.of(1, new Perspective(-17.0F, 0.0F, -37.0F, -0.4F, -0.375F, 0.275F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(5, new Perspective(-11.4F, 0.0F, -35.0F, -0.25F, -0.475F, 0.225F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(9, new Perspective(-8.4F, -12.0F, -35.0F, -0.175F, -0.475F, 0.2F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(11, new Perspective(2.0F, -31.0F, 48.0F, -0.225F, -0.075F, 0.4F, 0.9F, 1.3F, 1.1F))))
                        .quadAnim(PIN, PIN_OFF, Animations.of()
                                .append(Animation.of(0, new Perspective(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(10, new Perspective(0.0F, 21.0F, 0.0F, -23.75F, -1.975F, 3.025F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(0.0F, 38.0F, 0.0F, -44.975F, -6.35F, 13.025F, 1.0F, 1.0F, 1.0F))))
                        .tossRight(Animations.of()
                                .append(Animation.of(0, new Perspective(16.0F, -21.0F, 4.0F, 0.025F, -0.3F, 0.275F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(1, new Perspective(16.0F, -21.0F, 4.0F, -0.025F, -0.3F, 0.05F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-55.0F, -26.0F, -8.0F, -0.35F, 0.3F, 0.25F, 1.0F, 1.1F, 1.0F)))
                                .append(Animation.of(9, new Perspective(-76.0F, -19.0F, 2.0F, -0.725F, 0.275F, 0.05F, 1.0F, 1.375F, 1.0F)))
                                .append(Animation.of(11, new Perspective(-76.0F, -19.0F, 2.0F, -0.725F, 0.275F, 0.05F, 1.0F, 1.375F, 1.0F))))
                        .thr0wRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-80.0F, -19.0F, -13.0F, -0.725F, 0.05F, 0.25F, 1.0F, 1.675F, 1.0F)))
                                .append(Animation.of(1, new Perspective(-80.0F, -19.0F, -13.0F, -0.725F, 0.375F, 0.25F, 1.0F, 1.675F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-41.0F, -12.0F, -2.0F, -0.45F, 0.15F, 0.175F, 1.0F, 1.475F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-1.0F, -7.0F, -26.0F, -0.4F, -0.075F, 0.0F, 1.025F, 0.85F, 0.9F)))
                                .append(Animation.of(9, new Perspective(20.0F, -7.0F, -39.0F, -0.375F, -0.15F, 0.15F, 1.025F, 0.75F, 0.9F)))
                                .append(Animation.of(11, new Perspective(20.0F, -7.0F, -39.0F, -0.375F, -0.15F, 0.15F, 1.025F, 0.75F, 0.9F))))
                        .display(ItemDisplayContext.FIXED, new Perspective(0, 90, 0, 0, -1, -1.25F, 1.25F, 1.25F, 1.25F))
                        .display(ItemDisplayContext.GUI, new Perspective(38, -66, 0, 0, 0, 0, 1.25F, 1.25F, 1.25F))
        );
        copyFrom(M116A1, M18_GREY, PIN);
        copyFrom(M18_GREY_II, M18_GREY, PIN);
        copyFrom(M18_GREY_III, M18_GREY, PIN);
        copyFrom(M18_RED, M18_GREY, PIN);
        copyFrom(M18_RED_II, M18_GREY, PIN);
        copyFrom(M18_RED_III, M18_GREY, PIN);
        copyFrom(M18_PURPLE, M18_GREY, PIN);
        copyFrom(M18_TOXIC, M18_GREY, PIN);
        copyFrom(M18_YELLOW, M18_GREY, PIN);
        copyFrom(M18_BLUE, M18_GREY, PIN);
        copyFrom(MK3A2, M18_GREY, PIN);
        put(
                M67,
                new Perspective(30.0F, 39.0F, -5.0F, -3.85F, 2.2F, -2.825F, 1.0F, 1.0F, 1.0F),
                Perspective.EMPTY,
                new Perspective(-27.0F, 0.0F, -30.0F, -0.375F, -0.325F, 0.525F, 1.4F, 1.375F, 0.975F),
                (p, o, l, r) -> p.subModels(PIN)
                        .run(new Perspective(-6.0F, 31.0F, 51.0F, 3.2F, 1.125F, -1.7F, 1.0F, 1.0F, 1.0F))
                        .runRight(new Perspective(-35.0F, -39.0F, -36.0F, -0.9F, -0.325F, 0.6F, 1.4F, 1.3F, 0.975F))
                        .draw(Animations.of()
                                .append(Animation.of(0, new Perspective(6.0F, 78.0F, 56.0F, -8.2F, -3.25F, 0.125F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(6, new Perspective(24.0F, 83.0F, 3.0F, -5.1F, 2.25F, -1.85F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, o)))
                        .drawRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-18.0F, -48.0F, -80.0F, -0.65F, -0.325F, 0.25F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(6, new Perspective(-15.0F, -6.0F, -54.0F, -0.75F, -0.375F, 0.225F, 1.4F, 1.425F, 0.975F)))
                                .append(Animation.of(9, r)))
                        .pinOff(Animations.of()
                                .append(Animation.of(0, o))
                                .append(Animation.of(1, new Perspective(18.0F, 38.0F, -3.0F, -4.975F, 2.85F, -2.1F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(9.0F, 26.0F, 11.0F, -6.725F, 2.15F, -1.55F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(5.0F, 22.0F, 20.0F, -8.05F, 1.625F, -1.75F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(5.0F, -43.0F, -10.0F, 10.625F, 2.775F, -2.775F, 0.55F, 0.725F, 0.675F))))
                        .pinOffLeft(Animations.of()
                                .append(Animation.of(0, new Perspective(-19.0F, 0.0F, 1.0F, -0.025F, -0.325F, 0.55F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(1, new Perspective(-29.0F, 0.0F, 28.0F, 0.075F, -0.325F, 0.2F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-23.0F, 0.0F, 48.0F, 0.225F, -0.225F, 0.225F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(-16.0F, 0.0F, 40.0F, 0.175F, -0.3F, 0.175F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(-25.0F, 0.0F, 27.0F, 0.575F, -0.5F, 0.45F, 1.0F, 1.0F, 1.0F))))
                        .pinOffRight(Animations.of()
                                .append(Animation.of(0, r))
                                .append(Animation.of(1, new Perspective(-17.0F, 0.0F, -37.0F, -0.4F, -0.375F, 0.275F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(5, new Perspective(-11.4F, 0.0F, -35.0F, -0.25F, -0.475F, 0.225F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(9, new Perspective(-8.4F, -12.0F, -35.0F, -0.175F, -0.475F, 0.2F, 1.4F, 1.3F, 0.975F)))
                                .append(Animation.of(11, new Perspective(2.0F, -31.0F, 48.0F, -0.225F, -0.075F, 0.4F, 0.9F, 1.3F, 1.1F))))
                        .quadAnim(PIN, PIN_OFF, Animations.of()
                                .append(Animation.of(0, new Perspective(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(9, new Perspective(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(10, new Perspective(0.0F, 21.0F, 0.0F, -23.75F, -1.975F, 3.025F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(11, new Perspective(0.0F, 38.0F, 0.0F, -44.975F, -6.35F, 13.025F, 1.0F, 1.0F, 1.0F))))
                        .tossRight(Animations.of()
                                .append(Animation.of(0, new Perspective(16.0F, -21.0F, 4.0F, 0.025F, -0.3F, 0.275F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(1, new Perspective(16.0F, -21.0F, 4.0F, -0.025F, -0.3F, 0.05F, 1.0F, 1.0F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-55.0F, -26.0F, -8.0F, -0.35F, 0.3F, 0.25F, 1.0F, 1.1F, 1.0F)))
                                .append(Animation.of(9, new Perspective(-76.0F, -19.0F, 2.0F, -0.725F, 0.275F, 0.05F, 1.0F, 1.375F, 1.0F)))
                                .append(Animation.of(11, new Perspective(-76.0F, -19.0F, 2.0F, -0.725F, 0.275F, 0.05F, 1.0F, 1.375F, 1.0F))))
                        .thr0wRight(Animations.of()
                                .append(Animation.of(0, new Perspective(-80.0F, -19.0F, -13.0F, -0.725F, 0.05F, 0.25F, 1.0F, 1.675F, 1.0F)))
                                .append(Animation.of(1, new Perspective(-80.0F, -19.0F, -13.0F, -0.725F, 0.375F, 0.25F, 1.0F, 1.675F, 1.0F)))
                                .append(Animation.of(3, new Perspective(-41.0F, -12.0F, -2.0F, -0.45F, 0.15F, 0.175F, 1.0F, 1.475F, 1.0F)))
                                .append(Animation.of(5, new Perspective(-1.0F, -7.0F, -26.0F, -0.4F, -0.075F, 0.0F, 1.025F, 0.85F, 0.9F)))
                                .append(Animation.of(9, new Perspective(20.0F, -7.0F, -39.0F, -0.375F, -0.15F, 0.15F, 1.025F, 0.75F, 0.9F)))
                                .append(Animation.of(11, new Perspective(20.0F, -7.0F, -39.0F, -0.375F, -0.15F, 0.15F, 1.025F, 0.75F, 0.9F))))
                        .display(ItemDisplayContext.FIXED, new Perspective(0, 90, 0, 0, -1, -1.25F, 1.25F, 1.25F, 1.25F))
                        .display(ItemDisplayContext.GUI, new Perspective(38, -66, 0, 0, -0.75F, 0, 1.75F, 1.75F, 1.75F))
        );
        copyFrom(M67_SPORTS, M67, PIN);
    }
}
