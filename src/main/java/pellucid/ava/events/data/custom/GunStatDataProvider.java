package pellucid.ava.events.data.custom;

import com.google.gson.JsonObject;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.world.item.Item;
import pellucid.ava.events.data.BaseDataProvider;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAWeaponUtil;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class GunStatDataProvider extends BaseDataProvider
{
    public static final boolean FORCE_OVERRIDE = true;
    public GunStatDataProvider(PackOutput generator)
    {
        super(generator);
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cache)
    {
        List<CompletableFuture<?>> list = new ArrayList<>();
        Path path = dataGenerator.getOutputFolder();
        for (Item item : AVAWeaponUtil.getAllGuns())
        {
            Path outputFile = path.resolve("data/ava/stats/guns/" + BuiltInRegistries.ITEM.getKey(item).getPath() + ".json");
            AVAItemGun gun = (AVAItemGun) item;
            list.add(DataProvider.saveStable(cache, AVAItemGun.Serializer.serialize(gun), outputFile));
        }
        Path outputFile = path.resolve("data/ava/stats/guns/global.json");
        JsonObject obj = new JsonObject();
        obj.addProperty("weapon", "global");
        list.add(DataProvider.saveStable(cache, obj, outputFile));
        return CompletableFuture.allOf(list.toArray(CompletableFuture[]::new));
    }

    @Override
    public String getName()
    {
        return "Gun Stats";
    }
}
