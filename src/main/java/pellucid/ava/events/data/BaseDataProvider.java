package pellucid.ava.events.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BaseDataProvider implements DataProvider
{
    protected static final Logger LOGGER = LogManager.getLogger();
    protected static final Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    protected final PackOutput dataGenerator;

    public BaseDataProvider(PackOutput generator)
    {
        this.dataGenerator = generator;
    }
}
