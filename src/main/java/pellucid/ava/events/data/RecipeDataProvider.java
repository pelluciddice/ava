package pellucid.ava.events.data;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.events.data.tags.AVAItemTagsProvider;
import pellucid.ava.items.init.Materials;

import java.util.concurrent.CompletableFuture;

public class RecipeDataProvider extends RecipeProvider
{
    public RecipeDataProvider(PackOutput generatorIn, CompletableFuture<HolderLookup.Provider> lookup)
    {
        super(generatorIn, lookup);
    }

    private RecipeOutput consumer;

    @Override
    protected void buildRecipes(RecipeOutput consumer)
    {
        this.consumer = consumer;
        ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, AVABlocks.GUN_COLOURING_TABLE.get())
                .define('I', Tags.Items.INGOTS_IRON)
                .define('B', Items.BONE_BLOCK)
                .define('W', Blocks.WHITE_WOOL)
                .pattern("III")
                .pattern("BWB")
                .pattern("III")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "gun_colouring_table"));

        ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, AVABlocks.GUN_CRAFTING_TABLE.get())
                .define('I', Tags.Items.INGOTS_IRON)
                .define('G', Tags.Items.INGOTS_GOLD)
                .define('O', Tags.Items.OBSIDIANS)
                .pattern("III")
                .pattern("OGO")
                .pattern("III")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "gun_crafting_table"));

        ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, AVABlocks.BUILDERS_TABLE.get())
                .define('S', Tags.Items.STONES)
                .define('W', Items.WATER_BUCKET)
                .define('I', Items.ICE)
                .define('L', ItemTags.LEAVES)
                .pattern("SLS")
                .pattern("IWI")
                .pattern("SSS")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "builders_table"));

        ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, AVABlocks.MASTERY_TABLE.get())
                .define('O', Tags.Items.OBSIDIANS)
                .define('C', Blocks.CRYING_OBSIDIAN)
                .define('E', Items.END_CRYSTAL)
                .pattern("OCO")
                .pattern("CEC")
                .pattern("OCO")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "mastery_table"));

        ShapedRecipeBuilder.shaped(RecipeCategory.DECORATIONS, AVABlocks.GUN_MODIFYING_TABLE.get())
                .define('A', Items.ANVIL)
                .define('I', Tags.Items.INGOTS_IRON)
                .define('O', Tags.Items.OBSIDIANS)
                .pattern("III")
                .pattern("OAO")
                .pattern("III")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "gun_modifying_table"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.WORK_HARDENED_IRON.get(), 4)
                .define('C', Tags.Items.INGOTS_COPPER)
                .define('I', Tags.Items.INGOTS_IRON)
                .pattern("CIC")
                .pattern("ICI")
                .pattern("CIC")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "work_hardened_iron"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.LENS.get(), 2)
                .define('T', Tags.Items.GLASS_BLOCKS_TINTED)
                .define('G', Blocks.LIGHT_BLUE_STAINED_GLASS)
                .define('F', AVAItemTagsProvider.FIBRE)
                .define('I', AVAItemTagsProvider.WORK_HARDENED_IRON)
                .pattern("FTF")
                .pattern("TGT")
                .pattern("FIF")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "lens"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.SPRING.get(), 4)
                .define('I', AVAItemTagsProvider.WORK_HARDENED_IRON)
                .pattern("I")
                .pattern("I")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "spring"));

        simpleCookingRecipe(consumer, new ResourceLocation(AVA.MODID, "plastic").toString(), RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new, 300, Items.LEATHER, Materials.PLASTIC.get(), 0.55F);

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.PACKED_GUNPOWDER.get(), 2)
                .define('G', Tags.Items.GUNPOWDERS)
                .pattern("GG")
                .pattern("GG")
                .pattern("GG")
                .unlockedBy("has_gunpowder", has(Tags.Items.GUNPOWDERS))
                .save(consumer, new ResourceLocation(AVA.MODID, "packed_gunpowder"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.SILICON.get(), 16)
                .define('L', Items.LAVA_BUCKET)
                .define('C', ItemTags.COALS)
                .define('S', Tags.Items.SANDS)
                .pattern("SSS")
                .pattern("SSS")
                .pattern("CLC")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "silicon"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.COMPRESSED_WOOD.get(), 4)
                .define('W', ItemTags.LOGS)
                .pattern(" W ")
                .pattern("WWW")
                .pattern(" W ")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "compressed_wood"));

        ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, Materials.MECHANICAL_COMPONENTS.get(), 2)
                .requires(Materials.SPRING.get(), 4)
                .requires(Materials.PLASTIC.get(), 2)
                .requires(Items.IRON_NUGGET, 2)
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "mechanical_components"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.ACETONE_SOLUTION.get())
                .define('W', Items.WATER_BUCKET)
                .define('P', AVAItemTagsProvider.PLASTIC)
                .define('B', Tags.Items.BONES)
                .pattern("PBP")
                .pattern("PBP")
                .pattern("PWP")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "acetone_solution"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.FUSE.get(), 4)
                .define('G', Tags.Items.GLASS_BLOCKS)
                .define('S', AVAItemTagsProvider.SPRING)
                .define('I', AVAItemTagsProvider.WORK_HARDENED_IRON)
                .pattern("III")
                .pattern("GSG")
                .pattern("III")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "fuse"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.FIBRE.get(), 4)
                .define('C', Items.COBWEB)
                .define('I', AVAItemTagsProvider.WORK_HARDENED_IRON)
                .pattern(" C ")
                .pattern("CIC")
                .pattern(" C ")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "fibre"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Materials.CERAMIC.get(), 2)
                .define('M', Items.MUD)
                .define('S', Tags.Items.SANDS)
                .define('C', Items.CLAY)
                .define('L', Items.LAVA_BUCKET)
                .pattern("MSM")
                .pattern("MCM")
                .pattern("MLM")
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "ceramic"));

        ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, Materials.SMOKE_POWDER.get(), 4)
                .requires(Items.SUGAR, 4)
                .requires(Items.REDSTONE, 2)
                .requires(Items.CLAY_BALL, 2)
                .unlockedBy("has_iron_ingot", has(Tags.Items.INGOTS_IRON))
                .save(consumer, new ResourceLocation(AVA.MODID, "smoke_powder"));

        ShapedRecipeBuilder.shaped(RecipeCategory.MISC, Items.COBWEB, 5)
                .define('S', Tags.Items.STRINGS)
                .pattern("S S")
                .pattern(" S ")
                .pattern("S S")
                .unlockedBy("has_string", has(Tags.Items.STRINGS))
                .save(consumer, new ResourceLocation(AVA.MODID, "cobweb"));
    }
}
