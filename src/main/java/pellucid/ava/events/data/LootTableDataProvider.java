package pellucid.ava.events.data;

import com.google.common.collect.ImmutableList;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.WritableRegistry;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.ProblemReporter;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.EmptyLootItem;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import pellucid.ava.items.guns.AVASpecialWeapon;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class LootTableDataProvider extends LootTableProvider
{
    public LootTableDataProvider(PackOutput gen, CompletableFuture<HolderLookup.Provider> provider)
    {
        super(gen, AVAChestLoot.SET, ImmutableList.of(new SubProviderEntry(AVAChestLoot::new, LootContextParamSets.CHEST)), provider);
    }

    @Override
    protected void validate(WritableRegistry<LootTable> writableregistry, ValidationContext validationcontext, ProblemReporter.Collector problemreporter$collector)
    {
        // Avoid crashes
    }

    public static class AVAChestLoot implements LootTableSubProvider
    {
        public static final ResourceKey<LootTable> BLUE_ROBOT_CONTAINER = loc("blue_robot_container");
        public static final ResourceKey<LootTable> YELLOW_ROBOT_CONTAINER = loc("yellow_robot_container");
        public static final ResourceKey<LootTable> DARK_BLUE_ROBOT_CONTAINER = loc("dark_blue_robot_container");

        public static final ResourceKey<LootTable> OUTPOST_DESERT = loc("outpost_desert");
        public static final ResourceKey<LootTable> OUTPOST_SNOW = loc("outpost_snow");
        public static final ResourceKey<LootTable> OUTPOST_FOREST = loc("outpost_forest");
        public static final ResourceKey<LootTable> OUTPOST_OCEAN = loc("outpost_ocean");

        public static Set<ResourceKey<LootTable>> SET = null;
        private static ResourceKey<LootTable> loc(String name)
        {
            ResourceKey<LootTable> loc = ResourceKey.create(Registries.LOOT_TABLE, AVACommonUtil.modLoc("chests/" + name));
            if (SET == null)
                SET = new HashSet<>();
            SET.add(loc);
            return loc;
        }

        public AVAChestLoot()
        {

        }

        @Override
        public void generate(HolderLookup.Provider provider, BiConsumer<ResourceKey<LootTable>, LootTable.Builder> consumer)
        {
            consumer.accept(BLUE_ROBOT_CONTAINER, LootTable.lootTable()
                    .withPool(guns(1))
                    .withPool(projectiles(3))
                    .withPool(melees(1))
                    .withPool(magazines(3))
                    .withPool(bullets(4))
                    .withPool(irons(4))
                    .withPool(golds(3))
                    .withPool(materials(3))
            );
            consumer.accept(YELLOW_ROBOT_CONTAINER, LootTable.lootTable()
                    .withPool(guns(1))
                    .withPool(projectiles(2))
                    .withPool(melees(1))
                    .withPool(magazines(2))
                    .withPool(bullets(2))
                    .withPool(irons(3))
                    .withPool(golds(2))
                    .withPool(diamonds(1))
                    .withPool(materials(4))
            );
            consumer.accept(DARK_BLUE_ROBOT_CONTAINER, LootTable.lootTable()
                    .withPool(guns(2))
                    .withPool(projectiles(3))
                    .withPool(melees(2))
                    .withPool(magazines(3))
                    .withPool(bullets(3))
                    .withPool(irons(4))
                    .withPool(golds(3))
                    .withPool(diamonds(2))
                    .withPool(netherites(1))
                    .withPool(materials(5))
            );
            consumer.accept(OUTPOST_DESERT, outpost().withPool(desert()).withPool(dryFood(3)));
            consumer.accept(OUTPOST_SNOW, outpost().withPool(snow()).withPool(dryFood(3)));
            consumer.accept(OUTPOST_FOREST, outpost().withPool(forest()).withPool(dryFood(2)).withPool(meat(3)));
            consumer.accept(OUTPOST_OCEAN, outpost().withPool(ocean()).withPool(dryFood(2)).withPool(seafood(3)));
        }

        private static LootPool.Builder ocean()
        {
            return items(ImmutableList.of(Items.KELP, Items.STICK), 0, 3, 8, 0.35F);
        }

        private static LootPool.Builder forest()
        {
            return items(ImmutableList.of(Items.OAK_LEAVES, Blocks.COBBLESTONE), 0, 3, 6, 0.35F);
        }

        private static LootPool.Builder snow()
        {
            return items(ImmutableList.of(Items.SNOWBALL, Blocks.ICE), 0, 3, 8, 0.35F);
        }

        private static LootPool.Builder desert()
        {
            return items(ImmutableList.of(Items.WATER_BUCKET, Blocks.SAND), 0, 3, 5, 0.25F);
        }

        private static LootTable.Builder outpost()
        {
            return LootTable.lootTable()
                    .withPool(guns(2))
                    .withPool(projectiles(1))
                    .withPool(melees(1))
                    .withPool(magazines(2))
                    .withPool(bullets(3))
                    .withPool(materials(4));
        }

        private static LootPool.Builder dryFood(int maxRolls)
        {
            return items(ImmutableList.of(Items.BREAD, Items.BONE, Items.COOKIE, Items.DRIED_KELP, Items.POTATO), 0, maxRolls, 5, 0.35F);
        }

        private static LootPool.Builder meat(int maxRolls)
        {
            return items(ImmutableList.of(Items.COOKED_BEEF, Items.COOKED_CHICKEN, Items.COOKED_MUTTON, Items.COOKED_RABBIT, Items.COOKED_PORKCHOP), 0, maxRolls, 5, 0.35F);
        }

        private static LootPool.Builder seafood(int maxRolls)
        {
            return items(ImmutableList.of(Items.TROPICAL_FISH, Items.COOKED_COD, Items.COOKED_SALMON, Items.PUFFERFISH), 0, maxRolls, 4, 4F);
        }

        private static LootPool.Builder netherites(int maxRolls)
        {
            return items(ImmutableList.of(Items.NETHERITE_BLOCK, Items.NETHERITE_INGOT), 0, maxRolls, 1, 0.85F);
        }

        private static LootPool.Builder diamonds(int maxRolls)
        {
            return items(ImmutableList.of(Items.DIAMOND_BLOCK, Items.DIAMOND), 0, maxRolls, 2, 0.75F);
        }

        private static LootPool.Builder golds(int maxRolls)
        {
            return items(ImmutableList.of(Items.GOLD_BLOCK, Items.GOLD_INGOT, Items.GOLD_NUGGET), 1, maxRolls, 4, 0.4F);
        }

        private static LootPool.Builder irons(int maxRolls)
        {
            return items(ImmutableList.of(Items.IRON_BLOCK, Items.IRON_INGOT, Items.IRON_NUGGET), 1, maxRolls, 6, 0.2F);
        }

        private static LootPool.Builder bullets(int maxRolls)
        {
            return items(AVAWeaponUtil.getAllAmmo((item) -> item instanceof Ammo && (((Ammo) item).isMagazine() || ((Ammo) item).isSpecial())), 1, maxRolls, 48, 0.95F);
        }

        private static LootPool.Builder magazines(int maxRolls)
        {
            return items(AVAWeaponUtil.getAllAmmo((item) -> item instanceof Ammo && !((Ammo) item).isMagazine()), 3, maxRolls, 4, 0.9F);
        }

        private static LootPool.Builder melees(int maxRolls)
        {
            return items(AVAWeaponUtil.getAllGunLikes((item) -> item instanceof AVASpecialWeapon), 0, maxRolls, 1, 0.5F);
        }

        private static LootPool.Builder guns(int maxRolls)
        {
            return items(AVAWeaponUtil.getAllGunLikes((item) -> item instanceof AVASpecialWeapon), 0, maxRolls, 1, 0.25F);
        }

        private static LootPool.Builder projectiles(int maxRolls)
        {
            return items(AVAWeaponUtil.getProjectileWeapons(), 0, maxRolls, 4, 0.2F);
        }

        private static LootPool.Builder materials(int maxRolls)
        {
            return items(AVACommonUtil.collect(Materials.CACHED_ITEMS, Supplier::get), 1, maxRolls, 16, 0.85F);
        }

        private static LootPool.Builder items(List<? extends ItemLike> items, int minRolls, int maxRolls, int maxCount, float emptyChance)
        {
            LootPool.Builder pool = LootPool.lootPool().setRolls(UniformGenerator.between(minRolls, maxRolls));
            for (ItemLike item : items)
                pool.add(LootItem.lootTableItem(item).setWeight(1).apply(SetItemCountFunction.setCount(UniformGenerator.between(1, maxCount))));
            if (emptyChance != 0)
                pool.add(EmptyLootItem.emptyItem().setWeight(Math.round(items.size() * emptyChance)));
            return pool;
        }
    }
}
