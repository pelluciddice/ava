package pellucid.ava.events.data;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.common.data.SoundDefinition;
import net.neoforged.neoforge.common.data.SoundDefinitionsProvider;
import net.neoforged.neoforge.registries.DeferredHolder;
import pellucid.ava.AVA;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Pair;

import static pellucid.ava.sounds.AVASounds.*;

/**
 *
 * gun: -15db
 * footsteps: -18db
 * broadcast: ~-12db
 * robots: ~-8db
 * other: -14db
 * radio: -9db
 * passive radio: -12db
 */
public class SoundProvider extends SoundDefinitionsProvider
{
    public SoundProvider(PackOutput generator, ExistingFileHelper helper)
    {
        super(generator, AVA.MODID, helper);
    }

    @Override
    public void registerSounds()
    {
        for (String sound : AVASoundTracks.GROUP_GUNS.getB())
            addGun(sound, BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(AVA.MODID, sound)), "weapon_action", 50);

        for (String sound : AVASoundTracks.GROUP_MELEES.getB())
            addMelee(sound, BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(AVA.MODID, sound)), "weapon_action", 6);

        FOOTSTEP_SOUNDS.forEach((sound) -> addFootstepSound(sound.get(), 4));
        BROADCAST_SOUNDS.forEach(this::addBroadcastSound);

        for (AVAWeaponUtil.RadioMessage message : AVAWeaponUtil.RadioMessage.values())
        {
            if (!message.noAudio())
            {
                Pair<Integer, Integer> multiple = message.getMultiple();
                addFutureRadio(message.getEUSound(), "entities/radio/eu", multiple.getA());
                addFutureRadio(message.getNRFSound(), "entities/radio/nrf", multiple.getB());
            }
        }

        addRobotSound(AVASounds.COMMON_ROBOT_ATTACK, 2);
        addRobotSound(AVASounds.COMMON_ROBOT_DEATH, 3);
        addRobotSound(AVASounds.COMMON_ROBOT_ONHIT, 1);
        addRobotSound(AVASounds.BLUE_ROBOT_AMBIENT, 1);
        addRobotSound(AVASounds.DARK_BLUE_ROBOT_AMBIENT, 1);
        addRobotSound(AVASounds.DARK_BLUE_ROBOT_ONHIT, 1);
        addRobotSound(AVASounds.DARK_BLUE_ROBOT_STEP, 1);
        addRobotSound(AVASounds.YELLOW_ROBOT_AMBIENT, 1);

        addLeopardSound(AVASounds.LEOPARD_AMBIENT, 1);
        addLeopardSound(LEOPARD_MOVING, 1);

        addBlockHitSound(BULLET_BLOCK_DIRT, 3);
        addBlockHitSound(BULLET_BLOCK_METAL_CRUNCHY, 3);
        addBlockHitSound(BULLET_BLOCK_METAL_EMPTY, 3);
        addBlockHitSound(BULLET_BLOCK_METAL_THIN, 3);
        addBlockHitSound(BULLET_BLOCK_ORGANIC, 3);
        addBlockHitSound(BULLET_BLOCK_ROCK, 3);
        addBlockHitSound(BULLET_BLOCK_WATER, 3);
        addBlockHitSound(BULLET_BLOCK_WOOD, 11);
        addBlockHitSound(COMMON_BLOCK_GLASS, 6);
        addBlockHitSound(COMMON_BLOCK_PLASTIC, 2);
        addBlockHitSound(MELEE_BLOCK_METAL_CRUNCHY, 3);
        addBlockHitSound(MELEE_BLOCK_METAL_THIN, 3);
        addBlockHitSound(MELEE_BLOCK_ORGANIC, 3);
        addBlockHitSound(MELEE_BLOCK_ROCK, 3);
        addBlockHitSound(MELEE_BLOCK_WOOD, 8);

        addVoice(GRENADE_EU, "entities/radio_passive/eu", 1);
        addVoice(GRENADE_NRF, "entities/radio_passive/nrf", 1);
        addVoice(RELOAD_EU, "entities/radio_passive/eu", 3);
        addVoice(RELOAD_NRF, "entities/radio_passive/nrf", 3);
        addVoice(ENEMY_GRENADE_EU, "entities/radio_passive/eu", 4);
        addVoice(ENEMY_GRENADE_NRF, "entities/radio_passive/nrf", 4);
        addVoice(ALLY_DOWN_EU, "entities/radio_passive/eu", 3);
        addVoice(ALLY_DOWN_NRF, "entities/radio_passive/nrf", 3);
        addVoice(ENEMY_DOWN_EU, "entities/radio_passive/eu", 3);
        addVoice(ENEMY_DOWN_NRF, "entities/radio_passive/nrf", 3);
        addVoice(ON_HIT_EU, "entities/radio_passive/eu", 2);
        addVoice(ON_HIT_NRF, "entities/radio_passive/nrf", 2);

        addMisc(AIM, "guns/default");
        addMisc(COMMON_DRAW_LIGHT, "guns/default");
        addMisc(COMMON_DRAW_HEAVY, "guns/default");
        addMisc(COMMON_DRAW_SNIPER, "guns/default");
        addMisc(COMMON_DRAW, "guns/default");
        addMisc(COMMON_DRAW_OUT, "guns/default");
        addMisc(COMMON_EQUIP, "guns/default");
        addMisc(ROCKET_EXPLODE, "guns/default", 40);
        addMisc(ROCKET_TRAVEL, "guns/default");
        addMisc(SILENCER_INSTALLS, "guns/default");
        addMisc(SILENCER_UNINSTALLS, "guns/default");
        addMisc(SILENCED_SHOT, "guns/default");

        addMisc(GRENADE_PULL, "grenades/default");
        addMisc(GRENADE_HIT, "grenades/default");
        addMultipleMisc(GENERIC_GRENADE_EXPLODE, "grenades", 70, 3);
        addMultipleMisc(MK3A2_EXPLODE, "grenades", 65, 3);
        addMisc(FLASH_GRENADE_EXPLODE, "grenades", 60);
        addMisc(SMOKE_GRENADE_ACTIVE, "grenades", 25);

        addMisc(PARACHUTE_OPEN, "entities");
        addMultipleMisc(HEADSHOT, "entities", 3);
        addMultipleMisc(HEADSHOT_HELMET, "entities", 3);
        addMisc(COLLECTS_PICKABLE, "entities");
        addMisc(PICKUP_ITEM, "entities");
        addMultipleMisc(BULLET_FLY_BY, "entities/bullet", 3, 7);

        addMisc(NIGHT_VISION_ACTIVATE, "overlay");
        addMisc(BIO_INDICATOR_BEEP, "overlay");
        addMisc(SELECT_PRESET, "overlay");
        addMisc(CHAT_MESSAGE, "overlay");

        addMisc(UAV_CAPTURED, "items");
        addMisc(UAV_CAPTURES, "items");
        addMultipleMisc(UAV_SUPPORT, "environment", 4);
        addMisc(C4_BUTTON_HIT, "items");
        addMisc(C4_LEVER_UP, "items");
        addMisc(C4_DRAW, "items");
        addMisc(C4_BEEPS, "items", 30);
        addMisc(C4_EXPLODE, "items");

        addMisc(AMMO_KIT_SUPPLIER_CONSUME, "blocks");
        addMisc(BLOCK_BOOSTS_PLAYER, "blocks");
        addMisc(EXPLOSIVE_BARREL_EXPLODE, "blocks", 40);
        addMisc(RPG_BOX_OPEN, "blocks");
        addMisc(RPG_BOX_CLOSE, "blocks");

        for (DeferredHolder<SoundEvent, SoundEvent> sound : ENVIRONMENT_SOUNDS)
            addEnvironment(sound);
    }

    private void addEnvironment(DeferredHolder<SoundEvent, SoundEvent> sound)
    {
        String name = sound.getId().getPath().replace("environment_", "");
        add(sound.get(), "environment/" + name, null);
    }

    private void addLeopardSound(DeferredHolder<SoundEvent, SoundEvent> sound, int multiple)
    {
        add(sound.get(), "entities/" + sound.getId().getPath(), "leopard_noise", 16, multiple);
    }

    private void addRobotSound(DeferredHolder<SoundEvent, SoundEvent> sound, int multiple)
    {
        addFutureMultiple(sound.get(), "entities/robots/" + sound.getId().getPath(), "robot_noise", 16, multiple);
    }

    private void addBlockHitSound(DeferredHolder<SoundEvent, SoundEvent> sound, int multiple)
    {
        addFutureMultiple(sound.get(), "blocks/onhit/" + sound.getId().getPath(), "subtitles.block.generic.break", 16, multiple);
    }

    private void addBroadcastSound(DeferredHolder<SoundEvent, SoundEvent> sound)
    {
        add(sound.get(), "broadcast/" + sound.getId().getPath(), "broadcast", 16, 1);
    }

    protected void addFootstepSound(SoundEvent sound, int count)
    {
        String name = BuiltInRegistries.SOUND_EVENT.getKey(sound).getPath().replace("footstep_", "");
        add(sound, "steps/steps_" + name, "subtitles.block.generic.footsteps", 20, 4);
    }

    protected void addGun(SoundEvent sound, String subtitle, int attenuation)
    {
        addGun(BuiltInRegistries.SOUND_EVENT.getKey(sound).getPath(), sound, subtitle, attenuation);
    }

    protected void addGun(String name, SoundEvent sound, String subtitle, int attenuation)
    {
        add(sound, "guns/" + name, subtitle, attenuation);
    }

    protected void addMelee(String name, SoundEvent sound, String subtitle, int attenuation)
    {
        add(sound, "melees/" + name, subtitle, attenuation);
    }

    protected void addFutureRadio(SoundEvent sound, String subfolder, int multiple)
    {
        addFutureMultiple(sound, subfolder + "/" + BuiltInRegistries.SOUND_EVENT.getKey(sound).getPath(), "radio", 5, multiple);
    }

    protected void addVoice(DeferredHolder<SoundEvent, SoundEvent> sound, String subfolder, int multiple)
    {
        add(sound.get(), subfolder + "/" + BuiltInRegistries.SOUND_EVENT.getKey(sound.get()).getPath(), "voice", 15, multiple);
    }

    protected void addRadio(SoundEvent sound, String subfolder, int multiple)
    {
        add(sound, subfolder + "/" + BuiltInRegistries.SOUND_EVENT.getKey(sound).getPath(), "radio", 5, multiple);
    }

    protected void addMisc(DeferredHolder<SoundEvent, SoundEvent> sound, String subfolder)
    {
        addMisc(sound, subfolder, 16);
    }

    protected void addMisc(DeferredHolder<SoundEvent, SoundEvent> sound, String subfolder, int attenuation)
    {
        addMultipleMisc(sound.get(), BuiltInRegistries.SOUND_EVENT.getKey(sound.get()).getPath(), subfolder, attenuation, 1);
    }

    protected void addMultipleMisc(DeferredHolder<SoundEvent, SoundEvent> sound, String subfolder, int multiple)
    {
        addMultipleMisc(sound.get(), BuiltInRegistries.SOUND_EVENT.getKey(sound.get()).getPath(), subfolder, 16, multiple);
    }

    protected void addMultipleMisc(SoundEvent sound, String name, String subfolder, int multiple)
    {
        addMultipleMisc(sound, subfolder + "/" + name, "ava." + name, 16, multiple);
    }

    protected void addMultipleMisc(DeferredHolder<SoundEvent, SoundEvent> sound, String subfolder, int attenuation, int multiple)
    {
        addMultipleMisc(sound.get(), BuiltInRegistries.SOUND_EVENT.getKey(sound.get()).getPath(), subfolder, attenuation, multiple);
    }

    protected void addMultipleMisc(SoundEvent sound, String name, String subfolder, int attenuation, int multiple)
    {
        add(sound, subfolder + "/" + name, name, attenuation, multiple);
    }

    protected void add(SoundEvent sound, String subtitle, int attenuation)
    {
        String name = BuiltInRegistries.SOUND_EVENT.getKey(sound).getPath();
        add(sound, name, subtitle, attenuation);
    }

    protected void add(SoundEvent sound, String name, String subtitle)
    {
        add(sound, name, subtitle, 16);
    }

    protected void add(SoundEvent sound, String name, String subtitle, int attenuation)
    {
        add(sound, name, subtitle, attenuation, 1);
    }

    protected void add(SoundEvent sound, String name, String subtitle, int attenuation, int multiple)
    {
        SoundDefinition.Sound[] sounds = new SoundDefinition.Sound[multiple];
        name = AVA.MODID + ":" + name;
        if (multiple > 1)
        {
            for (int i = 1; i <= multiple; i++)
                sounds[i - 1] = sound(name + "_" + i).attenuationDistance(attenuation);
        }
        else sounds[0] = sound(name).attenuationDistance(attenuation);
        if (subtitle != null && !subtitle.contains("."))
            subtitle = "subtitles.ava." + subtitle;
        add(BuiltInRegistries.SOUND_EVENT.getKey(sound), definition().subtitle(subtitle).with(sounds));
    }

    protected void addFutureMultiple(SoundEvent sound, String name, String subtitle, int attenuation, int multiple)
    {
        SoundDefinition.Sound[] sounds = new SoundDefinition.Sound[multiple];
        name = AVA.MODID + ":" + name;
        for (int i = 1; i <= multiple; i++)
            sounds[i - 1] = sound(name + "_" + i).attenuationDistance(attenuation);
        if (subtitle != null && !subtitle.contains("."))
            subtitle = "subtitles.ava." + subtitle;
        add(BuiltInRegistries.SOUND_EVENT.getKey(sound), definition().subtitle(subtitle).with(sounds));
    }
}
