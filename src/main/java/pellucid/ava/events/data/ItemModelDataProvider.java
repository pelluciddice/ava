package pellucid.ava.events.data;

import net.minecraft.client.renderer.block.model.BlockModel;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder;
import net.neoforged.neoforge.client.model.generators.ItemModelProvider;
import net.neoforged.neoforge.client.model.generators.ModelFile;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.registries.DeferredBlock;
import net.neoforged.neoforge.registries.DeferredItem;
import org.apache.commons.lang3.StringUtils;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;

public class ItemModelDataProvider extends ItemModelProvider
{
    public ItemModelDataProvider(PackOutput generator, ExistingFileHelper existingFileHelper)
    {
        super(generator, AVA.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels()
    {
        List<Item> items = AVAWeaponUtil.getAllAmmo((item) -> true);
        for (Item item : items)
            generatedAmmo(BuiltInRegistries.ITEM.getKey(item).getPath());
        block(AVABlocks.EXPLOSIVE_BARREL.getId().getPath());

        AVAWeaponUtil.getAllGuns().forEach((item) -> {
            try
            {
                String path = BuiltInRegistries.ITEM.getKey(item).getPath();
                getBuilder("item/" + path + "/" + path + "_texture")
                        .parent(new ModelFile.UncheckedModelFile("item/generated"))
                        .texture("layer0", modLoc("item/gun_textures/icon/" + path));
            }
            catch (Exception ignored) {}
        });

        for (Supplier<Block> b : AVABuildingBlocks.CLASSIC_BLOCK_EXTENSIONS)
        {
            Block block = b.get();
            String path = AVACommonUtil.getRegistryNameBlock(block).getPath();
            String path2 = "";
            if (AVABuildingBlocks.THIN_PILLAR_BLOCKS.contains(b))
                path2 = "thin_pillars/";
            else if (AVABuildingBlocks.WALL_THIN_PILLAR_BLOCKS.contains(b))
                path2 = "wall_thin_pillars/";
            if (!path2.isEmpty())
                getBuilder("item/" + path)
                        .parent(new ModelFile.UncheckedModelFile("ava:block/" + path2 + path));
        }

        for (DeferredBlock<Block> block : AVABuildingBlocks.SLOPE_BLOCKS)
        {
            String name = block.getId().getPath();
            getBuilder("item/" + name)
                    .parent(new ModelFile.UncheckedModelFile("ava:block/slope_" + AVABuildingBlocks.SLOPE_ANGLES.get(block) + "s/" + name)).transforms()
                    .transform(ItemDisplayContext.GUI).scale(0.5F).rotation(0.0F, 90.0F, 0.0F).end()
                    .transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).scale(0.5F).end()
                    .transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).scale(0.5F).end()
                    .transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).scale(0.5F).end()
                    .transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).scale(0.5F).end()
                    .transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).scale(0.5F).end().end()
                    .guiLight(BlockModel.GuiLight.FRONT)
            ;
        }

        stairs("item/" + AVABuildingBlocks.SMOOTH_STONE_STAIRS.getId().getPath(), new ResourceLocation("block/smooth_stone"));
        //        block(AVABlocks.SMOOTH_STONE_STAIRS);

        //        new ArrayList<>(AVABlocks.BLOCKS).forEach((block) -> block(AVACommonUtil.getRegistryNameBlock(block).getPath()));

        Materials.commonSetup();
        for (Item item : Materials.MATERIALS)
            simpleItem(item, "materials");

        simpleItem(AVABuildingBlocks.GLASS_FENCE.get().asItem(), "misc");
        simpleItem(AVABuildingBlocks.GLASS_FENCE_TALL.get().asItem(), "misc");
        simpleItem(AVABuildingBlocks.GLASS_WALL.get().asItem(), "misc");
        simpleItem(AVABuildingBlocks.GLASS_TRIG_WALL.get().asItem(), "misc");
        simpleItem(AVABuildingBlocks.GLASS_TRIG_WALL_FLIPPED.get().asItem(), "misc");

        generated(AVABlocks.RPG_BOX.getId().getPath(), "block/rpg_box_bottom");

        roundGrenade(Projectiles.M67);
//        roundGrenade(Projectiles.M67_SPORTS);

        cylinderGrenade(Projectiles.MK3A2);
        cylinderGrenade(Projectiles.M116A1);

        cylinderGrenade(Projectiles.M18_GREY);
        cylinderGrenade(Projectiles.M18_GREY_II);
        cylinderGrenade(Projectiles.M18_GREY_III);
        cylinderGrenade(Projectiles.M18_RED);
        cylinderGrenade(Projectiles.M18_RED_II);
        cylinderGrenade(Projectiles.M18_RED_III);
        cylinderGrenade(Projectiles.M18_PURPLE);
        cylinderGrenade(Projectiles.M18_BLUE);
        cylinderGrenade(Projectiles.M18_TOXIC);
        cylinderGrenade(Projectiles.M18_YELLOW);
    }

    private void roundGrenade(DeferredItem<Item> item)
    {
        grenade(item, "round");
    }

    private void cylinderGrenade(DeferredItem<Item> item)
    {
        grenade(item, "cylinder");
    }


    private void grenade(DeferredItem<Item> item, String type)
    {
        ResourceLocation texture = modLoc("item/grenades/" + item.getId().getPath());
        String name = item.getId().getPath();
        getBuilder(name)
                .parent(getExistingFile(modLoc("item/aaparent/grenade_" + type)))
                .texture("skin", texture);
        getBuilder("item/" + name + "/" + name + "_pin")
                .parent(getExistingFile(modLoc("item/aaparent/grenade_" + type + "_pin")))
                .texture("skin", texture);
    }

    private void simpleItem(Item item, String folder)
    {
        ResourceLocation id = BuiltInRegistries.ITEM.getKey(item);
        getBuilder(item.toString())
                .parent(new ModelFile.UncheckedModelFile("item/generated"))
                .texture("layer0", new ResourceLocation(id.getNamespace(), "item/" + folder + "/" + id.getPath()));
    }

    public ItemModelBuilder stairs(String name, ResourceLocation texture)
    {
        return stairs(name, texture, texture, texture);
    }

    private void generated(String name)
    {
        generated(name, name);
    }

    private void generated(String fileName, String textureName)
    {
        getBuilder(fileName)
                .parent(new ModelFile.UncheckedModelFile("item/generated"))
                .texture("layer0", modLoc(textureName));
    }

    private static boolean DEBUG = false;

    private void generatedAmmo(String name)
    {
        try
        {
            getBuilder(name)
                    .parent(new ModelFile.UncheckedModelFile("item/generated"))
                    .texture("layer0", modLoc("item/ammo/" + name));
        }
        catch (Exception e)
        {
            if (DEBUG)
                e.printStackTrace();
        }
    }

    private void block(String name)
    {
        getBuilder(name)
                .parent(getExistingFile(modLoc("block/" + name)));
    }

    //just converts a name to capitalized, and without any '_'s
    private String getName(ResourceLocation location)
    {
        String name = location.getPath().replaceAll("_", " ");
        StringBuilder builder = new StringBuilder(name.length());
        String[] s = name.split(" ");
        for (String n : s)
        {
            n = StringUtils.capitalize(n.toLowerCase(Locale.ROOT));
            if (builder.length() != 0)
                builder.append(" ");
            builder.append(n);
        }
        return builder.toString();
    }
}
