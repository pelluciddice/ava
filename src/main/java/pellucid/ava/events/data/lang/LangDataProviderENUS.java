package pellucid.ava.events.data.lang;

import net.minecraft.data.PackOutput;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.DeferredItem;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlockDyeColours;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.init.Materials;

import java.util.HashMap;
import java.util.Map;

import static pellucid.ava.blocks.AVABlocks.*;
import static pellucid.ava.blocks.AVABuildingBlocks.*;
import static pellucid.ava.items.init.Magazines.*;
import static pellucid.ava.items.init.MeleeWeapons.FIELD_KNIFE;
import static pellucid.ava.items.init.MeleeWeapons.SCYTHE_IGNIS;
import static pellucid.ava.items.init.MiscItems.*;
import static pellucid.ava.items.init.Pistols.*;
import static pellucid.ava.items.init.Projectiles.*;
import static pellucid.ava.items.init.Rifles.*;
import static pellucid.ava.items.init.Snipers.*;
import static pellucid.ava.items.init.SpecialWeapons.*;
import static pellucid.ava.items.init.SubmachineGuns.*;

public class LangDataProviderENUS extends AVALanguageProvider
{
    public LangDataProviderENUS(PackOutput gen)
    {
        this(gen, "en_us");
    }

    public LangDataProviderENUS(PackOutput gen, String locale)
    {
        super(gen, locale);
    }

    private final Map<String, String> map = new HashMap<>();

    protected void addItemTranslations()
    {
        // Guns
        addItem(AK12, "AK-12");
        addItem(AK12_UNIT_01, "AK-12 Unit 01");
        addItem(AK47, "AK47");
        addItem(AK47_BLITZ, "AK47 Blitz");
        addItem(AK47_PREDATOR, "AK47 Predator");
        addItem(AKS74U, "AKS-74U");
        addItem(AWM, "AWM");
        addItem(BARRETT, "Barrett M82A3");
        addItem(BERETTA_92FS, "Beretta92FS");
        addItem(BERETTA_92FS_SPORTS, "Beretta Sports");
        addItem(BERETTA_92FS_BARBATOS, "Beretta Barbatos");
        addItem(COLT_SAA, "Colt SAA");
        addItem(CZ_EVO3, "CZ Skorpion Evo3");
        addItem(CZ_EVO3_COTTON_CANDY, "CZ Evo3 Cotton Candy");
        addItem(D_DEFENSE_10GA, "D.Defense 10ga");
        addItem(DESERT_EAGLE, "DesertEagle");
        addItem(DESERT_EAGLE_BLACK, "DesertEagle Black");
        addItem(DESERT_EAGLE_SILVER, "DesertEagle Silver");
        addItem(DESERT_EAGLE_THE_ARGUS, "DesertEagle The Argus");
        addItem(DSR1, "DSR-1");
        addItem(FG42, "FG42");
        addItem(FG42_DREAMCATCHER, "FG42 Dreamcatcher");
        addItem(FG42_SUMIRE, "FG42 Sumire");
        addItem(FN_FNC, "FN-FNC");
        addItem(FN_FNC_DREAMCATCHER, "FN-FNC Dreamcatcher");
        addItem(FN_FNC_FULLMOON, "FN-FNC Fullmoon");
        addItem(FN_TPS, "FN TPS");
        addItem(FN57, "FN57");
        addItem(FN57_CHRISTMAS, "Milad FN57");
        addItem(FN57_SNOWFALL, "FN57 Snowfall");
        addItem(FR_F2, "FR-F2");
        addItem(FR_F2_CHRISTMAS, "Noël FR-F2");
        addItem(FR_F2_SUMIRE, "FR-F2 Sumire");
        addItem(G36KA1, "G36K");
        addItem(GLOCK, "Glock21C");
        addItem(GM94, "GM-94 Grenade Launcher");
        addItem(HEAVY_AP_MAGAZINE, "Heavy AP Magazine");
        addItem(K1A1, "K1A1 Rail");
        addItem(K1A1_RED_SECTOR, "K1A1 Red Sector");
        addItem(K1A1_SKILLED, "K1A1 Skilled");
        addItem(KELTEC, "Kel-Tec RFB");
        addItem(KELTEC_COSMIC, "Kel-Tec RFB Cosmic");
        addItem(KRISS, "Kriss Super V");
        addItem(KRISS_CHRISTMAS, "Noël Super Kriss V");
        addItem(M1_GARAND, "M1 Garand");
        addItem(M1_GARAND_R_LAB, "M1 Garand R-Lab");
        addItem(M16_VN, "M16 VN");
        addItem(M16_VN_FROST_SNOW, "M16 VN Frost Snow");
        addItem(M18_RED, "M18 Red");
        addItem(M18_RED_II, "M18 Red II");
        addItem(M18_RED_III, "M18 Red III");
        addItem(M202, "M202 Rocket Launcher");
        addItem(M24, "M24");
        addItem(M24_FLEUR_DE_LYS, "M24 Fleur-de-lys");
        addItem(M4A1, "M4A1");
        addItem(M4A1_COTTON_CANDY, "M4A1 Cotton Candy");
        addItem(M4A1_DREAMCATCHER, "M4A1 Dreamcatcher");
        addItem(M4A1_SUMIRE, "M4A1 Sumire");
        addItem(M4A1_XPLORER, "M4A1 X-Plorer");
        addItem(MAUSER_C96, "Mauser C96 MP");
        addItem(MK18, "MK.18 MOD 0");
        addItem(MK18_AIR_WARFARE, "MK.18 Air Warfare");
        addItem(MK18_KUYO_MON, "MK.18 Kuyo-mon");
        addItem(MK20, "Mk.20 Proto SSR");
        addItem(MK20_BALD_EAGLE, "Mk.20 SSR Bald Eagle");
        addItem(MOSIN_NAGANT, "Mosin-Nagant");
        addItem(MOSIN_NAGANT_SPORTS, "Mosin-Nagant Sports");
        addItem(MOSIN_NAGANT_SUMIRE, "Mosin-Nagant Sumire");
        addItem(MP5K, "MP5K");
        addItem(MP5K_FROST, "MP5K Frost");
        addItem(MP5SD5, "MP5SD5");
        addItem(MP7A1, "MP7A1");
        addItem(MP7A1_LIGHT, "MP7A1 Light");
        addItem(MP7A1_SUMIRE, "MP7A1 Sumire");
        addItem(MP7A1_VALKYRIE, "MP7A1 Red Valkyrie");
        addItem(P226, "P226");
        addItem(P90, "P90");
        addItem(PYTHON357, "Python.357");
        addItem(PYTHON357_GOLD, "Python Gold");
        addItem(PYTHON357_OVERRIDER, "Python OverRider");
        addItem(REGULAR_PISTOL_MAGAZINE, "Regular Pistol Magazine");
        addItem(REMINGTON870, "Remington870");
        addItem(REMINGTON870_DREAMCATCHER, "Remington870 Dreamcatcher");
        addItem(RK95, "Sako RK.95");
        addItem(RPG7, "RPG-7");
        addItem(SA58, "SA58 Para");
        addItem(SA58_MILAD, "Milad FAL");
        addItem(SCAR_L, "SCAR-L");
        addItem(SCYTHE_IGNIS, "Ignis Scythe");
        addItem(SG556, "SG556");
        addItem(SG556_BLACK_WIDOW, "SG556 Black Widow");
        addItem(SR_25, "Knights SR-25");
        addItem(SR_25_KNUT, "Knut's M110");
        addItem(SR_2M_VERESK, "SR-2M Veresk");
        addItem(SR_2M_VERESK_SUMIRE, "SR-2M Veresk Sumire");
        addItem(SW1911_COLT, "SW1911");
        addItem(SW1911_COLT_ARGENTO, "SW1911 Argento");
        addItem(X95R, "X95R");
        addItem(X95R_AUBE, "X95R Aube");
        addItem(X95R_CHRISTMAS, "X95R Christmas");
        addItem(XCR, "Robinson XCR");
        addItem(XM8, "XM8");
        addItem(XM8_FROST, "XM8 Frost");
        addItem(XM8_SNOWFALL, "XM8 Snowfall");

        // Knifes
        addItem(FIELD_KNIFE, "Field Knife");

        // Ammo
        addItem(AMMO_PISTOL, "Pistol Ammo");
        addItem(AMMO_SHOTGUN, "Shotgun Ammo");
        addItem(AMMO_SNIPER, "Sniper Ammo");
        addItem(LARGE_PISTOL_MAGAZINE, "Large Pistol Magazine");
        addItem(REGULAR_RIFLE_MAGAZINE, "Regular Rifle Magazine");
        addItem(REGULAR_SNIPER_MAGAZINE, "Regular Sniper Magazine");
        addItem(REGULAR_SUB_MACHINEGUN_MAGAZINE, "Regular Sub-Machinegun Magazine");
        addItem(SMALL_PISTOL_MAGAZINE, "Small Pistol Magazine");
        addItem(SMALL_RIFLE_MAGAZINE, "Small Rifle Magazine");
        addItem(SMALL_SNIPER_MAGAZINE, "Small Sniper Magazine");
        addItem(SMALL_SUB_MACHINEGUN_MAGAZINE, "Small Sub-Machinegun Magazine");

        // Special Ammo
        addItem(GM94_GRENADE, "GM-94 Grenade");
        addItem(M202_ROCKET, "M202 Rocket");

        // Projectiles
        addItem(M116A1, "M116A1");
        addItem(M18_GREY, "M18 Grey");
        addItem(M18_GREY_II, "M18 Grey II");
        addItem(M18_GREY_III, "M18 Grey III");
        addItem(M18_PURPLE, "M18 Purple");
        addItem(M18_TOXIC, "M18 Toxic");
        addItem(M18_YELLOW, "M18 Yellow");
        addItem(M18_BLUE, "M18 Blue");
        addItem(M67, "M67");
        addItem(M67_SPORTS, "M67 Sports");
        addItem(MK3A2, "MK3A2");

        // Tools
        addItem(AMMO_KIT, "Ammo Kit");
        addItem(AMMO_KIT_I, "Ammo Kit I");
        addItem(AMMO_KIT_II, "Ammo Kit II");
        addItem(BINOCULAR, "Binocular");
        addItem(C4, "C4");
        addItem(EU_STANDARD_BOOTS, "EU Standard Boots");
        addItem(EU_STANDARD_TROUSERS, "EU Standard Trousers");
        addItem(EU_STANDARD_KEVLAR, "EU Standard Kevlar");
        addItem(EU_STANDARD_HELMET, "EU Standard Helmet");
        addItem(NRF_STANDARD_BOOTS, "NRF Standard Boots");
        addItem(NRF_STANDARD_TROUSERS, "NRF Standard Trousers");
        addItem(NRF_STANDARD_KEVLAR, "NRF Standard Kevlar");
        addItem(NRF_STANDARD_HELMET, "NRF Standard Helmet");
        addItem(PARACHUTE, "Parachute");
        addItem(VOID_WATER_BUCKET, "Void Water Bucket");
        addItem(WEAPON_CHEST_MAIN, "Main Weapon Crate");
        addItem(WEAPON_CHEST_SECONDARY, "Secondary Weapon Crate");
        addItem(WEAPON_CHEST_MELEE, "Melee Weapon Crate");
        addItem(WEAPON_CHEST_PROJECTILE, "Projectile Weapon Crate");
        addItem(WEAPON_CHEST_SPECIAL_WEAPON, "Special Weapon Crate");

        // Materials
        for (DeferredItem<Item> item : Materials.CACHED_ITEMS)
            addItem(item, getName(item.getId()));

        // Item Tooltips
        String p = AVA.MODID + ".item.tips.";
        add(p + "ammo", "Ammo: %d/%d");
        add(p + "ammo_kit", "Ammo provided by this item is likely applicable for all weapons");
        add(p + "ammo_kit_2", "Can be repaired with gunpowder");
        add(p + "ammo_kit_3", "\"Infinity\" enchantment will slowly repair this overtime");
        add(p + "ammo_type", "Ammo Type: %s");
        add(p + "armour_bio_indicator", "Biodetector (Zombies)");
        add(p + "armour_full_equipped", "When Fully Equipped:");
        add(p + "armour_hurt_indicator", "Hurt Indicator");
        add(p + "armour_knockback_resistance", "Increase your knockback resistance");
        add(p + "armour_night_vision", "Night Vision Device - press N to activate/deactivate");
        add(p + "armour_projectile_indicator", "Projectile Indicator");
        add(p + "armour_radio", "Communication Radio");
        add(p + "mastery", "Mastery: %s");
        add(p + "mastery_task", "Mastery Task: %s");
        add(p + "more_info", "Hold Shift for More Info");
        add(p + "parachute", "Right click to activate/deactivate (unable to deactivate under competitive mode)");
        add(p + "parachute_open", "Press Space to open Parachute");
        add(p + "projectile_damage", "Damage (Varies by server settings): %.2f");
        add(p + "projectile_flash_duration", "Flash Duration (Varies by server settings): %d");
        add(p + "projectile_impact", "Explode on impact: %s");
        add(p + "projectile_power", "Power: %.2f");
        add(p + "projectile_radius", "Radius (Varies by server settings): %.2f");
        add(p + "projectile_range", "Range (Varies by server settings): %d");
        add(p + "repairable", "Use command %s to repair or destruct");
        add(p + "site_block.info", "C4 will only be plantable near this block if competitive mode is enabled");
        add(p + "smoke.colour", "Colour");
        add(p + "smoke.grow_time", "Grow Time: %d");
        add(p + "smoke.release_time", "Release Smoke Duration: %d");
        add(p + "smoke.release_interval", "Release Smoke Interval: %d");
        add(p + "smoke.steady_time", "Steady Time: %d");
        add(p + "smoke.shrink_time", "Shrink Time: %d");
    }

    protected void addBlockTranslations()
    {
        // Normal Blocks
        String p = "block." + AVA.MODID + ".";
        addBlock(AMMO_KIT_SUPPLIER, "Ammo Kit Supplier");
        addBlock(ATTACK_DAMAGE_BOOST_BLOCK, "Attack Damage Boost Block");
        addBlock(BUILDERS_TABLE, "AVA Builder's Table");
        addBlock(COBBLED_SANDSTONE_TILE, "Cobbled Sandstone Tiles");
        addBlock(COBBLED_SANDSTONE_TILE_SLAB, "Cobbled Sandstone Tile Slab");
        addBlock(COBBLED_SANDSTONE_TILE_STAIRS, "Cobbled Sandstone Tile Stairs");
        addBlock(COMMAND_EXECUTOR, "Command Executor");
        addBlock(CONTROLLER, "Controller");
        addBlock(EXPLOSIVE_BARREL, "Explosive Barrel");
        addBlock(GLASS_FENCE, "Glass Fence");
        addBlock(GLASS_FENCE_TALL, "Tall Glass Fence");
        addBlock(GLASS_TRIG_WALL, "Trig Glass Wall");
        addBlock(GLASS_WALL, "Glass Wall");
        addBlock(GLASS_TRIG_WALL_FLIPPED, "Trig Glass Wall Flipped");
        addBlock(GUN_CRAFTING_TABLE, "A.V.A Crafting Table");
        addBlock(GUN_COLOURING_TABLE, "A.V.A Colouring Table");
        addBlock(GUN_MODIFYING_TABLE, "A.V.A Modifying Table");
        addBlock(HARDENED_IRON_BARS, "Hardened Iron Bars");
        addBlock(HEALTH_BOOST_BLOCK, "Health Boost Block");
        addBlock(IRON_GRID, "Iron Grid");
        addBlock(MASTERY_TABLE, "A.V.A Mastery Workbench");
        addBlock(PRESET_TABLE, "Preset Configuration Table");
        addBlock(REPAIRABLE_FLOWER_POT, "Repairable Flower Pot");
        add(p + "repairable_glass", "Repairable Glass");
        add(p + "repairable_glass_pane", "Repairable Glass Pane");
        addBlock(RPG_BOX, "RPG Weapon Box");
        addBlock(SITE_A, "Site - A");
        addBlock(SITE_B, "Site - B");
        addBlock(SMOOTH_STONE_STAIRS, "Smooth Stone Stairs");
        addBlock(VOID_WATER, "Void Water");

        p = AVA.MODID +".block.";
        // Series Block Basic Names
        add(p + "thin", "Thin");
        add(p + "pillar", "Pillar");
        add(p + "pillar_wall", "Pillar Wall");
        add(p + "stairs", "Stairs");
        add(p + "wall_light", "Wall Light");
        add(p + "planks_floor", "Planks Floor");
        add(p + "slope_90", "Slope 90*");
        add(p + "slope_225", "Slope 22.5* Bottom");
        add(p + "slope_2252", "Slope 22.5* Middle");
        add(p + "slope_2253", "Slope 22.5* Top");
        add(p + "slope_675", "Slope 67.5* Bottom");
        add(p + "slope_6752", "Slope 67.5* Middle");
        add(p + "slope_6753", "Slope 67.5* Top");

        p = "block." + AVA.MODID + ".";
        // Colour Series Blocks
        for (DyeColor colour : DyeColor.values())
        {
            String name = colour.getName();
            add(p + "plaster_" + name, getName(name) + " Plaster");
            add(p + "plaster_slabs_" + name, getName(name) + " Plaster Slabs");
            add(p + "plaster_stairs_" + name, getName(name) + " Plaster Stairs");
            add(p + "repairable_" + name + "_stained_glass", "Repairable " + getName(name) + " Stained Glass");
            add(p + "repairable_" + name + "_stained_glass_pane", "Repairable " + getName(name) + " Stained Glass Pane");
        }
        for (AVABlockDyeColours colour : AVABlockDyeColours.values())
        {
            String name = colour.getName();
            add(p + name + "_concrete", getName(name) + " Concrete");
            add(p + name + "_concrete_powder", getName(name) + " Concrete Powder");
            add(p + name + "_wool", getName(name) + " Wool");
            add(p + "plaster_" + name, getName(name) + " Plaster");
            add(p + "plaster_slabs_" + name, getName(name) + " Plaster Slabs");
            add(p + "plaster_stairs_" + name, getName(name) + " Plaster Stairs");
        }
    }

    protected void addSoundSubtitleTranslations()
    {
        String p = "subtitles." + AVA.MODID + ".";
        add(p + "aim", "Scope Uses");
        add(p + "ammo_kit_supplier_consume", "Ammo Kit Supplier uses");
        add(p + "bio_indicator_beep", "Biosensor Beeps");
        add(p + "block_boosts_player", "Player Boosts");
        add(p + "broadcast", "Broadcast");
        add(p + "bullet_fly_by", "Bullet Flies By");
        add(p + "c4_beeps", "C4 Beeps");
        add(p + "c4_button_hit", "C4 Sets");
        add(p + "c4_draw", "C4 Equipped");
        add(p + "c4_explode", "C4 Explodes");
        add(p + "c4_lever_up", "C4 Sets");
        add(p + "c4_sets", "C4 Sets");
        add(p + "chat_message", "Chat Messages");
        add(p + "collects_pickable", "Collects Pickable");
        add(p + "common_draw", "Weapon Action");
        add(p + "common_draw_heavy", "Weapon Action");
        add(p + "common_draw_light", "Weapon Action");
        add(p + "common_draw_out", "Weapon Action");
        add(p + "common_draw_sniper", "Weapon Action");
        add(p + "common_equip", "Weapon Action");
        add(p + "draw", "Weapon Action");
        add(p + "explosive_barrel_explode", "Explosive Explodes");
        add(p + "flash_grenade_explode", "Explosive Explodes");
        add(p + "generic_grenade_explode", "Explosive Explodes");
        add(p + "grenade_hit", "Metal Collision");
        add(p + "grenade_pull", "Grenade Pin Pulls");
        add(p + "headshot", "Headshot");
        add(p + "headshot_helmet", "Helmet Hit");
        add(p + "leopard_noise", "Engine Noise");
        add(p + "mk3a2_explode", "Grenade Explodes");
        add(p + "night_vision_activate", "Night Vision Activates");
        add(p + "parachute_open", "Parachute Opens");
        add(p + "pickup_item", "Item Picked Up");
        add(p + "radio", "Radio");
        add(p + "reload", "Weapon Action");
        add(p + "robot_noise", "Robot Noise");
        add(p + "rocket_explode", "Rocket Explodes");
        add(p + "rocket_travel", "Rocket Travels");
        add(p + "rpg_box_close", "Weapon Box Closed");
        add(p + "rpg_box_open", "Weapon Box Opened");
        add(p + "select_preset", "Preset Selected");
        add(p + "shot", "Gun Fires");
        add(p + "silencer_installs", "Silencer Installs");
        add(p + "silenced_shot", "Silenced Shot");
        add(p + "silencer_uninstalls", "Silencer Uninstalls");
        add(p + "smoke_grenade_active", "Smoke Grenade Releases Smoke");
        add(p + "uav_captures", "Binocular Captures");
        add(p + "uav_captured", "UAV Warning");
        add(p + "uav_support", "UAV Warning");
        add(p + "voice", "Voice");
        add(p + "weapon_action", "Weapon Action");
    }

    protected void addEntityTranslations()
    {
        // Guards
        addEntityType(AVAEntities.BLUE_MELEE_GUARD, "Guard");
        addEntityType(AVAEntities.GRENADE_LAUNCHER_GUARD, "Guard");
        addEntityType(AVAEntities.PISTOL_GUARD, "Guard");
        addEntityType(AVAEntities.RIFLE_GUARD, "Guard");
        addEntityType(AVAEntities.SHOTGUN_GUARD, "Guard");
        addEntityType(AVAEntities.TOXIC_SMOKE_GUARD, "Guard");

        // Leopard
        addEntityType(AVAEntities.LEOPARD_DESERT, "Leopard2A5");
        addEntityType(AVAEntities.LEOPARD_FOREST, "Leopard2A5");
        addEntityType(AVAEntities.LEOPARD_SNOW, "Leopard2A5");

        // Prisoners
        addEntityType(AVAEntities.GREY_MELEE_PRISONER, "Prisoner");
        addEntityType(AVAEntities.RED_MELEE_PRISONER, "Prisoner");
        addEntityType(AVAEntities.SHOTGUN_PRISONER, "Prisoner");
        addEntityType(AVAEntities.YELLOW_MELEE_PRISONER, "Prisoner");

        // Robots
        addEntityType(AVAEntities.BLUE_ROBOT, "XBG-003 Zonda");
        addEntityType(AVAEntities.DARK_BLUE_ROBOT, "XBG-006 Barbas");
        addEntityType(AVAEntities.YELLOW_ROBOT, "XBG-005 Raum");

        // Soldiers
        addEntityType(AVAEntities.EU_SOLDIER, "EU Soldier");
        addEntityType(AVAEntities.ROLED_SOLDIER, "Roled Soldier");
        addEntityType(AVAEntities.NRF_SOLDIER, "NRF Soldier");

        // Nicknames
        String p = AVA.MODID + ".entity.nicknames.";
        // EU
        add(p + "eu_1", "Hans Joachim Marsell");
        add(p + "eu_2", "Douglas Bader");
        add(p + "eu_3", "Francis Gabreski");
        add(p + "eu_4", "Werner Molders");
        add(p + "eu_5", "Anton Hafner");
        add(p + "eu_6", "David McCampbell");
        add(p + "eu_7", "Adolf Galland");
        add(p + "eu_8", "Walter Nowotny");
        // NRF
        add(p + "nrf_1", "Mikhail Wudenkov");
        add(p + "nrf_2", "Sergei Fedorov");
        add(p + "nrf_3", "Heinz Guderian");
        add(p + "nrf_4", "Zhukov");
        add(p + "nrf_5", "Bradley");
        add(p + "nrf_6", "Francisco Pizarro");
        add(p + "nrf_7", "George Patton");
        add(p + "nrf_8", "Ludwig Zolofov");
        add(p + "nrf_9", "Harold Marshall");
        add(p + "nrf_10", "Vladimir Pavlov");
        add(p + "nrf_11", "Ivan Konev");
        add(p + "nrf_12", "Mikhail Konovalov");
        add(p + "nrf_13", "William Wallace");
        add(p + "nrf_14", "Erwin Rommel");
        add(p + "nrf_15", "Heinz Guderian");
        add(p + "nrf_16", "Erich Von Manstein");
        // C4
        add(p + "c4_defused", "Defused");

        // Objects
        addEntityType(AVAEntities.AMMO_KIT, "Ammo Kit");
        addEntityType(AVAEntities.BULLET, "Bullet");
        addEntityType(AVAEntities.C4, "C4");
        addEntityType(AVAEntities.FIRST_AID_KIT, "First Aid Kit");
        addEntityType(AVAEntities.GRENADE, "Grenade");
        addEntityType(AVAEntities.GUN_ITEM, "Gun Item");
        addEntityType(AVAEntities.M67, "M67 Grenade");
        addEntityType(AVAEntities.M116A1, "M116A1 Grenade");
        addEntityType(AVAEntities.M18, "M18 Smoke Grenade");
        addEntityType(AVAEntities.M18_TOXIC, "M18 Toxic Smoke Grenade");
        addEntityType(AVAEntities.MELEE_RAYTRACING, "Melee Raytracing");
        addEntityType(AVAEntities.MK3A2, "MK3A2 Grenade");
        addEntityType(AVAEntities.PLAIN_SMOKE, "Plain Smoke");
        addEntityType(AVAEntities.ROCKET, "Rocket");
    }

    protected void addCreativeTabTranslations()
    {
        String p = AVA.MODID + ".creative_tab.";
        add(p + "alpha", "Testing Tab");
        add(p + "main", "Main Tab");
        add(p + "map_creation", "Map Creations Tab");
        add(p + "survival", "Survivals Tab");
    }

    protected void addDeathMessageTranslations()
    {
        String p = "death.attack." + AVA.MODID + ":";
        add(p + "bullet", "%1$s was shot by %2$s");
        add(p + "bullet.weapon", "%1$s was shot by %2$s using %3$s");
        add(p + "explosion", "%1$s was blown up by %2$s");
        add(p + "explosion.weapon", "%1$s was blown up by %2$s using %3$s");
        add(p + "direct", "%1$s was killed by %2$s");
        add(p + "direct.weapon", "%1$s was killed by %2$s using %3$s");
        add(p + "killed", "%1$s was killed");
        add(p + "killed.weapon", "%1$s was killed by %3$s");
    }

    protected void addHUDTranslations()
    {
        String p = AVA.MODID + ".hud.";
        add(p + "ads_shake", "Shake");
        add(p + "annihilation", "Annihilation");
        add(p + "annihilation_target", "%1$s Annihilation");
        add(p + "demolish", "Demolition");
        add(p + "demolish_target", "%1$s First Wins");
        add(p + "escort", "Escort Operation");
        add(p + "request_uav_support", "Request UAV Support");
        add(p + "scoreboard_damage", "Dmg.");
        add(p + "scoreboard_latency", "Amt.");
        add(p + "scoreboard_s_d", "S/D");
    }

    protected void addGUITranslations()
    {
        // Widgets
        String p = AVA.MODID + ".gui.widget.";
        add(p + "craft", "Craft");
        add(p + "paint", "Paint");

        // Tooltips
        p = AVA.MODID + ".gui.tooltip.";
        add(p + "valid_for_modify", "Click to modify");
        add(p + "invalid_for_modify", "Not valid for modify");

        // Descriptions
        p = AVA.MODID + ".gui.description.";
        add(p + "average_smoke", "Faster, but lasts shorter.");
        add(p + "binocular", "Can be used to mark enemies.");
        add(p + "command_executor_delay_constant", "Constant Delay");
        add(p + "command_executor_delay_rand_from", "Min Random Delay");
        add(p + "command_executor_delay_rand_to", "Max Random Delay");
        add(p + "fast_smoke", "Fastest, but lasts shortest.");
        add(p + "general_smoke", "General smoke");
        add(p + "m116a1", "Flash Grenade");
        add(p + "mk3a2", "Flash grenade with damage.");
        add(p + "slow_smoke", "Slowest, but lasts longer and larger.");
        add(p + "standard_armour", "Provides same armour as diamond.");
        add(p + "toxic_smoke", "Toxic smoke");

        // Tab Titles
        p = AVA.MODID + ".gui.tab.";
        add(p + "miscs", "Miscs");
        add(p + "pistols", "Pistols");
        add(p + "rifles", "Rifles");
        add(p + "snipers", "Snipers");
        add(p + "submachine_guns", "Submachine Guns");
        add(p + "control", "Control");
        add(p + "display", "Display");
        add(p + "effect", "Effect");
        add(p + "preset", "Preset");

        // Mastery GUI
        p = AVA.MODID + ".gui.mastery.";
        add(p + "pick_mastery", "Pick a mastery for your weapon (Can't be undone)");
        add(p + "shooter_buffs", "Shooter Buffs:");
        add(p + "target_debuffs", "Target Debuffs:");
        add(p + "random_boosts", "Enchant the gun with random %1$s (mastery level) boosts, costs 1 exp level");

        // Client Config GUI
        p = AVA.MODID + ".gui.client_config.";
        add(p + "ai_fast_assets", "Whether simple gun model and texture should be used for mobs. Improves performance.");
        add(p + "ally_status", "Whether nearby ally status (health and name) should be displayed.");
        add(p + "armour", "Whether fancy armour model should be used");
        add(p + "bio", "Whether bio indicator is enabled.");
        add(p + "blue", "Blue Value: 0 ~ 255");
        add(p + "blood", "Environment Effect - Whether blood on walls should be showed. Improves performance significantly.");
        add(p + "bobbing", "Whether AVA's bobbing effect should be applied. Improves performance.");
        add(p + "bullet_hole", "Environment Effect - Whether bullet holes on walls should be showed. Improves performance significantly.");
        add(p + "bullet_trail", "Environment Effect - Whether bullet trails should be showed. Improves performance significantly.");
        add(p + "centre_dot_size", "Dot Size");
        add(p + "creature_status", "Whether nearby creature status (health and name) should be displayed.");
        add(p + "crosshair", "Whether crosshair should be rendered, will be override by server's setting");
        add(p + "damage_tilt", "Whether camera should tilt when receiving damage while having AVA Armour fully equipped.");
        add(p + "discard", "Discard the changes");
        add(p + "draw", "Whether gun's draw animation should be used. Improves performance.");
        add(p + "fast_assets", "Whether simple gun model and texture should be used. Improves performance.");
        add(p + "fire", "Whether gun's fire animation should be used. Improves performance.");
        add(p + "green", "Green Value: 0 ~ 255");
        add(p + "gui_fast_assets", "Whether simple gun model and texture should be used for GUI rendering. Improves performance.");
        add(p + "hit_effect", "Entity Effect - Whether an icon should be rendered upon enemy hit.");
        add(p + "jump", "Whether gun's jump animation should be used. Improves performance.");
        add(p + "kill_effect", "Entity Effect - Whether an icon should be rendered upon enemy kill.");
        add(p + "kill_tip", "Whether kill feed/tip is enabled.");
        add(p + "lens_tint", "Gun Model Effect - Whether tint on gun lens should be used. Disable if conflicts with shaders.");
        add(p + "line_length", "Line Length");
        add(p + "line_thickness", "Line Thickness");
        add(p + "magnif", "ADS Sensitivity (%d)");
        add(p + "offset_scale", "Offset Scale");
        add(p + "passive_radio", "Whether passive radio voice effect is enabled.");
        add(p + "ping_hotkey", "Whether pinging hotkeys are enabled.");
        add(p + "player_model", "Whether A.V.A customized player model should be used, turn off if bugged with other mods installed.");
        add(p + "preset_hotkey", "Whether preset hotkeys are enabled.");
        add(p + "projectile", "Whether projectile indicator is enabled.");
        add(p + "projectile_trail", "Environment Effect - Whether projectile trails should be showed.");
        add(p + "quick_swap_hotkey", "Whether quick swap hotkeys are enabled.");
        add(p + "radio_hotkey", "Whether radio hotkeys are enabled.");
        add(p + "red", "Red Value: 0 ~ 255");
        add(p + "reject_image", "Reject image sent from the server, disable if you have bad connection or you don't trust the server.");
        add(p + "reload", "Whether gun's reload animation should be used. Improves performance.");
        add(p + "restore", "Restore the changes to default");
        add(p + "run", "Whether gun's run animation should be used. Improves performance.");
        add(p + "save", "Save the changes");
        add(p + "select", "Select the loadout (hotkey: %1$s)");
        add(p + "silencer_install", "Whether gun's silencer installation animation should be used. Improves performance.");
        add(p + "title_ai_fast_assets", "Mob Fast Assets");
        add(p + "title_ally_status", "Ally Status");
        add(p + "title_armour", "Fancy Armour");
        add(p + "title_bio", "Bio Indicator");
        add(p + "title_blue", "Blue Value: 0 ~ 255");
        add(p + "title_blood", "Blood Effect");
        add(p + "title_bobbing", "Bobbing Animation");
        add(p + "title_bullet_hole", "Bullet Hole Effect");
        add(p + "title_bullet_trail", "Bullet Trail Effect");
        add(p + "title_centre_dot_size", "Centre Dot Size");
        add(p + "title_creature_status", "Creature Status");
        add(p + "title_crosshair", "Crosshair");
        add(p + "title_damage_tilt", "Damage Tilt");
        add(p + "title_draw", "Draw Animation");
        add(p + "title_fast_assets", "Fast Assets");
        add(p + "title_fire", "Fire Animation");
        add(p + "title_green", "Green Value: 0 ~ 255");
        add(p + "title_gui_fast_assets", "GUI Fast Assets");
        add(p + "title_hit_effect", "Hit Effect");
        add(p + "title_jump", "Jump Animation");
        add(p + "title_kill_effect", "Kill Effect");
        add(p + "title_kill_tip", "Kill Feed");
        add(p + "title_lens_tint", "Lens Tint Effect");
        add(p + "title_line_length", "Line Length");
        add(p + "title_line_thickness", "Line Thickness");
        add(p + "title_magnif", "ADS Sensitivity");
        add(p + "title_offset_scale", "Offset Scale");
        add(p + "title_passive_radio", "Passive Radio");
        add(p + "title_ping_hotkey", "Ping");
        add(p + "title_player_model", "Player Model");
        add(p + "title_preset_hotkey", "Preset");
        add(p + "title_projectile", "Projectile Indicator");
        add(p + "title_projectile_trail", "Projectile Trail Effect");
        add(p + "title_quick_swap_hotkey", "Quick Swap");
        add(p + "title_radio_hotkey", "Radio");
        add(p + "title_red", "Red Value: 0 ~ 255");
        add(p + "title_reject_image", "Reject Image");
        add(p + "title_reload", "Reload Animation");
        add(p + "title_restore", "Restore the changes to default");
        add(p + "title_run", "Run Animation");
        add(p + "title_save", "Save the changes");
        add(p + "title_select", "Select the loadout (hotkey: %1$s)");
        add(p + "title_silencer_install", "Silencer Installation Animation");
        add(p + "title_transparent", "Transparency Value: 0 ~ 100");
        add(p + "transparent", "Transparency Value: 0 ~ 100");

        // JEI
        p = AVA.MODID + ".gui.jei.";
        add(p + "builders", "A.V.A Builders");
        add(p + "colouring", "A.V.A Colouring");
        add(p + "crafting", "A.V.A Crafting");
        add(p + "stats", "A.V.A Gun Stats");
    }

    protected void addCommonPhraseTranslations()
    {
        String p = AVA.MODID + ".common.";
        add(p + "n_players", "%d Players");
        add(p + "seconds", "Seconds");
    }

    protected void addInteractableTips()
    {
        String p = AVA.MODID + ".interaction.";
        add(p + "leopard", "Press [%s] to operate the Leopard2A5");
        add(p + "leopard_repair", "Press [%s] to repair the Leopard2A5");
        add(p + "panel", "Press [%s] to operate the control panel!!!");
        add(p + "rpg_box", "Press [%s] to open weapon box");
        add(p + "smart_entity", "Press [%s] to let the soldier follow you");
    }

    protected void addBroadCastTranslations()
    {
        String p = AVA.MODID + ".broadcast.";

        //Escord
        add(p + "broke_defense_line_eu", "Our tank broke through enemy's line of defense! Forward!");
        add(p + "broke_defense_line_nrf", "Enemy tank broke through our line of defense! Stop it!");
        add(p + "tank_damaged_eu", "Tank Damaged! Repair it to keep it moving again!");
        add(p + "tank_damaged_nrf", "%s has destroyed the enemy tank!");
        add(p + "tank_repaired_eu", "%s has repaired the tank!");
        add(p + "tank_repaired_nrf", "Enemy tank is moving again! Destroy it!");
        add(p + "tank_destroy", "Stop the enemy tank! Destroy it with RPG-7s!");
        add(p + "tank_stopped", "Tank stopped! Escort the tank!");
        add(p + "uav_support_start_eu", "Friendly UAV has started the reconnaissance support!");
        add(p + "uav_support_start_nrf", "The enemy UAV has started the reconnaissance support!");

        // Demolition
        add(p + "friendly_charge_set", "%1$s has planted the bomb (C4).");
        add(p + "enemy_charge_set", "Enemy troops have planted the bomb (C4).");
        add(p + "friendly_target_destroyed", "Failed to stop the bombing!");
        add(p + "enemy_target_destroyed", "Target successfully bombed!");
        add(p + "friendly_charge_defused", "Enemy troops have defused the bomb (C4).");
        add(p + "enemy_charge_defused", "Friendly troops have defused the bomb (C4).");

        // Common
        add(p + "preparing", "Waiting...");
        add(p + "times_up", "Mission time out!");
        add(p + "friendly_troops_win", "Friendly troops win!");
        add(p + "enemy_troops_win", "Enemy troops win!");
        add(p + "friendly_troops_eliminated", "Friendly troops have been eliminated!");
        add(p + "enemy_troops_eliminated", "Enemy troops have been eliminated!");
    }

    protected void addMasteryTranslations()
    {
        // Tasks
        String p = AVA.MODID + ".mastery.task.";
        add(p + "title.annihilator", "Task: Annihilator");
        add(p + "description.annihilator", "Kill %2$s %1$s (%3$s/%2$s)");
        add(p + "title.bleeder", "Task: Bleeder");
        add(p + "description.bleeder", "Deal %1$s damage (%2$s/%1$s)");
        add(p + "title.grim_reaper", "Task: Grim Reaper");
        add(p + "description.grim_reaper", "Kill %1$s entities (%2$s/%1$s)");
        add(p + "title.headless", "Task: Headless");
        add(p + "description.headless", "Headshot %1$s times (%2$s/%1$s)");
        add(p + "title.ranger", "Task: Ranger");
        add(p + "description.ranger", "Hit %1$s times (%2$s/%1$s)");

        // Tpye - Skills
        p = AVA.MODID + ".mastery.";
        add(p + "title.medic", "Mastery - Medic %s");
        add(p + "description.medic", "Medic Mastery - Self-defense and weakens the enemies");
        add(p + "skill.medic", "25% Chance to float.");
        add(p + "skill.medic_2", "75% Chance getting health boost.");
        add(p + "title.scout", "Mastery - Scout %s");
        add(p + "description.scout", "Scout Mastery - High mobility and assist allies to target the enemies");
        add(p + "skill.scout", "75% Chance give self a temporary jump boost.");
        add(p + "title.sniper", "Mastery - Sniper %s");
        add(p + "description.sniper", "Sniper Mastery - High lethality and prevents the enemies from escaping");
        add(p + "skill.sniper", "Deal extra damage if enemy has <= 50% hp.");
        add(p + "skill.sniper_2", "Gain Night Vision if enemy has > 50% hp");
        add(p + "title.worrier", "Mastery - Warrior %s");
        add(p + "description.worrier", "Warrior Mastery - High sustainability and self-defense");
        add(p + "skill.worrier", "Heals self. Doubled if hp <= 50%");
        add(p + "level_up", "You just achieved the new level: %1$s on %2$s");
    }

    protected void addDeprecatedTranslations()
    {
        add("ava.weather.start", "The storm is coming.");
        add("ava.weather.end", "The storm has gone.");
    }

    protected void addAttachmentTranslations()
    {
        for (GunAttachmentTypes att : GunAttachmentTypes.values())
            add(att.translationKey, getName(att.name()));
    }

    protected void addChatMessageTranslations()
    {
        String p = AVA.MODID + ".chat.";

        // Blocks
        add(p + "main_hand_empty", "Your main hand is empty!");
        add(p + "mastery_disabled", "Mastery System is not valid on this server (disabled)");
        add(p + "not_holding_gun", "You are not holding a gun");

        // Commands
        add(p + "boost_changed", "Your %s boost has been changed by %d, currently on level %d");
        add(p + "boost_set", "Your %s boost has been set to %d, currently on level %d");
        add(p + "crosshair", "Show Crosshair: %s");
        add(p + "current_play_mode", "Current Play Mode: %s");
        add(p + "deployed_ai", "Deployed %d roled entities at %s");
        add(p + "deployed_mob", "Deployed %d entities at %s");
        add(p + "deployed_team", "Deployed %d entities at their team spawn");
        add(p + "do_glass_break", "Bullet Destroys Glass is now: %s");
        add(p + "friendly_fire", "Friendly Fire is now: %s");
        add(p + "max_entity_count", "Set max entity count of each type to %d");
        add(p + "mob_drop_kit_chance", "Set mob drop kit chance to %d");
        add(p + "recoil_type", "Recoil refund type is now: %s");
        add(p + "reduced_friendly_fire", "Reduced friendly fire is now: %s");
        add(p + "site_height", "Set site %s height to %s");
        add(p + "site_radius", "Set site %s radius to %s");
        add(p + "teamspawn_added", "Set %s's teamspawn to %s with radius %s");
        add(p + "teamspawn_removed", "Removed all %s's teamspawns");
        add(p + "timer_empty", "No Timer Present!");

        // Entities
        add(p + "entity_following_leader", "[Following %s]");
        add(p + "entity_now_following_leader", "%1$s is now following %2$s");

        // Items
        add(p + "c4_plant_restricted", "You can only plant c4 near the bomb site!");
        add(p + "binocular_captured", "%s has marked the enemy %s at %s");

        // Game
        add(p + "c4_found", "%s found the bomb at %s");
        add(p + "weapon_damage_test_damage", "Damage: %.2f");
        add(p + "weapon_damage_test_weapon", "Weapon: %s");
        add(p + "weapon_disabled", "Current weapon is disabled on this server");
        add(p + "weapon_disabled_bug", "If this is not intended, it may caused by a legacy bug. Use the command %s");
    }

    protected void addGunStatTypeTranslations()
    {
        for (GunStatTypes value : GunStatTypes.values())
            add(value.getKey(), getName(value.name()));
    }

    protected void addRadioTranslations()
    {
        String p = AVA.MODID + ".radio.";
        add(p + "z1", "Go go go!");
        add(p + "z2", "Wait! Everyone stop!");
        add(p + "z3", "Enemy spotted!");
        add(p + "z4", "Behind us!");
        add(p + "z5", "Request backup!");
        add(p + "z6", "Cover me!");
        add(p + "z7", "Area clear!");
        add(p + "z8", "Everyone, fall back!");
        add(p + "z9", "Follow me! I will lead!");
        add(p + "z0", "Cancel");

        add(p + "x1", "Roger that");
        add(p + "x2", "Negative");
        add(p + "x3", "On my way");
        add(p + "x4", "Sorry");
        add(p + "x5", "Nice!");
        add(p + "x6", "Thanks!");
        add(p + "x7", "Get out of my way!");
        add(p + "x8", "I am ready");
        add(p + "x9", "[REVEAL] Taunt Enemies");
        add(p + "x0", "Cancel");

        //        add("ava.radio.c1", "Request primary objective");
        //        add("ava.radio.c2", "Request recon");
        //        add("ava.radio.c3", "Follow leader's order");
        //        add("ava.radio.c4", "Enemy on behind!");
        //        add("ava.radio.c5", "Objective first");
        //        add("ava.radio.c6", "Eliminate first");
        //        add("ava.radio.c7", "Plant the bomb!");
        //        add("ava.radio.c8", "C4 down!");
        //        add("ava.radio.c9", "Enemies have the C4");
        //        add("ava.radio.c0", "Cancel");
    }

    protected void addKeyBindingTranslations()
    {
        String p = AVA.MODID + ".keybindings.";
        add(p + "interaction", "Interaction");
        add(p + "install_silencer", "Install Silencer");
        add(p + "night_vision_device_switch", "Night Vision Device Switch");
        add(p + "ping", "Ping");
        add(p + "preset_f1", "Select Preset 1");
        add(p + "preset_f2", "Select Preset 2");
        add(p + "preset_f3", "Select Preset 3");
        add(p + "quick_swap", "Quick Swap");
        add(p + "radio_1", "Radio 1");
        add(p + "radio_2", "Radio 2");
        add(p + "radio_3", "Radio 3");
        add(p + "reload", "Reload");
        add(p + "tab", "Scoreboard");
    }

    @Override
    protected void addTranslations()
    {
        addAttachmentTranslations();
        addBlockTranslations();
        addBroadCastTranslations();
        addChatMessageTranslations();
        addCommonPhraseTranslations();
        addCreativeTabTranslations();
        addDeathMessageTranslations();
        addDeprecatedTranslations();
        addEntityTranslations();
        addGUITranslations();
        addGunStatTypeTranslations();
        addHUDTranslations();
        addInteractableTips();
        addItemTranslations();
        addKeyBindingTranslations();
        addMasteryTranslations();
        addRadioTranslations();
        addSoundSubtitleTranslations();
    }

    @Override
    public void add(String key, String value)
    {
        try
        {
            if (map.containsKey(key) && map.get(key).equals(value))
                AVA.LOGGER.error("Duplicate translation value! -> " + key + ": " + value);
            map.put(key, value);
            super.add(key, value);
        }
        catch (Exception e)
        {
        }
    }
}
