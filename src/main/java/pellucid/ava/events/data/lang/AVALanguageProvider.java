package pellucid.ava.events.data.lang;

import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.common.data.LanguageProvider;
import pellucid.ava.AVA;
import pellucid.ava.util.AVACommonUtil;

public abstract class AVALanguageProvider extends LanguageProvider
{
    public AVALanguageProvider(PackOutput gen, String locale)
    {
        super(gen, AVA.MODID, locale);
    }

    protected String getName(ResourceLocation location)
    {
        return getName(location.getPath());
    }
    protected String getName(String name)
    {
        return AVACommonUtil.toDisplayString(name);
    }
}
