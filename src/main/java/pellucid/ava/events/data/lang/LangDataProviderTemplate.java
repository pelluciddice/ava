package pellucid.ava.events.data.lang;

import net.minecraft.data.PackOutput;

/**
 * Translations by <user>
 * <link to profile>
 */
public class LangDataProviderTemplate extends LangDataProviderENUS
{
    public LangDataProviderTemplate(PackOutput gen)
    {
        super(gen, "template");
    }

    @Override
    protected void addTranslations()
    {
        super.addTranslations();
    }
}
