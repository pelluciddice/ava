package pellucid.ava.events.forge;

import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.neoforged.bus.api.Event;
import net.neoforged.bus.api.ICancellableEvent;
import pellucid.ava.entities.scanhits.BulletEntity;

public class AVABulletHitEvent<R extends HitResult> extends Event
{
    private final R Hit;
    private final BulletEntity bullet;

    private AVABulletHitEvent(R Hit, BulletEntity bullet)
    {
        this.Hit = Hit;
        this.bullet = bullet;
    }

    public R getHit()
    {
        return Hit;
    }

    public BulletEntity getBullet()
    {
        return bullet;
    }

    public static class Entity extends AVABulletHitEvent<EntityHitResult>
    {
        private Entity(EntityHitResult Hit, BulletEntity bullet)
        {
            super(Hit, bullet);
        }

        public static class Pre extends Entity implements ICancellableEvent
        {
            public Pre(EntityHitResult Hit, BulletEntity bullet)
            {
                super(Hit, bullet);
            }
        }

        public static class Post extends Entity
        {
            public Post(EntityHitResult Hit, BulletEntity bullet)
            {
                super(Hit, bullet);
            }
        }
    }

    public static class Block extends AVABulletHitEvent<BlockHitResult>
    {
        private Block(BlockHitResult Hit, BulletEntity bullet)
        {
            super(Hit, bullet);
        }

        public static class Pre extends Block implements ICancellableEvent
        {
            public Pre(BlockHitResult Hit, BulletEntity bullet)
            {
                super(Hit, bullet);
            }
        }

        public static class Post extends Block
        {
            public Post(BlockHitResult Hit, BulletEntity bullet)
            {
                super(Hit, bullet);
            }
        }
    }
}
