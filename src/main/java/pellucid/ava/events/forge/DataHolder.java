package pellucid.ava.events.forge;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import pellucid.ava.util.INBTSerializable;
import pellucid.ava.util.Pair;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class DataHolder<T> implements Supplier<T>, Consumer<T>, INBTSerializable<CompoundTag>
{
    private final String name;
    private final BiConsumer<DataHolder<T>, CompoundTag> writer;
    private final BiConsumer<DataHolder<T>, CompoundTag> reader;
    private final BiConsumer<DataHolder<T>, Level> ticker;
    private final boolean persistent;
    private T t;

    public static <T> Pair<String, Supplier<DataHolder<T>>> newDataHolder(String name, T t, BiConsumer<DataHolder<T>, CompoundTag> writer, BiConsumer<DataHolder<T>, CompoundTag> reader, boolean persistent)
    {
        return newDataHolder(name, t, writer, reader, null, persistent);
    }

    public static <T> Pair<String, Supplier<DataHolder<T>>> newDataHolder(String name, T t, BiConsumer<DataHolder<T>, CompoundTag> writer, BiConsumer<DataHolder<T>, CompoundTag> reader, BiConsumer<DataHolder<T>, Level> ticker, boolean persistent)
    {
        return Pair.of(name, () -> new DataHolder<>(name, t, writer, reader, ticker, persistent));
    }

    private DataHolder(String name, T t, BiConsumer<DataHolder<T>, CompoundTag> writer, BiConsumer<DataHolder<T>, CompoundTag> reader, BiConsumer<DataHolder<T>, Level> ticker, boolean persistent)
    {
        this.name = name;
        this.writer = writer;
        this.reader = reader;
        this.ticker = ticker;
        this.persistent = persistent;
        accept(t);
    }

    public void tick(Level world)
    {
        if (ticker != null)
            ticker.accept(this, world);
    }

    public String getName()
    {
        return name;
    }

    public boolean isPersistent()
    {
        return persistent;
    }

    @Override
    public void accept(T t)
    {
        this.t = t;
    }

    @Override
    public T get()
    {
        return t;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        writer.accept(this, compound);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        reader.accept(this, nbt);
    }
}
