package pellucid.ava.events.forge;

import net.minecraft.world.level.Level;
import net.neoforged.bus.api.Event;
import net.neoforged.neoforge.common.NeoForge;
import pellucid.ava.cap.AVAWorldData;

public class AVAWorldDataEvent extends Event
{
    public final Level world;

    private AVAWorldDataEvent(Level world)
    {
        this.world = world;
    }

    public static void onWorldDataPopulate(Level world, AVAWorldData data)
    {
        NeoForge.EVENT_BUS.post(new World.Creation(world, data));
    }

    public static class World extends AVAWorldDataEvent
    {
        public final AVAWorldData data;
        private World(Level world, AVAWorldData data)
        {
            super(world);
            this.data = data;
        }

        public static class Creation extends World
        {
            public Creation(Level world, AVAWorldData data)
            {
                super(world, data);
            }
        }
    }
}
