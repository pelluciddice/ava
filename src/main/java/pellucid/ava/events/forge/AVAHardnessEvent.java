package pellucid.ava.events.forge;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.neoforged.bus.api.Event;
import pellucid.ava.entities.scanhits.BulletEntity;

public class AVAHardnessEvent<R extends HitResult> extends Event
{
    private final R result;
    private final BulletEntity bullet;
    private float value;

    private AVAHardnessEvent(R result, BulletEntity bullet, float value)
    {
        this.result = result;
        this.bullet = bullet;
        this.value = value;
    }

    public R getHitResult()
    {
        return result;
    }

    public BulletEntity getBullet()
    {
        return bullet;
    }

    public float getValue()
    {
        return value;
    }

    public void setValue(float value)
    {
        this.value = value;
    }

    public static class Entity extends AVAHardnessEvent<EntityHitResult>
    {
        private final net.minecraft.world.entity.Entity entity;
        public Entity(EntityHitResult result, BulletEntity bullet, float defaultValue, net.minecraft.world.entity.Entity entity)
        {
            super(result, bullet, defaultValue);
            this.entity = entity;
        }

        public net.minecraft.world.entity.Entity getEntity()
        {
            return entity;
        }
    }

    public static class Block extends AVAHardnessEvent<BlockHitResult>
    {
        private final BlockState block;
        public Block(BlockHitResult result, BulletEntity bullet, float defaultValue, BlockState block)
        {
            super(result, bullet, defaultValue);
            this.block = block;
        }

        public BlockState getBlockState()
        {
            return block;
        }
    }
}
