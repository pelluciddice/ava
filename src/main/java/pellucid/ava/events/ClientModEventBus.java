package pellucid.ava.events;

import com.mojang.blaze3d.platform.InputConstants;
import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BiomeColors;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.block.Block;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;
import net.neoforged.neoforge.client.event.RegisterClientReloadListenersEvent;
import net.neoforged.neoforge.client.event.RegisterColorHandlersEvent;
import net.neoforged.neoforge.client.event.RegisterGuiLayersEvent;
import net.neoforged.neoforge.client.event.RegisterKeyMappingsEvent;
import net.neoforged.neoforge.client.event.RegisterMenuScreensEvent;
import net.neoforged.neoforge.client.settings.KeyConflictContext;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.blocks.AVATEContainers;
import pellucid.ava.blocks.colouring_table.GunColouringGUI;
import pellucid.ava.blocks.crafting_table.GunCraftingGUI;
import pellucid.ava.blocks.modifying_table.GunModifyingGUI;
import pellucid.ava.blocks.preset_table.PresetTableTER;
import pellucid.ava.blocks.rpg_box.RPGBoxTER;
import pellucid.ava.client.inputs.ForceKeyMapping;
import pellucid.ava.client.renderers.AVAModelLayers;
import pellucid.ava.client.renderers.AVARenderer;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.competitive_mode.CompetitiveModeClient;
import pellucid.ava.competitive_mode.CompetitiveUI;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.entities.base.EntityObjectRenderer;
import pellucid.ava.entities.livings.renderers.guard.GuardRenderer;
import pellucid.ava.entities.livings.renderers.sodier.SoldierRenderer;
import pellucid.ava.entities.objects.c4.C4Renderer;
import pellucid.ava.entities.objects.item.GunItemEntityRenderer;
import pellucid.ava.entities.objects.kits.renderers.KitRenderer;
import pellucid.ava.entities.objects.leopard.LeopardEntityRenderer;
import pellucid.ava.entities.robots.renderers.BlueRobotModel;
import pellucid.ava.entities.robots.renderers.DarkBlueRobotModel;
import pellucid.ava.entities.robots.renderers.RobotRenderer;
import pellucid.ava.entities.robots.renderers.YellowRobotRenderer;
import pellucid.ava.entities.scanhits.renderers.EmptyRenderer;
import pellucid.ava.entities.shootables.renderers.M202RocketModel;
import pellucid.ava.entities.throwables.GrenadeEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.entities.throwables.SmokeGrenadeEntity;
import pellucid.ava.entities.throwables.renderers.GM94GrenadeModel;
import pellucid.ava.entities.throwables.renderers.ObjectRenderer;
import pellucid.ava.fluids.AVAFluids;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesClientManager;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.weapon_chest.WeaponChestGUI;

import java.io.IOException;
import java.nio.file.Path;
import java.util.function.Supplier;

@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientModEventBus
{
    private static final ResourceLocation M67_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/m67.png");
    private static final ResourceLocation MK3A2_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/mk3a2.png");
    private static final ResourceLocation M116A1_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/m116a1.png");
    private static final ResourceLocation ROCKET_TEXTURE = new ResourceLocation(AVA.MODID, "textures/item/gun_textures/palette_forest.png");
    private static final ResourceLocation GRENADE_TEXTURE = new ResourceLocation(AVA.MODID, "textures/entities/gm94_grenade.png");

    private static final String KEY_BINDING_CATEGORY = "Alliance of Valiant Arms Control";
    public static final KeyMapping RELOAD = new KeyMapping(Component.translatable("ava.keybindings.reload").getString(), GLFW.GLFW_KEY_R, KEY_BINDING_CATEGORY);
    public static final KeyMapping NIGHT_VISION_DEVICE_SWITCH = new KeyMapping(Component.translatable("ava.keybindings.night_vision_device_switch").getString(), GLFW.GLFW_KEY_N, KEY_BINDING_CATEGORY);
    public static final ForceKeyMapping QUICK_SWAP = new ForceKeyMapping(Component.translatable("ava.keybindings.quick_swap").getString(), GLFW.GLFW_KEY_Q, KEY_BINDING_CATEGORY, () -> AVAClientConfig.ENABLE_QUICK_SWAP_HOTKEY.get() ? Minecraft.getInstance().options.keyDrop : null);
    public static final KeyMapping PING = new KeyMapping(Component.translatable("ava.keybindings.ping").getString(), KeyConflictContext.IN_GAME, InputConstants.Type.MOUSE, GLFW.GLFW_MOUSE_BUTTON_MIDDLE, KEY_BINDING_CATEGORY);

    public static final KeyMapping PRESET_F1 = new KeyMapping(Component.translatable("ava.keybindings.preset_f1").getString(), GLFW.GLFW_KEY_F6, KEY_BINDING_CATEGORY);
    public static final KeyMapping PRESET_F2 = new KeyMapping(Component.translatable("ava.keybindings.preset_f2").getString(), GLFW.GLFW_KEY_F7, KEY_BINDING_CATEGORY);
    public static final KeyMapping PRESET_F3 = new KeyMapping(Component.translatable("ava.keybindings.preset_f3").getString(), GLFW.GLFW_KEY_F8, KEY_BINDING_CATEGORY);
    public static final KeyMapping RADIO_1 = new KeyMapping(Component.translatable("ava.keybindings.radio_1").getString(), GLFW.GLFW_KEY_Z, KEY_BINDING_CATEGORY);
    public static final KeyMapping RADIO_2 = new KeyMapping(Component.translatable("ava.keybindings.radio_2").getString(), GLFW.GLFW_KEY_X, KEY_BINDING_CATEGORY);

    public static final KeyMapping INSTALL_SILENCER = new KeyMapping(Component.translatable("ava.keybindings.install_silencer").getString(), GLFW.GLFW_KEY_V, KEY_BINDING_CATEGORY);

    public static final ForceKeyMapping TAB = new ForceKeyMapping(Component.translatable("ava.keybindings.tab").getString(), GLFW.GLFW_KEY_TAB, KEY_BINDING_CATEGORY, () -> AVAClientConfig.ENABLE_TAB_HOTKEY.get() && AVAServerConfig.isCompetitiveModeActivated() ? Minecraft.getInstance().options.keyPlayerList : null);

    @SubscribeEvent
    public static void blockColours(RegisterColorHandlersEvent.Block event)
    {
        event.register((state, tint, pos, c) -> {
            return tint != null && pos != null ? BiomeColors.getAverageWaterColor(tint, pos) : -1;
        }, AVABlocks.VOID_WATER.get());
    }

    @SubscribeEvent
    public static void onClientReloadListenersRegister(RegisterClientReloadListenersEvent event)
    {
        AVAModelTypes.TYPES.forEach((t) -> event.registerReloadListener(t.getModelManager()));
    }

    @SubscribeEvent
    public static void addLayerDefs(EntityRenderersEvent.RegisterLayerDefinitions event)
    {
        AVAModelLayers.registerAll(event);
    }

    private static EntityRenderersEvent.RegisterRenderers RENDERER_REGISTRY;
    private static <T extends Entity> void registerRenderer(Supplier<EntityType<Entity>> type, EntityRendererProvider constructor)
    {
        RENDERER_REGISTRY.registerEntityRenderer(type.get(), constructor);
    }

    private static <T extends Entity> void registerRenderer2(Supplier<EntityType<T>> type, EntityRendererProvider<T> constructor)
    {
        RENDERER_REGISTRY.registerEntityRenderer(type.get(), constructor);
    }

    @SubscribeEvent
    public static void onRenderersRegistered(EntityRenderersEvent.RegisterRenderers event)
    {
        RENDERER_REGISTRY = event;
        registerRenderer(AVAEntities.BULLET, EmptyRenderer::new);
        registerRenderer(AVAEntities.MELEE_RAYTRACING, EmptyRenderer::new);
        registerRenderer(AVAEntities.PLAIN_SMOKE, EmptyRenderer::new);
        registerRenderer(AVAEntities.M67, (manager) -> new EntityObjectRenderer<>(manager, HandGrenadeEntity::getWeapon));
        registerRenderer(AVAEntities.MK3A2, (manager) -> new EntityObjectRenderer<>(manager, Projectiles.MK3A2));
        registerRenderer(AVAEntities.M116A1, (manager) -> new EntityObjectRenderer<>(manager, Projectiles.M116A1));
        registerRenderer(AVAEntities.M18, (manager) -> new EntityObjectRenderer<>(manager, SmokeGrenadeEntity::getGrenade));
        registerRenderer(AVAEntities.M18_TOXIC, (manager) -> new EntityObjectRenderer<>(manager, Projectiles.M18_TOXIC));
        registerRenderer(AVAEntities.AMMO_KIT, KitRenderer::new);
        registerRenderer(AVAEntities.FIRST_AID_KIT, KitRenderer::new);
        registerRenderer(AVAEntities.ROCKET, (manager) -> new ObjectRenderer<HandGrenadeEntity>(manager, new M202RocketModel(manager.bakeLayer(AVAModelLayers.ROCKET)), ROCKET_TEXTURE, (stack) -> {
            stack.scale(0.2F, 0.2F, 0.2F);
            stack.translate(0.0F, 0.25F, 0.0F);
        }));
        registerRenderer(AVAEntities.GRENADE, (manager) -> new ObjectRenderer<GrenadeEntity>(manager, new GM94GrenadeModel(manager.bakeLayer(AVAModelLayers.GM94_GRENADE)), GRENADE_TEXTURE, (stack) -> {
            stack.translate(0.0F, -1F, 0.0F);
        }));
        registerRenderer(AVAEntities.C4, C4Renderer::new);
        registerRenderer2(AVAEntities.BLUE_MELEE_GUARD, GuardRenderer::new);
        registerRenderer2(AVAEntities.SHOTGUN_GUARD, GuardRenderer::new);
        registerRenderer2(AVAEntities.RIFLE_GUARD, GuardRenderer::new);
        registerRenderer2(AVAEntities.GRENADE_LAUNCHER_GUARD, GuardRenderer::new);
        registerRenderer2(AVAEntities.PISTOL_GUARD, GuardRenderer::new);
        registerRenderer2(AVAEntities.TOXIC_SMOKE_GUARD, GuardRenderer::new);
        registerRenderer2(AVAEntities.GREY_MELEE_PRISONER, (manager) -> new GuardRenderer<>(manager, (stack) -> stack.scale(0.9F, 1.05F, 0.9F)));
        registerRenderer2(AVAEntities.YELLOW_MELEE_PRISONER, (manager) -> new GuardRenderer<>(manager, (stack) -> stack.scale(0.915F, 1.05F, 0.915F)));
        registerRenderer2(AVAEntities.RED_MELEE_PRISONER, (manager) -> new GuardRenderer<>(manager, (stack) -> stack.scale(1.05F, 1.2F, 1.05F)));
        registerRenderer2(AVAEntities.SHOTGUN_PRISONER, (manager) -> new GuardRenderer<>(manager, (stack) -> stack.scale(1.1F, 1.3F, 1.1F)));
        event.registerEntityRenderer(AVAEntities.EU_SOLDIER.get(), (manager) -> new SoldierRenderer(manager, (stack) -> stack.scale(1.0F, 1.0F, 1.0F)));
        event.registerEntityRenderer(AVAEntities.NRF_SOLDIER.get(), (manager) -> new SoldierRenderer(manager, (stack) -> stack.scale(1.0F, 1.0F, 1.0F)));
        event.registerEntityRenderer(AVAEntities.ROLED_SOLDIER.get(), (manager) -> new SoldierRenderer(manager, (stack) -> stack.scale(1.0F, 1.0F, 1.0F)));
        registerRenderer2(AVAEntities.GUN_ITEM, GunItemEntityRenderer::new);
        registerRenderer2(AVAEntities.LEOPARD_DESERT, (manager) -> new LeopardEntityRenderer(manager, "desert"));
        registerRenderer2(AVAEntities.LEOPARD_FOREST, (manager) -> new LeopardEntityRenderer(manager, "forest"));
        registerRenderer2(AVAEntities.LEOPARD_SNOW, (manager) -> new LeopardEntityRenderer(manager, "snow"));
        registerRenderer2(AVAEntities.BLUE_ROBOT, (manager) -> new RobotRenderer<>(manager, new BlueRobotModel(manager.bakeLayer(AVAModelLayers.BLUE_ROBOT)), RobotRenderer.BLUE));
        registerRenderer2(AVAEntities.YELLOW_ROBOT, YellowRobotRenderer::new);
        registerRenderer2(AVAEntities.DARK_BLUE_ROBOT, (manager) -> new RobotRenderer<>(manager, new DarkBlueRobotModel(manager.bakeLayer(AVAModelLayers.DARK_BLUE_ROBOT)), RobotRenderer.DARK_BLUE));

        event.registerBlockEntityRenderer(AVATEContainers.PRESET_TABLE_TE.get(), PresetTableTER::new);
        event.registerBlockEntityRenderer(AVATEContainers.RPG_BOX_TE.get(), RPGBoxTER::new);
    }

    @SubscribeEvent
    public static void registerKeyBinds(RegisterKeyMappingsEvent event)
    {
        event.register(RELOAD);
        event.register(NIGHT_VISION_DEVICE_SWITCH);
        event.register(QUICK_SWAP);
        event.register(PING);
        event.register(PRESET_F1);
        event.register(PRESET_F2);
        event.register(PRESET_F3);
        event.register(RADIO_1);
        event.register(RADIO_2);
        event.register(INSTALL_SILENCER);
    }

    @SubscribeEvent
    public static void registerOverlays(RegisterGuiLayersEvent event)
    {
        event.registerAboveAll(new ResourceLocation(AVA.MODID, "crosshair"), AVARenderer.CROSSHAIR_UI);
        event.registerAboveAll(new ResourceLocation(AVA.MODID, "hud_indicators"), AVARenderer.HUD_INDICATORS_UI);
        event.registerAboveAll(new ResourceLocation(AVA.MODID, "overlay"), AVARenderer.OVERLAY_UI);
        event.registerAboveAll(new ResourceLocation(AVA.MODID, "gamemode"), AVARenderer.GAMEMODE_UI);
        event.registerAboveAll(new ResourceLocation(AVA.MODID, "competitive"), CompetitiveUI.COMPETITIVE_UI);
    }

    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event) throws IOException
    {
        event.enqueueWork((() ->
        {
            ClientModEventBus.addAllItemModelsOverrides();
            ItemBlockRenderTypes.setRenderLayer(AVABlocks.VOID_WATER.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVAFluids.VOID_WATER.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVAFluids.FLOWING_VOID_WATER.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABlocks.BUILDERS_TABLE.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABuildingBlocks.GLASS_FENCE.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABuildingBlocks.GLASS_FENCE_TALL.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABuildingBlocks.GLASS_WALL.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABuildingBlocks.GLASS_TRIG_WALL.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABuildingBlocks.GLASS_TRIG_WALL_FLIPPED.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABlocks.TEST_BLOCK.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABlocks.PRESET_TABLE.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABuildingBlocks.HARDENED_IRON_BARS.get(), RenderType.cutoutMipped());
            for (Supplier<Block> block : AVABlocks.GLASS_BLOCKS)
                ItemBlockRenderTypes.setRenderLayer(block.get(), RenderType.translucent());
            for (Supplier<Block> block : AVABlocks.GLASS_PANE_BLOCKS)
                ItemBlockRenderTypes.setRenderLayer(block.get(), RenderType.translucent());
            ItemBlockRenderTypes.setRenderLayer(AVABlocks.REPAIRABLE_FLOWER_POT.get(), RenderType.cutout());
        }));
        CompetitiveModeClient.init();
        LoadingImagesClientManager.INSTANCE.init(Path.of(Minecraft.getInstance().gameDirectory.getAbsolutePath()));
    }

    @SubscribeEvent
    public static void registerMenus(RegisterMenuScreensEvent event)
    {
        event.register(AVATEContainers.GUN_CRAFTING_TABLE_CONTAINER.get(), GunCraftingGUI::new);
        event.register(AVATEContainers.GUN_COLOURING_TABLE_CONTAINER.get(), GunColouringGUI::new);
        event.register(AVATEContainers.GUN_MODIFYING_TABLE_CONTAINER.get(), GunModifyingGUI::new);
        event.register(AVATEContainers.WEAPON_CHEST_CONTAINER.get(), WeaponChestGUI::new);
    }

    private static void addAllItemModelsOverrides()
    {

    }
}
