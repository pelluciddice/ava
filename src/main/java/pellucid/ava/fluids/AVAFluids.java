package pellucid.ava.fluids;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.WaterFluid;
import net.neoforged.neoforge.common.NeoForgeMod;
import net.neoforged.neoforge.fluids.FluidType;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.items.init.MiscItems;

import java.util.function.Supplier;

public class AVAFluids
{
    public static final DeferredRegister<Fluid> FLUIDS = DeferredRegister.create(BuiltInRegistries.FLUID, AVA.MODID);
    public static final Supplier<FlowingFluid> FLOWING_VOID_WATER = FLUIDS.register("flowing_void_water", () -> new VoidWaterFluid.Flowing());
    public static final Supplier<FlowingFluid> VOID_WATER = FLUIDS.register("void_water", () -> new VoidWaterFluid.Source());

    static abstract class VoidWaterFluid extends WaterFluid
    {
        @Override
        public FluidType getFluidType()
        {
            return NeoForgeMod.WATER_TYPE.value();
        }

        @Override
        public Fluid getFlowing() {
            return FLOWING_VOID_WATER.get();
        }

        @Override
        public Fluid getSource() {
            return VOID_WATER.get();
        }

        @Override
        public Item getBucket() {
            return MiscItems.VOID_WATER_BUCKET.get();
        }

        @Override
        public BlockState createLegacyBlock(FluidState state)
        {
            return AVABlocks.VOID_WATER.get().defaultBlockState().setValue(LiquidBlock.LEVEL, getLegacyLevel(state));
        }

        @Override
        public boolean isSame(Fluid fluid)
        {
            return fluid == AVAFluids.VOID_WATER.get() || fluid == AVAFluids.FLOWING_VOID_WATER.get();
        }

        public static class Flowing extends VoidWaterFluid
        {
            @Override
            protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> p_76476_)
            {
                super.createFluidStateDefinition(p_76476_);
                p_76476_.add(LEVEL);
            }

            @Override
            public int getAmount(FluidState p_76480_)
            {
                return p_76480_.getValue(LEVEL);
            }

            @Override
            public boolean isSource(FluidState p_76478_)
            {
                return false;
            }
        }

        public static class Source extends VoidWaterFluid
        {
            @Override
            public int getAmount(FluidState p_76485_)
            {
                return 8;
            }

            @Override
            public boolean isSource(FluidState p_76483_)
            {
                return true;
            }
        }
    }
}
