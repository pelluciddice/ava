package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class ItemGunStat extends GunStat<Supplier<Item>>
{
    public static ItemGunStat item(Supplier<Item> value)
    {
        return item(() -> Items.AIR, value);
    }

    public static ItemGunStat item(Supplier<Item> defaultValue, Supplier<Item> value)
    {
        return new ItemGunStat(defaultValue, value);
    }

    private ItemGunStat(Supplier<Item> defaultValue, Supplier<Item> value)
    {
        super(defaultValue, value);
    }

    @Override
    public ItemGunStat copy()
    {
        return item(defaultValue, value);
    }

    @Override
    Pair<TriConsumer<CompoundTag, String, Supplier<Item>>, BiFunction<CompoundTag, String, Supplier<Item>>> createSerializer()
    {
        return Pair.of(DataTypes.ITEM_HOLDER::write, DataTypes.ITEM_HOLDER::read);
    }

    @Override
    Pair<TriConsumer<JsonObject, String, Supplier<Item>>, Function<JsonElement, Supplier<Item>>> createJsonSerializer()
    {
        return Pair.of(DataTypes.ITEM_HOLDER::write, DataTypes.ITEM_HOLDER::read);
    }

    @Override
    public Supplier<Item> calculate(Supplier<Item> origin)
    {
        return null;
    }

    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public String toDisplayValue()
    {
        return value.get().getDescription().getString();
    }
}
