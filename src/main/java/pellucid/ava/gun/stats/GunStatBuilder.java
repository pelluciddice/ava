package pellucid.ava.gun.stats;

import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.Pair;

import java.util.HashMap;
import java.util.Map;

import static pellucid.ava.gun.stats.BooleanGunStat.bool;
import static pellucid.ava.gun.stats.FloatPairGunStat.pair;
import static pellucid.ava.gun.stats.GunStatTypes.*;

public class GunStatBuilder
{
    private final Map<GunStatTypes, GunStat<?>> stats = new HashMap<>();


    public static GunStatBuilder of()
    {
        return new GunStatBuilder();
    }

    private GunStatBuilder()
    {
    }

    public <T extends GunStat<?>> GunStatBuilder put(GunStatTypes stat, T t)
    {
        if (stats.containsKey(stat))
            throw new IllegalArgumentException("Trying to put duplicated stat " + stat.getKey());
        stats.put(stat, t);
        t.type(stat);
        return this;
    }

    public GunStatBuilder capacity(int amount)
    {
        return put(CAPACITY, IntegerGunStat.integer(amount));
    }

    public GunStatBuilder range(float range)
    {
        return put(RANGE, FloatGunStat.floating(range));
    }

    public GunStatBuilder damage(float damage)
    {
        return put(ATTACK_DAMAGE, FloatGunStat.floating(damage));
    }

    public GunStatBuilder fireRate(float speed)
    {
        return put(FIRE_RATE, FloatGunStat.floating(speed));
    }

    public GunStatBuilder reloadTime(int time)
    {
        return put(RELOAD_TIME, IntegerGunStat.integer(time));
    }

    public GunStatBuilder preReloadTime(int time)
    {
        return put(PRE_RELOAD_TIME, IntegerGunStat.integer(time));
    }

    public GunStatBuilder postReloadTime(int time)
    {
        return put(POST_RELOAD_TIME, IntegerGunStat.integer(time));
    }

    public GunStatBuilder scopeType(AVAItemGun.IScopeType type)
    {
        return put(SCOPE_TYPE, ScopeGunStat.scope(type));
    }

    public GunStatBuilder manual()
    {
        return put(AUTOMATIC, bool(true, false));
    }

    public GunStatBuilder noCrosshair()
    {
        return put(CROSSHAIR, bool(true, false));
    }

    public GunStatBuilder requireAim()
    {
        return put(REQUIRE_AIM, bool(false, true));
    }

    public GunStatBuilder reloadInteractable()
    {
        return put(RELOAD_INTERACTABLE, bool(false, true));
    }

    public GunStatBuilder extraReloadSteps()
    {
        return put(EXTRA_RELOAD_STEPS, bool(false, true));
    }

    public GunStatBuilder mobility(float mobility)
    {
        return put(MOBILITY, FloatGunStat.floating(mobility));
    }

    public GunStatBuilder stability(float stability)
    {
        return stability(stability, stability, stability, stability, stability, stability, stability, stability);
    }

    public GunStatBuilder stability(float stability, float stabilityC, float stabilityM, float stabilityJ)
    {
        return stability(stability, stabilityC, stabilityM, stabilityJ, stability, stabilityC, stabilityM, stabilityJ);
    }

    public GunStatBuilder stability(float stability, float stabilityC, float stabilityM, float stabilityJ, float stabilityA, float stabilityAC, float stabilityAM, float stabilityAJ)
    {
        return put(STABILITY, FloatGunStat.floating(stability))
                .put(STABILITY_CROUCH, FloatGunStat.floating(stabilityC))
                .put(STABILITY_MOVING, FloatGunStat.floating(stabilityM))
                .put(STABILITY_JUMPING, FloatGunStat.floating(stabilityJ))
                .put(STABILITY_AIM, FloatGunStat.floating(stabilityA))
                .put(STABILITY_AIM_CROUCH, FloatGunStat.floating(stabilityAC))
                .put(STABILITY_AIM_MOVING, FloatGunStat.floating(stabilityAM))
                .put(STABILITY_AIM_JUMPING, FloatGunStat.floating(stabilityAJ))
                ;
    }

    public GunStatBuilder accuracy(float accuracy)
    {
        return accuracy(accuracy, accuracy, accuracy, accuracy, accuracy, accuracy, accuracy, accuracy);
    }

    public GunStatBuilder accuracy(float accuracy, float accuracyC, float accuracyM, float accuracyJ)
    {
        return accuracy(accuracy, accuracyC, accuracyM, accuracyJ, accuracy, accuracyC, accuracyM, accuracyJ);
    }

    public GunStatBuilder accuracy(float accuracy, float accuracyC, float accuracyM, float accuracyJ, float accuracyA, float accuracyAC, float accuracyAM, float accuracyAJ)
    {
        return put(ACCURACY, FloatGunStat.floating(accuracy))
                .put(ACCURACY_CROUCH, FloatGunStat.floating(accuracyC))
                .put(ACCURACY_MOVING, FloatGunStat.floating(accuracyM))
                .put(ACCURACY_JUMPING, FloatGunStat.floating(accuracyJ))
                .put(ACCURACY_AIM, FloatGunStat.floating(accuracyA))
                .put(ACCURACY_AIM_CROUCH, FloatGunStat.floating(accuracyAC))
                .put(ACCURACY_AIM_MOVING, FloatGunStat.floating(accuracyAM))
                .put(ACCURACY_AIM_JUMPING, FloatGunStat.floating(accuracyAJ))
                ;
    }

    public GunStatBuilder initialAccuracy(float accuracy)
    {
        return initialAccuracy(accuracy, accuracy, accuracy, accuracy, accuracy, accuracy, accuracy, accuracy);
    }

    public GunStatBuilder initialAccuracy(float accuracy, float accuracyC, float accuracyM, float accuracyJ)
    {
        return initialAccuracy(accuracy, accuracyC, accuracyM, accuracyJ, accuracy, accuracyC, accuracyM, accuracyJ);
    }

    public GunStatBuilder initialAccuracy(float accuracy, float accuracyC, float accuracyM, float accuracyJ, float accuracyA, float accuracyAC, float accuracyAM, float accuracyAJ)
    {
        return put(INITIAL_ACCURACY, FloatGunStat.floating(accuracy))
                .put(INITIAL_ACCURACY_CROUCH, FloatGunStat.floating(accuracyC))
                .put(INITIAL_ACCURACY_MOVING, FloatGunStat.floating(accuracyM))
                .put(INITIAL_ACCURACY_JUMPING, FloatGunStat.floating(accuracyJ))
                .put(INITIAL_ACCURACY_AIM, FloatGunStat.floating(accuracyA))
                .put(INITIAL_ACCURACY_AIM_CROUCH, FloatGunStat.floating(accuracyAC))
                .put(INITIAL_ACCURACY_AIM_MOVING, FloatGunStat.floating(accuracyAM))
                .put(INITIAL_ACCURACY_AIM_JUMPING, FloatGunStat.floating(accuracyAJ))
                ;
    }

    public GunStatBuilder silenced()
    {
        return put(SILENCED, bool(false, true));
    }

    public GunStatBuilder fireAnimationTime(int ticks)
    {
        return put(FIRE_ANIMATION_TIME, IntegerGunStat.integer(ticks));
    }

    public GunStatBuilder recoilCompensation(float value)
    {
        return put(RECOIL_COMPENSATION, FloatGunStat.floating(value));
    }

    public GunStatBuilder recoilCompensation(float recoil, float recoilC, float recoilM, float recoilJ)
    {
        return recoilCompensation(recoil, recoilC, recoilM, recoilJ, recoil, recoilC, recoilM, recoilJ);
    }

    public GunStatBuilder recoilCompensation(float recoil, float recoilC, float recoilM, float recoilJ, float recoilA, float recoilAC, float recoilAM, float recoilAJ)
    {
        return put(RECOIL_COMPENSATION, FloatGunStat.floating(recoil))
                .put(RECOIL_COMPENSATION_CROUCH, FloatGunStat.floating(recoilC))
                .put(RECOIL_COMPENSATION_MOVING, FloatGunStat.floating(recoilM))
                .put(RECOIL_COMPENSATION_JUMPING, FloatGunStat.floating(recoilJ))
                .put(RECOIL_COMPENSATION_AIM, FloatGunStat.floating(recoilA))
                .put(RECOIL_COMPENSATION_AIM_CROUCH, FloatGunStat.floating(recoilAC))
                .put(RECOIL_COMPENSATION_AIM_MOVING, FloatGunStat.floating(recoilAM))
                .put(RECOIL_COMPENSATION_AIM_JUMPING, FloatGunStat.floating(recoilAJ))
                ;
    }

    public GunStatBuilder drawSpeed(int drawSpeed)
    {
        return put(DRAW_TIME, IntegerGunStat.integer(drawSpeed));
    }

    public GunStatBuilder damageFloating(float value)
    {
        return damageFloating(-value, value);
    }

    public GunStatBuilder damageFloating(float min, float max)
    {
        return put(ATTACK_DAMAGE_FLOATING, pair(Pair.of(min, max - min)));
    }

    public GunStatBuilder penetration(float value)
    {
        return put(PENETRATION, FloatGunStat.floating(value));
    }

    public GunStatBuilder pellets(int amount)
    {
        return put(PELLETS, IntegerGunStat.integer(amount));
    }

    public GunStatBuilder aimTime(int aimTime)
    {
        return put(AIM_TIME, IntegerGunStat.integer(aimTime));
    }

    public GunStatBuilder optionalSilencer()
    {
        return put(OPTIONAL_SILENCER, bool(false, true));
    }

    public GunStatBuilder spreadMax(float spreadMax)
    {
        return put(SPREAD_MAX, FloatGunStat.floating(spreadMax));
    }

    public GunStatBuilder spreadFactor(float spreadFactor)
    {
        return put(SPREAD_FACTOR, FloatGunStat.floating(spreadFactor));
    }

    public GunStatBuilder spreadRecovery(float spreadRecovery)
    {
        return put(SPREAD_RECOVERY, FloatGunStat.floating(spreadRecovery));
    }

    public GunStatBuilder spreadRecoveryFactor(float spreadRecovery)
    {
        return put(SPREAD_RECOVERY_FACTOR, FloatGunStat.floating(spreadRecovery));
    }

    public GunStatBuilder trail()
    {
        return put(BULLET_TRAIL, bool(false, true));
    }

    public GunStatBuilder shakeFactor(float shakeFactor)
    {
        return put(SHAKE_FACTOR, FloatGunStat.floating(shakeFactor));
    }

    public GunStatBuilder shakeTurnChance(float shakeTurnChance)
    {
        return put(SHAKE_TURN_CHANCE, FloatGunStat.floating(shakeTurnChance));
    }

    public GunStatBuilder exitAimOnFire()
    {
        return put(EXIT_AIM_ON_FIRE, bool(false, true));
    }

    public Map<GunStatTypes, GunStat<?>> build()
    {
        return stats;
    }
}
