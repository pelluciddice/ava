package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.util.IJsonSerializable;
import pellucid.ava.util.INBTSerializable;
import pellucid.ava.util.Pair;

import java.util.List;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Function;

public abstract class GunStat<T> implements INBTSerializable<CompoundTag>, IJsonSerializable<GunStat<T>>
{
    private final Pair<TriConsumer<CompoundTag, String, T>, BiFunction<CompoundTag, String, T>> serializer = createSerializer();
    private final Pair<TriConsumer<JsonObject, String, T>, Function<JsonElement, T>> jsonSerializer = createJsonSerializer();
    protected GunStatTypes statType;
    protected T defaultValue;
    protected T value;

    public GunStat(T defaultValue, T value)
    {
        this.defaultValue = defaultValue;
        this.value = value;
    }

    public abstract GunStat<T> copy();

    abstract Pair<TriConsumer<CompoundTag, String, T>, BiFunction<CompoundTag, String, T>> createSerializer();
    abstract Pair<TriConsumer<JsonObject, String, T>, Function<JsonElement, T>> createJsonSerializer();

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        serializer.getA().accept(compound, "value", getValue());
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        value = serializer.getB().apply(nbt, "value");
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        jsonSerializer.getA().accept(json, statType.name().toLowerCase(Locale.ROOT), value);
    }

    @Override
    public void readFromJson(JsonElement json)
    {
        value = jsonSerializer.getB().apply(json);
    }

    public GunStat<T> in(List<GunStat<?>> list)
    {
        list.add(this);
        return this;
    }

    public void type(GunStatTypes type)
    {
        this.statType = type;
    }

    public abstract T calculate(T origin);

    public abstract boolean isEmpty();

    public T getDefaultValue()
    {
        return defaultValue;
    }

    public T getValue()
    {
        return value;
    }

    public abstract String toDisplayValue();

    @Override
    public String toString()
    {
        return isEmpty() ? "GunStat{Empty}" : "GunStat{" +
                "defaultValue=" + defaultValue +
                ", value=" + value +
                '}';
    }

}
