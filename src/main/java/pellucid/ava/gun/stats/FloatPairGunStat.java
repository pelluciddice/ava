package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import java.util.function.BiFunction;
import java.util.function.Function;

public class FloatPairGunStat extends GunStat<Pair<Float, Float>>
{
    public static FloatPairGunStat pair(Pair<Float, Float> pair)
    {
        return new FloatPairGunStat(Pair.of(0.0F, 0.0F), pair);
    }

    private FloatPairGunStat(Pair<Float, Float> defaultValue, Pair<Float, Float> value)
    {
        super(defaultValue, value);
    }

    @Override
    public FloatPairGunStat copy()
    {
        return pair(value);
    }

    @Override
    Pair<TriConsumer<CompoundTag, String, Pair<Float, Float>>, BiFunction<CompoundTag, String, Pair<Float, Float>>> createSerializer()
    {
        return Pair.of((compound, key, pair) -> {
            DataTypes.FLOAT.write(compound, "left", pair.getA());
            DataTypes.FLOAT.write(compound, "right", pair.getB());
        }, (compound, key) -> Pair.of(DataTypes.FLOAT.read(compound, "left"), DataTypes.FLOAT.read(compound, "right")));
    }

    @Override
    Pair<TriConsumer<JsonObject, String, Pair<Float, Float>>, Function<JsonElement, Pair<Float, Float>>> createJsonSerializer()
    {
        return Pair.of((json, key, pair) -> {
            JsonObject obj = new JsonObject();
            obj.addProperty("min", pair.getA());
            obj.addProperty("max", pair.getB());
            json.add(key, obj);
        }, (json) -> {
            JsonObject obj = json.getAsJsonObject();
            return Pair.of(obj.get("min").getAsFloat(), obj.get("max").getAsFloat());
        });
    }

    @Override
    public Pair<Float, Float> calculate(Pair<Float, Float> origin)
    {
        return null;
    }

    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public String toDisplayValue()
    {
        return "(" + AVACommonUtil.getDamageFloatingString(value) + ")";
    }
}
