package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import java.util.function.BiFunction;
import java.util.function.Function;

public class BooleanGunStat extends GunStat<Boolean>
{
    public static BooleanGunStat bool(boolean defaultValue, boolean bool)
    {
        return new BooleanGunStat(defaultValue, bool);
    }

    private BooleanGunStat(boolean defaultValue, boolean value)
    {
        super(defaultValue, value);
    }

    @Override
    public BooleanGunStat copy()
    {
        return bool(defaultValue, value);
    }

    @Override
    public Boolean calculate(Boolean origin)
    {
        return null;
    }

    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public String toDisplayValue()
    {
        return StringUtils.capitalize(value.toString());
    }

    @Override
    Pair<TriConsumer<CompoundTag, String, Boolean>, BiFunction<CompoundTag, String, Boolean>> createSerializer()
    {
        return Pair.of(DataTypes.BOOLEAN::write, DataTypes.BOOLEAN::read);
    }

    @Override
    Pair<TriConsumer<JsonObject, String, Boolean>, Function<JsonElement, Boolean>> createJsonSerializer()
    {
        return Pair.of(DataTypes.BOOLEAN::write, DataTypes.BOOLEAN::read);
    }

    @Override
    public void writeToJson(JsonObject json)
    {

    }

    @Override
    public void readFromJson(JsonElement json)
    {

    }
}
