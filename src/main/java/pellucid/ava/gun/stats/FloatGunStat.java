package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import java.util.function.BiFunction;
import java.util.function.Function;

public class FloatGunStat extends NumberGunStat<Float>
{
    public static FloatGunStat floating(float value)
    {
        return floating(0.0F, value);
    }

    public static FloatGunStat floating(float defaultValue, float value)
    {
        return floating(defaultValue, value, 0.0F);
    }

    public static FloatGunStat floating(float defaultValue, float value, float min)
    {
        return floating(defaultValue, value, min, Float.MAX_VALUE);
    }

    public static FloatGunStat floating(float defaultValue, float value, float min, float max)
    {
        if (Float.isNaN(value) || Float.isInfinite(value))
            value = min;
        return new FloatGunStat(defaultValue, value, min, max, Double::floatValue);
    }

    public FloatGunStat(Float defaultValue, Float value, Float min, Float max, Function<Double, Float> caster)
    {
        super(defaultValue, value, min, max, caster);
    }


    @Override
    public FloatGunStat copy()
    {
        return floating(defaultValue, value, min, max);
    }

    @Override
    Pair<TriConsumer<CompoundTag, String, Float>, BiFunction<CompoundTag, String, Float>> createSerializer()
    {
        return Pair.of(DataTypes.FLOAT::write, DataTypes.FLOAT::read);
    }

    @Override
    Pair<TriConsumer<JsonObject, String, Float>, Function<JsonElement, Float>> createJsonSerializer()
    {
        return Pair.of(DataTypes.FLOAT::write, DataTypes.FLOAT::read);
    }

    @Override
    public String toDisplayValue()
    {
        return String.valueOf(AVACommonUtil.round(value, 2));
    }
}
