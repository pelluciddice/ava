package pellucid.ava.gun.stats;

import pellucid.ava.util.AVACommonUtil;

import java.util.function.Function;

public abstract class NumberGunStat<T extends Number> extends GunStat<T>
{
    protected final Function<Double, T> caster;
    protected final T min;
    protected final T max;

    public NumberGunStat(T defaultValue, T value, T min, T max, Function<Double, T> caster)
    {
        super(defaultValue, value);
        this.caster = caster;
        this.min = min;
        this.max = max;
    }

    public T calculateClamped(T origin)
    {
        return AVACommonUtil.clamp(calculate(origin), min, max);
    }

    @Override
    public T calculate(T origin)
    {
        return AVACommonUtil.add(origin, value, caster);
    }

    public boolean isEmpty()
    {
        return AVACommonUtil.similar(value, defaultValue);
    }

    @Override
    public String toString()
    {
        return isEmpty() ? "GunStat{Empty}" : "NumberGunStat{" +
                "defaultValue=" + defaultValue +
                ", value=" + value +
                ", min=" + min +
                ", max=" + max +
                '}';
    }

}
