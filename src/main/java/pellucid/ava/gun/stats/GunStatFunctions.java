package pellucid.ava.gun.stats;

import java.util.function.Function;

public enum GunStatFunctions implements Function<Float, Float>
{
    RANGE((value) -> value * 1.5F),
    DAMAGE((value) -> value / 5.0F),
    PER_TICK((value) -> 20.0F / value),
    STABILITY((value) -> 40.0F / value * 2.75F),
    ACCURACY((value) -> Math.max(0, 100.0F - value) * 0.003F + 0.002F),
    SPREAD_RECOVERY((value) -> Math.max(0, 100.0F - value) * 0.0015F + 0.0015F),
    INITIAL_ACCURACY((value) -> Math.max(0, (100.0F - value) * 0.025F - 0.275F)),
    RELOAD_TIME((value) -> (float) Math.round(value * 20.0F)),
    ;
    private final Function<Float, Float> function;

    GunStatFunctions(Function<Float, Float> function)
    {
        this.function = function;
    }

    @Override
    public Float apply(Float a)
    {
        return function.apply(a);
    }
}
