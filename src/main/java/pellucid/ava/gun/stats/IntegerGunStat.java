package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import java.util.function.BiFunction;
import java.util.function.Function;

public class IntegerGunStat extends NumberGunStat<Integer>
{
    public static IntegerGunStat integer(int value)
    {
        return integer(0, value);
    }

    public static IntegerGunStat integer(int defaultValue, int value)
    {
        return integer(defaultValue, value, 0);
    }

    public static IntegerGunStat integer(int defaultValue, int value, int min)
    {
        return integer(defaultValue, value, min, Integer.MAX_VALUE);
    }

    public static IntegerGunStat integer(int defaultValue, int value, int min, int max)
    {
        return new IntegerGunStat(defaultValue, value, min, max, Double::intValue);
    }

    public IntegerGunStat(Integer defaultValue, Integer value, Integer min, Integer max, Function<Double, Integer> caster)
    {
        super(defaultValue, value, min, max, caster);
    }

    @Override
    public IntegerGunStat copy()
    {
        return integer(defaultValue, value, min, max);
    }

    @Override
    Pair<TriConsumer<CompoundTag, String, Integer>, BiFunction<CompoundTag, String, Integer>> createSerializer()
    {
        return Pair.of(DataTypes.INT::write, DataTypes.INT::read);
    }

    @Override
    Pair<TriConsumer<JsonObject, String, Integer>, Function<JsonElement, Integer>> createJsonSerializer()
    {
        return Pair.of(DataTypes.INT::write, DataTypes.INT::read);
    }

    @Override
    public String toDisplayValue()
    {
        return value.toString();
    }
}
