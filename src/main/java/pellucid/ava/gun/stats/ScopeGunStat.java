package pellucid.ava.gun.stats;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.Pair;

import java.util.function.BiFunction;
import java.util.function.Function;

public class ScopeGunStat extends GunStat<AVAItemGun.IScopeType>
{
    public static ScopeGunStat scope(AVAItemGun.IScopeType scope)
    {
        return new ScopeGunStat(AVAItemGun.ScopeTypes.NONE, scope);
    }

    private ScopeGunStat(AVAItemGun.IScopeType defaultValue, AVAItemGun.IScopeType value)
    {
        super(defaultValue, value);
    }

    @Override
    public ScopeGunStat copy()
    {
        return scope(value);
    }

    @Override
    Pair<TriConsumer<CompoundTag, String, AVAItemGun.IScopeType>, BiFunction<CompoundTag, String, AVAItemGun.IScopeType>> createSerializer()
    {
        return Pair.of((compound, key, item) -> {}, (compound, key) -> value);
    }

    @Override
    Pair<TriConsumer<JsonObject, String, AVAItemGun.IScopeType>, Function<JsonElement, AVAItemGun.IScopeType>> createJsonSerializer()
    {
        return Pair.of((json, key, item) -> {}, (json) -> value);
    }

    @Override
    public AVAItemGun.IScopeType calculate(AVAItemGun.IScopeType origin)
    {
        return getValue();
    }

    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public String toDisplayValue()
    {
        return "";
    }
}
