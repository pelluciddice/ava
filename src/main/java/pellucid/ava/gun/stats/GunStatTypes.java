package pellucid.ava.gun.stats;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import pellucid.ava.util.AVACommonUtil;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public enum GunStatTypes
{
    ATTACK_DAMAGE(1, true),
    ATTACK_DAMAGE_FLOATING(2, true),
    RANGE(1, true),
    FIRE_RATE(1, true),
    PENETRATION(1, true),

    CAPACITY(1, true),
    AMMO_TYPE(2, true, false),

    STABILITY(1, true),
    STABILITY_CROUCH(1, true, false),
    STABILITY_MOVING(1, true, false),
    STABILITY_JUMPING(1, true, false),
    STABILITY_AIM(1, true, false),
    STABILITY_AIM_CROUCH(1, true, false),
    STABILITY_AIM_MOVING(1, true, false),
    STABILITY_AIM_JUMPING(1, true, false),
    ACCURACY(1, true),
    ACCURACY_CROUCH(1, true, false),
    ACCURACY_MOVING(1, true, false),
    ACCURACY_JUMPING(1, true, false),
    ACCURACY_AIM(1, true, false),
    ACCURACY_AIM_CROUCH(1, true, false),
    ACCURACY_AIM_MOVING(1, true, false),
    ACCURACY_AIM_JUMPING(1, true, false),
    INITIAL_ACCURACY(1, true),
    INITIAL_ACCURACY_CROUCH(1, true, false),
    INITIAL_ACCURACY_MOVING(1, true, false),
    INITIAL_ACCURACY_JUMPING(1, true, false),
    INITIAL_ACCURACY_AIM(1, true, false),
    INITIAL_ACCURACY_AIM_CROUCH(1, true, false),
    INITIAL_ACCURACY_AIM_MOVING(1, true, false),
    INITIAL_ACCURACY_AIM_JUMPING(1, true, false),
    MOBILITY(1, true),

    RECOIL_COMPENSATION(1, true),
    RECOIL_COMPENSATION_CROUCH(1, true, false),
    RECOIL_COMPENSATION_MOVING(1, true, false),
    RECOIL_COMPENSATION_JUMPING(1, true, false),
    RECOIL_COMPENSATION_AIM(1, true, false),
    RECOIL_COMPENSATION_AIM_CROUCH(1, true, false),
    RECOIL_COMPENSATION_AIM_MOVING(1, true, false),
    RECOIL_COMPENSATION_AIM_JUMPING(1, true, false),
    SPREAD_MAX(0, true, false),
    SPREAD_FACTOR(0, true, false, false),
    SPREAD_RECOVERY(1, true),
    SPREAD_RECOVERY_FACTOR(1, true),
    SHAKE_FACTOR(0, true),
    SHAKE_TURN_CHANCE(0, true, false),

    AUTOMATIC(2, false),
    CROSSHAIR(2, false, false),
    REQUIRE_AIM(2, false, false, false),
    PELLETS(1, true),

    RELOAD_INTERACTABLE(2, false, false),
    EXTRA_RELOAD_STEPS(2, false, false),
    SILENCED(2, false, true),
    BULLET_TRAIL(2, false, false),

    FIRE_ANIMATION_TIME(2, false, false, false),
    RELOAD_TIME(0, false, true),
    PRE_RELOAD_TIME(0, false, true),
    POST_RELOAD_TIME(0, false, true),
    DRAW_TIME(0, false, true),
    AIM_TIME(0, false, true),
    OPTIONAL_SILENCER(2, false, true, true),
    EXIT_AIM_ON_FIRE(2, false, false, false),

    SCOPE_TYPE(2, false, false, false),
    ;

    private final int beneficial;
    private final boolean mutable;
    private final boolean display;
    private final boolean jei;
    private final String key;
    private final MutableComponent translatable;
    GunStatTypes(int beneficial, boolean mutable)
    {
        this(beneficial, mutable, true);
    }

    GunStatTypes(int beneficial, boolean mutable, boolean display)
    {
        this(beneficial, mutable, display, true);
    }

    GunStatTypes(int beneficial, boolean mutable, boolean display, boolean jei)
    {
        this.beneficial = beneficial;
        this.mutable = mutable;
        this.display = display;
        this.jei = jei;
        this.key = "ava.gui.gun_stat_" + name().toLowerCase(Locale.ROOT);
        this.translatable = Component.translatable(key);
    }


    public ChatFormatting getBeneficialColour(boolean increased)
    {
        if (beneficial == 2)
            return ChatFormatting.GOLD;
        return (beneficial == 0 && increased) || (beneficial == 1 && !increased) ? ChatFormatting.RED : ChatFormatting.GREEN;
    }
    public String getKey()
    {
        return key;
    }

    public MutableComponent getTranslatedName()
    {
        return translatable.copy();
    }

    public static List<MutableComponent> getTranslatedNames()
    {
        return Arrays.stream(values()).collect(ArrayList::new, (list, type) -> list.add((MutableComponent) type.translatable.copy()), ArrayList::addAll);
    }

    public static List<? extends MutableComponent> getFullDisplayStrings(Map<GunStatTypes, GunStat<?>> map)
    {
        return getStrings(map, (k, v) -> !v.isEmpty());
    }

    public static List<? extends MutableComponent> getConcludedDisplayStrings(Map<GunStatTypes, GunStat<?>> map)
    {
        return getStrings(map, (k, v) -> k.forDisplay() && !v.isEmpty());
    }

    public static List<? extends MutableComponent> getStrings(Map<GunStatTypes, GunStat<?>> map, @Nullable BiPredicate<GunStatTypes, GunStat<?>> valid)
    {
        List<MutableComponent> list = new ArrayList<>();
        AVACommonUtil.forEachSorted(map, Comparator.comparingInt(Enum::ordinal), (k, v) -> {
            if (valid == null || valid.test(k, v))
                list.add(k.getTranslatedName().copy().append(": ").append(v.toDisplayValue()));
        });
        return list;
    }

    public static List<GunStatTypes> getDisplayingTypes()
    {
        return Arrays.stream(values()).filter(GunStatTypes::forDisplay).collect(Collectors.toList());
    }

    public boolean forDisplay()
    {
        return display;
    }

    public boolean isMutable()
    {
        return mutable;
    }

    public boolean forJei()
    {
        return jei;
    }
}
