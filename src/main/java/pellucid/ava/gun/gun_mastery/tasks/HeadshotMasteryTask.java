package pellucid.ava.gun.gun_mastery.tasks;

import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.util.AVAWeaponUtil;

public class HeadshotMasteryTask extends MasteryTask
{
    public HeadshotMasteryTask(int targetProgress)
    {
        super("headless", targetProgress);
    }

    @Override
    protected float getScale(IClassification classification)
    {
        AVAWeaponUtil.Classification c = classification.getClassification();
        switch (c)
        {
            case SHOTGUN:
                return 1.75F;
            case SUB_MACHINEGUN:
                return 1.25F;
            case RIFLE:
                return 1.0F;
            case SNIPER:
                return 0.25F;
            case SNIPER_SEMI:
                return 0.4F;
            case PISTOL:
                return 0.35F;
            case PISTOL_AUTO:
                return 0.7F;
        }
        return 1.0F;
    }
}
