package pellucid.ava.gun.gun_mastery.tasks;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.entity.EntityType;
import pellucid.ava.items.functionalities.IClassification;

public class KillTypeMasteryTask extends MasteryTask
{
    private final EntityType<?> target;
    public KillTypeMasteryTask(int targetProgress, EntityType<?> target)
    {
        super("annihilator", targetProgress);
        this.target = target;
    }

    @Override
    protected float getScale(IClassification classification)
    {
        switch (classification.getClassification())
        {
            case PISTOL:
                return 0.3F;
            case PISTOL_AUTO:
                return 0.5F;
        }
        return 1.0F;
    }

    @Override
    public MutableComponent getDescription(IClassification classification, int currentProgress)
    {
        return Component.translatable(descriptionKey, target.getDescription().getString(), getTargetProgress(classification), currentProgress);
    }

    @Override
    public boolean test(Object t)
    {
        return t == target;
    }
}
