package pellucid.ava.gun.gun_mastery.tasks;

import pellucid.ava.items.functionalities.IClassification;

public class KillMasteryTask extends MasteryTask
{
    public KillMasteryTask(int targetProgress)
    {
        super("grim_reaper", targetProgress);
    }

    @Override
    protected float getScale(IClassification classification)
    {
        switch (classification.getClassification())
        {
            case PISTOL:
                return 0.3F;
            case PISTOL_AUTO:
                return 0.5F;
        }
        return 1.0F;
    }
}
