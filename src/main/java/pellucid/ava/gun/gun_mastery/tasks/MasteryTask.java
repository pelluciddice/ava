package pellucid.ava.gun.gun_mastery.tasks;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import pellucid.ava.items.functionalities.IClassification;

public abstract class MasteryTask
{
    public final String titleKey;
    public final String descriptionKey;
    protected final int targetProgress;

    protected MasteryTask(String name, int targetProgress)
    {
        this.titleKey = "ava.mastery.task.title." + name;
        this.descriptionKey = "ava.mastery.task.description." + name;
        this.targetProgress = targetProgress;
    }

    protected abstract float getScale(IClassification classification);
    public boolean test(Object t)
    {
        return true;
    }

    public int getTargetProgress(IClassification classification)
    {
        return (int) (targetProgress * getScale(classification));
    }

    public MutableComponent getTitle()
    {
        return Component.translatable(titleKey);
    }

    public MutableComponent getDescription(IClassification classification, int currentProgress)
    {
        return Component.translatable(descriptionKey, getTargetProgress(classification), currentProgress);
    }

    @Override
    public String toString()
    {
        return "MasteryTask{" +
                "titleKey='" + titleKey + '\'' +
                ", descriptionKey='" + descriptionKey + '\'' +
                ", targetProgress=" + targetProgress +
                '}';
    }
}
