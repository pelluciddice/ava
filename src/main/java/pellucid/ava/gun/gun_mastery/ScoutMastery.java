package pellucid.ava.gun.gun_mastery;

import com.google.common.collect.ImmutableList;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.gun.gun_mastery.tasks.DamageMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HeadshotMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HitMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillTypeMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class ScoutMastery extends GunMastery
{
    public ScoutMastery()
    {
        super("scout", ChatFormatting.LIGHT_PURPLE, 5);
    }

    @Override
    protected List<MasteryTask> createTasks()
    {
        return ImmutableList.of(
                new KillMasteryTask(40),
                new DamageMasteryTask(1750),
                new HitMasteryTask(450),
                new KillTypeMasteryTask(15, EntityType.ENDERMAN),
                new HeadshotMasteryTask(150)
        );
    }

    @Override
    public Pair<List<MutableComponent>, List<MutableComponent>> getPerkStrings(int mastery)
    {
        List<MutableComponent> buffs = new ArrayList<>();
        List<MutableComponent> debuffs = new ArrayList<>();
        String s = Component.translatable("ava.common.seconds").getString();
        if (mastery == 5)
        {
            addEffect(debuffs, MobEffects.GLOWING, 1, 5);
            addEffect(buffs, MobEffects.MOVEMENT_SPEED, 2, 3);
            addEffect(buffs, MobEffects.INVISIBILITY, 2, 3);
            buffs.add(Component.translatable("ava.mastery.skill.scout"));
        }
        else if (mastery == 4)
        {
            addEffect(debuffs, MobEffects.GLOWING, 1, 4);
            addEffect(buffs, MobEffects.MOVEMENT_SPEED, 1, 2);
            addEffect(buffs, MobEffects.INVISIBILITY, 1, 2);
        }
        else if (mastery == 3)
        {
            addEffect(debuffs, MobEffects.GLOWING, 1, 3);
            addEffect(buffs, MobEffects.MOVEMENT_SPEED, 1, 1);
            addEffect(buffs, MobEffects.INVISIBILITY, 1, 1);
        }
        else if (mastery == 2)
        {
            addEffect(debuffs, MobEffects.GLOWING, 1, 2);
            addEffect(buffs, MobEffects.MOVEMENT_SPEED, 1, 1);
        }
        else if (mastery == 1)
            addEffect(debuffs, MobEffects.GLOWING, 1, 1);
        return Pair.of(buffs, debuffs);
    }

    @Override
    public void onHitEntity(BulletEntity bullet, EntityHitResult result, int mastery)
    {
        if (result.getEntity() instanceof LivingEntity && bullet.getShooter() instanceof Player)
        {
            LivingEntity entity = (LivingEntity) result.getEntity();
            Player shooter = (Player) bullet.getShooter();
            effect(entity, true, MobEffects.GLOWING, mastery * 20, 0);
            if (mastery == 5)
            {
                effect(shooter, false, MobEffects.MOVEMENT_SPEED, 60, 1);
                effect(shooter, false, MobEffects.INVISIBILITY, 60, 1);
                if (random.nextFloat() <= 0.75F)
                    effect(shooter, false, MobEffects.JUMP, 60, 1);
            }
            else if (mastery == 4)
            {
                effect(shooter, false, MobEffects.MOVEMENT_SPEED, 60, 0);
                effect(shooter, false, MobEffects.INVISIBILITY, 60, 0);
            }
            else if (mastery == 3)
            {
                effect(shooter, false, MobEffects.MOVEMENT_SPEED, 40, 0);
                effect(shooter, false, MobEffects.INVISIBILITY, 40, 0);
            }
            else if (mastery == 2)
                effect(shooter, false, MobEffects.MOVEMENT_SPEED, 20, 0);
        }
    }

    @Override
    public void onHitBlock(BulletEntity bullet, BlockHitResult result, int mastery)
    {

    }
}
