package pellucid.ava.gun.gun_mastery;

import net.minecraft.ChatFormatting;
import net.minecraft.core.Holder;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public abstract class GunMastery implements IGunMastery
{
    public static final Map<String, GunMastery> MASTERIES = new HashMap<>();
    public static final List<GunMastery> MASTERIES_LIST = new ArrayList<>();

    protected final Random random = new Random();
    private final String name;
    public final String titleKey;
    public final String descriptionKey;
    private final ChatFormatting colour;
    private final int maxMastery;
    private final List<MasteryTask> tasks = createTasks();

    protected abstract List<MasteryTask> createTasks();

    public GunMastery(String name, ChatFormatting colour, int maxMastery)
    {
        this.name = name;
        this.titleKey = "ava.mastery.title." + name;
        this.descriptionKey = "ava.mastery.description." + name;
        this.colour = colour;
        this.maxMastery = maxMastery;
        MASTERIES.put(name, this);
        MASTERIES_LIST.add(this);
    }

    protected void effect(LivingEntity entity, boolean harmful, Holder<MobEffect> effect, int ticks, int amplifier)
    {
        if (!harmful || !(entity instanceof Player))
            entity.addEffect(new MobEffectInstance(effect, ticks, amplifier, false, false));
    }

    public abstract Pair<List<MutableComponent>, List<MutableComponent>> getPerkStrings(int mastery);

    private static final Component SECOND = Component.translatable("ava.common.seconds");
    protected void addEffect(List<MutableComponent> effects, Holder<MobEffect> effect, int level, int duration)
    {
        effects.add(effect.value().getDisplayName().copy().append(" ").append("I".repeat(level)).append(" - ").append(String.valueOf(duration)).append(" ").append(SECOND));
    }

    public MutableComponent getTitle(int mastery)
    {
        return mastery > 0 ? Component.translatable(titleKey, AVAConstants.ROME_I_V.get(mastery)) : Component.translatable(titleKey);
    }

    public MutableComponent getDescription()
    {
        return Component.translatable(descriptionKey);
    }

    @Override
    public ChatFormatting getColour()
    {
        return colour;
    }

    @Override
    public MasteryTask getTaskForLevel(int mastery)
    {
        return mastery >= 5 ? null : tasks.get(mastery);
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public int getMaxMastery()
    {
        return maxMastery;
    }

    @Override
    public String toString()
    {
        return "GunMastery{" +
                "random=" + random +
                ", name='" + name + '\'' +
                ", titleKey='" + titleKey + '\'' +
                ", descriptionKey='" + descriptionKey + '\'' +
                ", colour=" + colour +
                ", maxMastery=" + maxMastery +
                ", tasks=" + tasks +
                '}';
    }
}
