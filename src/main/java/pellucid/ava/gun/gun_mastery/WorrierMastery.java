package pellucid.ava.gun.gun_mastery;

import com.google.common.collect.ImmutableList;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.gun.gun_mastery.tasks.DamageMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HitMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillTypeMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class WorrierMastery extends GunMastery
{
    public WorrierMastery()
    {
        super("worrier", ChatFormatting.RED, 5);
    }

    @Override
    protected List<MasteryTask> createTasks()
    {
        return ImmutableList.of(
                new KillMasteryTask(40),
                new DamageMasteryTask(1750),
                new HitMasteryTask(450),
                new KillTypeMasteryTask(120, EntityType.ZOMBIE),
                new KillMasteryTask(120)
        );
    }

    @Override
    public Pair<List<MutableComponent>, List<MutableComponent>> getPerkStrings(int mastery)
    {
        List<MutableComponent> buffs = new ArrayList<>();
        List<MutableComponent> debuffs = new ArrayList<>();
        String s = Component.translatable("ava.common.seconds").getString();
        if (mastery == 5)
        {
            addEffect(buffs, MobEffects.REGENERATION, 2, 3);
            addEffect(buffs, MobEffects.DAMAGE_RESISTANCE, 2, 3);
            buffs.add(Component.translatable("ava.mastery.skill.worrier"));
        }
        else if (mastery == 4)
        {
            addEffect(buffs, MobEffects.REGENERATION, 1, 3);
            addEffect(buffs, MobEffects.DAMAGE_RESISTANCE, 1, 3);
        }
        else if (mastery == 3)
        {
            addEffect(buffs, MobEffects.REGENERATION, 1, 2);
            addEffect(buffs, MobEffects.DAMAGE_RESISTANCE, 1, 2);
        }
        else if (mastery == 2)
        {
            addEffect(buffs, MobEffects.REGENERATION, 1, 1);
            addEffect(buffs, MobEffects.DAMAGE_RESISTANCE, 1, 1);
        }
        else if (mastery == 1)
            addEffect(buffs, MobEffects.REGENERATION, 1, 1);
        return Pair.of(buffs, debuffs);
    }

    @Override
    public void onHitEntity(BulletEntity bullet, EntityHitResult result, int mastery)
    {
        if (result.getEntity() instanceof LivingEntity && bullet.getShooter() instanceof Player)
        {
            LivingEntity entity = (LivingEntity) result.getEntity();
            Player shooter = (Player) bullet.getShooter();
            if (mastery == 5)
            {
                effect(shooter, false, MobEffects.REGENERATION, 60, 1);
                effect(shooter, false, MobEffects.DAMAGE_RESISTANCE, 60, 1);
                if (shooter.getHealth() <= shooter.getMaxHealth() / 2.0F)
                    effect(shooter, false, MobEffects.HEAL, 1, 1);
                else
                    effect(shooter, false, MobEffects.HEAL, 1, 0);
            }
            else if (mastery == 4)
            {
                effect(shooter, false, MobEffects.REGENERATION, 60, 0);
                effect(shooter, false, MobEffects.DAMAGE_RESISTANCE, 60, 0);
            }
            else if (mastery == 3)
            {
                effect(shooter, false, MobEffects.REGENERATION, 40, 0);
                effect(shooter, false, MobEffects.DAMAGE_RESISTANCE, 40, 0);
            }
            else if (mastery == 2)
            {
                effect(shooter, false, MobEffects.REGENERATION, 20, 0);
                effect(shooter, false, MobEffects.DAMAGE_RESISTANCE, 20, 0);
            }
            else if (mastery == 1)
                effect(shooter, false, MobEffects.REGENERATION, 20, 0);
        }
    }

    @Override
    public void onHitBlock(BulletEntity bullet, BlockHitResult result, int mastery)
    {

    }
}
