package pellucid.ava.gun.gun_mastery;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.INBTSerializable;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class GunMasteryManager implements INBTSerializable<CompoundTag>
{
    public static final MedicMastery MEDIC;
    public static final ScoutMastery SCOUT;
    public static final SniperMastery SNIPER;
    public static final WorrierMastery WORRIER;

    public static final List<GunStatTypes> BOOSTS = ImmutableList.of(
            GunStatTypes.ATTACK_DAMAGE,
            GunStatTypes.ACCURACY,
            GunStatTypes.STABILITY,
            GunStatTypes.RANGE,
            GunStatTypes.MOBILITY
    );

    public static final Map<GunStatTypes, List<GunStatTypes>> SUBS = ImmutableMap.of(
            GunStatTypes.ATTACK_DAMAGE, ImmutableList.of(GunStatTypes.ATTACK_DAMAGE),
            GunStatTypes.ACCURACY, ImmutableList.of(GunStatTypes.ACCURACY, GunStatTypes.ACCURACY_CROUCH, GunStatTypes.ACCURACY_MOVING, GunStatTypes.ACCURACY_JUMPING, GunStatTypes.ACCURACY_AIM, GunStatTypes.ACCURACY_AIM_CROUCH, GunStatTypes.ACCURACY_AIM_MOVING, GunStatTypes.ACCURACY_AIM_JUMPING),
            GunStatTypes.STABILITY, ImmutableList.of(GunStatTypes.STABILITY, GunStatTypes.STABILITY_CROUCH, GunStatTypes.STABILITY_MOVING, GunStatTypes.STABILITY_JUMPING, GunStatTypes.STABILITY_AIM, GunStatTypes.STABILITY_AIM_CROUCH, GunStatTypes.STABILITY_AIM_MOVING, GunStatTypes.STABILITY_AIM_JUMPING),
            GunStatTypes.RANGE, ImmutableList.of(GunStatTypes.RANGE),
            GunStatTypes.MOBILITY, ImmutableList.of(GunStatTypes.MOBILITY)
    );

    public static final Set<GunStatTypes> ALL = ImmutableSet.of(
            GunStatTypes.ATTACK_DAMAGE,
            GunStatTypes.ACCURACY, GunStatTypes.ACCURACY_CROUCH, GunStatTypes.ACCURACY_MOVING, GunStatTypes.ACCURACY_JUMPING, GunStatTypes.ACCURACY_AIM, GunStatTypes.ACCURACY_AIM_CROUCH, GunStatTypes.ACCURACY_AIM_MOVING, GunStatTypes.ACCURACY_AIM_JUMPING,
            GunStatTypes.STABILITY, GunStatTypes.STABILITY_CROUCH, GunStatTypes.STABILITY_MOVING, GunStatTypes.STABILITY_JUMPING, GunStatTypes.STABILITY_AIM, GunStatTypes.STABILITY_AIM_CROUCH, GunStatTypes.STABILITY_AIM_MOVING, GunStatTypes.STABILITY_AIM_JUMPING,
            GunStatTypes.RANGE,
            GunStatTypes.MOBILITY
    );

    static
    {
        MEDIC = new MedicMastery();
        SCOUT = new ScoutMastery();
        SNIPER = new SniperMastery();
        WORRIER = new WorrierMastery();
    }

    private final AVAItemGun gun;
    private final ItemStack stack;
    public GunMastery mastery = null;
    public int masteryLevel = 0;
    public MasteryTask task;
    public int taskProgress = 0;
    public int[] boosts = new int[] {0, 0, 0, 0, 0};

    private GunMasteryManager(AVAItemGun gun, ItemStack stack)
    {
        this.gun = gun;
        this.stack = stack;
        deserializeNBT(stack.getOrDefault(AVADataComponents.TAG_ITEM_MASTERY_MANAGER, new CompoundTag()));
    }

    public static GunMasteryManager ofUnsafe(ItemStack stack)
    {
        return of(stack).get();
    }


    public static Optional<GunMasteryManager> of(ItemStack stack)
    {
        return Optional.ofNullable(stack.getItem() instanceof AVAItemGun ? new GunMasteryManager((AVAItemGun) stack.getItem(), stack) : null);
    }

    public int getBoost(GunStatTypes type)
    {
        return boosts[BOOSTS.indexOf(type)];
    }

    public boolean hasOngoingTask()
    {
        return task != null && masteryLevel < 5;
    }

    public float progressPercent()
    {
        return hasMasteryUnlocked() ? masteryLevel >= 5 ? 1.0F : taskProgress / (float) task.getTargetProgress(gun) : 0.0F;
    }

    public void save()
    {
        stack.set(AVADataComponents.TAG_ITEM_MASTERY_MANAGER, serializeNBT());
    }

    public static <T extends MasteryTask> boolean isTaskRunning(ItemStack stack, Class<T>... clazzes)
    {
        for (Class<T> clazz : clazzes)
            if (isTaskRunning(stack, clazz))
                return true;
        return false;
    }

    public static <T extends MasteryTask> boolean isTaskRunning(ItemStack stack, Class<T> clazz)
    {
        return isTaskRunning(stack, clazz, null);
    }

    public static <T extends MasteryTask> boolean isTaskRunning(ItemStack stack, Class<T> clazz, Object obj)
    {
        if (stack.getItem() instanceof AVAItemGun && !AVAServerConfig.DISABLE_MASTERY_SYSTEM.get())
        {
            GunMasteryManager manager = ofUnsafe(stack);
            return manager.masteryLevel < 5 && manager.hasOngoingTask() && manager.task.getClass().isAssignableFrom(clazz) && manager.task.test(obj);
        }
        return false;
    }

    public void progress(Level world, LivingEntity entity, int amount)
    {
        if (!hasOngoingTask() || !hasMasteryUnlocked())
        {
            if (taskProgress != 0)
            {
                taskProgress = 0;
                save();
            }
        }
        else if (!AVAServerConfig.DISABLE_MASTERY_SYSTEM.get())
        {
            if (masteryLevel < 5)
            {
                taskProgress += amount;
                if (taskProgress >= task.getTargetProgress(gun))
                {
                    if (entity instanceof Player)
                        entity.sendSystemMessage(Component.translatable("ava.mastery.level_up", mastery.getTitle(masteryLevel + 1), entity.getMainHandItem().getItem().getDescription().getString()));
                    taskProgress = 0;
                    masteryLevel++;
                    task = mastery.getTaskForLevel(masteryLevel);
                    world.playSound(null, entity, SoundEvents.PLAYER_LEVELUP, SoundSource.PLAYERS, 1.0F, 1.5F);
                }
                save();
            }
        }
    }

    public boolean hasMasteryUnlocked()
    {
        return mastery != null;
    }

    public void setMastery(GunMastery mastery)
    {
        setMastery(mastery, 0);
    }

    public void setMastery(GunMastery mastery, int masteryLevel)
    {
        this.mastery = mastery;
        this.masteryLevel = masteryLevel;
        this.taskProgress = 0;
        this.task = mastery.getTaskForLevel(masteryLevel);
        save();
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        if (mastery != null)
            compound.putString("mastery", mastery.getName());
        if (task != null)
            compound.putString("task", task.titleKey);
        compound.putInt("masteryLevel", masteryLevel);
        compound.putInt("taskProgress", taskProgress);
        compound.putIntArray("boosts", boosts);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        mastery = nbt.contains("mastery") ? GunMastery.MASTERIES.get(nbt.getString("mastery")) : null;
        masteryLevel = nbt.getInt("masteryLevel");
        taskProgress = nbt.getInt("taskProgress");
        task = mastery == null ? null : mastery.getTaskForLevel(masteryLevel);
        boosts = !nbt.contains("boosts") ? new int[] {0, 0, 0, 0, 0} : nbt.getIntArray("boosts");
    }
}
