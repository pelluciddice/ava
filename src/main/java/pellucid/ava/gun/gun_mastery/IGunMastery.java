package pellucid.ava.gun.gun_mastery;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;

public interface IGunMastery
{
    String getName();

    MutableComponent getTitle(int mastery);
    MutableComponent getDescription();

    ChatFormatting getColour();

    void onHitEntity(BulletEntity bullet, EntityHitResult result, int mastery);

    void onHitBlock(BulletEntity bullet, BlockHitResult result, int mastery);

    int getMaxMastery();

    MasteryTask getTaskForLevel(int mastery);
}
