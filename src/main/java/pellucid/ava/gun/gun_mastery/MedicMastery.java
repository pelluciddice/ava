package pellucid.ava.gun.gun_mastery;

import com.google.common.collect.ImmutableList;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.gun.gun_mastery.tasks.DamageMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HitMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillTypeMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class MedicMastery extends GunMastery
{
    public MedicMastery()
    {
        super("medic", ChatFormatting.GREEN, 5);
    }

    @Override
    protected List<MasteryTask> createTasks()
    {
        return ImmutableList.of(
                new KillMasteryTask(40),
                new DamageMasteryTask(1750),
                new HitMasteryTask(450),
                new KillTypeMasteryTask(15, EntityType.WITCH),
                new KillMasteryTask(120)
        );
    }

    @Override
    public Pair<List<MutableComponent>, List<MutableComponent>> getPerkStrings(int mastery)
    {
        List<MutableComponent> buffs = new ArrayList<>();
        List<MutableComponent> debuffs = new ArrayList<>();
        if (mastery == 5)
        {
            addEffect(debuffs, MobEffects.WEAKNESS, 2, 3);
            addEffect(buffs, MobEffects.ABSORPTION, 2, 3);
            debuffs.add(Component.translatable("ava.mastery.skill.medic"));
            buffs.add(Component.translatable("ava.mastery.skill.medic_2"));
        }
        else if (mastery == 4)
        {
            addEffect(debuffs, MobEffects.WEAKNESS, 1, 3);
            addEffect(buffs, MobEffects.ABSORPTION, 1, 3);
        }
        else if (mastery == 3)
        {
            addEffect(debuffs, MobEffects.WEAKNESS, 1, 2);
            addEffect(buffs, MobEffects.ABSORPTION, 1, 2);
        }
        else if (mastery == 2)
        {
            addEffect(debuffs, MobEffects.WEAKNESS, 1, 1);
            addEffect(buffs, MobEffects.ABSORPTION, 1, 1);
        }
        else if (mastery == 1)
            addEffect(buffs, MobEffects.ABSORPTION, 1, 1);
        return Pair.of(buffs, debuffs);
    }

    @Override
    public void onHitEntity(BulletEntity bullet, EntityHitResult result, int mastery)
    {
        if (result.getEntity() instanceof LivingEntity && bullet.getShooter() instanceof Player)
        {
            LivingEntity entity = (LivingEntity) result.getEntity();
            Player shooter = (Player) bullet.getShooter();
            if (mastery == 5)
            {
                effect(entity, true, MobEffects.WEAKNESS, 60, 1);
                effect(shooter, false, MobEffects.ABSORPTION, 60, 1);
                if (random.nextFloat() <= 0.25F)
                    effect(entity, true, MobEffects.LEVITATION, 40, 1);
                else
                {
                    effect(shooter, false, MobEffects.HEALTH_BOOST, 60, 1);
                    effect(shooter, false, MobEffects.HEAL, 20, 0);
                }
            }
            else if (mastery == 4)
            {
                effect(entity, true, MobEffects.WEAKNESS, 60, 0);
                effect(shooter, false, MobEffects.ABSORPTION, 60, 0);
            }
            else if (mastery == 3)
            {
                effect(entity, true, MobEffects.WEAKNESS, 40, 0);
                effect(shooter, false, MobEffects.ABSORPTION, 40, 0);
            }
            else if (mastery == 2)
            {
                effect(entity, true, MobEffects.WEAKNESS, 20, 0);
                effect(shooter, false, MobEffects.ABSORPTION, 20, 0);
            }
            else if (mastery == 1)
                effect(shooter, false, MobEffects.ABSORPTION, 20, 0);
        }
    }

    @Override
    public void onHitBlock(BulletEntity bullet, BlockHitResult result, int mastery)
    {

    }
}
