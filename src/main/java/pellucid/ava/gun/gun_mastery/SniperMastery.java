package pellucid.ava.gun.gun_mastery;

import com.google.common.collect.ImmutableList;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.gun.gun_mastery.tasks.DamageMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HeadshotMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.HitMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.KillTypeMasteryTask;
import pellucid.ava.gun.gun_mastery.tasks.MasteryTask;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class SniperMastery extends GunMastery
{
    public SniperMastery()
    {
        super("sniper", ChatFormatting.DARK_AQUA, 5);
    }

    @Override
    protected List<MasteryTask> createTasks()
    {
        return ImmutableList.of(
                new KillMasteryTask(40),
                new DamageMasteryTask(1750),
                new HitMasteryTask(450),
                new KillTypeMasteryTask(60, EntityType.SKELETON),
                new HeadshotMasteryTask(150)
        );
    }

    @Override
    public Pair<List<MutableComponent>, List<MutableComponent>> getPerkStrings(int mastery)
    {
        List<MutableComponent> buffs = new ArrayList<>();
        List<MutableComponent> debuffs = new ArrayList<>();
        String s = Component.translatable("ava.common.seconds").getString();
        if (mastery == 5)
        {
            addEffect(debuffs, MobEffects.MOVEMENT_SLOWDOWN, 2, 3);
            addEffect(debuffs, MobEffects.POISON, 3, 4);
            debuffs.add(Component.translatable("ava.mastery.skill.sniper"));
            buffs.add(Component.translatable("ava.mastery.skill.sniper_2"));
        }
        else if (mastery == 4)
        {
            addEffect(debuffs, MobEffects.MOVEMENT_SLOWDOWN, 1, 3);
            addEffect(debuffs, MobEffects.POISON, 2, 3);
        }
        else if (mastery == 3)
        {
            addEffect(debuffs, MobEffects.MOVEMENT_SLOWDOWN, 1, 2);
            addEffect(debuffs, MobEffects.POISON, 2, 2);
        }
        else if (mastery == 2)
        {
            addEffect(debuffs, MobEffects.MOVEMENT_SLOWDOWN, 1, 1);
            addEffect(debuffs, MobEffects.POISON, 1, 1);
        }
        else if (mastery == 1)
            addEffect(debuffs, MobEffects.MOVEMENT_SLOWDOWN, 1, 1);
        return Pair.of(buffs, debuffs);
    }

    @Override
    public void onHitEntity(BulletEntity bullet, EntityHitResult result, int mastery)
    {
        if (result.getEntity() instanceof LivingEntity && bullet.getShooter() instanceof Player)
        {
            LivingEntity entity = (LivingEntity) result.getEntity();
            Player shooter = (Player) bullet.getShooter();
            if (mastery == 5)
            {
                effect(entity, true, MobEffects.MOVEMENT_SLOWDOWN, 60, 1);
                effect(entity, true, MobEffects.POISON, 80, 2);
                if (entity.getHealth() <= entity.getMaxHealth() / 2.0F)
                    effect(entity, true, entity.isInvertedHealAndHarm() ? MobEffects.HEAL : MobEffects.HARM, 1, 1);
                else
                    effect(shooter, false, MobEffects.NIGHT_VISION, 100, 0);
            }
            else if (mastery == 4)
            {
                effect(entity, true, MobEffects.MOVEMENT_SLOWDOWN, 60, 0);
                effect(entity, true, MobEffects.POISON, 60, 1);
            }
            else if (mastery == 3)
            {
                effect(entity, true, MobEffects.MOVEMENT_SLOWDOWN, 40, 0);
                effect(entity, true, MobEffects.POISON, 40, 1);
            }
            else if (mastery == 2)
            {
                effect(entity, true, MobEffects.MOVEMENT_SLOWDOWN, 20, 0);
                effect(entity, true, MobEffects.POISON, 20, 0);
            }
            else if (mastery == 1)
                effect(entity, true, MobEffects.MOVEMENT_SLOWDOWN, 20, 0);
        }
    }

    @Override
    public void onHitBlock(BulletEntity bullet, BlockHitResult result, int mastery)
    {

    }
}
