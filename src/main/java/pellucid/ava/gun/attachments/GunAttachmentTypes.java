package pellucid.ava.gun.attachments;

import net.minecraft.nbt.StringTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import pellucid.ava.AVA;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.init.Materials;
import pellucid.ava.recipes.Recipe;

import java.util.Locale;

import static pellucid.ava.gun.attachments.GunAttachmentCategory.*;

public enum GunAttachmentTypes
{
    //Scopes
    X2_HS_SCOPE(SCOPE, 5),
    X4_ACOG_SCOPE(SCOPE, 10),
    QUICK_SCOPE(SCOPE, 15),
    SHARP_SHOOTER_SCOPE(SCOPE, 20),

    //Barrels
    HEAVY_BARREL(BARREL, 6),
    BURST_BARREL(BARREL, 5),
    LONG_RANGE_BARREL(BARREL, 6),
    SHARP_SHOOTER_BARREL(BARREL, 8),
    REINFORCED_BARREL(BARREL, 5),
    SPETSNAZ_BARREL(BARREL, 5),
    BIRDSHOT_BARREL(BARREL, 5),

    //Triggers
    CUSTOM_TRIGGER(TRIGGER, 5),
    ADVANCED_TRIGGER(TRIGGER, 5),
    PRECISION_TRIGGER(TRIGGER, 5),
    EXTENDED_MAGAZINE(TRIGGER, 5),
    MECHANISM_IMPROVEMENT(TRIGGER, 5),
    VETERAN_MECHANISM(TRIGGER, 5),
    STABILITY_UPGRADE(TRIGGER, 8),

    //Grips
    ERGONOMIC_GRIP(GRIP, 5),
    PROTOTYPE_GRIP(GRIP, 5),
    SILICON_GRIP(GRIP, 5),
    SOFT_GRIP(GRIP, 5),
    CARBON_GRIP(GRIP, 4),
    WEIGHTED_GRIP(GRIP, 6),

    RECOIL_CONTROL_STOCK(STOCK, 6),
    LIGHT_STOCK(STOCK, 4),
    FAST_REACTION_STOCK(STOCK, 5),
    SHOCK_ABSORBER(STOCK, 5);


    private final GunAttachmentCategory category;
    public final String translationKey;
    private final MutableComponent translated;
    private final Recipe recipe;
    private final int cost;

    GunAttachmentTypes(GunAttachmentCategory category, int cost)
    {
        this.category = category;
        category.addToCategory(this);
        this.translationKey = AVA.MODID + ".attachment." + name().toLowerCase(Locale.ROOT);
        this.translated = Component.translatable(translationKey);
        this.cost = cost;
        this.recipe = new Recipe().addItem(Materials.MECHANICAL_COMPONENTS, cost);
    }

    public Recipe getRecipe()
    {
        return recipe;
    }

    public int getCost()
    {
        return AVAServerConfig.FREE_ATTACHMENTS.get() ? 0 : cost;
    }

    public static GunAttachmentTypes of(StringTag compound)
    {
        return valueOf(compound.getAsString());
    }

    public MutableComponent getTranslated()
    {
        return translated;
    }

    public GunAttachmentCategory getCategory()
    {
        return category;
    }

    public StringTag serializeNBT()
    {
        return StringTag.valueOf(name());
    }
}
