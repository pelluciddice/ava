package pellucid.ava.gun.attachments;

import net.minecraft.world.item.ItemStack;
import pellucid.ava.gun.stats.GunStat;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.gun.stats.NumberGunStat;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface IAttachmentManager
{
    List<GunAttachmentTypes> getTypes();

    List<Map<GunStatTypes, GunStat<?>>> getStats(ItemStack stack);

    default <T> T get(ItemStack stack, GunStatTypes type, T origin, T defaultValue)
    {
        boolean noOrigin = origin == null;
        if (noOrigin)
            origin = defaultValue;
        for (Map<GunStatTypes, GunStat<?>> stats : getStats(stack))
        {
            GunStat<T> stat = (GunStat<T>) stats.get(type);
            if (stat != null && !stat.isEmpty())
                origin = (T) (noOrigin || !(stat instanceof NumberGunStat) ? stat.calculate(origin) : ((NumberGunStat) stat).calculateClamped((Number) origin));
        }
        return origin;
    }

    @Nullable
    private <T> T get(ItemStack stack, Function< Map<GunStatTypes, GunStat<?>>, T> getter)
    {
        T t = null;
        for ( Map<GunStatTypes, GunStat<?>> stat : getStats(stack))
            t = getter.apply(stat);
        return t;
    }
}
