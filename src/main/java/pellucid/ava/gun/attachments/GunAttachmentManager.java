package pellucid.ava.gun.attachments;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.gun.stats.GunStat;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.INBTSerializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GunAttachmentManager implements INBTSerializable<CompoundTag>, IAttachmentManager
{
    private final ItemStack stack;
    private final List<GunAttachmentCategory> categories = new ArrayList<>();
    private final List<GunAttachmentTypes> types = new ArrayList<>();

    public static GunAttachmentManager of(ItemStack stack)
    {
        return new GunAttachmentManager(stack);
    }

    private GunAttachmentManager(ItemStack stack)
    {
        this(stack, stack.getOrDefault(AVADataComponents.TAG_ITEM_GUN_ATTACHMENTS_MANAGER, new CompoundTag()));
    }

    public GunAttachmentManager(ItemStack stack, CompoundTag compound)
    {
        this.stack = stack;
        deserializeNBT(compound);
    }

    public boolean install(GunAttachmentTypes type)
    {
        if (!types.contains(type) && !categories.contains(type.getCategory()))
        {
            types.add(type);
            categories.add(type.getCategory());
            save();
            return true;
        }
        return false;
    }

    public boolean uninstall(GunAttachmentTypes type)
    {
        if (types.contains(type) && categories.contains(type.getCategory()))
        {
            types.remove(type);
            categories.remove(type.getCategory());
            save();
            return true;
        }
        return false;
    }

    public ItemStack save()
    {
        stack.set(AVADataComponents.TAG_ITEM_GUN_ATTACHMENTS_MANAGER, serializeNBT());
        return stack;
    }

    public Optional<GunAttachmentTypes> fromCategory(GunAttachmentCategory category)
    {
        return getTypes().stream().filter((type) -> type.getCategory() == category).findFirst();
    }

    public List<GunAttachmentTypes> getTypes()
    {
        return types;
    }

    @Override
    public List<Map<GunStatTypes, GunStat<?>>> getStats(ItemStack stack)
    {
        if (stack.getItem() instanceof AVAItemGun gun)
        {
            List<Map<GunStatTypes, GunStat<?>>> list = new ArrayList<>();
            gun.getAttachmentTypes().forEach((k, v) -> {
                if (types.contains(k))
                    list.add(v);
            });
            return list;
        }
        return Collections.emptyList();
    }

//    @NotNull
//    @Override
//    public <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side)
//    {
//        return cap == GUN_ATTACHMENT_CAPABILITY ? lazyOptional.cast() : LazyOptional.empty();
//    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        ListTag list = new ListTag();
        for (GunAttachmentTypes type : getTypes())
            list.add(type.serializeNBT());
        compound.put("types", list);
        ListTag list2 = new ListTag();
        for (GunAttachmentCategory category : categories)
            list2.add(category.serializeNBT());
        compound.put("categories", list2);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        types.clear();
        for (Tag tag : nbt.getList("types", Tag.TAG_STRING))
            types.add(GunAttachmentTypes.of((StringTag) tag));
        categories.clear();
        for (Tag tag : nbt.getList("categories", Tag.TAG_STRING))
            categories.add(GunAttachmentCategory.of((StringTag) tag));
    }

    @Override
    public String toString()
    {
        return super.toString() + "{" +
                "types=" + types +
                '}';
    }
}