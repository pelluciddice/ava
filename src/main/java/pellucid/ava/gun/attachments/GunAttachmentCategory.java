package pellucid.ava.gun.attachments;

import net.minecraft.nbt.StringTag;

import java.util.ArrayList;
import java.util.List;

public enum GunAttachmentCategory
{
    UPGRADE,
    BARREL,
    SCOPE,
    TRIGGER,
    GRIP,
    STOCK
    ;

    private final List<GunAttachmentTypes> types = new ArrayList<>();
    GunAttachmentCategory()
    {

    }

    public void addToCategory(GunAttachmentTypes type)
    {
        types.add(type);
    }

    public List<GunAttachmentTypes> getTypes()
    {
        return types;
    }

    public static GunAttachmentCategory of(StringTag compound)
    {
        return valueOf(compound.getAsString());
    }

    public StringTag serializeNBT()
    {
        return StringTag.valueOf(name());
    }
}
