package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.client.Minecraft;
import net.minecraft.commands.CommandBuildContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.blocks.BlockStateArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Property;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientChatEvent;
import net.neoforged.neoforge.client.event.RegisterClientCommandsEvent;
import pellucid.ava.client.guis.ClientConfigGUI;
import pellucid.ava.client.guis.ViewModelGUI;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.miscs.MagicStickItem;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAConstants;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

@EventBusSubscriber(Dist.CLIENT)
public class FakeCommands
{
	@SubscribeEvent
	public static void onClientCommandRegister(RegisterClientCommandsEvent event)
	{
		CommandDispatcher<CommandSourceStack> dispatcher = event.getDispatcher();
		CommandBuildContext build = event.getBuildContext();
		dispatcher.register(
			literal("ava")
					.then(literal("debug").executes((context) -> {
						ItemStack stack = Minecraft.getInstance().player.getMainHandItem();
						AVAModelTypes.TYPES.forEach((t) -> {
							if (t.isRightItem(stack.getItem()))
								AVAClientUtil.setDeferredScreen(t.getAnimatingScreen());
						});
						return 1;
					}))
					.then(literal("clientConfig").executes((context) -> {
						AVAClientUtil.setDeferredScreen(new ClientConfigGUI());
						return 1;
					}))
					.then(literal("viewModel").executes((context) -> {
						Player player = Minecraft.getInstance().player;
						ItemStack stack = player.getMainHandItem();
						if (stack.isEmpty())
							player.displayClientMessage(AVAConstants.MAIN_HAND_EMPTY, false);
						else
							AVAClientUtil.setDeferredScreen(new ViewModelGUI(stack));
						return 1;
					}))
					.then(literal("refreshCache").executes((context) -> {
						refreshCache();
						return 1;
					}))
					.then(literal("magic")
							.then(literal("set")
									.then(argument("block", BlockStateArgument.block(build))
											.executes((context) -> {
												BlockPos pos = MagicStickItem.POS;
												BlockPos pos2 = MagicStickItem.POS_2;
												if (pos == null || pos2 == null)
													return 0;
												Minecraft.getInstance().player.connection.sendCommand("fill " + pos.getX() + " " + pos.getY() + " " + pos.getZ() + " " + pos2.getX() + " " + pos2.getY() + " " + pos2.getZ() + " " + blockStateToString(BlockStateArgument.getBlock(context, "block").getState()));
												return 1;
											}))))
					.then(literal("magic")
							.then(literal("replace")
									.then(argument("toReplace", BlockStateArgument.block(build))
											.then(argument("replacement", BlockStateArgument.block(build))
													.executes((context) -> {
														BlockPos pos = MagicStickItem.POS;
														BlockPos pos2 = MagicStickItem.POS_2;
														if (pos == null || pos2 == null)
															return 0;
														Minecraft.getInstance().player.connection.sendCommand("fill " + pos.getX() + " " + pos.getY() + " " + pos.getZ() + " " + pos2.getX() + " " + pos2.getY() + " " + pos2.getZ() + " " + blockStateToString(BlockStateArgument.getBlock(context, "replacement").getState()) + " replace " + blockStateToString(BlockStateArgument.getBlock(context, "toReplace").getState()));
														return 1;
													})))))
		);
	}

	private static String blockStateToString(BlockState state)
	{
		String toSend = BuiltInRegistries.BLOCK.getKey(state.getBlock()).toString();
		if (!state.getValues().isEmpty())
		{
			toSend += '[';
			toSend += state.getValues().entrySet().stream().map(PROPERTY_ENTRY_TO_STRING_FUNCTION).collect(Collectors.joining(","));
			toSend += ']';
		}
		return toSend;
	}

	private static final Function<Map.Entry<Property<?>, Comparable<?>>, String> PROPERTY_ENTRY_TO_STRING_FUNCTION = new Function<Map.Entry<Property<?>, Comparable<?>>, String>(

	) {
		public String apply(@Nullable Map.Entry<Property<?>, Comparable<?>> p_61155_) {
			if (p_61155_ == null) {
				return "<NULL>";
			} else {
				Property<?> property = p_61155_.getKey();
				return property.getName() + "=" + this.getName(property, p_61155_.getValue());
			}
		}

		private <T extends Comparable<T>> String getName(Property<T> p_61152_, Comparable<?> p_61153_) {
			return p_61152_.getName((T)p_61153_);
		}
	};

	public static void refreshCache()
	{
		Player player = Minecraft.getInstance().player;
		if (player != null)
		{
			player.displayClientMessage(Component.literal("Pending Refresh"), false);
			AVAClientUtil.PENDING_REFRESH = true;
		}
	}

    @SubscribeEvent
    public static void onClientTextInput(ClientChatEvent event)
    {
//        String message = event.getOriginalMessage();
//		Player player = Minecraft.getInstance().player;
//		if (!AVAWeaponUtil.isValidEntity(player))
//			return;
//		if (message.charAt(0) == '/')
//        {
//            String[] args = new String[]{};
//            try
//            {
//                args = message.replace("/", "").split(" ");
//				if (args[0].equals("ava"))
//                {
//					boolean succeed = true;
//					String parent = args[1];
//					switch (parent)
//					{
//						case "debug":
//							ModifiedGunModel.ANIMATING = true;
//							ModifiedGunModel.INITIAL_UPDATE = false;
//							AVAClientUtil.setDeferredScreen(new DummyScreen());
//							break;
//						case "preset":
//							int id = Integer.parseInt(args[3]);
//							switch (args[2])
//							{
//								case "select":
//									CompetitiveModeClient.choosePreset(id);
//									break;
//								case "set":
//									CompetitiveModeClient.setPreset(id);
//									break;
//								case "default":
//									CompetitiveModeClient.restoreDefault(id);
//									break;
//							}
//							break;
//						case "crosshair":
//							switch (args[2])
//							{
//								case "enable":
//									AVAClientConfig.SHOULD_RENDER_CROSS_HAIR.set(Boolean.parseBoolean(args[3]));
//									break;
//								case "red":
//									AVAClientConfig.RED.set(Double.parseDouble(args[3]));
//									break;
//								case "green":
//									AVAClientConfig.GREEN.set(Double.parseDouble(args[3]));
//									break;
//								case "blue":
//									AVAClientConfig.BLUE.set(Double.parseDouble(args[3]));
//									break;
//							}
//							break;
//						case "animation":
//						{
//							boolean value = Boolean.parseBoolean(args[3]);
//							switch (args[2])
//							{
//								case "bobbing":
//									AVAClientConfig.FP_BOBBING.set(value);
//									break;
//								case "run":
//									AVAClientConfig.FP_RUN_ANIMATION.set(value);
//									break;
//								case "jump":
//									AVAClientConfig.FP_JUMP_ANIMATION.set(value);
//									break;
//								case "fire":
//									AVAClientConfig.FP_FIRE_ANIMATION.set(value);
//									break;
//								case "reload":
//									AVAClientConfig.FP_RELOAD_ANIMATION.set(value);
//									break;
//							}
//							break;
//						}
//						case "keybind":
//						{
//							boolean value = Boolean.parseBoolean(args[3]);
//							switch (args[2])
//							{
//								case "quick_swap":
//									AVAClientConfig.ENABLE_QUICK_SWAP_HOTKEY.set(value);
//									break;
//								case "preset_hotkey":
//									AVAClientConfig.ENABLE_PRESET_HOTKEY.set(value);
//									break;
//								case "radio_hotkey":
//									AVAClientConfig.ENABLE_RADIO_HOTKEY.set(value);
//									break;
//							}
//							break;
//						}
//						case "indicator":
//						{
//							boolean value = Boolean.parseBoolean(args[3]);
//							switch (args[2])
//							{
//								case "projectile":
//									AVAClientConfig.ENABLE_PROJECTILE_INDICATOR.set(value);
//									break;
//								case "bio":
//									AVAClientConfig.ENABLE_BIO_INDICATOR.set(value);
//									break;
//							}
//							break;
//						}
//						case "fast_assets":
//						{
//							boolean value = Boolean.parseBoolean(args[3]);
//							switch (args[2])
//							{
//								case "ai":
//									AVAClientConfig.AI_FAST_ASSETS.set(value);
//									break;
//								case "gui":
//									AVAClientConfig.GUI_FAST_ASSETS.set(value);
//									break;
//								case "all_other":
//									AVAClientConfig.FAST_ASSETS.set(value);
//									break;
//							}
//							break;
//						}
//						case "kill_tips":
//						{
//							boolean value = Boolean.parseBoolean(args[3]);
//							switch (args[2])
//							{
//								case "display":
//									AVAClientConfig.ENABLE_KILL_TIP.set(value);
//									break;
//							}
//							break;
//						}
//						case "clientConfig":
//						{
//							System.out.println("set screen");
//							AVAClientUtil.setDeferredScreen(new ClientConfigGUI());
//							break;
//						}
//						case "viewModel":
//						{
//							ItemStack stack = player.getMainHandItem();
//							if (stack.isEmpty())
//								player.displayClientMessage(AVAConstants.MAIN_HAND_EMPTY, false);
//							else
//								AVAClientUtil.setDeferredScreen(new ViewModelGUI(stack));
//							break;
//						}
//						default:
//							succeed = false;
//							break;
//					}
//					event.setCanceled(succeed);
//                }
//            }
//            catch (Exception e)
//            {
//                String eMessage = "Cannot recognize command: " + message + Arrays.toString(args);
//                message(eMessage, ChatFormatting.RED);
//                AVA.LOGGER.error(eMessage);
//                e.printStackTrace();
//            }
//        }
    }
}
