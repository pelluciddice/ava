package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.commands.CommandBuildContext;
import net.minecraft.commands.CommandSourceStack;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesServerManager;

import static net.minecraft.commands.Commands.literal;

public class AVACommands
{
    public AVACommands() {}

    public static void registerAll(CommandDispatcher<CommandSourceStack> dispatcher, CommandBuildContext context)
    {
        dispatcher.register(literal("ava")
                .then(SetCrosshairServerCommand.register(dispatcher))
                .then(FriendlyFireCommand.register(dispatcher))
                .then(ReducedFriendlyFireCommand.register(dispatcher))
                .then(SetGlassDestroyableCommand.register(dispatcher))
                .then(SetMobDropsKitsCommand.register(dispatcher))
                .then(RecoilRefundTypeCommand.register(dispatcher))
                .then(BoostPlayerCommand.register(dispatcher))
                .then(OrganizeItemFramesCommand.register(dispatcher))
                .then(DeployCommand.register(dispatcher, context))
                .then(AICommand.register(dispatcher, context))
                .then(TeamSpawnpointCommand.register(dispatcher))
                .then(PresetWithParachuteCommand.register(dispatcher))
                .then(RepairRepairablesCommand.register(dispatcher))
                .then(SiteCommand.register(dispatcher))
                .then(TimerCommand.register(dispatcher))
                .then(PlayTypeCommand.register(dispatcher))
                .then(AVAScoreboardCommand.register(dispatcher))
                .then(RenderingAreaCommand.register(dispatcher))
                .then(MasteryCommand.register(dispatcher))
                .then(RestrictedMovementCommand.register(dispatcher))
                .then(BroadcastTextCommand.register(dispatcher))
                .then(MaxEntityCountCommand.register(dispatcher))
                .then(CompetitiveCommand.register(dispatcher))
                .then(LoadingImagesServerManager.registerCommand(dispatcher))
        );
    }
}
