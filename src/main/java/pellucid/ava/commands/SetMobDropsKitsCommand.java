package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.util.AVACommonUtil;

public class SetMobDropsKitsCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return Commands.literal("setMobDropKitChance")
                .requires(player -> player.hasPermission(2))
                .then(Commands.argument("value", FloatArgumentType.floatArg(0, 1))
                .executes((context) ->
                {
                    float chance = FloatArgumentType.getFloat(context, "value");
                    AVACrossWorldData.getInstance().mobDropsKitsChance = chance;
                    AVACommonUtil.broadcastSystemMessage(Component.translatable("ava.chat.mob_drop_kit_chance", chance));
                    return 1;
                }));
    }
}
