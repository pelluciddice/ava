package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import pellucid.ava.config.AVAServerConfig;

public class RestrictedMovementCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return Commands.literal("restrictedMovement")
                .requires(player -> player.hasPermission(2))
                .then(Commands.argument("value", BoolArgumentType.bool())
                        .executes((context) -> {
                            boolean value = BoolArgumentType.getBool(context, "value");
                            AVAServerConfig.AVA_RESTRICTED_MOVEMENT.set(value);
                            return value ? 1 : 0;
                        }));
    }
}
