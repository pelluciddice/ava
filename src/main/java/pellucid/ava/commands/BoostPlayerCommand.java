package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.packets.DataToClientMessage;

import java.util.Collection;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class BoostPlayerCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return literal("playerBoosts")
                .requires(player -> player.hasPermission(2))
                .then(literal("attackDamage")
                .then(literal("add")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(-20, 20))
                .executes((context) -> add(true, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets"))))))
                .then(literal("set")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(0, 20))
                .executes((context) -> set(true, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets")))))))
                .then(literal("health")
                .then(literal("add")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(-20, 20))
                .executes((context) -> add(false, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets"))))))
                .then(literal("set")
                .then(argument("targets", EntityArgument.players())
                .then(argument("amount", IntegerArgumentType.integer(0, 20))
                .executes((context) -> set(false, IntegerArgumentType.getInteger(context, "amount"), EntityArgument.getPlayers(context, "targets")))))));
    }

    private static int add(boolean attackDamage, int amount, Collection<ServerPlayer> players)
    {
        players.forEach((player) -> {
            PlayerAction cap = PlayerAction.getCap(player);
            if (attackDamage)
                cap.setAttackDamageBoost(cap.getAttackDamageBoost() + amount);
            else
                cap.setHealthBoost(cap.getHealthBoost() + amount);
            DataToClientMessage.playerBoosts(player);
            player.sendSystemMessage(Component.translatable("ava.chat.boost_changed", attackDamage ? "attack damage" : "health", amount, attackDamage ? cap.getAttackDamageBoost() : cap.getHealthBoost()));
        });
        return amount;
    }

    private static int set(boolean attackDamage, int amount, Collection<ServerPlayer> players)
    {
        players.forEach((player) -> {
            PlayerAction cap = PlayerAction.getCap(player);
            if (attackDamage)
                cap.setAttackDamageBoost(amount);
            else
                cap.setHealthBoost(amount);
            player.sendSystemMessage(Component.translatable("ava.chat.boost_set", attackDamage ? "attack damage" : "health", amount, attackDamage ? cap.getAttackDamageBoost() : cap.getHealthBoost()));
        });
        return amount;
    }
}
