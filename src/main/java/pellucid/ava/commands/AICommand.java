package pellucid.ava.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.commands.CommandBuildContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.coordinates.Vec3Argument;
import net.minecraft.commands.arguments.item.ItemArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.smart.RoledSidedSmartAIEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class AICommand
{
    private static final SimpleCommandExceptionType ERROR_FAILED = new SimpleCommandExceptionType(Component.translatable("commands.summon.failed"));
    private static final SimpleCommandExceptionType ERROR_DUPLICATE_UUID = new SimpleCommandExceptionType(Component.translatable("commands.summon.failed.uuid"));

    private static final String[] SIDES = new String[] {"eu", "nrf"};

    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher, CommandBuildContext command)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("ai").requires(player -> player.hasPermission(2));
        for (String side : SIDES)
        {
            type(command, side, "custom", builder,
                    argument("stationary", BoolArgumentType.bool())
                            .then(argument("sensingRange", IntegerArgumentType.integer(0))
                                    .then(argument("lookAround", BoolArgumentType.bool())
                                            .then(argument("isLeader", BoolArgumentType.bool())
                                                    .then(argument("followLeader", BoolArgumentType.bool())
                                                            .then(argument("followPlayer", BoolArgumentType.bool()).executes((context) -> deploy(side, context))))))));

            type(command, side, "guard", builder, (context) -> deploy(side, context, 0, true, false, false, false, false));
            type(command, side, "ward", builder, (context) -> deploy(side, context, 0, true, true, false, false, false));
            type(command, side, "reinforce", builder, (context) -> deploy(side, context, 100, false, true, false, false, false));
            type(command, side, "skirmish", builder, (context) -> deploy(side, context, 0, false, true, false, false, false));

        }
        return builder;
    }

    private static LiteralArgumentBuilder<CommandSourceStack> type(CommandBuildContext p_250122_, String side, String name, LiteralArgumentBuilder<CommandSourceStack> builder, ArgumentBuilder<CommandSourceStack, ?> args)
    {
        return builder.then(literal(side).then(literal(name)
                .then(argument("weapon", ItemArgument.item(p_250122_))
                .then(argument("pos", Vec3Argument.vec3())
                .then(argument("health", FloatArgumentType.floatArg(1))
                .then(argument("yaw", FloatArgumentType.floatArg())
                .then(argument("pitch", FloatArgumentType.floatArg())
                .then(argument("speed", FloatArgumentType.floatArg(0))
                .then(argument("sightRange", IntegerArgumentType.integer(0))
                .then(argument("hearingRange", IntegerArgumentType.integer(0, 30))
                .then(argument("notifyNearby", IntegerArgumentType.integer(0))
                .then(argument("difficultyScaledCounted", BoolArgumentType.bool())
                .then(argument("count", IntegerArgumentType.integer(1))
                .then(args))))))))))))));
    }

    private static LiteralArgumentBuilder<CommandSourceStack> type(CommandBuildContext p_250122_, String side, String name, LiteralArgumentBuilder<CommandSourceStack> builder, Command<CommandSourceStack> executes)
    {
        return builder.then(literal(side).then(literal(name)
                .then(argument("weapon", ItemArgument.item(p_250122_))
                .then(argument("pos", Vec3Argument.vec3())
                .then(argument("health", FloatArgumentType.floatArg(1))
                .then(argument("yaw", FloatArgumentType.floatArg())
                .then(argument("pitch", FloatArgumentType.floatArg())
                .then(argument("speed", FloatArgumentType.floatArg(0))
                .then(argument("sightRange", IntegerArgumentType.integer(0))
                .then(argument("hearingRange", IntegerArgumentType.integer(0, RoledSidedSmartAIEntity.MAX_HEARING_RANGE))
                .then(argument("notifyNearby", IntegerArgumentType.integer(0))
                .then(argument("difficultyScaledCounted", BoolArgumentType.bool())
                .then(argument("count", IntegerArgumentType.integer(1))
                .executes(executes))))))))))))));
    }

    private static int deploy(String side, CommandContext<CommandSourceStack> context) throws CommandSyntaxException
    {
        return deploy(side, context, IntegerArgumentType.getInteger(context, "sensingRange"), BoolArgumentType.getBool(context, "stationary"), BoolArgumentType.getBool(context, "lookAround"), BoolArgumentType.getBool(context, "isLeader"), BoolArgumentType.getBool(context, "followLeader"), BoolArgumentType.getBool(context, "followPlayer"));
    }

    private static int deploy(String side, CommandContext<CommandSourceStack> context, int sensingRange, boolean stationary, boolean lookAround, boolean isLeader, boolean followLeader, boolean followPlayer) throws CommandSyntaxException
    {
        try
        {
            CommandSourceStack source = context.getSource();
            ServerLevel world = source.getLevel();
            AVAWeaponUtil.TeamSide team = AVAWeaponUtil.TeamSide.fromName(side).orElseThrow(ERROR_FAILED::create);
            Item weapon = ItemArgument.getItem(context, "weapon").getItem();
            Vec3 vec = Vec3Argument.getVec3(context, "pos");
            float health = FloatArgumentType.getFloat(context, "health");
            float yaw = FloatArgumentType.getFloat(context, "yaw");
            float pitch = FloatArgumentType.getFloat(context, "pitch");
            float speed = FloatArgumentType.getFloat(context, "speed");
            int sightRange = IntegerArgumentType.getInteger(context, "sightRange");
            int hearingRange = IntegerArgumentType.getInteger(context, "hearingRange");
            int notifyNearby = IntegerArgumentType.getInteger(context, "notifyNearby");
            boolean difficultyScaledCount = BoolArgumentType.getBool(context, "difficultyScaledCounted");
            int count = IntegerArgumentType.getInteger(context, "count");

            if (!Level.isInSpawnableBounds(BlockPos.containing(vec)))
                throw AVAConstants.INVALID_POSITION.create();

            if (difficultyScaledCount)
            {
                int expected = AVAServerConfig.EXPECTED_PLAYERS.get();
                if (expected > 0 && count > 1)
                    count *= (Math.max(1, (float) world.players().size()) / expected);
            }

            for (int i = 0; i < count; i++)
            {
                RoledSidedSmartAIEntity entity = new RoledSidedSmartAIEntity(world);
                entity.role.updateAll(team, sightRange, sensingRange, hearingRange, stationary, lookAround, isLeader, followLeader, followPlayer, notifyNearby);
                entity.registerGoals();
                entity.moveTo(vec.x, vec.y, vec.z, yaw, pitch);

                entity.finalizeSpawn(source.getLevel(), source.getLevel().getCurrentDifficultyAt(entity.blockPosition()), MobSpawnType.COMMAND, null);

                entity.getAttribute(Attributes.MAX_HEALTH).setBaseValue(health);
                entity.setHealth(entity.getMaxHealth());
                entity.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(entity.getAttribute(Attributes.MOVEMENT_SPEED).getBaseValue() * speed);
                ItemStack stack = new ItemStack(weapon);
                if (weapon instanceof AVAItemGun)
                    ((AVAItemGun) weapon).forceReload(stack);
                entity.setItemInHand(InteractionHand.MAIN_HAND, stack);

                if (!world.tryAddFreshEntityWithPassengers(entity))
                    throw ERROR_DUPLICATE_UUID.create();
            }
            int finalCount = count;
            source.sendSuccess(() -> Component.translatable("ava.chat.deployed_ai", finalCount, vec.toString()), false);
            return count;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return 0;
        }
    }
}
