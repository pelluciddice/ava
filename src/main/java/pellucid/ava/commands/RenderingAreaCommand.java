package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.LiteralMessage;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class RenderingAreaCommand
{
    private static final SimpleCommandExceptionType DUPLICATED = new SimpleCommandExceptionType(new LiteralMessage("Duplicated Name"));
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = Commands.literal("renderingArea")
                .requires(player -> player.hasPermission(2));
        builder.then(Commands.literal("add")
                .then(Commands.argument("name", StringArgumentType.string())
                        .then(Commands.argument("from", BlockPosArgument.blockPos())
                                .then(Commands.argument("to", BlockPosArgument.blockPos())
                                        .executes((context) -> {
                                            AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                                            String name = StringArgumentType.getString(context, "name");
                                            BlockPos from = BlockPosArgument.getSpawnablePos(context, "from");
                                            BlockPos to = BlockPosArgument.getSpawnablePos(context, "to");
                                            if (cap.renderingArea.stream().noneMatch((area) -> area.getName().equals(name)))
                                                cap.renderingArea.add(new Area(name, from, to));
                                            else
                                                throw DUPLICATED.create();
                                            return 1;
                                        })))));
        builder.then(Commands.literal("remove")
                .then(Commands.argument("name", StringArgumentType.string())
                        .executes((context) -> {
                            AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                            String name = StringArgumentType.getString(context, "name");
                            if (cap.renderingArea.removeIf((area) -> area.getName().equals(name)))
                                syncWithClient(context.getSource().getLevel());
                            return 1;
                        })));
        builder.then(Commands.literal("enable")
                .then(Commands.argument("name", StringArgumentType.string())
                        .executes((context) -> {
                            AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                            String name = StringArgumentType.getString(context, "name");
                            cap.renderingArea.stream().filter((area) -> area.getName().equals(name)).findFirst().ifPresent((area) -> {
                                if (!area.isEnabled())
                                {
                                    area.enable();
                                    syncWithClient(context.getSource().getLevel());
                                }
                            });
                            return 1;
                        })));
        builder.then(Commands.literal("disable")
                .then(Commands.argument("name", StringArgumentType.string())
                        .executes((context) -> {
                            AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                            String name = StringArgumentType.getString(context, "name");
                            cap.renderingArea.stream().filter((area) -> area.getName().equals(name)).findFirst().ifPresent((area) -> {
                                if (area.isEnabled())
                                {
                                    area.disable();
                                    syncWithClient(context.getSource().getLevel());
                                }
                            });
                            return 1;
                        })));
        builder.then(Commands.literal("disableAll")
                .executes((context) -> {
                    AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                    List<String> changed = new ArrayList<>();
                    for (Area area : cap.renderingArea)
                    {
                        if (area.isEnabled())
                        {
                            area.disable();
                            changed.add(area.getName());
                        }
                    }
                    if (!changed.isEmpty())
                        syncWithClient(context.getSource().getLevel());
                    return 1;
                }));
        builder.then(Commands.literal("enableAll")
                .executes((context) -> {
                    AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                    List<String> changed = new ArrayList<>();
                    for (Area area : cap.renderingArea)
                    {
                        if (!area.isEnabled())
                        {
                            area.enable();
                            changed.add(area.getName());
                        }
                    }
                    if (!changed.isEmpty())
                        syncWithClient(context.getSource().getLevel());
                    return 1;
                }));
        builder.then(Commands.literal("list")
                .executes((context) -> {
                    AVAWorldData cap = AVAWorldData.getInstance(context.getSource().getLevel());
                    Consumer<String> sender = (text) -> context.getSource().sendSuccess(() -> Component.literal(text), false);
                    sender.accept("-------Rendering Areas-------");
                    for (Area area : cap.renderingArea)
                    {
                        sender.accept("Name: " + area.getName());
                        sender.accept("Enabled: " + area.isEnabled());
                        sender.accept("From: " + area.getFrom());
                        sender.accept("To: " + area.getTo());
                    }
                    return 1;
                }));
        return builder;
    }

    public static void syncWithClient(Level world)
    {
        AVAWorldData.getInstance(world).setRequireRenderUpdate(true);
//        AVAPlayerControls.syncWorldCapWithClients((ServerLevel) world);
    }

    public static class Area
    {
        private final String name;
        private final BlockPos from;
        private final BlockPos to;
        private boolean enabled = false;

        public Area(CompoundTag compound)
        {
            this(DataTypes.STRING.read(compound, "name"), DataTypes.BLOCKPOS.read(compound, "from"), DataTypes.BLOCKPOS.read(compound, "to"));
            this.enabled = DataTypes.BOOLEAN.read(compound, "enabled");
        }

        public Area(String name, BlockPos from, BlockPos to)
        {
            this.name = name;
            this.from = AVACommonUtil.minPos(from, to);
            this.to = AVACommonUtil.maxPos(from, to);
        }

        public CompoundTag write()
        {
            CompoundTag compound = new CompoundTag();
            DataTypes.STRING.write(compound, "name", name);
            DataTypes.BLOCKPOS.write(compound, "from", from);
            DataTypes.BLOCKPOS.write(compound, "to", to);
            DataTypes.BOOLEAN.write(compound, "enabled", enabled);
            return compound;
        }

        public boolean isEnabled()
        {
            return enabled;
        }

        public void enable()
        {
            enabled = true;
        }

        public void disable()
        {
            enabled = false;
        }

        public String getName()
        {
            return name;
        }

        public BlockPos getFrom()
        {
            return from;
        }

        public BlockPos getTo()
        {
            return to;
        }

        @Override
        public String toString()
        {
            return super.toString() + "{" +
                    "name='" + name + '\'' +
                    ", from=" + from +
                    ", to=" + to +
                    ", enabled=" + enabled +
                    '}';
        }
    }
}
