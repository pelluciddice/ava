package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.TeamArgument;
import net.minecraft.commands.arguments.coordinates.Vec3Argument;
import net.minecraft.network.chat.Component;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.player.PositionWithRotation;

import java.util.ArrayList;
import java.util.Locale;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class TeamSpawnpointCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return literal("teamspawn")
                .requires(player -> player.hasPermission(2))
                .then(literal("add")
                .then(argument("team", TeamArgument.team())
                .then(argument("pos", Vec3Argument.vec3())
                .then(argument("yaw", IntegerArgumentType.integer())
                .then(argument("pitch", IntegerArgumentType.integer())
                .then(argument("radius", IntegerArgumentType.integer(-1))
                        .executes((context) -> {
                            String team = TeamArgument.getTeam(context, "team").getName();
                            Vec3 vec = Vec3Argument.getVec3(context, "pos");
                            int radius = IntegerArgumentType.getInteger(context, "radius");
                            AVAWorldData.getInstance(context.getSource().getLevel()).teamSpawns.computeIfAbsent(team, (n) -> new ArrayList<>()).add(new PositionWithRotation(vec, IntegerArgumentType.getInteger(context, "yaw"), IntegerArgumentType.getInteger(context, "pitch"), radius));
                            context.getSource().sendSuccess(() -> Component.translatable("ava.chat.teamspawn_added", team, vec.toString(), radius), false);
                            return 1;
                        })
                ))))))
                .then(literal("remove")
                .then(argument("team", TeamArgument.team())
                .executes((context) -> {
                    String team = TeamArgument.getTeam(context, "team").getName();
                    AVAWorldData.getInstance(context.getSource().getLevel()).teamSpawns.remove(team);
                    context.getSource().sendSuccess(() -> Component.translatable("ava.chat.teamspawn_removed", team), false);
                    return 1;
                })))
                .then(literal("list")
                        .executes((context) -> {
                            AVAWorldData.getInstance(context.getSource().getLevel()).teamSpawns.forEach((team, positions) -> {
                                context.getSource().sendSuccess(() -> Component.literal(team + ": " + positions.toString()), false);
                            });
                            return 1;
                        }))
                .then(literal("spawnRestricted")
                .then(argument("value", BoolArgumentType.bool())
                .executes((context) -> {
                    AVAServerConfig.SPAWN_RESTRICTION.set(BoolArgumentType.getBool(context, "value"));
                    return 1;
                })));
    }
}
