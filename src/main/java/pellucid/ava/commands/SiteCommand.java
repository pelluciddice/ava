package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.network.chat.Component;
import pellucid.ava.config.AVAServerConfig;

import java.util.Locale;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class SiteCommand
{
    private static final String[] SITES = new String[]{"a", "b"};
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("site")
                .requires(player -> player.hasPermission(2));
        for (String site : SITES)
        {
            builder.then(literal(site)
                    .then(literal("radius")
                            .then(argument("value", IntegerArgumentType.integer(-1, 75))
                                    .executes((context) -> {
                                        int radius = IntegerArgumentType.getInteger(context, "value");
                                        (site.equals("a") ? AVAServerConfig.A_SITE_RADIUS : AVAServerConfig.B_SITE_RADIUS).set(radius);
                                        context.getSource().sendSuccess(() -> Component.translatable("ava.chat.site_radius", site.toUpperCase(Locale.ROOT), radius), true);
                                        return radius;
                                    })))
                    .then(literal("height")
                            .then(argument("value", IntegerArgumentType.integer(-1, 75))
                                    .executes((context) -> {
                                        int height = IntegerArgumentType.getInteger(context, "value");
                                        (site.equals("a") ? AVAServerConfig.A_SITE_HEIGHT : AVAServerConfig.B_SITE_HEIGHT).set(height);
                                        context.getSource().sendSuccess(() -> Component.translatable("ava.chat.site_height", site.toUpperCase(Locale.ROOT), height), true);
                                        return height;
                                    }))));
        }
        return builder;
    }
}
