package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.util.AVACommonUtil;

public class FriendlyFireCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return Commands.literal("enableFriendlyFire")
                .requires(player -> player.hasPermission(2))
                .then(Commands.argument("value", BoolArgumentType.bool())
                .executes((context) ->
                {
                    boolean bool = BoolArgumentType.getBool(context, "value");
                    AVACrossWorldData.getInstance().friendlyFire = bool;
                    AVACommonUtil.broadcastSystemMessage(Component.translatable("ava.chat.friendly_fire", bool));
                    return bool ? 1 : 0;
                }));
    }
}
