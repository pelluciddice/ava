package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.commands.CommandBuildContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.CompoundTagArgument;
import net.minecraft.commands.arguments.ResourceArgument;
import net.minecraft.commands.arguments.coordinates.Vec3Argument;
import net.minecraft.commands.synchronization.SuggestionProviders;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import java.util.concurrent.atomic.AtomicBoolean;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class DeployCommand
{
    private static final SimpleCommandExceptionType ERROR_FAILED = new SimpleCommandExceptionType(Component.translatable("commands.summon.failed"));
    private static final SimpleCommandExceptionType ERROR_DUPLICATE_UUID = new SimpleCommandExceptionType(Component.translatable("commands.summon.failed.uuid"));

    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher, CommandBuildContext p_250122_)
    {
        return literal("deploy")
                .requires(player -> player.hasPermission(2))
                .then(argument("entity", ResourceArgument.resource(p_250122_, Registries.ENTITY_TYPE)).suggests(SuggestionProviders.SUMMONABLE_ENTITIES)
                .then(argument("pos", Vec3Argument.vec3())
                .then(argument("radius", IntegerArgumentType.integer(0))
                .then(argument("count", IntegerArgumentType.integer(1))
                    .executes((source) -> serializeAndExecute(source, true, false))
                .then(argument("difficultyScaledCount", BoolArgumentType.bool())
                        .executes((source) -> serializeAndExecute(source, false, false))
                .then(argument("nbt", CompoundTagArgument.compoundTag())
                        .executes((source) -> serializeAndExecute(source, BoolArgumentType.getBool(source, "difficultyScaledCount"), true))
                ))))))
                .then(literal("expected_players")
                        .then(argument("count", IntegerArgumentType.integer(-1, Integer.MAX_VALUE))
                                .executes((source) -> {
                                    AVAServerConfig.EXPECTED_PLAYERS.set(IntegerArgumentType.getInteger(source, "count"));
                                    return AVAServerConfig.EXPECTED_PLAYERS.get();
                                })));
    }

    private static int serializeAndExecute(CommandContext<CommandSourceStack> context, boolean difficultyScaledCount, boolean hasNBT) throws CommandSyntaxException
    {
        return deployEntity(context.getSource(),
                ResourceArgument.getSummonableEntityType(context, "entity"),
                Vec3Argument.getVec3(context, "pos"),
                IntegerArgumentType.getInteger(context, "radius"),
                IntegerArgumentType.getInteger(context, "count"),
                difficultyScaledCount,
                hasNBT ? CompoundTagArgument.getCompoundTag(context, "nbt") : new CompoundTag(),
                !hasNBT);
    }

    private static int deployEntity(CommandSourceStack source, Holder.Reference<EntityType<?>> type, Vec3 vec, int radius, int count, boolean difficultyScaledCount, CompoundTag nbt, boolean randomizeProperties) throws CommandSyntaxException
    {
        ServerLevel world = source.getLevel();
        if (!Level.isInSpawnableBounds(BlockPos.containing(vec)))
            throw AVAConstants.INVALID_POSITION.create();
        AtomicBoolean byTeamSpawn = new AtomicBoolean(false);
        DataTypes.STRING.write(nbt, "id", type.key().location().toString());

        if (difficultyScaledCount)
        {
            int expected = AVAServerConfig.EXPECTED_PLAYERS.get();
            if (expected > 0 && count > 1)
                count *= (Math.max(1, (float) world.players().size()) / expected);
        }

        for (int i = 0; i < count; i++)
        {
            CompoundTag compound = nbt.copy();
            Entity entity = EntityType.loadEntityRecursive(compound, world, (entity2) ->
            {
                if (AVAWeaponUtil.setEntityPosByTeamSpawn(entity2, radius /*spawnRestriction*/))
                    byTeamSpawn.set(true);
                else
                    entity2.moveTo(vec.x + AVAWeaponUtil.randomOffset(radius), vec.y, vec.z + AVAWeaponUtil.randomOffset(radius), entity2.getYRot(), entity2.getXRot());
                return entity2;
            });
            if (entity == null)
                throw ERROR_FAILED.create();

            if (randomizeProperties && entity instanceof Mob)
                ((Mob) entity).finalizeSpawn(source.getLevel(), source.getLevel().getCurrentDifficultyAt(entity.blockPosition()), MobSpawnType.COMMAND, null);

            if (!world.tryAddFreshEntityWithPassengers(entity))
                throw ERROR_DUPLICATE_UUID.create();

            if (entity instanceof SidedSmartAIEntity)
                ((SidedSmartAIEntity) entity).findFarTarget();
        }
        int finalCount = count;
        if (byTeamSpawn.get())
            source.sendSuccess(() -> Component.translatable("ava.chat.deployed_team", finalCount), false);
        else
            source.sendSuccess(() -> Component.translatable("ava.chat.deployed_mob", finalCount, vec.toString()), false);
        return count;
    }
}
