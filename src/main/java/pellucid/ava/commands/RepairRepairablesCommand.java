package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.server.level.ServerLevel;
import pellucid.ava.cap.AVAWorldData;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class RepairRepairablesCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return literal("setRepairablesRepaired")
                .requires(player -> player.hasPermission(2))
                .then(argument("value", BoolArgumentType.bool())
                        .executes((context) -> {
                            ServerLevel world = context.getSource().getLevel();
                            boolean repaired = BoolArgumentType.getBool(context, "value");
                            AVAWorldData.getInstance(world).repairAll(world, repaired);
                            return 1;
                        }));
    }
}
