package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.network.PacketDistributor;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.INBTSerializable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Predicate;

public class PlayTypeCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = Commands.literal("setPlaymode")
                .requires(player -> player.hasPermission(2));
        for (FunctionType function : FunctionType.values())
        {
            for (PlayType type : PlayType.values())
                if (type != PlayType.FREE)
                    builder.then(Commands.literal(type.getDisplayName()).then(Commands.literal(function.getDisplayName()).executes((context) -> {
                        PlayMode mode = AVACrossWorldData.getInstance().playMode;
                        mode.apply(type, function);
                        DataToClientMessage.playMode(PacketDistributor::sendToAllPlayers);
                        AVACommonUtil.broadcastSystemMessage(Component.translatable("ava.chat.current_play_mode", mode.allowed));
                        return type.ordinal();
                    })));
            builder.then(Commands.literal(PlayType.FREE.getDisplayName()).executes((context) -> {
                PlayMode mode = AVACrossWorldData.getInstance().playMode;
                AVACommonUtil.merge(mode.allowed, Arrays.stream(PlayType.values()).toList());
                DataToClientMessage.playMode(PacketDistributor::sendToAllPlayers);
                AVACommonUtil.broadcastSystemMessage(Component.translatable("ava.chat.current_play_mode", "All"));
                return PlayType.FREE.ordinal();
            }));
        }
        return builder;
    }

    public enum PlayType implements Predicate<Item>
    {
        SNIPER(AVAWeaponUtil.Classification.SNIPER),
        SNIPER_SEMI(AVAWeaponUtil.Classification.SNIPER_SEMI),
        RIFLE(AVAWeaponUtil.Classification.RIFLE),
        SUB_MACHINEGUN(AVAWeaponUtil.Classification.SUB_MACHINEGUN),
        SHOTGUN(AVAWeaponUtil.Classification.SHOTGUN),
        PISTOL(AVAWeaponUtil.Classification.PISTOL),
        PISTOL_AUTO(AVAWeaponUtil.Classification.PISTOL_AUTO),
        MELEE_WEAPON(AVAWeaponUtil.Classification.MELEE_WEAPON),
        PROJECTILE(AVAWeaponUtil.Classification.PROJECTILE),
        SPECIAL_WEAPON(AVAWeaponUtil.Classification.SPECIAL_WEAPON),
        FREE(null)
        ;

        private final AVAWeaponUtil.Classification classification;
        private final String displayName;

        PlayType(AVAWeaponUtil.Classification classification)
        {
            this.classification = classification;
            displayName = AVACommonUtil.toDisplayStringForEnum(this);
        }

        public String getDisplayName()
        {
            return displayName;
        }

        public boolean isFree()
        {
            return this == FREE;
        }

        @Override
        public boolean test(Item item)
        {
            if (!isFree())
            {
                if (item instanceof IClassification classification)
                    return classification.getClassification() == this.classification;
                return false;
            }
            return true;
        }
    }

    enum FunctionType
    {
        ONLY((list, e) -> {
            list.clear();
            list.add(e);
        }),
        ONLY_NO((list, e) -> {
            list.clear();
            list.addAll(Arrays.stream(PlayType.values()).toList());
            list.remove(e);
        }),
        WITH((list, e) -> {
            if (!list.contains(e))
                list.add(e);
        }),
        WITH_NO(List::remove)
        ;

        private final String displayName;
        private final BiConsumer<List<PlayType>, PlayType> action;

        FunctionType(BiConsumer<List<PlayType>, PlayType> action)
        {
            this.action = action;
            displayName = AVACommonUtil.toDisplayStringForEnum(this);
        }

        private static void unFree(List<PlayType> list)
        {
            list.remove(PlayType.FREE);
        }

        public String getDisplayName()
        {
            return displayName;
        }

        public void action(PlayMode playMode, List<PlayType> list, PlayType type)
        {
            playMode.modified = true;
            action.accept(list, type);
            unFree(list);
        }
    }

    public static class PlayMode implements Predicate<Item>, BiFunction<PlayType, FunctionType, PlayMode>, INBTSerializable<CompoundTag>
    {
        private final List<PlayType> allowed = new ArrayList<>() {{
            add(PlayType.FREE);
        }};

        private boolean modified = false;
        public PlayMode()
        {

        }

        @Override
        public boolean test(Item item)
        {
            return new ArrayList<>(allowed).stream().anyMatch((type) -> type.test(item));
        }

        public PlayMode apply(PlayType playType, ModConfigSpec.BooleanValue value)
        {
            (value.get() ? FunctionType.WITH : FunctionType.WITH_NO).action(this, allowed, playType);
            return this;
        }

        @Override
        public PlayMode apply(PlayType playType, FunctionType functionType)
        {
            functionType.action(this, allowed, playType);
            return this;
        }

        @Override
        public CompoundTag serializeNBT()
        {
            CompoundTag compound = new CompoundTag();
            ListTag list = new ListTag();
            for (PlayType type : new ArrayList<>(allowed))
                list.add(StringTag.valueOf(type.name()));
            compound.put("types", list);
            DataTypes.BOOLEAN.write(compound, "modified", modified);
            return compound;
        }

        @Override
        public void deserializeNBT(CompoundTag nbt)
        {
            modified = DataTypes.BOOLEAN.read(nbt, "modified");
            if (modified)
                allowed.clear();
            modified = true;
            Set<PlayType> allowedSet = new HashSet<>();
            for (Tag type : nbt.getList("types", Tag.TAG_STRING))
                allowedSet.add(PlayType.valueOf(type.getAsString()));
            allowed.addAll(allowedSet);
            allowed.sort(Comparator.comparing(PlayType::getDisplayName));
        }

        @Override
        public String toString()
        {
            return "PlayMode{" +
                    "allowed=" + allowed +
                    '}';
        }
    }
}
