package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVACommonUtil;

import java.util.Locale;

public class RecoilRefundTypeCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = Commands.literal("recoilCompensationType")
                .requires(player -> player.hasPermission(2));
        for (RefundType type : RefundType.values())
        {
            builder.then(Commands.literal(type.toString().toLowerCase(Locale.ROOT)).executes((context) -> {
                AVACrossWorldData.getInstance().recoilRefundType = type;
                DataToClientMessage.recoilRefundType();
                AVACommonUtil.broadcastSystemMessage(Component.translatable("ava.chat.recoil_type", type.toString().toLowerCase(Locale.ROOT)));
                return type.ordinal();
            }));
        }
        return builder;
    }

    public enum RefundType
    {
        NONE,
        LINEAR,
        EXPONENTIAL,
    }
}
