package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import javax.annotation.Nullable;
import java.util.Collection;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class BroadcastTextCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("broadcast").requires((player) -> player.hasPermission(2));
        builder.then(literal("string")
                .then(argument("duration", IntegerArgumentType.integer(20))
                        .then(argument("text", StringArgumentType.greedyString())
                                .executes((context) -> {
                                    return broadcastTo(StringArgumentType.getString(context, "text"), false, IntegerArgumentType.getInteger(context, "duration"), null);
                                }))));
        builder.then(literal("key")
                .then(argument("duration", IntegerArgumentType.integer(20))
                        .then(argument("text", StringArgumentType.greedyString())
                                .executes((context) -> {
                                    return broadcastTo(StringArgumentType.getString(context, "text"), true, IntegerArgumentType.getInteger(context, "duration"), null);
                                }))));
        builder.then(literal("string")
                .then(argument("duration", IntegerArgumentType.integer(20))
                        .then(argument("text", StringArgumentType.greedyString())
                                .then(argument("target", EntityArgument.players())
                                    .executes((context) -> {
                                        return broadcastTo(StringArgumentType.getString(context, "text"), false, IntegerArgumentType.getInteger(context, "duration"), EntityArgument.getPlayers(context, "target"));
                                    })))));
        builder.then(literal("key")
                .then(argument("duration", IntegerArgumentType.integer(20))
                        .then(argument("text", StringArgumentType.greedyString())
                                .executes((context) -> {
                                    return broadcastTo(StringArgumentType.getString(context, "text"), true, IntegerArgumentType.getInteger(context, "duration"), null);
                                }))));
        builder.then(literal("key")
                .then(argument("duration", IntegerArgumentType.integer(20))
                        .then(argument("text", StringArgumentType.greedyString())
                                .then(argument("target", EntityArgument.players())
                                        .executes((context) -> {
                                            return broadcastTo(StringArgumentType.getString(context, "text"), true, IntegerArgumentType.getInteger(context, "duration"), EntityArgument.getPlayers(context, "target"));
                                        })))));
        return builder;
    }

    public static void broadcastToTeam(ServerLevel world, String key, int duration, @Nullable AVAWeaponUtil.TeamSide team, String... args)
    {
        BroadcastTextCommand.broadcastTo(key, true, duration, team != null ? AVACommonUtil.cast(AVAWeaponUtil.getPlayersForSide(world, team), ServerPlayer.class) : null, args);
    }

    public static int broadcastTo(String text, boolean key, int duration, @Nullable Collection<ServerPlayer> players, String... args)
    {
        DataToClientMessage.broadcastText(text, key, duration, players, args);
        return duration;
    }
}
