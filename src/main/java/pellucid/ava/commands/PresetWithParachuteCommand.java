package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import pellucid.ava.config.AVAServerConfig;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class PresetWithParachuteCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return literal("presetWithParachute")
                .requires(player -> player.hasPermission(2))
                .then(argument("value", BoolArgumentType.bool())
                        .executes((context) -> {
                            AVAServerConfig.PRESET_WITH_PARACHUTE.set(BoolArgumentType.getBool(context, "value"));
                            return 1;
                        })
                );
    }
}
