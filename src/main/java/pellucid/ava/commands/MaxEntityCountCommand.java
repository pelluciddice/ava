package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import pellucid.ava.AVA;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.concurrent.atomic.AtomicInteger;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

@EventBusSubscriber
public class MaxEntityCountCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return literal("maxEntity")
                .requires(player -> player.hasPermission(2))
                        .then(argument("count", IntegerArgumentType.integer(-1))
                                .executes((context) -> {
                                    int count = IntegerArgumentType.getInteger(context, "count");
                                    AVAServerConfig.MAX_ENTITY_OF_SAME_TYPE.set(count);
                                    context.getSource().sendSuccess(() -> Component.translatable("ava.chat.max_entity_count", count), true);
                                    return count;
                                }));
    }

    private static int CD = 0;
    @SubscribeEvent
    public static void onEntityJoinWorld(EntityJoinLevelEvent event)
    {
        Entity entity = event.getEntity();
        EntityType<?> type = entity.getType();
        Level world = event.getLevel();
        int max = AVAServerConfig.MAX_ENTITY_OF_SAME_TYPE.get();
        if (max != -1 && world instanceof ServerLevel && !(entity instanceof Player))
        {
            AtomicInteger count = new AtomicInteger(0);
            ((ServerLevel) world).getAllEntities().forEach((e) -> {
                if (e.getType() == type && AVAWeaponUtil.isValidEntity(e))
                    count.addAndGet(1);
            });
            if (count.get() >= max)
            {
                event.setCanceled(true);
                if (CD == 0)
                {
                    AVA.LOGGER.warn("Entity " + type + " reached max count of " + max + " new entities are not added");
                    CD = 40;
                }
                CD--;
            }
        }
    }
}
