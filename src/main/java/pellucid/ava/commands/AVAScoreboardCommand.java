package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.ChatFormatting;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.EntityArgument;
import net.minecraft.commands.arguments.ObjectiveArgument;
import net.minecraft.commands.arguments.TeamArgument;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.commands.arguments.coordinates.Vec3Argument;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.scores.PlayerTeam;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.gamemodes.modes.GameMode;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.gamemodes.scoreboard.AVAPlayerStat;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.player.PositionWithRotation;

import java.util.Collection;
import java.util.Locale;
import java.util.function.Consumer;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class AVAScoreboardCommand
{
    private static final String[] TEAMS = new String[] {"A", "B"};
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("scoreboard").requires((player) -> player.hasPermission(2));
        for (String teamName : TEAMS)
        {
            builder.then(literal("setTeam").then(literal(teamName).then(argument("team", TeamArgument.team()).executes((context) -> {
                AVACrossWorldData cap = AVACrossWorldData.getInstance();
                PlayerTeam team = TeamArgument.getTeam(context, "team");
                boolean aTeam = teamName.equals("A");
                if (aTeam)
                    cap.scoreboardManager.setTeamA(team.getName());
                else
                    cap.scoreboardManager.setTeamB(team.getName());
                DataToClientMessage.scoreboard();
                return aTeam ? 1 : 0;
            }))));
        }
        builder.then(literal("removeOfflinePlayersFromTeams").executes((context) -> {
            AVACrossWorldData cap = AVACrossWorldData.getInstance();
            cap.scoreboardManager.removeOfflinePlayersFromTeams(context.getSource().getLevel());
            DataToClientMessage.scoreboard();
            return 1;
        }));
        builder.then(literal("clearScore").then(argument("players", EntityArgument.players()).executes((context) -> {
            AVACrossWorldData cap = AVACrossWorldData.getInstance();
            Collection<ServerPlayer> players = EntityArgument.getPlayers(context, "players");
            for (ServerPlayer player : players)
                cap.scoreboardManager.getStats().put(player.getUUID(), new AVAPlayerStat());
            DataToClientMessage.scoreboard();
            return players.size();
        })));
        Direction.Plane.HORIZONTAL.stream().forEach((direction) -> {
            builder.then(literal("updateMap").then(argument("from", BlockPosArgument.blockPos()).then(argument("to", BlockPosArgument.blockPos()).then(literal(direction.getName()).then(argument("scale", FloatArgumentType.floatArg(0.0F)).then(argument("heightSensitive", BoolArgumentType.bool()).then(argument("transparentAir", BoolArgumentType.bool()).executes((context) -> {
                AVACrossWorldData.getInstance().scoreboardManager.updateMap(context.getSource().getLevel(), BlockPosArgument.getSpawnablePos(context, "from"), BlockPosArgument.getSpawnablePos(context, "to"), direction, FloatArgumentType.getFloat(context, "scale"), BoolArgumentType.getBool(context, "heightSensitive"), BoolArgumentType.getBool(context, "transparentAir"));
                return 1;
            }))))))));
        });
        builder.then(literal("updateMap").then(literal("setSitePos").then(argument("aPos", BlockPosArgument.blockPos()).then(argument("bPos", BlockPosArgument.blockPos()).executes((context) -> {
            ServerLevel world = context.getSource().getLevel();
            AVAWorldData.getInstance(world).sitesMark.set(BlockPosArgument.getSpawnablePos(context, "aPos"), BlockPosArgument.getSpawnablePos(context, "bPos"));
            DataToClientMessage.sitePos(world);
            return 1;
        })))));
        builder.then(literal("gamemode")
                .then(literal("setRunning")
                        .then(argument("value", BoolArgumentType.bool())
                                .executes((context) -> {
                                    boolean running = AVACrossWorldData.getInstance().scoreboardManager.getGamemode().setRunning(context.getSource().getLevel(), BoolArgumentType.getBool(context, "value"));
                                    DataToClientMessage.gamemode();
                                    return running ? 1 : 0;
                                }))));
        builder.then(literal("gamemode")
                .then(literal("isRunning")
                            .executes((context) -> {
                                boolean running = AVACrossWorldData.getInstance().scoreboardManager.getGamemode().isRunning();
                                context.getSource().sendSuccess(() -> Component.literal(String.valueOf(running)), false);
                                return running ? 1 : 0;
                            })));
        builder.then(literal("gamemode")
                .then(literal("detail")
                        .executes((context) -> {
                            context.getSource().sendSuccess(() -> Component.literal(AVACrossWorldData.getInstance().scoreboardManager.getGamemode().toString()), true);
                            return 1;
                        })));
        builder.then(literal("gamemode")
                .then(literal("setTimer")
                        .then(argument("name", StringArgumentType.word())
                                .then(argument("obj", ObjectiveArgument.objective())
                                        .executes((context) -> {
                                            try
                                            {
                                                AVACrossWorldData.getInstance().scoreboardManager.getGamemode().setTimer(
                                                        StringArgumentType.getString(context, "name"),
                                                        ObjectiveArgument.getObjective(context, "obj").getName()
                                                );
                                                DataToClientMessage.gamemode();
                                            }
                                            catch (CommandSyntaxException e)
                                            {
                                                throw new RuntimeException(e);
                                            }
                                            return 1;
                                        })))));
        builder.then(literal("gamemode")
                .then(literal("defaultTeams")
                        .executes((context) -> {
                            ServerLevel world = context.getSource().getLevel();
                            PlayerTeam team = world.getScoreboard().addPlayerTeam("eu");
                            team.setColor(ChatFormatting.RED);
                            team.setNameTagVisibility(PlayerTeam.Visibility.NEVER);
                            team.setAllowFriendlyFire(true);
                            team.setDisplayName(Component.literal("EU"));
                            AVACrossWorldData.getInstance().scoreboardManager.setTeamA(team.getName());
                            team = world.getScoreboard().addPlayerTeam("nrf");
                            team.setColor(ChatFormatting.BLUE);
                            team.setNameTagVisibility(PlayerTeam.Visibility.NEVER);
                            team.setAllowFriendlyFire(true);
                            team.setDisplayName(Component.literal("NRF"));
                            AVACrossWorldData.getInstance().scoreboardManager.setTeamB(team.getName());
                            DataToClientMessage.scoreboard();
                            return 1;
                        })));
        builder.then(literal("gamemode")
                .then(literal("setMaxPlayers")
                        .then(argument("maxPlayers", IntegerArgumentType.integer(1))
                                .executes((context) -> {
                                    GameModes.getRunningAVAGamemode().ifPresent((mode) -> {
                                        mode.setMaxAICount(IntegerArgumentType.getInteger(context, "maxPlayers"));
                                        DataToClientMessage.gamemode();
                                    });
                                    return 1;
                                }))));
        builder.then(literal("gamemode")
                .then(literal("setTrigger")
                        .then(argument("name", StringArgumentType.word())
                                .then(argument("obj", ObjectiveArgument.objective())
                                        .executes((context) -> {
                                            try
                                            {
                                                AVACrossWorldData.getInstance().scoreboardManager.getGamemode().setTrigger(
                                                        StringArgumentType.getString(context, "name"),
                                                        ObjectiveArgument.getObjective(context, "obj").getName()
                                                );
                                                DataToClientMessage.gamemode();
                                            }
                                            catch (CommandSyntaxException e)
                                            {
                                                throw new RuntimeException(e);
                                            }
                                            return 1;
                                        })))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.VANILLA.getName())
                        .executes((context) -> {
                            return setGameMode(GameModes.VANILLA, (mode) -> {});
                        })));
        annihilationCommands(builder);
        demolishCommands(builder);
        escortCommands(builder);
        return builder;
    }

    private static <M extends GameMode> int setGameMode(M gamemode, Consumer<M> action)
    {
        if (AVACrossWorldData.getInstance().scoreboardManager.setGamemode(gamemode))
        {
            action.accept(gamemode);
            DataToClientMessage.gamemode();
            return 1;
        }
        return 0;
    }

    public static void annihilationCommands(LiteralArgumentBuilder<CommandSourceStack> builder)
    {
        builder.then(literal("gamemode")
                .then(literal(GameModes.ANNIHILATION.getName())
                        .then(argument("targetScore", IntegerArgumentType.integer(1))
                                .then(argument("maxPlayers", IntegerArgumentType.integer(1))
                                        .then(argument("name", StringArgumentType.word())
                                                .then(argument("obj", ObjectiveArgument.objective())
                                                        .executes((context) -> {
                                                            return setGameMode(GameModes.ANNIHILATION, (mode) -> {
                                                                try
                                                                {
                                                                    mode.init(
                                                                            IntegerArgumentType.getInteger(context, "targetScore"),
                                                                            StringArgumentType.getString(context, "name"),
                                                                            ObjectiveArgument.getObjective(context, "obj").getName(),
                                                                            IntegerArgumentType.getInteger(context, "maxPlayers")
                                                                    );
                                                                }
                                                                catch (CommandSyntaxException e)
                                                                {
                                                                    throw new RuntimeException(e);
                                                                }
                                                                context.getSource().sendSuccess(() -> Component.literal(mode.toString()), true);
                                                            });
                                                        })))))));
    }

    public static void demolishCommands(LiteralArgumentBuilder<CommandSourceStack> builder)
    {
        builder.then(literal("gamemode")
                .then(literal(GameModes.DEMOLISH.getName())
                        .then(argument("targetScore", IntegerArgumentType.integer(1))
                                .then(argument("nrfAICount", IntegerArgumentType.integer(0))
                                        .then(argument("c4Spawn", Vec3Argument.vec3())
                                                .then(argument("name", StringArgumentType.word())
                                                        .then(argument("obj", ObjectiveArgument.objective())
                                                                .executes((context) -> {
                                                                    return setGameMode(GameModes.DEMOLISH, (mode) -> {
                                                                        try
                                                                        {
                                                                            mode.init(
                                                                                    IntegerArgumentType.getInteger(context, "targetScore"),
                                                                                    StringArgumentType.getString(context, "name"),
                                                                                    ObjectiveArgument.getObjective(context, "obj").getName(),
                                                                                    IntegerArgumentType.getInteger(context, "nrfAICount"),
                                                                                    Vec3Argument.getVec3(context, "c4Spawn")
                                                                            );
                                                                        }
                                                                        catch (CommandSyntaxException e)
                                                                        {
                                                                            throw new RuntimeException(e);
                                                                        }
                                                                        context.getSource().sendSuccess(() -> Component.literal(mode.toString()), true);
                                                                    });
                                                                }))))))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.DEMOLISH.getName())
                        .then(literal("setC4Spawn")
                                .then(argument("c4Spawn", Vec3Argument.vec3())
                                        .executes((context) -> {
                                            GameModes.DEMOLISH.setC4Spawn(Vec3Argument.getVec3(context, "c4Spawn"));
                                            return 1;
                                        })))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.DEMOLISH.getName())
                        .then(literal("setTargetScore")
                                .then(argument("score", IntegerArgumentType.integer(1))
                                        .executes((context) -> {
                                            GameModes.DEMOLISH.setTargetScore(IntegerArgumentType.getInteger(context, "score"));
                                            DataToClientMessage.gamemode();
                                            return 1;
                                        })))));
    }

    public static void escortCommands(LiteralArgumentBuilder<CommandSourceStack> builder)
    {
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(argument("maxPlayers", IntegerArgumentType.integer(1))
                                .executes((context) -> {
                                    return setGameMode(GameModes.ESCORT, (mode) -> {
                                        mode.setMaxAICount(IntegerArgumentType.getInteger(context, "maxPlayers"));
                                        context.getSource().sendSuccess(() -> Component.literal(mode.toString()), true);
                                        DataToClientMessage.gamemode();
                                    });
                                }))));
        for (LeopardEntity.Variant variant : LeopardEntity.Variant.values())
        {
            builder.then(literal("gamemode")
                    .then(literal(GameModes.ESCORT.getName())
                            .then(literal("setup")
                                    .then(literal(variant.name().toLowerCase(Locale.ROOT))
                                            .then(argument("speed", FloatArgumentType.floatArg(0.0F))
                                                    .executes((context) -> {
                                                        GameModes.ESCORT.tankVariant = variant;
                                                        GameModes.ESCORT.tankSpeed = FloatArgumentType.getFloat(context, "speed");
                                                        return 1;
                                                    }))))));
        }
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("path")
                                .then(literal("add")
                                        .then(argument("path", Vec3Argument.vec3())
                                                .executes((context) -> {
                                                    GameModes.ESCORT.addPath(Vec3Argument.getVec3(context, "path"));
                                                    return 1;
                                                }))))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("uavCD")
                                .then(argument("cd", IntegerArgumentType.integer(10))
                                        .executes((context) -> {
                                            GameModes.ESCORT.uavSupportCD = IntegerArgumentType.getInteger(context, "cd");
                                            return 1;
                                        })))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("path")
                                .then(literal("clear")
                                                .executes((context) -> {
                                                    GameModes.ESCORT.clearPath();
                                                    return 1;
                                                })))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("defenseLine")
                                .then(literal("addAttackSpawn")
                                        .then(argument("pathIndex", IntegerArgumentType.integer())
                                                .then(argument("attackSpawn", Vec3Argument.vec3())
                                                        .then(argument("yaw", IntegerArgumentType.integer())
                                                                .then(argument("pitch", IntegerArgumentType.integer())
                                                                        .then(argument("radius", IntegerArgumentType.integer(-1))
                                                                                .executes((context) -> {
                                                                                    GameModes.ESCORT.addDefenseLine(IntegerArgumentType.getInteger(context, "pathIndex"), new PositionWithRotation(Vec3Argument.getVec3(context, "attackSpawn"), IntegerArgumentType.getInteger(context, "yaw"), IntegerArgumentType.getInteger(context, "pitch"), IntegerArgumentType.getInteger(context, "radius")), true);
                                                                                    DataToClientMessage.gamemode();
                                                                                    return 1;
                                                                                }))))))))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("defenseLine")
                                .then(literal("finalTarget")
                                        .then(argument("pathIndex", IntegerArgumentType.integer())
                                                .executes((context) -> {
                                                    GameModes.ESCORT.addDefenseLine(IntegerArgumentType.getInteger(context, "pathIndex"), (PositionWithRotation) null, true);
                                                    DataToClientMessage.gamemode();
                                                    return 1;
                                                }))))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("defenseLine")
                                .then(literal("addDefenseSpawn")
                                        .then(argument("pathIndex", IntegerArgumentType.integer())
                                                .then(argument("defenseSpawn", Vec3Argument.vec3())
                                                        .then(argument("yaw", IntegerArgumentType.integer())
                                                                .then(argument("pitch", IntegerArgumentType.integer())
                                                                        .then(argument("radius", IntegerArgumentType.integer(-1))
                                                                                .executes((context) -> {
                                                                                    GameModes.ESCORT.addDefenseLine(IntegerArgumentType.getInteger(context, "pathIndex"), new PositionWithRotation(Vec3Argument.getVec3(context, "defenseSpawn"), IntegerArgumentType.getInteger(context, "yaw"), IntegerArgumentType.getInteger(context, "pitch"), IntegerArgumentType.getInteger(context, "radius")), false);
                                                                                    DataToClientMessage.gamemode();
                                                                                    return 1;
                                                                                }))))))))));
        builder.then(literal("gamemode")
                .then(literal(GameModes.ESCORT.getName())
                        .then(literal("defenseLine")
                                .then(literal("clear")
                                        .executes((context) -> {
                                            GameModes.ESCORT.clearDefenseLines();
                                            DataToClientMessage.gamemode();
                                            return 1;
                                        })))));
    }
}
