package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.decoration.ItemFrame;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.concurrent.atomic.AtomicInteger;

import static net.minecraft.commands.Commands.literal;

public class OrganizeItemFramesCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        return literal("organizeitemframes")
                .requires(player -> player.hasPermission(2))
                .executes((context) -> {
                    AtomicInteger itemFrames = new AtomicInteger(0);
                    ServerLevel world = context.getSource().getLevel();
                    world.getAllEntities().forEach((entity) ->
                    {
                        if (entity instanceof ItemFrame frame)
                        {
                            ItemStack stack = frame.getItem();
                            if (stack.getItem() instanceof AVAItemGun)
                            {
                                AVAWeaponUtil.clearAnimationData(stack);
                                itemFrames.addAndGet(1);
                                frame.setItem(stack);
                            }
                        }
                    });
                    return itemFrames.get();
                });
    }
}
