package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.ObjectiveArgument;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;

import static net.minecraft.commands.Commands.argument;
import static net.minecraft.commands.Commands.literal;

public class TimerCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("timer");
        builder.then(literal("add")
                .then(argument("name", StringArgumentType.word())
                        .then(argument("object", ObjectiveArgument.objective())
                                .then(argument("initial", IntegerArgumentType.integer(0))
                                        .executes((context) -> {
                                            ServerLevel world = context.getSource().getLevel();
                                            String name = StringArgumentType.getString(context, "name");
                                            String obj = ObjectiveArgument.getObjective(context, "object").getName();
                                            List<Timer> timers = AVACrossWorldData.getInstance().timers;
                                            if (timers.stream().noneMatch((timer) -> timer.getName().equals(name) && timer.getStorage().equals(obj)))
                                            {
                                                timers.add(new Timer(name, obj, IntegerArgumentType.getInteger(context, "initial")));
                                                AVAWeaponUtil.trackScoreboard(world, obj);
                                                return sync();
                                            }
                                            return 0;
                                        })))));
        builder.then(literal("remove")
                .then(argument("name", StringArgumentType.word())
                        .then(argument("object", ObjectiveArgument.objective())
                                .executes((context) -> {
                                    ServerLevel world = context.getSource().getLevel();
                                    String name = StringArgumentType.getString(context, "name");
                                    String obj = ObjectiveArgument.getObjective(context, "object").getName();
                                    AVACrossWorldData.getInstance().timers.removeIf((timer) -> timer.getName().equals(name) && timer.getStorage().equals(obj));
                                    try
                                    {
                                        AVAWeaponUtil.untrackScoreboard(world, obj);
                                    }
                                    catch (Exception ignored)
                                    {

                                    }
                                    return sync();
                                }))));
        builder.then(literal("modify")
                .then(argument("name", StringArgumentType.word())
                        .then(argument("object", ObjectiveArgument.objective())
                                .then(literal("reset")
                                        .executes((context) -> {
                                            AVACrossWorldData.getInstance().getTimer(StringArgumentType.getString(context, "name"), ObjectiveArgument.getObjective(context, "object").getName()).reset(context.getSource().getLevel());
                                            return sync();
                                        })))));
        builder.then(literal("modify")
                .then(argument("name", StringArgumentType.word())
                        .then(argument("object", ObjectiveArgument.objective())
                                .then(literal("setPaused")
                                        .then(argument("paused", BoolArgumentType.bool())
                                                .executes((context) -> {
                                                    AVACrossWorldData.getInstance().getTimer(StringArgumentType.getString(context, "name"), ObjectiveArgument.getObjective(context, "object").getName()).setPaused(BoolArgumentType.getBool(context, "paused"));
                                                    return sync();
                                                }))))));
        builder.then(literal("modify")
                .then(argument("name", StringArgumentType.word())
                        .then(argument("object", ObjectiveArgument.objective())
                                .then(literal("setDisplay")
                                        .then(argument("display", BoolArgumentType.bool())
                                                .executes((context) -> {
                                                    AVACrossWorldData.getInstance().getTimer(StringArgumentType.getString(context, "name"), ObjectiveArgument.getObjective(context, "object").getName()).setDisplaying(BoolArgumentType.getBool(context, "display"));
                                                    return sync();
                                                }))))));
        builder.then(literal("modify")
                .then(argument("name", StringArgumentType.word())
                        .then(argument("object", ObjectiveArgument.objective())
                                .then(literal("setInitial")
                                        .then(argument("value", IntegerArgumentType.integer(1))
                                                .executes((context) -> {
                                                    AVACrossWorldData.getInstance().getTimer(StringArgumentType.getString(context, "name"), ObjectiveArgument.getObjective(context, "object").getName()).setInitial(IntegerArgumentType.getInteger(context, "value"));
                                                    return sync();
                                                }))))));
        builder.then(literal("list").executes((context) -> {
            List<Timer> timers = AVACrossWorldData.getInstance().timers;
            for (Timer timer : timers)
                context.getSource().sendSuccess(() -> Component.literal(timer.toString()), false);
            if (timers.isEmpty())
                context.getSource().sendFailure(Component.translatable("ava.chat.timer_empty"));
            return timers.size();
        }));
        return builder;
    }

    private static int sync()
    {
        DataToClientMessage.timer();
        return 1;
    }
}
