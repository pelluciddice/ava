package pellucid.ava.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.gun.gun_mastery.GunMastery;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.items.guns.AVAItemGun;

import static net.minecraft.commands.Commands.literal;

public class MasteryCommand
{
    public static ArgumentBuilder<CommandSourceStack, LiteralArgumentBuilder<CommandSourceStack>> register(CommandDispatcher<CommandSourceStack> dispatcher)
    {
        LiteralArgumentBuilder<CommandSourceStack> builder = literal("mastery").requires(player -> player.hasPermission(2));
        //todo
        GunMasteryManager.MEDIC.toString();
        for (GunMastery mastery : GunMastery.MASTERIES_LIST)
            for (int i = 0; i < 6; i ++)
            {
                int finalI = i;
                builder.then(literal("set")
                        .then(literal(mastery.getName())
                                .then(literal(String.valueOf(i))
                                        .executes((context) -> {
                                            Player player = context.getSource().getPlayerOrException();
                                            ItemStack stack = player.getMainHandItem();
                                            if (stack.getItem() instanceof AVAItemGun)
                                                GunMasteryManager.ofUnsafe(stack).setMastery(mastery, finalI);
                                            else
                                                throw new DynamicCommandExceptionType((arg) -> Component.translatable("ava:mastery.invalid")).create(null);
                                            return finalI;
                                        }))));
            }
        return builder;
    }
}
