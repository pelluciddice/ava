package pellucid.ava.client.inputs;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import pellucid.ava.competitive_mode.CompetitiveInventory;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAWeaponUtil;

import static pellucid.ava.config.AVAClientConfig.ENABLE_QUICK_SWAP_HOTKEY;
import static pellucid.ava.events.ClientModEventBus.QUICK_SWAP;

@EventBusSubscriber(Dist.CLIENT)
public class AVAQuickSwap
{
    private static int LAST_SELECTED = -1;
    private static int QUICK_SWAP_INDEX = -1;

    private static AVAWeaponUtil.WeaponCategory LAST_SELECTED_CATEGORY = AVAWeaponUtil.WeaponCategory.MAIN;
    private static AVAWeaponUtil.WeaponCategory QUICK_SWAP_CATEGORY = AVAWeaponUtil.WeaponCategory.MAIN;

    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        ClientLevel world = minecraft.level;
        if (world == null || player == null || !player.isAlive() || !AVAClientUtil.isFocused())
            return;
        ItemStack stack = player.getMainHandItem();
        if (ENABLE_QUICK_SWAP_HOTKEY.get() && QUICK_SWAP.justPressed())
        {
            if (AVAClientUtil.isCompetitiveModeEnabled())
                CompetitiveInventory.updateChoice(player.getInventory(), QUICK_SWAP_CATEGORY.getIndex());
            else if (QUICK_SWAP_INDEX != -1)
                player.getInventory().selected = QUICK_SWAP_INDEX;
        }

        if (AVAClientUtil.isCompetitiveModeEnabled())
        {
            AVAWeaponUtil.WeaponCategory cat = AVAWeaponUtil.WeaponCategory.fromItem(stack.getItem());
            if (cat != null)
            {
                if (cat != LAST_SELECTED_CATEGORY)
                {
                    QUICK_SWAP_CATEGORY = LAST_SELECTED_CATEGORY;
                    LAST_SELECTED_CATEGORY = cat;
                }
            }
        }
        else
        {
            int selected = player.getInventory().selected;
            if (selected != LAST_SELECTED)
            {
                QUICK_SWAP_INDEX = LAST_SELECTED;
                LAST_SELECTED = selected;
            }
        }
    }


}
