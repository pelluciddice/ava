package pellucid.ava.client.inputs;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.player.LocalPlayer;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import pellucid.ava.competitive_mode.CompetitiveModeClient;

import static pellucid.ava.config.AVAClientConfig.ENABLE_PRESET_HOTKEY;
import static pellucid.ava.events.ClientModEventBus.*;
import static pellucid.ava.util.AVAClientUtil.isFocused;
import static pellucid.ava.util.AVAClientUtil.onPresetUpdate;

@EventBusSubscriber(Dist.CLIENT)
public class AVACompetitiveInventory
{
    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        ClientLevel world = minecraft.level;
        if (player == null || !player.isAlive() || world == null || !isFocused())
            return;
        if (ENABLE_PRESET_HOTKEY.get())
        {
            boolean pressed = false;
            while (PRESET_F1.consumeClick())
            {
                CompetitiveModeClient.choosePreset(0);
                pressed = true;
            }
            while (PRESET_F2.consumeClick())
            {
                CompetitiveModeClient.choosePreset(1);
                pressed = true;
            }
            while (PRESET_F3.consumeClick())
            {
                CompetitiveModeClient.choosePreset(2);
                pressed = true;
            }
            if (pressed)
                onPresetUpdate();
        }
    }
}
