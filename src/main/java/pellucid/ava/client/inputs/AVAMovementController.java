package pellucid.ava.client.inputs;

import net.minecraft.client.Minecraft;
import net.minecraft.client.player.Input;
import net.minecraft.world.entity.player.Player;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.MovementInputUpdateEvent;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.player.status.C4StatusManager;
import pellucid.ava.util.AVAClientUtil;

import static java.lang.Math.abs;
import static java.lang.Math.max;

@EventBusSubscriber(Dist.CLIENT)
public class AVAMovementController
{
    public static float CURRENT_MOVEMENT = 0.0F;

    @SubscribeEvent
    public static void movementInput(MovementInputUpdateEvent event)
    {
        Input input = event.getInput();
        CURRENT_MOVEMENT = max(abs(input.forwardImpulse), abs(input.leftImpulse)) * (input.shiftKeyDown ? 0.5F : 1.0F);
        if (AVAServerConfig.AVA_RESTRICTED_MOVEMENT.get())
        {
            Minecraft minecraft = Minecraft.getInstance();
            Player player = minecraft.player;
            if (player != null && !player.isCreative() && !player.isSpectator())
            {
                if (input.jumping)
                    AVAClientUtil.stopSprinting(2);
                if (input.leftImpulse != 0.0D)
                {
                    player.setSprinting(false);
                    minecraft.options.keySprint.setDown(false);
                }
            }
        }
        if (C4StatusManager.INSTANCE.isActive(C4StatusManager.C4Status.SET))
        {
            input.forwardImpulse = 0.0F;
            input.leftImpulse = 0.0F;
            input.jumping = false;
        }
    }
}
