package pellucid.ava.client.inputs;

import com.mojang.blaze3d.platform.InputConstants;
import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.util.AVAClientUtil;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ForceKeyMapping extends KeyMapping
{
    public static final List<ForceKeyMapping> KEY_MAPPINGS = new ArrayList<>();
    private final int code;
    private final InputConstants.Type type;
    @Nullable
    private final Supplier<KeyMapping> possibleConflict;
    private boolean lastPressed;
    private boolean pressed;

    public ForceKeyMapping(String name, int code, String category, @Nullable Supplier<KeyMapping> possibleConflict)
    {
        this(name, InputConstants.Type.KEYSYM, code, category, possibleConflict);
    }

    public ForceKeyMapping(String name, InputConstants.Type type, int code, String category, @Nullable Supplier<KeyMapping> possibleConflict)
    {
        super(name, type, code, category);
        this.code = code;
        this.type = type;
        this.possibleConflict = possibleConflict;
        KEY_MAPPINGS.add(this);
    }

    public void tick()
    {
        lastPressed = pressed;
        if (type == InputConstants.Type.MOUSE)
            pressed = GLFW.glfwGetMouseButton(Minecraft.getInstance().getWindow().getWindow(), code) == GLFW.GLFW_PRESS;
        else if (type == InputConstants.Type.KEYSYM || type == InputConstants.Type.SCANCODE)
            pressed = GLFW.glfwGetKey(Minecraft.getInstance().getWindow().getWindow(), code) == GLFW.GLFW_PRESS;
        if (possibleConflict != null && possibleConflict.get() != null && possibleConflict.get().getKey().getValue() == code)
            AVAClientUtil.unpressKeybind(possibleConflict.get());
    }

    public boolean isPressed()
    {
        return pressed;
    }

    public boolean justPressed()
    {
        return !lastPressed && isPressed();
    }
}
