package pellucid.ava.client.inputs;

import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.util.AVAClientUtil;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ForceKeybind
{
    public static final List<ForceKeybind> KEY_BINDS = new ArrayList<>();
    private final int code;
    @Nullable
    private final Supplier<KeyMapping> possibleConflict;
    private final Supplier<Boolean> shouldCancelOriginal;
    private boolean lastPressed;
    private boolean pressed;

    public ForceKeybind(int code, @Nullable Supplier<KeyMapping> possibleConflict)
    {
        this(code, possibleConflict, null);
    }

    public ForceKeybind(int code, @Nullable Supplier<KeyMapping> possibleConflict, @Nullable Supplier<Boolean> shouldCancelOriginal)
    {
        this.code = code;
        this.possibleConflict = possibleConflict;
        this.shouldCancelOriginal = shouldCancelOriginal == null ? () -> true : shouldCancelOriginal;
        KEY_BINDS.add(this);
    }

    public void tick()
    {
        lastPressed = pressed;
        pressed = GLFW.glfwGetKey(Minecraft.getInstance().getWindow().getWindow(), code) == GLFW.GLFW_PRESS;
        if (possibleConflict != null && possibleConflict.get().getKey().getValue() == code && shouldCancelOriginal.get())
            AVAClientUtil.unpressKeybind(possibleConflict.get());
    }

    public boolean isPressed()
    {
        return pressed;
    }

    public boolean justPressed()
    {
        return !lastPressed && isPressed();
    }
}
