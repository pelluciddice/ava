package pellucid.ava.client.inputs;

import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.player.LocalPlayer;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import net.neoforged.neoforge.network.PacketDistributor;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.packets.RadioMessage;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAWeaponUtil;

import static pellucid.ava.config.AVAClientConfig.ENABLE_RADIO_HOTKEY;
import static pellucid.ava.events.ClientModEventBus.RADIO_1;
import static pellucid.ava.events.ClientModEventBus.RADIO_2;
import static pellucid.ava.util.AVAWeaponUtil.RadioCategory.*;

@EventBusSubscriber(Dist.CLIENT)
public class AVARadio
{
    private static AVAWeaponUtil.RadioCategory RADIO_CATEGORY = NONE;
    private static int RADIO_CD = 30;
    private static int RADIO_SELECT_CD = 0;

    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Post event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        ClientLevel world = minecraft.level;
        if (world == null || player == null || !player.isAlive() || !AVAClientUtil.isFocused())
            return;
        if (ENABLE_RADIO_HOTKEY.get())
        {
            if (RADIO_SELECT_CD <= 0)
            {
                AVAWeaponUtil.RadioCategory select = NONE;
                while (RADIO_1.consumeClick())
                    select = Z;
                while (RADIO_2.consumeClick())
                    select = X;
                if (select == RADIO_CATEGORY)
                    RADIO_CATEGORY = NONE;
                else if (select != NONE)
                    RADIO_CATEGORY = select;
                RADIO_SELECT_CD = 3;
            }
            else RADIO_SELECT_CD--;
        }
        else cancelRadio();
        int pressed = -1;
        for (int i = 0; i < 11; i++)
            if (GLFW.glfwGetKey(minecraft.getWindow().getWindow(), GLFW.GLFW_KEY_0 + i) == GLFW.GLFW_PRESS)
                pressed = i;
        if (pressed != -1 && RADIO_CATEGORY.exists())
        {
            if (pressed < 10)
                selectRadio(RADIO_CATEGORY, pressed);
            cancelRadio();
        }
        if (RADIO_CD > 0)
            RADIO_CD--;
    }

    public static void cancelRadio()
    {
        RADIO_CATEGORY = NONE;
    }

    public static void selectRadio(AVAWeaponUtil.RadioCategory category, int pressed)
    {
        if (RADIO_CD <= 0)
        {
            if (pressed != 0)
            {
                PacketDistributor.sendToServer(new RadioMessage(category, pressed));
                RADIO_CD = 30;
            }
            else cancelRadio();
        }
    }

    public static AVAWeaponUtil.RadioCategory getActiveRadio()
    {
        return RADIO_CATEGORY;
    }

    public static boolean isRadioActive()
    {
        return RADIO_CATEGORY.exists();
    }

    @SubscribeEvent
    public static void cancelHotbarSelection(ClientTickEvent.Pre event)
    {
        if (isRadioActive() || (AVAClientUtil.isCompetitiveModeEnabled() && Minecraft.getInstance().player != null && !Minecraft.getInstance().player.getAbilities().instabuild && !Minecraft.getInstance().player.isSpectator()))
        {
            for (KeyMapping keyBinding : Minecraft.getInstance().options.keyHotbarSlots)
            {
                int code = keyBinding.getKey().getValue();
                if (code >= GLFW.GLFW_KEY_0 && code <= GLFW.GLFW_KEY_9)
                    AVAClientUtil.unpressKeybind(keyBinding);
            }
        }
    }
}
