package pellucid.ava.client.components;

public interface ITickableButton
{
    void tick();
}
