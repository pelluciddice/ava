package pellucid.ava.client.components;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.narration.NarratedElementType;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class ItemWidgets
{
    public static <S extends Screen> DisplayItemWidget<S> toolTipHover(S screen, @Nullable ItemStack icon, Supplier<MutableComponent> tooltip, int x, int y)
    {
        return new DisplayItemWidget<>(screen, icon, x, y) {
            @Override
            protected MutableComponent createNarrationMessage()
            {
                return tooltip.get();
            }
        };
    }

    public static <S extends Screen> DisplayItemWidget<S> display(S screen, @Nullable ItemStack item, int x, int y)
    {
        return new DisplayItemWidget<>(screen, item, x, y);
    }

    public static <S extends Screen> ItemButton<S> select(S screen, @Nullable Item item, int x, int y, Consumer<ItemButton<S>> action)
    {
        return new ItemButton<>(screen, item, x, y, action);
    }

    public static <S extends Screen> ItemButton<S> selectDynamic(S screen, Supplier<Item> item, int x, int y, Consumer<ItemButton<S>> action)
    {
        return selectDynamicItemStack(screen, () -> new ItemStack(item.get()), x, y, action);
    }

    public static <S extends Screen> ItemButton<S> selectDynamicItemStack(S screen, Supplier<ItemStack> item, int x, int y, Consumer<ItemButton<S>> action)
    {
        return new ItemButton<>(screen, item.get().getItem(), x, y, action) {
            @Override
            public Item getItem()
            {
                return item.get().getItem();
            }

            @Override
            public ItemStack getRenderItemStack()
            {
                return item.get();
            }
        };
    }

    public static <S extends Screen> ItemButton<S> selectEmpty(S screen, MutableComponent tooltip, int x, int y, int width, int height, Consumer<ItemButton<S>> action)
    {
        return new ItemButton<>(screen, null, x, y, width, height, action) {
            @Override
            protected boolean notEmpty()
            {
                return true;
            }

            @Override
            protected MutableComponent createNarrationMessage()
            {
                return tooltip;
            }

//            @Override
//            public void renderToolTip(GuiGraphics stack, int p_renderToolTip_1_, int p_renderToolTip_2_)
//            {
//                screen.renderTooltip(stack, tooltip, p_renderToolTip_1_, p_renderToolTip_2_);
//            }
        };
    }

    public static <S extends Screen> ItemButton<S> selectEmptyEither(S screen, Function<ItemButton<S>, Boolean> condition, Pair<MutableComponent, MutableComponent> tooltip, int x, int y, int width, int height, Consumer<ItemButton<S>> action)
    {
        return new ItemButton<>(screen, null, x, y, width, height, action) {
            @Override
            protected boolean notEmpty()
            {
                return true;
            }

            @Override
            protected MutableComponent createNarrationMessage()
            {
                return Pair.or(tooltip, condition.apply(this));
            }

//            @Override
//            public void renderToolTip(GuiGraphics stack, int p_renderToolTip_1_, int p_renderToolTip_2_)
//            {
//                screen.renderTooltip(stack, Pair.or(tooltip, condition.apply(this)), p_renderToolTip_1_, p_renderToolTip_2_);
//            }
        };
    }

    public static class ItemButton<S extends Screen> extends AbstractButton
    {
        private boolean selected;
        private final S screen;
        private Item item;
        private final Consumer<ItemButton<S>> action;

        public ItemButton(S screen, @Nullable Item item, int x, int y, Consumer<ItemButton<S>> action)
        {
            this(screen, item, x, y, 18, 18, action);
        }

        public ItemButton(S screen, @Nullable Item item, int x, int y, int width, int height, Consumer<ItemButton<S>> action)
        {
            super(x, y, width, height, Component.empty());
            this.screen = screen;
            setItem(item == null ? Items.AIR : item);
            this.action = action;
        }

        public void setItem(Item item)
        {
            this.item = item;
            this.renderItemStack = Lazy.of(() -> new ItemStack(item));
            setTooltip(Tooltip.create(createNarrationMessage(), null));
            setMessage(createNarrationMessage());
        }

        public Item getItem()
        {
            return item;
        }

        @Override
        public void onPress()
        {
            if (notEmpty())
                action.accept(this);
        }

        private Lazy<ItemStack> renderItemStack = Lazy.of(() -> new ItemStack(item));

        public ItemStack getRenderItemStack()
        {
            return renderItemStack.get();
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            if (notEmpty())
            {
                if (this.isSelected())
                    stack.fill(getX() + 1, getY() + 1, getX() + width - 1, getY() + height - 1, AVAConstants.AVA_HUD_COLOUR_BLUE.getRGB());
                else if (isHoveredOrFocused())
                    stack.fill(getX() + 1, getY() + 1, getX() + width - 1, getY() + height - 1, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
                stack.renderFakeItem(getRenderItemStack(), getX() + 1, getY() + 1);
            }
        }

//        @Override
//        public void renderToolTip(GuiGraphics stack, int p_renderToolTip_1_, int p_renderToolTip_2_)
//        {
//            if (notEmpty())
//                screen.renderTooltip(stack, getRenderItemStack(), p_renderToolTip_1_, p_renderToolTip_2_);
//        }

        @Override
        protected MutableComponent createNarrationMessage()
        {
            return getRenderItemStack().getHoverName().copy();
        }

        public boolean isSelected()
        {
            return this.selected && notEmpty();
        }

        public void setSelected(boolean selectedIn)
        {
            this.selected = selectedIn;
        }

        protected boolean notEmpty()
        {
            return item != Items.AIR;
        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {
            p_169152_.add(NarratedElementType.USAGE, createNarrationMessage());
        }
    }

    public static class DisplayItemWidget<S extends Screen> extends AbstractWidget
    {
        private final S screen;
        private ItemStack item;

        public DisplayItemWidget(S screen, @Nullable ItemStack item, int x, int y)
        {
            super(x, y, 18, 18, Component.empty());
            this.screen = screen;
            setItem(item);
        }

        public void setItem(Item item)
        {
            setItem(new ItemStack(item));
        }

        public void setItem(ItemStack item)
        {
            this.item = item == null ? ItemStack.EMPTY : item;
            setTooltip(Tooltip.create(createNarrationMessage(), null));
            setMessage(createNarrationMessage());
        }

        public ItemStack getItem()
        {
            return item;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_renderWidget_1_, int p_renderWidget_2_, float p_renderWidget_3_)
        {
            if (!item.isEmpty())
            {
                stack.renderFakeItem(item, getX() + 1, getY() + 1);
                stack.renderItemDecorations(screen.getMinecraft().font, item, getX() + 1, getY() + 1);
            }
        }

        @Override
        protected MutableComponent createNarrationMessage()
        {
            return item.getHoverName().copy();
        }

//        @Override
//        public void renderToolTip(GuiGraphics stack, int p_renderToolTip_1_, int p_renderToolTip_2_)
//        {
//            if (!item.isEmpty())
//                screen.renderTooltip(stack, item, p_renderToolTip_1_, p_renderToolTip_2_);
//        }

        @Override
        public void updateWidgetNarration(NarrationElementOutput p_169152_)
        {
            p_169152_.add(NarratedElementType.USAGE, createNarrationMessage());
        }
    }
}
