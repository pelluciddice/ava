package pellucid.ava.client.components;

import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import org.apache.commons.lang3.tuple.Pair;

public class IntTextField<S extends Screen> extends EditBox
{
    protected final S screen;
    protected final Pair<Integer, Integer> limit;
    private final int defaultValue;

    public IntTextField(S screen, int x, int y, int width, int height, int min, int max, int defaultValue)
    {
        this(screen, x, y, width, height, min, max, defaultValue, Component.empty());
    }

    public IntTextField(S screen, int x, int y, int width, int height, int min, int max, int defaultValue, Component title)
    {
        super(screen.getMinecraft().font, x, y, width, height, null, title);
        this.screen = screen;
        this.defaultValue = defaultValue;
        this.limit = Pair.of(min, max);
        setValue(String.valueOf(defaultValue));
        setFilter(this::isValid);
    }

    @Override
    public void setValue(String text)
    {
        if (!text.isEmpty())
            super.setValue(text);
        else
            setValue(String.valueOf(getDefaultValue()));
    }

    public int getDefaultValue()
    {
        return defaultValue;
    }

    public int getInt()
    {
        try
        {
            return Integer.parseInt(getValue());
        }
        catch (Exception ignored)
        {
            setValue(String.valueOf(defaultValue));
            return defaultValue;
        }
    }

    protected boolean isValid(String text)
    {
        int value;
        try
        {
            value = Integer.parseInt(text);
            if (!(value >= limit.getLeft() && value <= limit.getRight()))
                return false;
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
}
