package pellucid.ava.client.animation;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.player.AbstractClientPlayer;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.ItemInHandRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.player.PlayerRenderer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.RenderHandEvent;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.player.status.C4StatusManager;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.player.status.MeleeStatusManager;
import pellucid.ava.player.status.ProjectileStatusManager;

import static net.minecraft.world.item.ItemDisplayContext.FIRST_PERSON_LEFT_HAND;
import static net.minecraft.world.item.ItemDisplayContext.FIRST_PERSON_RIGHT_HAND;
import static pellucid.ava.util.AVAClientUtil.revertBobbing;
import static pellucid.ava.util.AVACommonUtil.getGun;
import static pellucid.ava.util.AVACommonUtil.isAvailable;

@EventBusSubscriber(Dist.CLIENT)
public class AVAFPRenderer
{
    public static void renderItem(AbstractClientPlayer player, ItemInHandRenderer renderer, HumanoidArm hand, PoseStack stack, MultiBufferSource buffer, int light, ItemStack item)
    {
        if (!player.isScoping())
        {
            boolean main = hand == HumanoidArm.RIGHT;
            HumanoidArm humanoidarm = main ? player.getMainArm() : player.getMainArm().getOpposite();
            stack.pushPose();
            int i = humanoidarm == HumanoidArm.RIGHT ? 1 : -1;
            stack.translate((float) i * 0.56F, -0.5199999809265137D, -0.7200000286102295D);
            renderer.renderItem(player, item, humanoidarm == HumanoidArm.RIGHT ? FIRST_PERSON_RIGHT_HAND : FIRST_PERSON_LEFT_HAND, !main, stack, buffer, light);
            stack.popPose();
        }
    }

    @SubscribeEvent
    public static void onHandRender(RenderHandEvent event)
    {
        LocalPlayer player = Minecraft.getInstance().player;
        if (event.getHand() == InteractionHand.OFF_HAND)
            return;
        if (player == null || !player.isAlive())
            return;
        if (ItemAnimatingGUI.isAnimating())
            return;
        ItemStack itemStack = player.getMainHandItem();
        PlayerRenderer renderer = (PlayerRenderer) Minecraft.getInstance().getEntityRenderDispatcher().getRenderer(player);
        final float partialTicks = event.getPartialTick();
        PoseStack stack = event.getPoseStack();
        if (itemStack.getItem() instanceof AVAAnimatedItem<?>)
        {
            revertBobbing(stack, partialTicks, () ->
            {
                switch (itemStack.getItem())
                {
                    case AVAItemGun gun ->
                    {
                        renderer.getModel().rightArmPose = HumanoidModel.ArmPose.EMPTY;
                        GunStatusManager manager = GunStatusManager.INSTANCE;
                        AVAItemGun.IScopeType type = gun.getScopeType(itemStack, true);
                        if (((!PlayerAction.getCap(player).isADS() && !manager.isActive(GunStatusManager.GunStatus.ADS_IN)) || (!type.animated(itemStack) && type.getTexture() == null)))
                            AnimationFactory.renderHandsWithAnimations(player, event, AVAModelTypes.GUNS, gun, AnimationFactory::getGunAnimations, GunStatusManager.GunStatus.DRAW, GunStatusManager.GunStatus.RELOAD, GunStatusManager.GunStatus.RELOAD_PRE, GunStatusManager.GunStatus.RELOAD_POST, GunStatusManager.GunStatus.FIRE, GunStatusManager.GunStatus.INSTALL_SILENCER, GunStatusManager.GunStatus.UNINSTALL_SILENCER, GunStatusManager.GunStatus.FIRE);
                    }
                    case AVAMeleeItem knife ->
                            AnimationFactory.renderHandsWithAnimations(player, event, AVAModelTypes.MELEES, knife, AnimationFactory::getMeleeAnimations, MeleeStatusManager.MeleeStatus.DRAW, MeleeStatusManager.MeleeStatus.ATTACK_LIGHT, MeleeStatusManager.MeleeStatus.ATTACK_HEAVY);
                    case BinocularItem knife ->
                    {
                        BinocularStatusManager manager = BinocularStatusManager.INSTANCE;
                        if (((!PlayerAction.getCap(player).isADS() && !manager.isActive(BinocularStatusManager.BinocularStatus.ADS_IN))))
                            AnimationFactory.renderHandsWithAnimations(player, event, AVAModelTypes.BINOCULAR, knife, AnimationFactory::getBinocularAnimations, BinocularStatusManager.BinocularStatus.DRAW, BinocularStatusManager.BinocularStatus.ADS_IN);
                    }
                    case ThrowableItem projectile ->
                    {
                        ProjectileStatusManager manager = ProjectileStatusManager.INSTANCE;
                        if (!manager.cooking)
                            AnimationFactory.renderHandsWithAnimations(player, event, AVAModelTypes.PROJECTILE, projectile, AnimationFactory::getProjectileAnimations, ProjectileStatusManager.ProjectileStatus.DRAW, ProjectileStatusManager.ProjectileStatus.PIN_OFF, ProjectileStatusManager.ProjectileStatus.THROW, ProjectileStatusManager.ProjectileStatus.TOSS);
                    }
                    case C4Item c4 ->
                            AnimationFactory.renderHandsWithAnimations(player, event, AVAModelTypes.C4, c4, AnimationFactory::getC4Animations, C4StatusManager.C4Status.DRAW, C4StatusManager.C4Status.SET);
                    default ->
                    {
                    }
                }
            });
        }

        if ((isAvailable(player) && PlayerAction.getCap(player).isADS() && getGun(player).getScopeType(itemStack, true).getTexture() != null) || (itemStack.getItem() instanceof BinocularItem && PlayerAction.getCap(player).isADS()) || (itemStack.getItem() instanceof ThrowableItem && (ProjectileStatusManager.INSTANCE.cooking || ProjectileStatusManager.INSTANCE.isActive(ProjectileStatusManager.ProjectileStatus.THROW, ProjectileStatusManager.ProjectileStatus.TOSS))))
            event.setCanceled(true);
    }
}
