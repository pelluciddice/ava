package pellucid.ava.client.animation;

import net.minecraft.world.entity.player.Player;
import org.joml.Vector3f;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.util.Pair;

import java.util.function.Consumer;

public class AVARunTransistor
{
    public static final AVARunTransistor INSTANCE = new AVARunTransistor();
    public static final int RUN_TRANSITION_TIME = 6;

    private final Pair<Perspective, Pair<Perspective, Perspective>> lastRun = Pair.of(new Perspective(), Pair.of(new Perspective(), new Perspective()));
    private final Pair<Perspective, Pair<Perspective, Perspective>> lastIdle = Pair.of(new Perspective(), Pair.of(new Perspective(), new Perspective()));
    public int transitionTicks;
    private boolean wasSprinting = false;

    public Perspective getRunBob(Perspective runPos, Vector3f bobScale, boolean forceBob)
    {
        return transInternal(getLastIdle(), runPos, runPos, bobScale, this::setLastRun, forceBob);
    }

    public Pair<Perspective, Perspective> getRunBobHands(Pair<Perspective, Perspective> runPos, Vector3f bobScale, boolean forceBob)
    {
        return transInternal(getLastIdleHands(), runPos, runPos, bobScale, this::setLastRunHands, forceBob);
    }

    public Perspective getIdleBob(Perspective idlePos, Perspective addBobTo, Vector3f bobScale, boolean forceBob)
    {
        return transInternal(getLastRun(), idlePos, addBobTo, bobScale, this::setLastIdle, forceBob);
    }

    public Pair<Perspective, Perspective> getIdleBobHands(Pair<Perspective, Perspective> idlePos, Pair<Perspective, Perspective> addBobTo, Vector3f bobScale, boolean forceBob)
    {
        return transInternal(getLastRunHands(), idlePos, addBobTo, bobScale, this::setLastIdleHands, forceBob);
    }

    private Perspective transInternal(Perspective from, Perspective to, Perspective addBobTo, Vector3f bobScale, Consumer<Perspective> cacheSetter, boolean forceBob)
    {
        Perspective perspective = addBobTo.copy();
        perspective.translation.add(AVABobAnimator.INSTANCE.getBob().mul(bobScale));
        if (!forceBob)
        {
            if (transitionTicks >= 0 && transitionTicks < RUN_TRANSITION_TIME)
                perspective = partial(from, to);
            else if (transitionTicks == RUN_TRANSITION_TIME)
                perspective = to.copy();
        }
        cacheSetter.accept(perspective);
        return perspective;
    }

    private Pair<Perspective, Perspective> transInternal(Pair<Perspective, Perspective> from, Pair<Perspective, Perspective> to, Pair<Perspective, Perspective> addBobTo, Vector3f bobScale, Consumer<Pair<Perspective, Perspective>> cacheSetter, boolean forceBob)
    {
        Pair<Perspective, Perspective> perspective = Pair.of(addBobTo.getA().copy(), addBobTo.getB().copy());
        Vector3f bob = AVABobAnimator.INSTANCE.getBob().mul(bobScale);
        perspective.getA().translation.add(new Vector3f(bob.x, bob.z, bob.y));
        perspective.getB().translation.add(new Vector3f(bob.x, bob.z, bob.y));
        if (!forceBob)
        {
            if (transitionTicks >= 0 && transitionTicks < RUN_TRANSITION_TIME)
                perspective = Pair.of(partial(from.getA(), to.getA()), partial(from.getB(), to.getB()));
            else if (transitionTicks == RUN_TRANSITION_TIME)
                perspective = Pair.of(to.getA().copy(), to.getB().copy());
        }
        cacheSetter.accept(perspective);
        return perspective;
    }

    private Perspective partial(Perspective from, Perspective to)
    {
        return from.getPerspectivePartial(to, transitionTicks, RUN_TRANSITION_TIME, AVABakedItemModel.getPartialTicks());
    }

    public void tick(Player player)
    {
        if (transitionTicks >= 0)
            transitionTicks++;
        if (transitionTicks > RUN_TRANSITION_TIME)
        {
            transitionTicks = -1;
            AVABobAnimator.INSTANCE.ticks = 0;
        }
        boolean sprinting = player.isSprinting();
        if (sprinting != wasSprinting)
            transitionTicks = 0;
        wasSprinting = sprinting;
    }

    private Perspective getLastIdle()
    {
        return lastIdle.getA();
    }

    private Pair<Perspective, Perspective> getLastIdleHands()
    {
        return lastIdle.getB();
    }

    private Perspective getLastRun()
    {
        return lastRun.getA();
    }

    private Pair<Perspective, Perspective> getLastRunHands()
    {
        return lastRun.getB();
    }

    private void setLastIdle(Perspective lastIdle)
    {
        this.lastIdle.setA(lastIdle);
    }

    private void setLastIdleHands(Pair<Perspective, Perspective> lastIdleHands)
    {
        this.lastIdle.setB(lastIdleHands);
    }

    private void setLastRun(Perspective lastRun)
    {
        this.lastRun.setA(lastRun);
    }

    private void setLastRunHands(Pair<Perspective, Perspective> lastRunHands)
    {
        this.lastRun.setB(lastRunHands);
    }
}
