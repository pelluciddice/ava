package pellucid.ava.client.animation;

import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.world.entity.player.Player;
import org.joml.Vector3f;
import pellucid.ava.client.inputs.AVAMovementController;
import pellucid.ava.client.renderers.AVABakedModel;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import static java.lang.Math.sin;

public class AVABobAnimator
{
    public static final AVABobAnimator INSTANCE = new AVABobAnimator();

    private int totalTransitionTime = 0;
    public int transitionTime = -1;

    public BobType type = BobType.REST;
    public BobType nextType = null;
    public int ticks = 0;
    public Vector3f currentBob = new Vector3f(0, 0, 0);

    public boolean isSprinting = false;

    public void tick(Player player)
    {
        isSprinting = player.isSprinting();
        if (nextType != null)
        {
            if (transitionTime > 0)
            {
                ticks = 0;
                transitionTime--;
            }
            else
            {
                transitionTime = -1;
                ticks = 0;
                type = nextType;
                nextType = null;
            }
        }
        else
        {
            ticks++;
            if (!AVAWeaponUtil.isOnGroundNotStrict(player))
                setNextType(BobType.REST, 4);
            else if (AVAMovementController.CURRENT_MOVEMENT == 0)
                setNextType(BobType.REST, 4);
            else if (isSprinting)
                setNextType(BobType.RUN, -1);
            else if (player.isCrouching())
                setNextType(BobType.SNEAK, 4);
            else
                setNextType(BobType.WALK, 2);
        }
    }

    public Vector3f getBob()
    {
        LocalPlayer player = Minecraft.getInstance().player;
        float partialTicks = AVABakedModel.getPartialTicks();
        if (player != null)
        {
            if (transitionTime > 0)
                return AVACommonUtil.lerpVector3f(new Vector3f(currentBob).mul((float) transitionTime / totalTransitionTime), new Vector3f(currentBob).mul((float) (transitionTime - 1) / totalTransitionTime), partialTicks);
            else if (transitionTime == 0)
                return new Vector3f(0);
            else if (shouldBob())
                return bobInternalLerp(ticks, partialTicks);
        }
        return new Vector3f(0, 0, 0);
    }

    private boolean shouldBob()
    {
        return AVAClientConfig.FP_BOBBING.get();
    }

    private Vector3f bobInternalLerp(int time, float partialTicks)
    {
        currentBob = bobInternal(time + 1);
        return AVACommonUtil.lerpVector3f(bobInternal(time), currentBob, partialTicks);
    }

    private Vector3f bobInternal(int time)
    {
        float tsltX = (float) sin(time * 0.5F * type.speed) * 0.4F;
        float tsltY = (float) sin(time * 1.0F * type.speed) * 0.25F;
        return new Vector3f(tsltX, tsltY, 0.0F).mul(type.factor);
    }

    private void setNextType(BobType nextType, int transitionTime)
    {
        boolean hasTransition = transitionTime != 0;
        if (this.type == nextType)
        {
            this.nextType = null;
            this.transitionTime = -1;
        }
        else if (hasTransition)
        {
            if (this.nextType != nextType)
            {
                this.nextType = nextType;
                this.totalTransitionTime = transitionTime;
                this.transitionTime = this.totalTransitionTime;
            }
        }
        else
        {
            this.type = nextType;
            this.nextType = null;
            this.ticks = 0;
            this.transitionTime = -1;
        }
    }

    public enum BobType
    {
        SNEAK(0.3F, new Vector3f(0.15F, 0.9F, 1.0F)),
        REST(0.3F, new Vector3f(0.15F, 0.9F, 1.0F)),
        WALK(0.85F, new Vector3f(1.1F, 1.1F, 1.0F)),
        RUN(1.0F, new Vector3f(2.5F, 2.5F, 1.0F)),
        ;
        public final float speed;
        public final Vector3f factor;

        BobType(float speed, Vector3f factor)
        {
            this.speed = speed;
            this.factor = factor;
        }

        public boolean isRest()
        {
            return this == REST;
        }
    }
}
