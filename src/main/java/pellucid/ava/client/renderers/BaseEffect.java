package pellucid.ava.client.renderers;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.world.level.Level;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.INBTSerializable;

import javax.annotation.Nullable;
import java.awt.Color;
import java.util.List;
import java.util.Random;

public abstract class BaseEffect implements INBTSerializable<CompoundTag>
{
    protected static final Random RANDOM = new Random();
    private boolean blend;
    public int live = 300;
    public Color colour = null;
    protected boolean killed;

    public BaseEffect()
    {

    }

    public BaseEffect(boolean blend)
    {
        this.blend = blend;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.BOOLEAN.write(compound, "blend", blend);
        DataTypes.INT.write(compound, "live", live);
        if (colour != null)
            DataTypes.INT.write(compound, "colour", colour.getRGB());
        DataTypes.BOOLEAN.write(compound, "killed", killed);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        blend = DataTypes.BOOLEAN.read(nbt, "blend");
        live = DataTypes.INT.read(nbt, "live");
        if (nbt.contains("colour"))
            colour = new Color(DataTypes.INT.read(nbt, "colour"));
        killed = DataTypes.BOOLEAN.read(nbt, "killed");
    }

    public static <T extends BaseEffect> void tickList(List<T> list)
    {
        list.forEach(BaseEffect::tick);
        list.removeIf(BaseEffect::shouldBeDead);
    }

    public boolean doBlend()
    {
        return blend;
    }

    public void tick()
    {
        live--;
    }

    public void setKilled(boolean killed)
    {
        this.killed = killed;
    }

    public boolean shouldBeDead()
    {
        return live <= 0 || killed;
    }

    public float lerpLive(float partialTicks)
    {
        if (live > 50)
            return 1.0F;
        return Mth.lerp(partialTicks, live, live + 1) / 50.0F;
    }

    public BaseEffect setColour(@Nullable Color colour)
    {
        this.colour = colour;
        return this;
    }

    public Color getColour(Level world)
    {
        return colour;
    }

    @Override
    public String toString()
    {
        return "BaseEffect{" +
                "blend=" + blend +
                ", live=" + live +
                '}';
    }
}
