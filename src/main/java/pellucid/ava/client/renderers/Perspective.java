package pellucid.ava.client.renderers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.joml.Vector3f;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.IJsonSerializable;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;

public class Perspective implements IJsonSerializable<Perspective>
{
    public static final Perspective EMPTY = new Perspective(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F);
    public final Vector3f rotation;
    public final Vector3f translation;
    public final Vector3f scale;

    public static Perspective empty()
    {
        return new Perspective(0);
    }

    public Perspective()
    {
        this(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
    }

    public Perspective(List<Float> value)
    {
        this(value.get(0), value.get(1), value.get(2), value.get(3), value.get(4), value.get(5), value.get(6), value.get(7), value.get(8));
    }

    public Perspective(float value)
    {
        this(value, value, value, value, value, value, value, value, value);
    }

    public Perspective(float rotationX, float rotationY, float rotationZ, float translationX, float translationY, float translationZ, float scaleX, float scaleY, float scaleZ)
    {
        this(new Vector3f(rotationX, rotationY, rotationZ), new Vector3f(translationX, translationY, translationZ), new Vector3f(scaleX, scaleY, scaleZ));
    }

    public Perspective(float[] rotation, float[] translation, float[] scale)
    {
        this(new Vector3f(rotation), new Vector3f(translation), new Vector3f(scale));
    }

    public Perspective(Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        this.rotation = rotation;
        this.translation = translation;
        this.scale = scale;
    }

    public static Perspective rotation(float r)
    {
        return new Perspective().withRotation(r, r, r).withScale(1.0F, 1.0F, 1.0F);
    }

    public static Perspective rotation(float x, float y, float z)
    {
        return new Perspective().withRotation(x, y, z).withScale(1.0F, 1.0F, 1.0F);
    }

    public static Perspective translation(float t)
    {
        return new Perspective().withTranslation(t, t, t).withScale(1.0F, 1.0F, 1.0F);
    }

    public static Perspective translation(float x, float y, float z)
    {
        return new Perspective().withTranslation(x, y, z).withScale(1.0F, 1.0F, 1.0F);
    }

    public static Perspective scale(float s)
    {
        return new Perspective().withScale(s, s, s);
    }

    public static Perspective scale(float x, float y, float z)
    {
        return new Perspective().withScale(x, y, z);
    }

    public Perspective withRotation(float x, float y, float z)
    {
        rotation.set(x, y, z);
        return this;
    }

    public Perspective withTranslation(float x, float y, float z)
    {
        translation.set(x, y, z);
        return this;
    }

    public Perspective withScale(float x, float y, float z)
    {
        scale.set(x, y, z);
        return this;
    }

    public static Perspective copy(@Nullable Perspective perspective)
    {
        return perspective == null ? null : new Perspective(new Vector3f(perspective.rotation), new Vector3f(perspective.translation), new Vector3f(perspective.scale));
    }

    public float[] getRotation()
    {
        return new float[]{this.rotation.x(), this.rotation.y(), this.rotation.z()};
    }

    public float[] getTranslation()
    {
        return new float[]{this.translation.x(), this.translation.y(), this.translation.z()};
    }

    public float[] getScale()
    {
        return new float[]{this.scale.x(), this.scale.y(), this.scale.z()};
    }

    public Perspective add(Perspective perspective)
    {
        Perspective newPerspective = copy(this);
        newPerspective.rotation.add(perspective.rotation);
        newPerspective.translation.add(perspective.translation);
        newPerspective.scale.add(perspective.scale);
        return newPerspective;
    }

    public Perspective substract(Perspective perspective)
    {
        Perspective newPerspective = copy(this);
        newPerspective.rotation.sub(perspective.rotation);
        newPerspective.translation.sub(perspective.translation);
        newPerspective.scale.sub(perspective.scale);
        return newPerspective;
    }

    public Perspective mul(Perspective perspective)
    {
        Perspective newPerspective = copy(this);
        newPerspective.rotation.mul(perspective.rotation.x(), perspective.rotation.y(), perspective.rotation.z());
        newPerspective.translation.mul(perspective.translation.x(), perspective.translation.y(), perspective.translation.z());
        newPerspective.scale.mul(perspective.scale.x(), perspective.scale.y(), perspective.scale.z());
        return newPerspective;
    }

    public Perspective mul(float scale)
    {
        Perspective newPerspective = copy(this);
        newPerspective.rotation.mul(scale);
        newPerspective.translation.mul(scale);
        newPerspective.scale.mul(scale);
        return newPerspective;
    }

    public Perspective getPerspectivePartial(Perspective nextPerspective, float partialTicks)
    {
        if (this == EMPTY || equals(nextPerspective))
            return nextPerspective;
        return add(nextPerspective.substract(this).mul(partialTicks));
    }

    public Perspective getPerspectivePartial(Perspective end, int current, int total, float partialTicks)
    {
        Perspective currentP = getPerspectivePartial(end, current / (float) total);
        Perspective nextP = getPerspectivePartial(end, (current + 1) / (float) total);
        return currentP.getPerspectivePartial(nextP, partialTicks);
    }

    public boolean isEmpty()
    {
        return this.equals(EMPTY);
    }

    public void reset()
    {
        withRotation(0.0F, 0.0F, 0.0F).withTranslation(0.0F, 0.0F, 0.0F).withScale(1.0F, 1.0F, 1.0F);
    }

    public void round(int dp)
    {
        rotation.set(AVACommonUtil.round(rotation.x(), dp), AVACommonUtil.round(rotation.y(), dp), AVACommonUtil.round(rotation.z(), dp));
        translation.set(AVACommonUtil.round(translation.x(), dp), AVACommonUtil.round(translation.y(), dp), AVACommonUtil.round(translation.z(), dp));
        scale.set(AVACommonUtil.round(scale.x(), dp), AVACommonUtil.round(scale.y(), dp), AVACommonUtil.round(scale.z(), dp));
    }

    public Perspective copy()
    {
        return Perspective.copy(this);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Perspective that = (Perspective) o;
        return Objects.equals(rotation, that.rotation) && Objects.equals(translation, that.translation) && Objects.equals(scale, that.scale);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(rotation, translation, scale);
    }

    @Override
    public String toString()
    {
        return "Perspective{" +
                "rotation=" + rotation +
                ", translation=" + translation +
                ", scale=" + scale +
                '}';
    }

    public String toStaticFieldCode(String name)
    {
        return "private static final Perspective " + name + " = new Perspective(" + vecString(rotation) + ", " + vecString(translation) + ", " + vecString(scale) + ");";
    }

    public String toCode()
    {
        return "new Perspective(" + vecString(rotation) + ", " + vecString(translation) + ", " + vecString(scale) + ")";
    }

    private String vecString(Vector3f vec)
    {
        return AVACommonUtil.round(vec.x(), 3) + "F, " + AVACommonUtil.round(vec.y(), 3) + "F, " + AVACommonUtil.round(vec.z(), 3) + "F";
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        JsonArray array = new JsonArray();
        array.add(rotation.x);
        array.add(rotation.y);
        array.add(rotation.z);
        array.add(translation.x);
        array.add(translation.y);
        array.add(translation.z);
        array.add(scale.x);
        array.add(scale.y);
        array.add(scale.z);
        json.add("perspective", array);
    }

    @Override
    public void readFromJson(JsonElement json)
    {
        Perspective perspective = new Perspective(AVACommonUtil.collect(json.getAsJsonObject().get("perspective").getAsJsonArray().asList(), JsonElement::getAsFloat));
        rotation.set(perspective.rotation);
        translation.set(perspective.translation);
        scale.set(perspective.scale);
    }
}
