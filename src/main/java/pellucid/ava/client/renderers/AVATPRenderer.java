package pellucid.ava.client.renderers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.entity.player.Player;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.RenderLevelStageEvent;
import net.neoforged.neoforge.client.event.RenderLivingEvent;
import net.neoforged.neoforge.client.event.RenderNameTagEvent;
import net.neoforged.neoforge.common.util.TriState;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.objects.parachute.renderers.ParachuteRenderer;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.Optional;

import static pellucid.ava.util.AVAClientUtil.drawRect3D;
import static pellucid.ava.util.AVACommonUtil.getHeldStack;
import static pellucid.ava.util.AVAConstants.*;

@EventBusSubscriber(Dist.CLIENT)
public class AVATPRenderer
{
    @SubscribeEvent
    public static void renderEntityEvent(RenderLivingEvent.Pre<LivingEntity, HumanoidModel<LivingEntity>> event)
    {
        LivingEntity entity = event.getEntity();
        if (getHeldStack(entity).getItem() instanceof AVAItemGun)
        {
            if (!shouldRender(entity))
                return;
            if (!(entity instanceof Player) && !(entity instanceof SidedSmartAIEntity))
                return;
            HumanoidModel<LivingEntity> model = event.getRenderer().getModel();
            model.leftArmPose = HumanoidModel.ArmPose.EMPTY;
            model.rightArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
        }
    }

    public static Optional<LivingEntity> validAlly(Player player, Entity entity)
    {
        if (!(entity instanceof LeopardEntity))
        {
            if (entity instanceof Player || entity instanceof ArmorStand || entity instanceof SidedSmartAIEntity)
            {
                if (!AVAClientConfig.showAllyStatus())
                    return Optional.empty();
            }
            else if (!AVAClientConfig.showCreatureStatus())
                return Optional.empty();
            if (!(entity instanceof LivingEntity))
                return Optional.empty();
            if (!AVAWeaponUtil.isValidEntity(entity) || (!player.isSpectator() && !player.isCreative() && !AVAWeaponUtil.isSameSide(player, entity)))
                return Optional.empty();
        }
        return Optional.ofNullable(player.distanceTo(entity) > 100.0F ? null : (LivingEntity) entity);
    }

    @SubscribeEvent
    public static void onNameTagRender(RenderNameTagEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        if (player != null && player.isAlive() && AVAClientConfig.showAllyStatus())
            validAlly(player, event.getEntity()).ifPresent((entity) -> event.setCanRender(TriState.FALSE));
    }

    @SubscribeEvent
    public static void renderAllyStatus(RenderLevelStageEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        if (event.getStage() == RenderLevelStageEvent.Stage.AFTER_PARTICLES && player != null && player.isAlive())
        {
            float partialTicks = event.getPartialTick();
            for (Entity e : player.clientLevel.entitiesForRendering())
            {
                validAlly(player, e).ifPresent((entity) -> {
                    PoseStack stack = event.getPoseStack();
                    stack.pushPose();

                    AVAClientUtil.translateMatrixWithCamera(stack, AVAClientUtil.clientEntityPos(entity, partialTicks), 100.0F);
                    AVAClientUtil.rotateWithCamera(stack, false);
                    stack.translate(0.0F, entity.getBbHeight() + 0.65F, 0.0F);
                    stack.mulPose(Axis.YP.rotationDegrees(180.0F));

                    double distance = player.distanceTo(entity);
                    float size = (float) (distance / 200.0F);
                    stack.scale(size, size, size);
                    float width = 20;
                    float height = 1F;

                    RenderSystem.disableDepthTest();
                    RenderSystem.disableCull();
                    AVAClientUtil.drawTransparent(true);

                    drawRect3D(stack, -width, height, width, -height, AVA_HEALTH_BAR_COLOUR_DARK_GRAY.getRed(), AVA_HEALTH_BAR_COLOUR_DARK_GRAY.getGreen(), AVA_HEALTH_BAR_COLOUR_DARK_GRAY.getBlue(), 0.65F);
                    drawRect3D(stack, -width, height, entity.getHealth() / entity.getMaxHealth() * width * 2 - width, -height, AVA_HEALTH_BAR_COLOUR_GREEN.getRed(), AVA_HEALTH_BAR_COLOUR_GREEN.getGreen(), AVA_HEALTH_BAR_COLOUR_GREEN.getBlue(), 0.75F);

                    RenderSystem.disableDepthTest();
                    AVAClientUtil.drawTransparent(true);

                    RenderSystem.setShader(GameRenderer::getPositionTexShader);
                    Component name = entity.getDisplayName().copy().setStyle(Style.EMPTY);
                    Font font = minecraft.font;
                    float textWidth = (float) (-font.width(name) / 2);
                    stack.mulPose(Axis.XP.rotationDegrees(180.0F));
                    stack.translate(0.0F, -8.5F, 0.0F);
                    size = 0.85F;
                    stack.scale(size, size, size);


                    RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
                    MultiBufferSource.BufferSource impl = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
                    font.drawInBatch(name, textWidth, 0, AVACommonUtil.getColourForName(entity, player), true, stack.last().pose(), impl, Font.DisplayMode.NORMAL, 0, VANILLA_FULL_PACKED_LIGHT);
                    impl.endBatch();

                    AVAClientUtil.drawTransparent(false);
                    RenderSystem.enableCull();
                    RenderSystem.enableDepthTest();
                    stack.popPose();
                });
            }
        }
    }

    @SubscribeEvent
    public static void renderParachute(RenderLivingEvent.Post event)
    {
        LivingEntity target = event.getEntity();
        if (!(target instanceof Player) || !target.isAlive())
            return;
        if (PlayerAction.getCap((Player) target).getUsingParachute())
            ParachuteRenderer.renderParachute(event.getPoseStack(), event.getMultiBufferSource(), event.getPackedLight());
    }

    private static boolean shouldRender(LivingEntity entity)
    {
        return entity.isAlive() && !entity.isSwimming() && !entity.isSpectator() && !entity.isFallFlying();
    }

    // For hotswap
    public static void transformThirdPersonItemMatrix(PoseStack stack, int time, boolean sprinting, boolean actioning, boolean botIdling, boolean aiming)
    {
        if (AVAClientConfig.ENABLE_ALTERNATED_THIRD_PERSON_MODEL.get())
        {
            stack.translate(0.3F, -0.15F, -0.1F);

            stack.mulPose(Axis.ZP.rotationDegrees(-20.0F));
            stack.mulPose(Axis.YP.rotationDegrees(-30.0F));
            stack.mulPose(Axis.XP.rotationDegrees(-45.0F));
            if (aiming)
                stack.mulPose(Axis.XP.rotationDegrees(-10.0F));

            if (sprinting || actioning || botIdling)
            {
                stack.translate(0.0F, -0.05F, -0.05F);
                stack.mulPose(Axis.ZP.rotationDegrees(-90.0F));
                stack.mulPose(Axis.YP.rotationDegrees(180.0F));
                stack.mulPose(Axis.XP.rotationDegrees(135.0F));
            }
        }
        else
        {
            stack.translate(0.0F, -0.3F, 0.0F);
            stack.mulPose(Axis.XP.rotationDegrees(-75.0F));
        }
    }

    public static void transformThirdPersonHandModel(ModelPart l, ModelPart r, int time, boolean sprinting, boolean actioning, boolean botIdling, boolean aiming)
    {
        double t = Mth.lerp(Minecraft.getInstance().getPartialTick(), time - 1, time);


        if (aiming)
        {
            l.xRot -= 10.0F / 180.0F * Math.PI;
            r.xRot -= 10.0F / 180.0F * Math.PI;
        }

        l.y += -20.5F;
        l.z += -2.5F;
        l.xRot += 10.0F / 180.0F * Math.PI;
        l.yRot += 5.0F / 180.0F * Math.PI;

        r.y += -0.5F;
        r.z += 2.5F;
        r.xRot += 20.0F / 180.0F * Math.PI;
        r.yRot += -35.0F / 180.0F * Math.PI;

        if (sprinting || actioning)
        {
            l.xRot += (40.0F + Math.sin(t / 1.5F) * 6.0F) / 180.0F * Math.PI;
            r.xRot += (10.0F + Math.sin(t / 1.5F) * 6.0F) / 180.0F * Math.PI;
        }
        else if (botIdling)
        {
            l.xRot += (40.0F + Math.sin(t / 7.5F) * 3.0F) / 180.0F * Math.PI;
            r.xRot += (10.0F + Math.sin(t / 7.5F) * 3.0F) / 180.0F * Math.PI;
        }
    }
}
