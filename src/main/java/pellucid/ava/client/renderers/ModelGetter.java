package pellucid.ava.client.renderers;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;

public interface ModelGetter
{
    BakedModel getModel(BakedModel origin, ItemStack stack, ClientLevel world, LivingEntity entity);
}
