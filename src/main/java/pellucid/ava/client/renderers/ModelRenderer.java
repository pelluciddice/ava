package pellucid.ava.client.renderers;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ModelEvent;
import pellucid.ava.client.renderers.models.binocular.RegularBinoModel;
import pellucid.ava.client.renderers.models.c4.RegularC4Model;
import pellucid.ava.client.renderers.models.guns.RegularGunModel;
import pellucid.ava.client.renderers.models.melee.RegularMeleeModel;
import pellucid.ava.client.renderers.models.projectile.RegularProjectileModel;
import pellucid.ava.items.functionalities.ICustomModel;
import pellucid.ava.items.init.MeleeWeapons;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.init.Pistols;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.init.Rifles;
import pellucid.ava.items.init.Snipers;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.items.init.SubmachineGuns;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.HashMap;
import java.util.Map;

import static pellucid.ava.util.AVAClientUtil.*;

@EventBusSubscriber(bus = EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ModelRenderer
{
    @SubscribeEvent
    public static void onModelBake(ModelEvent.ModifyBakingResult event)
    {
        BuiltInRegistries.ITEM.stream().forEach((i) -> {
            if (i instanceof ICustomModel)
                replaceModel(event, i);
        });
        replaceModel(event, MiscItems.TEST_ITEM.get());
        replaceModel(event, MiscItems.TEST_ITEM_2.get());
    }

    public static final Map<Item, ModelResourceLocation> SIMPLE_MODELS = new HashMap<>();
    public static final Map<Item, ModelResourceLocation> TEXTURE_MODELS = new HashMap<>();

    @SubscribeEvent
    public static void onModelRegister(ModelEvent.RegisterAdditional event)
    {
        MiscItems.clientSetup();

        AVAWeaponUtil.getAllGuns().forEach((gun) -> gun.setCustomModel(RegularGunModel::new));

        MeleeWeapons.ITEM_MELEE_WEAPONS.forEach((knife) -> knife.setCustomModel(RegularMeleeModel::new));

        ((BinocularItem) SpecialWeapons.BINOCULAR.get()).setCustomModel(RegularBinoModel::new);
        ((C4Item) MiscItems.C4.get()).setCustomModel(RegularC4Model::new);

        Projectiles.ITEM_PROJECTILES.forEach((projectile) -> projectile.setCustomModel(RegularProjectileModel::new));

        BuiltInRegistries.ITEM.stream().forEach((item) -> {
            if (item instanceof ICustomModel)
                ((ICustomModel) item).getStaticModel(new ItemStack(item)).addSpecialModels(event);
        });

        for (Item item : Pistols.ITEM_PISTOLS)
            getAndAddSimpleModel(event, item);
        for (Item item : Rifles.ITEM_RIFLES)
            getAndAddSimpleModel(event, item);
        for (Item item : Snipers.ITEM_SNIPERS)
            getAndAddSimpleModel(event, item);
        for (Item item : SubmachineGuns.ITEM_SUBMACHINE_GUNS)
            getAndAddSimpleModel(event, item);
    }

    private static void getAndAddSimpleModel(ModelEvent.RegisterAdditional event, Item item)
    {
        putIfPresent(event, item, getSimpleModelFor(item), SIMPLE_MODELS);
        putIfPresent(event, item, getTextureModelFor(item), TEXTURE_MODELS);
    }

    private static void putIfPresent(ModelEvent.RegisterAdditional event, Item item, ModelResourceLocation rel, Map<Item, ModelResourceLocation> map)
    {
        if (rel != null)
        {
            event.register(rel);
            map.put(item, rel);
        }
    }
}
