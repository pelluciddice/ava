package pellucid.ava.client.renderers;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import pellucid.ava.AVA;

import java.awt.Color;

public class HitEffect extends EntityEffect
{
    public HitEffect(Entity entity)
    {
        super(entity);
        setColour(new Color(1.0F, 0.0F, RANDOM.nextFloat() / 4.0F + 0.2F, 1.0F));
    }

    @Override
    public float yOffset(Entity entity)
    {
        return 0.2F;
    }

    @Override
    public float width(float distance)
    {
        return distance / 20.0F;
    }

    @Override
    public float height(float distance)
    {
        return width(distance) / 6.0F;
    }

    private static final ResourceLocation HIT_MARK = new ResourceLocation(AVA.MODID + ":textures/environment/hit_mark.png");
    @Override
    public ResourceLocation getTexture()
    {
        return HIT_MARK;
    }
}
