package pellucid.ava.client.renderers;

import com.google.common.collect.ImmutableSet;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Axis;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.player.ItemTooltipEvent;
import org.joml.Matrix4f;
import pellucid.ava.AVA;
import pellucid.ava.blocks.IInteractable;
import pellucid.ava.blocks.rpg_box.RPGBoxBlock;
import pellucid.ava.blocks.rpg_box.RPGBoxTE;
import pellucid.ava.blocks.rpg_box.RPGBoxTER;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.inputs.AVARadio;
import pellucid.ava.client.overlay.HurtInfo;
import pellucid.ava.competitive_mode.CompetitiveModeClient;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.objects.item.GunItemEntity;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.events.ClientModEventBus;
import pellucid.ava.items.functionalities.ICustomTooltip;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.EitherPair;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import static pellucid.ava.util.AVAClientUtil.*;
import static pellucid.ava.util.AVACommonUtil.getHeldStack;
import static pellucid.ava.util.AVACommonUtil.isFullEquipped;
import static pellucid.ava.util.AVAConstants.SHIFT_MORE_INFO;

@EventBusSubscriber(Dist.CLIENT)
public class HUDIndicators
{
    @SubscribeEvent
    public static void modifyToolTips(ItemTooltipEvent event)
    {
        ItemStack stack = event.getItemStack();
        Item item = stack.getItem();
        boolean modify = false;
        List<Component> tips = event.getToolTip();
        if (item instanceof ICustomTooltip custom)
        {
            tips.clear();
            if (custom.addToolTips(event))
            {
                if (shiftDown())
                    custom.addAdditionalToolTips(event);
                else
                    tips.add(SHIFT_MORE_INFO);
            }
            modify = true;
        }
        if (modify && event.getFlags().isAdvanced())
        {
            tips.add((Component.literal(BuiltInRegistries.ITEM.getKey(item).toString())).withStyle(ChatFormatting.DARK_GRAY));
            int i = stack.getComponents().size();
            if (i > 0) {
                tips.add(Component.translatable("item.components", i).withStyle(ChatFormatting.DARK_GRAY));
            }
        }
    }

    private static final ImmutableSet<IIndicator> INDICATORS = ImmutableSet.of(
            new Hurt(),
            new Projectile(),
            new Preset(),
            new UAVReconnaissance(),
            new BioIndicator(),
            new Interaction(),
            new C4Timer(),
            new Radio(),
            new PickUp(),
            new ActionPing(),
            new NightVision(),
            new Binocular(),
            new BroadCastText()
    );

    public static void tick(Minecraft minecraft, Player player)
    {
        forEach((indicator) -> indicator.tick(minecraft, player));
    }

    public static void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, float partialTicks)
    {
        drawTransparent(true);
        forEach((indicator) -> {
            if (indicator.canRender(minecraft, player) && (!indicator.firstPersonOnly() || minecraft.options.getCameraType().isFirstPerson()) && (!indicator.requiredFullyEquipped() || isFullEquipped(player)) && (!indicator.requiredFocused() || AVAClientUtil.isFocused()))
                indicator.render(minecraft, player, stack, capability, screenWidth, screenHeight, partialTicks);
        });
        drawTransparent(false);
    }

    public static void forEach(Consumer<IIndicator> consumer)
    {
        ImmutableSet.copyOf(INDICATORS).forEach(consumer);
    }

    public interface IIndicator
    {
        default void tick(Minecraft minecraft, Player player)
        {

        }

        default boolean canRender(Minecraft minecraft, Player player)
        {
            return true;
        }

        default boolean firstPersonOnly()
        {
            return true;
        }

        default boolean requiredFullyEquipped()
        {
            return true;
        }

        default boolean requiredFocused()
        {
            return true;
        }

        void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks);
    }

    public static class Hurt implements IIndicator
    {
        private static final ResourceLocation HURT = new ResourceLocation(AVA.MODID + ":textures/overlay/hurt.png");
        private static final ResourceLocation HURT_INDICATOR = new ResourceLocation(AVA.MODID + ":textures/overlay/hurt_indicator.png");

        private static final int MAX_HURT_TIEMR = 16;
        private static int HURT_TIMER = 0;

        public static void onHurt()
        {
            HURT_TIMER = MAX_HURT_TIEMR;
        }

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (HURT_TIMER > 0)
                HURT_TIMER--;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            RenderSystem.setShaderTexture(0, HURT_INDICATOR);
            for (HurtInfo.Info info : new ArrayList<>(capability.getHurtInfo().getInfo()))
            {
                stack.pose().pushPose();
                RenderSystem.setShader(GameRenderer::getPositionTexShader);
                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, AnimationFactory.HURT_INFO_ALPHA.computeIfAbsent(info, (e) -> new AnimationFactory()).lerpPercentage(partialTicks, info.ticksLeft, 20.0D));

                rotateRelative(player, info.attackerPos, stack.pose(), screenWidth, screenHeight, 1, (rot) -> AnimationFactory.HURT_INFO_ROT.computeIfAbsent(info, (e) -> new AnimationFactory()).lerpRotF(partialTicks, rot));

                Matrix4f matrix = stack.pose().last().pose();
                Tesselator tessellator = Tesselator.getInstance();
                BufferBuilder bufferbuilder = tessellator.getBuilder();
                bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
                float w = screenWidth / 4.0F;
                bufferbuilder.vertex(matrix, w, screenHeight / 2.0F + w, -90.0F).uv(0.0F, 1.0F).endVertex();
                bufferbuilder.vertex(matrix, w * 3, screenHeight / 2.0F + w, -90.0F).uv(1.0F, 1.0F).endVertex();
                bufferbuilder.vertex(matrix, w * 3, screenHeight / 2.0F - w, -90.0F).uv(1.0F, 0.0F).endVertex();
                bufferbuilder.vertex(matrix, w, screenHeight / 2.0F - w, -90.0F).uv(0.0F, 0.0F).endVertex();
                tessellator.end();
                stack.pose().popPose();

                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            }
            if (HURT_TIMER > 0)
            {
                stack.pose().pushPose();
                RenderSystem.setShaderTexture(0, HURT);
                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, AnimationFactory.HURT_ALPHA.lerpPercentage(partialTicks, HURT_TIMER, MAX_HURT_TIEMR));
                AVAClientUtil.fillScreen((int) screenWidth, (int) screenHeight);
                stack.pose().popPose();
            }
        }

        @Override
        public boolean requiredFocused()
        {
            return false;
        }
    }

    public static class Preset implements IIndicator
    {
        public static final EitherPair<ResourceLocation> F1 = Pair.eitherPair(new ResourceLocation("ava:textures/overlay/f1.png"), new ResourceLocation("ava:textures/overlay/f1_selected.png"), () -> CompetitiveModeClient.getCurrentPresetChoice() != 0);
        public static final EitherPair<ResourceLocation> F2 = Pair.eitherPair(new ResourceLocation("ava:textures/overlay/f2.png"), new ResourceLocation("ava:textures/overlay/f2_selected.png"), () -> CompetitiveModeClient.getCurrentPresetChoice() != 1);
        public static final EitherPair<ResourceLocation> F3 = Pair.eitherPair(new ResourceLocation("ava:textures/overlay/f3.png"), new ResourceLocation("ava:textures/overlay/f3_selected.png"), () -> CompetitiveModeClient.getCurrentPresetChoice() != 2);
        public static int PRESET_SET_DISPLAY = 0;

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (PRESET_SET_DISPLAY > 0)
                PRESET_SET_DISPLAY--;
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return PRESET_SET_DISPLAY > 0;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            stack.pose().pushPose();
            stack.pose().translate(screenWidth / 2.0F, screenHeight, 0.0F);

            int blitOffset = 0;
            AVAClientUtil.drawTransparent(true);
            // Icons Backgrounds
            float w = 50;
            float y = -100;
            float h = 20.0F;
            AVAClientUtil.fill(stack.pose(), -w / 2.0F, y - 0.5F, w, 1, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
            AVAClientUtil.fill(stack.pose(), -w / 2.0F, y + h - 0.5F, w, 1, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
            AVAClientUtil.fill(stack.pose(), -w / 2.0F, y, w, h, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB());
            float w2 = 20;
            float offset = w / 2.0F;
            AVAClientUtil.fillGradient(stack.pose(), -offset - w2 / 2.0F, y - 0.5F, -offset, y + 0.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.EAST);
            AVAClientUtil.fillGradient(stack.pose(), offset, y - 0.5F, offset + w2 / 2.0F, y + 0.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.WEST);
            AVAClientUtil.fillGradient(stack.pose(), -offset - w2 / 2.0F, y + h - 0.5F, -offset, y + h + 0.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.EAST);
            AVAClientUtil.fillGradient(stack.pose(), offset, y + h - 0.5F, offset + w2 / 2.0F, y + h + 0.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.WEST);
            AVAClientUtil.fillGradient(stack.pose(), -offset - w2, y, -offset, y + h, blitOffset, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB(), Direction.EAST);
            AVAClientUtil.fillGradient(stack.pose(), offset, y, offset + w2, y + h, blitOffset, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB(), Direction.WEST);

            // Icons Images
            float y2 = y + 2.5F;
            float offset2 = 15;
            float size = 15;
            AVAClientUtil.drawTransparent(true);
            int p = CompetitiveModeClient.getCurrentPresetChoice();
            AVAClientUtil.blit(stack.pose(), F1.get(), -offset2 - size, y2, -offset2, y2 + size);
            AVAClientUtil.blit(stack.pose(), F2.get(), -size / 2.0F, y2, size / 2.0F, y2 + size);
            AVAClientUtil.blit(stack.pose(), F3.get(), offset2, y2, offset2 + size, y2 + size);

            // Weapon Backgrounds
            float y3 = y2 + 22;
            float w3 = 50;
            float h3 = 46.0F;
            AVAClientUtil.fill(stack.pose(), -w3 / 2.0F, y3 + 2.5F, w3, 1, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
            AVAClientUtil.fill(stack.pose(), -w3 / 2.0F, y3 + h3 - 3.5F, w3, 1, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
            AVAClientUtil.fill(stack.pose(), -w3 / 2.0F, y3, w3, h3, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB());

            float w4 = 50;
            float offset3 = w3 / 2.0F;
            AVAClientUtil.fillGradient(stack.pose(), -offset3 - w4 / 2.0F, y3 + 2.5F, -offset3, y3 + 3.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.EAST);
            AVAClientUtil.fillGradient(stack.pose(), offset3, y3 + 2.5F, offset3 + w4 / 2.0F, y3 + 3.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.WEST);
            AVAClientUtil.fillGradient(stack.pose(), -offset3 - w4 / 2.0F, y3 + h3 - 3.5F, -offset3, y3 + h3 - 2.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.EAST);
            AVAClientUtil.fillGradient(stack.pose(), offset3, y3 + h3 - 3.5F, offset3 + w4 / 2.0F, y3 + h3 - 2.5F, blitOffset, AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), Direction.WEST);
            AVAClientUtil.fillGradient(stack.pose(), -offset3 - w4, y3, -offset3, y3 + h3, blitOffset, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB(), Direction.EAST);
            AVAClientUtil.fillGradient(stack.pose(), offset3, y3, offset3 + w4, y3 + h3, blitOffset, AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT.getRGB(), AVAConstants.AVA_COMPETITIVE_UI_GREEN_BG.getRGB(), Direction.WEST);


            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            AVAClientUtil.drawTransparent(false);

            ItemStack mainWeapon = new ItemStack(CompetitiveModeClient.getIconicWeapon());
            stack.drawCenteredString(minecraft.font, mainWeapon.getHoverName(), 0, -42, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());

            stack.pose().popPose();
            final float scale = 3.75F;
            AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
                stack2.translate(screenWidth / 2.0F - 8 * scale, screenHeight - 23 * scale, 0.0F);
                stack2.scale(scale, scale, scale);
            }, (stack2) -> {
                AVAClientUtil.forceEnable3DModel(() -> stack.renderFakeItem(mainWeapon, 0, 0));
            });
        }

        @Override
        public boolean requiredFullyEquipped()
        {
            return false;
        }
    }

    static class Projectile implements IIndicator
    {
        private static final ResourceLocation PROJECTILE_INDICATOR = new ResourceLocation(AVA.MODID + ":textures/overlay/projectile_indicator.png");
        private static HandGrenadeEntity PROJECTILE_ENTITY = null;

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (AVAClientConfig.ENABLE_PROJECTILE_INDICATOR.get() && isFullEquipped(player))
            {
                AABB projectileDetection = player.getBoundingBox().inflate(20);
                if (PROJECTILE_ENTITY != null && (!isProjectileInRange(player, PROJECTILE_ENTITY) || !PROJECTILE_ENTITY.isAlive()))
                    PROJECTILE_ENTITY = null;
                if (PROJECTILE_ENTITY == null)
                {
                    List<HandGrenadeEntity> list = player.level().getEntitiesOfClass(HandGrenadeEntity.class, projectileDetection);
                    list.removeIf((entity) -> !isProjectileInRange(player, entity) || !entity.isAwarable());
                    PROJECTILE_ENTITY = list.stream().findFirst().orElse(null);
                }
            }
        }

        private static boolean isProjectileInRange(Player player, HandGrenadeEntity entity)
        {
            return entity.getBoundingBox().inflate(AVAWeaponUtil.getAffectingRadiusForEntity(entity)).intersects(player.getBoundingBox());
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return AVAClientConfig.ENABLE_PROJECTILE_INDICATOR.get() && PROJECTILE_ENTITY != null;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            int x = (int) (screenWidth / 2.0F);
            int y = (int) (screenHeight / 2.0F);
            RenderSystem.setShaderTexture(0, PROJECTILE_INDICATOR);
            blitCentreRelative(stack.pose(), (matrix) -> {
                stack.pose().translate(0.0F, 50, 0.0F);
                rotateRelative(player, PROJECTILE_ENTITY.position(), stack.pose(), screenWidth, screenHeight, 0.125F, (rot) -> AnimationFactory.PROJECTILE_INDICATOR_ROT.computeIfAbsent(PROJECTILE_ENTITY, (e) -> new AnimationFactory()).lerpRotF(partialTicks, rot));
            }, screenWidth, screenHeight);

            float scale = 2F;

            AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
                stack2.translate(x, y, 0.0F);
                stack2.scale(scale, scale, 1.0F);
                stack2.translate(-x, -y, 0.0F);
            }, (stack2) -> {
                stack.renderFakeItem(new ItemStack(PROJECTILE_ENTITY.getIcon()), x - 8, y + 17);
            });

            stack.pose().pushPose();
            float scale2 = 0.875F;
            stack.pose().scale(scale2, scale2, scale2);
            stack.drawCenteredString(minecraft.font, AVACommonUtil.roundEntityDistance(player, PROJECTILE_ENTITY) + "m", (int) (x / scale2), (int) (y / scale2) + 63, AVAConstants.AVA_HUD_TEXT_ORANGE);
            stack.pose().popPose();
        }
    }

    static class UAVReconnaissance implements IIndicator
    {
        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return isCaughtByUAV(player);
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            stack.drawCenteredString(minecraft.font, "UAV Reconnaissance: Position revealed!", (int) (screenWidth / 2.0F), (int) (screenHeight / 6.0F * 5.0F), AVAConstants.AVA_HUD_TEXT_ORANGE);
        }
    }

    static class BioIndicator implements IIndicator
    {
        private static final ResourceLocation BIO_INDICATOR = new ResourceLocation(AVA.MODID + ":textures/overlay/bio_indicator.png");
        private static final ResourceLocation BIO_INDICATOR_LIT = new ResourceLocation(AVA.MODID + ":textures/overlay/bio_indicator_lit.png");
        private static float DISTANCE = -1;
        private static int FLASH_INTERVAL = 0;

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (AVAClientConfig.ENABLE_BIO_INDICATOR.get() && isFullEquipped(player))
            {
                AABB projectileDetection = player.getBoundingBox().inflate(20);
                List<Zombie> list = player.level().getEntitiesOfClass(Zombie.class, projectileDetection);
                list.removeIf((entity) -> entity.position().distanceTo(player.position()) > 20.0F);
                list.sort(Comparator.comparingDouble((entity) -> entity.position().distanceTo(player.position())));
                Entity entity = list.stream().findFirst().orElse(null);
                if (entity != null)
                    DISTANCE = (float) entity.position().distanceTo(player.position());
                else
                {
                    DISTANCE = -1;
                    FLASH_INTERVAL = -1;
                }
                if (FLASH_INTERVAL > 0 && DISTANCE < 10)
                    FLASH_INTERVAL--;
                else if (DISTANCE > 0)
                    FLASH_INTERVAL = Math.round(DISTANCE);
                if (FLASH_INTERVAL < 1 && DISTANCE > 0 && DISTANCE <= 10 && minecraft.isWindowActive() && minecraft.screen == null)
                    player.playNotifySound(AVASounds.BIO_INDICATOR_BEEP.get(), SoundSource.PLAYERS, 0.8F, 1.0F + (10 - DISTANCE) / 20.0F);
            }
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return AVAClientConfig.ENABLE_BIO_INDICATOR.get() && DISTANCE > 0;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            int x = (int) (screenWidth / 2.0F);
            int y = (int) (screenHeight / 2.0F);

            RenderSystem.setShaderTexture(0, FLASH_INTERVAL == 0 ? BIO_INDICATOR_LIT : BIO_INDICATOR);
            drawTransparent(true);
            blitCentreRelative(stack.pose(), (matrix)-> {
                stack.pose().translate(0.0F, 75, 0.0F);
                scaleMatrix(stack.pose(), 0.125F, screenWidth, screenHeight);
            }, screenWidth, screenHeight);
            stack.pose().pushPose();
            float scale = 0.875F;
            stack.pose().scale(scale, scale, scale);
            stack.drawCenteredString(minecraft.font, AVACommonUtil.round(DISTANCE, 2) + "m", (int) (x / scale), (int) (y / scale) + 96, AVAConstants.AVA_HUD_TEXT_ORANGE);
            stack.pose().popPose();
        }
    }

    public static class Interaction implements IIndicator
    {
        private static final ResourceLocation INTERACTION_INDICATOR = new ResourceLocation(AVA.MODID + ":textures/overlay/interaction_indicator.png");
        private static final ResourceLocation INTERACTION_INDICATOR_PROGRESS = new ResourceLocation(AVA.MODID + ":textures/overlay/interaction_indicator_progress.png");
        private static int PRESSED = 0;
        private static IInteractable<?> TARGET = null;
        private static final BiFunction<Player, BlockHitResult, Block> BLOCK_INTERACTOR = (player, blockResult) -> player.level().getBlockState(blockResult.getBlockPos()).getBlock();
        private static final BiFunction<Player, HitResult, Entity> ENTITY_INTERACTOR = (player, entityResult) -> ((EntityHitResult) entityResult).getEntity();
        protected static final Map<IInteractable<?>, Integer> INTERACTABLES_CD = new HashMap<>();

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            AVACommonUtil.decreaseAndRemove(INTERACTABLES_CD);
            HitResult result = minecraft.hitResult;
            if (result != null)
            {
                if (AVAClientUtil.isCompetitiveModeEnabled() && !player.isCreative() && CompetitiveModeClient.INTERACT.justPressed())
                {
                    if (result.getType() == HitResult.Type.BLOCK && minecraft.gameMode != null)
                    {
                        minecraft.gameMode.useItemOn((LocalPlayer) player, InteractionHand.MAIN_HAND, (BlockHitResult) result);
                        return;
                    }
                }
                if (AVAClientUtil.isCompetitiveModeEnabled() && !player.isCreative() ? CompetitiveModeClient.INTERACT.isPressed() : rightMouseDown())
                {
                    if (result.getType() == HitResult.Type.BLOCK && interact(player, (BlockHitResult) result, (blockResult) -> BLOCK_INTERACTOR.apply(player, blockResult), (interactable, blockResult) -> interactable.interact(player.level(), blockResult, blockResult.getBlockPos(), blockResult.getLocation(), player), PlayerActionMessage::interactionCompletionBlock)) return;
                    else if (result.getType() == HitResult.Type.ENTITY && interact(player, (EntityHitResult) result, (entityResult) -> ENTITY_INTERACTOR.apply(player, entityResult), (interactable, blockResult) -> interactable.interact(player.level(), blockResult, null, blockResult.getLocation().subtract(blockResult.getEntity().getX(), blockResult.getEntity().getY(), blockResult.getEntity().getZ()), player), PlayerActionMessage::interactionCompletionEntity)) return;
                }
            }
            TARGET = null;
            PRESSED = 0;
        }

        private static <R extends HitResult, T> boolean interact(Player player, R result, Function<R, T> objectGetter, BiConsumer<IInteractable<R>, R> interacter, Runnable messageSender)
        {
            T target = objectGetter.apply(result);
            if (target instanceof IInteractable)
            {
                IInteractable<R> interactable = (IInteractable<R>) target;
                if (!INTERACTABLES_CD.containsKey(interactable) && IInteractable.canInteract(result, player, interactable, BlockPos.containing(result.getLocation()), result.getLocation(), AVAWeaponUtil.getEyePositionFor(player)))
                {
                    int duration = interactable.getDuration();
                    if (PRESSED < duration)
                    {
                        TARGET = interactable;
                        TARGET.whileInteract(player.level(), player);
                        if (TARGET.sharedProgress())
                            PRESSED = TARGET.getSharedProgress();
                        else
                            PRESSED++;
                    }
                    else
                    {
                        PRESSED = 0;
                        interacter.accept(interactable, result);
                        messageSender.run();
                        INTERACTABLES_CD.put(interactable, interactable.cd());
                    }
                    return true;
                }
            }
            return false;
        }

        public static boolean interacting()
        {
            return PRESSED > 0 && TARGET != null;
        }

        private static final int TEXTURE_DIMENSION = 134;

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            if (interacting())
            {
                int y = (int) (screenHeight / 2.0F - TEXTURE_DIMENSION / 2);
                stack.pose().pushPose();
                stack.pose().translate(0.0F, 0.0F, -100.0F);
                blit(minecraft, INTERACTION_INDICATOR, stack, y, 0, TEXTURE_DIMENSION, screenWidth, screenHeight);
                int progress = Math.round(AnimationFactory.INTERACT.lerpF(partialTicks, PRESSED) / (float) TARGET.getDuration() * (float) TEXTURE_DIMENSION);
                int gap = TEXTURE_DIMENSION - progress;
                blit(minecraft, INTERACTION_INDICATOR_PROGRESS, stack, y + gap, gap, progress, screenWidth, screenHeight);
                stack.pose().popPose();
            }

            boolean shouldDrawWrench = false;
            Function<String, MutableComponent> tip = null;
            HitResult result = minecraft.hitResult;
            if (result != null)
            {
                if (result.getType() == HitResult.Type.BLOCK)
                {
                    Block block = BLOCK_INTERACTOR.apply(player, (BlockHitResult) result);
                    if (block instanceof IInteractable interactable && IInteractable.canInteract(result, player, interactable, ((BlockHitResult) result).getBlockPos(), result.getLocation(), AVAWeaponUtil.getEyePositionFor(player)))
                    {
                        shouldDrawWrench = interactable.shouldDrawTipWrench();
                        if (interactable.getInteractableTip() != null)
                            tip = (k) -> Component.translatable(interactable.getInteractableTip(), k);
                    }
                }
                else if (result.getType() == HitResult.Type.ENTITY)
                {
                    Entity entity = ENTITY_INTERACTOR.apply(player, result);
                    if (entity instanceof IInteractable interactable && IInteractable.canInteract(result, player, interactable, BlockPos.containing(result.getLocation()), result.getLocation(), AVAWeaponUtil.getEyePositionFor(player)))
                    {
                        shouldDrawWrench = interactable.shouldDrawTipWrench();
                        if (interactable.getInteractableTip() != null)
                            tip = (k) -> Component.translatable(interactable.getInteractableTip(), k);
                    }
                }
            }
            if (shouldDrawWrench)
            {
                stack.pose().pushPose();
                AVAClientUtil.scaleMatrix(stack.pose(), 0.15F, screenWidth, screenHeight);
                stack.blit(INTERACTION_INDICATOR_PROGRESS, (int) (screenWidth / 2.0F - 550), (int) (screenHeight / 2.0F + 150), 0, 0, 0, TEXTURE_DIMENSION, TEXTURE_DIMENSION, TEXTURE_DIMENSION, TEXTURE_DIMENSION);
                stack.pose().popPose();
            }

            if (tip != null)
            {
                String key = AVAClientUtil.isCompetitiveModeEnabled() ? "E" : "Right Mouse";
                stack.pose().pushPose();
                AVAClientUtil.scaleMatrix(stack.pose(), 0.8F, screenWidth, screenHeight);
                stack.drawCenteredString(minecraft.font, tip.apply(key), (int) (screenWidth / 2.0F), (int) (screenHeight / 2.0F + 35), AVAConstants.AVA_HUD_TEXT_WHITE);
                stack.pose().popPose();
            }
        }

        private static void blit(Minecraft minecraft, ResourceLocation texture, GuiGraphics stack, int y, int textureOffsetY, int textureSizeY, float screenWidth, float screenHeight)
        {
            int x = Math.round(screenWidth / 2.0F - TEXTURE_DIMENSION / 2.0F);
            blitCentreRelative(stack, texture, (stack2) -> scaleMatrix(stack2, 0.325F, screenWidth, screenHeight), TEXTURE_DIMENSION, x, y, TEXTURE_DIMENSION, textureSizeY, 0, textureOffsetY, TEXTURE_DIMENSION, textureSizeY, screenWidth, screenHeight);
        }

        @Override
        public boolean requiredFullyEquipped()
        {
            return false;
        }
    }

    @EventBusSubscriber(Dist.CLIENT)
    public static class C4Timer implements IIndicator
    {
        private static ItemStack C4_STACK = null;
        public static C4Entity C4_ENTITY = null;

        @SubscribeEvent
        public static void onEntityJoinWorld(EntityJoinLevelEvent event)
        {
            if (C4_STACK == null)
                C4_STACK = new ItemStack(MiscItems.C4.get());
            if (AVAServerConfig.isCompetitiveModeActivated() && event.getEntity() instanceof C4Entity c4 && C4_ENTITY == null)
                C4_ENTITY = c4;
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            boolean canRender = AVAClientUtil.isCompetitiveModeEnabled() && C4_ENTITY != null && C4_ENTITY.isAlive() && !C4_ENTITY.defused();
            if (!canRender)
                C4_ENTITY = null;
            return canRender;
        }

        @Override
        public boolean firstPersonOnly()
        {
            return false;
        }

        @Override
        public boolean requiredFullyEquipped()
        {
            return false;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            stack.pose().pushPose();
            float scale = 2.0F;

            AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
                stack2.translate((int) (screenWidth / 2.0F - 8 * scale), 2, 0.0F);
                stack2.scale(scale, scale, scale);
            }, (stack2) -> {
                stack.renderFakeItem(C4_STACK, 0, 0);
            });
            stack.pose().popPose();

            if (C4_ENTITY.getTimeLeft() >= 0.0F)
            {
                AVAClientUtil.scaleText(stack.pose(), (int) (screenWidth / 2), 24, 1.5F, 1.5F, () -> {
                    stack.drawCenteredString(minecraft.font, String.valueOf(C4Timer.C4_ENTITY.getDisplayTime()), 0, 0, C4_ENTITY.flash() ? AVAConstants.AVA_HUD_TEXT_RED : AVAConstants.AVA_HUD_TEXT_WHITE);
                });
            }
        }

        @Override
        public boolean requiredFocused()
        {
            return false;
        }
    }

    public static class Radio implements IIndicator
    {
        @Override
        public void tick(Minecraft minecraft, Player player)
        {
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return AVARadio.isRadioActive();
        }

        @Override
        public boolean firstPersonOnly()
        {
            return false;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            AVAWeaponUtil.RadioCategory radio = AVARadio.getActiveRadio();
            float y = screenHeight / 2.0F;
            for (int i = 0; i < radio.getMessages().size();)
            {
                AVAWeaponUtil.RadioMessage message = radio.getMessages().get(i);
                int pref = ++i;
                if (pref >= 10)
                    pref = 0;
                stack.drawString(minecraft.font, pref + ". " + message.getDisplayName().getString(), 10, y += 9, AVAConstants.AVA_HUD_TEXT_ORANGE, true);
            }
        }
    }

    public static class PickUp implements IIndicator
    {
        private static GunItemEntity ENTITY = null;
        private static boolean WEAPON_BOX = false;
        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (AVAClientUtil.isCompetitiveModeEnabled() && AVAWeaponUtil.isPrimaryWeapon(player.getMainHandItem().getItem()) && !player.isSpectator())
            {
                if ((ENTITY != null || WEAPON_BOX) && CompetitiveModeClient.PICK_UP.justPressed())
                    PlayerActionMessage.pickupWeapon();
                if (ENTITY == null)
                    ENTITY = player.level().getEntitiesOfClass(GunItemEntity.class, player.getBoundingBox().inflate(0.15F)).stream().findFirst().orElse(null);
                else if (!ENTITY.getBoundingBox().intersects(player.getBoundingBox().inflate(0.15F)) || !ENTITY.isAlive())
                    ENTITY = null;

                if (!WEAPON_BOX)
                {
                    if (minecraft.hitResult instanceof BlockHitResult result && result.getLocation().distanceTo(player.getEyePosition()) <= 1.0F)
                    {
                        BlockPos pos = result.getBlockPos();
                        BlockState state = player.level().getBlockState(pos);
                        if (state.getBlock() instanceof RPGBoxBlock)
                        {
                            if (state.getValue(RPGBoxBlock.HALF) == DoubleBlockHalf.UPPER)
                                pos = pos.below();
                            if (player.level().getBlockEntity(pos) instanceof RPGBoxTE box && box.hasWeapon && box.isOpened())
                                WEAPON_BOX = true;
                        }
                    }
                }
                else
                {
                    if (!(minecraft.hitResult instanceof BlockHitResult result) || result.getLocation().distanceTo(player.getEyePosition()) > 1.0F)
                    {
                        WEAPON_BOX = false;
                        return;
                    }
                    BlockPos pos = result.getBlockPos();
                    BlockState state = player.level().getBlockState(pos);
                    if (state.getBlock() instanceof RPGBoxBlock)
                    {
                        if (state.getValue(RPGBoxBlock.HALF) == DoubleBlockHalf.UPPER)
                            pos = pos.below();
                        if (player.level().getBlockEntity(pos) instanceof RPGBoxTE box)
                        {
                            if (!box.hasWeapon || !box.isOpened())
                                WEAPON_BOX = false;
                        }
                        else WEAPON_BOX = false;
                    }
                    else WEAPON_BOX = false;
                }
            }
            else
            {
                ENTITY = null;
                WEAPON_BOX = false;
            }
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return ENTITY != null || WEAPON_BOX;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            ItemStack item = WEAPON_BOX ? RPGBoxTER.RPG_STACK.get() : ENTITY.getStack();

            AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
                stack2.translate(screenWidth / 2.0F, screenHeight, 0.0F);
                stack2.scale(4.5F, 4.5F, 4.5F);
            }, (stack2) -> {
                AVAClientUtil.forceEnable3DModel(() -> stack.renderFakeItem(item, -8, -25));
            });

            stack.pose().pushPose();
            stack.pose().translate(screenWidth / 2.0F, screenHeight, 0.0F);

            stack.drawCenteredString(minecraft.font,
                    "Exchange " + AVAClientUtil.getWeaponDisplayWithAmmoCount(player.getMainHandItem()) + "] with " + AVAClientUtil.getWeaponDisplayWithAmmoCount(item) + "] (G Key)"
                    , 0, -50, AVAConstants.AVA_HUD_TEXT_WHITE);

            stack.pose().popPose();
        }
    }

    public static class ActionPing implements IIndicator
    {
        public static boolean ACTIVE = false;
        public static Vec3 VEC = null;
        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (minecraft.isWindowActive() && minecraft.screen == null && AVAClientConfig.ENABLE_PING_HOTKEY.get() && isFullEquipped(player))
            {
                if (ClientModEventBus.PING.isDown())
                {
                    if (!ACTIVE)
                    {
                        Vec3 eye = AVAWeaponUtil.getEyePositionFor(player);
                        BlockHitResult result = player.level().clip(new ClipContext(eye, eye.add(player.getLookAngle().scale(100.0F)), ClipContext.Block.VISUAL, ClipContext.Fluid.NONE, player));
                        if (result.getType() != HitResult.Type.MISS)
                        {
                            ACTIVE = true;
                            minecraft.mouseHandler.releaseMouse();
                            VEC = result.getLocation();
                        }
                    }
                }
                else
                {
                    int type = getHoveringType(minecraft);
                    if (ACTIVE && VEC != null && type != -1)
                        DataToServerMessage.ping(VEC, type);
                    reset(minecraft);
                }
            }
            else reset(minecraft);
        }

        private void reset(Minecraft minecraft)
        {
            if (ACTIVE)
            {
                ACTIVE = false;
                minecraft.mouseHandler.grabMouse();
                VEC = null;
            }
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return ACTIVE;
        }

        private int getHoveringType(Minecraft minecraft)
        {
            double mX = minecraft.mouseHandler.xpos();
            double mY = minecraft.mouseHandler.ypos();
            double actualW = minecraft.getWindow().getScreenWidth() / 2.0F;
            double actualH = minecraft.getWindow().getScreenHeight() / 2.0F;
            double mX2 = mX - actualW;
            double mY2 = mY - actualH;
            double angle = AVAWeaponUtil.getAngleFromCoord(mX2, -mY2) + 30;
            if (Math.sqrt(mX2 * mX2 + mY2 * mY2) <= 15 * minecraft.getWindow().getGuiScale())
                return -1;
            if (angle > 360 || angle <= 60)
                return 0;
            return ((int) angle / 60);
        }

        private static final ResourceLocation UI_BG = new ResourceLocation(AVA.MODID, "textures/overlay/ping_ui_bg.png");
        private static final ResourceLocation UI_BG_LIT = new ResourceLocation(AVA.MODID, "textures/overlay/ping_ui_bg_lit.png");
        private static final ResourceLocation UI_BG_2 = new ResourceLocation(AVA.MODID, "textures/overlay/ping_ui_bg_2.png");
        private static final ResourceLocation UI_BG_2_LIT = new ResourceLocation(AVA.MODID, "textures/overlay/ping_ui_bg_2_lit.png");
        private static final ResourceLocation UI_BG_ICON_LAYER = new ResourceLocation(AVA.MODID, "textures/overlay/ping_ui_icon_layer.png");

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            float x = screenWidth / 2.0F;
            float y = screenHeight / 2.0F;
            float size = 60;
            int type = getHoveringType(minecraft);
            AVAClientUtil.blit(stack.pose(), type == -1 ? UI_BG_LIT : UI_BG, x - size, y - size, x + size, y + size);
            for (int i = 0; i < 6; i++)
            {
                stack.pose().pushPose();
                stack.pose().translate(x, y, 0.0F);
                stack.pose().mulPose(Axis.ZP.rotationDegrees(i * 60));
                stack.pose().translate(-x, -y, 0.0F);
                AVAClientUtil.blit(stack.pose(), type == i ? UI_BG_2_LIT : UI_BG_2, x - size, y - size, x + size, y + size);
                stack.pose().popPose();
            }
            AVAClientUtil.blit(stack.pose(), UI_BG_ICON_LAYER, x - size, y - size, x + size, y + size);
        }
    }

    public static class NightVision implements IIndicator
    {
        private static int BATTERY_LIVE = 600;
        public static boolean ACTIVATED = false;
        public static final ResourceLocation NIGHT_VISION = new ResourceLocation(AVA.MODID + ":textures/overlay/night_vision.png");
        public static final ResourceLocation BATTERY = new ResourceLocation(AVA.MODID + ":textures/overlay/battery.png");

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (isFullEquipped(player))
            {
                if (ACTIVATED)
                {
                    if (BATTERY_LIVE > 0)
                    {
                        if (!player.isCreative())
                            BATTERY_LIVE--;
                    }
                    else
                    {
                        BATTERY_LIVE = 0;
                        ACTIVATED = false;
                    }
                }
                else if (BATTERY_LIVE < 600) BATTERY_LIVE = Math.min(600, BATTERY_LIVE + 3);
                boolean pressed = false;
                while (ClientModEventBus.NIGHT_VISION_DEVICE_SWITCH.consumeClick())
                    pressed = true;
                if (pressed)
                {
                    if (BATTERY_LIVE <= 200 && !ACTIVATED)
                        return;
                    playSoundOnServer(true, AVASounds.NIGHT_VISION_ACTIVATE.get());
                    ACTIVATED = !ACTIVATED;
                }
            }
            else
                ACTIVATED = false;
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return ACTIVATED;
        }

        @Override
        public boolean firstPersonOnly()
        {
            return false;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            drawTransparent(true);
            RenderSystem.setShader(GameRenderer::getPositionTexShader);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 0.2F);
            RenderSystem.setShaderTexture(0, NIGHT_VISION);
            fillScreen((int) screenWidth, (int) screenHeight);
            RenderSystem.setShaderTexture(0, BATTERY);
            int x = (int) (screenWidth / 2 - 16);
            int y = (int) (screenHeight / 4 * 3);
            Tesselator tessellator = Tesselator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuilder();
            bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
            bufferbuilder.vertex(x, y + 32, -90.0D).uv(0.0F, 1.0F).endVertex();
            bufferbuilder.vertex(x + 32, y + 32, -90.0D).uv(1.0F, 1.0F).endVertex();
            bufferbuilder.vertex(x + 32, y, -90.0D).uv(1.0F, 0.0F).endVertex();
            bufferbuilder.vertex(x, y, -90.0D).uv(0.0F, 0.0F).endVertex();
            tessellator.end();
            drawTransparent(false);
            for (int i = 0; i < 6; i++)
                stack.fill(x + 10, y + 2 + 5 * i, x + 22, y + 6 + 5 * i, BATTERY_LIVE >= (5 - i) * 100 ? 0x80FFFFFF : 0x40AAAAAA);
        }
    }

    public static class Binocular implements IIndicator
    {
        public static final ResourceLocation BINOCULAR = new ResourceLocation(AVA.MODID + ":textures/overlay/binocular.png");
        public static final ResourceLocation BINOCULAR_CD = new ResourceLocation(AVA.MODID + ":textures/overlay/binocular_cd.png");

        private static double DISTANCE = 0.0F;

        @Override
        public boolean requiredFullyEquipped()
        {
            return false;
        }

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            ItemStack stack = getHeldStack(player);
            if (stack.getItem() instanceof BinocularItem && PlayerAction.getCap(player).isADS())
            {
                if (isFocused())
                {
                    if (player.level().getGameTime() % 30 == 0)
                    {
                        Map<Integer, Integer> uavs = AVAWorldData.getInstance(player.level()).uavC;
                        for (LivingEntity entity : AVAWeaponUtil.getEntitiesInSight(LivingEntity.class, player, 20, 20, 100, (entity) -> AVAWeaponUtil.isValidEntity(entity) && (!AVAWeaponUtil.isSameSide(player, entity)) && !(entity instanceof LeopardEntity), false, true, true))
                            uavs.compute(entity.getId(), (id, ticks) -> ticks != null ? Math.max(15, ticks) : 15);
                    }
                    BlockHitResult result = AVAWeaponUtil.rayTraceBlocks(player, ClipContext.Block.VISUAL, 100.0F, false);
                    if (result.getType() != HitResult.Type.MISS)
                        DISTANCE = AVACommonUtil.round(AVAWeaponUtil.getEyePositionFor(player).distanceTo(result.getLocation()), 2);
                    else DISTANCE = -1.0F;
                }
            }
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            ItemStack stack = getHeldStack(player);
            return stack.getItem() instanceof BinocularItem && PlayerAction.getCap(player).isADS();
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            drawTransparent(true);
            RenderSystem.setShader(GameRenderer::getPositionTexShader);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            fillCentredMaxWithFilledSidesAndBob(player, stack.pose(), (int) screenWidth, (int) screenHeight, BinocularStatusManager.INSTANCE.fireInterval <= 0 ? BINOCULAR : BINOCULAR_CD);
            drawTransparent(false);
            bobRenderable(player, stack.pose(), () -> stack.drawCenteredString(minecraft.font, String.valueOf(DISTANCE), (int) (minecraft.getWindow().getGuiScaledWidth() / 2.0F), (int) (minecraft.getWindow().getGuiScaledHeight() / 2.0F + 15), 0xFFDFDFDF));
        }
    }

    public static class BroadCastText implements IIndicator
    {
        private static String TEXT = null;
        private static int DURATION = 0;
        private static int ANIM_DURATION = 0;
        private static String[] ARGS = null;

        public static void setKey(String text, int duration)
        {
            TEXT = text;
            DURATION = duration;
            ANIM_DURATION = 3;
            if (Minecraft.getInstance().cameraEntity != null)
                Minecraft.getInstance().cameraEntity.playSound(AVASounds.BROADCAST_APPEAR.get());
        }

        public static void setArgs(String[] args)
        {
            ARGS = args;
        }

        @Override
        public void render(Minecraft minecraft, Player player, GuiGraphics stack, PlayerAction capability, float screenWidth, float screenHeight, double partialTicks)
        {
            float s = AnimationFactory.BROADCAST_FONT_SIZE.lerpF(partialTicks, ANIM_DURATION / 3.0F);
            stack.pose().pushPose();
            stack.pose().translate(0.0F, 0.0F, 100.0F);
            AVAClientUtil.scaleText(stack.pose(), (int) (screenWidth / 2), 40, 1.3F + s, 1.325F + s, () -> {
                stack.drawCenteredString(minecraft.font, TEXT, 0, -4, AVAConstants.AVA_HUD_TEXT_ORANGE);
            });
            stack.pose().popPose();
        }

        @Override
        public void tick(Minecraft minecraft, Player player)
        {
            if (ANIM_DURATION > 0)
                ANIM_DURATION--;
            if (DURATION > 0)
            {
                DURATION--;
                if (DURATION <= 0)
                    TEXT = null;
            }
        }

        @Override
        public boolean canRender(Minecraft minecraft, Player player)
        {
            return DURATION > 0 && TEXT != null;
        }

        @Override
        public boolean firstPersonOnly()
        {
            return false;
        }

        @Override
        public boolean requiredFullyEquipped()
        {
            return false;
        }

        @Override
        public boolean requiredFocused()
        {
            return false;
        }
    }
}
