package pellucid.ava.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.AbstractClientPlayer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.player.PlayerRenderer;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.client.event.RenderHandEvent;
import org.joml.Vector3f;
import pellucid.ava.client.animation.AVABobAnimator;
import pellucid.ava.client.animation.AVARunTransistor;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.overlay.HurtInfo;
import pellucid.ava.client.renderers.environment.ActivePingEffect;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.player.status.C4StatusManager;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.player.status.ItemStatusManager;
import pellucid.ava.player.status.MeleeStatusManager;
import pellucid.ava.player.status.ProjectileStatusManager;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.Pair;
import pellucid.ava.util.TriFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import static pellucid.ava.util.AVAClientUtil.transferStack;

public class AnimationFactory
{
    private static final List<AnimationFactory> ANIMATION_FACTORIES = new ArrayList<>();
    public static final AnimationFactory HP = new AnimationFactory(0.0F);
    public static final AnimationFactory AP = new AnimationFactory(0.0F);
    public static final AnimationFactory SPREAD = new AnimationFactory(0.0F);
    public static final AnimationFactory RELOAD_ICON = new AnimationFactory(0.0F);
    public static final AnimationFactory SILENCER_INSTALL_ICON = new AnimationFactory(0.0F);
    public static final AnimationFactory FLASH = new AnimationFactory(1.0F);
    public static final AnimationFactory INTERACT = new AnimationFactory(0.0F);
    public static final Map<Entity, AnimationFactory> SMOKE = new HashMap<>();
    public static final Map<ActivePingEffect, AnimationFactory> PING_SCALE = new HashMap<>();
    public static final Map<HitEffect, AnimationFactory> HIT_EFFECT_SCALE = new HashMap<>();
    public static final Map<HurtInfo.Info, AnimationFactory> HURT_INFO_ALPHA = new HashMap<>();
    public static final Map<HurtInfo.Info, AnimationFactory> HURT_INFO_ROT = new HashMap<>();
    public static final Map<Entity, AnimationFactory> PROJECTILE_INDICATOR_ROT = new HashMap<>();
    public static final AnimationFactory BROADCAST_FONT_SIZE = new AnimationFactory(1.0F);
    public static final AnimationFactory MAP_SITE_AREA_ALPHA = new AnimationFactory(0.0F);
    public static final AnimationFactory HURT_ALPHA = new AnimationFactory(1.0F);
    public static final AnimationFactory ADS_SHAKE = new AnimationFactory(0.0F);

    private final double defaultValue;
    private double last = 1.0F;
    private double next = 1.0F;
    private boolean set = false;

    public AnimationFactory()
    {
        this(Double.MAX_VALUE);
    }

    public AnimationFactory(double defaultValue)
    {
        this.defaultValue = defaultValue;
        ANIMATION_FACTORIES.add(this);
    }

    public float lerpPercentage(double partialTicks, double value, double starts)
    {
        return lerpF(partialTicks, Math.min(1.0F, (value / starts)));
    }

    public float lerpPercentage(double partialTicks, double value, double starts, double max)
    {
        return lerpF(partialTicks, Math.min(1.0F, (max - value) / starts));
    }

    public float lerpF(double partialTicks, double value)
    {
        return (float) lerpD(partialTicks, value);
    }

    public double lerpD(double partialTicks, double value)
    {
        return lerp(partialTicks, value, Mth::lerp);
    }

    public float lerpRotF(double partialTicks, double value)
    {
        return (float) lerpRotD(partialTicks, value);
    }

    public double lerpRotD(double partialTicks, double value)
    {
        return lerp(partialTicks, value, AVACommonUtil::rotLerpD);
    }

    private double lerp(double partialTicks, double value, TriFunction<Double, Double, Double, Double> smoother)
    {
        if (!set)
        {
            next = value;
            set = true;
        }
        return smoother.apply(partialTicks, last, next);
    }

    public static void tick(Minecraft minecraft, Player player)
    {
        for (AnimationFactory factory : ANIMATION_FACTORIES)
        {
            if (!factory.set)
                factory.next = factory.defaultValue;
            else
                factory.set = false;
            factory.last = factory.next;
        }
        AVACommonUtil.removeIf(SMOKE, (t, v) -> t.isRemoved());
        AVACommonUtil.removeIf(PING_SCALE, (t, v) -> t.shouldBeDead());
        AVACommonUtil.removeIf(HIT_EFFECT_SCALE, (t, v) -> t.shouldBeDead());
        AVACommonUtil.removeIf(HURT_INFO_ALPHA, (t, v) -> t.shouldBeDead());
        AVACommonUtil.removeIf(HURT_INFO_ROT, (t, v) -> t.shouldBeDead());
        AVACommonUtil.removeIf(PROJECTILE_INDICATOR_ROT, (t, v) -> !t.isAlive());
    }

    public static <I extends Item, SM extends ItemStatusManager<I>> void renderHandsWithAnimations(AbstractClientPlayer player, RenderHandEvent event, AVAModelTypes<I, ?, ?, ?, SM, ?> type, I item, BiFunction<I, ItemStatusManager.IStatus, Pair<Animations, Animations>> animationsGetter, ItemStatusManager.IStatus... animatableStatus)
    {
        PoseStack stack = event.getPoseStack();
        SM manager = type.getStatusManager();

        stack.pushPose();
        stack.translate(0.0D, -0.46F, -0.32F);
        stack.mulPose(Axis.XP.rotationDegrees(-85.0F));
        stack.mulPose(Axis.YP.rotationDegrees(180.0F));
        int progress = manager.getProgress();

        RegularModelProperty<I, ?, ?, ?> model = type.getModel(item);
        PlayerRenderer renderer = (PlayerRenderer) Minecraft.getInstance().getEntityRenderDispatcher().getRenderer(player);

        AVARunTransistor transistor = AVARunTransistor.INSTANCE;
        Pair<Perspective, Perspective> pair;
        if (manager.isActive(animatableStatus))
        {
            Pair<Animations, Animations> animations = animationsGetter.apply(item, manager.status);
            pair = Pair.of(
                    animations.getA().isEmpty() ? Perspective.empty() : getPerspectiveInBetween(animations.getA(), progress),
                    animations.getB().isEmpty() ? Perspective.empty() : getPerspectiveInBetween(animations.getB(), progress)
            );
            pair = transistor.getIdleBobHands(model.handsIdle, pair, new Vector3f(-0.05F), true);
        }
        else
        {
            if (AVABobAnimator.INSTANCE.isSprinting)
                pair = transistor.getRunBobHands(model.run.getB(), new Vector3f(-0.05F), false);
            else
                pair = transistor.getIdleBobHands(model.handsIdle, model.handsIdle, new Vector3f(-0.05F), false);
        }

        if (!pair.getA().isEmpty() && player.getOffhandItem().isEmpty())
        {
            stack.pushPose();
            transferStack(stack, pair.getA());
            renderer.renderLeftHand(stack, event.getMultiBufferSource(), event.getPackedLight(), player);
            stack.popPose();
        }
        if (!pair.getB().isEmpty())
        {
            stack.pushPose();
            transferStack(stack, pair.getB());
            renderer.renderRightHand(stack, event.getMultiBufferSource(), event.getPackedLight(), player);
            stack.popPose();
        }

        stack.popPose();
    }

    @Deprecated
    public static boolean renderHand(PoseStack stack, MultiBufferSource buffer, AbstractClientPlayer player, Animations animations, int ticks, float partialTicks, Pair<Integer, Integer> light, boolean left)
    {
        if (!animations.isEmpty())
        {
            stack.pushPose();
            AVAClientUtil.transferStack(stack, getPerspectiveInBetween(animations, ticks, partialTicks));

            PlayerRenderer renderer = (PlayerRenderer) Minecraft.getInstance().getEntityRenderDispatcher().getRenderer(player);
            if (left)
                renderer.renderLeftHand(stack, buffer, light.getA(), player);
            else
                renderer.renderRightHand(stack, buffer, light.getB(), player);
            stack.popPose();
            return true;
        }
        return false;
    }

    public static Pair<Animations, Animations> getC4Animations(C4Item c4, ItemStatusManager.IStatus status)
    {
        if (status == C4StatusManager.C4Status.DRAW)
            return AVAModelTypes.C4.getModel(c4).draw.getB();
        if (status == C4StatusManager.C4Status.SET)
            return AVAModelTypes.C4.getModel(c4).set.getB();
        return Pair.of(Animations.EMPTY, Animations.EMPTY);
    }

    public static Pair<Animations, Animations> getProjectileAnimations(ThrowableItem projectile, ItemStatusManager.IStatus status)
    {
        if (status == ProjectileStatusManager.ProjectileStatus.DRAW)
            return AVAModelTypes.PROJECTILE.getModel(projectile).draw.getB();
        if (status == ProjectileStatusManager.ProjectileStatus.PIN_OFF)
            return AVAModelTypes.PROJECTILE.getModel(projectile).pinOff.getB();
        if (status == ProjectileStatusManager.ProjectileStatus.THROW)
            return AVAModelTypes.PROJECTILE.getModel(projectile).thr0w.getB();
        if (status == ProjectileStatusManager.ProjectileStatus.TOSS)
            return AVAModelTypes.PROJECTILE.getModel(projectile).toss.getB();
        return Pair.of(Animations.EMPTY, Animations.EMPTY);
    }

    public static Pair<Animations, Animations> getBinocularAnimations(BinocularItem binocular, ItemStatusManager.IStatus status)
    {
        if (status == BinocularStatusManager.BinocularStatus.DRAW)
            return AVAModelTypes.BINOCULAR.getModel(binocular).draw.getB();
        return Pair.of(Animations.EMPTY, Animations.EMPTY);
    }

    public static Pair<Animations, Animations> getMeleeAnimations(AVAMeleeItem knife, ItemStatusManager.IStatus status)
    {
        if (status == MeleeStatusManager.MeleeStatus.DRAW)
            return AVAModelTypes.MELEES.getModel(knife).draw.getB();
        if (status == MeleeStatusManager.MeleeStatus.ATTACK_LIGHT)
            return AVAModelTypes.MELEES.getModel(knife).attackLight.getB();
        if (status == MeleeStatusManager.MeleeStatus.ATTACK_HEAVY)
            return AVAModelTypes.MELEES.getModel(knife).attackHeavy.getB();
        return Pair.of(Animations.EMPTY, Animations.EMPTY);
    }

    public static Pair<Animations, Animations> getGunAnimations(Item item, ItemStatusManager.IStatus status)
    {
        AVAItemGun gun = (AVAItemGun) item;
        if (status == GunStatusManager.GunStatus.RELOAD)
            return AVAModelTypes.GUNS.getModel(gun).reload.getB();
        if (status == GunStatusManager.GunStatus.RELOAD_PRE)
            return AVAModelTypes.GUNS.getModel(gun).preReload.getB();
        if (status == GunStatusManager.GunStatus.RELOAD_POST)
            return AVAModelTypes.GUNS.getModel(gun).postReload.getB();
        if (status == GunStatusManager.GunStatus.FIRE)
            return AVAModelTypes.GUNS.getModel(gun).fire.getB();
        if (status == GunStatusManager.GunStatus.DRAW)
            return AVAModelTypes.GUNS.getModel(gun).draw.getB();
        if (status == GunStatusManager.GunStatus.INSTALL_SILENCER || status == GunStatusManager.GunStatus.UNINSTALL_SILENCER)
            return AVAModelTypes.GUNS.getModel(gun).installSilencerHands;
        return Pair.of(Animations.EMPTY, Animations.EMPTY);
    }

    public static Perspective getPerspectiveInBetween(List<Animation> animations, int currentTicks)
    {
        return getPerspectiveInBetween(animations, currentTicks, !AVAClientUtil.isFocused() ? 1.0F : AVABakedModel.getPartialTicks());
    }

    public static Perspective getPerspectiveInBetween(List<Animation> animations, int currentTicks, float partialTicks)
    {
        if (partialTicks == -1)
            return getPerspectiveInBetween(animations, currentTicks);
        if (AVACommonUtil.similar(partialTicks, 1.0D) || ItemAnimatingGUI.isAnimating())
            partialTicks = 1.0F;
        Function<Integer, Integer> test = (index) -> Math.max(Math.min(index, animations.size() - 1), 0);
        if (!AVAClientUtil.isFocused() || ItemAnimatingGUI.isAnimating())
            return animations.get(test.apply(currentTicks)).target3f;
        return animations.get(test.apply(currentTicks)).target3f.getPerspectivePartial(animations.get(test.apply(currentTicks + 1)).target3f, partialTicks);
    }
}
