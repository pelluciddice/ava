package pellucid.ava.client.renderers;

import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.VertexFormat;
import net.minecraft.client.renderer.RenderStateShard;
import net.minecraft.client.renderer.RenderType;

import java.util.OptionalDouble;

public abstract class AVARenderTypes extends RenderStateShard
{
    public AVARenderTypes(String nameIn, Runnable setupTaskIn, Runnable clearTaskIn)
    {
        super(nameIn, setupTaskIn, clearTaskIn);
    }

    public static final RenderType LINE_0_25D = makeLine(0.25D);
    public static final RenderType LINE_0_5D = makeLine(0.5D);
    public static final RenderType LINE_0_75D = makeLine(0.75D);
    public static final RenderType LINE_1_0D = makeLine(1.0D);
    public static final RenderType LINE_1_5D = makeLine(1.5D);
    public static final RenderType LINE_2_0D = makeLine(2.0D);
    public static final RenderType LINE_3_0D = makeLine(3.0D);
    public static final RenderType LINE_4_0D = makeLine(4.0D);
    public static final RenderType LINE_5_0D = makeLine(5.0D);
    public static final RenderType LINE_8_0D = makeLine(5.0D);
    public static final RenderType LINE_10_0D = makeLine(10.0D);


    private static RenderType makeLine(double width)
    {
        return RenderType.create("lines_" + width, DefaultVertexFormat.POSITION_COLOR_NORMAL, VertexFormat.Mode.LINES, 256, false, false, RenderType.CompositeState.builder().setShaderState(RENDERTYPE_LINES_SHADER).setLineState(new RenderStateShard.LineStateShard(OptionalDouble.of(width))).setLayeringState(VIEW_OFFSET_Z_LAYERING).setTransparencyState(TRANSLUCENT_TRANSPARENCY).setOutputState(ITEM_ENTITY_TARGET).setWriteMaskState(COLOR_DEPTH_WRITE).setCullState(NO_CULL).createCompositeState(false));
    }
}
