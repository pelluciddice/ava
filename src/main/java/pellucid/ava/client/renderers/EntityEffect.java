package pellucid.ava.client.renderers;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;

import java.awt.Color;

public abstract class EntityEffect extends BaseEffect
{
    protected final Entity entity;
    public EntityEffect(Entity entity)
    {
        super(false);
        this.entity = entity;
        this.live = 6;
        this.setColour(new Color(1.0F, 1.0F, 1.0F, 1.0F));
    }

    @Override
    public boolean shouldBeDead()
    {
        return entity == null || entity.isSpectator() || super.shouldBeDead();
    }

    public Entity getEntity()
    {
        return entity;
    }

    public abstract float yOffset(Entity entity);
    public abstract float width(float distance);
    public abstract float height(float distance);
    public abstract ResourceLocation getTexture();
}
