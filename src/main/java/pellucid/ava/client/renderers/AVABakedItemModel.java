package pellucid.ava.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.client.event.ModelEvent;
import org.joml.Vector3f;
import pellucid.ava.client.animation.AVABobAnimator;
import pellucid.ava.client.animation.AVARunTransistor;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.client.renderers.models.QuadAnimator;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.player.status.ItemStatusManager;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class AVABakedItemModel<I extends Item, V extends IModelVariables, S extends ISubModels<I>, P extends RegularModelProperty<I, V, S, P>, SM extends ItemStatusManager<I>> extends AVABakedModel
{
    private final AVAModelTypes<I, V, S, P, ?, ?> type;
    protected final Map<V, Integer> variables = new HashMap<>();
    protected final P properties;
    protected final I item;
    protected final SM manager;

    protected final AVABobAnimator bobber = AVABobAnimator.INSTANCE;
    protected final AVARunTransistor runTransistor = AVARunTransistor.INSTANCE;

    protected final ItemStack stack;
    protected final ClientLevel world;
    protected final LivingEntity entity;
    protected final boolean run;

    public AVABakedItemModel(AVAModelTypes<I, V, S, P, ?, ?> type, BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(origin);
        this.type = type;
        this.properties = type.getModel((I) stack.getItem());
        this.item = (I) stack.getItem();
        this.manager = (SM) type.getStatusManager();
        this.stack = stack;
        this.world = world;
        this.entity = entity;
        Player local = Minecraft.getInstance().player;
        this.run = entity != null && (local == entity ? AVABobAnimator.INSTANCE.isSprinting : entity.isSprinting());
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, RandomSource rand)
    {
        List<BakedQuad> quads = new ArrayList<>();
        if (side != null)
            return quads;
        if (entity != null || AVAClientUtil.USE_FAST_ASSETS)
        {
            if (!(entity instanceof Player ? AVAClientConfig.FAST_ASSETS.get() : AVAClientConfig.AI_FAST_ASSETS.get()))
                quads.addAll(origin.getQuads(state, side, rand));
            else if (hasSimpleModel())
            {
                BakedModel model = getOtherModel(ModelRenderer.SIMPLE_MODELS.get(stack.getItem()));
                if (model != null)
                    quads.addAll(model.getQuads(state, side, rand));
            }
        }
        if (quads.isEmpty())
            quads.addAll(origin.getQuads(state, side, rand));

        if (ItemAnimatingGUI.isAnimating())
        {
            ItemAnimatingGUI gui = (ItemAnimatingGUI<?, ?, ?, ?, ?, ?>) Minecraft.getInstance().screen;
            if (gui.item == stack.getItem())
            {
                if (gui.mode.getVariable() != null)
                    variables.put((V) gui.mode.getVariable(), gui.slider.getValueInt());
            }
        }
        properties.subModels.keySet().stream().filter(ISubModels::addedToModelByDefault).forEach((type) -> {
            getSubModelQuads(type).ifPresent(quads::addAll);
        });
        return quads;
    }

    protected static final Map<ModelResourceLocation, List<BakedQuad>> STATIC_QUADS = new HashMap<>();
    protected List<BakedQuad> get(S model, ModelResourceLocation rel, RandomSource random, List<QuadAnimator.Animator> animators, Map<V, Animations> animations)
    {
        Lazy<List<BakedQuad>> modelQuads = Lazy.of(() -> AVACommonUtil.collect(Minecraft.getInstance().getModelManager().getModel(rel).getQuads(null, null, random), this::copyQuad));
        AtomicBoolean applied = new AtomicBoolean(false);
        for (QuadAnimator.Animator animator : animators)
        {
            List<Integer> current = AVACommonUtil.collect(animator.getVariables(type::constructVariable), this::getVariable);
            if (!animator.test(current))
            {
                if (animator.emptyQuadIfFailed())
                    return Collections.emptyList();
                continue;
            }
            if (!ItemAnimatingGUI.isAnimating() && !animator.getVariables(type::constructVariable).isEmpty() && animator.getVariables(type::constructVariable).get(0).actionExistInactive() || Minecraft.getInstance().player != entity)
                continue;
            animator.apply(modelQuads.get(), current);
            applied.set(true);
        }
        animations.forEach((v, a) -> {
            int current = getVariable(v);
            if (current != 0)
            {
                Vector3f o = properties.quadOrigins.getOrDefault(model, new Vector3f());
                Perspective perspective = AnimationFactory.getPerspectiveInBetween(a, current);
                QuadAnimator.translateQuad(modelQuads.get(), perspective.translation);
                QuadAnimator.rotateQuad(modelQuads.get(), perspective.rotation, o);
                QuadAnimator.scaleQuad(modelQuads.get(), perspective.scale, o);
                applied.set(true);
            }
        });
        return applied.get() ? modelQuads.get() : STATIC_QUADS.computeIfAbsent(rel, (r) -> modelQuads.get());
    }

    public Optional<ModelResourceLocation> getSubModel(S type)
    {
        return Optional.ofNullable(properties.subModels.getOrDefault(type, null));
    }

    public Optional<List<BakedQuad>> getSubModelQuads(S type)
    {
        return getSubModel(type).map((rel) -> {
            return get(type, rel, null, properties.quadAnimators.getOrDefault(type, Collections.emptyList()), properties.quadAnimators2.getOrDefault(type, new HashMap<>()));
        });
    }

    protected <T> T getVariable(IModelVariables variable)
    {
        return (T) variables.get(variable);
    }

    public void addSpecialModels(ModelEvent.RegisterAdditional event)
    {
        Objects.requireNonNull(properties, "Property is null, the model is not registered in the guns model manager?");
        assert properties.subModels != null;
        for (ModelResourceLocation model : properties.subModels.values())
            if (model != null)
                event.register(model);
    }

    @Override
    public BakedModel applyTransform(ItemDisplayContext type, PoseStack poseStack, boolean applyLeftHandTransform)
    {
        Perspective perspective = properties.display.get(type).copy();
        return modifyPerspectiveInner(perspective.rotation, perspective.translation, perspective.scale, type, poseStack);
    }

    protected BakedModel modifyPerspectiveInner(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        if (ItemAnimatingGUI.isAnimating())
        {
            copy(ItemAnimatingGUI.ANIMATING_PERSPECTIVE, rotation, translation, scale);
            return push(rotation, translation, scale, mat);
        }
        return modifyPerspective(rotation, translation, scale, type, mat);
    }

    public static void refreshAll()
    {
        AIM_ANIMATIONS.clear();
        AIM_BACKWARD_ANIMATIONS.clear();
        STATIC_QUADS.clear();
    }

    protected static final Map<Item, Animations> AIM_ANIMATIONS = new HashMap<>();
    protected static final Map<Item, Animations> AIM_BACKWARD_ANIMATIONS = new HashMap<>();

    protected abstract BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat);

    public Vector3f getBobScale(Player player, boolean aiming)
    {
        if (aiming)
        {
            float transScale = 0.075F;
            if (player.isShiftKeyDown())
                transScale *= 0.45F;
            return new Vector3f(transScale, transScale, 1.0F);
        }
        return new Vector3f(0.8F);
    }

    public Perspective getOriginalFpRight()
    {
        return properties.display.get(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND);
    }

    protected boolean hasSimpleModel()
    {
        return true;
    }

    protected boolean canAnimateQuads()
    {
        return entity instanceof Player;
    }
}
