package pellucid.ava.client.renderers.test;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.joml.Vector3f;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.models.AVAModelTypes;

import javax.annotation.Nullable;

public class TestItemModel extends AVABakedItemModel
{
    public TestItemModel(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(AVAModelTypes.GUNS, origin, stack, world, entity);
    }

    @Override
    public BakedModel applyTransform(ItemDisplayContext type, PoseStack mat, boolean applyLeftHandTransform)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(-6F, 6.0F, 6.75F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 0.5F);
        return push(rotation, translation, scale, mat);
    }

    @Override
    protected BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        return null;
    }
}
