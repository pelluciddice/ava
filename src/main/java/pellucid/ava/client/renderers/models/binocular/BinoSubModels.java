package pellucid.ava.client.renderers.models.binocular;

import net.minecraft.client.resources.model.ModelResourceLocation;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.items.miscs.BinocularItem;

import java.util.Locale;

public enum BinoSubModels implements ISubModels<BinocularItem>
{
    ;
    BinoSubModels()
    {
    }

    @Override
    public ModelResourceLocation generateFor(BinocularItem item)
    {
        return null;
    }

    @Override
    public boolean addedToModelByDefault()
    {
        return false;
    }

    @Override
    public String getRLName()
    {
        return name().toLowerCase(Locale.ROOT);
    }
}
