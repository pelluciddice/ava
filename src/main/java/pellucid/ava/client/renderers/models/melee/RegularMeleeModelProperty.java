package pellucid.ava.client.renderers.models.melee;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.util.Pair;

public class RegularMeleeModelProperty extends RegularModelProperty<AVAMeleeItem, MeleeModelVariables, MeleeSubModels, RegularMeleeModelProperty>
{
    public Pair<Animations, Pair<Animations, Animations>> attackLight = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Pair<Animations, Pair<Animations, Animations>> attackHeavy = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));

    public RegularMeleeModelProperty(AVAMeleeItem knife)
    {
        super(AVAModelTypes.MELEES, knife);
    }

    @Override
    public RegularMeleeModelProperty construct(AVAMeleeItem item)
    {
        return new RegularMeleeModelProperty(item);
    }

    public RegularMeleeModelProperty copyFor(AVAMeleeItem knife)
    {
        return super.copyFor(knife)
                .attackLight(Animations.of(attackLight.getA()))
                .attackLightLeft(Animations.of(attackLight.getB().getA()))
                .attackLightRight(Animations.of(attackLight.getB().getB()))
                .attackHeavy(Animations.of(attackHeavy.getA()))
                .attackHeavyLeft(Animations.of(attackHeavy.getB().getA()))
                .attackHeavyRight(Animations.of(attackHeavy.getB().getB()));
    }

    public RegularMeleeModelProperty attackLight(Animations animations)
    {
        attackLight.setA(animations);
        return this;
    }

    public RegularMeleeModelProperty attackLightLeft(Animations animations)
    {
        attackLight.getB().setA(animations);
        return this;
    }

    public RegularMeleeModelProperty attackLightRight(Animations animations)
    {
        attackLight.getB().setB(animations);
        return this;
    }

    public RegularMeleeModelProperty attackHeavy(Animations animations)
    {
        attackHeavy.setA(animations);
        return this;
    }

    public RegularMeleeModelProperty attackHeavyLeft(Animations animations)
    {
        attackHeavy.getB().setA(animations);
        return this;
    }

    public RegularMeleeModelProperty attackHeavyRight(Animations animations)
    {
        attackHeavy.getB().setB(animations);
        return this;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        super.writeToJson(json);

        writeAnimations(attackLight, json, "attackLeft");
        writeAnimations(attackHeavy, json, "attackRight");
    }

    @Override
    public void readFromJson(JsonElement json2)
    {
        super.readFromJson(json2);
        JsonObject json = json2.getAsJsonObject();

        readAnimations(attackLight, json, "attackLeft");
        readAnimations(attackHeavy, json, "attackRight");
    }
}
