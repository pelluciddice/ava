package pellucid.ava.client.renderers.models.projectile;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.util.Pair;

public class RegularProjectileModelProperty extends RegularModelProperty<ThrowableItem, ProjectileModelVariables, ProjectileSubModels, RegularProjectileModelProperty>
{
    public Pair<Animations, Pair<Animations, Animations>> pinOff = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Pair<Animations, Pair<Animations, Animations>> thr0w = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Pair<Animations, Pair<Animations, Animations>> toss = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));

    public RegularProjectileModelProperty(ThrowableItem Projectilecular)
    {
        super(AVAModelTypes.PROJECTILE, Projectilecular);
    }

    @Override
    public RegularProjectileModelProperty construct(ThrowableItem item)
    {
        return new RegularProjectileModelProperty(item);
    }

    public RegularProjectileModelProperty copyFor(ThrowableItem Projectilecular)
    {
        return super.copyFor(Projectilecular)
                .pinOff(Animations.of(pinOff.getA()))
                .pinOffLeft(Animations.of(pinOff.getB().getA()))
                .pinOffRight(Animations.of(pinOff.getB().getB()))
                .thr0w(Animations.of(thr0w.getA()))
                .thr0wLeft(Animations.of(thr0w.getB().getA()))
                .thr0wRight(Animations.of(thr0w.getB().getB()))
                .toss(Animations.of(toss.getA()))
                .tossLeft(Animations.of(toss.getB().getA()))
                .tossRight(Animations.of(toss.getB().getB()))
                ;
    }

    public RegularProjectileModelProperty pinOff(Animations animations)
    {
        pinOff.setA(animations);
        return this;
    }

    public RegularProjectileModelProperty pinOffLeft(Animations animations)
    {
        pinOff.getB().setA(animations);
        return this;
    }

    public RegularProjectileModelProperty pinOffRight(Animations animations)
    {
        pinOff.getB().setB(animations);
        return this;
    }

    public RegularProjectileModelProperty thr0w(Animations animations)
    {
        thr0w.setA(animations);
        return this;
    }

    public RegularProjectileModelProperty thr0wLeft(Animations animations)
    {
        thr0w.getB().setA(animations);
        return this;
    }

    public RegularProjectileModelProperty thr0wRight(Animations animations)
    {
        thr0w.getB().setB(animations);
        return this;
    }

    public RegularProjectileModelProperty toss(Animations animations)
    {
        toss.setA(animations);
        return this;
    }

    public RegularProjectileModelProperty tossLeft(Animations animations)
    {
        toss.getB().setA(animations);
        return this;
    }

    public RegularProjectileModelProperty tossRight(Animations animations)
    {
        toss.getB().setB(animations);
        return this;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        super.writeToJson(json);
        writeAnimations(pinOff, json, "pinOff");
        writeAnimations(thr0w, json, "throw");
        writeAnimations(toss, json, "toss");
    }

    @Override
    public void readFromJson(JsonElement json2)
    {
        super.readFromJson(json2);
        JsonObject json = json2.getAsJsonObject();
        readAnimations(pinOff, json, "pinOff");
        readAnimations(thr0w, json, "throw");
        readAnimations(toss, json, "toss");
    }
}
