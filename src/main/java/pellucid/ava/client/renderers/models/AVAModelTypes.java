package pellucid.ava.client.renderers.models;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.world.item.Item;
import pellucid.ava.client.guis.BinocularAnimatingGUI;
import pellucid.ava.client.guis.C4AnimatingGUI;
import pellucid.ava.client.guis.GunAnimatingGUI;
import pellucid.ava.client.guis.MeleeAnimatingGUI;
import pellucid.ava.client.guis.ProjectileAnimatingGUI;
import pellucid.ava.client.renderers.models.binocular.BinoModelVariables;
import pellucid.ava.client.renderers.models.binocular.BinoSubModels;
import pellucid.ava.client.renderers.models.binocular.RegularBinoModelProperty;
import pellucid.ava.client.renderers.models.c4.C4ModelVariables;
import pellucid.ava.client.renderers.models.c4.C4SubModels;
import pellucid.ava.client.renderers.models.c4.RegularC4ModelProperty;
import pellucid.ava.client.renderers.models.guns.GunModelVariables;
import pellucid.ava.client.renderers.models.guns.GunSubModels;
import pellucid.ava.client.renderers.models.guns.RegularGunModelProperty;
import pellucid.ava.client.renderers.models.melee.MeleeModelVariables;
import pellucid.ava.client.renderers.models.melee.MeleeSubModels;
import pellucid.ava.client.renderers.models.melee.RegularMeleeModelProperty;
import pellucid.ava.client.renderers.models.projectile.ProjectileModelVariables;
import pellucid.ava.client.renderers.models.projectile.ProjectileSubModels;
import pellucid.ava.client.renderers.models.projectile.RegularProjectileModelProperty;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.events.data.custom.models.items.BinoModelResourcesManager;
import pellucid.ava.events.data.custom.models.items.C4ModelResourcesManager;
import pellucid.ava.events.data.custom.models.items.GunModelResourcesManager;
import pellucid.ava.events.data.custom.models.items.MeleeModelResourcesManager;
import pellucid.ava.events.data.custom.models.items.ProjectileModelResourcesManager;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.player.status.C4StatusManager;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.player.status.ItemStatusManager;
import pellucid.ava.player.status.MeleeStatusManager;
import pellucid.ava.player.status.ProjectileStatusManager;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class AVAModelTypes<I extends Item, V extends IModelVariables, S extends ISubModels<I>, P extends RegularModelProperty<I, V, S, P>, SM extends ItemStatusManager<I>, MM extends ItemModelResourcesManager<I, S, P>>
{
    public static final AVAModelTypes<AVAItemGun, GunModelVariables, GunSubModels, RegularGunModelProperty, GunStatusManager, GunModelResourcesManager> GUNS = new AVAModelTypes<>("guns", GunModelVariables.getAnimatableVariables(), () -> GunStatusManager.INSTANCE, () -> GunModelResourcesManager.INSTANCE, RegularGunModelProperty::new, GunModelVariables::valueOf, GunSubModels::valueOf, (item) -> item instanceof AVAItemGun, GunAnimatingGUI::new);
    public static final AVAModelTypes<AVAMeleeItem, MeleeModelVariables, MeleeSubModels, RegularMeleeModelProperty, MeleeStatusManager, MeleeModelResourcesManager> MELEES = new AVAModelTypes<>("melees", MeleeModelVariables.getAnimatableVariables(), () -> MeleeStatusManager.INSTANCE, () -> MeleeModelResourcesManager.INSTANCE, RegularMeleeModelProperty::new, MeleeModelVariables::valueOf, MeleeSubModels::valueOf, (item) -> item instanceof AVAMeleeItem, MeleeAnimatingGUI::new);
    public static final AVAModelTypes<BinocularItem, BinoModelVariables, BinoSubModels, RegularBinoModelProperty, BinocularStatusManager, BinoModelResourcesManager> BINOCULAR = new AVAModelTypes<>("binocular", BinoModelVariables.getAnimatableVariables(), () -> BinocularStatusManager.INSTANCE, () -> BinoModelResourcesManager.INSTANCE, RegularBinoModelProperty::new, BinoModelVariables::valueOf, BinoSubModels::valueOf, (item) -> item instanceof BinocularItem, BinocularAnimatingGUI::new);
    public static final AVAModelTypes<ThrowableItem, ProjectileModelVariables, ProjectileSubModels, RegularProjectileModelProperty, ProjectileStatusManager, ProjectileModelResourcesManager> PROJECTILE = new AVAModelTypes<>("projectile", ProjectileModelVariables.getAnimatableVariables(), () -> ProjectileStatusManager.INSTANCE, () -> ProjectileModelResourcesManager.INSTANCE, RegularProjectileModelProperty::new, ProjectileModelVariables::valueOf, ProjectileSubModels::valueOf, (item) -> item instanceof ThrowableItem, ProjectileAnimatingGUI::new);
    public static final AVAModelTypes<C4Item, C4ModelVariables, C4SubModels, RegularC4ModelProperty, C4StatusManager, C4ModelResourcesManager> C4 = new AVAModelTypes<>("c4", C4ModelVariables.getAnimatableVariables(), () -> C4StatusManager.INSTANCE, () -> C4ModelResourcesManager.INSTANCE, RegularC4ModelProperty::new, C4ModelVariables::valueOf, C4SubModels::valueOf, (item) -> item instanceof C4Item, C4AnimatingGUI::new);

    public static final List<AVAModelTypes<?, ?, ?, ?, ?, ?>> TYPES = ImmutableList.of(GUNS, MELEES, BINOCULAR, PROJECTILE, C4);

    public final String name;
    public final List<? extends IModelVariables> animatableVariables;
    private final Supplier<SM> statusManager;
    private final Supplier<MM> modelManager;
    private final Function<I, P> constructProperty;
    private final Function<String, V> constructVariable;
    private final Function<String, S> subModelGetter;
    private final Predicate<Item> isRightItem;
    private final Supplier<Screen> animatingScreen;

    public AVAModelTypes(String name, List<? extends IModelVariables> animatableVariables, Supplier<SM> statusManager, Supplier<MM> modelManager, Function<I, P> constructProperty, Function<String, V> constructVariable, Function<String, S> subModelGetter, Predicate<Item> isRightItem, Supplier<Screen> animatingScreen)
    {
        this.name = name;
        this.animatableVariables = animatableVariables;
        this.statusManager = statusManager;
        this.modelManager = modelManager;
        this.constructProperty = constructProperty;
        this.constructVariable = constructVariable;
        this.subModelGetter = subModelGetter;
        this.isRightItem = isRightItem;
        this.animatingScreen = animatingScreen;
    }

    public Screen getAnimatingScreen()
    {
        return animatingScreen.get();
    }

    public SM getStatusManager()
    {
        return statusManager.get();
    }

    public MM getModelManager()
    {
        return modelManager.get();
    }

    public boolean isRightItem(Item item)
    {
        return isRightItem.test(item);
    }

    public P getModel(I item)
    {
        return modelManager.get().getModelProperties().getOrDefault(item, constructProperty(item));
    }

    public S getSubModel(String name)
    {
        return subModelGetter.apply(name);
    }

    public P constructProperty(I item)
    {
        return constructProperty.apply(item);
    }

    public V constructVariable(String name)
    {
        return constructVariable.apply(name);
    }
}
