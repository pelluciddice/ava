package pellucid.ava.client.renderers.models;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.ItemOverrides;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import pellucid.ava.client.renderers.ModelGetter;

import javax.annotation.Nullable;
import java.util.List;

public abstract class BakedModelWrapper implements BakedModel
{
    protected BakedModel origin;

    public BakedModelWrapper(BakedModel origin)
    {
        this.origin = origin;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, RandomSource rand)
    {
        return origin.getQuads(state, side, rand);
    }

    public static ItemOverrides createOverride(ModelGetter modelConstructor)
    {
        return new ItemOverrides()
        {
            @Nullable
            @Override
            public BakedModel resolve(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity, int id)
            {
                return modelConstructor.getModel(origin, stack, world, entity);
            }
        };
    }
    @Override
    public boolean useAmbientOcclusion()
    {
        return origin.useAmbientOcclusion();
    }

    @Override
    public boolean isGui3d()
    {
        return origin.isGui3d();
    }

    @Override
    public boolean usesBlockLight()
    {
        return false;
    }

    @Override
    public boolean isCustomRenderer()
    {
        return origin.isCustomRenderer();
    }

    @Override
    public TextureAtlasSprite getParticleIcon()
    {
        return origin.getParticleIcon();
    }

    public static class Modded extends BakedModelWrapper
    {
        private final ModelGetter modelConstructor;

        public Modded(BakedModel origin, ModelGetter modelConstructor)
        {
            super(origin);
            this.modelConstructor = modelConstructor;
        }

        @Override
        public ItemOverrides getOverrides()
        {
            return createOverride(modelConstructor);
        }
    }
}
