package pellucid.ava.client.renderers.models;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.math.Transformation;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.core.Direction;
import net.minecraft.util.Mth;
import net.neoforged.neoforge.client.model.IQuadTransformer;
import net.neoforged.neoforge.client.model.QuadTransformers;
import net.neoforged.neoforge.common.util.TransformationHelper;
import org.joml.Vector3f;
import pellucid.ava.client.renderers.AVABakedModel;
import pellucid.ava.client.renderers.models.guns.GunModelVariables;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.IJsonSerializable;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.lang.Math.abs;

public abstract class QuadAnimator implements IJsonSerializable<QuadAnimator>
{

    public static void translateQuad(List<BakedQuad> quads, Vector3f vec)
    {
        if (!AVACommonUtil.similar(vec.x, 0))
            translateQuad(quads, vec.x, Direction.EAST);
        if (!AVACommonUtil.similar(vec.y, 0))
            translateQuad(quads, vec.y, Direction.UP);
        if (!AVACommonUtil.similar(vec.z, 0))
            translateQuad(quads, vec.z, Direction.SOUTH);
    }

    public static void rotateQuad(List<BakedQuad> quads, Vector3f vec, Vector3f origin)
    {
        if (!AVACommonUtil.similar(vec.x, 0))
            rotateQuad(quads, Direction.Axis.X, vec.x, origin);
        if (!AVACommonUtil.similar(vec.y, 0))
            rotateQuad(quads, Direction.Axis.Y, vec.y, origin);
        if (!AVACommonUtil.similar(vec.z, 0))
            rotateQuad(quads, Direction.Axis.Z, vec.z, origin);
    }

    public static void scaleQuad(List<BakedQuad> quads, Vector3f vec, Vector3f origin)
    {
        if (!AVACommonUtil.similar(vec.x, 1))
            scaleQuad(quads, vec.x, origin, Direction.Axis.X);
        if (!AVACommonUtil.similar(vec.y, 1))
            scaleQuad(quads, vec.y, origin, Direction.Axis.Y);
        if (!AVACommonUtil.similar(vec.z, 1))
            scaleQuad(quads, vec.z, origin, Direction.Axis.Z);
    }

    public static boolean translateQuadFrom(List<BakedQuad> quads, float awayFrom, int currentTicks, int fromTicks, int toTicks, Direction... directions)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        awayFrom = abs(awayFrom);
        float prev = awayFrom - awayFrom * AVACommonUtil.percentageF(fromTicks, toTicks, currentTicks);
        float next = awayFrom - awayFrom * AVACommonUtil.percentageF(fromTicks, toTicks, Math.min(currentTicks + 1, toTicks));
        translateQuad(quads, Mth.lerp(AVABakedModel.getPartialTicks(), prev, next), directions);
        return true;
    }

    public static boolean translateQuadTo(List<BakedQuad> quads, float amount, int currentTicks, int fromTicks, int toTicks, Direction... directions)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        float prev = amount * AVACommonUtil.percentageF(fromTicks, toTicks, currentTicks);
        float next = amount * AVACommonUtil.percentageF(fromTicks, toTicks, Math.min(currentTicks + 1, toTicks));
        translateQuad(quads, Mth.lerp(AVABakedModel.getPartialTicks(), prev, next), directions);
        return true;
    }

    public static boolean translateQuad(List<BakedQuad> quads, float amount, int currentTicks, int fromTicks, int toTicks, Direction... directions)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        translateQuad(quads, amount, directions);
        return true;
    }

    public static void translateQuad(List<BakedQuad> quads, float amount, Direction... directions)
    {
        for (Direction direction : directions)
            translateQuad(quads, direction, amount);
    }

    public static void translateQuad(List<BakedQuad> quads, Direction direction, final float amount)
    {
        for (BakedQuad quad : quads)
        {
            float amount2 = amount / 16.0F;
            int index;
            int[] vertex = quad.getVertices();
            int size = vertex.length / 4;
            switch (direction)
            {
                case NORTH:
                case SOUTH:
                default:
                    index = 2;
                    if (direction == Direction.NORTH)
                        amount2 = -amount;
                    break;
                case UP:
                case DOWN:
                    index = 1;
                    if (direction == Direction.DOWN)
                        amount2 = -amount;
                    break;
                case EAST:
                case WEST:
                    index = 0;
                    if (direction == Direction.WEST)
                        amount2 = -amount;
                    break;
            }
            for (; index < vertex.length; index += size)
                vertex[index] = Float.floatToRawIntBits(Float.intBitsToFloat(vertex[index]) + amount2);
        }
    }

    public static boolean rotateQuadFrom(List<BakedQuad> quads, float awayFrom, int currentTicks, int fromTicks, int toTicks, Direction.Axis direction, Vector3f pivot)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        float prev = awayFrom - awayFrom * AVACommonUtil.percentageF(fromTicks, toTicks, currentTicks);
        float next = awayFrom - awayFrom * AVACommonUtil.percentageF(fromTicks, toTicks, Math.min(currentTicks + 1, toTicks));
        return rotateQuad(quads, direction, Mth.lerp(AVABakedModel.getPartialTicks(), prev, next), pivot);
    }

    public static boolean rotateQuadTo(List<BakedQuad> quads, float amount, int currentTicks, int fromTicks, int toTicks, Direction.Axis direction, Vector3f pivot)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        float prev = amount * AVACommonUtil.percentageF(fromTicks, toTicks, currentTicks);
        float next = amount * AVACommonUtil.percentageF(fromTicks, toTicks, Math.min(currentTicks + 1, toTicks));
        return rotateQuad(quads, direction, Mth.lerp(AVABakedModel.getPartialTicks(), prev, next), pivot);
    }

    public static boolean rotateQuad(List<BakedQuad> quads, float amount, int currentTicks, int fromTicks, int toTicks, Direction.Axis direction, Vector3f pivot)
    {
        if (currentTicks < fromTicks || currentTicks >= toTicks)
            return false;
        rotateQuad(quads, direction, amount, pivot);
        return true;
    }

    public static boolean rotateQuad(List<BakedQuad> quads, Direction.Axis direction, float amount, Vector3f offsets)
    {
        Vector3f rotation = new Vector3f();
        switch (direction)
        {
            case X:
            default:
                rotation.x = 1.0F;
                break;
            case Y:
                rotation.y = 1.0F;
                break;
            case Z:
                rotation.z = 1.0F;
                break;
        }
        rotation.mul(amount);
        processQuads(quads, QuadTransformers.applying(new Transformation(null, TransformationHelper.quatFromXYZ(rotation, true), null, null)), offsets);
        return true;
        //        List<BakedQuad> quads1 = new ArrayList<>();
        //        offsets.mul(0.0625F, 0.0625F, 0.0625F);
        //        for (BakedQuad quad : quads)
        //        {
        //            int index;
        //            int[] vertex = quad.getVertexData();
        //            for (index=0;index<vertex.length; index+=8)
        //            {
        //                float x2 = Float.intBitsToFloat(vertex[index]);
        //                float y2 = Float.intBitsToFloat(vertex[index + 1]);
        //                float z2 = Float.intBitsToFloat(vertex[index + 2]);
        //                float x = x2 - offsets.getX();
        //                float y = y2 - offsets.getY();
        //                float z = z2 - offsets.getZ();
        //                if (direction == Direction.Axis.X)
        //                {
        //                    vertex[index + 1] = Float.floatToRawIntBits((float) (y * cos(amount) - z * sin(amount)) + offsets.getY());
        //                    vertex[index + 2] = Float.floatToRawIntBits((float) (z * cos(amount) + y * sin(amount)) + offsets.getZ());
        //                }
        //                else if (direction == Direction.Axis.Y)
        //                {
        //                    vertex[index] = Float.floatToRawIntBits((float) (x * cos(amount) + z * sin(amount)) + offsets.getX());
        //                    vertex[index + 2] = Float.floatToRawIntBits((float) (z * cos(amount) - x * sin(amount)) + offsets.getZ());
        //                }
        //                else
        //                {
        //                    vertex[index] = Float.floatToRawIntBits((float) (x * cos(amount) - y * sin(amount)) + offsets.getX());
        //                    vertex[index + 1] = Float.floatToRawIntBits((float) (y * cos(amount) + x * sin(amount)) + offsets.getY());
        //                }
        //            }
        //            quads1.add(quad);
        //        }
        //        return quads1;
    }

    public static List<BakedQuad> scaleQuad(List<BakedQuad> quads, float amount, Vector3f pivot, Direction.Axis... directions)
    {
        Vector3f scale = new Vector3f(1, 1, 1);
        for (Direction.Axis direction : directions)
        {
            switch (direction)
            {
                default -> scale.x = amount;
                case Y -> scale.y = amount;
                case Z -> scale.z = amount;
            }
        }
        scale.mul(amount);
        return processQuads(quads, QuadTransformers.applying(new Transformation(null, null, scale, null)), pivot);
    }

    public static List<BakedQuad> processQuads(List<BakedQuad> quads, IQuadTransformer transformer2, Vector3f pivot)
    {
        pivot = new Vector3f(pivot);
        pivot.mul(-0.0625F, -0.0625F, -0.0625F);
        IQuadTransformer transformer = QuadTransformers.applying(new Transformation(pivot, null, null, null));
        pivot.mul(-1.0F, -1.0F, -1.0F);
        IQuadTransformer transformer3 = QuadTransformers.applying(new Transformation(pivot, null, null, null));
        transformer.processInPlace(quads);
        transformer2.processInPlace(quads);
        transformer3.processInPlace(quads);
        return quads;
    }

    private static final Map<String, Supplier<Animator>> ANIMATOR_CONSTRUCTORS = ImmutableMap.of(
            "translate", Animator.Translator::new,
            "vanish", Animator.Vanisher::new,
            "silencer", Animator.Silencer::new,
            "rotate", Animator.Rotator::new,
            "scale", Animator.Scaler::new
    );
    public static Animator fromJson(JsonObject json)
    {
        Animator anim = ANIMATOR_CONSTRUCTORS.get(json.get("type").getAsString()).get();
        anim.readFromJson(json);
        return anim;
    }

    public static abstract class Animator implements BiFunction<List<BakedQuad>, List<Integer>, Boolean>, IJsonSerializable<Animator>, Predicate<List<Integer>>
    {
        protected Motion motion;
        protected String variable;
        protected float amount;
        protected int fromTicks;
        protected int toTicks;

        protected Animator()
        {

        }

        protected Animator(Motion motion, IModelVariables variables, float amount, int fromTicks, int toTicks)
        {
            this(motion, variables.getName(), amount, fromTicks, toTicks);
        }

        protected Animator(Motion motion, String variables, float amount, int fromTicks, int toTicks)
        {
            this.motion = motion;
            this.variable = variables;
            this.amount = amount;
            this.fromTicks = fromTicks;
            this.toTicks = toTicks;
        }

        @Override
        public boolean test(List<Integer> integers)
        {
            return integers.get(0) >= fromTicks && integers.get(0) < toTicks;
        }

        public List<IModelVariables> getVariables(Function<String, IModelVariables> caster)
        {
            return ImmutableList.of(caster.apply(variable));
        }

        public boolean emptyQuadIfFailed()
        {
            return false;
        }

        public abstract Animator copy();

        @Override
        public void writeToJson(JsonObject json2)
        {
            JsonObject json = json2.getAsJsonObject();
            json.addProperty("type", getName());
            json.addProperty("motion", motion.name());
            json.addProperty("variable", variable);
            json.addProperty("amount", amount);
            json.addProperty("fromTicks", fromTicks);
            json.addProperty("toTicks", toTicks);
        }

        @Override
        public void readFromJson(JsonElement json2)
        {
            JsonObject json = json2.getAsJsonObject();
            motion = Motion.valueOf(json.get("motion").getAsString());
            variable = json.get("variable").getAsString();
            amount = json.get("amount").getAsFloat();
            fromTicks = json.get("fromTicks").getAsInt();
            toTicks = json.get("toTicks").getAsInt();
        }

        protected abstract String getName();

        public static class Translator extends Animator
        {
            protected Direction[] directions;

            public Translator()
            {
                super();
            }

            public Translator(Motion motion, IModelVariables variables, float amount, int fromTicks, int toTicks, Direction... directions)
            {
                super(motion, variables, amount, fromTicks, toTicks);
                this.directions = directions;
            }

            public Translator(Motion motion, String variable, float amount, int fromTicks, int toTicks, Direction... directions)
            {
                super(motion, variable, amount, fromTicks, toTicks);
                this.directions = directions;
            }

            @Override
            public Animator copy()
            {
                return new Translator(motion, variable, amount, fromTicks, toTicks, directions.clone());
            }

            @Override
            public void writeToJson(JsonObject json2)
            {
                super.writeToJson(json2);
                JsonArray directions = new JsonArray();
                for (Direction direction : this.directions)
                    directions.add(direction.name().toLowerCase(Locale.ROOT));
                json2.add("directions", directions);
            }

            @Override
            public void readFromJson(JsonElement json2)
            {
                super.readFromJson(json2);
                JsonArray array = json2.getAsJsonObject().getAsJsonArray("directions");
                this.directions = new Direction[array.size()];
                int i = 0;
                for (JsonElement json : array)
                    this.directions[i++] = (Direction.valueOf(json.getAsString().toUpperCase(Locale.ROOT)));
            }

            @Override
            protected String getName()
            {
                return "translate";
            }

            @Override
            public Boolean apply(List<BakedQuad> bakedQuad, List<Integer> current)
            {
                return switch (motion)
                {
                    case TO -> translateQuadTo(bakedQuad, amount, current.get(0), fromTicks, toTicks, directions);
                    case FROM -> translateQuadFrom(bakedQuad, amount, current.get(0), fromTicks, toTicks, directions);
                    case CONSTANT -> translateQuad(bakedQuad, amount, current.get(0), fromTicks, toTicks, directions);
                };
            }

            @Override
            public String toString()
            {
                return "Translator{" +
                        "directions=" + Arrays.toString(directions) +
                        ", motion=" + motion +
                        ", variable=" + variable +
                        ", amount=" + amount +
                        ", fromTicks=" + fromTicks +
                        ", toTicks=" + toTicks +
                        '}';
            }
        }

        public static class Rotator extends Animator
        {
            private Direction.Axis axis;
            private Vector3f pivot;

            public Rotator()
            {
                super();
            }

            public Rotator(Motion motion, IModelVariables variables, float amount, int fromTicks, int toTicks, Direction.Axis axis, Vector3f pivot)
            {
                super(motion, variables, amount, fromTicks, toTicks);
                this.axis = axis;
                this.pivot = pivot;
            }

            public Rotator(Motion motion, String variable, float amount, int fromTicks, int toTicks, Direction.Axis axis, Vector3f pivot)
            {
                super(motion, variable, amount, fromTicks, toTicks);
                this.axis = axis;
                this.pivot = pivot;
            }

            @Override
            public Animator copy()
            {
                return new Rotator(motion, variable, amount, fromTicks, toTicks, axis, new Vector3f(pivot));
            }

            @Override
            public void writeToJson(JsonObject json2)
            {
                super.writeToJson(json2);
                json2.addProperty("axis", axis.name().toLowerCase(Locale.ROOT));
                json2.add("pivot", IJsonSerializable.vector3fW(pivot));

            }

            @Override
            public void readFromJson(JsonElement json2)
            {
                super.readFromJson(json2);
                axis = Direction.Axis.valueOf(json2.getAsJsonObject().get("axis").getAsString().toUpperCase(Locale.ROOT));
                pivot = IJsonSerializable.vector3fR(json2.getAsJsonObject().getAsJsonArray("pivot"));
            }

            @Override
            protected String getName()
            {
                return "rotate";
            }

            @Override
            public Boolean apply(List<BakedQuad> bakedQuad, List<Integer> current)
            {
                return switch (motion)
                {
                    case TO -> rotateQuadTo(bakedQuad, amount, current.get(0), fromTicks, toTicks, axis, pivot);
                    case FROM -> rotateQuadFrom(bakedQuad, amount, current.get(0), fromTicks, toTicks, axis, pivot);
                    case CONSTANT -> rotateQuad(bakedQuad, amount, current.get(0), fromTicks, toTicks, axis, pivot);
                };
            }

            @Override
            public String toString()
            {
                return "Rotator{" +
                        "axis=" + axis +
                        ", pivot=" + pivot +
                        '}';
            }
        }

        public static class Vanisher extends Animator
        {
            public Vanisher()
            {
                super();
            }

            public Vanisher(Motion motion, IModelVariables variables, int fromTicks, int toTicks)
            {
                super(motion, variables, 0, fromTicks, toTicks);
            }

            public Vanisher(Motion motion, String variable, int fromTicks, int toTicks)
            {
                super(motion, variable, 0, fromTicks, toTicks);
            }

            @Override
            public Animator copy()
            {
                return new Vanisher(motion, variable, fromTicks, toTicks);
            }

            @Override
            public boolean emptyQuadIfFailed()
            {
                return true;
            }

            @Override
            protected String getName()
            {
                return "vanish";
            }

            @Override
            public Boolean apply(List<BakedQuad> bakedQuad, List<Integer> current)
            {
                return !switch (motion)
                {
                    case TO -> current.get(0) <= fromTicks || current.get(0) >= toTicks;
                    case FROM -> current.get(0) >= fromTicks && current.get(0) <= toTicks;
                    case CONSTANT -> current.get(0) == fromTicks || current.get(0) == toTicks;
                };
            }

            @Override
            public boolean test(List<Integer> integers)
            {
                return apply(ImmutableList.of(), integers);
            }

        }

        public static class Silencer extends Animator
        {
            public Silencer()
            {
                super();
            }

            @Override
            public boolean emptyQuadIfFailed()
            {
                return true;
            }

            @Override
            public Animator copy()
            {
                return new Silencer();
            }

            @Override
            protected String getName()
            {
                return "silencer";
            }

            @Override
            public List<IModelVariables> getVariables(Function<String, IModelVariables> caster)
            {
                return ImmutableList.of(GunModelVariables.INSTALLED_SILENCER, GunModelVariables.INSTALL_SILENCER);
            }

            @Override
            public Boolean apply(List<BakedQuad> bakedQuad, List<Integer> integer)
            {
//                if (installedSilencer ? (installSilencer <= 8) : (installSilencer >= 8))
                return integer.get(0) == 1 ? (integer.get(1) <= 8) : (integer.get(1) >= 8);
            }

            @Override
            public boolean test(List<Integer> integers)
            {
                return apply(ImmutableList.of(), integers);
            }

            @Override
            public void writeToJson(JsonObject json2)
            {
                JsonObject json = json2.getAsJsonObject();
                json.addProperty("type", getName());
            }

            @Override
            public void readFromJson(JsonElement json2)
            {
            }
        }

        public static class Scaler extends Animator
        {
            private Vector3f pivot;

            public Scaler()
            {
                super();
            }

            public Scaler(Motion motion, IModelVariables variables, float amount, int fromTicks, int toTicks, Vector3f pivot)
            {
                super(motion, variables, amount, fromTicks, toTicks);
                this.pivot = pivot;
            }

            public Scaler(Motion motion, String variable, float amount, int fromTicks, int toTicks, Vector3f pivot)
            {
                super(motion, variable, amount, fromTicks, toTicks);
                this.pivot = pivot;
            }

            @Override
            public Animator copy()
            {
                return new Scaler(motion, variable, amount, fromTicks, toTicks, new Vector3f(pivot));
            }

            @Override
            public void writeToJson(JsonObject json2)
            {
                super.writeToJson(json2);
                json2.add("pivot", IJsonSerializable.vector3fW(pivot));

            }

            @Override
            public void readFromJson(JsonElement json2)
            {
                super.readFromJson(json2);
                pivot = IJsonSerializable.vector3fR(json2.getAsJsonObject().getAsJsonArray("pivot"));
            }

            @Override
            protected String getName()
            {
                return "scale";
            }

            @Override
            public Boolean apply(List<BakedQuad> bakedQuad, List<Integer> current)
            {
                if (!switch (motion)
                {
                    case TO -> current.get(0) <= fromTicks || current.get(0) >= toTicks;
                    case FROM -> current.get(0) >= fromTicks && current.get(0) <= toTicks;
                    case CONSTANT -> current.get(0) == fromTicks || current.get(0) == toTicks;
                })
                {
                    if (!bakedQuad.isEmpty())
                        scaleQuad(bakedQuad, amount, pivot);
                    return true;
                }
                return false;
            }

            @Override
            public boolean test(List<Integer> integers)
            {
                return apply(ImmutableList.of(), integers);
            }
        }
    }

    public enum Motion
    {
        TO,
        FROM,
        CONSTANT,
        ;
    }
}
