package pellucid.ava.client.renderers.models.c4;

import com.google.common.collect.ImmutableList;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.player.status.C4StatusManager;

import java.util.List;

public enum C4ModelVariables implements IModelVariables
{
    IDLE(C4StatusManager.C4Status.IDLE),
    RUN(C4StatusManager.C4Status.RUN),
    DRAW(C4StatusManager.C4Status.DRAW),
    SET(C4StatusManager.C4Status.SET),
    ;
    private static final List<C4ModelVariables> ANIMATABLE_VARIALES = ImmutableList.of(
            IDLE, RUN, DRAW, SET
    );

    private final C4StatusManager.C4Status action;

    C4ModelVariables(C4StatusManager.C4Status action)
    {
        this.action = action;
    }

    @Override
    public boolean actionExistInactive()
    {
        return action != null && !C4StatusManager.INSTANCE.isActive(action);
    }

    public static List<C4ModelVariables> getAnimatableVariables()
    {
        return ANIMATABLE_VARIALES;
    }

    @Override
    public String getName()
    {
        return name();
    }
}
