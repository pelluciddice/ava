package pellucid.ava.client.renderers.models.binocular;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.items.miscs.BinocularItem;

public class RegularBinoModelProperty extends RegularModelProperty<BinocularItem, BinoModelVariables, BinoSubModels, RegularBinoModelProperty>
{
    public Perspective aimingPos = Perspective.EMPTY;

    public RegularBinoModelProperty(BinocularItem binocular)
    {
        super(AVAModelTypes.BINOCULAR, binocular);
    }

    @Override
    public RegularBinoModelProperty construct(BinocularItem item)
    {
        return new RegularBinoModelProperty(item);
    }

    public RegularBinoModelProperty copyFor(BinocularItem binocular)
    {
        return super.copyFor(binocular)
                .aimingPos(aimingPos.copy());
    }

    public RegularBinoModelProperty aimingPos(Perspective pos)
    {
        aimingPos = pos;
        return this;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        super.writeToJson(json);
        json.add("aimingPos", aimingPos.writeToJsonR());
    }

    @Override
    public void readFromJson(JsonElement json2)
    {
        super.readFromJson(json2);
        JsonObject json = json2.getAsJsonObject();
        aimingPos.readFromJson(json.get("aimingPos"));
    }
}
