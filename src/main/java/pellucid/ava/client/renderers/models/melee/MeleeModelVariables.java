package pellucid.ava.client.renderers.models.melee;

import com.google.common.collect.ImmutableList;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.player.status.MeleeStatusManager;

import java.util.List;

public enum MeleeModelVariables implements IModelVariables
{
    IDLE(MeleeStatusManager.MeleeStatus.IDLE),
    ATTACK_LIGHT(MeleeStatusManager.MeleeStatus.ATTACK_LIGHT),
    ATTACK_HEAVY(MeleeStatusManager.MeleeStatus.ATTACK_HEAVY),
    DRAW(MeleeStatusManager.MeleeStatus.DRAW),
    RUN(MeleeStatusManager.MeleeStatus.RUN),
    ;
    private static final List<MeleeModelVariables> ANIMATABLE_VARIALES = ImmutableList.of(
            IDLE, ATTACK_LIGHT, ATTACK_HEAVY, DRAW, RUN
    );

    private final MeleeStatusManager.MeleeStatus action;

    MeleeModelVariables(MeleeStatusManager.MeleeStatus action)
    {
        this.action = action;
    }

    @Override
    public boolean actionExistInactive()
    {
        return action != null && !MeleeStatusManager.INSTANCE.isActive(action);
    }

    public static List<MeleeModelVariables> getAnimatableVariables()
    {
        return ANIMATABLE_VARIALES;
    }

    @Override
    public String getName()
    {
        return name();
    }
}
