package pellucid.ava.client.renderers.models.melee;

import net.minecraft.client.resources.model.ModelResourceLocation;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.items.miscs.AVAMeleeItem;

import java.util.Locale;

public enum MeleeSubModels implements ISubModels<AVAMeleeItem>
{
    ;
    MeleeSubModels()
    {
    }

    @Override
    public ModelResourceLocation generateFor(AVAMeleeItem knife)
    {
        return null;
    }

    @Override
    public boolean addedToModelByDefault()
    {
        return false;
    }

    @Override
    public String getRLName()
    {
        return name().toLowerCase(Locale.ROOT);
    }
}
