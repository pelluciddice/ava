package pellucid.ava.client.renderers.models.c4;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.joml.Vector3f;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.animation.AVABobAnimator;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.player.status.C4StatusManager;

import javax.annotation.Nullable;

import static pellucid.ava.player.status.BinocularStatusManager.BinocularStatus.ADS_IN;
import static pellucid.ava.player.status.BinocularStatusManager.BinocularStatus.ADS_OUT;
import static pellucid.ava.player.status.C4StatusManager.C4Status.DRAW;
import static pellucid.ava.player.status.C4StatusManager.C4Status.SET;

public class RegularC4Model extends AVABakedItemModel<C4Item, C4ModelVariables, C4SubModels, RegularC4ModelProperty, C4StatusManager>
{
    public RegularC4Model(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(AVAModelTypes.C4, origin, stack, world, entity);

        variables.put(C4ModelVariables.RUN, run ? 1 : 0);
        if (entity == Minecraft.getInstance().player)
        {
            variables.put(C4ModelVariables.DRAW, C4StatusManager.INSTANCE.getProgressFor(stack, DRAW));
            variables.put(C4ModelVariables.SET, C4StatusManager.INSTANCE.getProgressFor(stack, SET));
        }
        else
        {
            variables.put(C4ModelVariables.DRAW, stack.get(AVADataComponents.TAG_ITEM_DRAW));
            variables.put(C4ModelVariables.SET, 0);
        }
    }

    @Override
    public BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        if (type.firstPerson())
        {
            if (entity instanceof Player && entity == Minecraft.getInstance().player)
            {
                Player player = (Player) entity;
                if (this.run && !manager.isActive(SET, DRAW))
                    copy(runTransistor.getRunBob(getRunPos(), getBobScale(player, false), false), rotation, translation, scale);
                else
                {
                    int p = manager.getProgress();

                    Animations animation = null;
                    Perspective perspective = null;

                    if (manager.isActive(DRAW))
                        animation = getDrawAnimation();
                    else if (manager.isActive(SET))
                        animation = getSetAnimation();

                    if (animation != null && !animation.isEmpty())
                        copy(AnimationFactory.getPerspectiveInBetween(animation, p), rotation, translation, scale);
                    else if (perspective != null)
                        copy(perspective, rotation, translation, scale);

                    if (!(manager.isActive(DRAW, SET, BinocularStatusManager.BinocularStatus.DRAW)) && !ItemAnimatingGUI.isAnimating())
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, false), false), rotation, translation, scale);
                    else
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, false), true), rotation, translation, scale);
                }
            }
        }
        return push(rotation, translation, scale, mat);
    }

    protected Perspective getRunPos()
    {
        return properties.run.getA();
    }

    protected Animations getDrawAnimation()
    {
        return properties.draw.getA();
    }

    protected Animations getSetAnimation()
    {
        return properties.set.getA();
    }
}
