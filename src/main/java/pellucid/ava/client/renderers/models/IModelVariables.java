package pellucid.ava.client.renderers.models;

public interface IModelVariables
{
    boolean actionExistInactive();

    String getName();
}
