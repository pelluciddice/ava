package pellucid.ava.client.renderers.models.melee;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.joml.Vector3f;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.animation.AVABobAnimator;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.player.status.MeleeStatusManager;

import javax.annotation.Nullable;

import static pellucid.ava.player.status.MeleeStatusManager.MeleeStatus.*;

public class RegularMeleeModel extends AVABakedItemModel<AVAMeleeItem, MeleeModelVariables, MeleeSubModels, RegularMeleeModelProperty, MeleeStatusManager>
{
    public RegularMeleeModel(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(AVAModelTypes.MELEES, origin, stack, world, entity);

        variables.put(MeleeModelVariables.RUN, run ? 1 : 0);
        if (entity == Minecraft.getInstance().player)
        {
            variables.put(MeleeModelVariables.DRAW, MeleeStatusManager.INSTANCE.getProgressFor(stack, DRAW));
            variables.put(MeleeModelVariables.ATTACK_LIGHT, MeleeStatusManager.INSTANCE.getProgressFor(stack, ATTACK_LIGHT));
            variables.put(MeleeModelVariables.ATTACK_HEAVY, MeleeStatusManager.INSTANCE.getProgressFor(stack, ATTACK_HEAVY));
        }
        else
        {
            variables.put(MeleeModelVariables.DRAW, stack.get(AVADataComponents.TAG_ITEM_DRAW));
            variables.put(MeleeModelVariables.ATTACK_LIGHT, stack.get(AVADataComponents.TAG_ITEM_ATTACK_LIGHT));
            variables.put(MeleeModelVariables.ATTACK_HEAVY, stack.get(AVADataComponents.TAG_ITEM_ATTACK_HEAVY));
        }
    }

    @Override
    public BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        if (type.firstPerson())
        {
            if (entity instanceof Player && entity == Minecraft.getInstance().player)
            {
                Player player = (Player) entity;
                if (this.run && !manager.isActive(ATTACK_LIGHT, ATTACK_HEAVY, DRAW))
                    copy(runTransistor.getRunBob(getRunPos(), getBobScale(player, false), false), rotation, translation, scale);
                else
                {
                    int p = manager.getProgress();

                    Animations animation = null;
                    Perspective perspective = null;

                    if (manager.isActive(DRAW))
                        animation = getDrawAnimation();
                    else if (manager.isActive(ATTACK_LIGHT))
                        animation = getAttackLightAnimation();
                    else if (manager.isActive(ATTACK_HEAVY))
                        animation = getAttackHeavyAnimation();

                    if (animation != null && !animation.isEmpty())
                        copy(AnimationFactory.getPerspectiveInBetween(animation, p), rotation, translation, scale);
                    else if (perspective != null)
                        copy(perspective, rotation, translation, scale);

                    if (!manager.isActive(ATTACK_LIGHT, ATTACK_HEAVY, DRAW) && !ItemAnimatingGUI.isAnimating())
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, false), false), rotation, translation, scale);
                    else
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, false), true), rotation, translation, scale);
                }
            }
        }
        return push(rotation, translation, scale, mat);
    }

    protected Perspective getRunPos()
    {
        return properties.run.getA();
    }

    protected Animations getDrawAnimation()
    {
        return properties.draw.getA();
    }

    protected Animations getAttackLightAnimation()
    {
        return properties.attackLight.getA();
    }

    protected Animations getAttackHeavyAnimation()
    {
        return properties.attackHeavy.getA();
    }
}
