package pellucid.ava.client.renderers.models;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.world.item.Item;

public interface ISubModels<I extends Item>
{
    ModelResourceLocation generateFor(I item);

    boolean addedToModelByDefault();

    String getRLName();
}
