package pellucid.ava.client.renderers.models.projectile;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.registries.BuiltInRegistries;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.throwables.ThrowableItem;

import java.util.Locale;
import java.util.function.Function;

public enum ProjectileSubModels implements ISubModels<ThrowableItem>
{
    PIN(),
    ;
    private final Function<ThrowableItem, ModelResourceLocation> model;

    ProjectileSubModels()
    {
        this.model = (proj) -> {
            String name = BuiltInRegistries.ITEM.getKey(proj).getPath();
            return new ModelResourceLocation(AVA.MODID, name + "/" + name + "_" + name().toLowerCase(Locale.ROOT), "inventory");
        };
    }

    @Override
    public ModelResourceLocation generateFor(ThrowableItem proj)
    {
        return model.apply(proj);
    }

    @Override
    public boolean addedToModelByDefault()
    {
        return true;
    }

    @Override
    public String getRLName()
    {
        return name().toLowerCase(Locale.ROOT);
    }
}
