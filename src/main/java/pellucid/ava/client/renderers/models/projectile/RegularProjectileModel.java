package pellucid.ava.client.renderers.models.projectile;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.joml.Vector3f;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.player.status.ProjectileStatusManager;

import javax.annotation.Nullable;

import static pellucid.ava.player.status.ProjectileStatusManager.ProjectileStatus.*;

public class RegularProjectileModel extends AVABakedItemModel<ThrowableItem, ProjectileModelVariables, ProjectileSubModels, RegularProjectileModelProperty, ProjectileStatusManager>
{
    public RegularProjectileModel(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(AVAModelTypes.PROJECTILE, origin, stack, world, entity);

        variables.put(ProjectileModelVariables.RUN, run ? 1 : 0);
        if (entity == Minecraft.getInstance().player)
        {
            variables.put(ProjectileModelVariables.DRAW, ProjectileStatusManager.INSTANCE.getProgressFor(stack, DRAW));
            variables.put(ProjectileModelVariables.PIN_OFF, ProjectileStatusManager.INSTANCE.getProgressFor(stack, PIN_OFF));
            variables.put(ProjectileModelVariables.THROW, ProjectileStatusManager.INSTANCE.getProgressFor(stack, THROW));
            variables.put(ProjectileModelVariables.TOSS, ProjectileStatusManager.INSTANCE.getProgressFor(stack, TOSS));
        }
        else
        {
            variables.put(ProjectileModelVariables.DRAW, stack.get(AVADataComponents.TAG_ITEM_DRAW));
            variables.put(ProjectileModelVariables.PIN_OFF, 0);
            variables.put(ProjectileModelVariables.THROW, 0);
            variables.put(ProjectileModelVariables.TOSS, 0);
        }
    }

    @Override
    public BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        if (type.firstPerson())
        {
            if (entity instanceof Player && entity == Minecraft.getInstance().player)
            {
                Player player = (Player) entity;

                if (this.run && !manager.isActive(DRAW, PIN_OFF, THROW, TOSS))
                    copy(runTransistor.getRunBob(getRunPos(), getBobScale(player, false), false), rotation, translation, scale);
                else
                {
                    int p = manager.getProgress();

                    Animations animation = null;
                    Perspective perspective = null;

                    if (manager.isActive(DRAW))
                        animation = getDrawAnimation();
                    else if (manager.isActive(PIN_OFF))
                        animation = getPinOffAnimation();
                    else if (manager.isActive(THROW))
                        animation = getThrowAnimation();
                    else if (manager.isActive(TOSS))
                        animation = getTossAnimation();

                    if (animation != null && !animation.isEmpty())
                        copy(AnimationFactory.getPerspectiveInBetween(animation, p), rotation, translation, scale);
                    else if (perspective != null)
                        copy(perspective, rotation, translation, scale);

                    if (!manager.isActive(DRAW, PIN_OFF, THROW, TOSS) && !ItemAnimatingGUI.isAnimating())
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, false), false), rotation, translation, scale);
                    else
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, false), true), rotation, translation, scale);
                }
            }
        }
        return push(rotation, translation, scale, mat);
    }

    protected Animations getPinOffAnimation()
    {
        return properties.pinOff.getA();
    }

    protected Animations getThrowAnimation()
    {
        return properties.thr0w.getA();
    }

    protected Animations getTossAnimation()
    {
        return properties.toss.getA();
    }

    protected Perspective getRunPos()
    {
        return properties.run.getA();
    }

    protected Animations getDrawAnimation()
    {
        return properties.draw.getA();
    }
}
