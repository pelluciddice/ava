package pellucid.ava.client.renderers.models.projectile;

import com.google.common.collect.ImmutableList;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.player.status.ProjectileStatusManager;

import java.util.List;

public enum ProjectileModelVariables implements IModelVariables
{
    IDLE(ProjectileStatusManager.ProjectileStatus.IDLE),
    RUN(ProjectileStatusManager.ProjectileStatus.RUN),
    DRAW(ProjectileStatusManager.ProjectileStatus.DRAW),
    PIN_OFF(ProjectileStatusManager.ProjectileStatus.PIN_OFF),
    THROW(ProjectileStatusManager.ProjectileStatus.THROW),
    TOSS(ProjectileStatusManager.ProjectileStatus.TOSS),
    ;
    private static final List<ProjectileModelVariables> ANIMATABLE_VARIABLES = ImmutableList.of(
            IDLE, RUN, DRAW, PIN_OFF, THROW, TOSS
    );

    private final ProjectileStatusManager.ProjectileStatus action;

    ProjectileModelVariables(ProjectileStatusManager.ProjectileStatus action)
    {
        this.action = action;
    }

    @Override
    public boolean actionExistInactive()
    {
        return action != null && !ProjectileStatusManager.INSTANCE.isActive(action);
    }

    public static List<ProjectileModelVariables> getAnimatableVariables()
    {
        return ANIMATABLE_VARIABLES;
    }

    @Override
    public String getName()
    {
        return name();
    }
}