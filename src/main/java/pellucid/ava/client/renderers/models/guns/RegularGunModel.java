package pellucid.ava.client.renderers.models.guns;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;
import org.joml.Vector3f;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.ModelRenderer;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.minecraft.world.item.ItemDisplayContext.GROUND;
import static net.minecraft.world.item.ItemDisplayContext.GUI;
import static pellucid.ava.player.status.GunStatusManager.GunStatus.*;

public class RegularGunModel extends AVABakedItemModel<AVAItemGun, GunModelVariables, GunSubModels, RegularGunModelProperty, GunStatusManager>
{
    protected final boolean installedSilencer;

    public RegularGunModel(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(AVAModelTypes.GUNS, origin, stack, world, entity);
        this.installedSilencer = stack.getOrDefault(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, false);

        int ammo = stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0);
        variables.put(GunModelVariables.ALWAYS, 1);
        variables.put(GunModelVariables.RUN, run ? 1 : 0);
        variables.put(GunModelVariables.AMMO, ammo);
        variables.put(GunModelVariables.INSTALLED_SILENCER, installedSilencer ? 1 : 0);
        if (entity == Minecraft.getInstance().player)
        {
            variables.put(GunModelVariables.FIRE_TICKS, GunStatusManager.INSTANCE.getProgressFor(stack, FIRE));
            variables.put(GunModelVariables.RELOAD, GunStatusManager.INSTANCE.getProgressFor(stack, RELOAD));
            variables.put(GunModelVariables.PRE_RELOAD, GunStatusManager.INSTANCE.getProgressFor(stack, RELOAD_PRE));
            variables.put(GunModelVariables.POST_RELOAD, GunStatusManager.INSTANCE.getProgressFor(stack, RELOAD_POST));
            variables.put(GunModelVariables.DRAW, GunStatusManager.INSTANCE.getProgressFor(stack, DRAW));
            variables.put(GunModelVariables.AIM, GunStatusManager.INSTANCE.getProgressFor(stack, ADS_IN));
            variables.put(GunModelVariables.INSTALL_SILENCER, GunStatusManager.INSTANCE.getProgressFor(stack, INSTALL_SILENCER));
            variables.put(GunModelVariables.EMPTY_AND_NOT_RELOADING, ammo == 0 && !GunStatusManager.INSTANCE.isActive(RELOAD) ? 1 : 0);
        }
        else
        {
            variables.put(GunModelVariables.FIRE_TICKS, stack.getOrDefault(AVADataComponents.TAG_ITEM_FIRE, 0) != 0 ? 1 : 0);
            variables.put(GunModelVariables.RELOAD, stack.getOrDefault(AVADataComponents.TAG_ITEM_RELOAD, 0));
            variables.put(GunModelVariables.PRE_RELOAD, stack.getOrDefault(AVADataComponents.TAG_ITEM_PRE_RELOAD, 0));
            variables.put(GunModelVariables.POST_RELOAD, stack.getOrDefault(AVADataComponents.TAG_ITEM_POST_RELOAD, 0));
            variables.put(GunModelVariables.DRAW, stack.getOrDefault(AVADataComponents.TAG_ITEM_DRAW, 0));
            variables.put(GunModelVariables.AIM, 0);
            variables.put(GunModelVariables.INSTALL_SILENCER, 0);
            variables.put(GunModelVariables.EMPTY_AND_NOT_RELOADING, ammo == 0 && stack.getOrDefault(AVADataComponents.TAG_ITEM_RELOAD, 0) == 0 ? 1 : 0);
        }
    }

    public static void refreshAll()
    {
        INSTALL_SILENCER_ANIMATIONS.clear();
        ADS_FIRE_ANIMATIONS.clear();
    }

    public Perspective getAimingPosition()
    {
        return properties.aimingPos;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, RandomSource rand)
    {
        List<BakedQuad> quads = super.getQuads(state, side, rand);
        if (side != null)
            return quads;
        if (!quads.isEmpty() && !AVAClientUtil.USE_FAST_ASSETS)
        {
            Pair<ModelResourceLocation, ModelResourceLocation> pair = getLensModel();
            if (!pair.isEmpty())
            {
                boolean aiming = PlayerAction.getCap(Minecraft.getInstance().player).isADS();
                ModelResourceLocation lens = null;
                if (aiming)
                    lens = pair.getB();
                else if (AVAClientConfig.ENABLE_LENS_TINT.get())
                    lens = pair.getA();
                if (lens != null)
                    quads.addAll(get(lens));
            }
        }
        return quads;
    }

    protected static final Map<Item, Animations> INSTALL_SILENCER_ANIMATIONS = new HashMap<>();
    protected static final Map<Item, Animations> ADS_FIRE_ANIMATIONS = new HashMap<>();

    @Override
    protected BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        if (type == GUI && AVAClientUtil.USE_TEXTURE_MODEL_GUI && AVAClientConfig.GUI_FAST_ASSETS.get())
        {
            BakedModel model = getOtherModel(ModelRenderer.TEXTURE_MODELS.get(stack.getItem()));
            if (model != null)
                return model;
            return push(rotation, translation, scale, mat);
        }
        if (type.firstPerson())
        {
            if (entity instanceof Player && entity == Minecraft.getInstance().player)
            {
                Player player = (Player) entity;
                AVAItemGun.IScopeType scope = item.getScopeType(stack, true);
                boolean ads = PlayerAction.getCap(player).isADS() && !scope.isIronSight() && !scope.noAim(stack);
                if (this.run && !manager.isActive(ADS_IN, ADS_OUT, DRAW, FIRE, RELOAD, RELOAD_PRE, RELOAD_POST, INSTALL_SILENCER, UNINSTALL_SILENCER) && !ads)
                    copy(runTransistor.getRunBob(getRunPos(), getBobScale(player, false), false), rotation, translation, scale);
                else
                {
                    int p = manager.getProgress();

                    Animations animation = null;
                    Perspective perspective = null;

                    if (manager.isActive(DRAW))
                        animation = getDrawAnimation();
                    else if (manager.isActive(RELOAD))
                        animation = getReloadAnimation();
                    else if (manager.isActive(RELOAD_PRE))
                        animation = getPreReloadAnimation();
                    else if (manager.isActive(RELOAD_POST))
                        animation = getPostReloadAnimation();
                    else if (manager.isActive(INSTALL_SILENCER, UNINSTALL_SILENCER) && getInstallSilencerPosition() != null)
                        animation = INSTALL_SILENCER_ANIMATIONS.computeIfAbsent(item, (item) -> Animations.of()
                                .append(Animation.of(0, getOriginalFpRight()))
                                .append(Animation.of(6, getInstallSilencerPosition()))
                                .append(Animation.of(14, getInstallSilencerPosition()))
                                .append(Animation.of(20, getOriginalFpRight())));
                    else if (manager.isActive(ADS_IN))
                        animation = AIM_ANIMATIONS.computeIfAbsent(item, (item) -> Animations.of()
                                .append(Animation.of(0, getOriginalFpRight()))
                                .append(Animation.of(this.item.getAimTime(stack, true) + 1, getAimingPosition()))
                        );
                    else if (manager.isActive(ADS_OUT))
                        animation = AIM_BACKWARD_ANIMATIONS.computeIfAbsent(item, (item) -> Animations.of()
                                .append(Animation.of(0, getAimingPosition()))
                                .append(Animation.of(this.item.getAimTime(stack, true) + 1, getOriginalFpRight()))
                        );
                    else
                    {
                        boolean fire = manager.isActive(FIRE);
                        if (ads)
                        {
                            if (fire)
                                animation = ADS_FIRE_ANIMATIONS.computeIfAbsent(item, (gun) -> Animations.of()
                                        .append(Animation.of(0, getAimingPosition().add(Perspective.translation(0, 0, 0.05F).withScale(0, 0, 0))))
                                        .append(Animation.of(item.getFireAnimation(stack), getAimingPosition()))
                                );
                            else
                                perspective = getAimingPosition();
                        }
                        else if (fire)
                            animation = getFireAnimation();
                    }
                    if (animation != null && !animation.isEmpty())
                        copy(AnimationFactory.getPerspectiveInBetween(animation, p), rotation, translation, scale);
                    else if (perspective != null)
                        copy(perspective, rotation, translation, scale);

                    if (!manager.isActive(ADS_IN, ADS_OUT, DRAW, FIRE, RELOAD, RELOAD_PRE, RELOAD_POST, INSTALL_SILENCER, UNINSTALL_SILENCER) && !ItemAnimatingGUI.isAnimating() && !ads)
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, ads), false), rotation, translation, scale);
                    else
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, ads), true), rotation, translation, scale);
                }
            }
        }
        return push(rotation, translation, scale, mat);
    }

    @Override
    public BakedModel applyTransform(ItemDisplayContext type, PoseStack poseStack, boolean applyLeftHandTransform)
    {
        Perspective perspective = properties.display.get(type).copy();
        return modifyPerspectiveInner(perspective.rotation, perspective.translation, type == GROUND ? new Vector3f(perspective.scale).mul(1.2F, 1.0F, 1.2F) : perspective.scale, type, poseStack);
    }

    public Perspective getInstallSilencerPosition()
    {
        return properties.silencerPos.isEmpty() ? null : properties.silencerPos;
    }

    protected Perspective getRunPos()
    {
        return properties.run.getA();
    }

    protected Animations getReloadAnimation()
    {
        return properties.reload.getA();
    }

    protected Animations getPreReloadAnimation()
    {
        return properties.preReload.getA();
    }

    protected Animations getPostReloadAnimation()
    {
        return properties.postReload.getA();
    }

    protected Animations getFireAnimation()
    {
        return properties.fire.getA();
    }

    protected Animations getDrawAnimation()
    {
        return properties.draw.getA();
    }

    protected Pair<ModelResourceLocation, ModelResourceLocation> getLensModel()
    {
        return Pair.of(properties.subModels.getOrDefault(GunSubModels.LENS, null), properties.subModels.getOrDefault(GunSubModels.LENS_ADS, null));
    }
}
