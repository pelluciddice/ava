package pellucid.ava.client.renderers.models.guns;

import com.google.common.collect.ImmutableList;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.player.status.GunStatusManager;

import java.util.List;

public enum GunModelVariables implements IModelVariables
{
    IDLE(GunStatusManager.GunStatus.IDLE),
    RUN(GunStatusManager.GunStatus.RUN),
    PRE_RELOAD(GunStatusManager.GunStatus.RELOAD_PRE),
    RELOAD(GunStatusManager.GunStatus.RELOAD),
    POST_RELOAD(GunStatusManager.GunStatus.RELOAD_POST),
    DRAW(GunStatusManager.GunStatus.DRAW),
    FIRE_TICKS(GunStatusManager.GunStatus.FIRE),
    INSTALL_SILENCER(GunStatusManager.GunStatus.INSTALL_SILENCER),
    AIM(GunStatusManager.GunStatus.ADS_IN),
    INSTALLED_SILENCER(null),
    AMMO(null),
    EMPTY_AND_NOT_RELOADING(null),
    ALWAYS(null),
    ;
    private static final List<GunModelVariables> ANIMATABLE_VARIALES = ImmutableList.of(
            IDLE, RUN, PRE_RELOAD, RELOAD, POST_RELOAD, DRAW, FIRE_TICKS, INSTALL_SILENCER, AIM
    );

    private final GunStatusManager.GunStatus action;

    GunModelVariables(GunStatusManager.GunStatus action)
    {
        this.action = action;
    }

    @Override
    public boolean actionExistInactive()
    {
        return action != null && !GunStatusManager.INSTANCE.isActive(action);
    }

    public static List<GunModelVariables> getAnimatableVariables()
    {
        return ANIMATABLE_VARIALES;
    }


    @Override
    public String getName()
    {
        return name();
    }
}
