package pellucid.ava.client.renderers.models.c4;

import net.minecraft.client.resources.model.ModelResourceLocation;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.items.miscs.C4Item;

import java.util.Locale;

public enum C4SubModels implements ISubModels<C4Item>
{
    SCREEN_UNSET,
    SCREEN_SET_TIMER,
    SCREEN_SET_15,
    SCREEN_SET_30,
    SCREEN_SET_40,
    SCREEN_SET_OK,
    LEVER,
    ;
    C4SubModels()
    {
    }

    @Override
    public ModelResourceLocation generateFor(C4Item item)
    {
        return new ModelResourceLocation(AVA.MODID, "c4/c4_" + name().toLowerCase(Locale.ROOT),"inventory");
    }

    @Override
    public boolean addedToModelByDefault()
    {
        return true;
    }

    @Override
    public String getRLName()
    {
        return name().toLowerCase(Locale.ROOT);
    }
}
