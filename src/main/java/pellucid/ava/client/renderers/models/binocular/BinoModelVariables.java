package pellucid.ava.client.renderers.models.binocular;

import com.google.common.collect.ImmutableList;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.player.status.BinocularStatusManager;

import java.util.List;

public enum BinoModelVariables implements IModelVariables
{
    IDLE(BinocularStatusManager.BinocularStatus.IDLE),
    RUN(BinocularStatusManager.BinocularStatus.RUN),
    DRAW(BinocularStatusManager.BinocularStatus.DRAW),
    AIM(BinocularStatusManager.BinocularStatus.ADS_IN),
    ;
    private static final List<BinoModelVariables> ANIMATABLE_VARIALES = ImmutableList.of(
            IDLE, RUN, DRAW, AIM
    );

    private final BinocularStatusManager.BinocularStatus action;

    BinoModelVariables(BinocularStatusManager.BinocularStatus action)
    {
        this.action = action;
    }

    @Override
    public boolean actionExistInactive()
    {
        return action != null && !BinocularStatusManager.INSTANCE.isActive(action);
    }

    public static List<BinoModelVariables> getAnimatableVariables()
    {
        return ANIMATABLE_VARIALES;
    }

    @Override
    public String getName()
    {
        return name();
    }
}
