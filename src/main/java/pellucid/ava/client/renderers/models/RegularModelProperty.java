package pellucid.ava.client.renderers.models;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import org.joml.Vector3f;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.IJsonSerializable;
import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public abstract class RegularModelProperty<I extends Item, V extends IModelVariables, S extends ISubModels<I>, P extends RegularModelProperty<I, V, S, P>> implements IJsonSerializable<P>
{
    private final AVAModelTypes<I, V, S, P, ?, ?> type;
    private final I item;
    public Pair<Perspective, Perspective> handsIdle = Pair.of(new Perspective(), new Perspective());
    public Map<S, ModelResourceLocation> subModels = new HashMap<>();
    public Pair<Perspective, Pair<Perspective, Perspective>> run = Pair.of(Perspective.empty(), Pair.of(Perspective.empty(), Perspective.empty()));
    public Pair<Animations, Pair<Animations, Animations>> draw = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Map<ItemDisplayContext, Perspective> display = new HashMap<>() {{
        for (ItemDisplayContext value : ItemDisplayContext.values())
            put(value, new Perspective());
    }};
    public Map<S, List<QuadAnimator.Animator>> quadAnimators = new HashMap<>();
    public Map<S, Map<V, Animations>> quadAnimators2 = new HashMap<>();
    public Map<S, Vector3f> quadOrigins = new HashMap<>();

    public RegularModelProperty(AVAModelTypes<I, V, S, P, ?, ?> type, I item)
    {
        this.type = type;
        this.item = item;
    }
    
    public abstract P construct(I item);

    public P copyFor(I item)
    {
        P properties = construct(item)
                .leftHandsIdle(handsIdle.getA().copy())
                .rightHandsIdle(handsIdle.getB().copy())
                .run(run.getA().copy())
                .runLeft(run.getB().getA().copy())
                .runRight(run.getB().getB().copy())
                .draw(Animations.of(draw.getA()))
                .drawLeft(Animations.of(draw.getB().getA()))
                .drawRight(Animations.of(draw.getB().getB()));
        properties.subModels.putAll(subModels);
        display.forEach((k, v) -> properties.display(k, v.copy()));
        quadAnimators.forEach((k, v) -> v.forEach((anim) -> properties.quadAnim(k, anim.copy())));
        quadAnimators2.forEach((k, v) -> v.forEach((k2, v2) -> properties.quadAnim(k, k2, Animations.of(v2))));
        return properties;
    }

    public P leftHandsIdle(Perspective left)
    {
        handsIdle.setA(left);
        return (P) this;
    }

    public P rightHandsIdle(Perspective right)
    {
        handsIdle.setB(right);
        return (P) this;
    }

    public P subModels(S... model)
    {
        for (S m : model)
            putSubModel(m);
        return (P) this;
    }

    public P run(Perspective perspective)
    {
        run.setA(perspective);
        return (P) this;
    }

    public P runLeft(Perspective perspective)
    {
        run.getB().setA(perspective);
        return (P) this;
    }

    public P runRight(Perspective perspective)
    {
        run.getB().setB(perspective);
        return (P) this;
    }

    public P draw(Animations animations)
    {
        draw.setA(animations);
        return (P) this;
    }

    public P drawLeft(Animations animations)
    {
        draw.getB().setA(animations);
        return (P) this;
    }

    public P drawRight(Animations animations)
    {
        draw.getB().setB(animations);
        return (P) this;
    }

    public P display(ItemDisplayContext context, Perspective perspective)
    {
        display.put(context, perspective);
        return (P) this;
    }

    private static final Map<ItemDisplayContext, ItemDisplayContext> DISPLAY_MIRROR = ImmutableMap.of(
            ItemDisplayContext.THIRD_PERSON_LEFT_HAND, ItemDisplayContext.THIRD_PERSON_RIGHT_HAND,
            ItemDisplayContext.THIRD_PERSON_RIGHT_HAND, ItemDisplayContext.THIRD_PERSON_LEFT_HAND,
            ItemDisplayContext.FIRST_PERSON_LEFT_HAND, ItemDisplayContext.FIRST_PERSON_RIGHT_HAND,
            ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, ItemDisplayContext.FIRST_PERSON_LEFT_HAND
    );

    public P display2(ItemDisplayContext context, Perspective perspective)
    {
        display.put(context, perspective);
        if (DISPLAY_MIRROR.containsKey(context))
            display.put(DISPLAY_MIRROR.get(context), perspective);
        return (P) this;
    }

    public P putSubModel(S model)
    {
        subModels.put(model, model.generateFor(item));
        return (P) this;
    }

    public P nullSubModel(S model)
    {
        subModels.put(model, null);
        return (P) this;
    }

    public P quadAnimT(S model, V varaible, int toStart, int toEnd, int fromStart, int fromEnd, Map<Direction, Float> translations)
    {
        List<QuadAnimator.Animator> list = quadAnimators.computeIfAbsent(model, (k) -> new ArrayList<>());
        translations.forEach((d, a) -> {
            if (toStart != toEnd)
                list.add(new QuadAnimator.Animator.Translator(QuadAnimator.Motion.TO, varaible, a, toStart, toEnd, d));
            if (toEnd != fromStart)
                list.add(new QuadAnimator.Animator.Translator(QuadAnimator.Motion.CONSTANT, varaible, a, toEnd, fromStart, d));
            if (fromStart != fromEnd)
                list.add(new QuadAnimator.Animator.Translator(QuadAnimator.Motion.FROM, varaible, a, fromStart, fromEnd, d));
        });
        return (P) this;
    }

    public P quadAnimR(S model, V varaible, int toStart, int toEnd, int fromStart, int fromEnd, float amount, Direction.Axis axis, Vector3f pivot)
    {
        List<QuadAnimator.Animator> list = quadAnimators.computeIfAbsent(model, (k) -> new ArrayList<>());
        if (toStart != toEnd)
            list.add(new QuadAnimator.Animator.Rotator(QuadAnimator.Motion.TO, varaible, amount, toStart, toEnd, axis, pivot));
        if (toEnd != fromStart)
            list.add(new QuadAnimator.Animator.Rotator(QuadAnimator.Motion.CONSTANT, varaible, amount, toEnd, fromStart, axis, pivot));
        if (fromStart != fromEnd)
            list.add(new QuadAnimator.Animator.Rotator(QuadAnimator.Motion.FROM, varaible, amount, fromStart, fromEnd, axis, pivot));
        return (P) this;
    }

    public P quadAnim(S model, QuadAnimator.Animator animator)
    {
        quadAnimators.computeIfAbsent(model, (k) -> new ArrayList<>()).add(animator);
        return (P) this;
    }

    public P quadAnim(S model, V variable, Animations animation)
    {
        quadAnimators2.computeIfAbsent(model, (k) -> new HashMap<>()).put(variable, animation);
        return (P) this;
    }

    public P quadOrigin(S model, Vector3f animator)
    {
        quadOrigins.put(model, animator);
        return (P) this;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        JsonObject handIdle = new JsonObject();
        handsIdle.getA().saveTo(handIdle, "left");
        handsIdle.getB().saveTo(handIdle, "right");
        json.add("handsIdle", handIdle);

        JsonArray array = new JsonArray();
        for (S model : subModels.keySet().stream().sorted().toList())
        {
            JsonObject obj = new JsonObject();
            obj.addProperty("model", model.getRLName().toLowerCase(Locale.ROOT));
            obj.addProperty("loc", subModels.get(model) == null ? AVAConstants.NULL : subModels.get(model).toString());
            array.add(obj);
        }
        json.add("subModels", array);

        JsonObject runObj = new JsonObject();
        run.getA().saveTo(runObj, "item");
        run.getB().getA().saveTo(runObj, "left");
        run.getB().getB().saveTo(runObj, "right");
        json.add("run", runObj);

        writeAnimations(draw, json, "draw");

        JsonArray array2 = new JsonArray();
        quadAnimators.keySet().stream().sorted().forEach((key) -> {
            JsonObject obj = new JsonObject();
            obj.addProperty("model", key.getRLName().toLowerCase(Locale.ROOT));
            JsonArray array3 = new JsonArray();
            for (QuadAnimator.Animator animator : quadAnimators.get(key))
                array3.add(animator.writeToJsonR());
            obj.add("transforms", array3);
            array2.add(obj);
        });
        json.add("animators", array2);

        JsonObject quadAnimations = new JsonObject();
        JsonArray array4 = new JsonArray();
        quadAnimators2.keySet().stream().sorted().forEach((key) -> {
            JsonObject obj = new JsonObject();
            obj.addProperty("model", key.getRLName().toLowerCase(Locale.ROOT));
            obj.add("origin", IJsonSerializable.vector3fW(quadOrigins.getOrDefault(key, new Vector3f())));
            JsonArray array5 = new JsonArray();
            quadAnimators2.get(key).keySet().stream().sorted().forEach((key2) -> {
                JsonObject obj2 = new JsonObject();
                obj2.addProperty("variable", key2.getName().toLowerCase(Locale.ROOT));
                obj2.add("animation", quadAnimators2.get(key).get(key2).writeToJsonR());
                array5.add(obj2);
            });
            obj.add("animations", array5);
            array4.add(obj);
        });
        quadAnimations.add("model_anim", array4);
        json.add("animators2", quadAnimations);

        JsonArray array3 = new JsonArray();
        display.keySet().stream().sorted().forEach((key) -> {
            JsonObject obj = new JsonObject();
            obj.addProperty("type", key.name().toLowerCase(Locale.ROOT));
            display.get(key).writeToJson(obj);
            array3.add(obj);
        });
        json.add("displays", array3);
    }

    @Override
    public void readFromJson(JsonElement json2)
    {
        JsonObject json = json2.getAsJsonObject();

        JsonObject handsIdle = json.getAsJsonObject("handsIdle");
        this.handsIdle.getA().readFromJson(handsIdle.get("left"));
        this.handsIdle.getB().readFromJson(handsIdle.get("right"));

        this.subModels.clear();
        for (JsonElement element : json.get("subModels").getAsJsonArray())
        {
            JsonObject obj = element.getAsJsonObject();
            subModels.put(type.getSubModel(obj.get("model").getAsString().toUpperCase(Locale.ROOT)), AVAClientUtil.modelRLFromString(obj.get("loc").getAsString()));
        }

        JsonObject runObj = json.getAsJsonObject("run");
        run.setA(IJsonSerializable.create(new Perspective(), runObj.get("item")));
        run.getB().setA(IJsonSerializable.create(new Perspective(), runObj.get("left")));
        run.getB().setB(IJsonSerializable.create(new Perspective(), runObj.get("right")));

        readAnimations(draw, json, "draw");

        quadAnimators.clear();
        for (JsonElement element : json.getAsJsonArray("animators"))
        {
            JsonObject obj = element.getAsJsonObject();
            S model = type.getSubModel(obj.get("model").getAsString().toUpperCase(Locale.ROOT));
            for (JsonElement transform : obj.getAsJsonArray("transforms"))
                quadAnim(model, QuadAnimator.fromJson(transform.getAsJsonObject()));
        }

        JsonObject quadAnimations = json.getAsJsonObject("animators2");
        quadAnimators2.clear();
        for (JsonElement element : quadAnimations.getAsJsonArray("model_anim"))
        {
            JsonObject obj = element.getAsJsonObject();
            S model = type.getSubModel(obj.get("model").getAsString().toUpperCase(Locale.ROOT));
            quadOrigin(model, IJsonSerializable.vector3fR(obj.getAsJsonArray("origin")));
            obj.getAsJsonArray("animations").forEach((element2) -> {
                JsonObject obj2 = element2.getAsJsonObject();
                V variable = (V) type.animatableVariables.stream().filter((v) -> v.getName().equals(obj2.get("variable").getAsString().toUpperCase(Locale.ROOT))).findFirst().get();
                quadAnim(model, variable, Animations.of(obj2.getAsJsonObject("animation")));
            });
        }

        display.clear();
        for (JsonElement element : json.getAsJsonArray("displays"))
        {
            JsonObject obj = element.getAsJsonObject();
            ItemDisplayContext display = ItemDisplayContext.valueOf(obj.get("type").getAsString().toUpperCase(Locale.ROOT));
            display(display, IJsonSerializable.create(new Perspective(), obj));
        }
    }

    protected void readAnimations(Pair<Animations, Pair<Animations, Animations>> animations, JsonObject element, String name)
    {
        JsonObject json = element.get(name).getAsJsonObject();
        animations.getA().readFromJson(json.get("item"));
        animations.getB().getA().readFromJson(json.get("left"));
        animations.getB().getB().readFromJson(json.get("right"));
    }

    protected void writeAnimations(Pair<Animations, Pair<Animations, Animations>> animations, JsonObject element, String name)
    {
        JsonObject json = new JsonObject();
        animations.getA().saveTo(json, "item");
        animations.getB().getA().saveTo(json, "left");
        animations.getB().getB().saveTo(json, "right");
        element.add(name, json);
    }
}
