package pellucid.ava.client.renderers.models.binocular;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import org.joml.Vector3f;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.animation.AVABobAnimator;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.player.status.BinocularStatusManager;
import pellucid.ava.util.AVAConstants;

import javax.annotation.Nullable;

import static pellucid.ava.player.status.BinocularStatusManager.BinocularStatus.*;

public class RegularBinoModel extends AVABakedItemModel<BinocularItem, BinoModelVariables, BinoSubModels, RegularBinoModelProperty, BinocularStatusManager>
{
    public RegularBinoModel(BakedModel origin, ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity entity)
    {
        super(AVAModelTypes.BINOCULAR, origin, stack, world, entity);

        variables.put(BinoModelVariables.RUN, run ? 1 : 0);
        if (entity == Minecraft.getInstance().player)
        {
            variables.put(BinoModelVariables.DRAW, BinocularStatusManager.INSTANCE.getProgressFor(stack, DRAW));
            variables.put(BinoModelVariables.AIM, BinocularStatusManager.INSTANCE.getProgressFor(stack, ADS_IN));
        }
        else
        {
            variables.put(BinoModelVariables.DRAW, stack.get(AVADataComponents.TAG_ITEM_DRAW));
            variables.put(BinoModelVariables.AIM, 0);
        }
    }

    @Override
    public BakedModel modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemDisplayContext type, PoseStack mat)
    {
        if (type.firstPerson())
        {
            if (entity instanceof Player && entity == Minecraft.getInstance().player)
            {
                Player player = (Player) entity;
                if (this.run && !manager.isActive(ADS_IN, ADS_OUT, DRAW))
                    copy(runTransistor.getRunBob(getRunPos(), getBobScale(player, false), false), rotation, translation, scale);
                else
                {
                    int p = manager.getProgress();
                    boolean ads = PlayerAction.getCap(player).isADS();

                    Animations animation = null;
                    Perspective perspective = null;

                    if (manager.isActive(DRAW))
                        animation = getDrawAnimation();
                    else if (manager.isActive(ADS_IN))
                        animation = AIM_ANIMATIONS.computeIfAbsent(item, (item) -> Animations.of()
                                .append(Animation.of(0, getOriginalFpRight()))
                                .append(Animation.of(AVAConstants.BINOCULAR_AIM_TIME + 1, getAimingPosition()))
                        );
                    else if (manager.isActive(ADS_OUT))
                        animation = AIM_BACKWARD_ANIMATIONS.computeIfAbsent(item, (item) -> Animations.of()
                                .append(Animation.of(0, getAimingPosition()))
                                .append(Animation.of(AVAConstants.BINOCULAR_AIM_TIME + 1, getOriginalFpRight()))
                        );
                    else
                    {
                        if (ads)
                            perspective = getAimingPosition();
                    }

                    if (animation != null && !animation.isEmpty())
                        copy(AnimationFactory.getPerspectiveInBetween(animation, p), rotation, translation, scale);
                    else if (perspective != null)
                        copy(perspective, rotation, translation, scale);

                    if (!(manager.isActive(ADS_IN, ADS_OUT, DRAW)) && !ItemAnimatingGUI.isAnimating())
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, ads), false), rotation, translation, scale);
                    else
                        copy(runTransistor.getIdleBob(getOriginalFpRight(), new Perspective(rotation, translation, scale), getBobScale(player, ads), true), rotation, translation, scale);
                }
            }
        }
        return push(rotation, translation, scale, mat);
    }

    protected Perspective getAimingPosition()
    {
        return properties.aimingPos;
    }

    protected Perspective getRunPos()
    {
        return properties.run.getA();
    }

    protected Animations getDrawAnimation()
    {
        return properties.draw.getA();
    }
}
