package pellucid.ava.client.renderers.models.c4;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.util.Pair;

public class RegularC4ModelProperty extends RegularModelProperty<C4Item, C4ModelVariables, C4SubModels, RegularC4ModelProperty>
{
    public Pair<Animations, Pair<Animations, Animations>> set = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));

    public RegularC4ModelProperty(C4Item c4)
    {
        super(AVAModelTypes.C4, c4);
    }

    @Override
    public RegularC4ModelProperty construct(C4Item item)
    {
        return new RegularC4ModelProperty(item);
    }

    public RegularC4ModelProperty copyFor(C4Item c4)
    {
        return super.copyFor(c4)
                .set(Animations.of(set.getA()))
                .setLeft(Animations.of(set.getB().getA()))
                .setRight(Animations.of(set.getB().getB()));
    }

    public RegularC4ModelProperty set(Animations perspective)
    {
        set.setA(perspective);
        return this;
    }

    public RegularC4ModelProperty setLeft(Animations perspective)
    {
        set.getB().setA(perspective);
        return this;
    }

    public RegularC4ModelProperty setRight(Animations perspective)
    {
        set.getB().setB(perspective);
        return this;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        super.writeToJson(json);
        writeAnimations(set, json, "set");
    }

    @Override
    public void readFromJson(JsonElement json2)
    {
        super.readFromJson(json2);
        JsonObject json = json2.getAsJsonObject();
        readAnimations(set, json, "set");
    }
}
