package pellucid.ava.client.renderers.models.guns;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.registries.BuiltInRegistries;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.items.guns.AVAItemGun;

import java.util.Locale;
import java.util.function.Function;

public enum GunSubModels implements ISubModels<AVAItemGun>
{
    LID(),
    LID_CLOSE(),
    LID_OPEN(),
    BULLET(),
    BULLETS(),
    FRONT(),
    WHEEL(),
    MAGAZINE(),
    MAGAZINE_2(),
    MAGAZINE_CLIP(),
    FIRE(),
    SLIDE(),
    HANDLE(),
    STOCK(),
    SILENCER(),
    SCOPE(),
    LENS(),
    LENS_ADS(),
    GRENADE(),
    ROCKET(),
    FOREARM(),
    ;
    private final Function<AVAItemGun, ModelResourceLocation> model;

    GunSubModels()
    {
        this.model = (gun) -> {
            String root = BuiltInRegistries.ITEM.getKey(gun.getMaster()).getPath();
            String name = BuiltInRegistries.ITEM.getKey(gun).getPath();
            return new ModelResourceLocation(AVA.MODID, root + "/" + name + "_" + name().toLowerCase(Locale.ROOT), "inventory");
        };
    }

    @Override
    public ModelResourceLocation generateFor(AVAItemGun gun)
    {
        return model.apply(gun);
    }

    @Override
    public boolean addedToModelByDefault()
    {
        return this != LENS && this != LENS_ADS;
    }

    @Override
    public String getRLName()
    {
        return name().toLowerCase(Locale.ROOT);
    }
}
