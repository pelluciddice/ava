package pellucid.ava.client.renderers.models.guns;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.QuadAnimator;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.Pair;

import static net.minecraft.core.Direction.SOUTH;
import static pellucid.ava.client.renderers.models.QuadAnimator.Motion.*;
import static pellucid.ava.client.renderers.models.guns.GunModelVariables.FIRE_TICKS;
import static pellucid.ava.client.renderers.models.guns.GunModelVariables.INSTALLED_SILENCER;
import static pellucid.ava.client.renderers.models.guns.GunSubModels.*;

public class RegularGunModelProperty extends RegularModelProperty<AVAItemGun, GunModelVariables, GunSubModels, RegularGunModelProperty>
{
    public Pair<Animations, Pair<Animations, Animations>> reload = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Pair<Animations, Pair<Animations, Animations>> preReload = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Pair<Animations, Pair<Animations, Animations>> postReload = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Pair<Animations, Pair<Animations, Animations>> fire = Pair.of(Animations.of(), Pair.of(Animations.of(), Animations.of()));
    public Perspective silencerPos = Perspective.EMPTY;
    public Perspective aimingPos = Perspective.EMPTY;
    public Pair<Animations, Animations> installSilencerHands = Pair.of(Animations.of(), Animations.of());

    public RegularGunModelProperty(AVAItemGun gun)
    {
        super(AVAModelTypes.GUNS, gun);
    }

    @Override
    public RegularGunModelProperty construct(AVAItemGun item)
    {
        return new RegularGunModelProperty(item);
    }

    public RegularGunModelProperty copyFor(AVAItemGun gun)
    {
        return super.copyFor(gun).reload(Animations.of(reload.getA()))
                .reloadLeft(Animations.of(reload.getB().getA()))
                .reloadRight(Animations.of(reload.getB().getB()))
                .preReload(Animations.of(preReload.getA()))
                .preReloadLeft(Animations.of(preReload.getB().getA()))
                .preReloadRight(Animations.of(preReload.getB().getB()))
                .postReload(Animations.of(postReload.getA()))
                .postReloadLeft(Animations.of(postReload.getB().getA()))
                .postReloadRight(Animations.of(postReload.getB().getB()))
                .silencerPos(silencerPos.copy())
                .aimingPos(aimingPos.copy())
                .silencerLeft(Animations.of(installSilencerHands.getA()))
                .silencerRight(Animations.of(installSilencerHands.getB()))
                .fire(Animations.of(fire.getA()))
                .fireLeft(Animations.of(fire.getB().getA()))
                .fireRight(Animations.of(fire.getB().getB()));
    }

    public RegularGunModelProperty reload(Animations animations)
    {
        reload.setA(animations);
        return this;
    }

    public RegularGunModelProperty reloadLeft(Animations animations)
    {
        reload.getB().setA(animations);
        return this;
    }

    public RegularGunModelProperty reloadRight(Animations animations)
    {
        reload.getB().setB(animations);
        return this;
    }

    public RegularGunModelProperty preReload(Animations animations)
    {
        preReload.setA(animations);
        return this;
    }

    public RegularGunModelProperty preReloadLeft(Animations animations)
    {
        preReload.getB().setA(animations);
        return this;
    }

    public RegularGunModelProperty preReloadRight(Animations animations)
    {
        preReload.getB().setB(animations);
        return this;
    }

    public RegularGunModelProperty postReload(Animations animations)
    {
        postReload.setA(animations);
        return this;
    }

    public RegularGunModelProperty postReloadLeft(Animations animations)
    {
        postReload.getB().setA(animations);
        return this;
    }

    public RegularGunModelProperty postReloadRight(Animations animations)
    {
        postReload.getB().setB(animations);
        return this;
    }

    public RegularGunModelProperty quadAnimFire()
    {
        return quadAnim(FIRE, new QuadAnimator.Animator.Vanisher(TO, FIRE_TICKS, 0, 2));
    }

    public RegularGunModelProperty quadAnimFire(int stopAt)
    {
        return quadAnimFire().quadAnim(FIRE, new QuadAnimator.Animator.Vanisher(FROM, FIRE_TICKS, stopAt, Integer.MAX_VALUE));
    }

    public RegularGunModelProperty quadAnimSilencer()
    {
        return quadAnim(FIRE, new QuadAnimator.Animator.Vanisher(CONSTANT, INSTALLED_SILENCER, 1, 1))
                .quadAnim(SILENCER, new QuadAnimator.Animator.Silencer());
    }

    public RegularGunModelProperty quadAnimSlide(float amount)
    {
        return quadAnim(SLIDE, new QuadAnimator.Animator.Translator(TO, FIRE_TICKS, amount, 0, 1, SOUTH))
                .quadAnim(SLIDE, new QuadAnimator.Animator.Translator(FROM, FIRE_TICKS, amount, 1, 3, SOUTH));
    }

    public RegularGunModelProperty quadAnimHandle(float amount)
    {
        return quadAnim(HANDLE, new QuadAnimator.Animator.Translator(TO, FIRE_TICKS, amount, 0, 1, SOUTH))
                .quadAnim(HANDLE, new QuadAnimator.Animator.Translator(FROM, FIRE_TICKS, amount, 1, 3, SOUTH));
    }

    public RegularGunModelProperty silencerPos(Perspective pos)
    {
        silencerPos = pos;
        return this;
    }

    public RegularGunModelProperty aimingPos(Perspective pos)
    {
        aimingPos = pos;
        return this;
    }

    public RegularGunModelProperty silencerLeft(Animations animations)
    {
        installSilencerHands.setA(animations);
        return this;
    }

    public RegularGunModelProperty silencerRight(Animations animations)
    {
        installSilencerHands.setB(animations);
        return this;
    }

    public RegularGunModelProperty fire(Animations animations)
    {
        fire.setA(animations);
        return this;
    }

    public RegularGunModelProperty fireLeft(Animations animations)
    {
        fire.getB().setA(animations);
        return this;
    }

    public RegularGunModelProperty fireRight(Animations animations)
    {
        fire.getB().setB(animations);
        return this;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        super.writeToJson(json);
        writeAnimations(reload, json, "reload");
        writeAnimations(fire, json, "fire");

        json.add("silencerPos", silencerPos.writeToJsonR());

        json.add("aimingPos", aimingPos.writeToJsonR());

        JsonObject obj2 = new JsonObject();
        obj2.add("left", installSilencerHands.getA().writeToJsonR());
        obj2.add("right", installSilencerHands.getB().writeToJsonR());
        json.add("silencer", obj2);
    }

    @Override
    public void readFromJson(JsonElement json2)
    {
        super.readFromJson(json2);
        JsonObject json = json2.getAsJsonObject();

        readAnimations(reload, json, "reload");
        readAnimations(fire, json, "fire");

        silencerPos.readFromJson(json.get("silencerPos"));

        aimingPos.readFromJson(json.get("aimingPos"));

        JsonObject obj2 = json.get("silencer").getAsJsonObject();
        installSilencerHands.set(Animations.of(obj2.get("left")), Animations.of(obj2.get("right")));
    }
}
