package pellucid.ava.client.renderers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.util.IJsonSerializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class Animations extends ArrayList<Animation> implements IJsonSerializable<Animations>
{
    public static final Animations EMPTY = of();
    private final List<Animation> unpopulatedAnimations = new ArrayList<>();

    public static Animations of()
    {
        return new Animations();
    }
    public static Animations of(Animations animations)
    {
        return new Animations(animations);
    }
    public static Animations of(JsonElement json)
    {
        return IJsonSerializable.create(new Animations(), json);
    }

    private Animations(Animations animations)
    {
        this();
        for (Animation anim : animations.unpopulatedAnimations)
            append(Animation.of(anim));
    }

    private Animations()
    {
        super();
    }

    public List<Animation> getUnpopulatedAnimations()
    {
        return unpopulatedAnimations;
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        JsonArray array = new JsonArray();
        for (Animation anim : unpopulatedAnimations)
        {
            JsonObject element = new JsonObject();
            anim.writeToJson(element);
            array.add(element);
        }
        json.add("animations", array);
    }

    @Override
    public void readFromJson(JsonElement json)
    {
        JsonArray array = json.getAsJsonObject().get("animations").getAsJsonArray();
        unpopulatedAnimations.clear();
        clear();
        for (JsonElement element : array)
            append(Animation.of(element));
    }

    public Animations discard(int targetTicks)
    {
        unpopulatedAnimations.removeIf((a) -> a.targetTicks == targetTicks);
        populateAnimations(true);
        return this;
    }

    public <T extends Collection<Animation>> Animations appendAll(T coll)
    {
        coll.forEach(this::append);
        return this;
    }

    public Animations append(Animation animation)
    {
        boolean removed = unpopulatedAnimations.removeIf((a) -> a.targetTicks == animation.targetTicks);
        unpopulatedAnimations.add(animation);
        populateAnimations(removed);
        return this;
    }

    public Animations replaceEmptyWithDefault(Perspective defaultPerspective)
    {
        List<Animation> unpopulatedAnimations = new ArrayList<>(this.unpopulatedAnimations);
        for (int i = 0; i < unpopulatedAnimations.size(); i++)
        {
            Animation animation = unpopulatedAnimations.get(i);
            if (animation.target3f.isEmpty())
                unpopulatedAnimations.set(i, Animation.of(animation.targetTicks, defaultPerspective));
        }
        populateAnimations(unpopulatedAnimations, true);
        return this;
    }

    public void populateAnimations(boolean override)
    {
        populateAnimations(unpopulatedAnimations, override);
    }

    private void populateAnimations(List<Animation> unpopulatedAnimations, boolean override)
    {
        if (override)
            clear();
        unpopulatedAnimations.sort(Comparator.comparingInt((a) -> a.targetTicks));
        for (int i = 0; i < unpopulatedAnimations.size(); i++)
        {
            Animation prev = unpopulatedAnimations.get(Math.max(i - 1, 0));
            Animation current = unpopulatedAnimations.get(i);
//            for (int j = (override ? prev.targetTicks : size()) + 1; j <= current.targetTicks; j++)
            for (int j = (override ? prev.targetTicks : size()); j <= current.targetTicks; j++)
            {
                int finalJ = j;
                removeIf((a) -> a.targetTicks == finalJ);
                add(Animation.of(j, prev.getPerspectiveInBetween(current, j)));
            }
        }
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        for (Animation anim : this)
            str.append(anim).append("\n");
        return str.toString();
    }

    public String toCode(String key, Perspective o, Perspective l, Perspective r)
    {
        if (isEmpty())
            return "";
        StringBuilder str = new StringBuilder();
        if (!key.isEmpty())
            str.append(".").append(key).append("(");
        str.append("Animations.of()");
        for (Animation anim : unpopulatedAnimations)
        {
            str.append("\n");
            String animString = anim.toCode();
            if (!anim.target3f.isEmpty())
            {
                if (anim.target3f.equals(o))
                    animString = "Animation.of(" + anim.targetTicks + ", o)";
                else if (anim.target3f.equals(l))
                    animString = "Animation.of(" + anim.targetTicks + ", l)";
                else if (anim.target3f.equals(r))
                    animString = "Animation.of(" + anim.targetTicks + ", r)";
            }
            str.append("        .append(").append(animString).append(")");
        }
        if (!key.isEmpty())
            str.append(")");
        return str.toString();
    }
}
