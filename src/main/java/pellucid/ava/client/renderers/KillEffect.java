package pellucid.ava.client.renderers;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import pellucid.ava.AVA;

public class KillEffect extends EntityEffect
{
    public KillEffect(Entity entity)
    {
        super(entity);
    }

    @Override
    public float yOffset(Entity entity)
    {
        return -entity.getBbHeight() / 2.0F;
    }

    @Override
    public float width(float distance)
    {
        return distance / 27.5F;
    }

    @Override
    public float height(float distance)
    {
        return distance / 27.5F;
    }

    private static final ResourceLocation KILL_MARK = new ResourceLocation(AVA.MODID + ":textures/environment/kill_mark.png");
    @Override
    public ResourceLocation getTexture()
    {
        return KILL_MARK;
    }
}
