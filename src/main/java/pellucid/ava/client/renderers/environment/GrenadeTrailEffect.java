package pellucid.ava.client.renderers.environment;

import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;

public class GrenadeTrailEffect extends EnvironmentObjectEffect
{
    public GrenadeTrailEffect(Vec3 vec)
    {
        super(BlockPos.ZERO, vec, null, true);
        live = 16;
    }

    @Override
    public float lerpLive(float partialTicks)
    {
        return Mth.lerp(partialTicks, live, live - 1) / 17.0F;
    }
}
