package pellucid.ava.client.renderers.environment;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Mth;
import net.minecraft.world.item.Item;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.entities.objects.plain_smoke.PlainSmokeEntity.Phase;
import pellucid.ava.items.throwables.SmokeGrenade;
import pellucid.ava.util.DataTypes;

import java.awt.Color;

public class SmokeEffect extends EnvironmentObjectEffect
{
    private Phase phase = Phase.GROW;
    private SmokeGrenade grenade;

    public SmokeEffect()
    {
    }

    public SmokeEffect(Vec3 vec, SmokeGrenade grenade)
    {
        super(BlockPos.ZERO, vec, null, true);
        setColour(new Color(grenade.colour));
        this.live = grenade.growTime;
        this.grenade = grenade;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = super.serializeNBT();
        DataTypes.INT.write(compound, "colour", colour.getRGB());
        DataTypes.ITEM.write(compound, "grenade", grenade);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        super.deserializeNBT(nbt);
        Item item = DataTypes.ITEM.read(nbt, "grenade");
        grenade = item instanceof SmokeGrenade ? (SmokeGrenade) item : null;
        colour = new Color(DataTypes.INT.read(nbt, "colour"));
    }

    @Override
    public void tick()
    {
        super.tick();
        if (live <= 0)
        {
            phase = phase.next();
            switch (phase)
            {
                case GROW:
                    setKilled(true);
                    break;
                case STEADY:
                    live = grenade.steadyTime;
                    break;
                case SHRINK:
                    live = grenade.shrinkTime;
                    break;
            }
        }
    }

    public float getSize(float partialTicks)
    {
        return phase == Phase.GROW ? Mth.lerp(partialTicks, grenade.growTime - live, grenade.growTime - live + 1) / (float) grenade.growTime : 1.0F;
    }

    @Override
    public float lerpLive(float partialTicks)
    {
        return (phase == Phase.SHRINK ? Mth.lerp(partialTicks, live, live - 1) / (float) grenade.shrinkTime : getSize(partialTicks)) * 0.95F;
    }

    @Override
    public boolean shouldBeDead()
    {
        return killed || (super.shouldBeDead() && phase == Phase.SHRINK);
    }


}
