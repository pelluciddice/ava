package pellucid.ava.client.renderers.environment;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.client.renderers.BaseEffect;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;
import java.awt.Color;

public class EnvironmentObjectEffect extends BaseEffect
{
    private BlockPos pos;
    private Vec3 vec;
    @Nullable
    private Direction direction;
    public float random = AVAConstants.RAND.nextFloat();

    public EnvironmentObjectEffect()
    {
    }

    public EnvironmentObjectEffect(BlockHitResult result)
    {
        this(result, false);
    }

    public EnvironmentObjectEffect(BlockHitResult result, boolean blend)
    {
        this(result.getBlockPos(), result.getLocation(), result.getDirection(), blend);
    }

    public EnvironmentObjectEffect(BlockPos pos, Vec3 vec, @Nullable Direction direction)
    {
        this(pos, vec, direction, false);
    }

    public EnvironmentObjectEffect(BlockPos pos, Vec3 vec, @Nullable Direction direction, boolean blend)
    {
        super(blend);
        this.pos = pos;
        this.vec = vec;
        this.direction = direction;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = AVAWeaponUtil.writeVec(super.serializeNBT(), getVec());
        DataTypes.BLOCKPOS.write(compound, "pos", pos);
        DataTypes.COMPOUND.write(compound, "vec", AVAWeaponUtil.writeVec(new CompoundTag(), getVec()));
        if (direction != null)
            DataTypes.DIRECTION.write(compound, "direction", direction);
        DataTypes.FLOAT.write(compound, "random", random);
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        super.deserializeNBT(nbt);
        pos = DataTypes.BLOCKPOS.read(nbt, "pos");
        vec = AVAWeaponUtil.readVec(DataTypes.COMPOUND.read(nbt, "vec"));
        if (nbt.contains("direction"))
            direction = DataTypes.DIRECTION.read(nbt, "direction");
        random = DataTypes.FLOAT.read(nbt, "random");
    }

    @Override
    public Color getColour(Level world)
    {
        if (colour == null)
            colour = new Color(world.getBlockState(getPos()).getMapColor(world, getPos()).col);
        return colour;
    }

    public BlockPos getPos()
    {
        return pos;
    }

    public Vec3 getVec()
    {
        return vec;
    }

    @Nullable
    public Direction getDirection()
    {
        return direction;
    }

    @Override
    public String toString()
    {
        return "EnvironmentObjectEffect{" +
                "live=" + live +
                ", pos=" + pos +
                ", vec=" + vec +
                ", direction=" + direction +
                ", colour=" + colour +
                '}';
    }
}
