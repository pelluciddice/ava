package pellucid.ava.client.renderers.environment;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.AVA;
import pellucid.ava.util.DataTypes;

public class ActivePingEffect extends EnvironmentObjectEffect
{
    private Type type;

    public ActivePingEffect()
    {
    }

    public ActivePingEffect(Vec3 vec, Type type)
    {
        super(BlockPos.ZERO, vec, null, false);
        this.type = type;
        this.live = 100;
    }

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = super.serializeNBT();
        DataTypes.INT.write(compound, "type", type.ordinal());
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        super.deserializeNBT(nbt);
        type = Type.values()[DataTypes.INT.read(nbt, "type")];
    }

    private static final ResourceLocation ATTACK_1 = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_attack_1.png");
    private static final ResourceLocation ATTACK_2 = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_attack_2.png");
    private static final ResourceLocation ATTACK_3 = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_attack_3.png");
    private static final ResourceLocation ATTACK_4 = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_attack_4.png");
    private static final ResourceLocation BACKUP = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_backup.png");
    private static final ResourceLocation C4 = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_c4.png");
    private static final ResourceLocation DANGER = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_danger.png");
    private static final ResourceLocation DEFEND = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_defend.png");
    private static final ResourceLocation MOVE = new ResourceLocation(AVA.MODID, "textures/overlay/active_ping_move.png");

    public ResourceLocation getTexture()
    {
        switch (getType())
        {
            case ATTACK:
                if (live >= 80)
                    return ATTACK_3;
                else if (live >= 60)
                    return ATTACK_2;
                else if (live >= 40)
                    return ATTACK_1;
                return ATTACK_4;
            case BACKUP:
            default:
                return BACKUP;
            case C4:
                return C4;
            case DANGER:
                return DANGER;
            case DEFEND:
                return DEFEND;
            case MOVE:
                return MOVE;
        }
    }

    public Type getType()
    {
        return type;
    }

    @Override
    public float lerpLive(float partialTicks)
    {
        return 1.0F;
    }

    public enum Type
    {
        BACKUP,
        MOVE,
        DEFEND,
        DANGER,
        C4,
        ATTACK,
    }
}
