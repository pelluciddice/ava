package pellucid.ava.client.renderers.environment;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.math.Axis;
import net.minecraft.Util;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.ModList;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.RenderLevelStageEvent;
import net.neoforged.neoforge.client.event.RenderLivingEvent;
import org.joml.Vector3f;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.client.renderers.AVARenderTypes;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.EntityEffect;
import pellucid.ava.client.renderers.HUDIndicators;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.entities.objects.plain_smoke.PlainSmokeEntity;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import static pellucid.ava.util.AVAClientUtil.draw3DLine;
import static pellucid.ava.util.AVAClientUtil.drawTransparent;

@EventBusSubscriber(Dist.CLIENT)
public class EnvironmentRenderer
{
    private static final ResourceLocation BULLET_HOLE = new ResourceLocation(AVA.MODID + ":textures/environment/bullet_hole.png");
    private static final ResourceLocation BLOOD = new ResourceLocation(AVA.MODID + ":textures/environment/blood.png");
    private static final ResourceLocation KNIFE_HOLE = new ResourceLocation(AVA.MODID + ":textures/environment/knife_hole.png");
    private static final ResourceLocation GRENADE_MARK = new ResourceLocation(AVA.MODID + ":textures/environment/grenade_mark.png");
    private static final ResourceLocation SMOKE = new ResourceLocation(AVA.MODID + ":textures/environment/smoke.png");
    private static final ResourceLocation UAV = new ResourceLocation(AVA.MODID + ":textures/overlay/uav.png");
    public static final ResourceLocation X9 = new ResourceLocation(AVA.MODID + ":textures/overlay/x9.png");

    private static final ResourceLocation SHADER_SNOWSTORM = new ResourceLocation(AVA.MODID, "shaders/post/snowstorm.json");
    private static final ResourceLocation SHADER_SANDSTORM = new ResourceLocation(AVA.MODID, "shaders/post/sandstorm.json");

    @SubscribeEvent
    public static void onEntityRender(RenderLivingEvent.Pre<?, ?> event)
    {
        Entity entity = event.getEntity();
        AVAWorldData cap = AVAWorldData.getInstance(entity.level());
        if (cap.hasRenderAreaEnabled() && new ArrayList<>(cap.renderingArea).stream().noneMatch((area) -> AVAWeaponUtil.isInArea(entity.position(), area.getFrom(), area.getTo())))
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void onRenderBlockOverlay(RenderLevelStageEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Level world = minecraft.level;
        Player player = Minecraft.getInstance().player;
        PoseStack stack = event.getPoseStack();
        float partialTicks = event.getPartialTick();
        if (world != null)
        {
            AVAWorldData data = AVAWorldData.getInstance(world);
            final Vec3 camera = minecraft.gameRenderer.getMainCamera().getPosition();
            AVAClientUtil.drawTransparent(true);
            if (event.getStage() == RenderLevelStageEvent.Stage.AFTER_ENTITIES)
            {
                // Won't work.
                if (!ModList.get().isLoaded(AVAConstants.OPTIFINE))
                {
                    for (Pair<Vec3, Vec3> trail : data.bulletTrails.keySet())
                    {
                        Vec3 left = trail.getA();
                        Vec3 right = trail.getB();
                        if (left.distanceTo(camera) <= 150.0F || right.distanceTo(camera) <= 150.0F)
                            AVAClientUtil.draw3DLine(stack, AVARenderTypes.LINE_1_5D, trail.getA().subtract(camera), trail.getB().subtract(camera), 255, 210, 0, 0.5F, null);
                    }
                }
                if (HUDIndicators.ActionPing.ACTIVE)
                {
                    Vec3 pingVec = HUDIndicators.ActionPing.VEC;
                    if (pingVec != null && player != null)
                    {
                        Camera info = minecraft.gameRenderer.getMainCamera();
                        Vector3f view = info.getLookVector();
                        Vec3 vec = info.getPosition().add(view.x(), view.y(), view.z());
                        draw3DLine(stack, AVARenderTypes.LINE_10_0D, vec.subtract(camera), pingVec.subtract(camera), 255, 255, 255, 1.0F, null);
                    }
                }
            }
            if (event.getStage() == RenderLevelStageEvent.Stage.AFTER_TRIPWIRE_BLOCKS)
            {
                RenderSystem.setShader(GameRenderer::getPositionTexShader);
                RenderSystem.enableDepthTest();
                boolean thirdPerson = !minecraft.options.getCameraType().isFirstPerson();
                for (EnvironmentObjectEffect bulletHole : new ArrayList<>(data.bulletHoles))
                    renderObjectAt(bulletHole, world, stack, 0.075F, 0.01F, BULLET_HOLE, partialTicks);
                for (EnvironmentObjectEffect blood : new ArrayList<>(data.bloods))
                    renderObjectAt(blood, world, stack, 0.525F, 0.011F, BLOOD, partialTicks);
                for (EnvironmentObjectEffect knifeHole : new ArrayList<>(data.knifeHoles))
                    renderObjectAt(knifeHole, world, stack, 0.095F, 0.0105F, KNIFE_HOLE, partialTicks);
                for (EnvironmentObjectEffect grenadeMark : new ArrayList<>(data.grenadeMarks))
                    renderObjectAt(grenadeMark, world, stack, 2.5F, 0.075F, GRENADE_MARK, partialTicks);
                if (AVAClientConfig.ENABLE_PROJECTILE_TRAIL_EFFECT.get())
                    for (ProjectileEntity entity : data.grenadeTrails.keySet())
                        renderGrenadeTrails(new ArrayList<>(data.grenadeTrails.get(entity)), AVAWeaponUtil.isSameSide(entity.getShooter(), player), stack, partialTicks);
                RenderSystem.enableDepthTest();
                AVAClientUtil.drawTransparent(true);
                for (PlainSmokeEntity smoke : Util.make(() -> {
                    List<PlainSmokeEntity> list = AVACommonUtil.cast(AVACommonUtil.toList(minecraft.level.entitiesForRendering()).stream().filter((e) -> e instanceof PlainSmokeEntity && ((PlainSmokeEntity) e).effect != null).toList(), PlainSmokeEntity.class);
                    list.sort(Comparator.comparing((s) -> 1.0F - s.effect.lerpLive(partialTicks)));
                    return list;
                }))
                    renderObjectAt(smoke.effect, world, stack, (smoke.effect.getSize(partialTicks) + smoke.effect.random) * 2.25F, 0.0F, SMOKE, partialTicks);
                AVAClientUtil.drawTransparent(false);
                RenderSystem.disableDepthTest();
                for (ActivePingEffect activePing : new ArrayList<>(data.activePings))
                {
                    RenderSystem.disableDepthTest();
                    AVAClientUtil.drawTransparent(true);
                    double distance = activePing.getVec().distanceTo(Minecraft.getInstance().gameRenderer.getMainCamera().getPosition());
                    if (distance > 100.0F)
                        continue;
                    renderObjectAt(activePing, world, stack, (float) (distance / 20.0F), 0.0F, activePing.getTexture(), partialTicks);
                    stack.pushPose();
                    if (AVAClientUtil.translateMatrixWithCamera(stack, activePing.getVec(), 100.0F) == -1.0D)
                        return;
                    AVAClientUtil.rotateWithCamera(stack, true);
                    AVAClientUtil.scaleWithDistance(stack, AnimationFactory.PING_SCALE.computeIfAbsent(activePing, (ping) -> new AnimationFactory()).lerpD(partialTicks, distance));
                    MultiBufferSource.BufferSource impl = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
                    String text = AVACommonUtil.round(distance, 2) + "m";
                    minecraft.font.drawInBatch(Component.literal(text), -minecraft.font.width(text) / 2.0F, 10, AVAConstants.AVA_HUD_TEXT_WHITE, true, stack.last().pose(), impl, Font.DisplayMode.SEE_THROUGH, 0, AVAConstants.VANILLA_FULL_PACKED_LIGHT);
                    impl.endBatch();
                    stack.popPose();
                }
                if (player != null)
                {
                    new HashMap<>(data.uavEscortC).forEach((id, cd) ->
                    {
                        Entity entity = world.getEntity(id);
                        if (entity instanceof LivingEntity target)
                        {
                            RenderSystem.disableDepthTest();
                            drawTransparent(true);
                            stack.pushPose();
                            float f = target.getBbWidth() * 5.0F;
                            boolean self = AVACommonUtil.self(target, player);
                            if (!self || thirdPerson)
                            {
                                RenderSystem.setShaderColor(150.0F / 255.0F, 55 / 255.0F, 190 / 255.0F, 0.65F);
                                renderObjectAt(stack, AVAClientUtil.clientEntityPos(target, partialTicks).add(0.0F, target.getBbHeight() / 2.0F, 0.0F), true, -0.25F, -0.215F, 0.25F, 0.215F, 150.0F, UAV, () -> stack.scale(f, f, f));
                            }
                            stack.popPose();
                            drawTransparent(false);
                        }
                    });
                    RenderSystem.setShaderColor(1, 1, 1, 1);
                    new HashMap<>(data.uavC).forEach((id, cd) ->
                    {
                        Entity entity = world.getEntity(id);
                        if (entity instanceof LivingEntity target)
                        {
                            RenderSystem.disableDepthTest();
                            drawTransparent(true);
                            stack.pushPose();
                            float f = target.getBbWidth() * 5.0F;
                            boolean self = AVACommonUtil.self(target, player);
                            if (!self || thirdPerson)
                            {
                                RenderSystem.setShaderColor(AVAWeaponUtil.isSameSide(target, player) ? 0.0F : self ? 255.0F : 200.0F, self ? 200.0F : 0.0F, AVAWeaponUtil.isSameSide(target, player) ? 255.0F : 0.0F, 0.45F);
                                renderObjectAt(stack, AVAClientUtil.clientEntityPos(target, partialTicks).add(0.0F, target.getBbHeight() / 2.0F, 0.0F), true, -0.25F, -0.215F, 0.25F, 0.215F, 150.0F, UAV, () -> stack.scale(f, f, f));
                            }
                            stack.popPose();
                            drawTransparent(false);
                        }
                    });
                    RenderSystem.setShaderColor(1, 1, 1, 1);
                    new HashMap<>(data.x9C).forEach((id, cd) ->
                    {
                        Entity entity = world.getEntity(id);
                        if (entity instanceof LivingEntity target)
                        {
                            RenderSystem.disableDepthTest();
                            drawTransparent(true);
                            stack.pushPose();
                            float f = target.getBbWidth() * 5.0F;
                            if (!AVAWeaponUtil.isSameSide(target, player))
                                RenderSystem.setShaderColor(198.0F, 146.0F, 255.0F, 255.0F);
                            renderObjectAt(stack, AVAClientUtil.clientEntityPos(target, partialTicks).add(0.0F, target.getBbHeight() / 2.0F, 0.0F), true, -0.25F, -0.215F, 0.25F, 0.215F, 150.0F, X9, () -> stack.scale(f, f, f));
                            stack.popPose();
                            drawTransparent(false);
                        }
                    });
                    RenderSystem.setShaderColor(1, 1, 1, 1);
                    new ArrayList<>(AVAWorldData.getInstance(world).hitMarks).forEach((mark) -> renderObjectAtEntity(mark, stack, partialTicks));
                    new ArrayList<>(AVAWorldData.getInstance(world).killMarks).forEach((mark) -> renderObjectAtEntity(mark, stack, partialTicks));
                }
            }
            RenderSystem.enableDepthTest();
            AVAClientUtil.drawTransparent(false);
        }
    }

    private static void renderGrenadeTrails(List<GrenadeTrailEffect> grenadeTrail, boolean ally, PoseStack stack, float partialTicks)
    {
        Vec3 camera = Minecraft.getInstance().gameRenderer.getMainCamera().getPosition();
        for (int i = 0; i < grenadeTrail.size() - 1; i++)
        {
            drawTransparent(true);
            GrenadeTrailEffect current = grenadeTrail.get(i);
            GrenadeTrailEffect next = grenadeTrail.get(i + 1);
            Vec3 currentVec = current.getVec();
            Vec3 nextVec = next.getVec();
            if (i == 0 && current.live == 1)
                currentVec = nextVec.add(currentVec.subtract(nextVec).scale(1.0F - partialTicks));
            else if (i == grenadeTrail.size() - 2)
                nextVec = currentVec.add(nextVec.subtract(currentVec).scale(partialTicks));
            if (currentVec.distanceTo(camera) <= 150.0F || nextVec.distanceTo(camera) <= 150.0F)
            {
                if (ally)
                    draw3DLine(stack, AVARenderTypes.LINE_3_0D, currentVec.subtract(camera), nextVec.subtract(camera), 0, 120, 255, 0.55F, null);
                else
                    draw3DLine(stack, AVARenderTypes.LINE_3_0D, currentVec.subtract(camera), nextVec.subtract(camera), 230, 0, 0, 0.55F, null);
            }
            drawTransparent(false);
        }
    }

    private static void renderObjectAtEntity(EntityEffect effect, PoseStack stack, float partialTicks)
    {
        Entity entity = effect.getEntity();
        if (!effect.shouldBeDead() && entity instanceof LivingEntity target)
        {
            RenderSystem.disableDepthTest();
            drawTransparent(true);
            stack.pushPose();
            Color color = effect.colour;
            RenderSystem.setShaderColor(color.getRed() / 255.0F, color.getGreen() / 255.0F, color.getBlue() / 255.0F, color.getAlpha() / 255.0F);
            double distance = AVAClientUtil.translateMatrixWithCamera(stack, AVAClientUtil.clientEntityPos(target, partialTicks).add(0.0F, target.getBbHeight() + effect.yOffset(entity), 0.0F), 200.0D);
            if (distance == -1.0D)
                return;
            AVAClientUtil.rotateWithCamera(stack, true);
            float width = effect.width((float) distance);
            float height = effect.height((float) distance);
            AVAClientUtil.blit(stack, effect.getTexture(), -width, -height, width, height);
            stack.popPose();
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            drawTransparent(false);
            RenderSystem.enableDepthTest();
        }
    }

    private static void renderObjectAt(EnvironmentObjectEffect object, Level world, PoseStack stack, float size, float offsetScale, ResourceLocation texture, float partialTicks)
    {
        stack.pushPose();
        Direction direction = object.getDirection();
        if (object.doBlend())
        {
            Color colour = object.getColour(world);
            RenderSystem.setShaderColor(colour.getRed() / 255.0F, colour.getGreen() / 255.0F, colour.getBlue() / 255.0F, object.lerpLive(partialTicks));
        }
        else RenderSystem.setShaderColor(1.0F, 1.0F, 1.0f, 1.0F);
        float finalOffsetScale = (float) (offsetScale + object.random / 100.0F);
        renderObjectAt(stack, object.getVec(), direction == null, size, texture, () -> {
            if (direction != null)
            {
                Vec3i offset = direction.getNormal();
                stack.translate(offset.getX() * finalOffsetScale, offset.getY() * finalOffsetScale, offset.getZ() * finalOffsetScale);
                rotateByDirection(stack, direction);
            }
        });
        stack.popPose();
    }

    private static void renderObjectAt(PoseStack stack, Vec3 vec, boolean alwaysFacing, float size, ResourceLocation texture, @Nullable Runnable postTransforms)
    {
        renderObjectAt(stack, vec, alwaysFacing, -size, -size, size, size, 100.0F, texture, postTransforms);
    }

    private static void renderObjectAt(PoseStack stack, Vec3 vec, boolean alwaysFacing, float x1, float y1, float x2, float y2, float maxDistance, ResourceLocation texture, @Nullable Runnable postTransforms)
    {
        stack.pushPose();
        if (AVAClientUtil.translateMatrixWithCamera(stack, vec, maxDistance) == -1.0D)
            return;
        if (alwaysFacing)
            AVAClientUtil.rotateWithCamera(stack, true);
        if (postTransforms != null)
            postTransforms.run();
        AVAClientUtil.blit(stack, texture, x1, y1, x2, y2);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0f, 1.0F);
        stack.popPose();
    }

    private static void rotateByDirection(PoseStack stack, Direction direction)
    {
        stack.mulPose(direction.getRotation());
        stack.mulPose(Axis.XP.rotationDegrees(90));
    }
}
