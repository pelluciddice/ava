package pellucid.ava.client.renderers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.LayeredDraw;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.scores.PlayerTeam;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ComputeFovModifierEvent;
import net.neoforged.neoforge.client.event.RenderGuiLayerEvent;
import net.neoforged.neoforge.client.event.ViewportEvent;
import net.neoforged.neoforge.client.gui.VanillaGuiLayers;
import org.joml.Matrix4f;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.overlay.KillTips;
import pellucid.ava.competitive_mode.CompetitiveUI;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.events.ClientModEventBus;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.gamemodes.modes.TeamedMode;
import pellucid.ava.gamemodes.scoreboard.AVAPlayerStat;
import pellucid.ava.gamemodes.scoreboard.AVAScoreboardManager;
import pellucid.ava.gamemodes.scoreboard.GameModeRenderer;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.player.PositionWithRotation;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static pellucid.ava.util.AVAClientUtil.*;
import static pellucid.ava.util.AVACommonUtil.*;

@EventBusSubscriber(Dist.CLIENT)
public class AVARenderer
{
    public static final ResourceLocation BLACK = new ResourceLocation(AVA.MODID + ":textures/overlay/black.png");
    public static final ResourceLocation WHITE = new ResourceLocation(AVA.MODID + ":textures/overlay/white.png");
    public static final ResourceLocation CROSSHAIR = new ResourceLocation(AVA.MODID + ":textures/overlay/crosshair.png");
    public static final ResourceLocation TEAMMATE = new ResourceLocation(AVA.MODID + ":textures/overlay/teammate.png");
    public static final ResourceLocation LEOPARD = new ResourceLocation(AVA.MODID + ":textures/overlay/leopard.png");
    public static final ResourceLocation ESCORT_SPAWN_EU = new ResourceLocation(AVA.MODID + ":textures/overlay/escort_spawn_eu.png");
    public static final ResourceLocation ESCORT_SPAWN_NRF = new ResourceLocation(AVA.MODID + ":textures/overlay/escort_spawn_nrf.png");

    @SubscribeEvent
    public static void onCameraPosition(ViewportEvent.ComputeCameraAngles event)
    {
        if (CAMERA_VERTICAL_SHAKE_TIME > 0)
            event.setPitch((float) (event.getPitch() + Mth.lerp(event.getPartialTick(), calculateExplosionCameraShake(CAMERA_VERTICAL_SHAKE_TIME_MAX - CAMERA_VERTICAL_SHAKE_TIME, CAMERA_VERTICAL_SHAKE_WEAKNESS), calculateExplosionCameraShake(CAMERA_VERTICAL_SHAKE_TIME_MAX - CAMERA_VERTICAL_SHAKE_TIME + 1, CAMERA_VERTICAL_SHAKE_WEAKNESS))));
    }

    protected static float calculateExplosionCameraShake(int t, float weakness)
    {
        return (float) (15.0F / (weakness * t + 2) * Math.cos(10 * (t + 2)));
    }

    public static final LayeredDraw.Layer CROSSHAIR_UI = (stack, partialTicks) -> {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        boolean scopeCentering = ItemAnimatingGUI.isAnimating();
        if (isAvailable(player) && (scopeCentering || isFocused()) && minecraft.options.getCameraType().isFirstPerson())
        {
            //todo
//            if ((!AVAWeaponUtil.isWeaponAiming(stack) || scopeCentering) && !player.isSprinting() && WorldData.getCap(player.getCommandSenderWorld()).shouldRenderCrosshair() && AVAClientConfig.SHOULD_RENDER_CROSS_HAIR.get())
            ItemStack item = getHeldStack(player);
            AVAItemGun.IScopeType scope = getGun(player).getScopeType(item, true);
            if (scopeCentering ||
                    (AVACrossWorldData.getInstance().shouldRenderCrosshair &&
                            AVAClientConfig.SHOULD_RENDER_CROSS_HAIR.get() &&
                            !player.isSprinting()) &&
                            (!PlayerAction.getCap(player).isADS() || scope.isIronSight()))
            {
                int screenWidth = minecraft.getWindow().getGuiScaledWidth();
                int screenHeight = minecraft.getWindow().getGuiScaledHeight();
                if (GunStatusManager.INSTANCE.isActive(GunStatusManager.GunStatus.RELOAD, GunStatusManager.GunStatus.INSTALL_SILENCER, GunStatusManager.GunStatus.UNINSTALL_SILENCER))
                {
                    stack.pose().pushPose();
                    stack.pose().translate(screenWidth / 2.0F, screenHeight / 2.0F, 0.0F);
                    double to = AnimationFactory.RELOAD_ICON.lerpD(partialTicks, GunStatusManager.INSTANCE.getProgressFactor() * 360.0D);
                    AVAClientUtil.drawHollowCircle(stack, 6F, 6, 0, 0, 0, to, 5.0F, 150, 200, 200, 0.75F);
                    AVAClientUtil.drawHollowCircle(stack, 2.5F, 6, 0, 0, 0, to, 5.0F, 255, 255, 255, 1.0F);
                    stack.pose().popPose();
                }
                else
                {
                    if (getGun(player).hasCrosshair(item) || scopeCentering)
                    {
                        drawTransparent(true);
                        AVAItemGun gun = getGun(player);
                        float spreadOffset = AnimationFactory.SPREAD.lerpF(partialTicks, gun.spread(cap(player), player, item, getGun(player).getMultiplier(player), false) * 15.0F);
                        renderCrosshairAt(stack.pose(), screenWidth / 2.0F, screenHeight / 2.0F, spreadOffset);
                        drawTransparent(false);
                    }
                }
            }
        }
    };

    public static void renderCrosshairAt(PoseStack stack, float x, float y, float spread)
    {
        if (AVAClientConfig.SHOULD_RENDER_CROSS_HAIR.get())
        {
            drawTransparent(true);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);

            // Centre Dot
            float dotSize = AVAClientConfig.CENTRE_DOT_SIZE.get().floatValue() / 10.0F;
            AVAClientUtil.fill(stack, x - dotSize / 2.0F, y - dotSize / 2.0F, dotSize, dotSize, 0xFFFFFFFF);

            // Crosshair
            int colour = (int) (AVAClientConfig.TRANSPARENCY.get().floatValue() / 100.0F * 255.0F) << 24 | AVAClientConfig.RED.get() << 16 | AVAClientConfig.GREEN.get() << 8 | AVAClientConfig.BLUE.get();
            float l = AVAClientConfig.LINE_LENGTH.get().floatValue() / 10.0F;
            float w = AVAClientConfig.LINE_THICKNESS.get().floatValue() / 10.0F;
            spread += 1.0F;
            spread *= AVAClientConfig.OFFSET_SCALE.get().floatValue() / 10.0F;
            AVAClientUtil.fill(stack, x - l - spread, y - w / 2.0F, l, w, colour);
            AVAClientUtil.fill(stack, x + spread, y - w / 2.0F, l, w, colour);
            AVAClientUtil.fill(stack, x - w / 2.0F, y - l - spread, w, l, colour);
            AVAClientUtil.fill(stack, x - w / 2.0F, y + spread, w, l, colour);

            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        }
    }

    public static DynamicTexture MAP_TEXTURE = null;
    public static RenderType MAP_RENDER_TYPE = null;

    public static final LayeredDraw.Layer GAMEMODE_UI = (stack, partialTicks) -> {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player != null)
        {
            Level world = player.level();
            int screenWidth = minecraft.getWindow().getGuiScaledWidth();
            int screenHeight = minecraft.getWindow().getGuiScaledHeight();
            GameModeRenderer.renderHUD(Minecraft.getInstance(), world, player, minecraft.font, stack, partialTicks, screenWidth, screenHeight);
        }
    };

    public static final LayeredDraw.Layer OVERLAY_UI = (stack, partialTicks) -> {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player != null)
        {
            Level world = player.level();
            PlayerAction capability = cap(player);
            ItemStack item = getHeldStack(player);
            int duration = capability.getFlashDuration();
            int screenWidth = minecraft.getWindow().getGuiScaledWidth();
            int screenHeight = minecraft.getWindow().getGuiScaledHeight();
            if (capability.getFlashDuration() > 0)
            {
                drawTransparent(true);
                RenderSystem.setShaderTexture(0, WHITE);
                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, AnimationFactory.FLASH.lerpF(partialTicks, Math.min(duration / 20.0F, 1.0F)));
                fillScreen(screenWidth, screenHeight);
                drawTransparent(false);
            }
            if (minecraft.options.getCameraType().isFirstPerson())
            {
                if (PlayerAction.getCap(player).isADS())
                {
                    if (isAvailable(player))
                    {
                        AVAItemGun gun = getGun(player);
                        ResourceLocation texture = gun.getScopeType(item, true).getTexture();
                        if (texture != null)
                        {
                            fillCentredMaxWithFilledSidesAndBob(player, stack.pose(), screenWidth, screenHeight, texture);

                            // Inaccuracy
                            float inaccuracy = AnimationFactory.ADS_SHAKE.lerpF(partialTicks, (100.0F - gun.getInitialAccuracy(getHeldStack(player), player, true)) / 10.0F);

                            int width = 8;
                            int height = 2;
                            int x = screenWidth / 2 + screenHeight / 2 + 30;
                            int y = screenHeight / 2;
                            int yOffset = 0;
                            for (int i = 1; i <= 10; i++)
                            {
                                int colour = i < inaccuracy ? AVAConstants.AVA_HUD_ADS_SHAKE_WHITE : AVAConstants.AVA_HUD_ADS_SHAKE_GREY;
                                fill(stack.pose(), x, y + yOffset, width, 1, 0);
                                fill(stack.pose(), x, y - yOffset - 1, width, 1, 0);
                                yOffset += 1;
                                fill(stack.pose(), x, y + yOffset, width, height, colour);
                                fill(stack.pose(), x, y - yOffset - height, width, height, colour);
                                yOffset += height;
                            }

                            stack.drawString(minecraft.font, Component.translatable("ava.hud.ads_shake"), screenWidth / 2 + screenHeight / 2 + 30 + width + 4, screenHeight / 2 - 4, AVAConstants.AVA_HUD_ADS_SHAKE_WHITE, true);
                        }
                    }
                }
            }
            if (player.isAlive() && AVAClientConfig.ENABLE_KILL_TIP.get())
            {
                List<KillTips.KillTip> tips = KILLTIPS.getTips();
                int y = screenHeight / 30;
                for (int i=0;i<7 && i<tips.size();i++)
                {
                    KillTips.KillTip tip = tips.get(i);
                    int x = screenWidth - 5;
                    stack.pose().pushPose();
                    stack.pose().translate(0.0F, 0.0F, -100.0F);
                    stack.pose().popPose();
                    KillTips.KillTipObject target = tip.getTarget();
                    stack.drawString(Minecraft.getInstance().font, target.name.getString(), x -= minecraft.font.width(target.name.getString()), y, target.colour, true);
                    stack.renderFakeItem(new ItemStack(tip.getWeapon()), x -= 20, y - 3);
                    int finalX = x;
                    int finalY = y;
                    tip.getKiller().ifPresent((killer) -> {
                        stack.drawString(Minecraft.getInstance().font, killer.name.getString(), finalX - minecraft.font.width(killer.name.getString()) - 3, finalY, killer.colour, true);
                    });
                    y += 14;
                }
            }
            if (AVAServerConfig.isCompetitiveModeActivated() && player.isAlive() && AVAClientConfig.ENABLE_TAB_HOTKEY.get() && ClientModEventBus.TAB.isPressed() && minecraft.screen == null)
            {
                AVACrossWorldData crossData = AVACrossWorldData.getInstance();
                AVAWorldData data = AVAWorldData.getInstance(world);
                AVAScoreboardManager manager = crossData.scoreboardManager;
                int x = (int) (screenWidth / 2.0F + 40.0F);
                int y = (int) (screenHeight / 2.0F - 100);
                fill(stack.pose(), x, y, 150, 200, AVAConstants.AVA_SCOREBOARD_BG_BLACK.getRGB());
                renderPlayersScoreForTeam(manager, world, manager.getTeamA(world), stack, minecraft, x, y);
                renderPlayersScoreForTeam(manager, world, manager.getTeamB(world), stack, minecraft, x, y + 100);

                if (manager.hasMap())
                {
                    float scale = manager.getScale();
                    float w = manager.getMapWidth() * scale;
                    float h = manager.getMapHeight() * scale;
                    float x2 = screenWidth / 4.0F - w / 2.0F;
                    float y2 = screenHeight / 2.0F - h / 2.0F;
                    float x3 = x2 + w;
                    float y3 = y2 + h;
                    BlockPos from = manager.getFrom();
                    BlockPos to = manager.getTo();
                    Direction direction = manager.getDirection();

                    renderMap(stack, manager.getDirection(), x2, y2, x3, y3);

                    data.teamSpawns.forEach((name, spawns) -> {
                        spawns.forEach((spawn) -> {
                            renderTeamSpawn(stack, world, name, manager.getGamemode() instanceof TeamedMode mode && mode.getTeamA(world).getName().equals(name), spawn, from, to, direction, x2, y2, w, h, scale);
                        });
                    });

                    for (LivingEntity teammate : new ArrayList<>(TEAMMATES))
                        renderTeammate(stack, from, to, direction, teammate, x2, y2, w, h, scale, partialTicks);

                    renderSiteMarks(stack, world.getGameTime(), data.sitesMark.getA(), "1", AVAServerConfig.A_SITE_RADIUS.get(), from, to, manager.getDirection(), x2, y2, w, h, scale, partialTicks);
                    renderSiteMarks(stack, world.getGameTime(), data.sitesMark.getB(), "2", AVAServerConfig.B_SITE_RADIUS.get(), from, to, manager.getDirection(), x2, y2, w, h, scale, partialTicks);
                }
            }
        }
    };

    private static void renderSiteMarks(GuiGraphics stack, long time, BlockPos pos, String name, int radius, BlockPos from, BlockPos to, Direction direction, float oX, float oZ, float width, float height, float scale, float partialTicks)
    {
        if (pos != null && AVAWeaponUtil.isInHorizontalArea(pos.getX(), pos.getZ(), from, to))
        {
            Pair<Double, Double> pair = AVACommonUtil.flip2D(0, 0, (float) (pos.getX() - from.getX()), (float) (pos.getZ() - from.getZ()), width / scale, height / scale, direction);
            float x = (float) (oX + pair.getA() * scale);
            float z = (float) (oZ + pair.getB() * scale);
            float s = (radius == -1 ? 6 : radius) * scale * 2;
            AVAClientUtil.drawTransparent(true);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 0.4F);
            fill(stack.pose(), x - s / 2.0F, z - s / 2.0F, s + scale, s + scale, AVAClientUtil.transparencyColor(AVAConstants.AVA_COMPETITIVE_UI_YELLOW_BG, AnimationFactory.MAP_SITE_AREA_ALPHA.lerpF(partialTicks, Math.min(Math.max(time % 50, 5), 45)) / 50.0F).getRGB());
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            AVAClientUtil.drawTransparent(false);

            stack.pose().pushPose();
            float fScale = 0.35F * scale;
            stack.pose().translate(x, z, 0.0F);
            stack.pose().scale(fScale * 0.95F, fScale, 1.0F);
            stack.pose().translate(-x, -z, 0.0F);
            AVAClientUtil.drawCenteredShadowString(stack, Minecraft.getInstance().font, Component.literal(name), x, z, AVAConstants.AVA_HUD_TEXT_WHITE);
            stack.pose().popPose();
        }
    }

    private static void renderTeamSpawn(GuiGraphics stack, Level world, String teamName, boolean eu, PositionWithRotation spawn, BlockPos from, BlockPos to, Direction direction, float oX, float oZ, float width, float height, float scale)
    {
        Vec3 pos = spawn.getVec();
        PlayerTeam team = world.getScoreboard().getPlayerTeam(teamName);
        if (team != null && AVAWeaponUtil.isInHorizontalArea(pos.x(), pos.z(), from, to))
        {
            ChatFormatting teamColour = team.getColor();
            Pair<Double, Double> pair = AVACommonUtil.flip2D(0, 0, (float) (pos.x - from.getX()), (float) (pos.z - from.getZ()), width / scale, height / scale, direction);
            float x = (float) (oX + pair.getA() * scale);
            float z = (float) (oZ + pair.getB() * scale);
            int radius = spawn.getRadius();
            float s = (radius == -1 ? 6 : radius) * scale;

            if (GameModes.ESCORT.isRunning())
            {
                AVAClientUtil.drawTransparent(true);
                blit(stack.pose(), eu ? ESCORT_SPAWN_EU : ESCORT_SPAWN_NRF, x - s * 5.75F, z - s * 5.75F, x + s * 5.75F, z + s * 5.75F);
                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
                AVAClientUtil.drawTransparent(false);
            }
            else if (teamColour != null)
            {
                Color colour = new Color(teamColour.getColor());
                AVAClientUtil.drawTransparent(true);
                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 0.4F);
                fill(stack.pose(), x - s / 2.0F, z - s / 2.0F, s + scale, s + scale, AVAClientUtil.transparencyColor(colour, 0.25F).getRGB());
                RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
                AVAClientUtil.drawTransparent(false);

                stack.pose().pushPose();
                float fScale = 0.35F * scale;
                stack.pose().translate(x, z, 0.0F);
                stack.pose().scale(fScale * 0.95F, fScale, 1.0F);
                stack.pose().translate(-x, -z, 0.0F);
                AVAClientUtil.drawCenteredShadowString(stack, Minecraft.getInstance().font, team.getDisplayName().copy().withStyle(teamColour), x, z, AVAConstants.AVA_HUD_TEXT_WHITE);
                stack.pose().popPose();
            }
        }
    }

    private static void renderMap(GuiGraphics stack, Direction direction, float x, float y, float x2, float y2)
    {
        if (MAP_TEXTURE != null && MAP_RENDER_TYPE != null)
        {
            int border = 8;
            fillCoord(stack.pose(), x - border, y - border, x2 + border, y2 + border, AVAConstants.AVA_SCOREBOARD_BG_BLACK.getRGB());

            stack.pose().pushPose();

            MultiBufferSource.BufferSource buffer = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
            VertexConsumer consumer = buffer.getBuffer(MAP_RENDER_TYPE);
            Matrix4f matrix4f = stack.pose().last().pose();

            putTextureVertex(consumer, matrix4f, x, y, x2, y2, AVAConstants.VANILLA_FULL_PACKED_LIGHT, 0.8F, direction);
            buffer.endBatch();
            stack.pose().popPose();
        }
    }

    private static void renderTeammate(GuiGraphics stack, BlockPos from, BlockPos to, Direction direction, LivingEntity entity, float oX, float oZ, float width, float height, float scale, float partialTicks)
    {
        Vec3 pos = AVAClientUtil.clientEntityPos(entity, partialTicks);
        if (AVAWeaponUtil.isInHorizontalArea(pos.x, pos.z, from, to))
        {
            float yaw = AVAClientUtil.clientEntityYaw(entity, partialTicks);
            Pair<Double, Double> pair = AVACommonUtil.flip2D(0, 0, (float) (pos.x - from.getX()), (float) (pos.z - from.getZ()), width / scale, height / scale, direction);
            float x = pair.getA().floatValue();
            float z = pair.getB().floatValue();

            float s = 5 * scale / 2.0F;

            stack.pose().pushPose();
            x *= scale;
            z *= scale;

            x += oX;
            z += oZ;
            stack.pose().translate(x, z, 0.0F);
            stack.pose().mulPose(Axis.ZP.rotationDegrees((yaw - direction.toYRot()) % 360));
            stack.pose().translate(-x, -z, 0.0F);
            if (entity instanceof LeopardEntity)
                blit(stack.pose(), LEOPARD, x - s * 2.25F, z - s * 2, x + s * 2.25F, z + s * 2);
            else
                blit(stack.pose(), TEAMMATE, x - s, z - s, x + s, z + s);
            stack.pose().popPose();

            if (entity instanceof Player)
            {
                stack.pose().pushPose();
                float fScale = 0.5F * scale;
                stack.pose().translate(x, z, 0.0F);
                stack.pose().scale(fScale * 0.95F, fScale, 1.0F);
                stack.pose().translate(-x, -z - 10.0F, 0.0F);
                Component name = (entity instanceof Player ? Component.literal(entity.getScoreboardName()) : entity.getDisplayName().copy()).setStyle(Style.EMPTY);
                AVAClientUtil.drawCenteredShadowString(stack, Minecraft.getInstance().font, name, x, z, AVACommonUtil.getColourForName(entity, Minecraft.getInstance().player));
                stack.pose().popPose();
            }
        }
    }

    private static final Map<Integer, Color> COLOUR_MAP = new HashMap<>();
    private static void mapVertex(BufferBuilder builder, Matrix4f matrix4f, int x, int y, int id)
    {
        Color colour = COLOUR_MAP.computeIfAbsent(id, (e) -> new Color(MapColor.byId(id).col));
        int r = colour.getRed();
        int g = colour.getGreen();
        int b = colour.getBlue();
        int a = colour.getAlpha();

        builder.vertex(matrix4f, x, y + 1, 0.0F).color(r, g, b, a).uv(0.0F, 1.0F).endVertex();
        builder.vertex(matrix4f, x + 1, y + 1, 0.0F).color(r, g, b, a).uv(1.0F, 1.0F).endVertex();
        builder.vertex(matrix4f, x + 1, y, 0.0F).color(r, g, b, a).uv(1.0F, 0.0F).endVertex();
        builder.vertex(matrix4f, x, y, 0.0F).color(r, g, b, a).uv(0.0F, 0.0F).endVertex();
    }

    private static void renderPlayersScoreForTeam(AVAScoreboardManager manager, Level world, @Nullable PlayerTeam team, GuiGraphics stack, Minecraft minecraft, int x, int y)
    {
        if (team != null)
        {
            List<Player> players = AVACommonUtil.collect(team.getPlayers(), (name) -> {
                return getPlayerInfo(name).map((info) -> world.getPlayerByUUID(info.getProfile().getId())).orElse(null);
            });
            fill(stack.pose(), x + 5, y + 4, 140, 10, AVAConstants.AVA_SCOREBOARD_TITLE_BLACK.getRGB());
            Font font = minecraft.font;
            stack.drawString(font, team.getDisplayName(), x + 20, y + 5, AVAConstants.AVA_HUD_TEXT_WHITE, true);
            stack.drawString(font, Component.translatable("ava.common.n_players", players.size()), x + 40, y + 5, AVAConstants.AVA_HUD_TEXT_WHITE, true);
            stack.drawString(font, Component.translatable("ava.hud.scoreboard_s_d"), x + 86, y + 5, AVAConstants.AVA_HUD_TEXT_WHITE, true);
            stack.drawString(font, Component.translatable("ava.hud.scoreboard_damage"), x + 110, y + 5, AVAConstants.AVA_HUD_TEXT_WHITE, true);
            stack.drawString(font, Component.translatable("ava.hud.scoreboard_latency"), x + 130, y + 5, AVAConstants.AVA_HUD_TEXT_WHITE, true);
            for (Player player : players)
            {
                AVAPlayerStat stat = manager.getPlayerStat(player);
                y += 14;
                MutableComponent name = (MutableComponent) player.getDisplayName();
                name.withStyle(ChatFormatting.WHITE);
                stack.drawString(font, name, x + 20, y, AVAConstants.AVA_HUD_TEXT_WHITE, true);
                AVAClientUtil.drawCenteredShadowString(stack, font, Component.literal(String.valueOf(stat.getScore())), x + 86, y, AVAConstants.AVA_HUD_TEXT_WHITE);
                AVAClientUtil.drawCenteredShadowString(stack, font, Component.literal(String.valueOf(stat.getDeath())), x + 98, y, AVAConstants.AVA_HUD_TEXT_WHITE);
                AVAClientUtil.drawCenteredShadowString(stack, font, Component.literal(String.valueOf(AVACommonUtil.round(stat.getDamage() / 20.0F, 1))), x + 116, y, AVAConstants.AVA_HUD_TEXT_WHITE);
                int finalY = y;
                getPlayerInfo(player.getUUID()).ifPresent((info) -> {
                    AVAClientUtil.drawCenteredShadowString(stack, font, Component.literal(String.valueOf(info.getLatency())), x + 136, finalY, AVAConstants.AVA_HUD_TEXT_WHITE);
                });
            }
        }
    }

    public static final LayeredDraw.Layer HUD_INDICATORS_UI = (stack, partialTicks) -> {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player != null)
        {
            int screenWidth = minecraft.getWindow().getGuiScaledWidth();
            int screenHeight = minecraft.getWindow().getGuiScaledHeight();
            HUDIndicators.render(minecraft, player, stack, cap(player), screenWidth, screenHeight, partialTicks);
        }
    };

    @SubscribeEvent
    public static void onRenderHUD(RenderGuiLayerEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player == null || !player.isAlive())
            return;
        ItemStack item = getHeldStack(player);
        if (event.getName().equals(VanillaGuiLayers.CROSSHAIR))
        {
            if ((item.getItem() instanceof BinocularItem && PlayerAction.getCap(player).isADS()) || (isAvailable(player) && isFocused() && minecraft.options.getCameraType().isFirstPerson()))
                event.setCanceled(true);
        }
        if (isFocused() && minecraft.options.getCameraType().isFirstPerson())
        {
            if (isAvailable(player) && PlayerAction.getCap(player).isADS())
            {
                ResourceLocation texture = getGun(player).getScopeType(item, true).getTexture();
                if (texture != null)
                {
                    if (CompetitiveUI.RESTRICTED.contains(event.getName()))
                        event.setCanceled(true);
                }
            }
        }
        if (ItemAnimatingGUI.isAnimating())
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void FOVUpdate(ComputeFovModifierEvent event)
    {
        Player player = Minecraft.getInstance().player;
        if (player == null || !player.isAlive())
            return;
        float fov = event.getNewFovModifier();
        ItemStack stack = getHeldStack(player);
        if (isAvailable(player) && (PlayerAction.getCap(player).isADS() || GunStatusManager.INSTANCE.isActive(GunStatusManager.GunStatus.ADS_IN)) && !GunStatusManager.INSTANCE.isActive(GunStatusManager.GunStatus.ADS_OUT))
            event.setNewFovModifier(fov - getGun(player).getScopeType(getHeldStack(player), true).getMagnification(stack));
        if (stack.getItem() instanceof BinocularItem && PlayerAction.getCap(player).isADS())
            event.setNewFovModifier(event.getNewFovModifier() - 0.40F);
    }

    @SubscribeEvent
    public static void onRenderFogColour(ViewportEvent.ComputeFogColor event)
    {
        Player player = Minecraft.getInstance().player;
        if (player == null || !player.isAlive())
            return;
        if (HUDIndicators.NightVision.ACTIVATED)
        {
            float red = event.getRed();
            float green = event.getGreen();
            float blue = event.getBlue();
            float f10 = Math.min(1.0F / red, Math.min(1.0F / green, 1.0F / blue));
            if (Float.isInfinite(f10))
                f10 = Math.nextAfter(f10, 0.0);
            red = red * f10;
            green = green * f10;
            blue = blue * f10;
            event.setRed(red);
            event.setGreen(green);
            event.setBlue(blue);
        }
    }
}
