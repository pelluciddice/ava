package pellucid.ava.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Transformation;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.block.model.BakedQuad;
import net.minecraft.client.renderer.block.model.ItemOverrides;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.util.RandomSource;
import net.neoforged.neoforge.common.util.TransformationHelper;
import org.joml.Vector3f;
import pellucid.ava.client.guis.ItemAnimatingGUI;
import pellucid.ava.client.renderers.models.BakedModelWrapper;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.player.status.ItemStatusManager;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.porting.LazyPorting;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.lang.Math.log10;
import static java.lang.Math.sin;

public abstract class AVABakedModel extends BakedModelWrapper
{
    public AVABakedModel(BakedModel origin)
    {
        super(origin);
    }

    @Override
    public ItemOverrides getOverrides()
    {
        return null;
    }

    @Nullable
    protected static BakedModel getOtherModel(ModelResourceLocation loc)
    {
        if (loc == null)
            return null;
        Minecraft minecraft = Minecraft.getInstance();
        BakedModel model = minecraft.getModelManager().getModel(loc);
        return model == minecraft.getModelManager().getMissingModel() ? null : model;
    }

    public static float getPartialTicks()
    {
        return ItemAnimatingGUI.isAnimating() || !AVAClientUtil.isFocused() ? 1.0F : Minecraft.getInstance().getFrameTime();
    }

    protected List<BakedQuad> get(ModelResourceLocation rel)
    {
        return get(rel, null, (Function<BakedQuad, Boolean>) null);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, boolean empty)
    {
        return get(rel, (Function<BakedQuad, Boolean>) null, empty);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, RandomSource random)
    {
        return get(rel, random, (Function<BakedQuad, Boolean>) null);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, Consumer<BakedQuad> modify)
    {
        return get(rel, null, (q) -> {
            modify.accept(q);
            return true;
        });
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, Function<BakedQuad, Boolean> modify)
    {
        return get(rel, null, modify);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, RandomSource random, Function<BakedQuad, Boolean> modify)
    {
        return get(rel, random, modify, false);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, RandomSource random, Consumer<BakedQuad> modify)
    {
        return get(rel, random, (q) -> {
            modify.accept(q);
            return true;
        }, false);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, Function<BakedQuad, Boolean> modify, boolean empty)
    {
        return get(rel, null, modify, empty);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, Consumer<BakedQuad> modify, boolean empty)
    {
        return get(rel, null, (q) -> {
            modify.accept(q);
            return true;
        }, empty);
    }

    protected List<BakedQuad> get(ModelResourceLocation rel, RandomSource random, Function<BakedQuad, Boolean> modify, boolean empty)
    {
        if (empty)
            return Collections.emptyList();
        BakedModel newModel = Minecraft.getInstance().getModelManager().getModel(rel);
        ArrayList<BakedQuad> origin = new ArrayList<>();
        List<BakedQuad> modelQuads = newModel.getQuads(null, null, random);
        if (modify == null)
            return AVACommonUtil.collect(modelQuads, this::copyQuad);
        for (BakedQuad quad : modelQuads)
        {
            BakedQuad newQuad = copyQuad(quad);
            if (!modify.apply(newQuad))
                break;
            origin.add(newQuad);
        }
        return origin;
    }

    protected BakedQuad copyQuad(BakedQuad quad)
    {
        return new BakedQuad(quad.getVertices().clone(), quad.getTintIndex(), quad.getDirection(), quad.getSprite(), quad.isShade());
    }

    public static void copy(Perspective from, Vector3f rotation, Vector3f translation, Vector3f scale)
    {
        if (from == null)
            return;
        rotation.set(from.getRotation());
        translation.set(from.getTranslation());
        scale.set(from.getScale());
    }

    protected Transformation getTransformationMatrix(@Nullable Vector3f rotation, @Nullable Vector3f translation, @Nullable Vector3f scale)
    {
        if (translation != null)
        {
            translation.mul(0.0625F);
            LazyPorting.clampVector3f(translation, -5.0F, 5.0F);
        }
        if (scale != null)
            LazyPorting.clampVector3f(scale, -4.0F, 4.0F);
        return new Transformation(translation, TransformationHelper.quatFromXYZ(rotation, true), scale, null);
    }

    protected BakedModel push(Vector3f rotation, Vector3f translation, Vector3f scale, PoseStack mat)
    {
        Transformation transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
        {
            Vector3f trans = transformationMatrix.getTranslation();
            mat.translate(trans.x(), trans.y(), trans.z());

            mat.mulPose(transformationMatrix.getLeftRotation());

            Vector3f scale2 = transformationMatrix.getScale();
            mat.scale(scale2.x(), scale2.y(), scale2.z());

            mat.mulPose(transformationMatrix.getRightRotation());
        }
        return this;
    }
}
