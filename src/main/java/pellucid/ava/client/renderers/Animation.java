package pellucid.ava.client.renderers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.IJsonSerializable;

public class Animation implements IJsonSerializable<Animation>
{
    public int targetTicks;
    public Perspective target3f;

    public static Animation of(JsonElement json)
    {
        return IJsonSerializable.create(new Animation(), json);
    }
    public static Animation of(Animation animation)
    {
        return new Animation(animation);
    }
    public static Animation of(int targetTicks, Perspective target3f)
    {
        return new Animation(targetTicks, target3f);
    }

    private Animation()
    {
        this(-1, new Perspective());
    }

    public Animation(Animation animation)
    {
        this(animation.targetTicks, animation.target3f.copy());
    }

    private Animation(int targetTicks, Perspective target3f)
    {
        this.targetTicks = targetTicks;
        this.target3f = target3f;
    }

    public Perspective getPerspectiveInBetween(Animation nextAnimation, int currentTicks)
    {
        if (currentTicks < targetTicks)
            return target3f;
        Perspective next3f = nextAnimation.target3f;
        int ticksDiff = nextAnimation.targetTicks - this.targetTicks;
        if (ticksDiff == 0)
            return nextAnimation.target3f;
        float f = (currentTicks - targetTicks) / (float) (ticksDiff);
        return new Perspective(AVACommonUtil.rotLerpVector3f(target3f.rotation, next3f.rotation, f), AVACommonUtil.lerpVector3f(target3f.translation, next3f.translation, f), AVACommonUtil.lerpVector3f(target3f.scale, next3f.scale, f));
    }

    @Override
    public String toString()
    {
        return "Animation{" +
                "targetTicks=" + targetTicks +
                ", target3f=" + target3f +
                '}';
    }

    public String toCode()
    {
        return "Animation.of(" + targetTicks + ", " + target3f.toCode() + ")";
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        JsonObject obj = new JsonObject();
        obj.addProperty("targetTicks", targetTicks);
        target3f.writeToJson(obj);
        json.add("animation", obj);
    }

    @Override
    public void readFromJson(JsonElement json)
    {
        JsonObject obj = json.getAsJsonObject().get("animation").getAsJsonObject();
        targetTicks = obj.get("targetTicks").getAsInt();
        target3f.readFromJson(obj);
    }
}
