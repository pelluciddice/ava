package pellucid.ava.client.renderers;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;
import pellucid.ava.AVA;
import pellucid.ava.entities.objects.kits.renderers.KitModel;
import pellucid.ava.entities.objects.leopard.LeopardModel;
import pellucid.ava.entities.objects.parachute.renderers.ParachuteModel;
import pellucid.ava.entities.robots.renderers.BlueRobotModel;
import pellucid.ava.entities.robots.renderers.DarkBlueRobotModel;
import pellucid.ava.entities.robots.renderers.YellowRobotModel;
import pellucid.ava.entities.shootables.renderers.M202RocketModel;
import pellucid.ava.entities.throwables.renderers.GM94GrenadeModel;
import pellucid.ava.items.armours.models.StandardEUArmourModel;
import pellucid.ava.items.armours.models.StandardNRFArmourModel;

public class AVAModelLayers
{
    public static final ModelLayerLocation HUMANOID = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "humanoid_model"), "main");

    public static final ModelLayerLocation LEOPARD = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "leopard"), "main");

    public static final ModelLayerLocation STANDARD_EU_ARMOUR = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "standard_eu_armour_model"), "main");
    public static final ModelLayerLocation STANDARD_NRF_ARMOUR = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "standard_nrf_armour_model"), "main");

    public static final ModelLayerLocation PARACHUTE = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "parachute_model"), "main");

    public static final ModelLayerLocation BLUE_ROBOT = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "blue_robot_model"), "main");
    public static final ModelLayerLocation YELLOW_ROBOT = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "yellow_robot_model"), "main");
    public static final ModelLayerLocation DARK_BLUE_ROBOT = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "dark_blue_robot_model"), "main");

    public static final ModelLayerLocation ROCKET = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "rocket_model"), "main");
    public static final ModelLayerLocation GM94_GRENADE = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "gm94_grenade_model"), "main");

    public static final ModelLayerLocation KIT = new ModelLayerLocation(new ResourceLocation(AVA.MODID, "kit_model"), "main");

    public static void registerAll(EntityRenderersEvent.RegisterLayerDefinitions event)
    {
        event.registerLayerDefinition(HUMANOID, () -> LayerDefinition.create(HumanoidModel.createMesh(new CubeDeformation(0.25F), 0.0F), 64, 32));

        event.registerLayerDefinition(LEOPARD, LeopardModel::createBodyLayer);

        event.registerLayerDefinition(STANDARD_EU_ARMOUR, StandardEUArmourModel::createBodyLayer);
        event.registerLayerDefinition(STANDARD_NRF_ARMOUR, StandardNRFArmourModel::createBodyLayer);

        event.registerLayerDefinition(PARACHUTE, ParachuteModel::createBodyLayer);

        event.registerLayerDefinition(BLUE_ROBOT, BlueRobotModel::createBodyLayer);
        event.registerLayerDefinition(YELLOW_ROBOT, YellowRobotModel::createBodyLayer);
        event.registerLayerDefinition(DARK_BLUE_ROBOT, DarkBlueRobotModel::createBodyLayer);

        event.registerLayerDefinition(ROCKET, M202RocketModel::createBodyLayer);
        event.registerLayerDefinition(GM94_GRENADE, GM94GrenadeModel::createBodyLayer);

        event.registerLayerDefinition(KIT, KitModel::createBodyLayer);
    }
}
