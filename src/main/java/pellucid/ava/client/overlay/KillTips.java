package pellucid.ava.client.overlay;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import pellucid.ava.util.AVACommonUtil;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class KillTips
{
    public final List<KillTip> tips = new ArrayList<>();

    public KillTips()
    {

    }

    public void add(KillTip tip)
    {
        this.tips.add(tip);
    }

    public ArrayList<KillTip> getTips()
    {
        return new ArrayList<>(tips);
    }

    public void clear()
    {
        tips.clear();
    }

    public void remove()
    {
        if (!tips.isEmpty())
            tips.remove(0);
    }

    public void tick()
    {
        boolean pop = false;
        for (int i = 0; i < 7 && i < tips.size(); i++)
        {
            KillTip tip = tips.get(i);
            if (!tip.displaying())
                tip.startDisplay();
            tip.tick();
            if (tip.shouldBeDead() && i == 0)
                pop = true;
        }
        if (pop)
            tips.remove(0);
    }

    public static class KillTip
    {
        private final KillTipObject killer;
        private final Item weapon;
        private final KillTipObject target;
        private int lives = -1;

        public KillTip(@Nullable LivingEntity killer, @Nullable Item weapon, LivingEntity target, LivingEntity local)
        {
            this.killer = killer == null ? null : new KillTipObject(killer, local);
            this.weapon = (weapon == null || weapon == Items.AIR) ? Items.SKELETON_SKULL : weapon;
            this.target = new KillTipObject(target, local);
        }

        public Optional<KillTipObject> getKiller()
        {
            return killer == null ? Optional.empty() : Optional.of(killer);
        }

        public Item getWeapon()
        {
            return this.weapon;
        }

        public KillTipObject getTarget()
        {
            return target;
        }

        public boolean displaying()
        {
            return lives != -1;
        }

        public void startDisplay()
        {
            lives = 100;
        }

        public void tick()
        {
            if (lives > 0)
                lives--;
        }

        public boolean shouldBeDead()
        {
            return lives == 0;
        }
    }

    public static class KillTipObject
    {
        public final int colour;
        public final Component name;

        public KillTipObject(LivingEntity killer, LivingEntity local)
        {
            this(AVACommonUtil.getColourForName(killer, local), killer.getDisplayName());
        }

        public KillTipObject(int colour, Component name)
        {
            this.colour = colour;
            this.name = name;
        }
    }}
