package pellucid.ava.client.overlay;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.phys.Vec3;

import java.util.ArrayList;

public class HurtInfo
{
    private final ArrayList<Info> info = new ArrayList<>();

    public void add(Info info)
    {
        this.info.add(info);
    }

    public ArrayList<Info> getInfo()
    {
        return info;
    }

    public void update()
    {
        info.removeIf(info -> --info.ticksLeft <= 0);
    }

    public static class Info
    {
        public final Vec3 attackerPos;
        public int ticksLeft = 60;

        public Info(Entity attacker)
        {
            this(attacker.position());
        }

        public Info(Vec3 attackerPos)
        {
            this.attackerPos = attackerPos;
        }

        public boolean shouldBeDead()
        {
            return ticksLeft <= 0;
        }
    }
}
