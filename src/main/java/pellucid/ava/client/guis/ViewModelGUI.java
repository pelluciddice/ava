package pellucid.ava.client.guis;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.AVA;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.util.AVAClientUtil;

import java.util.Arrays;

public class ViewModelGUI extends Screen
{
    private static final ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/gui/view_model.png");
    private static final int TEXTURE_WINDOW_WIDTH = 229;
    private static final int TEXTURE_WINDOW_HEIGHT = 218;
    private int colour = BackgroundColours.BLACK.colour;
    private final ItemStack itemModel;
    protected int guiLeft;
    protected int guiTop;

    public ViewModelGUI(ItemStack stack)
    {
        super(Component.empty());
        this.itemModel = stack;
    }

    @Override
    protected void init()
    {
        guiLeft = (width - TEXTURE_WINDOW_WIDTH) / 2;
        guiTop = (height - TEXTURE_WINDOW_HEIGHT) / 2;

        for (int i = 0; i < BackgroundColours.values().length; i++)
            addRenderableWidget(new BackgroundColourButton(guiLeft + 90 + i * 5, guiTop + 7, i));
//        if (minecraft != null)
//            minecraft.keyboardHandler.setSendRepeatsToGui(true);
    }

    private double lastMouseXL = 0.0D;
    private double lastMouseYL = 0.0D;
    private boolean lastInFieldL = false;

    private double lastMouseXR = 0.0D;
    private double lastMouseYR = 0.0D;
    private boolean lastInFieldR = false;

    @Override
    public void mouseMoved(double mouseX, double mouseY)
    {
        if (minecraft == null)
            return;
        boolean inField = mouseInField(mouseX, mouseY) && AVAClientUtil.leftMouseDown();
        if (inField)
        {
            if (lastInFieldL)
            {
                double x = mouseX - lastMouseXL;
                double y = mouseY - lastMouseYL;
                float sensitivity = 0.5F;
                perspective.rotation.add((float) -y / sensitivity, (float) x / sensitivity / 2.0F, 0.0F);
            }
            lastMouseXL = mouseX;
            lastMouseYL = mouseY;
        }
        lastInFieldL = inField;

        inField = mouseInField(mouseX, mouseY) && AVAClientUtil.rightMouseDown();
        if (inField)
        {
            if (lastInFieldR)
            {
                double x = mouseX - lastMouseXR;
                double y = mouseY - lastMouseYR;
                float sensitivity = 0.08F;
                perspective.translation.add((float) x / sensitivity, (float) y / sensitivity, 0.0F);
            }
            lastMouseXR = mouseX;
            lastMouseYR = mouseY;
        }
        lastInFieldR = inField;
    }

    @Override
    public void renderBackground(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        if (minecraft == null)
            return;
        super.renderBackground(stack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        int x = (width - TEXTURE_WINDOW_WIDTH) / 2;
        int y = (height - TEXTURE_WINDOW_HEIGHT) / 2;
        stack.blit(TEXTURE, x, y, 0, 0, TEXTURE_WINDOW_WIDTH, TEXTURE_WINDOW_HEIGHT);


        stack.pose().pushPose();
        stack.pose().translate(guiLeft, guiTop, 0.0F);
        stack.fill(10, 7, 219, 210, colour);
        stack.pose().popPose();


        stack.blit(TEXTURE, guiLeft + 90, guiTop + 7, 0, 219, 50, 5);
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double deltaX, double deltaY)
    {
        if (mouseInField(mouseX, mouseY))
        {
            float scale = (float) Math.max(0.0F, Math.min(40, perspective.scale.x() + Math.max(deltaX, deltaY) / 2.0F));
            perspective.scale.set(scale, scale, perspective.scale.z());
        }
        return true;
    }

    protected final Perspective perspective = Perspective.scale(16.0F);
    @Override
    public void render(GuiGraphics stack, int mouseX, int mouseY, float partialTicks)
    {
        if (minecraft == null)
            return;

        stack.pose().pushPose();
        stack.pose().translate(0.0F, 0.0F, -100.0F);
        renderBackground(stack, mouseX, mouseY, partialTicks);
        super.render(stack, mouseX, mouseY, partialTicks);
        stack.pose().popPose();

        AVAClientUtil.renderItemStack(stack, perspective, itemModel, guiLeft + TEXTURE_WINDOW_WIDTH / 2.0F, guiTop + TEXTURE_WINDOW_HEIGHT / 2.0F, false);
    }

    private boolean mouseInField(double x, double y)
    {
        return x > guiLeft + 10 && x <= guiLeft + 219 && y > guiTop + 7 && y <= guiTop + 210;
    }

    class BackgroundColourButton extends Button
    {
        private final int index;
        public BackgroundColourButton(int x, int y, int index)
        {
            super(x, y, 5, 5, Component.empty(), (button) -> {}, (msg) -> {
                return Component.empty();
            });
            this.index = index;
        }

        @Override
        public void onPress()
        {
            ViewModelGUI.this.colour = BackgroundColours.fromIndex(index).colour;
        }

        @Override
        public void renderWidget(GuiGraphics stack, int p_93747_, int p_93748_, float p_93749_)
        {
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            stack.blit(TEXTURE, getX(), getY(), 5 * index, 224, width, height);
        }
    }

    enum BackgroundColours
    {
        BLACK(0, 0xFF000000),
        WHITE(1, 0xFFFFFFFF),
        RED(2, 0xFFFF0000),
        YELLOW(3, 0xFFF4FF00),
        BLUE(4, 0xFF00DEFF),
        GREY(5, 0xFFD1D1D1),
        PURPLE(6, 0xFFA2A6CF),
        GREEN(7, 0xFFBFCFA2),
        ORANGE(8, 0xFFCFBCA2),
        LIGHT_BLUE(9, 0xFFA2C9CF);
        private final int index;
        public final int colour;
        BackgroundColours(int index, int colour)
        {
            this.index = index;
            this.colour = colour;
        }

        public static BackgroundColours fromIndex(int index)
        {
            return Arrays.stream(values()).filter((colour) -> colour.index == index).findFirst().orElse(BLACK);
        }
    }
}
