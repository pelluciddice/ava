package pellucid.ava.client.guis;

import com.google.common.collect.ImmutableList;
import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.melee.MeleeModelVariables;
import pellucid.ava.client.renderers.models.melee.MeleeSubModels;
import pellucid.ava.client.renderers.models.melee.RegularMeleeModelProperty;
import pellucid.ava.events.data.custom.models.items.MeleeModelResourcesManager;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.player.status.MeleeStatusManager;

import java.util.List;

public class MeleeAnimatingGUI extends ItemAnimatingGUI<AVAMeleeItem, MeleeModelVariables, MeleeSubModels, RegularMeleeModelProperty, MeleeStatusManager, MeleeModelResourcesManager>
{
    public MeleeAnimatingGUI()
    {
        super(AVAModelTypes.MELEES);
    }

    @Override
    protected void print(IMode<RegularMeleeModelProperty> mode, Perspective o, Perspective l, Perspective r)
    {
        if (mode == IDLE)
        {
            System.out.println(o.toCode() + ",");
            System.out.println(property.handsIdle.getA().toCode() + ",");
            System.out.println(property.handsIdle.getB().toCode() + ",");
        }
        if (mode == RUN)
        {
            System.out.println(".run(" + property.run.getA().toCode() + ")");
            System.out.println(".runLeft(" + property.run.getB().getA().toCode() + ")");
            System.out.println(".runRight(" + property.run.getB().getB().toCode() + ")");
        }
        if (mode == DRAW)
        {
            System.out.println(property.draw.getA().toCode("draw", o, l, r));
            System.out.println(property.draw.getB().getA().toCode("drawLeft", o, l, r));
            System.out.println(property.draw.getB().getB().toCode("drawRight", o, l, r));
        }
        if (mode == ATTACK_LIGHT)
        {
            System.out.println(property.attackLight.getA().toCode("attackLight", o, l, r));
            System.out.println(property.attackLight.getB().getA().toCode("attackLightLeft", o, l, r));
            System.out.println(property.attackLight.getB().getB().toCode("attackLightRight", o, l, r));
        }
        if (mode == ATTACK_HEAVY)
        {
            System.out.println(property.attackHeavy.getA().toCode("attackHeavy", o, l, r));
            System.out.println(property.attackHeavy.getB().getA().toCode("attackHeavyLeft", o, l, r));
            System.out.println(property.attackHeavy.getB().getB().toCode("attackHeavyRight", o, l, r));
        }
    }

    public static final ModeImpl<MeleeModelVariables, RegularMeleeModelProperty> IDLE = new ModeImpl<>(MeleeModelVariables.IDLE, (p) -> perspectiveToAnimations(p.display.getOrDefault(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new Perspective())), (p) -> perspectiveToAnimations(p.handsIdle.getA()), (p) -> perspectiveToAnimations(p.handsIdle.getB()), (p, a) -> p.display2(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, a.get(0).target3f), (p, a) -> p.leftHandsIdle(a.get(0).target3f), (p, a) -> p.rightHandsIdle(a.get(0).target3f));
    public static final ModeImpl<MeleeModelVariables, RegularMeleeModelProperty> RUN = new ModeImpl<>(MeleeModelVariables.RUN, (p) -> perspectiveToAnimations(p.run.getA()), (p) -> perspectiveToAnimations(p.run.getB().getA()), (p) -> perspectiveToAnimations(p.run.getB().getB()), (p, a) -> p.run.setA(a.get(0).target3f), (p, a) -> p.run.getB().setA(a.get(0).target3f), (p, a) -> p.run.getB().setB(a.get(0).target3f));
    public static final ModeImpl<MeleeModelVariables, RegularMeleeModelProperty> DRAW = new ModeImpl<>(MeleeModelVariables.DRAW, (p) -> p.draw);
    public static final ModeImpl<MeleeModelVariables, RegularMeleeModelProperty> ATTACK_LIGHT = new ModeImpl<>(MeleeModelVariables.ATTACK_LIGHT, (p) -> p.attackLight);
    public static final ModeImpl<MeleeModelVariables, RegularMeleeModelProperty> ATTACK_HEAVY = new ModeImpl<>(MeleeModelVariables.ATTACK_HEAVY, (p) -> p.attackHeavy);

    @Override
    protected List<IMode<RegularMeleeModelProperty>> getModes()
    {
        return ImmutableList.of(IDLE, RUN, ATTACK_LIGHT, ATTACK_HEAVY, DRAW);
    }
}
