package pellucid.ava.client.guis;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.entity.player.PlayerRenderer;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.RenderHandEvent;
import net.neoforged.neoforge.client.gui.widget.ExtendedSlider;
import pellucid.ava.AVA;
import pellucid.ava.client.animation.AVAFPRenderer;
import pellucid.ava.client.renderers.Animation;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.IModelVariables;
import pellucid.ava.client.renderers.models.ISubModels;
import pellucid.ava.client.renderers.models.RegularModelProperty;
import pellucid.ava.events.data.custom.models.ItemModelResourcesManager;
import pellucid.ava.player.status.ItemStatusManager;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static org.lwjgl.glfw.GLFW.*;

@EventBusSubscriber(Dist.CLIENT)
public abstract class ItemAnimatingGUI<I extends Item, V extends IModelVariables, S extends ISubModels<I>, P extends RegularModelProperty<I, V, S, P>, SM extends ItemStatusManager<I>, MM extends ItemModelResourcesManager<I, S, P>> extends Screen
{
    private final AVAModelTypes<I, V, S, P, SM, MM> type;
    public P property;

    public Item item;

    private EditBox boxItem;
    private EditBox boxMaxTicks;
    private EditBox boxModel;
    private EditBox boxframe2Ticks;
    public ExtendedSlider slider;
    // IDLE, RUN, PRE_RELOAD, RELOAD, POST_RELOAD, DRAW, FIRE, INSTALL_SILENCER, AIM
    private final Button[] modeButtons;
    private final Button[] transformModeButtons;
    private final Button[] objectModeButtons;

    private S subModelCache = null;
    private int frameCache = 0;

    public ItemAnimatingGUI(AVAModelTypes<I, V, S, P, SM, MM> type)
    {
        super(Component.empty());
        this.type = type;
        modeButtons = new Button[type.animatableVariables.size()];
        transformModeButtons = new Button[3];
        objectModeButtons = new Button[4];
    }

    public static Perspective ANIMATING_PERSPECTIVE = new Perspective();
    public static boolean isAnimating()
    {
        return Minecraft.getInstance().screen instanceof ItemAnimatingGUI<?, ?, ?, ?, ?, ?> && ((ItemAnimatingGUI<?, ?, ?, ?, ?, ?>) Minecraft.getInstance().screen).valid();
    }

    protected void trySetItem(Item item)
    {
        if (type.isRightItem(item) && this.item != item)
        {
            this.item = item;
            this.property = type.getModelManager().type.getModel((I) item);
            boxItem.setFocused(false);
            this.modeButtons[0].onPress();
        }
    }

    @Override
    protected void init()
    {
        boxItem = addRenderableWidget(new EditBox(font, 10, 10, 70, 10, Component.empty()));
        boxItem.setResponder((text) -> {
            if (!text.contains(":"))
                text = AVA.MODID + ":" + text;
            ResourceLocation rel = ResourceLocation.tryParse(text);
            Item item = BuiltInRegistries.ITEM.get(rel);
            trySetItem(item);
        });
        setInitialFocus(boxItem);
        boxMaxTicks = addRenderableWidget(new EditBox(font, width - 30, 70, 20, 10, Component.empty()));
        boxMaxTicks.setResponder((text) -> {
            try
            {
                int maxTicks = Integer.parseInt(text);
                updateSlider(maxTicks - 1);
            }
            catch (Exception ignore)
            {

            }
        });
        int x = 44;
        int y = 16;
        for (IMode<P> mode : getModes())
        {
            modeButton(mode, x += 38 , y);
            printButton(mode, x + 3, y - 12);
        }
        int x2 = 150;
        int y2 = 70;
        for (Transform transform : Transform.values())
        {
            transformModeButton(transform, x2 += 60, y2);
            tranformResetButton(transform, x2 + 46, y2 + 1);
        }
        int x3 = 140;
        for (Object obj : Object.values())
        {
            int i = obj.ordinal();
            objectModeButton(obj, x3 += 14, y2);
            setToIdleButton(obj, 10 + 34 * i, y2 + 12);
            setToClearButton(obj, 10 + 34 * i, y2 + 12 * 2);
            copyButton(obj, 10 + 34 * i, y2 + 12 * 3);
            cutButton(obj, 10 + 34 * i, y2 + 12 * 4);
            pasteButton(obj, 10 + 34 * i, y2 + 12 * 5);
        }
        copyAllButton(10, 70 + 12 * 6);
        cutAllButton(10, 70 + 12 * 7);
        pasteAllButton(10, 70 + 12 * 8);

        boxModel = addRenderableWidget(new EditBox(font, 10, 70 + 12 * 9, 40, 10, Component.empty()));
        boxModel.setResponder((text) -> {
            try
            {
                subModelCache = type.getSubModel(text.toUpperCase(Locale.ROOT));
                boxModel.setFocused(false);
            }
            catch (Exception ignored)
            {

            }
        });
        boxframe2Ticks = addRenderableWidget(new EditBox(font, 10, 70 + 12 * 10, 40, 10, Component.empty()));
        boxframe2Ticks.setResponder((text) -> {
            try
            {
                frameCache = Math.round(Integer.parseInt(text) / 30.0F * 20.0F);
            }
            catch (Exception ignored)
            {

            }
        });
        addRenderableWidget(Button.builder(Component.literal("@"), (b) -> {
//            Minecraft.getInstance().reloadResourcePacks();
            type.getModelManager().generate();
            property = type.getModel((I) item);
        }).bounds(width - 12, 2, 10, 10).build());
        trySetItem(Minecraft.getInstance().player.getMainHandItem().getItem());
    }

    public void animateTick()
    {
        if (boxItem.isFocused() || boxMaxTicks.isFocused() || boxModel.isFocused() || boxframe2Ticks.isFocused())
            return;
        boolean shift = Screen.hasShiftDown();
        boolean w = AVAClientUtil.keyDown(GLFW_KEY_W);
        boolean a = AVAClientUtil.keyDown(GLFW_KEY_A);
        boolean s = AVAClientUtil.keyDown(GLFW_KEY_S);
        boolean d = AVAClientUtil.keyDown(GLFW_KEY_D);
        boolean q = AVAClientUtil.keyDown(GLFW_KEY_Q);
        boolean e = AVAClientUtil.keyDown(GLFW_KEY_E);
        Perspective change = Perspective.EMPTY.copy();
        Perspective changeHand = Perspective.EMPTY.copy();
        float amount;
        Transform t = getTransformMode();
        if (t == Transform.ROTATION)
        {
            amount = 1.0F;
            if (w)
                changeHand.rotation.add(-amount, 0.0F, 0.0F);
            if (s)
                changeHand.rotation.add(amount, 0.0F, 0.0F);
            if (a)
                changeHand.rotation.add(0.0F, 0.0F, -amount);
            if (d)
                changeHand.rotation.add(0.0F, 0.0F, amount);
            if (q)
                changeHand.rotation.add(0.0F, -amount, 0.0F);
            if (e)
                changeHand.rotation.add(0.0F, amount, 0.0F);
            if (w)
                change.rotation.add(amount, 0.0F, 0.0F);
            if (s)
                change.rotation.add(-amount, 0.0F, 0.0F);
            if (a)
                change.rotation.add(0.0F, amount, 0.0F);
            if (d)
                change.rotation.add(0.0F, -amount, 0.0F);
            if (q)
                change.rotation.add(0.0F, 0.0F, amount);
            if (e)
                change.rotation.add(0.0F, 0.0F, -amount);
        }
        if (t == Transform.TRANSLATION)
        {
            amount = 0.025F;
            if (shift)
            {
                if (w)
                    change.translation.add(0.0F, 0.0F, -amount);
                if (s)
                    change.translation.add(0.0F, 0.0F, amount);
            }
            else
            {
                if (w)
                    change.translation.add(0.0F, amount, 0.0F);
                if (s)
                    change.translation.add(0.0F, -amount, 0.0F);
                if (a)
                    change.translation.add(-amount, 0.0F, 0.0F);
                if (d)
                    change.translation.add(amount, 0.0F, 0.0F);
            }
            changeHand.translation.set(-change.translation.x, -change.translation.z, -change.translation.y);
        }
        if (t == Transform.SCALE)
        {
            amount = 0.025F;
            if (q)
                changeHand.scale.add(amount, 0.0F, 0.0F);
            if (w)
                changeHand.scale.add(0.0F, amount, 0.0F);
            if (e)
                changeHand.scale.add(0.0F, 0.0F, amount);
            if (a)
                changeHand.scale.add(-amount, 0.0F, 0.0F);
            if (s)
                changeHand.scale.add(0.0F, -amount, 0.0F);
            if (d)
                changeHand.scale.add(0.0F, 0.0F, -amount);
            if (q)
                change.scale.add(amount, 0.0F, 0.0F);
            if (w)
                change.scale.add(0.0F, 0.0F, amount);
            if (e)
                change.scale.add(0.0F, amount, 0.0F);
            if (a)
                change.scale.add(-amount, 0.0F, 0.0F);
            if (s)
                change.scale.add(0.0F, 0.0F, -amount);
            if (d)
                change.scale.add(0.0F, -amount, 0.0F);
        }
        ANIMATING_PERSPECTIVE = mode.oRead(property).isEmpty() ? Perspective.EMPTY : AnimationFactory.getPerspectiveInBetween(mode.oRead(property), slider.getValueInt());
        if (!change.isEmpty() && !changeHand.isEmpty())
        {
            if (AVAClientUtil.keyDown(GLFW_KEY_LEFT_CONTROL))
            {
                change = change.mul(0.1F);
                changeHand = changeHand.mul(0.1F);
            }
            else if (AVAClientUtil.keyDown(GLFW_KEY_LEFT_ALT))
            {
                change = change.mul(10.0F);
                changeHand = changeHand.mul(10.0F);
            }
            change.round(3);
            changeHand.round(3);
            int i = slider.getValueInt();
            if (objModeCache == Object.O)
                mode.oWrite(property, mode.oRead(property).append(Animation.of(i, AVACommonUtil.safePresentOr(() -> mode.oRead(property).get(i).target3f, Perspective::new).add(change))));
            if (objModeCache == Object.L)
                mode.lWrite(property, mode.lRead(property).append(Animation.of(i, AVACommonUtil.safePresentOr(() -> mode.lRead(property).get(i).target3f, Perspective::new).add(changeHand))));
            if (objModeCache == Object.R)
                mode.rWrite(property, mode.rRead(property).append(Animation.of(i, AVACommonUtil.safePresentOr(() -> mode.rRead(property).get(i).target3f, Perspective::new).add(changeHand))));
            if (objModeCache == Object.M)
                setSubModelAnim(getSubModelAnim().append(Animation.of(i, AVACommonUtil.safePresentOr(() -> getSubModelAnim().get(i).target3f, Perspective::new).add(change))));
        }
    }

    protected Animations getSubModelAnim()
    {
        return Animations.of(property.quadAnimators2.computeIfAbsent(subModelCache, (s2) -> new HashMap<>()).getOrDefault(mode.getVariable(), Animations.of()));
    }

    protected void setSubModelAnim(Animations animation)
    {
        property.quadAnimators2.computeIfAbsent(subModelCache, (s2) -> new HashMap<>()).put((V) mode.getVariable(), animation);
    }

    private int maxTicksCache = 1;
    protected void updateSlider(int maxTicks)
    {
        maxTicks++;
        if (slider != null)
            removeWidget(slider);
        slider = addRenderableWidget(new ExtendedSlider(10, 30, width - 20, 10, Component.empty(), Component.empty(), 0, maxTicks, 0, true));
        maxTicksCache = maxTicks;
    }

    @Override
    public void renderBackground(GuiGraphics p_283688_, int p_299421_, int p_298679_, float p_297268_)
    {

    }

    @Override
    public void render(GuiGraphics graphic, int mouseX, int mouseY, float partialTicks)
    {
        super.render(graphic, mouseX, mouseY, partialTicks);
        int size = 2;
        AVAClientUtil.fill(graphic.pose(), width / 2.0F - size / 2.0F, height / 2.0F - size / 2.0F, size, size, AVAConstants.AVA_HUD_TEXT_RED);
        graphic.drawString(font, "O", 2, 40, AVAConstants.AVA_HUD_TEXT_ORANGE, true);
        graphic.drawString(font, "L", 2, 50, AVAConstants.AVA_HUD_TEXT_ORANGE, true);
        graphic.drawString(font, "R", 2, 60, AVAConstants.AVA_HUD_TEXT_ORANGE, true);

        if (valid())
        {
            float split = (width - 20 - 10.0F) / maxTicksCache;
            BiConsumer<Integer, Integer> labeler = (value, y) -> AVAClientUtil.drawCenteredShadowString(graphic, font, Component.literal(String.valueOf(value)), 10 + value * split + 5, y, AVAConstants.AVA_HUD_TEXT_ORANGE);
            for (Animation a : mode.oRead(property).getUnpopulatedAnimations())
                labeler.accept(a.targetTicks, 40);
            for (Animation a : mode.lRead(property).getUnpopulatedAnimations())
                labeler.accept(a.targetTicks, 50);
            for (Animation a : mode.rRead(property).getUnpopulatedAnimations())
                labeler.accept(a.targetTicks, 60);

            graphic.pose().pushPose();
            float f = 4.0F;
            graphic.pose().scale(f, f, f);
            AVAClientUtil.drawCenteredShadowString(graphic, font, Component.literal("^"), (98 + 38 * getModes().indexOf(mode)) / f, 30 / f, AVAConstants.AVA_HUD_TEXT_YELLOW);
            AVAClientUtil.drawCenteredShadowString(graphic, font, Component.literal("^"), (161 + 14 * objModeCache.ordinal()) / f, 81 / f, AVAConstants.AVA_HUD_TEXT_YELLOW);
            AVAClientUtil.drawCenteredShadowString(graphic, font, Component.literal("^"), (234 + 60 * transformModeCache.ordinal()) / f, 81 / f, AVAConstants.AVA_HUD_TEXT_YELLOW);
            graphic.pose().popPose();
        }

        graphic.drawString(font, String.valueOf(frameCache), 54, 70 + 12 * 10, AVAConstants.AVA_HUD_TEXT_ORANGE);
    }

    @SubscribeEvent
    public static void onHandRender(RenderHandEvent event)
    {
        LocalPlayer player = Minecraft.getInstance().player;
        if (event.getHand() == InteractionHand.OFF_HAND)
            return;
        if (player != null && player.isAlive() && isAnimating())
        {
            ItemAnimatingGUI gui = (ItemAnimatingGUI<?, ?, ?, ?, ?, ?>) Minecraft.getInstance().screen;
            event.setCanceled(true);
            PlayerRenderer renderer = (PlayerRenderer) Minecraft.getInstance().getEntityRenderDispatcher().getRenderer(player);
            PoseStack stack = event.getPoseStack();
            renderer.getModel().rightArmPose = HumanoidModel.ArmPose.EMPTY;

            AVAFPRenderer.renderItem(player, Minecraft.getInstance().gameRenderer.itemInHandRenderer, HumanoidArm.RIGHT, stack, event.getMultiBufferSource(), AVAConstants.VANILLA_FULL_PACKED_LIGHT, new ItemStack(gui.item));

            stack.pushPose();
            stack.translate(0.0D, -0.46F, -0.32F);
            stack.mulPose(Axis.XP.rotationDegrees(-85.0F));
            stack.mulPose(Axis.YP.rotationDegrees(180.0F));
            AnimationFactory.renderHand(stack, event.getMultiBufferSource(), Minecraft.getInstance().player, gui.mode.lRead(gui.property), gui.slider.getValueInt(), 1.0F, Pair.of(AVAConstants.VANILLA_FULL_PACKED_LIGHT, AVAConstants.VANILLA_FULL_PACKED_LIGHT), true);
            AnimationFactory.renderHand(stack, event.getMultiBufferSource(), Minecraft.getInstance().player, gui.mode.rRead(gui.property), gui.slider.getValueInt(), 1.0F, Pair.of(AVAConstants.VANILLA_FULL_PACKED_LIGHT, AVAConstants.VANILLA_FULL_PACKED_LIGHT), false);
            stack.popPose();
        }
    }

    @Override
    public void tick()
    {
        super.tick();
        if (valid())
            animateTick();
    }

    public boolean valid()
    {
        return item != null && property != null && mode != null;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers)
    {
        if (keyCode >= GLFW_KEY_1 && keyCode <= GLFW_KEY_3 && !boxItem.isFocused() && !boxMaxTicks.isFocused() && !boxModel.isFocused() && !boxframe2Ticks.isFocused())
            transformModeButtons[keyCode - GLFW_KEY_1].onPress();
        return super.keyPressed(keyCode, scanCode, modifiers);
    }

    @Override
    public void onClose()
    {
        ANIMATING_PERSPECTIVE = new Perspective();
        super.onClose();
    }

    protected abstract void print(IMode<P> mode, Perspective o, Perspective l, Perspective r);

    private Button printButton(IMode<P> mode, int x, int y)
    {
        return addRenderableWidget(new Button.Builder(Component.literal("Print"), (b) -> {
            if (property != null)
            {
                print(mode, property.display.get(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND), property.handsIdle.getA(), property.handsIdle.getB());
                property.quadAnimators2.forEach((model, map) -> {
                    if (model != null)
                        map.forEach((v, a) -> {
                            if (v == mode.getVariable() && !a.isEmpty())
                                System.out.println(".quadAnim(" + model.getRLName().toUpperCase(Locale.ROOT) + ", " + v.getName() + ", " + a.toCode("", null, null, null) + ")");
                        });
                });
            }
        }).bounds(x, y, 24, 10).build());
    }

    public IMode<P> mode;
    private Button modeButton(IMode<P> mode, int x, int y)
    {
        int i = getModes().indexOf(mode);
        modeButtons[i] = new Button.Builder(Component.literal(mode.getName().toLowerCase(Locale.ROOT)), (b) -> {
            if (property != null)
            {
                for (Button b2 : modeButtons)
                    b2.setFocused(false);
                b.setFocused(true);
                this.mode = mode;
                if (!mode.oRead(property).isEmpty())
                    updateSlider(mode.oRead(property).get(mode.oRead(property).size() - 1).targetTicks - 1);
                else
                    updateSlider(0);
            }
        }).bounds(x, y, 30, 12).build();
        return addRenderableWidget(modeButtons[i]);
    }

    public Transform getTransformMode()
    {
        return transformModeCache;
    }

    private Transform transformModeCache = Transform.TRANSLATION;
    private Button transformModeButton(Transform transform, int x, int y)
    {
        transformModeButtons[transform.ordinal()] = new Button.Builder(Component.literal(transform.name().toLowerCase(Locale.ROOT)), (b) -> {
            if (property != null)
                transformModeCache = transform;
        }).bounds(x, y, 44, 12).build();
        return addRenderableWidget(transformModeButtons[transform.ordinal()]);
    }

    private Object objModeCache = Object.O;
    private Button objectModeButton(Object obj, int x, int y)
    {
        objectModeButtons[obj.ordinal()] = new Button.Builder(Component.literal(obj.name()), (b) -> {
            objModeCache = obj;
        }).bounds(x, y, 12, 12).build();
        return addRenderableWidget(objectModeButtons[obj.ordinal()]);
    }

    private Button setToIdleButton(Object obj, int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Idle (" + obj.name()), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                switch (obj)
                {
                    case O -> mode.oWrite(property, mode.oRead(property).append(Animation.of(t, property.display.get(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND))));
                    case L -> mode.lWrite(property, mode.lRead(property).append(Animation.of(t, property.handsIdle.getA())));
                    case R -> mode.rWrite(property, mode.rRead(property).append(Animation.of(t, property.handsIdle.getB())));
                    case M -> setSubModelAnim(getSubModelAnim().append(Animation.of(t, new Perspective())));
                }
            }
        }).bounds(x, y, 32, 10).build());
    }

    private Button setToClearButton(Object obj, int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Clr (" + obj.name()), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                switch (obj)
                {
                    case O -> mode.oWrite(property, mode.oRead(property).discard(t));
                    case L -> mode.lWrite(property, mode.lRead(property).discard(t));
                    case R -> mode.rWrite(property, mode.rRead(property).discard(t));
                    case M -> setSubModelAnim(getSubModelAnim().discard(t));
                }
            }
        }).bounds(x, y, 32, 10).build());
    }

    private Perspective copy = Perspective.empty();
    private Button copyButton(Object obj, int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Copy (" + obj.name()), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                switch (obj)
                {
                    case O -> copy = AVACommonUtil.safePresentOr(() -> mode.oRead(property).get(t).target3f, Perspective::empty).copy();
                    case L -> copy = AVACommonUtil.safePresentOr(() -> mode.lRead(property).get(t).target3f, Perspective::empty).copy();
                    case R -> copy = AVACommonUtil.safePresentOr(() -> mode.rRead(property).get(t).target3f, Perspective::empty).copy();
                    case M -> copy = AVACommonUtil.safePresentOr(() -> getSubModelAnim().get(t).target3f, Perspective::empty).copy();
                }
            }
        }).bounds(x, y, 32, 10).build());
    }

    private Button cutButton(Object obj, int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Cut (" + obj.name()), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                switch (obj)
                {
                    case O -> {
                        copy = AVACommonUtil.safePresentOr(() -> mode.oRead(property).get(t).target3f, Perspective::empty).copy();
                        mode.oWrite(property, mode.oRead(property).discard(t));
                    }
                    case L -> {
                        copy = AVACommonUtil.safePresentOr(() -> mode.lRead(property).get(t).target3f, Perspective::empty).copy();
                        mode.lWrite(property, mode.lRead(property).discard(t));
                    }
                    case R -> {
                        copy = AVACommonUtil.safePresentOr(() -> mode.rRead(property).get(t).target3f, Perspective::empty).copy();
                        mode.rWrite(property, mode.rRead(property).discard(t));
                    }
                    case M -> {
                        copy = AVACommonUtil.safePresentOr(() -> getSubModelAnim().get(t).target3f, Perspective::empty).copy();
                        setSubModelAnim(getSubModelAnim().discard(t));
                    }
                }
            }
        }).bounds(x, y, 30, 10).build());
    }

    private Perspective[] copyAll = new Perspective[4];
    private Button copyAllButton(int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Copy All"), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                copyAll[0] = AVACommonUtil.safePresentAndOr(() -> mode.oRead(property).get(t).target3f, (p) -> mode.oRead(property).getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                copyAll[1] = AVACommonUtil.safePresentAndOr(() -> mode.lRead(property).get(t).target3f, (p) -> mode.lRead(property).getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                copyAll[2] = AVACommonUtil.safePresentAndOr(() -> mode.rRead(property).get(t).target3f, (p) -> mode.rRead(property).getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                copyAll[3] = AVACommonUtil.safePresentAndOr(() -> getSubModelAnim().get(t).target3f, (p) -> getSubModelAnim().getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
            }
        }).bounds(x, y, 38, 10).build());
    }

    private Button cutAllButton(int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Cut All"), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                copyAll[0] = AVACommonUtil.safePresentAndOr(() -> mode.oRead(property).get(t).target3f, (p) -> mode.oRead(property).getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                mode.oWrite(property, mode.oRead(property).discard(t));
                copyAll[1] = AVACommonUtil.safePresentAndOr(() -> mode.lRead(property).get(t).target3f, (p) -> mode.lRead(property).getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                mode.lWrite(property, mode.lRead(property).discard(t));
                copyAll[2] = AVACommonUtil.safePresentAndOr(() -> mode.rRead(property).get(t).target3f, (p) -> mode.rRead(property).getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                mode.rWrite(property, mode.rRead(property).discard(t));
                copyAll[3] = AVACommonUtil.safePresentAndOr(() -> getSubModelAnim().get(t).target3f, (p) -> getSubModelAnim().getUnpopulatedAnimations().stream().anyMatch((a) -> a.targetTicks == t), Perspective::empty).copy();
                setSubModelAnim(getSubModelAnim().discard(t));
            }
        }).bounds(x, y, 38, 10).build());
    }

    private Button pasteButton(Object obj, int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Paste (" + obj.name()), (b) -> {
            if (valid() && !copy.isEmpty())
            {
                int t = slider.getValueInt();
                switch (obj)
                {
                    case O -> mode.oWrite(property, mode.oRead(property).append(Animation.of(t, copy)));
                    case L -> mode.lWrite(property, mode.lRead(property).append(Animation.of(t, copy)));
                    case R -> mode.rWrite(property, mode.rRead(property).append(Animation.of(t, copy)));
                    case M -> setSubModelAnim(getSubModelAnim().append(Animation.of(t, copy)));
                }
            }
        }).bounds(x, y, 32, 10).build());
    }

    private Button pasteAllButton(int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("Paste All"), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                if (copyAll[0] != null && !copyAll[0].isEmpty())
                    mode.oWrite(property, mode.oRead(property).append(Animation.of(t, copyAll[0])));
                if (copyAll[1] != null && !copyAll[1].isEmpty())
                    mode.lWrite(property, mode.lRead(property).append(Animation.of(t, copyAll[1])));
                if (copyAll[2] != null && !copyAll[2].isEmpty())
                    mode.rWrite(property, mode.rRead(property).append(Animation.of(t, copyAll[2])));
                if (copyAll[3] != null && !copyAll[3].isEmpty())
                    setSubModelAnim(getSubModelAnim().append(Animation.of(t, copyAll[3])));
            }
        }).bounds(x, y, 38, 10).build());
    }

    private Button tranformResetButton(Transform transform, int x, int y)
    {
        return addRenderableWidget(Button.builder(Component.literal("X"), (b) -> {
            if (valid())
            {
                int t = slider.getValueInt();
                Pair<Function<P, Animations>, BiConsumer<P, Animations>> pair = Pair.of(null, null);
                switch (objModeCache)
                {
                    case O -> pair.set(mode::oRead, mode::oWrite);
                    case L -> pair.set(mode::lRead, mode::lWrite);
                    case R -> pair.set(mode::rRead, mode::rWrite);
                    case M -> pair.set((p) -> getSubModelAnim(), (p, a) -> setSubModelAnim(a));
                }
                switch (transform)
                {
                    case TRANSLATION -> pair.getB().accept(property, pair.getA().apply(property).append(Animation.of(t, AVACommonUtil.safePresentOr(() -> pair.getA().apply(property).get(t).target3f, Perspective::empty).withTranslation(0, 0, 0))));
                    case ROTATION -> pair.getB().accept(property, pair.getA().apply(property).append(Animation.of(t, AVACommonUtil.safePresentOr(() -> pair.getA().apply(property).get(t).target3f, Perspective::empty).withRotation(0, 0, 0))));
                    case SCALE -> pair.getB().accept(property, pair.getA().apply(property).append(Animation.of(t, AVACommonUtil.safePresentOr(() -> pair.getA().apply(property).get(t).target3f, Perspective::empty).withScale(1, 1, 1))));
                }
            }
        }).bounds(x, y, 10, 10).build());
    }

    public enum Object
    {
        O,
        L,
        R,
        M,
        ;
    }

    public enum Transform
    {
        ROTATION,
        TRANSLATION,
        SCALE,
        ;
    }

    protected abstract List<IMode<P>> getModes();

    public interface IMode<P extends RegularModelProperty<?, ?, ?, P>>
    {
        String getName();
        IModelVariables getVariable();

        Animations oRead(P properties);
        Animations lRead(P properties);
        Animations rRead(P properties);
        void oWrite(P properties, Animations animations);
        void lWrite(P properties, Animations animations);
        void rWrite(P properties, Animations animations);
    }

    public static class ModeImpl<V extends IModelVariables, P extends RegularModelProperty<?, ?, ?, P>> implements IMode<P>
    {
        public final V variable;
        public final Function<P, Animations> oRead;
        public final Function<P, Animations> lRead;
        public final Function<P, Animations> rRead;
        private final BiConsumer<P, Animations> oWrite;
        private final BiConsumer<P, Animations> lWrite;
        private final BiConsumer<P, Animations> rWrite;

        public ModeImpl(V variable, Function<P, Pair<Animations, Pair<Animations, Animations>>> pair)
        {
            this(variable, (p) -> pair.apply(p).getA(), (p) -> pair.apply(p).getB().getA(), (p) -> pair.apply(p).getB().getB(), (p, a) -> pair.apply(p).setA(a), (p, a) -> pair.apply(p).getB().setA(a), (p, a) -> pair.apply(p).getB().setB(a));
        }

        public ModeImpl(V variable, Function<P, Animations> oRead, Function<P, Animations> lRead, Function<P, Animations> rRead, BiConsumer<P, Animations> oWrite, BiConsumer<P, Animations> lWrite, BiConsumer<P, Animations> rWrite)
        {
            this.variable = variable;
            this.oRead = (p) -> Animations.of(oRead.apply(p));
            this.lRead = (p) -> Animations.of(lRead.apply(p));
            this.rRead = (p) -> Animations.of(rRead.apply(p));
            this.oWrite = oWrite;
            this.lWrite = lWrite;
            this.rWrite = rWrite;
        }

        @Override
        public String getName()
        {
            return variable.getName().toLowerCase(Locale.ROOT);
        }

        @Override
        public V getVariable()
        {
            return variable;
        }

        @Override
        public Animations oRead(P properties)
        {
            return oRead.apply(properties);
        }

        @Override
        public Animations lRead(P properties)
        {
            return lRead.apply(properties);
        }

        @Override
        public Animations rRead(P properties)
        {
            return rRead.apply(properties);
        }

        @Override
        public void oWrite(P properties, Animations animations)
        {
            oWrite.accept(properties, animations);
        }

        @Override
        public void lWrite(P properties, Animations animations)
        {
            lWrite.accept(properties, animations);
        }

        @Override
        public void rWrite(P properties, Animations animations)
        {
            rWrite.accept(properties, animations);
        }
    }

    protected static Animations perspectiveToAnimations(Perspective perspective)
    {
        return perspectiveToAnimations(perspective, 1);
    }

    protected static Animations perspectiveToAnimations(Perspective perspective, int maxTime)
    {
        return Animations.of().append(Animation.of(0, perspective)).append(Animation.of(maxTime, perspective));
    }
}
