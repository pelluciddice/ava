package pellucid.ava.client.guis;

import com.google.common.collect.ImmutableList;
import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.c4.C4ModelVariables;
import pellucid.ava.client.renderers.models.c4.C4SubModels;
import pellucid.ava.client.renderers.models.c4.RegularC4ModelProperty;
import pellucid.ava.events.data.custom.models.items.C4ModelResourcesManager;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.player.status.C4StatusManager;

import java.util.List;

public class C4AnimatingGUI extends ItemAnimatingGUI<C4Item, C4ModelVariables, C4SubModels, RegularC4ModelProperty, C4StatusManager, C4ModelResourcesManager>
{
    public C4AnimatingGUI()
    {
        super(AVAModelTypes.C4);
    }

    @Override
    protected void print(IMode<RegularC4ModelProperty> mode, Perspective o, Perspective l, Perspective r)
    {
        if (mode == IDLE)
        {
            System.out.println(o.toCode() + ",");
            System.out.println(property.handsIdle.getA().toCode() + ",");
            System.out.println(property.handsIdle.getB().toCode() + ",");
        }
        if (mode == RUN)
        {
            System.out.println(".run(" + property.run.getA().toCode() + ")");
            System.out.println(".runLeft(" + property.run.getB().getA().toCode() + ")");
            System.out.println(".runRight(" + property.run.getB().getB().toCode() + ")");
        }
        if (mode == DRAW)
        {
            System.out.println(property.draw.getA().toCode("draw", o, l, r));
            System.out.println(property.draw.getB().getA().toCode("drawLeft", o, l, r));
            System.out.println(property.draw.getB().getB().toCode("drawRight", o, l, r));
        }
        if (mode == SET)
        {
            System.out.println(property.set.getA().toCode("set", o, l, r));
            System.out.println(property.set.getB().getA().toCode("setLeft", o, l, r));
            System.out.println(property.set.getB().getB().toCode("setRight", o, l, r));
        }
    }

    public static final ModeImpl<C4ModelVariables, RegularC4ModelProperty> IDLE = new ModeImpl<>(C4ModelVariables.IDLE, (p) -> perspectiveToAnimations(p.display.getOrDefault(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new Perspective())), (p) -> perspectiveToAnimations(p.handsIdle.getA()), (p) -> perspectiveToAnimations(p.handsIdle.getB()), (p, a) -> p.display2(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, a.get(0).target3f), (p, a) -> p.leftHandsIdle(a.get(0).target3f), (p, a) -> p.rightHandsIdle(a.get(0).target3f));
    public static final ModeImpl<C4ModelVariables, RegularC4ModelProperty> RUN = new ModeImpl<>(C4ModelVariables.RUN, (p) -> perspectiveToAnimations(p.run.getA()), (p) -> perspectiveToAnimations(p.run.getB().getA()), (p) -> perspectiveToAnimations(p.run.getB().getB()), (p, a) -> p.run.setA(a.get(0).target3f), (p, a) -> p.run.getB().setA(a.get(0).target3f), (p, a) -> p.run.getB().setB(a.get(0).target3f));
    public static final ModeImpl<C4ModelVariables, RegularC4ModelProperty> DRAW = new ModeImpl<>(C4ModelVariables.DRAW, (p) -> p.draw);
    public static final ModeImpl<C4ModelVariables, RegularC4ModelProperty> SET = new ModeImpl<>(C4ModelVariables.SET, (p) -> p.set);

    @Override
    protected List<IMode<RegularC4ModelProperty>> getModes()
    {
        return ImmutableList.of(IDLE, RUN, DRAW, SET);
    }
}
