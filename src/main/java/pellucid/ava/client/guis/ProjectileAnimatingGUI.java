package pellucid.ava.client.guis;

import com.google.common.collect.ImmutableList;
import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.projectile.ProjectileModelVariables;
import pellucid.ava.client.renderers.models.projectile.ProjectileSubModels;
import pellucid.ava.client.renderers.models.projectile.RegularProjectileModelProperty;
import pellucid.ava.events.data.custom.models.items.ProjectileModelResourcesManager;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.player.status.ProjectileStatusManager;

import java.util.List;

public class ProjectileAnimatingGUI extends ItemAnimatingGUI<ThrowableItem, ProjectileModelVariables, ProjectileSubModels, RegularProjectileModelProperty, ProjectileStatusManager, ProjectileModelResourcesManager>
{
    public ProjectileAnimatingGUI()
    {
        super(AVAModelTypes.PROJECTILE);
    }

    @Override
    protected void print(IMode<RegularProjectileModelProperty> mode, Perspective o, Perspective l, Perspective r)
    {
        if (mode == IDLE)
        {
            System.out.println(o.toCode() + ",");
            System.out.println(property.handsIdle.getA().toCode() + ",");
            System.out.println(property.handsIdle.getB().toCode() + ",");
        }
        if (mode == RUN)
        {
            System.out.println(".run(" + property.run.getA().toCode() + ")");
            System.out.println(".runLeft(" + property.run.getB().getA().toCode() + ")");
            System.out.println(".runRight(" + property.run.getB().getB().toCode() + ")");
        }
        if (mode == DRAW)
        {
            System.out.println(property.draw.getA().toCode("draw", o, l, r));
            System.out.println(property.draw.getB().getA().toCode("drawLeft", o, l, r));
            System.out.println(property.draw.getB().getB().toCode("drawRight", o, l, r));
        }
        if (mode == PIN_OFF)
        {
            System.out.println(property.pinOff.getA().toCode("pinOff", o, l, r));
            System.out.println(property.pinOff.getB().getA().toCode("pinOffLeft", o, l, r));
            System.out.println(property.pinOff.getB().getB().toCode("pinOffRight", o, l, r));
        }
        if (mode == THROW)
        {
            System.out.println(property.thr0w.getA().toCode("thr0w", o, l, r));
            System.out.println(property.thr0w.getB().getA().toCode("thr0wLeft", o, l, r));
            System.out.println(property.thr0w.getB().getB().toCode("thr0wRight", o, l, r));
        }
        if (mode == TOSS)
        {
            System.out.println(property.toss.getA().toCode("toss", o, l, r));
            System.out.println(property.toss.getB().getA().toCode("tossLeft", o, l, r));
            System.out.println(property.toss.getB().getB().toCode("tossRight", o, l, r));
        }
    }

    public static final ModeImpl<ProjectileModelVariables, RegularProjectileModelProperty> IDLE = new ModeImpl<>(ProjectileModelVariables.IDLE, (p) -> perspectiveToAnimations(p.display.getOrDefault(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new Perspective())), (p) -> perspectiveToAnimations(p.handsIdle.getA()), (p) -> perspectiveToAnimations(p.handsIdle.getB()), (p, a) -> p.display2(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, a.get(0).target3f), (p, a) -> p.leftHandsIdle(a.get(0).target3f), (p, a) -> p.rightHandsIdle(a.get(0).target3f));
    public static final ModeImpl<ProjectileModelVariables, RegularProjectileModelProperty> RUN = new ModeImpl<>(ProjectileModelVariables.RUN, (p) -> perspectiveToAnimations(p.run.getA()), (p) -> perspectiveToAnimations(p.run.getB().getA()), (p) -> perspectiveToAnimations(p.run.getB().getB()), (p, a) -> p.run.setA(a.get(0).target3f), (p, a) -> p.run.getB().setA(a.get(0).target3f), (p, a) -> p.run.getB().setB(a.get(0).target3f));
    public static final ModeImpl<ProjectileModelVariables, RegularProjectileModelProperty> DRAW = new ModeImpl<>(ProjectileModelVariables.DRAW, (p) -> p.draw);
    public static final ModeImpl<ProjectileModelVariables, RegularProjectileModelProperty> PIN_OFF = new ModeImpl<>(ProjectileModelVariables.PIN_OFF, (p) -> p.pinOff);
    public static final ModeImpl<ProjectileModelVariables, RegularProjectileModelProperty> THROW = new ModeImpl<>(ProjectileModelVariables.THROW, (p) -> p.thr0w);
    public static final ModeImpl<ProjectileModelVariables, RegularProjectileModelProperty> TOSS = new ModeImpl<>(ProjectileModelVariables.TOSS, (p) -> p.toss);

    @Override
    protected List<IMode<RegularProjectileModelProperty>> getModes()
    {
        return ImmutableList.of(IDLE, RUN, DRAW, PIN_OFF, THROW, TOSS);
    }
}
