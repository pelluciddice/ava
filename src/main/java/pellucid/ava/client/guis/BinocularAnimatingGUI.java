package pellucid.ava.client.guis;

import com.google.common.collect.ImmutableList;
import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.binocular.BinoModelVariables;
import pellucid.ava.client.renderers.models.binocular.BinoSubModels;
import pellucid.ava.client.renderers.models.binocular.RegularBinoModelProperty;
import pellucid.ava.events.data.custom.models.items.BinoModelResourcesManager;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.player.status.BinocularStatusManager;

import java.util.List;

public class BinocularAnimatingGUI extends ItemAnimatingGUI<BinocularItem, BinoModelVariables, BinoSubModels, RegularBinoModelProperty, BinocularStatusManager, BinoModelResourcesManager>
{
    public BinocularAnimatingGUI()
    {
        super(AVAModelTypes.BINOCULAR);
    }

    @Override
    protected void print(IMode<RegularBinoModelProperty> mode, Perspective o, Perspective l, Perspective r)
    {
        if (mode == IDLE)
        {
            System.out.println(o.toCode() + ",");
            System.out.println(property.handsIdle.getA().toCode() + ",");
            System.out.println(property.handsIdle.getB().toCode() + ",");
        }
        if (mode == RUN)
        {
            System.out.println(".run(" + property.run.getA().toCode() + ")");
            System.out.println(".runLeft(" + property.run.getB().getA().toCode() + ")");
            System.out.println(".runRight(" + property.run.getB().getB().toCode() + ")");
        }
        if (mode == DRAW)
        {
            System.out.println(property.draw.getA().toCode("draw", o, l, r));
            System.out.println(property.draw.getB().getA().toCode("drawLeft", o, l, r));
            System.out.println(property.draw.getB().getB().toCode("drawRight", o, l, r));
        }
        if (mode == AIM)
        {
            System.out.println(".aimingPos(" + property.aimingPos.toCode() + ")");
        }
    }

    public static final ModeImpl<BinoModelVariables, RegularBinoModelProperty> IDLE = new ModeImpl<>(BinoModelVariables.IDLE, (p) -> perspectiveToAnimations(p.display.getOrDefault(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new Perspective())), (p) -> perspectiveToAnimations(p.handsIdle.getA()), (p) -> perspectiveToAnimations(p.handsIdle.getB()), (p, a) -> p.display2(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, a.get(0).target3f), (p, a) -> p.leftHandsIdle(a.get(0).target3f), (p, a) -> p.rightHandsIdle(a.get(0).target3f));
    public static final ModeImpl<BinoModelVariables, RegularBinoModelProperty> RUN = new ModeImpl<>(BinoModelVariables.RUN, (p) -> perspectiveToAnimations(p.run.getA()), (p) -> perspectiveToAnimations(p.run.getB().getA()), (p) -> perspectiveToAnimations(p.run.getB().getB()), (p, a) -> p.run.setA(a.get(0).target3f), (p, a) -> p.run.getB().setA(a.get(0).target3f), (p, a) -> p.run.getB().setB(a.get(0).target3f));
    public static final ModeImpl<BinoModelVariables, RegularBinoModelProperty> DRAW = new ModeImpl<>(BinoModelVariables.DRAW, (p) -> p.draw);
    public static final ModeImpl<BinoModelVariables, RegularBinoModelProperty> AIM = new ModeImpl<>(BinoModelVariables.AIM, (p) -> perspectiveToAnimations(p.aimingPos), (p) -> Animations.of(), (p) -> Animations.of(), (p, a) -> p.aimingPos(a.get(0).target3f), (p, a) -> {}, (p, a) -> {});

    @Override
    protected List<IMode<RegularBinoModelProperty>> getModes()
    {
        return ImmutableList.of(IDLE, RUN, DRAW, AIM);
    }
}
