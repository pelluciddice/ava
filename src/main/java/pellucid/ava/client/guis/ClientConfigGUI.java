package pellucid.ava.client.guis;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.narration.NarratableEntry;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.client.gui.widget.ExtendedSlider;
import net.neoforged.neoforge.common.ModConfigSpec;
import pellucid.ava.client.components.IntTextField;
import pellucid.ava.client.renderers.AVARenderer;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static pellucid.ava.client.guis.ClientConfigGUI.Type.*;

public class ClientConfigGUI extends Screen
{
    private static final ResourceLocation WIDGET_TEXTURE = new ResourceLocation("ava:textures/gui/client_config_widgets.png");
    private static final int SIZE = 256;
    private final Map<Type, List<AbstractWidget>> typeWidgets = new HashMap<Type, List<AbstractWidget>>() {{
        for (Type type : Type.values())
            put(type, new ArrayList<>());
    }};
    private Type currentType;
    protected int guiLeft;
    protected int guiTop;

    public ClientConfigGUI()
    {
        super(Component.empty());
    }

    @Override
    public void renderBackground(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        super.renderBackground(matrixStack, mouseX, mouseY, partialTicks);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        matrixStack.blit(currentType.texture, guiLeft, guiTop, 0, 0, SIZE, SIZE);
    }

    @Override
    public void render(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        if (minecraft == null)
            return;
        currentType.renderSpecial(this, matrixStack, mouseX, mouseY, partialTicks);
    }

    @Override
    protected void init()
    {
        renderables.clear();
        children().clear();
        guiLeft = (width - SIZE) / 2;
        guiTop = (height - SIZE) / 2;
        typeWidgets.forEach((type, list) -> list.clear());
        selectType(DISPLAY);
        addRenderableWidget(new TypeOption(this, 7, 7, "", DISPLAY));
        addRenderableWidget(new TypeOption(this, 63, 7, "", EFFECT));
        addRenderableWidget(new TypeOption(this, 119, 7, "", CONTROL));
    }

    @Override
    protected <T extends GuiEventListener & NarratableEntry> T addWidget(T listener)
    {
        if (listener instanceof AbstractWidget && !(listener instanceof TypeOption))
            typeWidgets.get(currentType).add((AbstractWidget) listener);
        return super.addWidget(listener);
    }

    private void selectType(Type type)
    {
        if (currentType != null)
            setWidgetVisibilty(typeWidgets.get(currentType), false);
        currentType = type;
        type.init(this);
        setWidgetVisibilty(typeWidgets.get(currentType), true);
    }

    private void setWidgetVisibilty(List<AbstractWidget> widgets, boolean visibility)
    {
        widgets.forEach((widget) -> widget.visible = visibility);
    }

    private static String getTranslation(String text)
    {
        return "ava.gui.client_config." + text;
    }

    private static AbstractWidget createMagnificationSensitivitySlider(int index, int x, int y, int width, int height)
    {
        ModConfigSpec.DoubleValue setting = AVAClientConfig.MAGNIFS.get()[index];
        index++;
        return new ExtendedSlider(x, y, width, height, Component.translatable(getTranslation("magnif"), AVACommonUtil.round(index / 10.0F + 1.0F, 1)).append(" "), Component.literal(" %"), 0, 200, setting.get() * 100.0F, true) {
            @Override
            protected void applyValue()
            {
                setting.set((double) getValueInt() / 100.0F);
            }
        };
    }

    public enum Type
    {
        DISPLAY {
            @Override
            void init(ClientConfigGUI gui)
            {
                gui.addRenderableWidget(new BooleanOption(gui, 15, 31, "fast_assets", AVAClientConfig.FAST_ASSETS));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 53, "ai_fast_assets", AVAClientConfig.AI_FAST_ASSETS));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 75, "gui_fast_assets", AVAClientConfig.GUI_FAST_ASSETS));

                gui.addRenderableWidget(new BooleanOption(gui, 15, 113, "bobbing", AVAClientConfig.FP_BOBBING));

                gui.addRenderableWidget(new BooleanOption(gui, 108, 31, "crosshair", AVAClientConfig.SHOULD_RENDER_CROSS_HAIR));
                gui.addRenderableWidget(new IntOption(gui, 108, 50, "red", AVAClientConfig.RED, 0, 255));
                gui.addRenderableWidget(new IntOption(gui, 108, 61, "green", AVAClientConfig.GREEN, 0, 255));
                gui.addRenderableWidget(new IntOption(gui, 108, 72, "blue", AVAClientConfig.BLUE, 0, 255));
                gui.addRenderableWidget(new IntOption(gui, 108, 83, "transparent", AVAClientConfig.TRANSPARENCY));
                gui.addRenderableWidget(new IntOption(gui, 108, 94, "centre_dot_size", AVAClientConfig.CENTRE_DOT_SIZE)).showTitle();
                gui.addRenderableWidget(new IntOption(gui, 108, 105, "line_thickness", AVAClientConfig.LINE_THICKNESS).showTitle());
                gui.addRenderableWidget(new IntOption(gui, 108, 116, "line_length", AVAClientConfig.LINE_LENGTH).showTitle());
                gui.addRenderableWidget(new IntOption(gui, 108, 127, "offset_scale", AVAClientConfig.OFFSET_SCALE).showTitle());

                gui.addRenderableWidget(new BooleanOption(gui, 15, 135, "projectile", AVAClientConfig.ENABLE_PROJECTILE_INDICATOR));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 157, "bio", AVAClientConfig.ENABLE_BIO_INDICATOR));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 179, "armour", AVAClientConfig.ENABLE_COMPLICATED_ARMOUR_MODEL));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 201, "player_model", AVAClientConfig.ENABLE_ALTERNATED_THIRD_PERSON_MODEL));
            }

            @Override
            void renderSpecial(ClientConfigGUI gui, GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
            {
                AVARenderer.renderCrosshairAt(matrixStack.pose(), gui.guiLeft + 210, gui.guiTop + 61, 4.0F);
            }
        },
        EFFECT {
            @Override
            void init(ClientConfigGUI gui)
            {
                gui.addRenderableWidget(new BooleanOption(gui, 15, 31, "kill_tip", AVAClientConfig.ENABLE_KILL_TIP));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 53, "bullet_hole", AVAClientConfig.ENABLE_BULLET_HOLE_EFFECT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 75, "blood", AVAClientConfig.ENABLE_BLOOD_EFFECT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 97, "bullet_trail", AVAClientConfig.ENABLE_BULLET_TRAIL_EFFECT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 119, "passive_radio", AVAClientConfig.ENABLE_PASSIVE_RADIO_VOICE));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 141, "ally_status", AVAClientConfig.ENABLE_ALLY_STATUS_EFFECT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 163, "kill_effect", AVAClientConfig.ENABLE_KILL_EFFECT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 185, "hit_effect", AVAClientConfig.ENABLE_HIT_EFFECT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 207, "lens_tint", AVAClientConfig.ENABLE_LENS_TINT));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 229, "creature_status", AVAClientConfig.ENABLE_CREATURE_STATUS));

                gui.addRenderableWidget(new BooleanOption(gui, 150, 31, "damage_tilt", AVAClientConfig.REMOVE_HURT_CAMERA_TILT_ON_FULLY_EQUIPPED));
                gui.addRenderableWidget(new BooleanOption(gui, 150, 53, "reject_image", AVAClientConfig.REJECT_SERVER_DISPLAY_IMAGE));
                gui.addRenderableWidget(new BooleanOption(gui, 150, 75, "projectile_trail", AVAClientConfig.ENABLE_PROJECTILE_TRAIL_EFFECT));
            }
        },
        CONTROL {
            @Override
            void init(ClientConfigGUI gui)
            {
                gui.addRenderableWidget(new BooleanOption(gui, 15, 31, "quick_swap_hotkey", AVAClientConfig.ENABLE_QUICK_SWAP_HOTKEY));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 53, "preset_hotkey", AVAClientConfig.ENABLE_PRESET_HOTKEY));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 75, "radio_hotkey", AVAClientConfig.ENABLE_RADIO_HOTKEY));
                gui.addRenderableWidget(new BooleanOption(gui, 15, 97, "ping_hotkey", AVAClientConfig.ENABLE_PING_HOTKEY));

                for (int i = 0; i < AVAClientConfig.MAGNIFS.get().length; i++)
                    gui.addRenderableWidget(createMagnificationSensitivitySlider(i, 200, 31 + i * 22, 100, 20));
            }
        },
        ;
        private final ResourceLocation texture;
        Type()
        {
            this.texture = new ResourceLocation("ava:textures/gui/client_config_" + name().toLowerCase(Locale.ROOT) + ".png");
        }

        void init(ClientConfigGUI gui)
        {

        }

        void renderSpecial(ClientConfigGUI gui, GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
        {
        }
    }

    static class TypeOption extends Option
    {
        private final Type type;
        private final MutableComponent title;

        public TypeOption(ClientConfigGUI screen, int x, int y, String toolTip, Type type)
        {
            super(screen, x, y, 56, 16, toolTip);
            this.type = type;
            this.title = Component.translatable("ava.gui.tab." + type.name().toLowerCase(Locale.ROOT));
        }

        @Override
        public void renderWidget(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
        {
            matrixStack.drawCenteredString(screen.font, title, (int) (getX() + width / 2.0F) + 2, getY() + 3, AVAConstants.AVA_HUD_TEXT_ORANGE);
        }

        @Override
        public void onPress()
        {
            screen.selectType(type);
        }
    }

    static class IntOption extends IntTextField<ClientConfigGUI>
    {
        private final MutableComponent toolTip;
        private final MutableComponent title;
        private boolean showTitle = false;
        protected final ModConfigSpec.IntValue config;

        public IntOption(ClientConfigGUI screen, int x, int y, String toolTip, ModConfigSpec.IntValue config)
        {
            this(screen, x, y, 27, 10, toolTip, config, 0, 100, config.get());
        }

        public IntOption(ClientConfigGUI screen, int x, int y, String toolTip, ModConfigSpec.IntValue config, Integer min, Integer max)
        {
            this(screen, x, y, 27, 10, toolTip, config, min, max, config.get());
        }

        public IntOption(ClientConfigGUI screen, int x, int y, int width, int height, String toolTip, ModConfigSpec.IntValue config, Integer min, Integer max, int defaultValue)
        {
            super(screen, screen.guiLeft + x, screen.guiTop + y, width, height, min, max, defaultValue);
            this.toolTip = Component.translatable(getTranslation(toolTip));
            this.title = Component.translatable(getTranslation("title_" + toolTip));
            this.config = config;
        }

        public IntOption showTitle()
        {
            showTitle = true;
            return this;
        }

        @Override
        protected boolean isValid(String text)
        {
            if (super.isValid(text) && config != null)
            {
                config.set(Integer.parseInt(text));
                return true;
            }
            return false;
        }

        @Override
        public void renderWidget(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
        {
            super.renderWidget(matrixStack, mouseX, mouseY, partialTicks);
            if (showTitle)
                AVAClientUtil.renderWrappedText(matrixStack, getX() + width + 3, getY() + 5, 65, screen.font, AVAConstants.AVA_HUD_TEXT_ORANGE, title);
        }
    }

    static class BooleanOption extends Option
    {
        private final ModConfigSpec.BooleanValue config;
        private final Component title;

        public BooleanOption(ClientConfigGUI screen, int x, int y, String toolTip, ModConfigSpec.BooleanValue config)
        {
            this(screen, x, y, 18, 18, toolTip, config);
        }

        public BooleanOption(ClientConfigGUI screen, int x, int y, int width, int height, String toolTip, ModConfigSpec.BooleanValue config)
        {
            super(screen, x, y, width, height, toolTip);
            this.config = config;
            this.title = Component.translatable(getTranslation("title_" + toolTip));
        }

        @Override
        public void renderWidget(GuiGraphics matrixStack, int mouseX, int mouseY, float partialTicks)
        {
            int u = config.get() ? 0 : 22;
            int v = config.get() ? 0 : 40;
            matrixStack.blit(WIDGET_TEXTURE, getX(), getY(), u, v, width, height);
            //            screen.font.drawShadow(matrixStack, title, x + width + 3, y + 5, AVAConstants.AVA_HUD_TEXT_ORANGE);
            AVAClientUtil.renderWrappedText(matrixStack, getX() + width + 3, getY() + 5, 65, screen.font, AVAConstants.AVA_HUD_TEXT_ORANGE, title);
        }

        @Override
        public void onPress()
        {
            config.set(!config.get());
        }
    }

    static class Option extends Button
    {
        protected final ClientConfigGUI screen;
        protected final MutableComponent toolTip;

        public Option(ClientConfigGUI screen, int x, int y, int width, int height, String toolTip)
        {
            this(screen, x, y, width, height, toolTip, (button) -> {});
        }

        public Option(ClientConfigGUI screen, int x, int y, int width, int height, String toolTip, OnPress action)
        {
            super(screen.guiLeft + x, screen.guiTop + y, width, height, Component.empty(), action, (msg) -> {
                return Component.empty();
            });
            this.screen = screen;
            this.toolTip = Component.translatable(getTranslation(toolTip));
            setTooltip(Tooltip.create(createNarrationMessage(), null));
        }

        @Override
        protected MutableComponent createNarrationMessage()
        {
            return toolTip;
        }

        //        @Override
//        public void renderToolTip(GuiGraphics matrixStack, int mouseX, int mouseY)
//        {
//            screen.renderTooltip(matrixStack, toolTip, mouseX, mouseY);
//        }
    }
}
