package pellucid.ava.client.guis;

import com.google.common.collect.ImmutableList;
import net.minecraft.world.item.ItemDisplayContext;
import pellucid.ava.client.renderers.Animations;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.guns.GunModelVariables;
import pellucid.ava.client.renderers.models.guns.GunSubModels;
import pellucid.ava.client.renderers.models.guns.RegularGunModelProperty;
import pellucid.ava.events.data.custom.models.items.GunModelResourcesManager;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.util.AVAConstants;

import java.util.List;

public class GunAnimatingGUI extends ItemAnimatingGUI<AVAItemGun, GunModelVariables, GunSubModels, RegularGunModelProperty, GunStatusManager, GunModelResourcesManager>
{
    public GunAnimatingGUI()
    {
        super(AVAModelTypes.GUNS);
    }

    @Override
    protected void print(IMode<RegularGunModelProperty> mode, Perspective o, Perspective l, Perspective r)
    {
        if (mode == IDLE)
        {
            System.out.println(o.toCode() + ",");
            System.out.println(property.handsIdle.getA().toCode() + ",");
            System.out.println(property.handsIdle.getB().toCode() + ",");
        }
        if (mode == RUN)
        {
            System.out.println(".run(" + property.run.getA().toCode() + ")");
            System.out.println(".runLeft(" + property.run.getB().getA().toCode() + ")");
            System.out.println(".runRight(" + property.run.getB().getB().toCode() + ")");
        }
        if (mode == RELOAD)
        {
            System.out.println(property.reload.getA().toCode("reload", o, l, r));
            System.out.println(property.reload.getB().getA().toCode("reloadLeft", o, l, r));
            System.out.println(property.reload.getB().getB().toCode("reloadRight", o, l, r));
        }
        if (mode == PRE_RELOAD)
        {
            System.out.println(property.preReload.getA().toCode("preReload", o, l, r));
            System.out.println(property.preReload.getB().getA().toCode("preReloadLeft", o, l, r));
            System.out.println(property.preReload.getB().getB().toCode("preReloadRight", o, l, r));
        }
        if (mode == POST_RELOAD)
        {
            System.out.println(property.postReload.getA().toCode("postReload", o, l, r));
            System.out.println(property.postReload.getB().getA().toCode("postReloadLeft", o, l, r));
            System.out.println(property.postReload.getB().getB().toCode("postReloadRight", o, l, r));
        }
        if (mode == DRAW)
        {
            System.out.println(property.draw.getA().toCode("draw", o, l, r));
            System.out.println(property.draw.getB().getA().toCode("drawLeft", o, l, r));
            System.out.println(property.draw.getB().getB().toCode("drawRight", o, l, r));
        }
        if (mode == FIRE)
        {
            System.out.println(property.fire.getA().toCode("fire", o, l, r));
            System.out.println(property.fire.getB().getA().toCode("fireLeft", o, l, r));
            System.out.println(property.fire.getB().getB().toCode("fireRight", o, l, r));
        }
        if (mode == SILENCER)
        {
            System.out.println(".silencerPos(" + property.silencerPos.toCode() + ")");
            System.out.println(property.installSilencerHands.getA().toCode("silencerLeft", o, l, r));
            System.out.println(property.installSilencerHands.getB().toCode("silencerRight", o, l, r));
        }
        if (mode == AIM)
        {
            System.out.println(".aimingPos(" + property.aimingPos.toCode() + ")");
        }
    }

    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> IDLE = new ModeImpl<>(GunModelVariables.IDLE, (p) -> perspectiveToAnimations(p.display.getOrDefault(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new Perspective())), (p) -> perspectiveToAnimations(p.handsIdle.getA()), (p) -> perspectiveToAnimations(p.handsIdle.getB()), (p, a) -> p.display2(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, a.get(0).target3f), (p, a) -> p.leftHandsIdle(a.get(0).target3f), (p, a) -> p.rightHandsIdle(a.get(0).target3f));
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> RUN = new ModeImpl<>(GunModelVariables.RUN, (p) -> perspectiveToAnimations(p.run.getA()), (p) -> perspectiveToAnimations(p.run.getB().getA()), (p) -> perspectiveToAnimations(p.run.getB().getB()), (p, a) -> p.run.setA(a.get(0).target3f), (p, a) -> p.run.getB().setA(a.get(0).target3f), (p, a) -> p.run.getB().setB(a.get(0).target3f));
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> PRE_RELOAD = new ModeImpl<>(GunModelVariables.PRE_RELOAD, (p) -> p.preReload);
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> RELOAD = new ModeImpl<>(GunModelVariables.RELOAD, (p) -> p.reload);
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> POST_RELOAD = new ModeImpl<>(GunModelVariables.POST_RELOAD, (p) -> p.postReload);
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> DRAW = new ModeImpl<>(GunModelVariables.DRAW, (p) -> p.draw);
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> FIRE = new ModeImpl<>(GunModelVariables.FIRE_TICKS, (p) -> p.fire);
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> SILENCER = new ModeImpl<>(GunModelVariables.INSTALL_SILENCER, (p) -> perspectiveToAnimations(p.silencerPos, AVAConstants.SILENCER_INSTALL_TIME), (p) -> p.installSilencerHands.getA(), (p) -> p.installSilencerHands.getB(), (p, a) -> p.silencerPos(a.get(0).target3f), (p, a) -> p.silencerLeft(a), (p, a) -> p.silencerRight(a));
    public static final ModeImpl<GunModelVariables, RegularGunModelProperty> AIM = new ModeImpl<>(GunModelVariables.AIM, (p) -> perspectiveToAnimations(p.aimingPos), (p) -> Animations.of(), (p) -> Animations.of(), (p, a) -> p.aimingPos(a.get(0).target3f), (p, a) -> {}, (p, a) -> {});

    @Override
    protected List<IMode<RegularGunModelProperty>> getModes()
    {
        return ImmutableList.of(IDLE, RUN, PRE_RELOAD, RELOAD, POST_RELOAD, DRAW, FIRE, SILENCER, AIM);
    }
}
