package pellucid.ava.competitive_mode;

import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.client.CameraType;
import net.minecraft.client.Minecraft;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import net.neoforged.neoforge.client.event.InputEvent;
import net.neoforged.neoforge.client.event.RenderNameTagEvent;
import org.apache.commons.io.FileUtils;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.AVA;
import pellucid.ava.client.inputs.ForceKeybind;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.objects.c4.C4Renderer;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Loop;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static pellucid.ava.util.AVAClientUtil.onPresetUpdate;
import static pellucid.ava.util.AVAWeaponUtil.TeamSide;

@EventBusSubscriber(Dist.CLIENT)
public class CompetitiveModeClient
{
    private static final Loop PRESET_CHOICE = new Loop(2);
    public static final List<ForceKeybind> WEAPON_CATEGORY_KEYS = ImmutableList.of(
            new ForceKeybind(GLFW.GLFW_KEY_1, () -> Minecraft.getInstance().options.keyHotbarSlots[1]),
            new ForceKeybind(GLFW.GLFW_KEY_2, () -> Minecraft.getInstance().options.keyHotbarSlots[2]),
            new ForceKeybind(GLFW.GLFW_KEY_3, () -> Minecraft.getInstance().options.keyHotbarSlots[3]),
            new ForceKeybind(GLFW.GLFW_KEY_4, () -> Minecraft.getInstance().options.keyHotbarSlots[4]),
            new ForceKeybind(GLFW.GLFW_KEY_5, () -> Minecraft.getInstance().options.keyHotbarSlots[5])
    );

    public static final ForceKeybind SWAP_PRIMARY_WEAPON = new ForceKeybind(GLFW.GLFW_KEY_F, () -> Minecraft.getInstance().options.keySwapOffhand);
    public static final ForceKeybind INTERACT = new ForceKeybind(GLFW.GLFW_KEY_E, () -> Minecraft.getInstance().options.keyInventory);
    public static final ForceKeybind PICK_UP = new ForceKeybind(GLFW.GLFW_KEY_G, null);
    public static final ForceKeybind OPEN_PARACHUTE = new ForceKeybind(GLFW.GLFW_KEY_SPACE, () -> Minecraft.getInstance().options.keyJump, () -> canUseParachute(Minecraft.getInstance().player));

    public static void init()
    {
        configurePresetsFromDisk();
        configureAttachmentsFromDisk();
    }

    public static void configureAttachmentsFromDisk()
    {
        File file = attachmentsFile(presetDirectory());
        try
        {
            if (file.createNewFile())
                AVA.LOGGER.info("No existing attachments configuration found, creating at " + file.getAbsolutePath());
            loadAttachments();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void configurePresetsFromDisk()
    {
        Preset.init();
        File presetDirectory = presetDirectory();
        for (Preset preset : Preset.PRESETS)
        {
            File file = presetFile(presetDirectory, preset);
            try
            {
                if (file.createNewFile())
                {
                    savePreset(file, preset.getDefault());
                    AVA.LOGGER.info("No existing preset configuration for " + preset.getName() + " found, creating at " + file.getAbsolutePath());
                }
                else
                    loadPreset(file, preset);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
        }
    }

    public static File presetDirectory()
    {
        File presetDirectory = new File(Minecraft.getInstance().gameDirectory.getAbsolutePath(), "ava_presets");
        if (presetDirectory.mkdir())
            AVA.LOGGER.info("No existing preset directory found, creating at " + presetDirectory.getAbsolutePath());
        return presetDirectory;
    }

    public static File presetFile(File directory, Preset preset)
    {
        return new File(directory.getAbsolutePath(), preset.getName() + ".json");
    }

    public static File attachmentsFile(File directory)
    {
        return new File(directory.getAbsolutePath(), "attachments.json");
    }

    public static void savePresetUnsafe(Preset preset)
    {
        savePreset(presetFile(presetDirectory(), preset), preset);
    }

    public static void savePreset(File file, Preset preset)
    {
        try
        {
            FileUtils.writeStringToFile(file, AVAClientUtil.avaPrettyPrint(preset.writeToJsonR(), 0), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }


    public static void loadPresetUnsafe(Preset preset)
    {
        loadPreset(presetFile(presetDirectory(), preset), preset);
    }

    public static void loadPreset(File file, Preset preset)
    {
        try (FileReader reader = new FileReader(file))
        {
            preset.readFromJson(JsonParser.parseReader(reader));
        }
        catch (Exception e)
        {
            AVA.LOGGER.info("Error parsing preset configurations! Restoring " + preset.getName() + " back to default!");
            preset.toDefault();
            e.printStackTrace();
        }
    }

    public static void putAttachment(Item item, CompoundTag attachments)
    {
        try
        {
            Preset.ATTACHMENTS.put(item, attachments);
            JsonObject json = new JsonObject();
            Preset.ATTACHMENTS.forEach((gun, compound) -> {
                DataTypes.COMPOUND.write(json, BuiltInRegistries.ITEM.getKey(gun).toString(), compound);
            });
            FileUtils.writeStringToFile(attachmentsFile(presetDirectory()), json.toString(), StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void loadAttachments()
    {
        try (FileReader reader = new FileReader(attachmentsFile(presetDirectory())))
        {
            Preset.ATTACHMENTS.clear();
            JsonParser.parseReader(reader).getAsJsonObject().entrySet().forEach((entry) -> {
                Preset.ATTACHMENTS.put(BuiltInRegistries.ITEM.get(new ResourceLocation(entry.getKey())), DataTypes.COMPOUND.read(entry.getValue().getAsJsonObject()));
            });
        }
        catch (Exception e)
        {
            try
            {
                FileUtils.writeStringToFile(attachmentsFile(presetDirectory()), "{}", StandardCharsets.UTF_8);
            }
            catch (Exception ignored)
            {

            }
            AVA.LOGGER.info("Error parsing attachments configurations! Clearing back to default!");
        }
    }

    @SubscribeEvent
    public static void onMouseScroll(InputEvent.MouseScrollingEvent event)
    {
        Player player = Minecraft.getInstance().player;
        if (player == null || player.getAbilities().instabuild || player.isSpectator())
            return;
        if (AVAClientUtil.isCompetitiveModeEnabled())
            CompetitiveInventory.updateChoice(player.getInventory(), CompetitiveInventory.getCurrentChoice().getCategory().getIndex() + (Math.max(event.getScrollDeltaX(), event.getScrollDeltaY()) > 0.0D ? -1 : 1));
        else return;
        event.setCanceled(true);
    }

    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player == null || !player.isAlive() || !AVAClientUtil.isCompetitiveModeEnabled() || player.getAbilities().invulnerable)
            return;
        ForceKeybind.KEY_BINDS.forEach(ForceKeybind::tick);
        CompetitiveInventory.tick(player);
        if (OPEN_PARACHUTE.justPressed() && canUseParachute(player))
        {
            player.getPersistentData().putBoolean(AVAConstants.TAG_ENTITY_PARACHUTED, true);
            PlayerActionMessage.activateParachute();
        }

        minecraft.options.setCameraType(CameraType.FIRST_PERSON);
        AVAClientUtil.unpressKeybind(minecraft.options.keyTogglePerspective);
        AVAClientUtil.unpressKeybind(minecraft.options.keyDrop);
        player.getInventory().selected = 0;
    }

    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Post event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player == null || !player.isAlive() || !AVAClientUtil.isCompetitiveModeEnabled() || player.getAbilities().invulnerable)
            return;

        minecraft.options.setCameraType(CameraType.FIRST_PERSON);
        AVAClientUtil.unpressKeybind(minecraft.options.keyTogglePerspective);
        AVAClientUtil.unpressKeybind(minecraft.options.keyDrop);
        player.getInventory().selected = 0;
    }

    public static boolean canUseParachute(Player player)
    {
        return player != null && player.isAlive() && AVAServerConfig.PRESET_WITH_PARACHUTE.get() && !player.onGround() && !AVACommonUtil.cap(player).getUsingParachute() && !player.getPersistentData().getBoolean(AVAConstants.TAG_ENTITY_PARACHUTED);
    }

    @SubscribeEvent
    public static void onC4ItemRender(RenderNameTagEvent event)
    {
        Entity entity = event.getEntity();
        Player player = Minecraft.getInstance().player;
        if (entity instanceof ItemEntity && ((ItemEntity) entity).getItem().getItem() instanceof C4Item && player != null && AVACommonUtil.isFullEquippedSide(player, TeamSide.EU) && AVAClientUtil.isCompetitiveModeEnabled())
            C4Renderer.drawMarkAndDistance(player, entity, event.getPoseStack(), event.getMultiBufferSource(), event.getPackedLight(), C4Renderer.BLUE_MARK, -0.3F);
    }

    public static Item getIconicWeapon()
    {
        return getPreset().getPrimary();
    }

    public static void calculateAndSendPreset()
    {
        DataToServerMessage.sendPreset(getPreset().serializeNBT());
        CompetitiveInventory.PRIMARY.setSelected(true);
        CompetitiveInventory.CHOICES.forEach((choice) -> choice.setIndex(0));
    }

    public static void choosePreset(int id)
    {
        forceChoosePreset(id);
        onPresetUpdate();
    }

    public static void forceChoosePreset(int id)
    {
        PRESET_CHOICE.set(id);
    }

    public static int getCurrentPresetChoice()
    {
        return PRESET_CHOICE.current();
    }

    public static Preset getPreset()
    {
        return Preset.PRESETS.get(getCurrentPresetChoice());
    }
}
