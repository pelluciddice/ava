package pellucid.ava.competitive_mode;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.inputs.AVARadio;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.gun.attachments.GunAttachmentManager;
import pellucid.ava.items.armours.AVAArmours;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.guns.AVASpecialWeapon;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.packets.DataToServerMessage;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.AVAWeaponUtil.WeaponCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static pellucid.ava.competitive_mode.CompetitiveModeClient.SWAP_PRIMARY_WEAPON;
import static pellucid.ava.competitive_mode.CompetitiveModeClient.WEAPON_CATEGORY_KEYS;

public class CompetitiveInventory
{
    public static final List<CategoryChoice> CHOICES = new ArrayList<>();
    public static final CategoryChoice PRIMARY = new CategoryChoice(WeaponCategory.MAIN, true);
    public static final CategoryChoice SECONDARY = new CategoryChoice(WeaponCategory.SECONDARY);
    public static final CategoryChoice MELEE = new CategoryChoice(WeaponCategory.MELEE);
    public static final CategoryChoice PROJECTILE = new CategoryChoice(WeaponCategory.PROJECTILES);
    public static final CategoryChoice SPECIAL = new CategoryChoice(WeaponCategory.SPECIAL);

    public static void tick(Player player)
    {
        if (!AVAClientUtil.isFocused())
            return;
        Inventory inventory = player.getInventory();
        if (!AVARadio.isRadioActive())
        {
            int down = -1;
            for (int i = 0; i < WEAPON_CATEGORY_KEYS.size(); i++)
                if (WEAPON_CATEGORY_KEYS.get(i).justPressed())
                    down = i;
            if (down != -1)
                updateChoice(inventory, down);
        }
        CategoryChoice choice = getCurrentChoice();
        if (SWAP_PRIMARY_WEAPON.justPressed() && choice == PRIMARY)
        {
            choice.nextIndex(inventory);
            updateInventory(inventory);
        }
        if (player.getMainHandItem().isEmpty())
            updateChoice(inventory, 0);
    }

    public static void updateChoice(Inventory inventory, int index)
    {
        if (index > 4)
            index = 0;
        else if (index < 0)
            index = 4;
        CategoryChoice choice = CHOICES.get(index);
        if (choice.category.getStacks(inventory).isEmpty())
            return;
        if (choice.isSelected())
        {
            if (index != 0)
                choice.nextIndex(inventory);
        }
        else
            choice.setSelected(true);
        updateInventory(inventory);
    }

    public static void updateInventory(Inventory inventory)
    {
        CategoryChoice choice = getCurrentChoice();
        if (!choice.getCategory().getStacks(inventory).isEmpty())
            DataToServerMessage.selectWeapon(choice.category.getIndex(), choice.index);
    }

    public static CategoryChoice getCurrentChoice()
    {
        return CHOICES.stream().filter(CategoryChoice::isSelected).findFirst().orElse(PRIMARY);
    }

    public static boolean giveArmour(Player player, String team)
    {
        if (player.level().getScoreboard().getTeamNames().contains(team))
            if (player.getTeam() == player.level().getScoreboard().getPlayerTeam(team))
            {
                AVAArmours.giveTo(player, team.equals("eu"), true);
                return true;
            }
        return false;
    }

    public static void giveIfExist(Player player, List<ItemStack> toRestore, int[] slots, CompoundTag primaryMod, CompoundTag primaryMod2, Item... items)
    {
        List<ItemStack> magStacks = new ArrayList<>();
        int index = 9;
        Map<WeaponCategory, Integer> categoryID = new HashMap<>() {{
            for (WeaponCategory category : WeaponCategory.values())
            {
                put(category, 0);
            }
        }};
        for (int i = 0; i < items.length; i++, index++)
        {
            if (i == 1 && AVAServerConfig.PRESET_SINGLE_PRIMARY_WEAPON.get())
                continue;
            Item item = items[i];
            if (AVAWeaponUtil.isWeaponDisabled(item))
                continue;
            if (item instanceof IClassification)
            {
                if (slots[i] != ((IClassification) item).getClassification().getSlotIndex() - 1)
                    continue;
                ItemStack weaponStack = new ItemStack(item);
                if (item instanceof AVAItemGun)
                {
                    if (i == 0)
                        new GunAttachmentManager(weaponStack, primaryMod).save();
                    else if (i == 1)
                        new GunAttachmentManager(weaponStack, primaryMod2).save();
                    ((AVAItemGun) item).forceReload(weaponStack);
                    if (AVAServerConfig.isCompetitiveModeActivated())
                        weaponStack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, ((AVAItemGun) item).getCapacity(weaponStack, true) * 3);
                }
                if (!AVAServerConfig.isCompetitiveModeActivated())
                {
                    if (item instanceof AVASpecialWeapon)
                    {
                        Item mag = ((AVAItemGun) item).getAmmoType(weaponStack);
                        ItemStack magStack = new ItemStack(mag, ((AVASpecialWeapon) item).getCapacity(weaponStack, true));
                        magStacks.add(magStack);
                    }
                    else if (item instanceof AVAItemGun)
                    {
                        Item mag = ((AVAItemGun) item).getAmmoType(weaponStack);
                        ItemStack magStack;
                        if (mag instanceof Ammo)
                            magStack = ((Ammo) mag).addToInventory(player, 5, true);
                        else
                            magStack = new ItemStack(mag, 5);
                        magStacks.add(magStack);
                    }
                }
                else
                {
                    WeaponCategory category = WeaponCategory.fromItem(weaponStack.getItem());
                    if (category != null)
                        weaponStack.set(category.getTag(), categoryID.computeIfPresent(category, (cat, id) -> id + 1));
                }
                player.getInventory().setItem(i == 0 ? 0 : index, weaponStack);
            }
        }
        magStacks.forEach(player::addItem);
        toRestore.forEach(player::addItem);
        player.getInventory().setChanged();
    }

    public static class CategoryChoice
    {
        private final WeaponCategory category;
        private boolean selected = false;
        private int index = 0;

        CategoryChoice(WeaponCategory category, boolean selected)
        {
            this(category);
            this.selected = selected;
        }

        CategoryChoice(WeaponCategory category)
        {
            this.category = category;
            CHOICES.add(this);
        }

        public void lastIndex(Inventory inventory)
        {
            setIndex(inventory, index - 1);
        }

        public void nextIndex(Inventory inventory)
        {
            setIndex(inventory, index + 1);
        }

        public void setIndex(Inventory inventory, int index)
        {
            int size = category.getStacks(inventory).size();
            if (size > 0)
            {
                while (index >= size)
                    index -= size;
                this.index = index;
            }
            else this.index = 0;
        }

        public void setIndex(int index)
        {
            this.index = index;
        }

        public WeaponCategory getCategory()
        {
            return category;
        }

        public boolean isSelected()
        {
            return selected;
        }

        public void setSelected(boolean selected)
        {
            if (selected)
                CHOICES.forEach((choice) -> choice.setSelected(false));
            this.selected = selected;
        }
    }
}
