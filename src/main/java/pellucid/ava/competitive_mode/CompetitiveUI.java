package pellucid.ava.competitive_mode;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.LayeredDraw;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.RenderGuiLayerEvent;
import net.neoforged.neoforge.client.gui.VanillaGuiLayers;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Pair;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

@EventBusSubscriber(Dist.CLIENT)
public class CompetitiveUI
{
    public static final List<ResourceLocation> RESTRICTED = ImmutableList.of(
            VanillaGuiLayers.ARMOR_LEVEL,
            VanillaGuiLayers.PLAYER_HEALTH,
            VanillaGuiLayers.FOOD_LEVEL,
            VanillaGuiLayers.AIR_LEVEL,
            VanillaGuiLayers.HOTBAR,
            VanillaGuiLayers.EXPERIENCE_BAR,
            VanillaGuiLayers.EXPERIENCE_LEVEL,
            VanillaGuiLayers.VEHICLE_HEALTH,
            VanillaGuiLayers.JUMP_METER,
            VanillaGuiLayers.SUBTITLE_OVERLAY);

    public static final LayeredDraw.Layer COMPETITIVE_UI = (mStack, partialTicks) -> {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player != null && AVAClientUtil.isCompetitiveModeEnabled() && !player.isCreative())
        {
            int screenWidth = minecraft.getWindow().getGuiScaledWidth();
            int screenHeight = minecraft.getWindow().getGuiScaledHeight();
            renderCompetitiveUI(minecraft, player, player.level(), mStack, screenWidth, screenHeight, partialTicks);
        }
    };

    private static void renderCompetitiveUI(Minecraft minecraft, Player player, Level world, GuiGraphics stack, int screenWidth, int screenHeight, float partialTicks)
    {
        ItemStack item = player.getMainHandItem();
        Font font = minecraft.font;
        PlayerAction cap = null;
        try
        {
            cap = AVACommonUtil.cap(player);
        }
        catch (Exception ignored)
        {

        }
        stack.pose().pushPose();
        stack.pose().translate(screenWidth / 2.0F, screenHeight, 0.0F);

//        Timer timer = AVACommonUtil.cap(player.level()).getDisplayTimer();
        Timer timer = AVACrossWorldData.getInstance().getDisplayTimer();
        if (timer != null)
        {
            stack.pose().pushPose();
            float scale = 1.85F;
            stack.pose().scale(scale + 0.25F, scale, scale);
            stack.drawCenteredString(minecraft.font, timer.getMinuteString(world) + ":" + timer.getSecondsString(world), 0, -11, AVAConstants.AVA_HUD_TEXT_WHITE);
            stack.pose().popPose();
        }


        //HP
        stack.pose().pushPose();
        float scale = 0.7F;
        stack.pose().scale(scale + 0.15F, scale, scale);
        String text = "HP";
        stack.drawString(minecraft.font, text, -107 - font.width(text), -30, AVAConstants.AVA_HUD_TEXT_GRAY);
        stack.pose().popPose();
        AVAClientUtil.drawTransparent(true);
        final int minX = -90;
        final int maxXSize = 60;
        Color color = AVAConstants.AVA_HUD_COLOUR_WHITE;
        float health = player.getHealth();
        float maxHealth = player.getMaxHealth();
        if (health <= maxHealth / 3.0F)
            color = AVAConstants.AVA_HUD_COLOUR_RED;
        int maxX = (int) (minX + maxXSize * AnimationFactory.HP.lerpD(partialTicks, health / maxHealth));
        stack.fill(-90, -21, maxX, -15, color.getRGB());
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        AVAClientUtil.drawTransparent(false);


        //AP
        stack.pose().pushPose();
        float scale2 = 0.7F;
        stack.pose().scale(scale2 + 0.15F, scale2, scale2);
        String text2 = "AP";
        stack.drawString(minecraft.font, text2, -107 - font.width(text), -21, AVAConstants.AVA_HUD_TEXT_GRAY);
        stack.pose().popPose();
        if (cap != null)
        {
            final int minX2 = -90;
            final int maxXSize2 = 60;
            int maxX2 = (int) (minX2 + maxXSize2 * AnimationFactory.AP.lerpD(partialTicks, cap.getArmourValue() / 10.0F));
            stack.fill(-90, -13, maxX2, -10, AVAConstants.AVA_HUD_TEXT_WHITE);
        }

        //Bg
        drawLine(stack, -90, -14.15F, -30, -13.85F);
        drawLine(stack, -100, -9.15F, -25, -8.85F);
        drawLine(stack, 25, -9.15F, 100, -8.85F);
//        AVAClientUtil.draw2DLine(stack, 3, 0, -screenHeight / 2.0F, 10, 10 -screenHeight / 2.0F, 255, 255, 255, 1.0F);

        //Ammo
        if (item.getItem() instanceof AVAItemGun gun)
        {
            stack.pose().pushPose();
            float scale3 = 2.2F;
            stack.pose().scale(scale3 + 0.35F, scale3, scale3);
            int ammoCount = item.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0);
            MutableComponent ammo = Component.literal(String.valueOf(ammoCount));
            if (ammoCount <= gun.getCapacity(item, true) / (AVAWeaponUtil.isPrimaryWeapon(gun) ? 5 : AVAWeaponUtil.isSecondaryWeapon(gun) ? 6 : 1))
                ammo.withStyle(ChatFormatting.RED);


            stack.drawString(minecraft.font, ammo, 19 - font.width(ammo), -13, AVAConstants.AVA_HUD_TEXT_GRAY);
            stack.pose().popPose();


            stack.pose().pushPose();
            float scale4 = 1.75F;
            stack.pose().scale(scale4, scale4, scale4);
            stack.drawString(minecraft.font, "/", 27, -14, AVAConstants.AVA_HUD_TEXT_GRAY);
            stack.pose().popPose();


            stack.pose().pushPose();
            float scale5 = 1.6F;
            stack.pose().scale(scale5 + 0.35F, scale5, scale5);
            stack.drawString(minecraft.font, String.valueOf(AVAWeaponUtil.getAmmoCount(player, item, AVAClientUtil.isCompetitiveModeEnabled())), 28, -14, AVAConstants.AVA_HUD_TEXT_GRAY);
            stack.pose().popPose();
        }


        stack.pose().pushPose();
        float scale5 = 0.75F;
        stack.pose().scale(scale5 + 0.1F, scale5, scale5);
        stack.drawString(font, item.getHoverName(), 50, -11, AVAConstants.AVA_HUD_TEXT_WHITE, true);
        stack.pose().popPose();


        //Projectiles
        float scale6 = 1.45F;
        List<Pair<ItemStack, Integer>> projectiles = new ArrayList<>();
        int projectileX = 42;
        for (ItemStack projectile : AVAWeaponUtil.WeaponCategory.PROJECTILES.getStacks(player.getInventory()))
        {
            projectiles.add(Pair.of(projectile, projectileX += 9));
            if (item == projectile)
                AVAClientUtil.fillGradient(stack.pose(), (int) (projectileX * 1.4F) + 5, -24, (int) (projectileX * 1.4F + 17), -9, 0, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), Direction.NORTH);
        }

        AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
//            stack2.translate(screenWidth / 2.0F, screenHeight, 0.0F);
            stack2.scale(scale6 - 0.05F, scale6, scale6);
        }, (stack2) -> {
            for (Pair<ItemStack, Integer> projectile : projectiles)
            {
                stack.renderFakeItem(projectile.getA(), projectile.getB(), -19);
                stack.renderItemDecorations(font, projectile.getA(), projectile.getB(), -19);
            }
        });

        //Specials
        float scale7 = 1.3F;
        List<Pair<ItemStack, Integer>> specials = new ArrayList<>();
        int specialX = -80;
        for (ItemStack special : AVAWeaponUtil.WeaponCategory.SPECIAL.getStacks(player.getInventory()))
        {
            specials.add(Pair.of(special, specialX -= 16));
            if (item == special)
                AVAClientUtil.fillGradient(stack.pose(), (int) (specialX * 1.4F) + 14, -24, (int) (specialX * 1.4F + 34), -9, 0, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB(), AVAConstants.AVA_HUD_COLOUR_WHITE_TRANSPARENT.getRGB(), Direction.NORTH);
        }
        AVAClientUtil.transformItemMatrix(stack.pose(), (stack2) -> {
//            stack2.translate(screenWidth / 2.0F, screenHeight, 0.0F);
            stack2.scale(scale7 - 0.05F, scale7, scale7);
        }, (stack2) -> {
            for (Pair<ItemStack, Integer> special : specials)
            {
                stack.renderItem(special.getA(), special.getB(), -20);
                stack.renderItemDecorations(font, special.getA(), special.getB(), -20);
            }
        });


        //Primary weapon order icon
        List<ItemStack> stacks3 = AVAWeaponUtil.WeaponCategory.MAIN.getStacks(player.getInventory());
        int index = stacks3.indexOf(item);
        if (item.getItem() instanceof AVAItemGun && index != -1)
        {
            stack.fill(29, -7, 33, -3, index == 0 ? AVAConstants.AVA_HUD_TEXT_ORANGE : AVAConstants.AVA_HUD_TEXT_WHITE);
            stack.fill(35, -7, 39, -3, index == 0 ? AVAConstants.AVA_HUD_TEXT_WHITE : AVAConstants.AVA_HUD_TEXT_ORANGE);
        }

        if (CompetitiveModeClient.canUseParachute(player))
            stack.drawCenteredString(font, Component.translatable("ava.item.tips.parachute_open"), 0, -70, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());

        stack.pose().popPose();
    }

    @SubscribeEvent
    public static void onOverlayRender(RenderGuiLayerEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (player == null || player.getAbilities().instabuild)
            return;
        if (!AVAClientUtil.isCompetitiveModeEnabled())
            return;
        if (RESTRICTED.contains(event.getName()))
            event.setCanceled(true);
    }

    public static void drawLine(GuiGraphics stack, float x1, float y1, float x2, float y2)
    {
        AVAClientUtil.drawRect(stack.pose(), x1, y1, x2, y2, 255, 255, 255, 0.4F);
    }
}
