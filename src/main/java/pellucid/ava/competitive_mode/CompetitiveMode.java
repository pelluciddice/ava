package pellucid.ava.competitive_mode;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.common.util.TriState;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.ItemEntityPickupEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import net.neoforged.neoforge.event.tick.PlayerTickEvent;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.List;

import static pellucid.ava.util.AVAWeaponUtil.*;

@EventBusSubscriber
public class CompetitiveMode
{
    @SubscribeEvent
    public static void onPlayerDeath(LivingDeathEvent event)
    {
        LivingEntity entity = event.getEntity();
        if (isModeActive() && entity instanceof Player && !entity.level().isClientSide())
        {
            Inventory inventory = ((Player) entity).getInventory();
            List<ItemStack> stacks = AVAWeaponUtil.WeaponCategory.MAIN.getStacks(inventory);
            if (!stacks.isEmpty())
                entity.spawnAtLocation(stacks.get(0).copy());
            for (int index : AVAWeaponUtil.getSlotIndexesFor(inventory, MiscItems.C4.get()))
            {
                ItemStack stack = inventory.getItem(index).copy();
                if (!stack.isEmpty())
                {
                    stack.set(AVADataComponents.TAG_ITEM_SPECIAL, true);
                    inventory.setItem(index, ItemStack.EMPTY);
                    ((Player) entity).drop(stack, false);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent event)
    {
        if (!event.isEndConquered() && isModeActive())
        {
            Player player = event.getEntity();
            PlayerAction.getCap(player).setArmourValue(player, 10.0F);
            player.getPersistentData().putBoolean(AVAConstants.TAG_ENTITY_PARACHUTED, false);
            if (player instanceof ServerPlayer sp)
                DataToClientMessage.requestClientPreset(sp);
        }
    }

    @SubscribeEvent
    public static void onPlayerPickUpItem(ItemEntityPickupEvent.Pre event)
    {
        if (!event.getItemEntity().level().isClientSide)
        {
            Player player = event.getPlayer();
            ItemStack stack = event.getItemEntity().getItem();
            if (AVAServerConfig.isCompetitiveModeActivated())
            {
                if (stack.getItem() instanceof C4Item)
                {
                    event.setCanPickup(TriState.FALSE);
                    if (TeamSide.getSideFor(player) == TeamSide.EU)
                    {
                        AVAWeaponUtil.WeaponCategory.SPECIAL.addTo(player.getInventory(), stack.copy());
                        event.getItemEntity().remove(Entity.RemovalReason.DISCARDED);
                        player.level().playSound(null, player, AVASounds.PICKUP_ITEM.get(), SoundSource.PLAYERS, 1.0F, 1.0F);
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void playerTick(PlayerTickEvent.Post event)
    {
        Player player = event.getEntity();
        if (isPlayerValid(player) && !player.level().isClientSide() && isModeActive())
        {
            int primary = 0;
            boolean secondary = false;
            boolean melee = false;
            int projectile = 0;
            boolean special = false;
            Inventory inv = player.getInventory();
            for (int i = 0; i < inv.getContainerSize(); i++)
            {
                ItemStack stack = inv.getItem(i);
                Item item = inv.getItem(i).getItem();
                if (isPrimaryWeapon(item))
                {
                    if (primary < 2)
                        primary = Math.min(2, stack.getCount() + primary);
                    else
                        inv.removeItemNoUpdate(i);
                }
                else if (isSecondaryWeapon(item))
                {
                    if (!secondary)
                        secondary = true;
                    else
                        inv.removeItemNoUpdate(i);
                }
                else if (isMeleeWeapon(item))
                {
                    if (!melee)
                        melee = true;
                    else
                        inv.removeItemNoUpdate(i);
                }
                else if (isProjectile(item))
                {
                    if (projectile < 3)
                        projectile = Math.min(3, stack.getCount() + projectile);
                    else
                        inv.removeItemNoUpdate(i);
                }
                else if (isSpecialWeapon(item))
                {
                    if (!special)
                        special = true;
                    else
                        inv.removeItemNoUpdate(i);
                }
            }
        }
    }

    protected static boolean isPlayerValid(Player player)
    {
        return player.isAlive() && !player.isSpectator() && !player.isCreative();
    }

    protected static boolean isModeActive()
    {
        return AVAServerConfig.isCompetitiveModeActivated();
    }
}
