package pellucid.ava.competitive_mode;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import pellucid.ava.gun.attachments.GunAttachmentManager;
import pellucid.ava.items.init.MeleeWeapons;
import pellucid.ava.items.init.Pistols;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.init.Rifles;
import pellucid.ava.items.init.Snipers;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.items.init.SubmachineGuns;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.IJsonSerializable;
import pellucid.ava.util.INBTSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Preset implements INBTSerializable<CompoundTag>, IJsonSerializable<Preset>
{
    public static final List<Preset> PRESETS = new ArrayList<>();
    public static final Map<Preset, Preset> DEFAULT_MAP = new HashMap<>();
    public static Preset F1_DEFAULT;
    public static Preset F2_DEFAULT;
    public static Preset F3_DEFAULT;
    public static Preset F1;
    public static Preset F2;
    public static Preset F3;
    private String name;
    private Predicate<Item> primaryCategoryPredicate;
    private Item primary = Items.AIR;
    private Item primary2 = Items.AIR;
    private Item secondary = Items.AIR;
    private Item melee = Items.AIR;
    private Item projectile = Items.AIR;
    private Item projectile2 = Items.AIR;
    private Item projectile3 = Items.AIR;
    private Item special = Items.AIR;
    private CompoundTag primaryModification = new CompoundTag();
    private CompoundTag primaryModification2 = new CompoundTag();

    public Preset(CompoundTag compound)
    {
        deserializeNBT(compound);
    }

    public Preset(String name, Predicate<Item> primaryCategoryPredicate, Supplier<Item> primary, Supplier<Item> primary2, Supplier<Item> secondary, Supplier<Item> melee, Supplier<Item> projectile, Supplier<Item> projectile2, Supplier<Item> projectile3, Supplier<Item> special)
    {
        this.name = name;
        this.primaryCategoryPredicate = primaryCategoryPredicate;
        this.primary = primary.get();
        this.primary2 = primary2.get();
        this.secondary = secondary.get();
        this.melee = melee.get();
        this.projectile = projectile.get();
        this.projectile2 = projectile2.get();
        this.projectile3 = projectile3.get();
        this.special = special.get();
    }

    public Predicate<Item> getPrimaryCategoryPredicate()
    {
        return primaryCategoryPredicate;
    }

    public Preset getDefault()
    {
        return DEFAULT_MAP.getOrDefault(this, this);
    }

    public Preset toDefault()
    {
        Preset d = getDefault();
        setPrimary(d.getPrimary());
        setPrimary2(d.getPrimary2());
        setSecondary(d.getSecondary());
        setMelee(d.getMelee());
        setProjectile(d.getProjectile());
        setProjectile2(d.getProjectile2());
        setProjectile3(d.getProjectile3());
        setSpecial(d.getSpecial());
        setPrimaryModification(d.getPrimaryModification().copy());
        setPrimaryModification2(d.getPrimaryModification2().copy());
        return this;
    }

    public static void init()
    {
        F1_DEFAULT = new Preset("point_man", SubmachineGuns.ITEM_SUBMACHINE_GUNS::contains, SubmachineGuns.MP5K, SubmachineGuns.REMINGTON870, Pistols.P226, MeleeWeapons.FIELD_KNIFE, Projectiles.M18_GREY, Projectiles.M116A1, Projectiles.M67, SpecialWeapons.BINOCULAR);
        F2_DEFAULT = new Preset("rifle_man", Rifles.ITEM_RIFLES::contains, Rifles.SCAR_L, Rifles.M4A1, Pistols.P226, MeleeWeapons.FIELD_KNIFE, Projectiles.M18_GREY, Projectiles.M116A1, Projectiles.M67, SpecialWeapons.BINOCULAR);
        F3_DEFAULT = new Preset("sniper", Snipers.ITEM_SNIPERS::contains, Snipers.M24, Snipers.FR_F2, Pistols.P226, MeleeWeapons.FIELD_KNIFE, Projectiles.M18_GREY, Projectiles.M116A1, Projectiles.MK3A2, SpecialWeapons.BINOCULAR);
        PRESETS.add(F1 = new Preset("point_man", SubmachineGuns.ITEM_SUBMACHINE_GUNS::contains, SubmachineGuns.X95R, SubmachineGuns.REMINGTON870, Pistols.BERETTA_92FS, MeleeWeapons.FIELD_KNIFE, Projectiles.M18_GREY, Projectiles.M116A1, Projectiles.M67, SpecialWeapons.BINOCULAR));
        PRESETS.add(F2 = new Preset("rifle_man", Rifles.ITEM_RIFLES::contains, Rifles.M4A1, Rifles.M16_VN, Pistols.COLT_SAA, MeleeWeapons.FIELD_KNIFE, Projectiles.M18_GREY, Projectiles.M116A1, Projectiles.M67, SpecialWeapons.BINOCULAR));
        PRESETS.add(F3 = new Preset("sniper", Snipers.ITEM_SNIPERS::contains, Snipers.FR_F2, Snipers.SR_25, Pistols.FN57, MeleeWeapons.FIELD_KNIFE, Projectiles.M18_GREY, Projectiles.M116A1, Projectiles.MK3A2, SpecialWeapons.BINOCULAR));
        DEFAULT_MAP.put(F1, F1_DEFAULT);
        DEFAULT_MAP.put(F2, F2_DEFAULT);
        DEFAULT_MAP.put(F3, F3_DEFAULT);
    }

    // Side Safe
    public static final Map<Item, CompoundTag> ATTACHMENTS = new HashMap<>();

    @Override
    public CompoundTag serializeNBT()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.ITEM.write(compound, "primary", primary);
        DataTypes.ITEM.write(compound, "primary2", primary2);
        DataTypes.ITEM.write(compound, "secondary", secondary);
        DataTypes.ITEM.write(compound, "melee", melee);
        DataTypes.ITEM.write(compound, "projectile", projectile);
        DataTypes.ITEM.write(compound, "projectile2", projectile2);
        DataTypes.ITEM.write(compound, "projectile3", projectile3);
        DataTypes.ITEM.write(compound, "special", special);
        DataTypes.COMPOUND.write(compound, "primaryModification", ATTACHMENTS.getOrDefault(primary, new CompoundTag()));
        DataTypes.COMPOUND.write(compound, "primaryModification2", ATTACHMENTS.getOrDefault(primary2, new CompoundTag()));
        return compound;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt)
    {
        primary = DataTypes.ITEM.read(nbt, "primary");
        primary2 = DataTypes.ITEM.read(nbt, "primary2");
        secondary = DataTypes.ITEM.read(nbt, "secondary");
        melee = DataTypes.ITEM.read(nbt, "melee");
        projectile = DataTypes.ITEM.read(nbt, "projectile");
        projectile2 = DataTypes.ITEM.read(nbt, "projectile2");
        projectile3 = DataTypes.ITEM.read(nbt, "projectile3");
        special = DataTypes.ITEM.read(nbt, "special");
        primaryModification = DataTypes.COMPOUND.read(nbt, "primaryModification");
        primaryModification2 = DataTypes.COMPOUND.read(nbt, "primaryModification2");
    }

    @Override
    public void writeToJson(JsonObject json)
    {
        DataTypes.ITEM.write(json, "primary", primary);
        DataTypes.ITEM.write(json, "primary2", primary2);
        DataTypes.ITEM.write(json, "secondary", secondary);
        DataTypes.ITEM.write(json, "melee", melee);
        DataTypes.ITEM.write(json, "projectile", projectile);
        DataTypes.ITEM.write(json, "projectile2", projectile2);
        DataTypes.ITEM.write(json, "projectile3", projectile3);
        DataTypes.ITEM.write(json, "special", special);
    }

    @Override
    public void readFromJson(JsonElement json)
    {
        JsonObject obj = json.getAsJsonObject();
        primary = DataTypes.ITEM.read(obj.get("primary"));
        primary2 = DataTypes.ITEM.read(obj.get("primary2"));
        secondary = DataTypes.ITEM.read(obj.get("secondary"));
        melee = DataTypes.ITEM.read(obj.get("melee"));
        projectile = DataTypes.ITEM.read(obj.get("projectile"));
        projectile2 = DataTypes.ITEM.read(obj.get("projectile2"));
        projectile3 = DataTypes.ITEM.read(obj.get("projectile3"));
        special = DataTypes.ITEM.read(obj.get("special"));
    }

    public String getName()
    {
        return name;
    }

    public Item getPrimary()
    {
        return primary;
    }

    public ItemStack getPrimaryStack()
    {
        return new GunAttachmentManager(new ItemStack(primary), ATTACHMENTS.getOrDefault(primary, new CompoundTag())).save();
    }

    public void setPrimary(Item primary)
    {
        this.primary = primary;
    }

    public Item getPrimary2()
    {
        return primary2;
    }

    public ItemStack getPrimaryStack2()
    {
        return new GunAttachmentManager(new ItemStack(primary2), ATTACHMENTS.getOrDefault(primary2, new CompoundTag())).save();
    }

    public void setPrimary2(Item primary2)
    {
        this.primary2 = primary2;
    }

    public Item getSecondary()
    {
        return secondary;
    }

    public void setSecondary(Item secondary)
    {
        this.secondary = secondary;
    }

    public Item getMelee()
    {
        return melee;
    }

    public void setMelee(Item melee)
    {
        this.melee = melee;
    }

    public Item getProjectile()
    {
        return projectile;
    }

    public void setProjectile(Item projectile)
    {
        this.projectile = projectile;
    }

    public Item getProjectile2()
    {
        return projectile2;
    }

    public void setProjectile2(Item projectile2)
    {
        this.projectile2 = projectile2;
    }

    public Item getProjectile3()
    {
        return projectile3;
    }

    public void setProjectile3(Item projectile3)
    {
        this.projectile3 = projectile3;
    }

    public Item getSpecial()
    {
        return special;
    }

    public void setSpecial(Item special)
    {
        this.special = special;
    }

    public CompoundTag getPrimaryModification()
    {
        return primaryModification;
    }

    public void setPrimaryModification(CompoundTag primaryModification)
    {
        this.primaryModification = primaryModification;
    }

    public CompoundTag getPrimaryModification2()
    {
        return primaryModification2;
    }

    public void setPrimaryModification2(CompoundTag primaryModification2)
    {
        this.primaryModification2 = primaryModification2;
    }

    @Override
    public String toString()
    {
        return "Preset{" +
                "name='" + name + '\'' +
                ", primaryPredicate=" + primaryCategoryPredicate +
                ", primary=" + primary +
                ", primary2=" + primary2 +
                ", secondary=" + secondary +
                ", melee=" + melee +
                ", projectile=" + projectile +
                ", projectile2=" + projectile2 +
                ", projectile3=" + projectile3 +
                ", special=" + special +
                ", primaryModification=" + primaryModification +
                ", primaryModification2=" + primaryModification2 +
                '}';
    }
}
