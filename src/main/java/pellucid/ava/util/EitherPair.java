package pellucid.ava.util;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class EitherPair<T> extends Pair<T, T> implements Function<Boolean, T>, Supplier<T>, BiConsumer<Boolean, T>
{
    private final Supplier<Boolean> defaultBool;

    protected EitherPair(T t, T t2)
    {
        this(t, t2, () -> true);
    }

    protected EitherPair(T t, T t2, Supplier<Boolean> defaultBool)
    {
        super(t, t2);
        this.defaultBool = defaultBool;
    }

    @Override
    public void accept(Boolean aBoolean, T t)
    {
        if (aBoolean)
            setA(t);
        else
            setB(t);
    }

    @Override
    public T apply(Boolean aBoolean)
    {
        return Pair.or(this, aBoolean);
    }

    @Override
    public T get()
    {
        return apply(defaultBool.get());
    }
}
