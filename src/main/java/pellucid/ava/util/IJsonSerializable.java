package pellucid.ava.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.joml.Vector3f;

public interface IJsonSerializable<T extends IJsonSerializable<T>>
{

    void writeToJson(JsonObject json);
    default JsonObject writeToJsonR()
    {
        return writeToJsonR(new JsonObject());
    }
    default JsonObject writeToJsonR(JsonObject json)
    {
        writeToJson(json);
        return json;
    }

    default void saveTo(JsonObject json, String name)
    {
        JsonObject json2 = new JsonObject();
        writeToJson(json2);
        json.add(name, json2);
    }

    void readFromJson(JsonElement json);

    static <F extends IJsonSerializable<F>> F create(F f, JsonElement element)
    {
        f.readFromJson(element);
        return f;
    }

    static JsonArray vector3fW(Vector3f vec)
    {
        JsonArray array = new JsonArray();
        array.add(vec.x);
        array.add(vec.y);
        array.add(vec.z);
        return array;
    }

    static Vector3f vector3fR(JsonArray array)
    {
        return new Vector3f(array.get(0).getAsFloat(), array.get(1).getAsFloat(), array.get(2).getAsFloat());
    }
}
