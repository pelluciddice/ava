package pellucid.ava.util.porting;

import net.minecraft.util.Mth;
import org.joml.Vector3f;

public class LazyPorting
{
    public static Vector3f clampVector3f(Vector3f vec, double min, double max)
    {
        vec.set(Mth.clamp(vec.x, min, max), Mth.clamp(vec.y, min, max), Mth.clamp(vec.z, min, max));
        return vec;
    }
}
