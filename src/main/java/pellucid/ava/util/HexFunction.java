package pellucid.ava.util;

public interface HexFunction<A, B, C, D, E, F, T>
{
    T apply(A a, B b, C c, D d, E e, F f);
}
