package pellucid.ava.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.Window;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Axis;
import net.minecraft.client.KeyMapping;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.multiplayer.ClientPacketListener;
import net.minecraft.client.multiplayer.PlayerInfo;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.locale.Language;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.FormattedText;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import net.neoforged.neoforge.client.event.ModelEvent;
import net.neoforged.neoforge.client.event.RenderFrameEvent;
import net.neoforged.neoforge.event.tick.PlayerTickEvent;
import net.neoforged.neoforge.network.PacketDistributor;
import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.animation.AVABobAnimator;
import pellucid.ava.client.animation.AVARunTransistor;
import pellucid.ava.client.inputs.ForceKeyMapping;
import pellucid.ava.client.overlay.KillTips;
import pellucid.ava.client.renderers.AVABakedItemModel;
import pellucid.ava.client.renderers.AVARenderTypes;
import pellucid.ava.client.renderers.AVARenderer;
import pellucid.ava.client.renderers.AVATPRenderer;
import pellucid.ava.client.renderers.AnimationFactory;
import pellucid.ava.client.renderers.HUDIndicators;
import pellucid.ava.client.renderers.HitEffect;
import pellucid.ava.client.renderers.Perspective;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.client.renderers.models.guns.RegularGunModel;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.functionalities.ICustomOnHitEffect;
import pellucid.ava.events.ClientModEventBus;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesClientManager;
import pellucid.ava.items.armours.models.AVAArmourModel;
import pellucid.ava.items.functionalities.ICustomModel;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.packets.PlaySoundMessage;
import pellucid.ava.player.status.ItemStatusManager;
import pellucid.ava.sounds.AVASounds;

import javax.annotation.Nullable;
import java.awt.Color;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static org.lwjgl.glfw.GLFW.*;
import static pellucid.ava.util.AVACommonUtil.*;
import static pellucid.ava.util.AVAConstants.*;

@EventBusSubscriber(Dist.CLIENT)
public class AVAClientUtil
{
    public static int ACTION_COOLDOWN = 0;
    public static final KillTips KILLTIPS = new KillTips();
    public static boolean USE_TEXTURE_MODEL_GUI = true;
    public static boolean USE_FAST_ASSETS = false;
    private static Screen DEFERRED_SCREEN = null;

    public static void setDeferredScreen(Screen deferredScreen)
    {
        DEFERRED_SCREEN = deferredScreen;
    }

    public static Screen getDeferredScreen()
    {
        Screen screen = DEFERRED_SCREEN;
        DEFERRED_SCREEN = null;
        return screen;
    }

    public static boolean isCompetitiveModeEnabled()
    {
        return AVAServerConfig.isCompetitiveModeActivated();
    }

    public static final List<LivingEntity> TEAMMATES = new ArrayList<>();

    public static final int CAMERA_VERTICAL_SHAKE_TIME_MAX = 10;
    public static final int CAMERA_VERTICAL_SHAKE_DISTANCE_MAX = 12;
    public static final float CAMERA_VERTICAL_SHAKE_STRONG = 1.0F;
    public static final float CAMERA_VERTICAL_SHAKE_WEAK = 15.0F;
    public static int CAMERA_VERTICAL_SHAKE_TIME = 0;
    public static float CAMERA_VERTICAL_SHAKE_WEAKNESS = 0;

    public static int STOP_SPRINTING = 2;

    public static boolean shouldStopSprinting()
    {
        return STOP_SPRINTING > 0;
    }

    public static void postClientTick(Minecraft minecraft, LocalPlayer player)
    {
        Screen screen = getDeferredScreen();
        KILLTIPS.tick();
        if (screen != null)
            Minecraft.getInstance().setScreen(screen);
        if (AVACommonUtil.absSimilar(YAW_ROTATED, YAW_TO_ROTATE))
        {
            YAW_TO_ROTATE = 0.0D;
            YAW_ROTATED = 0.0D;
        }
        if (AVACommonUtil.absSimilar(PITCH_ROTATED, PITCH_TO_ROTATE))
        {
            PITCH_TO_ROTATE = 0.0D;
            PITCH_ROTATED = 0.0D;
        }
        Level world = player.level();
        AVAWeaponUtil.TeamSide side = AVAWeaponUtil.TeamSide.getSideFor(player);
        if (AVAServerConfig.isCompetitiveModeActivated() && side != null && world.getGameTime() % 10 == 0 && ClientModEventBus.TAB.isPressed())
        {
            TEAMMATES.clear();
            TEAMMATES.addAll(AVACommonUtil.collect(AVACommonUtil.toList(((ClientLevel) world).entitiesForRendering()), (e) -> {
                return AVATPRenderer.validAlly(player, e).orElse(null);
            }));
        }
        if (CAMERA_VERTICAL_SHAKE_TIME > 0)
            CAMERA_VERTICAL_SHAKE_TIME--;
        if (STOP_SPRINTING > 0)
        {
            STOP_SPRINTING--;
            if (STOP_SPRINTING <= 0)
                player.setSprinting(false);
        }
    }

    public static void stopSprinting(int delay)
    {
        STOP_SPRINTING = delay;
    }

    private static double YAW_TO_ROTATE = 0.0D;
    private static double YAW_ROTATED = 0.0D;
    private static double PITCH_TO_ROTATE = 0.0D;
    private static double PITCH_ROTATED = 0.0D;

    public static boolean PENDING_REFRESH = false;

    public static void clientPlayerTick(PlayerTickEvent.Pre event)
    {
        Player player = event.getEntity();
        if (AVAWeaponUtil.isValidEntity(player))
        {
            AVARunTransistor.INSTANCE.tick(player);
            ItemStack stack = player.getMainHandItem();
            for (ItemStatusManager<?> manager : ItemStatusManager.MANAGERS)
            {
                if (!manager.isRightItem(stack))
                    continue;
                manager.input.get().tick(player);
            }
        }
    }

    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        ClientLevel world = minecraft.level;
        if (world == null)
        {
            KILLTIPS.clear();
            AVAWorldData.getInstances().clear();
            return;
        }
        if (player == null || !player.isAlive())
            return;
        ForceKeyMapping.KEY_MAPPINGS.forEach(ForceKeyMapping::tick);
        HUDIndicators.tick(minecraft, player);
        AnimationFactory.tick(minecraft, player);
        AVABobAnimator.INSTANCE.tick(player);
    }

    @SubscribeEvent
    public static void onClientTick(ClientTickEvent.Post event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        LocalPlayer player = minecraft.player;
        ClientLevel world = minecraft.level;
        if (world == null)
        {
            KILLTIPS.clear();
            AVAWorldData.getInstances().clear();
            return;
        }
        if (player == null || !player.isAlive())
            return;
        AVAWorldData cap = AVAWorldData.getInstance(world);
        PlayerAction capability = cap(player);
        ItemStack stack = getHeldStack(player);
        boolean focused = minecraft.isWindowActive() && minecraft.screen == null;
        AVAModelTypes.TYPES.forEach((t) -> t.getStatusManager().tick(player));
        LoadingImagesClientManager.INSTANCE.tick();
        if (world != null && cap.requireRenderUpdate)
        {
            cap.requireRenderUpdate = false;
            LevelRenderer renderer = minecraft.levelRenderer;
            renderer.allChanged();
        }
        if (ACTION_COOLDOWN > 0)
            ACTION_COOLDOWN--;
        AVAWorldData data = AVAWorldData.getInstance(world);
        data.tick(player.level());
        AVAClientUtil.postClientTick(minecraft, player);
        AVAWeaponUtil.calculateRecoilRotations(player, capability, stack).ifPresent((pair) -> AVAClientUtil.rotateTowardsSmooth(pair.getA(), pair.getB()));
        if (!focused)
            ACTION_COOLDOWN = 10;
        if (isFullEquipped(player) && isCaughtByUAV(player))
        {
            Integer uav = data.uavC.get(player.getId());
            if (uav != null && (uav == 139 || uav == 134 || uav == 119 || uav == 114 || uav == 85 || uav == 80 || uav == 65 || uav == 60))
                player.playSound(AVASounds.UAV_CAPTURED.get(), 1.0F, 1.0F);
        }
        capability.getHurtInfo().update();
        if (capability.getFlashDuration() > 0)
            capability.setFlashDuration(capability.getFlashDuration() - 1);
    }

    @SubscribeEvent
    public static void onRenderTick(RenderFrameEvent.Pre event) throws IOException
    {
        Minecraft minecraft = Minecraft.getInstance();
        Player player = minecraft.player;
        if (AVAWeaponUtil.isValidEntity(player))
            onPreRenderTick(minecraft, player, event.getPartialTick());
    }

    public static void onPreRenderTick(Minecraft minecraft, Player player, float partialTicks) throws IOException
    {
        if (PENDING_REFRESH)
        {
            RegularGunModel.refreshAll();
            AVABakedItemModel.refreshAll();
            PENDING_REFRESH = false;
            LoadingImagesClientManager.INSTANCE.init(Path.of(minecraft.gameDirectory.getAbsolutePath()));
            player.displayClientMessage(Component.literal("Refresh Completed"), false);
            AVAModelTypes.TYPES.forEach((t) -> t.getModelManager().generate());
        }
        double yaw = 0.0D;
        double pitch = 0.0D;
        if (YAW_TO_ROTATE != 0.0D)
        {
            double d = YAW_TO_ROTATE * partialTicks;
            yaw = d - YAW_ROTATED;
            YAW_ROTATED = d;
        }
        if (PITCH_TO_ROTATE != 0.0D)
        {
            double d = PITCH_TO_ROTATE * partialTicks;
            pitch = d - PITCH_ROTATED;
            PITCH_ROTATED = d;
        }
        if (yaw != 0.0D || pitch != 0.0D)
            player.turn(yaw, pitch);
    }

    //    private static boolean APPLY_RECORD = true;
    //    @SubscribeEvent
    //    public static void onKeyPress(InputEvent.KeyInputEvent event)
    //    {
    //        PlayerEntity player = Minecraft.getInstance().player;
    //        if (player != null && event.getAction() == 1)
    //        {
    //            if (APPLY_RECORD)
    //                player.sendMessage(new StringTextComponent("Pressed " + (char) event.getKey()), player.getUniqueID());
    //            if (event.getKey() == GLFW.GLFW_KEY_F6)
    //            {
    //                Minecraft.getInstance().ingameGUI.getChatGUI().clearChatMessages(true);
    //                APPLY_RECORD = !APPLY_RECORD;
    //            }
    //        }
    //    }

    public static void rotateTowardsSmooth(double yaw, double pitch)
    {
        YAW_TO_ROTATE += yaw / 0.15D;
        PITCH_TO_ROTATE += pitch / 0.15D;
    }

    public static void rotateTowards(Player player, double yaw, double pitch)
    {
                player.turn(yaw / 0.15D, pitch / 0.15D);
//        rotateTowardsSmooth(yaw, pitch);
    }

    public static boolean isSamePlayer(Player player, Player player2)
    {
        return player.getId() == player2.getId();
    }

    public static void playSoundOnServer(boolean includeLocal, SoundEvent... sounds)
    {
        List<String> soundNames = new ArrayList<>();
        for (SoundEvent sound : sounds)
            soundNames.add(BuiltInRegistries.SOUND_EVENT.getKey(sound).toString());
        PacketDistributor.sendToServer(new PlaySoundMessage(includeLocal, soundNames.toArray(new String[0])));
    }

    public static void onPresetUpdate()
    {
        HUDIndicators.Preset.PRESET_SET_DISPLAY = 60;
        Player player = Minecraft.getInstance().player;
        if (player != null)
            player.level().playSound(player, player, AVASounds.SELECT_PRESET.get(), SoundSource.PLAYERS, 1.0F, 1.0F);
    }

    public static void revertBobbing(PoseStack matrixStackIn, float partialTicks, Runnable render)
    {
        boolean bob = Minecraft.getInstance().options.bobView.get();
        boolean shouldRevert = bob && Minecraft.getInstance().getCameraEntity() instanceof Player;
        if (shouldRevert)
        {
            Player playerentity = (Player) Minecraft.getInstance().getCameraEntity();
            float f = playerentity.walkDist - playerentity.walkDistO;
            float f1 = -(playerentity.walkDist + f * partialTicks);
            float f2 = Mth.lerp(partialTicks, playerentity.oBob, playerentity.bob);
            matrixStackIn.translate(-(Mth.sin(f1 * (float) Math.PI) * f2 * 0.5F), -(-Math.abs(Mth.cos(f1 * (float) Math.PI) * f2)), 0.0D);
            matrixStackIn.mulPose(Axis.ZP.rotationDegrees(-Mth.sin(f1 * (float) Math.PI) * f2 * 3.0F));
            matrixStackIn.mulPose(Axis.XP.rotationDegrees(-Math.abs(Mth.cos(f1 * (float) Math.PI - 0.2F) * f2) * 5.0F));
        }
        render.run();
        Minecraft.getInstance().options.bobView.set(bob);
    }

    @OnlyIn(Dist.CLIENT)
    public static void drawTransparent(boolean enable)
    {
        if (enable)
        {
            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();
            RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        }
        else
        {
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            RenderSystem.disableBlend();
        }
    }

    public static void fill(PoseStack stack, int screenWidth, int screenHeight, int xOffset, int yOffset, int width, int height, int colour)
    {
        float x = screenWidth / 2.0F + xOffset;
        float y = screenHeight + yOffset;
        fill(stack, x - width / 2.0F, y / 2.0F, width, height, colour);
    }

    public static void fill(PoseStack stack, float x, float y, float width, float height, int colour)
    {
        fillCoord(stack, x, y, x + width, y + height, colour);
    }

    public static void fillCoord(PoseStack stack, float x, float y, float x2, float y2, int colour)
    {
        fill(stack, x, y, x, y2, x2, y, x2, y2, colour);
    }

    public static void fill(PoseStack stack, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, int c)
    {
        fill(stack, x1, y1, x2, y2, x3, y3, x4, y4, (c >> 16 & 255), (c >> 8 & 255), (c & 255), (c >> 24 & 255) / 255.0F);
    }

    public static void fill(PoseStack stack, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float r, float g, float b, float a)
    {
        RenderSystem.enableBlend();
        RenderSystem.setShaderColor(r / 255.0F, g / 255.0F, b / 255.0F, a);
        blit(stack, AVARenderer.CROSSHAIR, x1, y1, x2, y2, x3, y3, x4, y4);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.disableBlend();
    }

    public static boolean isCaughtByUAV(LivingEntity entity)
    {
        AVAWorldData data = AVAWorldData.getInstance(entity.level());
        return data.uavC.containsKey(entity.getId()) || data.uavEscortC.containsKey(entity.getId());
    }

    public static boolean isCaughtX9(LivingEntity entity)
    {
        Level world = Minecraft.getInstance().level;
        if (world == null)
            return false;
        //        return cap(world).getX9C().containsKey(entity.getId());
        return AVAWorldData.getInstance(world).x9C.containsKey(entity.getId());
    }

    @OnlyIn(Dist.CLIENT)
    public static void fillScreen(int screenWidth, int screenHeight)
    {
        Tesselator tessellator = Tesselator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(0.0D, screenHeight, -90.0D).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(screenWidth, screenHeight, -90.0D).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(screenWidth, 0.0D, -90.0D).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(0.0D, 0.0D, -90.0D).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
    }

    public static void fillCentredMaxWithFilledSidesAndBob(Player player, PoseStack stack, int screenWidth, int screenHeight, ResourceLocation texture)
    {
        drawTransparent(true);
        bobRenderable(player, stack, () -> fillCentredMaxWithFilledSides(stack, screenWidth, screenHeight, texture));
        drawTransparent(false);
    }

    public static void bobRenderable(Player player, PoseStack stack, Runnable render)
    {
        stack.pushPose();
        Vector3f scale = !AVACommonUtil.isMovingOnGroundClient(player) ? new Vector3f(0.01F) : new Vector3f(35, 10, 0);
        transferStack(stack, new Perspective(new Vector3f(), AVABobAnimator.INSTANCE.getBob().mul(scale), new Vector3f(1)));
        render.run();
        stack.popPose();
    }

    @OnlyIn(Dist.CLIENT)
    public static void fillCentredMaxWithFilledSides(PoseStack stack, int screenWidth, int screenHeight, ResourceLocation image)
    {
        fillCentredMax(stack, screenWidth, screenHeight, image);
        fillBlankSides(stack, screenWidth, screenHeight);
    }

    @OnlyIn(Dist.CLIENT)
    public static void fillCentredMax(PoseStack stack, int screenWidth, int screenHeight, ResourceLocation image)
    {
        Tesselator tessellator = Tesselator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuilder();
        int w = (screenWidth - screenHeight) / 2;
        int p = screenWidth - w;
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, image);
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        Matrix4f mat = stack.last().pose();
        bufferbuilder.vertex(mat, w, screenHeight, -90.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, p, screenHeight, -90.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, p, 0.0F, -90.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(mat, w, 0.0F, -90.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
    }

    @OnlyIn(Dist.CLIENT)
    public static void fillBlankSides(PoseStack stack, int screenWidth, int screenHeight)
    {
        RenderSystem.setShaderTexture(0, AVARenderer.BLACK);
        int x = (screenWidth - screenHeight) / 2;
        int x2 = screenWidth - x;
        Matrix4f mat = stack.last().pose();
        Tesselator tessellator = Tesselator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        float extra = 100;
        bufferbuilder.vertex(mat, -extra, screenHeight + extra, -90.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, x, screenHeight + extra, -90.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, x, -extra, -90.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(mat, -extra, -extra, -90.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
        bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(mat, x2, screenHeight + extra, -90.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, screenWidth + extra, screenHeight + extra, -90.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, screenWidth + extra, -extra, -90.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(mat, x2, -extra, -90.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
        bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(mat, x, 0.0F, -90.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, x2, 0.0F, -90.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, x2, -extra, -90.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(mat, x, -extra, -90.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
        bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(mat, x, screenHeight + extra, -90.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, x2, screenHeight + extra, -90.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(mat, x2, screenHeight, -90.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(mat, x, screenHeight, -90.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
    }

    @OnlyIn(Dist.CLIENT)
    public static void rotateRelative(Entity origin, Vec3 target, PoseStack stack, float screenWidth, float screenHeight, float scale, Function<Float, Float> smoother)
    {
        if (smoother == null)
            smoother = (f) -> f;
        float w = screenWidth / 2.0F;
        float h = screenHeight / 2.0F;
        stack.translate(w, h, 0.0F);
        stack.scale(scale, scale, scale);
        stack.mulPose(Axis.ZP.rotationDegrees(smoother.apply(AVAWeaponUtil.getRelativeDegreesHorizontal(origin, target))));
        stack.translate(-w, -h, 0.0F);
    }

    public static void blitCentreRelative(PoseStack stack, Consumer<PoseStack> transforms, float screenWidth, float screenHeight)
    {
        stack.pushPose();
        transforms.accept(stack);
        float w = screenWidth / 4.0F;
        blit(stack, null, w, screenHeight / 2.0F - w, w * 3, screenHeight / 2.0F + w);
        stack.popPose();
    }

    public static void blit(PoseStack stack, @Nullable ResourceLocation texture, float x1, float y1, float x2, float y2)
    {
        blit(stack, texture, x1, y1, x1, y2, x2, y1, x2, y2);
    }

    public static void blit(PoseStack stack, @Nullable ResourceLocation texture, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
    {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        if (texture != null)
            RenderSystem.setShaderTexture(0, texture);
        PoseStack.Pose matrix = stack.last();
        Tesselator tessellator = Tesselator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(matrix.pose(), x2, y2, 0.0F).uv(0.0F, 1.0F).endVertex();
        bufferbuilder.vertex(matrix.pose(), x4, y4, 0.0F).uv(1.0F, 1.0F).endVertex();
        bufferbuilder.vertex(matrix.pose(), x3, y3, 0.0F).uv(1.0F, 0.0F).endVertex();
        bufferbuilder.vertex(matrix.pose(), x1, y1, 0.0F).uv(0.0F, 0.0F).endVertex();
        tessellator.end();
    }

    public static void blitCentreRelative(GuiGraphics stack, ResourceLocation texture, Consumer<PoseStack> transforms, int textureDimension, int x, int y, int width, int height, float textureOffsetX, float textureOffsetY, int textureSizeX, int textureSizeY, float screenWidth, float screenHeight)
    {
        stack.pose().pushPose();
        transforms.accept(stack.pose());
        stack.blit(texture, x, y, width, height, textureOffsetX, textureOffsetY, textureSizeX, textureSizeY, textureDimension, textureDimension);
        stack.pose().popPose();
    }

    public static void drawHollowCircle(GuiGraphics stack, float w, float h, float oX, float oY, double fromDegrees, double toDegrees, double detail, int r, int g, int b, float a)
    {
        int a2 = (int) (a * 255.0F);
        drawLineShaped2D(w, (builder) ->
        {
            PoseStack.Pose matrix = stack.pose().last();
            for (double degree = fromDegrees; degree < toDegrees; degree += detail)
                builder.vertex(matrix.pose(), oX + (float) (Math.sin(Math.toRadians(degree)) * h), oY + (float) -(Math.cos(Math.toRadians(degree)) * h), 10.0F).color(r, g, b, a2).normal(1.0F, 0.0F, 0.0F).endVertex();
        });
        drawLineShaped2D(w, (builder) ->
        {
            PoseStack.Pose matrix = stack.pose().last();
            for (double degree = fromDegrees; degree < toDegrees; degree += detail)
                builder.vertex(matrix.pose(), oX + (float) (Math.sin(Math.toRadians(degree)) * h), oY + (float) -(Math.cos(Math.toRadians(degree)) * h), 10.0F).color(r, g, b, a2).normal(0.0F, 1.0F, 0.0F).endVertex();
        });
    }

    public static void drawRect(PoseStack stack, float x1, float y1, float x2, float y2, int r, int g, int b, float a)
    {
        fill(stack, x1, y1, x1, y2, x2, y1, x2, y2, r, g, b, a);
    }

    public static void drawRect3D(PoseStack stack, float x1, float y1, float x2, float y2, int r, int g, int b, float a)
    {
        int a2 = (int) (a * 255.0F);

        RenderSystem.depthMask(false);
        RenderSystem.disableCull();
        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();

        RenderSystem.setShader(GameRenderer::getPositionColorShader);

        Tesselator tesselator = RenderSystem.renderThreadTesselator();
        BufferBuilder builder = tesselator.getBuilder();
        builder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);

        Matrix4f matrix = stack.last().pose();
        builder.vertex(matrix, x2, y2, 0.0F).color(r, g, b, a2).endVertex();
        builder.vertex(matrix, x1, y2, 0.0F).color(r, g, b, a2).endVertex();
        builder.vertex(matrix, x1, y1, 0.0F).color(r, g, b, a2).endVertex();
        builder.vertex(matrix, x2, y1, 0.0F).color(r, g, b, a2).endVertex();

        tesselator.end();
        RenderSystem.depthMask(true);
        RenderSystem.enableCull();
        RenderSystem.disableBlend();
    }

    public static void draw3DLine(PoseStack stack, @Nullable RenderType type, Vec3 from, Vec3 to, int r, int g, int b, float a, @Nullable MultiBufferSource.BufferSource impl)
    {
        try
        {
            if (impl == null)
                impl = Minecraft.getInstance().renderBuffers().bufferSource();

            if (type == null)
                type = RenderType.lines();

            Matrix4f matrix = stack.last().pose();
            Matrix3f normal = stack.last().normal();
            Vec3 norm = to.subtract(from).normalize();

            VertexConsumer builder = impl.getBuffer(type);
            builder.vertex(matrix, (float) from.x, (float) from.y, (float) from.z).color(r, g, b, (int) (a * 255.0F)).normal(stack.last(), (float) norm.x, (float) norm.y, (float) norm.z).endVertex();
            builder.vertex(matrix, (float) to.x, (float) to.y, (float) to.z).color(r, g, b, (int) (a * 255.0F)).normal(stack.last(), (float) norm.x, (float) norm.y, (float) norm.z).endVertex();

            builder.endVertex();
            impl.endBatch();
        }
        catch (Exception ignored) {}
    }

    public static void draw2DLine(GuiGraphics stack, float w, float x1, float y1, float x2, float y2, int r, int g, int b, float a)
    {
        int a2 = (int) (a / 255.0F);
        drawLineShaped2D(w, (builder) ->
        {
            Matrix4f matrix = stack.pose().last().pose();
            builder.vertex(matrix, x1, y1, 0.0F).color(r, g, b, a2).normal(1.0F, 1.0F, 0.0F).endVertex();
            builder.vertex(matrix, x2, y2, 0.0F).color(r, g, b, a2).normal(1.0F, 1.0F, 0.0F).endVertex();
        });
    }

    public static void drawLineShaped2D(float w, Consumer<VertexConsumer> vertex)
    {
        GlStateManager._depthMask(false);
        GlStateManager._disableCull();
        RenderSystem.setShader(GameRenderer::getRendertypeLinesShader);
        Tesselator tesselator = RenderSystem.renderThreadTesselator();
        BufferBuilder builder = tesselator.getBuilder();
        RenderSystem.lineWidth(w);
        builder.begin(VertexFormat.Mode.LINE_STRIP, DefaultVertexFormat.POSITION_COLOR_NORMAL);

        vertex.accept(builder);

        tesselator.end();
        RenderSystem.lineWidth(1.0F);
        GlStateManager._enableCull();
        GlStateManager._depthMask(true);
    }

    public static void drawLineShaped3D(GuiGraphics stack, @Nullable RenderType type, Consumer<VertexConsumer> vertex)
    {
        drawLineShaped3DWithBuffer(stack, type, vertex, Minecraft.getInstance().renderBuffers().bufferSource());
    }

    public static void drawLineShaped3DWithBuffer(GuiGraphics stack, @Nullable RenderType type, Consumer<VertexConsumer> vertex, @Nullable MultiBufferSource.BufferSource impl)
    {
        if (impl == null)
        {
            drawLineShaped3D(stack, type, vertex);
            return;
        }
        AVAClientUtil.drawTransparent(true);
        type = type == null ? AVARenderTypes.LINE_1_0D : type;

        VertexConsumer builder = impl.getBuffer(type);

        vertex.accept(builder);

        impl.endBatch(type);

        AVAClientUtil.drawTransparent(false);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
    }

    public static void scaleMatrix(PoseStack stack, float scale, float screenWidth, float screenHeight)
    {
        scaleMatrix(stack, scale, screenWidth, screenHeight, 0.0F, 0.0F);
    }

    public static void scaleMatrix(PoseStack stack, float scale, float screenWidth, float screenHeight, float width, float height)
    {
        float x = screenWidth / 2.0F;
        float y = screenHeight / 2.0F;
        float w = width / 2.0F;
        float h = height / 2.0F;
        stack.translate(x - w, y - height, 0.0F);
        stack.scale(scale, scale, scale);
        stack.translate(-x + width, -y + h, 0.0F);
    }

    public static boolean leftMouseDown()
    {
        return mouseDown(GLFW_MOUSE_BUTTON_LEFT);
    }

    public static boolean middleMouseDown()
    {
        return mouseDown(GLFW_MOUSE_BUTTON_MIDDLE);
    }

    public static boolean rightMouseDown()
    {
        return mouseDown(GLFW_MOUSE_BUTTON_RIGHT);
    }

    public static boolean mouseDown(int mouse)
    {
        return glfwGetMouseButton(Minecraft.getInstance().getWindow().getWindow(), mouse) == GLFW_PRESS;
    }

    public static boolean shiftDown()
    {
        return keyDown(GLFW_KEY_LEFT_SHIFT) || keyDown(GLFW_KEY_RIGHT_SHIFT);
    }

    public static boolean keyDown(int key)
    {
        return glfwGetKey(Minecraft.getInstance().getWindow().getWindow(), key) == GLFW_PRESS;
    }

    public static void bobMatrixStack(PoseStack stack, Player player, @Nullable ICustomModel modelHolder)
    {
        bobMatrixStack(stack, modelHolder == null ? new Vector3f(1.0F) : modelHolder.getStaticModel(player.getMainHandItem()).getBobScale(player, false));
    }

    public static void bobMatrixStack(PoseStack stack, Vector3f scale)
    {
        Vector3f tslt = AVABobAnimator.INSTANCE.getBob().mul(scale.mul(-0.05F, -0.05F, -0.075F));
        transferStack(stack, new Perspective(new Vector3f(), new Vector3f(tslt.x, tslt.z, tslt.y), new Vector3f(1)));
    }

    public static void rotateStack(PoseStack stack, float[] degree)
    {
        stack.mulPose(Axis.XP.rotationDegrees(degree[0]));
        stack.mulPose(Axis.YP.rotationDegrees(degree[1]));
        stack.mulPose(Axis.ZP.rotationDegrees(degree[2]));
    }

    public static void transferStack(PoseStack stack, Perspective perspective3f)
    {
        stack.translate(perspective3f.translation.x(), perspective3f.translation.y(), perspective3f.translation.z());
        rotateStack(stack, perspective3f.getRotation());
        stack.scale(perspective3f.scale.x(), perspective3f.scale.y(), perspective3f.scale.z());
    }

    public static void replaceModel(ModelEvent.ModifyBakingResult event, Item item)
    {
        if (item instanceof ICustomModel)
        {
            ModelResourceLocation modelLoc = new ModelResourceLocation(BuiltInRegistries.ITEM.getKey(item), "inventory");
            BakedModel origin = event.getModels().get(modelLoc);
            BakedModel newModel = ((ICustomModel) item).getCustomModel(origin);
            if (origin != null && newModel != null)
                event.getModels().put(modelLoc, newModel);
        }
    }

    @Nullable
    public static ModelResourceLocation getSimpleModelFor(Item item)
    {
        if (!(item instanceof AVAItemGun))
            return null;
        item = ((AVAItemGun) item).getMaster();
        String name = BuiltInRegistries.ITEM.getKey(item).getPath();
        return new ModelResourceLocation(AVA.MODID, name + "/" + name + "_simple", "inventory");
    }

    public static ModelResourceLocation getTextureModelFor(Item item)
    {
        String name = BuiltInRegistries.ITEM.getKey(item).getPath();
        return new ModelResourceLocation(AVA.MODID, name + "/" + name + "_texture", "inventory");
    }

    public static void copyArmourRotationFromBody(AVAArmourModel<?> model)
    {
        model.armorHead.copyFrom(model.head);
        model.armorBody.copyFrom(model.body);
        model.armorRightArm.copyFrom(model.rightArm);
        model.armorLeftArm.copyFrom(model.leftArm);
        model.armorRightLeg.copyFrom(model.rightLeg);
        model.armorLeftLeg.copyFrom(model.leftLeg);
        model.armorRightBoot.copyFrom(model.rightLeg);
        model.armorLeftBoot.copyFrom(model.leftLeg);
    }

    public static void forceEnable3DModel(Runnable action)
    {
        USE_TEXTURE_MODEL_GUI = false;
        action.run();
        USE_TEXTURE_MODEL_GUI = true;
    }

    public static void forceEnableFastAssets(Runnable action)
    {
        USE_FAST_ASSETS = true;
        action.run();
        USE_FAST_ASSETS = false;
    }

    public static void unpressKeybind(KeyMapping keyBinding)
    {
        while (keyBinding.consumeClick())
            keyBinding.setDown(false);
    }

    public static String getWeaponDisplayWithAmmoCount(ItemStack stack)
    {
        return stack.getHoverName().getString() + "[" + (stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) + stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0));
    }

    public static boolean isFocused()
    {
        return Minecraft.getInstance().isWindowActive() && Minecraft.getInstance().screen == null;
    }

    public static void transformItemMatrix(PoseStack stack, Consumer<PoseStack> transforms, Consumer<PoseStack> render)
    {
        stack.pushPose();
        transforms.accept(stack);
        render.accept(stack);
        stack.popPose();
    }

    public static void renderWrappedText(GuiGraphics stack, int x, int y, int maxTextWidth, Font font, int colour, FormattedText... textLines)
    {
        Window window = Minecraft.getInstance().getWindow();
        renderWrappedText(stack, Arrays.stream(textLines).toList(), x, y, window.getGuiScaledWidth(), window.getGuiScaledHeight(), maxTextWidth, font, colour);
    }

    public static void renderWrappedText(GuiGraphics mStack, List<? extends FormattedText> textLines, int x, int y, int screenWidth, int screenHeight, int maxTextWidth, Font font, int colour)
    {
        x -= 11;
        y += 7;
        if (!textLines.isEmpty())
        {
            RenderSystem.disableDepthTest();
            int tooltipTextWidth = 0;

            for (FormattedText textLine : textLines)
            {
                int textLineWidth = font.width(textLine);
                if (textLineWidth > tooltipTextWidth)
                    tooltipTextWidth = textLineWidth;
            }

            boolean needsWrap = false;

            int titleLinesCount = 1;
            int tooltipX = x + 12;
            if (tooltipX + tooltipTextWidth + 4 > screenWidth)
            {
                tooltipX = x - 16 - tooltipTextWidth;
                if (tooltipX < 4) // if the tooltip doesn't fit on the screen
                {
                    if (x > screenWidth / 2)
                        tooltipTextWidth = x - 12 - 8;
                    else
                        tooltipTextWidth = screenWidth - 16 - x;
                    needsWrap = true;
                }
            }

            if (maxTextWidth > 0 && tooltipTextWidth > maxTextWidth)
            {
                tooltipTextWidth = maxTextWidth;
                needsWrap = true;
            }

            if (needsWrap)
            {
                int wrappedTooltipWidth = 0;
                List<FormattedText> wrappedTextLines = new ArrayList<>();
                for (int i = 0; i < textLines.size(); i++)
                {
                    FormattedText textLine = textLines.get(i);
                    List<FormattedText> wrappedLine = font.getSplitter().splitLines(textLine, tooltipTextWidth, Style.EMPTY);
                    if (i == 0)
                        titleLinesCount = wrappedLine.size();

                    for (FormattedText line : wrappedLine)
                    {
                        int lineWidth = font.width(line);
                        if (lineWidth > wrappedTooltipWidth)
                            wrappedTooltipWidth = lineWidth;
                        wrappedTextLines.add(line);
                    }
                }
                tooltipTextWidth = wrappedTooltipWidth;
                textLines = wrappedTextLines;

                if (x > screenWidth / 2)
                    tooltipX = x - 16 - tooltipTextWidth;
                else
                    tooltipX = x + 12;
            }

            int tooltipY = y - 12;
            int tooltipHeight = 8;

            if (textLines.size() > 1)
            {
                tooltipHeight += (textLines.size() - 1) * 10;
                if (textLines.size() > titleLinesCount)
                    tooltipHeight += 2; // gap between title lines and next lines
            }

            if (tooltipY < 4)
                tooltipY = 4;
            else if (tooltipY + tooltipHeight + 4 > screenHeight)
                tooltipY = screenHeight - tooltipHeight - 4;

            mStack.pose().pushPose();
            Matrix4f mat = mStack.pose().last().pose();

            MultiBufferSource.BufferSource multibuffersource$buffersource = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
            mStack.pose().translate(0.0D, 0.0D, 400);

            for (int lineNumber = 0; lineNumber < textLines.size(); ++lineNumber)
            {
                FormattedText line = textLines.get(lineNumber);
                if (line != null)
                    font.drawInBatch(Language.getInstance().getVisualOrder(line), (float) tooltipX, (float) tooltipY, colour, false, mat, multibuffersource$buffersource, Font.DisplayMode.NORMAL, 0, 15728880);

                if (lineNumber + 1 == titleLinesCount)
                    tooltipY += 2;

                tooltipY += 10;
            }

            multibuffersource$buffersource.endBatch();
            mStack.pose().popPose();
            RenderSystem.enableDepthTest();
        }
    }

    public static void renderItemStack(GuiGraphics stack, Perspective perspective, ItemStack item, float x, float y, boolean flippled)
    {
        renderItemStack(stack, perspective, item, x, y, flippled, AVAClientUtil::forceEnable3DModel);
    }

    private static final Perspective Y_MIRROR = Perspective.rotation(0.0F, 180.0F, 0.0F);

    public static void renderItemStack(GuiGraphics stack, Perspective perspective, ItemStack item, float x, float y, boolean flipped, Consumer<Runnable> render)
    {
        stack.pose().pushPose();

        stack.pose().translate(x, y, -100.0F);
        float xS = perspective.scale.x();
        float yS = perspective.scale.y();
        stack.pose().translate(perspective.translation.x() / xS, perspective.translation.y() / yS, 0.0F);
        stack.pose().translate(-8.0F * xS, -8.0F * yS, 0.0F);
        stack.pose().scale(xS, yS, 1.0F);
        stack.pose().translate(8.0F, 8.0F, 100.0F);
        rotateStack(stack.pose(), perspective.getRotation());
        if (flipped)
            rotateStack(stack.pose(), Y_MIRROR.getRotation());
        stack.pose().translate(-8.0F, -8.0F, -100.0F);

        RenderSystem.applyModelViewMatrix();
        render.accept(() -> stack.renderFakeItem(item, 0, 0));

        stack.pose().popPose();
        RenderSystem.applyModelViewMatrix();
    }

    public static void scaleText(PoseStack stack, int x, int y, float xS, float yS, Runnable render)
    {
        stack.pushPose();
        stack.scale(xS, yS, 0.0F);
        stack.translate(x / xS, y / yS, 0.0F);
        render.run();
        stack.popPose();
    }

    public static void renderRightAlignedText(GuiGraphics stack, Font font, String text, int x, int y, int colour, boolean shadow)
    {
        positionRightAlignedText(stack.pose(), font.width(text), () -> {
            stack.drawString(font, text, x, y, colour, shadow);
        });
    }

    public static void renderRightAlignedText(GuiGraphics stack, Font font, Component text, int x, int y, int colour, boolean shadow)
    {
        positionRightAlignedText(stack.pose(), font.width(text), () -> {
            stack.drawString(font, text, x, y, colour, shadow);
        });
    }

    public static void positionRightAlignedText(PoseStack stack, int width, Runnable render)
    {
        stack.pushPose();
        stack.translate(-width, 0.0F, 0.0F);
        render.run();
        stack.popPose();
    }

    public static boolean inField(int mX, int mY, int x, int y, int x2, int y2)
    {
        return mX >= Math.min(x, x2) && mX <= Math.max(x, x2) && mY >= Math.min(y, y2) && mY <= Math.max(y, y2);
    }

    public static void renderHoverEffect(GuiGraphics stack, int x, int y, int width, int height)
    {
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 0.275F);
        stack.fill(x, y, x + width, y + height, AVAConstants.AVA_HUD_COLOUR_WHITE.getRGB());
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
    }

    public static void fillGradient(PoseStack stack, float x, float y, float x2, float y2, int z, int c, int c2, Direction direction)
    {
        Pair<Float, Float> xs = Pair.ascending(x, x2);
        Pair<Float, Float> ys = Pair.ascending(y, y2);
        x = xs.getA();
        x2 = xs.getB();
        y = ys.getA();
        y2 = ys.getB();
        fillGradient(stack, x, y, x2, y, x, y2, x2, y2, z, c, c2, direction);
    }

    public static void fillGradient(PoseStack stack, float x, float y, float x2, float y2, float x3, float y3, float x4, float y4, int z, int c, int c2, Direction direction)
    {
        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();
        RenderSystem.setShader(GameRenderer::getPositionColorShader);
        Tesselator tesselator = Tesselator.getInstance();
        BufferBuilder bufferbuilder = tesselator.getBuilder();
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);
        fillGradient(stack.last().pose(), bufferbuilder, x, y, x2, y2, x3, y3, x4, y4, z, c, c2, direction);
        tesselator.end();
        RenderSystem.disableBlend();
    }

    private static void fillGradient(Matrix4f matrix, BufferBuilder build, float x, float y, float x2, float y2, float x3, float y3, float x4, float y4, int z, int c, int c2, Direction direction)
    {
        float r = (float) (c >> 16 & 255) / 255.0F;
        float g = (float) (c >> 8 & 255) / 255.0F;
        float b = (float) (c & 255) / 255.0F;
        float a = (float) (c >> 24 & 255) / 255.0F;
        float r2 = (float) (c2 >> 16 & 255) / 255.0F;
        float g2 = (float) (c2 >> 8 & 255) / 255.0F;
        float b2 = (float) (c2 & 255) / 255.0F;
        float a2 = (float) (c2 >> 24 & 255) / 255.0F;
        // 12
        // 34
        switch (direction)
        {
            case EAST -> {
                build.vertex(matrix, (float) x2, (float) y2, (float) z).color(r2, g2, b2, a2).endVertex();
                build.vertex(matrix, (float) x, (float) y, (float) z).color(r, g, b, a).endVertex();
                build.vertex(matrix, (float) x3, (float) y3, (float) z).color(r, g, b, a).endVertex();
                build.vertex(matrix, (float) x4, (float) y4, (float) z).color(r2, g2, b2, a2).endVertex();
            }
            case WEST -> {
                build.vertex(matrix, (float) x2, (float) y2, (float) z).color(r, g, b, a).endVertex();
                build.vertex(matrix, (float) x, (float) y, (float) z).color(r2, g2, b2, a2).endVertex();
                build.vertex(matrix, (float) x3, (float) y3, (float) z).color(r2, g2, b2, a2).endVertex();
                build.vertex(matrix, (float) x4, (float) y4, (float) z).color(r, g, b, a).endVertex();
            }
            case SOUTH -> {
                build.vertex(matrix, (float) x2, (float) y2, (float) z).color(r, g, b, a).endVertex();
                build.vertex(matrix, (float) x, (float) y, (float) z).color(r, g, b, a).endVertex();
                build.vertex(matrix, (float) x3, (float) y3, (float) z).color(r2, g2, b2, a2).endVertex();
                build.vertex(matrix, (float) x4, (float) y4, (float) z).color(r2, g2, b2, a2).endVertex();
            }
            case NORTH -> {
                build.vertex(matrix, (float) x2, (float) y2, (float) z).color(r2, g2, b2, a2).endVertex();
                build.vertex(matrix, (float) x, (float) y, (float) z).color(r2, g2, b2, a2).endVertex();
                build.vertex(matrix, (float) x3, (float) y3, (float) z).color(r, g, b, a).endVertex();
                build.vertex(matrix, (float) x4, (float) y4, (float) z).color(r, g, b, a).endVertex();
            }
        }
    }

    public static void putTextureVertex(VertexConsumer consumer, Matrix4f matrix4f, float x, float y, float x2, float y2, int light, float alpha, @Nullable Direction direction)
    {
        int a = (int) (alpha * 255.0F);
        if (direction == null || direction == Direction.NORTH)
        {
            consumer.vertex(matrix4f, x, y2, 0.0F).color(255, 255, 255, a).uv(0.0F, 1.0F).uv2(light).endVertex();
            consumer.vertex(matrix4f, x2, y2, 0.0F).color(255, 255, 255, a).uv(1.0F, 1.0F).uv2(light).endVertex();
            consumer.vertex(matrix4f, x2, y, 0.0F).color(255, 255, 255, a).uv(1.0F, 0.0F).uv2(light).endVertex();
            consumer.vertex(matrix4f, x, y, 0.0F).color(255, 255, 255, a).uv(0.0F, 0.0F).uv2(light).endVertex();
            return;
        }
        switch (direction)
        {
            case SOUTH -> {
                consumer.vertex(matrix4f, x, y2, 0.0F).color(255, 255, 255, a).uv(1.0F, 0.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x2, y2, 0.0F).color(255, 255, 255, a).uv(0.0F, 0.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x2, y, 0.0F).color(255, 255, 255, a).uv(0.0F, 1.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x, y, 0.0F).color(255, 255, 255, a).uv(1.0F, 1.0F).uv2(light).endVertex();
            }
            case EAST -> {
                consumer.vertex(matrix4f, x, y2, 0.0F).color(255, 255, 255, a).uv(0.0F, 0.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x2, y2, 0.0F).color(255, 255, 255, a).uv(1.0F, 0.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x2, y, 0.0F).color(255, 255, 255, a).uv(1.0F, 1.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x, y, 0.0F).color(255, 255, 255, a).uv(0.0F, 1.0F).uv2(light).endVertex();
            }
            case WEST -> {
                consumer.vertex(matrix4f, x, y2, 0.0F).color(255, 255, 255, a).uv(1.0F, 1.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x2, y2, 0.0F).color(255, 255, 255, a).uv(0.0F, 1.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x2, y, 0.0F).color(255, 255, 255, a).uv(0.0F, 0.0F).uv2(light).endVertex();
                consumer.vertex(matrix4f, x, y, 0.0F).color(255, 255, 255, a).uv(1.0F, 0.0F).uv2(light).endVertex();
            }
        }
    }

    public static double translateMatrixWithCamera(PoseStack stack, Vec3 vec, double max)
    {
        Vec3 view = Minecraft.getInstance().gameRenderer.getMainCamera().getPosition();
        double x = vec.x - view.x();
        double y = vec.y - view.y();
        double z = vec.z - view.z();
        double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        if (distance > max)
            return -1.0D;
        stack.translate(x, y, z);
        return distance;
    }

    public static void rotateWithCamera(PoseStack stack, boolean forTexture)
    {
        stack.mulPose(Minecraft.getInstance().getEntityRenderDispatcher().cameraOrientation());
        if (forTexture)
            stack.mulPose(Axis.ZP.rotationDegrees(180.0F));
    }

    public static void scaleWithDistance(PoseStack stack, Vec3 vec, Vec3 vec2)
    {
        scaleWithDistance(stack, vec.distanceTo(vec2));
    }

    public static void scaleWithDistance(PoseStack stack, double distance)
    {
        float size = (float) (distance / 200.0F);
        stack.scale(size, size, size);
    }

    public static void addHitMark(Entity attacker, Entity target)
    {
        if (attacker == Minecraft.getInstance().player && (!(target instanceof ICustomOnHitEffect effect) || effect.onHitEffect()))
            AVAWorldData.getInstance(attacker.level()).hitMarks.add(new HitEffect(target));
    }

    public static Optional<PlayerInfo> getPlayerInfo(String name)
    {
        ClientPacketListener connection = Minecraft.getInstance().getConnection();
        return connection == null ? Optional.empty() : Optional.ofNullable(connection.getPlayerInfo(name));
    }

    public static Optional<PlayerInfo> getPlayerInfo(UUID id)
    {
        ClientPacketListener connection = Minecraft.getInstance().getConnection();
        return connection == null ? Optional.empty() : Optional.ofNullable(connection.getPlayerInfo(id));
    }

    public static void drawCenteredShadowString(GuiGraphics stack, Font font, Component text, float x, float y, int colour)
    {
        drawCenteredShadowString(stack, font, text.getVisualOrderText(), x, y, colour);
    }

    public static void drawCenteredShadowString(GuiGraphics stack, Font font, FormattedCharSequence text, float x, float y, int colour)
    {
        stack.drawString(font, text, x - font.width(text) / 2.0F, y, colour, true);
    }

    public static Vec3 clientEntityPos(Entity entity, float partialTicks)
    {
        return AVACommonUtil.lerpVec3(new Vec3(entity.xOld, entity.yOld, entity.zOld), entity.position(), partialTicks);
    }

    public static float clientEntityYaw(Entity entity, float partialTicks)
    {
        return Mth.rotLerp(partialTicks, entity.yRotO, entity.getYRot());
    }

    public static float clientEntityPutch(Entity entity, float partialTicks)
    {
        return Mth.rotLerp(partialTicks, entity.xRotO, entity.getXRot());
    }

    public static ResourceLocation toValidTexture(ResourceLocation texture)
    {
        return new ResourceLocation(texture.getNamespace(), toValidTexture(texture.getPath()));
    }

    public static String toValidTexture(String texture)
    {
        if (texture.equals("block/bamboo"))
            return "block/bamboo_block";
        return texture
                .replace("wood", "log")
                .replace("quartz_block", "quartz_block_top")
                .replace("smooth_sandstone", "sandstone_top")
                .replace("smooth_red_sandstone", "red_sandstone_top")
                .replace("smooth_quartz", "quartz_block_bottom")
                .replace("crimson_hyphae", "crimson_stem")
                .replace("warped_hyphae", "warped_stem")
                .replace("muddy_mangrove_roots", "muddy_mangrove_roots_side");
    }

    public static Color transparencyColor(Color original, float alpha)
    {
        return new Color(original.getRed() / 255.0F, original.getGreen() / 255.0F, original.getBlue() / 255.0F, alpha);
    }

    public static ModelResourceLocation modelRLFromString(String str)
    {
        if (str.equals(NULL))
            return null;
        String[] strs = str.split("#");
        return new ModelResourceLocation(new ResourceLocation(strs[0]), strs[1]);
    }


    public static String avaPrettyPrint(JsonElement element, int space)
    {
        if (element.isJsonPrimitive() || element.isJsonNull())
            return element.toString();

        StringBuilder result = new StringBuilder();
        if (element.isJsonObject())
        {
            result.append("{\n");
            JsonObject object = element.getAsJsonObject();
            boolean first = true;
            for (Map.Entry<String, JsonElement> entry : object.entrySet())
            {
                if (!first)
                    result.append(",\n");
                space(result, space + 1);
                result.append("\"").append(entry.getKey()).append("\": ");
                result.append(avaPrettyPrint(entry.getValue(), space + 1));
                first = false;
            }
            result.append("\n");
            space(result, space);
            result.append("}");
        }
        else if (element.isJsonArray())
        {
            result.append("[");
            JsonArray array = element.getAsJsonArray();
            boolean first = true;
            for (JsonElement arrayElement : array)
            {
                if (!first)
                    result.append(", ");
                result.append(avaPrettyPrint(arrayElement, space + 1));
                first = false;
            }
            result.append("]");
        }
        return result.toString();
    }

    private static void space(StringBuilder builder, int indentLevel)
    {
        if (indentLevel > 0)
            builder.append("  ".repeat(indentLevel));
    }
}
