package pellucid.ava.util;

public class Loop
{
    private final int min;
    private final int max;
    private final int range;
    private int current;

    public Loop(int max)
    {
        this(0, max);
    }

    public Loop(int min, int max)
    {
        this(min, max, min);
    }

    public Loop(int min, int max, int current)
    {
        this.min = min;
        this.max = max;
        this.range = max - min + 1;
        this.current = current;
    }

    public int size()
    {
        return max - min + 1;
    }

    public int move(boolean forward)
    {
        return forward ? next() : prev();
    }


    public int set(int i)
    {
        return change(i - current);
    }

    public int next()
    {
        return change(1);
    }

    public int prev()
    {
        return change(-1);
    }

    public int current()
    {
        return change(0);
    }

    public int change(int amount)
    {
        current += amount;
        while (current > max)
            current -= range;
        while (current < min)
            current += range;
        return current;
    }
}
