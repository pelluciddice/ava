package pellucid.ava.util;

import com.google.common.collect.Sets;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.component.DataComponentType;
import net.minecraft.core.component.DataComponents;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextColor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.TicketType;
import net.minecraft.server.players.PlayerList;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.CombatRules;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.EnderMan;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.EntityBasedExplosionDamageCalculator;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.ExplosionDamageCalculator;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BarrierBlock;
import net.minecraft.world.level.block.BellBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.NoteBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.TargetBlock;
import net.minecraft.world.level.block.TntBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.scores.Objective;
import net.minecraft.world.scores.PlayerTeam;
import net.minecraft.world.scores.Team;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.data.loading.DatagenModLoader;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.IInheritable;
import pellucid.ava.blocks.explosive_barrel.ExplosiveBarrelTE;
import pellucid.ava.blocks.repairable.RepairableTileEntity;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.commands.RecoilRefundTypeCommand;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.entities.base.AVAEntity;
import pellucid.ava.entities.functionalities.IDifficultyScaledHostile;
import pellucid.ava.entities.functionalities.IOwner;
import pellucid.ava.entities.functionalities.IVisionBlockingEntity;
import pellucid.ava.entities.livings.AVAHostileEntity;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.scanhits.BulletEntity;
import pellucid.ava.entities.smart.EUSmartEntity;
import pellucid.ava.entities.smart.NRFSmartEntity;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.entities.throwables.HandGrenadeEntity;
import pellucid.ava.events.forge.AVABulletHitEvent;
import pellucid.ava.events.forge.AVAHardnessEvent;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.items.armours.AVAArmourMaterials;
import pellucid.ava.items.functionalities.IClassification;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.MeleeWeapons;
import pellucid.ava.items.init.Pistols;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.init.Rifles;
import pellucid.ava.items.init.Snipers;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.items.init.SubmachineGuns;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.packets.PlaySoundToClientMessage;
import pellucid.ava.player.PositionWithRotation;
import pellucid.ava.player.status.GunStatusManager;
import pellucid.ava.recipes.Recipe;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.lang.Math.round;
import static java.lang.Math.*;
import static pellucid.ava.config.AVAServerConfig.*;
import static pellucid.ava.util.AVACommonUtil.*;
import static pellucid.ava.util.AVAConstants.*;
import static pellucid.ava.util.AVAWeaponUtil.RadioCategory.X;
import static pellucid.ava.util.AVAWeaponUtil.RadioCategory.Z;

public class AVAWeaponUtil
{
    public static void createExplosionProjectile(HandGrenadeEntity projectile, @Nullable LivingEntity thrower, Item weapon, BiConsumer<Entity, Double> extraAction, QuadConsumer<ServerLevel, Double, Double, Double> effect, SoundEvent explosionSound)
    {
        createExplosionEntity(projectile, projectile.explosiveRadius, getExplosiveDamageForEntity(projectile), thrower, weapon, extraAction, effect, explosionSound);
    }

    public static void createExplosionEntity(AVAEntity explosive, final double radius, final double originalDamage, @Nullable LivingEntity thrower, Item weapon, BiConsumer<Entity, Double> extraAction, QuadConsumer<ServerLevel, Double, Double, Double> effect, SoundEvent explosionSound)
    {
        Level world = explosive.level();
        if (!world.isClientSide())
        {
            AABB bb = explosive.getBoundingBox();
            if (radius <= 0)
                return;
            Vec3 position = explosive.position();
            for (Entity target : world.getEntities(explosive, bb.inflate(radius), explosive::canAttack))
            {
                if (!target.isAlive() || !(target instanceof LivingEntity))
                    continue;
                Vec3 hitPos = canBeSeen(explosive, false, target, false, false, true, true, true);
                if (hitPos == null)
                    continue;
                double distance = hitPos.distanceTo(position);
                double damage = originalDamage * max((radius - distance / 2.75F) / radius, 0.3F);
                if (damage > 0)
                    if (!attackEntityDependAllyDamage(target, AVADamageSources.causeProjectileExplodeDamage(world, thrower, explosive, weapon), (float) damage, 1.0F))
                        continue;
                extraAction.accept(target, distance);
            }
            effect.accept((ServerLevel) world, position.x, position.y, position.z);
            playAttenuableSoundToClient(world, explosionSound, position.x(), position.y(), position.z());
            createExplosionForBlocks(world, explosive, explosive.getX(), explosive.getY(), explosive.getZ(), radius);
        }
    }

    public static void createExplosionBlock(Level world, BlockPos pos, BlockState state, @Nullable LivingEntity attacker, final float radius, final double originDamage, @Nullable QuadConsumer<ServerLevel, Double, Double, Double> effect, SoundEvent explosionSound)
    {
        Vec3 position = new Vec3(pos.getX(), pos.getY(), pos.getZ()).add(0.5F, 0.5F, 0.5F);
        if (!world.isClientSide())
        {
            AABB bb = state.getShape(world, pos).bounds().move(pos);
            if (radius <= 0)
                return;
            BlockEntity te = world.getBlockEntity(pos);
            CompoundTag compound = null;
            if (te instanceof IInheritable)
                compound = ((IInheritable) te).drain(new CompoundTag());
            world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
            world.setBlockAndUpdate(pos, state);
            te = world.getBlockEntity(pos);
            if (compound != null && te instanceof IInheritable)
                ((IInheritable) te).inherit(compound);
            for (Entity entity : world.getEntities(null, bb.inflate(radius)))
            {
                if (!entity.isAlive() || !(entity instanceof LivingEntity))
                    continue;
                Vec3 hitPos = canBeSeen(null, pos, position, entity, false, false, true, true, true);
                if (hitPos == null)
                    continue;
                double distance = hitPos.distanceTo(position);
                float damage = (float) (originDamage * max((radius - distance) / radius, 0.35F));
                if (damage > 0)
                    attackEntityDependAllyDamage(entity, AVADamageSources.causeExplodeDamage(world, attacker), damage * (entity instanceof Player ? 0.3F : 1.0F), 1.0F);
            }
            if (effect != null)
                effect.accept((ServerLevel) world, position.x, position.y, position.z);
            playAttenuableSoundToClient(world, explosionSound, position.x(), position.y(), position.z());
            createExplosionForBlocks(world, null, position.x, position.y, position.z, radius);
        }
    }

    public static void createFlash(HandGrenadeEntity projectile, QuadConsumer<ServerLevel, Double, Double, Double> effect, SoundEvent explosionSound)
    {
        Level world = projectile.level();
        if (!world.isClientSide())
        {
            double radius = projectile.flashRadius;
            AABB bb = projectile.getBoundingBox();
            if (radius <= 0)
                return;
            Vec3 position = projectile.position();
            for (Entity entity : world.getEntities(projectile, bb.inflate(radius), entity -> entity instanceof LivingEntity))
            {
                int flash = getFlashDurationForEntity(projectile);
                if (flash <= 0)
                    break;
                if (!entity.isAlive())
                    continue;
                Vec3 hitPos = canBeSeen(projectile, false, entity, false, true, true, true, false);
                if (hitPos == null)
                    continue;
                double distance = hitPos.distanceTo(position);
                flash = (int) (flash * max((radius - distance) / radius, 0.15F));
                if (flash > 0)
                {
                    if (entity instanceof Player)
                    {
                        if (!((Player) entity).getAbilities().instabuild)
                            DataToClientMessage.flash((ServerPlayer) entity, flash);
                    }
                    else if (entity instanceof AVAHostileEntity)
                        ((AVAHostileEntity) entity).blind = flash;
                    else
                        entity.getPersistentData().putInt(AVAConstants.TAG_ENTITY_BLINDED, flash);
                }
            }
            effect.accept((ServerLevel) world, position.x, position.y, position.z);
            playAttenuableSoundToClient(world, explosionSound, position.x(), position.y(), position.z());
        }
    }

    public static void playAttenuableSoundToClientMoving(SoundEvent sound, @Nonnull Entity moving)
    {
        playAttenuableSoundToClientMoving(sound, moving, 1.0F);
    }

    public static void playAttenuableSoundToClientMoving(SoundEvent sound, @Nonnull Entity moving, float volume)
    {
        playAttenuableSoundToClientMoving(sound, moving, volume, 1.0F);
    }

    public static void playAttenuableSoundToClientMoving(SoundEvent sound, @Nonnull Entity moving, float volume, float pitch)
    {
        playSoundToClients(moving.level(), (p) -> new PlaySoundToClientMessage(sound, -1, -1, -1, volume, pitch).setMoving(moving.getId()));
    }

    public static void playAttenuableSoundToClient(SoundEvent sound, Entity source, boolean moving, Predicate<Player> exclude)
    {
        if (moving)
            playSoundToClients(source.level(), (p) -> new PlaySoundToClientMessage(sound, -1, -1, -1, 1.0F, 1.0F).setMoving(source.getId()), exclude);
        else
            playAttenuableSoundToClient(sound, source.level(), source.position(), exclude);
    }

    public static void playAttenuableSoundToClient(SoundEvent sound, Entity source, float volume, float pitch, boolean moving, Predicate<Player> exclude)
    {
        if (moving)
            playSoundToClients(source.level(), (p) -> new PlaySoundToClientMessage(sound, -1, -1, -1, volume, pitch).setMoving(source.getId()), exclude);
        else
            playAttenuableSoundToClient(sound, source.level(), source.position(), exclude);
    }

    public static void playAttenuableSoundToClient(SoundEvent sound, Level world, Vec3 source, Predicate<Player> exclude)
    {
        playSoundToClients(world, (p) -> new PlaySoundToClientMessage(sound, source.x(), source.y(), source.z(), 1.0F, 1.0F), exclude);
    }

    public static void playAttenuableSoundToClient(Level world, SoundEvent sound, double x, double y, double z)
    {
        playSoundToClients(world, (p) -> new PlaySoundToClientMessage(sound, x, y, z));
    }

    public static void playSoundToClients(Level world, SoundEvent sound, SoundSource category, double x, double y, double z, float volume, float pitch)
    {
        playSoundToClients(world, (p) -> new PlaySoundToClientMessage(BuiltInRegistries.SOUND_EVENT.getKey(sound).toString(), category, x, y, z, volume, pitch));
    }

    public static void playSoundToClients(Level world, Function<Player, PlaySoundToClientMessage> getter)
    {
        playSoundToClients(world, getter, null);
    }

    public static void playSoundToClients(Level world, Function<Player, PlaySoundToClientMessage> getter, @Nullable Predicate<Player> exclude)
    {
        if (world instanceof ServerLevel)
            new ArrayList<>(world.players()).stream().filter((player) -> player.level().dimension() == world.dimension()).forEach((player) -> {
                if (exclude == null || !exclude.test(player))
                    playSoundToClient(getter, player);
            });
    }

    public static void playSoundToClient(Function<Player, PlaySoundToClientMessage> getter, Player target)
    {
        if (target instanceof ServerPlayer player)
            PacketDistributor.sendToPlayer(player, getter.apply(player));
    }

    public static void createExplosionForBlocks(Level world, @Nullable Entity projectile, double x, double y, double z, double size)
    {
        if (!world.isClientSide())
            new ExplosionForBlocks(world, projectile, x, y, z, size);
    }

    public static boolean destroyBlockOnMeleeHit(Level world, BlockPos pos)
    {
        if (!world.isClientSide() && isBlockMeleeDestroyable(world.getBlockState(pos).getBlock()))
        {
            world.destroyBlock(pos, false);
            return true;
        }
        return false;
    }

    public static boolean destroyGlassOnHit(Level world, BlockPos pos)
    {
//        if (world.getBlockState(pos).getMaterial() == Material.GLASS && !world.isClientSide() && WorldData.getCap(world).isGlassDestroyable())
        if (world.getBlockState(pos).getSoundType() == SoundType.GLASS && !world.isClientSide() && AVACrossWorldData.getInstance().doGlassBreak)
        {
            world.destroyBlock(pos, false);
            return true;
        }
        return false;
    }

    public static boolean destructRepairable(Level world, BlockPos pos, @Nullable Player player)
    {
        BlockEntity te = world.getBlockEntity(pos);
        if (te instanceof RepairableTileEntity)
        {
            if (!world.isClientSide())
                ((RepairableTileEntity) te).destruct(player);
            return true;
        }
        return false;
    }

    public static boolean checkHitTargetBlock(Level world, BulletEntity bullet, LivingEntity shooter, BlockHitResult result)
    {
        BlockPos pos = result.getBlockPos();
        BlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        if (block instanceof TargetBlock && !world.isClientSide())
        {
            hitTargetBlock(world, state, result, bullet, shooter);
            return true;
        }
        return false;
    }

    public static void hitTargetBlock(Level world, BlockState state, BlockHitResult result, BulletEntity bullet, LivingEntity shooter)
    {
        int i = getPowerFromHitVec(world, state, result);
        if (shooter instanceof ServerPlayer)
        {
            ServerPlayer serverplayerentity = (ServerPlayer) shooter;
            serverplayerentity.awardStat(Stats.TARGET_HIT);
            CriteriaTriggers.TARGET_BLOCK_HIT.trigger(serverplayerentity, bullet, result.getLocation(), i);
        }
    }

    private static int getPowerFromHitVec(Level world, BlockState state, BlockHitResult result)
    {
        int i = getPowerFromHitVec(result.getDirection(), result.getLocation());
        if (!world.getBlockTicks().hasScheduledTick(result.getBlockPos(), state.getBlock()))
        {
            world.setBlock(result.getBlockPos(), state.setValue(BlockStateProperties.POWER, i), 3);
            world.scheduleTick(result.getBlockPos(), state.getBlock(), 8);
        }
        return i;
    }

    private static int getPowerFromHitVec(Direction direction, Vec3 vector)
    {
        double d0 = Math.abs(Mth.frac(vector.x) - 0.5D);
        double d1 = Math.abs(Mth.frac(vector.y) - 0.5D);
        double d2 = Math.abs(Mth.frac(vector.z) - 0.5D);
        Direction.Axis direction$axis = direction.getAxis();
        double d3;
        if (direction$axis == Direction.Axis.Y)
            d3 = Math.max(d0, d2);
        else if (direction$axis == Direction.Axis.Z)
            d3 = Math.max(d0, d1);
        else
            d3 = Math.max(d1, d2);
        return Math.max(1, Mth.ceil(15.0D * Mth.clamp((0.5D - d3) / 0.5D, 0.0D, 1.0D)));
    }

    public static boolean isSameSide(Entity entity, Entity entity2)
    {
        if (!isValidEntity(entity) || !isValidEntity(entity2))
            return false;
        if (entity == entity2)
            return true;
        if (!(entity instanceof LivingEntity) || !(entity2 instanceof LivingEntity))
            return false;
        TeamSide side = TeamSide.getSideFor((LivingEntity) entity);
        return entity.isAlliedTo(entity2) || (side != null && side.isSameSide((LivingEntity) entity2));
    }

    public static boolean isFriendlyFire(@Nullable Entity attacker, @Nullable Entity target)
    {
        if (attacker instanceof LivingEntity && target instanceof LivingEntity)
            return isSameSide((LivingEntity) attacker, (LivingEntity) target) && attacker != target;
        return false;
    }

    public static boolean attackEntityDependAllyDamage(Entity target, DamageSource source, float damage, float penetration)
    {
        return attackEntityDependAllyDamage(target, source, damage, 1.0F, penetration);
    }

    public static boolean attackEntityDependAllyDamage(Entity target, DamageSource source, float damage, float multiplier, float penetration)
    {
        //        IWorldData cap = WorldData.getCap(target.level);
        AVACrossWorldData data = AVACrossWorldData.getInstance();
        Entity attacker = source.getEntity();
        boolean friendlyFire = isFriendlyFire(attacker, target);
        //        if (!cap.isFriendlyFireAllowed() && friendlyFire)
        //            return false;
        if (!data.friendlyFire && friendlyFire)
            return false;
        multiplier *= friendlyFire && data.reducedFriendlyFire ? 0.25F : 1.0F;
        return attackEntity(target, source, damage, multiplier, penetration);
    }

    private static final UUID ARMOUR_MODIFIER = UUID.fromString("de0b4efe-b352-47dd-86fc-33f83c7b5a36");
    private static final UUID ARMOUR_TOUGHNESS_MODIFIER = UUID.fromString("3e7f0b4f-d233-4554-baf2-cf226b321a57");
    public static boolean attackEntity(Entity target, DamageSource source, float damage, float multiplier, float penetration)
    {
        Entity attacker = source.getEntity();
        int statDamage = round(damage * 10.0F);
        boolean isAttackerPlayer = attacker instanceof Player;
        boolean isTargetPlayer = target instanceof Player;
        boolean isTargetLiving = target instanceof LivingEntity;
        boolean isArmourStand = target instanceof ArmorStand;
        if (isAttackerPlayer && attacker.isAlive())
        {
            damage += PlayerAction.getCap((Player) attacker).getAttackDamageBoost();
            ((Player) attacker).awardStat(Stats.DAMAGE_DEALT, statDamage);
        }
        else if (attacker instanceof IDifficultyScaledHostile)
        {
            if (isTargetPlayer)
                multiplier *= ((IDifficultyScaledHostile) attacker).getDamageScale(target.level().getDifficulty());
        }
        if (target instanceof EnderMan && source instanceof AVADamageSources.IAVAWeaponDamageSource)
            source = AVADamageSources.causeDamageDirect(target.level(), source.getEntity(), ((AVADamageSources.IAVAWeaponDamageSource) source).getWeapon());
        if (isTargetPlayer)
            ((Player) target).awardStat(Stats.DAMAGE_TAKEN, statDamage);
        damage *= multiplier;
        damage *= isTargetPlayer || isArmourStand ? DAMAGE_MULTIPLIER_AGAINST_PLAYERS.get() : DAMAGE_MULTIPLIER_AGAINST_OTHERS.get();
        if (damage <= MIN_BULLET_DAMAGE)
            return false;
        if (isArmourStand && isFullEquipped((LivingEntity) target) && isAttackerPlayer && !attacker.level().isClientSide())
        {
            MutableComponent weaponText = Component.empty();
            MutableComponent damageText = Component.translatable("ava.chat.weapon_damage_test_damage", damage).withStyle(ChatFormatting.RED);
            if (source instanceof AVADamageSources.IAVAWeaponDamageSource weaponSource)
                weaponText = Component.translatable("ava.chat.weapon_damage_test_weapon", weaponSource.getWeapon().getName(weaponSource.getWeapon().getDefaultInstance())).append(", ").withStyle(ChatFormatting.GREEN);
            attacker.sendSystemMessage(weaponText.append(damageText));
        }
        if (!EXPLOSION_DESTROYS_ENTITIES.get() && source.is(DamageTypeTags.IS_EXPLOSION) && (!isTargetLiving || isArmourStand))
            return false;
        AtomicBoolean attacked = new AtomicBoolean(false);
        DamageSource finalSource = source;
        float finalDamage = damage;
        if (isTargetLiving)
        {
            penetrateArmour((LivingEntity) target, source, damage, penetration, () -> {
                attacked.set(target.hurt(finalSource, finalDamage));
                return attacked.get();
            });
        }
        if (!target.level().isClientSide() && !attacked.get())
            return target.hurt(source, damage);
        return true;
    }
    
    public static void penetrateArmour(LivingEntity target, DamageSource source, float damage, float penetration, Supplier<Boolean> action)
    {
        boolean isTargetPlayer = target instanceof Player;
        if (isCompetitiveModeActivated() && isTargetPlayer)
        {
            PlayerAction cap = PlayerAction.getCap((Player) target);
            if (cap.getArmourValue() <= 0.0F)
                penetration = 1.0F;
        }
        AttributeInstance armour = target.getAttribute(Attributes.ARMOR);
        AttributeInstance toughness = target.getAttribute(Attributes.ARMOR_TOUGHNESS);
        if (armour != null && armour.getModifier(ARMOUR_MODIFIER) == null)
            armour.addTransientModifier(new AttributeModifier(ARMOUR_MODIFIER, "Armour Penetration", -penetration, AttributeModifier.Operation.ADD_MULTIPLIED_TOTAL));
        if (toughness != null && toughness.getModifier(ARMOUR_TOUGHNESS_MODIFIER) == null)
            toughness.addTransientModifier(new AttributeModifier(ARMOUR_TOUGHNESS_MODIFIER, "Armour Penetration", -penetration, AttributeModifier.Operation.ADD_MULTIPLIED_TOTAL));
        boolean attacked = action.get();
        if (armour != null)
            armour.removeModifier(ARMOUR_MODIFIER);
        if (toughness != null)
            toughness.removeModifier(ARMOUR_TOUGHNESS_MODIFIER);
        if (attacked && isCompetitiveModeActivated() && isTargetPlayer)
        {
            PlayerAction cap = PlayerAction.getCap((Player) target);
            float value = cap.getArmourValue();
            if (value > 0.0F)
                cap.setArmourValue((Player) target, value - AVAWeaponUtil.getAbsorbedDamage(target, source, damage));
        }
    }

//  public static List<Triple<BlockRayTraceResult, Float, Float>> rayTraceAndPenetrateBlocks(World world, List<Vector3d> vecs, float projectileHealth, Function<BlockState, Float> getHealthReduction)
//	{
//		AtomicInteger health = new AtomicInteger(Float.floatToIntBits(projectileHealth));
//		List<Triple<BlockRayTraceResult, Float, Float>> results = new ArrayList<>();
//		Vector3d startVec = originalContext.getStartVec();
//		IBlockReader.doRayTrace(originalContext, (context, pos) -> {
//			BlockState blockstate = world.getBlockState(pos);
//			VoxelShape blockShape = context.getBlockShape(blockstate, world, pos);
//			Vector3d endVec = context.getEndVec();
//			BlockRayTraceResult blockResult = world.rayTraceBlocks(startVec, endVec, pos, blockShape, blockstate);
//			if (blockResult != null)
//			{
//				float reduction = getHealthReduction.apply(blockstate);
//				float newHealth = Float.intBitsToFloat(health.get()) - reduction;
//				results.add(Triple.of(blockResult, (float) startVec.distanceTo(blockResult.getHitVec()), newHealth));
//				health.set(Float.floatToIntBits(newHealth));
//				if (newHealth <= 0)
//					return blockResult;
//			}
//			return null;
//		}, AVAWeaponUtil::createMiss);
//		return results;
//	}

    public static BlockHitResult rayTraceBlocks(Entity entity, ClipContext.Block mode, double distance, boolean fluid)
    {
        Vec3 from = AVAWeaponUtil.getEyePositionFor(entity);
        Vec3 to = from.add(entity.getLookAngle().scale(distance));
        return entity.level().clip(new ClipContext(from, to, mode, fluid ? ClipContext.Fluid.ANY : ClipContext.Fluid.NONE, entity));
    }

    public static HitResult rayTrace(Entity entity, final double distance, boolean fluid)
    {
        Vec3 from = AVAWeaponUtil.getEyePositionFor(entity);
        Vec3 to = from.add(entity.getLookAngle().scale(distance));
        HitResult result = rayTraceBlocks(entity, ClipContext.Block.OUTLINE, distance, fluid);
        EntityHitResult result2 = rayTraceEntity(entity.level(), entity, from, to, (e) -> true, true);

        if (result.getType() == HitResult.Type.MISS)
            return result2;
        return result2 != null ? result.distanceTo(entity) >= result2.distanceTo(entity) ? result2 : result : result;
    }

    public static List<Pair<BlockHitResult, Float>> rayTraceAndPenetrateBlocks(Level world, List<Vec3> vecs, float health, Function<BlockState, Float> getHealthReduction)
    {
        List<Pair<BlockHitResult, Float>> results = new ArrayList<>();
        List<Float> duplicated = new ArrayList<>();
        if (vecs.isEmpty())
            return results;
        Vec3 startVec = vecs.get(0);
        for (int i=0;i<vecs.size()-1;i++)
        {
            Vec3 vec = vecs.get(i);
            Vec3 next = vecs.get(i + 1);
            BlockPos pos = BlockPos.containing(vec);
            BlockPos pos2 = BlockPos.containing(next);
            BlockState blockstate = world.getBlockState(pos);
            BlockState blockstate2 = world.getBlockState(pos2);
            if (blockstate.getBlock() instanceof BarrierBlock && blockstate2.getBlock() instanceof BarrierBlock)
                continue;
            VoxelShape blockShape = blockstate.getCollisionShape(world, pos, CollisionContext.empty());
            VoxelShape blockShape2 = blockstate2.getCollisionShape(world, pos2, CollisionContext.empty());
            BlockHitResult blockResult;
            boolean empty = blockShape.isEmpty() || blockShape.toAabbs().stream().noneMatch((bb) -> bb.contains(vec));
            boolean empty2 = blockShape2.isEmpty() || blockShape2.toAabbs().stream().noneMatch((bb) -> bb.contains(next));
            if ((isRayTraceIgnorableBlock(blockstate, blockShape) && isRayTraceIgnorableBlock(blockstate2, blockShape2)))
                continue;
            else if (empty && !empty2)
                blockResult = world.clipWithInteractionOverride(vec, next, pos2, blockShape2, blockstate2);
            else if (!empty && empty2)
                blockResult = world.clipWithInteractionOverride(next, vec, pos, blockShape, blockstate);
            else
                blockResult = world.clipWithInteractionOverride(vec, next, pos, blockShape, blockstate);

            if (blockResult != null)
            {
                float d = (float) startVec.distanceTo(blockResult.getLocation());
                if (!duplicated.contains(d))
                {
                    health -= getHealthReduction.apply(blockstate);
                    results.add(Pair.of(blockResult, d));
                    duplicated.add(d);
                    if (health <= 0)
                        break;
                }
            }
        }
        results.sort(Comparator.comparingDouble(Pair::getB));
        return results;
    }

    public static List<Pair<BlockHitResult, Float>> rayTraceAndPenetrateBlocks4(Level world, List<Vec3> vecs, float health, BiFunction<BlockHitResult, BlockState, Float> getHealthReduction)
    {
        List<Pair<BlockHitResult, Float>> results = new ArrayList<>();
        if (vecs.isEmpty())
            return results;
        Vec3 startVec = vecs.get(0);
        for (int i=0;i<vecs.size()-1;i++)
        {
            Vec3 vec = vecs.get(i);
            Vec3 next = vecs.get(i + 1);
            BlockPos pos = BlockPos.containing(vec);
            BlockPos pos2 = BlockPos.containing(next);

            BlockState blockstate = world.getBlockState(pos);
            BlockState blockstate2 = world.getBlockState(pos2);

            if (blockstate.getBlock() instanceof BarrierBlock && blockstate2.getBlock() instanceof BarrierBlock)
                continue;
            VoxelShape blockShape = blockstate.getCollisionShape(world, pos, CollisionContext.empty());
            VoxelShape blockShape2 = blockstate2.getCollisionShape(world, pos2, CollisionContext.empty());

            if ((isRayTraceIgnorableBlock(blockstate, blockShape) && isRayTraceIgnorableBlock(blockstate2, blockShape2)))
                continue;

            BlockHitResult blockResult;
            boolean empty = blockShape.isEmpty() || blockShape.toAabbs().stream().noneMatch((bb) -> bb.move(pos).contains(vec));
            boolean empty2 = blockShape2.isEmpty() || blockShape2.toAabbs().stream().noneMatch((bb) -> bb.move(pos2).contains(next));

            if (empty && !empty2)
                blockResult = world.clipWithInteractionOverride(vec, next, pos2, blockShape2, blockstate2);
            else if (!empty && empty2)
                blockResult = world.clipWithInteractionOverride(next, vec, pos, blockShape, blockstate);
            else
                blockResult = world.clipWithInteractionOverride(vec, next, pos, blockShape, blockstate);

            if (blockResult != null)
            {
                health -= getHealthReduction.apply(blockResult, blockstate);
                results.add(Pair.of(blockResult, (float) startVec.distanceTo(blockResult.getLocation())));
                if (health <= 0)
                    break;
            }
        }
        return results;
    }

    public static List<Pair<BlockHitResult, Float>> rayTraceAndPenetrateBlocks4Efficiency(Level world, List<Vec3> vecs, float health, BiFunction<BlockHitResult, BlockState, Float> getHealthReduction)
    {
        List<Pair<BlockHitResult, Float>> results = new ArrayList<>();
        if (vecs.isEmpty())
            return results;
        Vec3 startVec = vecs.get(0);
        Map<BlockPos, BlockState> cachedStates = new HashMap<>();


        for (int i=0;i<vecs.size()-1;i++)
        {
            Vec3 vec = vecs.get(i);
            Vec3 next = vecs.get(i + 1);
            BlockPos pos = BlockPos.containing(vec);
            BlockPos pos2 = BlockPos.containing(next);

            BlockState blockstate = cachedStates.computeIfAbsent(pos, world::getBlockState);
            BlockState blockstate2 = cachedStates.computeIfAbsent(pos2, world::getBlockState);

            VoxelShape blockShape = blockstate.getCollisionShape(world, pos, CollisionContext.empty());
            VoxelShape blockShape2 = blockstate2.getCollisionShape(world, pos2, CollisionContext.empty());

            if ((isRayTraceIgnorableBlock(blockstate, blockShape) && isRayTraceIgnorableBlock(blockstate2, blockShape2)))
                continue;

            BlockHitResult blockResult;
            boolean empty = blockShape.isEmpty() || blockShape.toAabbs().stream().noneMatch((bb) -> bb.move(pos).contains(vec));
            boolean empty2 = blockShape2.isEmpty() || blockShape2.toAabbs().stream().noneMatch((bb) -> bb.move(pos2).contains(next));

            if (empty && !empty2)
                blockResult = world.clipWithInteractionOverride(vec, next, pos2, blockShape2, blockstate2);
            else if (!empty && empty2)
                blockResult = world.clipWithInteractionOverride(next, vec, pos, blockShape, blockstate);
            else
                blockResult = world.clipWithInteractionOverride(vec, next, pos, blockShape, blockstate);

            if (blockResult != null)
            {
                health -= getHealthReduction.apply(blockResult, blockstate);
                results.add(Pair.of(blockResult, (float) startVec.distanceTo(blockResult.getLocation())));
                if (health <= MIN_BULLET_DAMAGE)
                    break;
            }
        }
        return results;
    }

    public static List<Pair<EntityHitResult, Float>> rayTraceAndPenetrateEntities(Level worldIn, Entity projectile, List<Vec3> vecs, AABB boundingBox, Predicate<Entity> filter, float health, BiFunction<EntityHitResult, Entity, Float> getHealthReduction)
    {
        List<Pair<EntityHitResult, Float>> list = new ArrayList<>();
		if (vecs.isEmpty())
			return list;
        List<Entity> list2 = worldIn.getEntities(projectile, boundingBox, filter);
        Vec3 start = vecs.get(0);
        list2.sort(Comparator.comparingDouble((entity) -> entity.position().distanceTo(start)));
        entityLoop:
        {
            for (Entity target : list2)
            {
                if (!filter.test(target))
                    continue;
                for (Vec3 vec : getVecsHit(vecs, getEntityAABBForRayTracing(target)))
                {
                    EntityHitResult result = new EntityHitResult(target, vec);
                    list.add(Pair.of(result, (float) start.distanceTo(vec)));
                    health -= getHealthReduction.apply(result, target);
                    if (health <= MIN_BULLET_DAMAGE)
                        break entityLoop;
                }
            }
        }
        list.sort(Comparator.comparingDouble(Pair::getB));
        return list;
    }

    public static boolean isRayTraceIgnorableBlock(BlockState state, @Nullable VoxelShape shape)
    {
        return (shape != null && shape.isEmpty()) || state.isAir() || state.getBlock() == Blocks.BARRIER || state.getBlock() == Blocks.LIGHT;
    }

    public static List<Vec3> getVecsHit(List<Vec3> vecs, AABB bb)
    {
        List<Vec3> hits = new ArrayList<>();
        Optional<Vec3> startOptional = bb.clip(vecs.get(0), vecs.get(vecs.size() - 1));
        Optional<Vec3> endOptional = bb.clip(vecs.get(vecs.size() - 1), vecs.get(0));
        if (startOptional.isEmpty() || endOptional.isEmpty())
            return hits;
        Vec3 start = vecs.get(0);
        Vec3 actualStart = startOptional.get();
        Vec3 actualEnd = endOptional.get();
        double minDistance = start.distanceTo(actualStart);
        double maxDistance = start.distanceTo(actualEnd);
        for (Vec3 v : vecs)
        {
            double d = start.distanceTo(v);
            if (d < minDistance)
                continue;
            if (d > maxDistance)
                break;
            hits.add(v);
        }
        return hits;
    }

    @Deprecated
    public static List<Vec3> getAllVectors(Vec3 startVec, Vec3 endVec, int distance)
    {
        List<Vec3> vecs = new ArrayList<>();
        Vec3 step = endVec.subtract(startVec).normalize();
        int rayTraceCount = RAY_TRACE_ACCURACY;
        for (int i=0;i<distance;i++)
            for (int j=0;j<rayTraceCount;j++)
                vecs.add(startVec.add(step.scale(i / (double) rayTraceCount * j)));
        return vecs;
    }

    public static List<Vec3> getAllVectors2(Vec3 startVec, Vec3 endVec, int distance)
    {
        List<Vec3> vecs = new ArrayList<>();
        Vec3 step = endVec.subtract(startVec).normalize();
        int rayTraceCount = RAY_TRACE_ACCURACY;
        for (int i=0;i<distance;i++)
            for (int j=0;j<rayTraceCount;j++)
                vecs.add(startVec.add(step.scale(i).add(step.scale((float) j / (float) rayTraceCount))));
        return vecs;
    }

    public static List<Vec3> getAllVectors2Efficiency(Vec3 startVec, Vec3 endVec, int minY, int maxY, int distance)
    {
        List<Vec3> vecs = new ArrayList<>();
        Vec3 step = endVec.subtract(startVec).normalize().scale(1.0D / RAY_TRACE_ACCURACY);
        Vec3 last = startVec;
        for (int i=0;i<distance;i++)
            for (int j = 0; j < RAY_TRACE_ACCURACY; j++)
            {
                vecs.add(last);
                last = last.add(step);
                double y = last.get(Direction.Axis.Y);
                if (y < minY || y > maxY)
                    return vecs;
            }
        return vecs;
    }

    public static List<Vec3> getHitVectors(List<HitResult> results)
    {
        List<Vec3> vecs = new ArrayList<>();
        for (HitResult result : results)
            vecs.add(result.getLocation());
        return vecs;
    }

    public static void message(Player player, MutableComponent message, @Nullable ChatFormatting colour)
    {
        if (colour != null)
            message.setStyle(Style.EMPTY.withColor(TextColor.fromLegacyFormat(colour)));
        player.sendSystemMessage(message);
    }

    public static <E extends Entity> List<E> getEntitiesInSight(Class<E> clazz, LivingEntity from, int horizontalAngle, int verticalAngle, int range, Predicate<E> filter, boolean strict, boolean ignoreEntities, boolean ignoresVisionBlockingEntities)
    {
        return from.level().getEntitiesOfClass(clazz, from.getBoundingBox().inflate(range), filter).stream().filter((entity) -> isInSight(from, entity, true, true, horizontalAngle, verticalAngle, strict, ignoreEntities, ignoresVisionBlockingEntities)).collect(Collectors.toList());
    }

    public static boolean isInSight(@Nonnull Entity from, Entity to, boolean visual, boolean seeThrough, boolean strict, boolean ignoreEntities, boolean ignoresVisionBlockingEntities)
    {
        return isInSight(from, to, visual, seeThrough, 70, 50, strict, ignoreEntities, ignoresVisionBlockingEntities);
    }

    public static boolean isInSight(@Nonnull Entity from, Entity to, boolean visual, boolean seeThrough, int horizontalAngle, int verticalAngle, boolean strict, boolean ignoreEntities, boolean ignoresVisionBlockingEntities)
    {
        Vec3 toVec = to.position();
        if (!isInConeSightHorizontal(from, toVec, horizontalAngle))
            return false;
        if (!isInConeSightVertical(from, toVec, verticalAngle))
            return false;
        return canBeSeen(from, true, to, visual, seeThrough, strict, ignoreEntities, ignoresVisionBlockingEntities) != null;
    }

    public static boolean isInConeSightHorizontal(Entity from, Vec3 to, int horizontalAngle)
    {
        float angle = getRelativeDegreesHorizontal(from, to) % 360;
        return angle <= horizontalAngle || angle >= 360 - horizontalAngle;
    }

    public static boolean isInConeSightVertical(Entity from, Vec3 to, int verticalAngle)
    {
        float angle = getRelativeDegreesVertical(from, to);
        return Math.abs(angle) <= verticalAngle;
    }

    @Nullable
    public static Vec3 canBeSeen(@Nonnull Entity entity, boolean fromEyes, Entity target, boolean visual, boolean seeThrough, boolean strict, boolean ignoreEntities, boolean ignoresVisionBlockingEntities)
    {
        return canBeSeen(entity, null, fromEyes ? getEyePositionFor(entity) : entity.position(), target, visual, seeThrough, strict, ignoreEntities, ignoresVisionBlockingEntities);
    }

    @Nullable
    public static Vec3 canBeSeen(@Nullable Entity entity, @Nullable BlockPos ingoringPos, Vec3 from, Entity target, boolean visual, boolean seeThrough, boolean strict, boolean ignoreEntities, boolean ignoresVisionBlockingEntities)
    {
        Vec3 to = target.position();
        float height = target.getBbHeight();
        if (strict)
        {
            int accuracy = 10;
            float offset = height / accuracy;
            float d = Float.MAX_VALUE;
            Vec3 hit = null;
            for (int i = 0; i < accuracy; i++)
            {
                Vec3 to2 = to.add(0.0F, offset * accuracy, 0.0F);
                Vec3 hit2 = canBeSeen(target.level(), ingoringPos, entity, target, from, to2, visual, seeThrough, ignoreEntities, ignoresVisionBlockingEntities);
                if (hit2 != null)
                {
                    float d2 = (float) from.distanceTo(hit2);
                    if (d2 < d)
                    {
                        d = d2;
                        hit = hit2;
                    }
                }
            }
            return hit;
        }
        else
            return canBeSeen(target.level(), ingoringPos, entity, target, from, to.add(0.0F, height / 2.0F, 0.0F), visual, seeThrough, ignoreEntities, ignoresVisionBlockingEntities);
    }

    public static Vec3 canBeSeen(Level world, @Nullable BlockPos ingoringPos, @Nullable Entity entity, Entity target, Vec3 from, Vec3 to, boolean visual, boolean seeThrough, boolean ignoreEntities, boolean ignoresVisionBlockingEntities)
    {
        if (rayTraceBlocks(new ClipContext(from, to, visual ? ClipContext.Block.VISUAL : ClipContext.Block.COLLIDER, ClipContext.Fluid.NONE, entity == null ? CollisionContext.empty() : CollisionContext.of(entity)), world, ingoringPos, seeThrough).getType() != HitResult.Type.MISS)
            return null;
        EntityHitResult result = ignoreEntities ?
                rayTraceEntity(world, entity, from, to, (e) -> e == target, true) :
                rayTraceEntity(world, entity, from, to, (e) -> true, ignoresVisionBlockingEntities);
        return result == null ? null : result.getEntity() == target ? result.getLocation() : null;
    }

    public static EntityHitResult rayTraceEntity(Level world, @Nullable Entity entity, Vec3 from, Vec3 to, Predicate<Entity> test, boolean ignoresVisionBlockingEntities)
    {
        return rayTraceEntities(world, entity, from, to, new AABB(from, to).inflate(8.0F), (target) -> !target.isSpectator() && target.isPickable() && test.test(target), ignoresVisionBlockingEntities);
    }

    public static EntityHitResult rayTraceEntities(Level worldIn, Entity projectile, Vec3 startVec, Vec3 endVec, AABB boundingBox, Predicate<Entity> filter, boolean ignoresVisionBlockingEntities)
    {
        double d0 = Double.MAX_VALUE;
        Entity entity = null;
        Vec3 vec3d = null;
        for (Entity target : worldIn.getEntities(projectile, boundingBox, filter.and((e) -> !(e instanceof IVisionBlockingEntity) || (((IVisionBlockingEntity) e).shouldBeBlocking() && !ignoresVisionBlockingEntities))))
        {
            AABB bb = AVAWeaponUtil.getEntityAABBForRayTracing(target);
            if (bb.contains(startVec))
                return new EntityHitResult(target, startVec);
            Optional<Vec3> optional = bb.clip(startVec, endVec);
            if (optional.isPresent())
            {
                vec3d = optional.get();
                double d1 = startVec.distanceToSqr(vec3d);
                if (d1 < d0)
                {
                    entity = target;
                    d0 = d1;
                }
            }
        }
        return entity == null ? null : new EntityHitResult(entity, vec3d);
    }

    public static BlockHitResult rayTraceBlocks(ClipContext context, Level world, @Nullable BlockPos ingoringPos, boolean seeThrough)
    {
        return BlockGetter.traverseBlocks(context.getFrom(), context.getTo(), context, (context2, pos) -> {
            BlockState state = world.getBlockState(pos);
            FluidState fluidState = world.getFluidState(pos);
            if (isSeeThroughBlock(world, pos) && seeThrough)
                return null;
            if (pos.equals(ingoringPos))
                return null;
            Vec3 from = context2.getFrom();
            Vec3 to = context2.getTo();
            VoxelShape shape = context2.getBlockShape(state, world, pos);
            BlockHitResult result = world.clipWithInteractionOverride(from, to, pos, shape, state);
            VoxelShape fluidShape = context2.getFluidShape(fluidState, world, pos);
            BlockHitResult fluidResult = fluidShape.clip(from, to, pos);
            double distance = result == null ? Double.MAX_VALUE : context2.getFrom().distanceToSqr(result.getLocation());
            double fluidDistance = fluidResult == null ? Double.MAX_VALUE : context2.getFrom().distanceToSqr(fluidResult.getLocation());
            if (distance > fluidDistance) return fluidResult;
            if (result != null && AVAConstants.isFakeBLock(world.getBlockState(result.getBlockPos()).getBlock()))
                return null;
            return result;
        }, (context2) -> {
            Vec3 vector3d = context2.getFrom().subtract(context2.getTo());
            return BlockHitResult.miss(context2.getTo(), Direction.getNearest(vector3d.x, vector3d.y, vector3d.z), BlockPos.containing(context2.getTo()));
        });
    }

    public static double getAffectingRadiusForEntity(HandGrenadeEntity entity)
    {
        return max(entity.explosiveRadius, entity.flashRadius);
    }

    public static double getExplosiveDamageForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.level().isClientSide())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0.0D;
            if (weapon == Projectiles.M67.get() || weapon == Projectiles.M67_SPORTS.get())
                return M67_EXPLOSIVE_DAMAGE.get();
            else if (weapon == Projectiles.MK3A2.get())
                return MK3A2_EXPLOSIVE_DAMAGE.get();
            return ((HandGrenadeEntity) entity).damage;
        }
        return 0.0D;
    }

    public static double getExplosiveRadiusForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.level().isClientSide())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0.0D;
            if (weapon == Projectiles.M67.get() || weapon == Projectiles.M67_SPORTS.get())
                return M67_EXPLOSIVE_RANGE.get();
            else if (weapon == Projectiles.MK3A2.get())
                return MK3A2_EXPLOSIVE_RANGE.get();
            else if (weapon == SpecialWeapons.M202.get() || weapon == SpecialWeapons.RPG7.get())
                return M202_ROCKET_EXPLOSIVE_RANGE.get() * (GameModes.ESCORT.isRunning() ? 0.7F : 1.0F);
            else if (weapon == SpecialWeapons.GM94.get())
                return GM94_GRENADE_EXPLOSIVE_RANGE.get();
        }
        return 0.0D;
    }

    public static double getFlashRadiusForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.level().isClientSide())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0.0D;
            if (weapon == Projectiles.M116A1.get())
                return M116A1_FLASH_RANGE.get();
            else if (weapon == Projectiles.MK3A2.get())
                return MK3A2_FLASH_RANGE.get();
        }
        return 0.0D;
    }

    public static int getFlashDurationForEntity(Entity entity)
    {
        if (entity instanceof HandGrenadeEntity && !entity.level().isClientSide())
        {
            Item weapon = ((HandGrenadeEntity) entity).getWeapon();
            if (weapon == null)
                return 0;
            if (weapon == Projectiles.M116A1.get())
                return M116A1_FLASH_DURATION.get();
            else if (weapon == Projectiles.MK3A2.get())
                return MK3A2_FLASH_DURATION.get();
        }
        return 0;
    }

    public static Vec3 getEyePositionFor(Entity entity)
    {
        return entity.position().add(0.0F, entity.getEyeHeight(), 0.0F);
    }

    public static boolean isWeaponDisabled(Item item)
    {
        if (DatagenModLoader.isRunningDataGen())
            return false;
        if (!AVACrossWorldData.getInstance().playMode.test(item))
            return true;
        String[] items = DISABLED_WEAPONS.get().split(",");
        for (int i = 0; i < items.length; i++)
            items[i] = items[i].replace(" ", "").toLowerCase(Locale.ROOT);
        return Arrays.stream(items).anyMatch((disabled) -> disabled.equals(BuiltInRegistries.ITEM.getKey(item).getPath())) || !Classification.isTypeEnabled(item);
    }

    public static boolean isBlockMeleeDestroyable(Block block)
    {
        String[] items = MELEE_DESTROYABLE_BLOCK.get().split(",");
        for (int i = 0; i < items.length; i++)
            items[i] = items[i].replace(" ", "").toLowerCase(Locale.ROOT);
        return Arrays.stream(items).anyMatch((disabled) -> disabled.equals(AVACommonUtil.getRegistryNameBlock(block).getPath()));
    }

    private static final List<Item> MAIN_WEAPONS = new ArrayList<>();
    private static final List<Item> SECONDARY_WEAPONS = new ArrayList<>();
    private static final List<Item> MELEE_WEAPONS = new ArrayList<>();
    private static final List<Item> PROJECTILE_WEAPONS = new ArrayList<>();
    private static final List<Item> SPECIAL_WEAPONS = new ArrayList<>();

    public static List<Item> getMainWeapons()
    {
        if (MAIN_WEAPONS.isEmpty())
        {
            addIfValid(MAIN_WEAPONS, SubmachineGuns.ITEM_SUBMACHINE_GUNS);
            addIfValid(MAIN_WEAPONS, Rifles.ITEM_RIFLES);
            addIfValid(MAIN_WEAPONS, Snipers.ITEM_SNIPERS);
        }
        return removeAllInvalid(MAIN_WEAPONS);
    }

    public static List<Item> getSecondaryWeapons()
    {
        return removeAllInvalid(Pistols.ITEM_PISTOLS);
    }

    public static List<Item> getMeleeWeapons()
    {
        return removeAllInvalid(AVACommonUtil.cast(MeleeWeapons.ITEM_MELEE_WEAPONS, Item.class));
    }

    public static List<Item> getProjectileWeapons()
    {
        return removeAllInvalid(AVACommonUtil.cast(Projectiles.ITEM_PROJECTILES, Item.class));
    }

    public static List<Item> getSpecialWeapons()
    {
        return removeAllInvalid(SpecialWeapons.ITEM_SPECIAL_WEAPONS);
    }

    private static void addIfValid(List<Item> list, List<Item> toAdd)
    {
        list.addAll(new ArrayList<>(removeAllInvalid(toAdd)));
    }

    public static List<Item> removeAllInvalid(List<Item> original)
    {
        List<Item> list = new ArrayList<>(original);
        list.removeIf(AVAWeaponUtil::isWeaponDisabled);
        return list;
    }

    public static List<AVAItemGun> getAllGuns()
    {
        return AVACommonUtil.cast(getAllGunLikes((item) -> !(item instanceof AVAItemGun)), AVAItemGun.class);
    }

    public static List<Item> getAllGunLikes(@Nullable Predicate<Item> predicate)
    {
        List<Item> list = new ArrayList<>(SubmachineGuns.ITEM_SUBMACHINE_GUNS);
        list.addAll(Pistols.ITEM_PISTOLS);
        list.addAll(Rifles.ITEM_RIFLES);
        list.addAll(Snipers.ITEM_SNIPERS);
        list.addAll(SpecialWeapons.ITEM_SPECIAL_WEAPONS);
        if (predicate != null)
            list.removeIf(predicate);
        return list;
    }

    public static List<Item> getAllAmmo(@Nullable Predicate<Item> predicate)
    {
        List<Item> list = new ArrayList<>(Ammo.AMMO);
        if (predicate != null)
            list.removeIf(predicate);
        return list;
    }

    public static <E extends Entity & IOwner> boolean checkForSpecialBlockHit(E entity, BlockHitResult blockResult)
    {
        BlockState state = entity.level().getBlockState(blockResult.getBlockPos());
        Block block = state.getBlock();
        LivingEntity shooter = entity.getShooter();
        if (block instanceof BellBlock)
            return ((BellBlock) block).onHit(entity.level(), state, blockResult, shooter instanceof Player ? (Player) shooter : null, true);
        else if (block instanceof NoteBlock && shooter instanceof Player)
        {
            state.useWithoutItem(entity.level(), (Player) shooter, blockResult);
            return true;
        }
        return false;
    }

    public static boolean igniteTNTOnHit(Level world, BlockState state, BlockPos pos, @Nullable LivingEntity igniter)
    {
        Block block = state.getBlock();
        if (block instanceof TntBlock)
        {
            if (!world.isClientSide())
            {
                block.onCaughtFire(state, world, pos, null, igniter);
                world.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
            }
            return true;
        }
        return false;
    }

    public static boolean attackExplosiveBarrelOnHit(Level world, BlockPos pos, @Nullable LivingEntity attacker, float damage)
    {
        BlockEntity te = world.getBlockEntity(pos);
        if (te instanceof ExplosiveBarrelTE)
        {
            if (!world.isClientSide())
                ((ExplosiveBarrelTE) te).attack(attacker, damage * 2.5F);
            return true;
        }
        return false;
    }

    public static DeferredItem<Item> createNormal(DeferredRegister.Items registry, String name, AVAItemGun.Properties properties, Recipe recipe)
    {
        return create(registry, name, properties, recipe, AVAItemGun::new);
    }

    public static DeferredItem<Item> create(DeferredRegister.Items registry, String name, AVAItemGun.Properties properties, Recipe recipe, BiFunction<AVAItemGun.Properties, Recipe, Item> constructor)
    {
        return registry.register(name, () -> constructor.apply(properties, recipe));
    }

    public static DeferredItem<Item> createSkinnedNormal(DeferredRegister.Items register, String name, DeferredItem<Item> master, Recipe recipe)
    {
        return createSkinned(register, name, master, recipe, AVAItemGun::new);
    }

    public static DeferredItem<Item> createSkinned(DeferredRegister.Items registry, String name, DeferredItem<Item> master, Recipe recipe, BiFunction<AVAItemGun.Properties, Recipe, Item> constructor)
    {
        return registry.register(master.getId().getPath() + "_" + name, () -> {
            AVAItemGun gun = (AVAItemGun) master.get();
            return constructor.apply(new AVAItemGun.Properties(gun.getProperties()).skinned(gun), new Recipe().addItem(gun).mergeIngredients(recipe));
        });
    }

	public static List<ItemStack> clearPlayerInventory(Player player)
	{
	    List<ItemStack> stacks = new ArrayList<>();
		for (int i = 0; i < player.getInventory().getContainerSize(); i++)
            player.getInventory().removeItemNoUpdate(i);
        return stacks;
	}

	public static ItemStack clearAnimationData(ItemStack stack)
    {
        if (stack.getItem() instanceof AVAItemGun)
        {
            stack.set(AVADataComponents.TAG_ITEM_FIRE, 0);
            stack.set(AVADataComponents.TAG_ITEM_TICKS, 0.0F);
            stack.set(AVADataComponents.TAG_ITEM_RELOAD, 0);
            stack.set(AVADataComponents.TAG_ITEM_DRAW, 0);
            for (WeaponCategory value : WeaponCategory.values())
                stack.remove(value.getTag());
        }
        return stack;
    }

    public static float getRotationYaw(Entity entity)
    {
        float yaw = entity.getYHeadRot() % 360;
        if (yaw < 0)
            yaw += 360;
        return yaw;
    }

    public static float getRelativeDegreesVertical(Entity origin, Vec3 target)
    {
        double a = sqrt(pow(target.x - origin.getX(), 2.0F) + pow(target.z - origin.getZ(), 2.0F));
        return (float) (Math.toDegrees(asin((target.y - origin.getY()) / a)) + origin.getXRot());
    }

    public static float getRelativeDegreesHorizontal(Entity origin, Vec3 target)
    {
        float yaw = getRotationYaw(origin);
        float q = 90;
        boolean xp = target.x < origin.getX();
        boolean zp = target.z > origin.getZ();
        if (xp && zp)
            q *= 0;
        else if (xp)
            q *= 1;
        else if (!zp)
            q *= 2;
        else
            q *= 3;
        double z = abs(target.z - origin.getZ());
        double x = abs(target.x - origin.getX());
        float direction;
        if (q == 0 || q == 180)
            direction = (float) (atan2(x, z));
        else
            direction = (float) (atan2(z, x));
        direction = (float) (direction * 180.0D / PI) + q;
        float angle = abs(direction - yaw);
        if (direction < yaw) angle = 360 - angle;
        return angle % 360;
    }

    public static double getAngleFromCoord(double x, double y)
    {
        double angle;
        angle = abs(atan2(x, y) * 180.0D / PI);
        if (x < 0)
            angle = 360 - angle;
        return angle;
    }

    public static boolean isValidEntity(Entity entity)
    {
        return entity != null && entity.isAlive() && !entity.isSpectator();
    }

    public static boolean canReceiveHealing(LivingEntity entity)
    {
        return entity.getHealth() > 0.0F && entity.getHealth() < entity.getMaxHealth();
    }

    @Deprecated
    public static <E extends Entity> E findNearestEntity(Vec3 from, Collection<E> entities)
    {
        E nearest = null;
        double distance = Double.MAX_VALUE;
        for (E entity : entities)
            distance = Math.min(distance, from.distanceTo(entity.position()));
        return nearest;
    }

    public static boolean forEachBlockPos(BlockPos from, int horizontalRadius, int verticalRadius, Predicate<BlockPos> action)
    {
        for (int x=from.getX()-horizontalRadius;x<from.getX()+horizontalRadius+1;x++)
            for (int y=from.getY()-verticalRadius;y<from.getY()+verticalRadius;y++)
                for (int z=from.getZ()-horizontalRadius;z<from.getZ()+horizontalRadius+1;z++)
                    if (action.test(new BlockPos(x, y, z)))
                        return true;
        return false;
    }

    public static boolean isOnSite(Entity entity)
    {
        return isOnSite(entity, true) || isOnSite(entity, false);
    }

    public static boolean isOnSite(Entity entity, boolean a)
    {
        int radius = (a ? A_SITE_RADIUS : B_SITE_RADIUS).get();
        int height = (a ? A_SITE_HEIGHT : B_SITE_HEIGHT).get();
        if (radius < 0 || height < 0)
            return false;
        return isOnSite(entity, radius, height, (a ? AVABlocks.SITE_A : AVABlocks.SITE_B).get());
    }

    public static boolean isOnSite(Entity entity, int radius, int height, Block block)
    {
        return forEachBlockPos(entity.blockPosition(), radius, height, (pos) -> entity.level().getBlockState(pos).getBlock() == block);
    }

    public static CompoundTag writeVec(Vec3 vec)
    {
        return writeVec(new CompoundTag(), vec);
    }

    public static CompoundTag writeVec(CompoundTag compound, Vec3 vec)
    {
        DataTypes.DOUBLE.write(compound, "x", vec.x);
        DataTypes.DOUBLE.write(compound, "y", vec.y);
        DataTypes.DOUBLE.write(compound, "z", vec.z);
        return compound;
    }

    @Nullable
    public static Vec3 readVec(CompoundTag compound)
    {
        if (compound.contains("x") && compound.contains("y") && compound.contains("z"))
            return new Vec3(DataTypes.DOUBLE.read(compound, "x"), DataTypes.DOUBLE.read(compound, "y"), DataTypes.DOUBLE.read(compound, "z"));
        return null;
    }

    public static boolean setEntityPosByTeamSpawn(Entity entity, int radius)
    {
//        IWorldData cap = WorldData.getCap(entity.level);
        AVAWorldData data = AVAWorldData.getInstance(entity.level());
        Team team = entity.getTeam();
        if (team != null)
        {
            String name = team.getName();
//            PositionWithRotation spawn = cap.getTeamSpawn(name);
            List<PositionWithRotation> spawns = data.teamSpawns.get(name);
            if (spawns != null && !spawns.isEmpty())
            {
                PositionWithRotation spawn = spawns.get(RAND.nextInt(spawns.size()));
                radius = spawn.getRadius() > 0 ? spawn.getRadius() : radius;
                Vec3 spawnVec = spawn.getVec();

                Vec3 randVec;
                for (int i=0;i<10;i++)
                {
                    randVec = spawnVec.add(randomOffset(radius), 0.2F, randomOffset(radius));
                    if (!SPAWN_RESTRICTION.get() || canEntitySpawnAt(entity.level(), BlockPos.containing(randVec)))
                    {
                        spawnVec = randVec;
                        break;
                    }
                }
                entity.setYHeadRot(spawn.getYaw());
                if (entity instanceof ServerPlayer)
                {
                    ChunkPos chunkpos = new ChunkPos(vecToPos(spawnVec));
                    ((ServerLevel) entity.level()).getChunkSource().addRegionTicket(TicketType.POST_TELEPORT, chunkpos, 1, entity.getId());
                    ((ServerPlayer) entity).connection.teleport(spawnVec.x, spawnVec.y, spawnVec.z, spawn.getYaw(), spawn.getPitch(), Sets.newHashSet());
                }
                else
                    entity.moveTo(spawnVec.x, spawnVec.y, spawnVec.z, spawn.getYaw(), spawn.getPitch());
                return true;
            }
        }
        return false;
    }

    public static double randomOffset(int maxOffset)
    {
        RandomSource rand = AVAConstants.RAND;
        return rand.nextDouble() * maxOffset * (rand.nextBoolean() ? -1 : 1);
    }

    public static void warnNearbySmarts(Player attacked, LivingEntity attacker)
    {
        TeamSide side = TeamSide.getSideFor(attacked);
        if (side != null)
        {
            Class<? extends SidedSmartAIEntity> clazz = null;
            if (side == TeamSide.EU)
                clazz = EUSmartEntity.class;
            else if (side == TeamSide.NRF)
                clazz = NRFSmartEntity.class;
            if (clazz != null)
                warnNearbySmarts(clazz, attacked, attacked.getBoundingBox().inflate(10.0D, 4.0D, 10.0D), attacker);
        }
    }

    public static <C extends SidedSmartAIEntity> void warnNearbySmarts(Class<C> clazz, LivingEntity attacked, AABB range, LivingEntity attacker)
    {
        warnNearbySmarts(clazz, attacked, range, attacker, new ArrayList<>());
    }

    public static <C extends SidedSmartAIEntity> void warnNearbySmarts(Class<C> clazz, LivingEntity attacked, AABB range, LivingEntity attacker, List<SidedSmartAIEntity> called)
    {
        for (SidedSmartAIEntity entity : attacked.level().getEntitiesOfClass(clazz, range, (entity) -> isValidEntity(entity) && entity.getSide().isSameSide(attacked) && !called.contains(entity)))
        {
            called.add(entity);
            entity.setCommonAttackTarget(attacker);

            warnNearbySmarts(clazz, attacked, entity.getBoundingBox().inflate(10.0D, 4.0D, 10.0D), attacker, called);
        }
    }

    public static boolean canEntitySpawnAt(Level world, BlockPos pos)
    {
        return !isAir(world, pos.below()) && isAir(world, pos) && isAir(world, pos.above());
    }

    public static boolean isAir(Level world, BlockPos pos)
    {
        return world.getBlockState(pos).isAir();
    }

    public static AABB getEntityAABBForRayTracing(Entity entity)
    {
        AABB bb = entity instanceof IVisionBlockingEntity ? ((IVisionBlockingEntity) entity).getVisionBlockingBox() : entity.getBoundingBox();
        return bb.inflate(0.15F).expandTowards(entity.getDeltaMovement().multiply(-10.0F, entity.onGround() ? 0.0F : -10.0F, -10.0F));
    }

    public static boolean isOnGroundNotStrict(Entity entity)
    {
        if (entity.onGround())
            return true;
        AABB bb = entity.getBoundingBox().expandTowards(0, -2F, 0);
        Level world = entity.level();
        Predicate<BlockPos> test = (pos) -> {
            VoxelShape shape = world.getBlockState(pos).getCollisionShape(world, pos);
            if (shape.isEmpty())
                return false;
            return shape.bounds().move(pos).intersects(bb);
        };
        return test.test(entity.blockPosition()) || test.test(entity.blockPosition().below());
    }

    @Nullable
    public static <T extends Entity> T getEntityByUUID(Class<T> clazz, Level world, UUID id)
    {
        if (world == null)
            return null;
        if (!world.isClientSide())
        {
            Entity entity = ((ServerLevel) world).getEntity(id);
            return entity != null && clazz.isAssignableFrom(entity.getClass()) ? (T) entity : null;
        }
        return null;
    }

    public static class ExplosionForBlocks extends Explosion
    {
        private static final ExplosionDamageCalculator DEFAULT_CONTEXT = new ExplosionDamageCalculator();
        private final Level world;
        private final double x;
        private final double y;
        private final double z;
        @Nullable
        private final Entity exploder;
        private final double size;
        private final ExplosionDamageCalculator context;
        private final ObjectArrayList<BlockPos> affectedBlockPositions = new ObjectArrayList<>();

        public ExplosionForBlocks(Level world, @Nullable Entity exploder, double x, double y, double z, double size)
        {
            super(world, exploder, null, null, x, y, z, (float) size, false, BlockInteraction.DESTROY, ParticleTypes.EXPLOSION, ParticleTypes.EXPLOSION_EMITTER, SoundEvents.GENERIC_EXPLODE);
            this.world = world;
            this.exploder = exploder;
            this.size = size;
            this.x = x;
            this.y = y;
            this.z = z;
            this.context = this.getEntityExplosionContext(exploder);
            if (!net.neoforged.neoforge.event.EventHooks.onExplosionStart(world, this))
            {
                explode();
                doExplosionB();
            }
        }

        private ExplosionDamageCalculator getEntityExplosionContext(@Nullable Entity entity)
        {
            return entity == null ? DEFAULT_CONTEXT : new EntityBasedExplosionDamageCalculator(entity);
        }

        @Override
        public void explode()
        {
            int destructionType = EXPLOSION_DESTRUCTION_TYPE.get();
            if (destructionType == 0)
                return;
            Set<BlockPos> set = Sets.newHashSet();
            for(int j = 0; j < 16; ++j)
                for(int k = 0; k < 16; ++k)
                    for(int l = 0; l < 16; ++l)
                        if (j == 0 || j == 15 || k == 0 || k == 15 || l == 0 || l == 15)
                        {
                            double d0 = (float)j / 15.0F * 2.0F - 1.0F;
                            double d1 = (float)k / 15.0F * 2.0F - 1.0F;
                            double d2 = (float)l / 15.0F * 2.0F - 1.0F;
                            double d3 = sqrt(d0 * d0 + d1 * d1 + d2 * d2);
                            d0 = d0 / d3;
                            d1 = d1 / d3;
                            d2 = d2 / d3;
                            float f = (float) (this.size * (0.7F + this.world.random.nextFloat() * 0.6F));
                            double d4 = this.x;
                            double d6 = this.y;
                            double d8 = this.z;
                            for(; f > 0.0F; f -= 0.22500001F)
                            {
                                BlockPos blockpos = BlockPos.containing(d4, d6, d8);
                                BlockState blockstate = this.world.getBlockState(blockpos);
                                FluidState fluidstate = this.world.getFluidState(blockpos);
                                Optional<Float> optional = this.context.getBlockExplosionResistance(this, this.world, blockpos, blockstate, fluidstate);
                                if (optional.isPresent())
                                    f -= (optional.get() + 0.3F) * 0.3F;
                                if (f > 0.0F && this.context.shouldBlockExplode(this, this.world, blockpos, blockstate, f) && exploder != null && exploder.position().distanceTo(new Vec3(blockpos.getX(), blockpos.getY(), blockpos.getZ())) <= size)
                                {
                                    boolean isGlass = blockstate.getSoundType() == SoundType.GLASS;
                                    if (destructionType == 1)
                                    {
                                        if (isGlass)
                                            set.add(blockpos);
                                    }
                                    else if (destructionType == 3)
                                    {
                                        if (isGlass && exploder instanceof C4Entity)
                                            set.add(blockpos);
                                    }
                                    else
                                        set.add(blockpos);
                                }
                                d4 += d0 * (double)0.3F;
                                d6 += d1 * (double)0.3F;
                                d8 += d2 * (double)0.3F;
                            }
                        }
            this.affectedBlockPositions.addAll(set);
        }

        public void doExplosionB()
        {
            ObjectArrayList<Pair<ItemStack, BlockPos>> objectarraylist = new ObjectArrayList<>();
            Util.shuffle(this.affectedBlockPositions, this.world.random);

            for (BlockPos blockpos : this.affectedBlockPositions)
            {
                BlockState blockstate = this.world.getBlockState(blockpos);
                if (!blockstate.isAir())
                {
                    BlockPos blockpos1 = blockpos.immutable();
                    this.world.getProfiler().push("explosion_blocks");
                    if (blockstate.canDropFromExplosion(this.world, blockpos, this) && this.world instanceof ServerLevel)
                    {
                        BlockEntity tileentity = blockstate.getBlock() instanceof EntityBlock ? this.world.getBlockEntity(blockpos) : null;
                        LootParams.Builder lootparams$builder = (new LootParams.Builder((ServerLevel) world))
                                .withParameter(LootContextParams.ORIGIN, Vec3.atCenterOf(blockpos))
                                .withParameter(LootContextParams.TOOL, ItemStack.EMPTY)
                                .withOptionalParameter(LootContextParams.BLOCK_ENTITY, tileentity)
                                .withOptionalParameter(LootContextParams.THIS_ENTITY, exploder)
                                .withParameter(LootContextParams.EXPLOSION_RADIUS, (float) size);
                        blockstate.getDrops(lootparams$builder).forEach((stack) -> handleExplosionDrops(objectarraylist, stack, blockpos1));
                    }
                    blockstate.onBlockExploded(this.world, blockpos, this);
                    this.world.getProfiler().pop();
                }
            }
            for (Pair<ItemStack, BlockPos> pair : objectarraylist)
                Block.popResource(this.world, pair.getB(), pair.getA());
        }

        private static void handleExplosionDrops(ObjectArrayList<Pair<ItemStack, BlockPos>> dropPositionArray, ItemStack stack, BlockPos pos)
        {
            int i = dropPositionArray.size();
            for(int j = 0; j < i; ++j)
            {
                Pair<ItemStack, BlockPos> pair = dropPositionArray.get(j);
                ItemStack itemstack = pair.getA();
                if (ItemEntity.areMergable(itemstack, stack))
                {
                    ItemStack itemstack1 = ItemEntity.merge(itemstack, stack, 16);
                    dropPositionArray.set(j, Pair.of(itemstack1, pair.getB()));
                    if (stack.isEmpty())
                        return;
                }
            }
            dropPositionArray.add(Pair.of(stack, pos));
        }

        @Override
        public void clearToBlow()
        {
            this.affectedBlockPositions.clear();
        }

        @Override
        public List<BlockPos> getToBlow()
        {
            return this.affectedBlockPositions;
        }

        @org.jetbrains.annotations.Nullable
        @Override
        public Entity getDirectSourceEntity()
        {
            return exploder;
        }
    }

    public enum WeaponCategory
    {
        MAIN(0, AVADataComponents.TAG_ITEM_GUN_WEAPON_CATEGORY_MAIN),
        SECONDARY(1, AVADataComponents.TAG_ITEM_GUN_WEAPON_CATEGORY_SECONDARY),
        MELEE(2, AVADataComponents.TAG_ITEM_GUN_WEAPON_CATEGORY_MELEE),
        PROJECTILES(3, AVADataComponents.TAG_ITEM_GUN_WEAPON_CATEGORY_PROJECTILE),
        SPECIAL(4, AVADataComponents.TAG_ITEM_GUN_WEAPON_CATEGORY_SPECIAL)
        ;
        private final int index;
        private final Supplier<DataComponentType<Integer>> tag;
        WeaponCategory(int index, Supplier<DataComponentType<Integer>> tag)
        {
            this.index = index;
            this.tag = tag;
        }

        @Nullable
        public static WeaponCategory fromItem(Item item)
        {
            return item instanceof IClassification ? fromIndex(((IClassification) item).getClassification().getSlotIndex() - 1) : null;
        }

        public static WeaponCategory fromIndex(int index)
        {
            return Arrays.stream(values()).filter((cat) -> cat.index == index).findFirst().orElse(MAIN);
        }

        public List<ItemStack> getStacks(Inventory inventory)
        {
            List<ItemStack> stacks = new ArrayList<>();
            for (int i = 0; i < inventory.getContainerSize(); i++)
            {
                ItemStack stack = inventory.getItem(i);
                if (stack.has(tag))
                    stacks.add(stack);
            }
            stacks.sort(Comparator.comparingInt((stack2) -> stack2.getOrDefault(tag, 0)));
            return stacks;
        }

        public List<Pair<ItemStack, Integer>> getStacksAndSlot(Inventory inventory)
        {
            List<Pair<ItemStack, Integer>> stacks = new ArrayList<>();
            for (int i = 0; i < inventory.getContainerSize(); i++)
            {
                ItemStack stack = inventory.getItem(i);
                if (stack.has(tag))
                    stacks.add(Pair.of(stack, i));
            }
            stacks.sort(Comparator.comparingInt((stack2) -> stack2.getA().getOrDefault(tag, 0)));
            return stacks;
        }

        public int getIndex()
        {
            return index;
        }

        public Supplier<DataComponentType<Integer>> getTag()
        {
            return tag;
        }

        public void addTo(Inventory inventory, ItemStack stack)
        {
            stack.set(getTag(), getStacks(inventory).size());
            inventory.add(stack);
        }
    }

    public static boolean isPrimaryWeapon(Item item)
    {
        if (AVAServerConfig.isCompetitiveModeActivated() && item == SpecialWeapons.RPG7.get())
            return true;
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 1;
    }

    public static boolean isSecondaryWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 2;
    }

    public static boolean isMeleeWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 3;
    }

    public static boolean isProjectile(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 4;
    }

    public static boolean isSpecialWeapon(Item item)
    {
        return item instanceof IClassification && ((IClassification) item).getClassification().getSlotIndex() == 5;
    }

    public enum Classification
    {
        SNIPER(1, 50),
        SNIPER_SEMI(1, 50),
        RIFLE(1, 45),
        SUB_MACHINEGUN(1, 40),
        SHOTGUN(1, 45),
        PISTOL(2, 35),
        PISTOL_AUTO(2, 35),
        MELEE_WEAPON(3, -1),
        PROJECTILE(4, -1),
        SPECIAL_WEAPON(5, 20),

        EXCLUDED(-1, -1);

        private final int slotIndex;
        private final int soundDistance;

        Classification(int slotIndex, int soundDistance)
        {
            this.slotIndex = slotIndex;
            this.soundDistance = soundDistance;
        }

        public int getSlotIndex()
        {
            return slotIndex;
        }

        public int getSoundDistance()
        {
            return soundDistance;
        }

        public static Classification fromSlot(int index)
        {
            return Arrays.stream(values()).filter((classification) -> classification.slotIndex == index).findFirst().orElse(SNIPER);
        }

        public Classification addToList(Item item)
        {
            switch (this)
            {
                case SNIPER:
                case SNIPER_SEMI:
                    Snipers.ITEM_SNIPERS.add(item);
                    break;
                case RIFLE:
                    Rifles.ITEM_RIFLES.add(item);
                    break;
                case SUB_MACHINEGUN:
                case SHOTGUN:
                    SubmachineGuns.ITEM_SUBMACHINE_GUNS.add(item);
                    break;
                case PISTOL:
                case PISTOL_AUTO:
                    Pistols.ITEM_PISTOLS.add(item);
                    break;
                case MELEE_WEAPON:
                    MeleeWeapons.ITEM_MELEE_WEAPONS.add((AVAMeleeItem) item);
                    break;
                case PROJECTILE:
                    Projectiles.ITEM_PROJECTILES.add((ThrowableItem) item);
                    break;
                case SPECIAL_WEAPON:
                    SpecialWeapons.ITEM_SPECIAL_WEAPONS.add(item);
                    break;
            }
            return this;
        }

        public static boolean isTypeEnabled(Item item)
        {
            if (item instanceof IClassification)
                return ((IClassification) item).getClassification().isTypeEnabled();
            return true;
        }

        public boolean isTypeEnabled()
        {
            switch (this)
            {
                case SNIPER:
                    return ENABLE_SNIPERS.get();
                case SNIPER_SEMI:
                    return ENABLE_SEMI_SNIPERS.get();
                case RIFLE:
                    return ENABLE_RIFLES.get();
                case SUB_MACHINEGUN:
                    return ENABLE_SUB_MACHINEGUNS.get();
                case SHOTGUN:
                    return ENABLE_SHOTGUNS.get();
                case PISTOL:
                    return ENABLE_PISTOLS.get();
                case PISTOL_AUTO:
                    return ENABLE_AUTO_PISTOLS.get();
                case MELEE_WEAPON:
                    return true;
                case PROJECTILE:
                    return ENABLE_PROJECTILES.get();
                case SPECIAL_WEAPON:
                    return ENABLE_SPECIAL_WEAPONS.get();
            }
            return true;
        }

        public boolean isSniper()
        {
            return isSniper(this);
        }

        public static boolean isSniper(Classification classification)
        {
            return classification == SNIPER || classification == SNIPER_SEMI;
        }

        private static Set<UUID> YELLED = new HashSet<>();
        public boolean validate(Player player, ItemStack stack)
        {
            if (!player.getAbilities().instabuild && !player.level().isClientSide() && isWeaponDisabled(stack.getItem()))
            {
                player.sendSystemMessage(Component.translatable("ava.chat.weapon_disabled"));
                UUID uuid = player.getUUID();
                if (!YELLED.contains(uuid))
                {
                    player.sendSystemMessage(Component.translatable("ava.chat.weapon_disabled_bug", "/ava setPlayMode free"));
                    YELLED.add(uuid);
                }
                return false;
            }
            return true;
        }
    }

    public enum TeamSide
    {
        EU(0x0000FF, SubmachineGuns.X95R, SubmachineGuns.MK18, SubmachineGuns.KRISS, Rifles.M16_VN, Rifles.FN_FNC, Rifles.XM8, Rifles.SCAR_L, Snipers.M24, Pistols.SW1911_COLT, SubmachineGuns.D_DEFENSE_10GA),
        NRF(0xFF0000, SubmachineGuns.MP5K, SubmachineGuns.MP5SD5, SubmachineGuns.SR_2M_VERESK, Rifles.FG42, Rifles.AK12, Rifles.SG556, Rifles.M1_GARAND, Snipers.MOSIN_NAGANT, Pistols.P226, SubmachineGuns.REMINGTON870),
        NEUTRAL(0xFFFF00),
        AGGRESIVE(0x0000FF);

        private final int colour;
        private final DeferredItem<Item>[] weaponSuppliers;
        private final List<Item> weapons = new ArrayList<>();

        @SafeVarargs
        TeamSide(int colour, DeferredItem<Item>... weapons)
        {
            this.colour = colour;
            this.weaponSuppliers = weapons;
        }

        public static Optional<TeamSide> fromName(String name)
        {
            String finalName = name.toUpperCase(Locale.ROOT);
            return Arrays.stream(values()).filter((e) -> e.name().equals(finalName)).findFirst();
        }

        public int getColour()
        {
            return colour;
        }

        public MutableComponent randomName()
        {
            return randomName(this);
        }

        public static MutableComponent randomName(TeamSide side)
        {
            String pref = null;
            int length = 0;
            if (side == EU)
            {
                pref = AVA.MODID + ".entity.nicknames.eu_";
                length = EU_NAMES;
            }
            else if (side == NRF)
            {
                pref = AVA.MODID + ".entity.nicknames.nrf_";
                length = NRF_NAMES;
            }
            if (pref != null)
                return Component.translatable(pref + RAND.nextInt(1, length + 1));
            return Component.empty();
        }

        public List<Item> getWeapons()
        {
            if (weapons.isEmpty())
                for (DeferredItem<Item> weaponSupplier : weaponSuppliers)
                    weapons.add(weaponSupplier.get());
            return weapons.stream().filter((item) -> !AVAWeaponUtil.isWeaponDisabled(item)).toList();
        }

        public boolean isOppositeSide(LivingEntity entity)
        {
            TeamSide side = getSideFor(entity);
            return (this == EU && side == NRF) || (this == NRF && side == EU);
        }

        public boolean isSameSide(LivingEntity entity)
        {
            return this == getSideFor(entity);
        }

        public boolean isSideArmour(Item item)
        {
            return item instanceof ArmorItem armour && AVAArmourMaterials.getSideFor(armour.getMaterial().value()) == this;
        }

        @Nullable
        public static TeamSide fromTeam(LivingEntity entity)
        {
            PlayerTeam team = entity.level().getScoreboard().getPlayersTeam(entity.getScoreboardName());
            if (team != null)
                return fromName(team.getName()).orElse(null);
            return null;
        }

        @Nullable
        public static TeamSide getSideFor(LivingEntity entity)
        {
            if (entity instanceof ISidedEntity sided && !sided.dynamicSide())
                return sided.getSide();
            return getSideFromArmour(entity);
        }

        @Nullable
        public static TeamSide getSideFromArmour(LivingEntity entity)
        {
            for (TeamSide side : values())
                if (isFullEquippedSide(entity, side))
                    return side;
            return null;
        }
    }

    public interface ISidedEntity
    {
        TeamSide getSide();

        // If the entity can change, use armour to check instead
        default boolean dynamicSide()
        {
            return false;
        }
    }

    public enum RadioCategory
    {
        Z,
        X,
        C,
        NONE;

        private final List<RadioMessage> messages = new ArrayList<>();

        RadioCategory()
        {
        }

        private void add(RadioMessage message)
        {
            messages.add(message);
        }

        public List<RadioMessage> getMessages()
        {
            return messages;
        }

        public boolean exists()
        {
            return this != NONE;
        }
    }

    public enum RadioMessage
    {
        Z1(Z, 3, 3),
        Z2(Z, 4, 4),
        Z3(Z, 3, 3),
        Z4(Z, 3, 3),
        Z5(Z, 3, 3),
        Z6(Z, 4, 3),
        Z7(Z, 1, 2),
        Z8(Z, 2, 2),
        Z9(Z, 2, 2),
        Z0(Z),

        X1(X, 1, 1),
        X2(X, 1, 1),
        X3(X, 2, 2),
        X4(X, 1, 1),
        X5(X, 4, 3),
        X6(X, 2, 2),
        X7(X, 3, 3),
        X8(X, 2, 2),
        X9(X, 10, 16),
        X0(X),

//        C1(C),
//        C2(C),
//        C3(C),
//        C4(C),
//        C5(C),
//        C6(C),
//        C7(C),
//        C8(C),
//        C9(C),
//        C0(C)
        ;

        private final MutableComponent displayName;
        private final RadioCategory category;
        private Supplier<SoundEvent> euSound;
        private Supplier<SoundEvent> nrfSound;
        private final Pair<Integer, Integer> multiple;

        RadioMessage(RadioCategory category)
        {
            this(category, 1, 1);
        }

        RadioMessage(RadioCategory category, int multiple, int multiple2)
        {
            this.displayName = Component.translatable("ava.radio." + name().toLowerCase(Locale.ROOT));
            this.category = category;
            category.add(this);
            this.multiple = Pair.of(multiple, multiple2);
        }

        public boolean noAudio()
        {
            return this == Z0 || this == X0;
        }

        public boolean isX9()
        {
            return this == X9;
        }

        public MutableComponent getDisplayName()
        {
            return displayName;
        }

        public RadioCategory getCategory()
        {
            return category;
        }

        @Nullable
        public SoundEvent getSound(LivingEntity entity)
        {
            if (!AVACommonUtil.isFullEquipped(entity))
                return null;
            return AVAWeaponUtil.eitherSound(entity, getEUSound(), getNRFSound());
        }

        public RadioMessage setEuSound(Supplier<SoundEvent> euSound)
        {
            this.euSound = euSound;
            return this;
        }

        public RadioMessage setNrfSound(Supplier<SoundEvent> nrfSound)
        {
            this.nrfSound = nrfSound;
            return this;
        }

        public SoundEvent getEUSound()
        {
            return euSound.get();
        }

        public SoundEvent getNRFSound()
        {
            return nrfSound.get();
        }

        public Pair<Integer, Integer> getMultiple()
        {
            return multiple;
        }
    }

    public static SoundEvent eitherSound(LivingEntity entity, SoundEvent eu, SoundEvent nrf)
    {
        return TeamSide.getSideFor(entity) == TeamSide.EU ? eu : nrf;
    }

    public static List<Player> getPlayersForSide(Player player)
    {
        TeamSide side = TeamSide.getSideFor(player);
        if (side == null)
            return Collections.emptyList();
        return getPlayersForSide(player.level(), side);
    }

    public static List<Player> getPlayersForSide(Level world, TeamSide side)
    {
        return world.players().stream().filter((player) -> TeamSide.getSideFor(player) == side).collect(Collectors.toList());
    }

    public static int getAmmoCount(Player player, ItemStack item, boolean competitive)
    {
        if (competitive)
            return item.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0);
        Item mag = item.getItem() instanceof AVAItemGun ? ((AVAItemGun) item.getItem()).getAmmoType(item) : null;
        int count = 0;
        if (mag == null)
            return count;
        for (int i = 0; i < player.getInventory().getContainerSize(); i++)
        {
            ItemStack stack = player.getInventory().getItem(i);
            if (stack.getItem() == mag)
            {
                if (stack.isDamageableItem())
                {
                    if (stack.getCount() > 1)
                        count += stack.getCount() * stack.getMaxDamage();
                    else count += getStackDurability(stack);
                }
                else count += stack.getCount();
            }
        }
        return count;
    }

    public static int getStackDurability(ItemStack stack)
    {
        return stack.getMaxDamage() - stack.getDamageValue();
    }

    public static boolean isStackDamaged(ItemStack stack)
    {
        return stack.getDamageValue() > 0;
    }

    public static float getAbsorbedDamage(LivingEntity entity, DamageSource source, float damage)
    {
        return damage - CombatRules.getDamageAfterAbsorb(damage, source, entity.getArmorValue(), (float) entity.getAttributeValue(Attributes.ARMOR_TOUGHNESS));
    }

    public static void addAmmoToPlayer(Player player, ItemStack stack, int count)
    {
        if (AVAServerConfig.isCompetitiveModeActivated())
        {
            int capacity = ((AVAItemGun) stack.getItem()).getCapacity(stack, true);
            int amount = Math.min(stack.getOrDefault(AVADataComponents.TAG_ITEM_INNER_CAPACITY, 0) + capacity * count, (AVAWeaponUtil.isPrimaryWeapon(stack.getItem()) ? 250 : 80) + capacity - stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0));
            stack.set(AVADataComponents.TAG_ITEM_INNER_CAPACITY, amount);
        }
        else if (((AVAItemGun) stack.getItem()).getAmmoType(stack) instanceof Ammo)
            ((Ammo) ((AVAItemGun) stack.getItem()).getAmmoType(stack)).addToInventory(player, count, false);
    }

    public static List<Integer> getSlotIndexesFor(Inventory inventory, Item item)
    {
        List<Integer> indexes = new ArrayList<>();
        for (int i = 0; i < inventory.getContainerSize(); i++)
        {
            ItemStack stack = inventory.getItem(i);
            if (stack.getItem() == item)
                indexes.add(i);
        }
        return indexes;
    }

    public static Vec3 getVectorForRotation(float pitch, float yaw)
    {
        float f = pitch * ((float)Math.PI / 180F);
        float f1 = -yaw * ((float)Math.PI / 180F);
        float f2 = Mth.cos(f1);
        float f3 = Mth.sin(f1);
        float f4 = Mth.cos(f);
        float f5 = Mth.sin(f);
        return new Vec3(f3 * f4, -f5, f2 * f4);
    }

    public static boolean canBulletHitBlock(BlockHitResult result, BulletEntity bullet)
    {
        return !NeoForge.EVENT_BUS.post(new AVABulletHitEvent.Block.Pre(result, bullet)).isCanceled();
    }

    public static void onBulletHitBlock(BlockHitResult result, BulletEntity bullet)
    {
        NeoForge.EVENT_BUS.post(new AVABulletHitEvent.Block.Post(result, bullet));
    }

    public static boolean canBulletHitEntity(EntityHitResult result, BulletEntity bullet)
    {
        return !NeoForge.EVENT_BUS.post(new AVABulletHitEvent.Entity.Pre(result, bullet)).isCanceled();
    }

    public static void onBulletHitEntity(EntityHitResult result, BulletEntity bullet)
    {
        NeoForge.EVENT_BUS.post(new AVABulletHitEvent.Entity.Post(result, bullet));
    }

    public static float getEntityHardness(EntityHitResult result, BulletEntity bullet, Entity entity)
    {
        AVAHardnessEvent.Entity event = new AVAHardnessEvent.Entity(result, bullet, AVAConstants.getEntityHardness(entity), entity);
        NeoForge.EVENT_BUS.post(event);
        return event.getValue();
    }

    public static float getBlockHardness(BlockHitResult result, BulletEntity bullet, BlockState state)
    {
        AVAHardnessEvent.Block event = new AVAHardnessEvent.Block(result, bullet, AVAConstants.getBlockHardness(bullet.level(), result.getBlockPos(), state), state);
        NeoForge.EVENT_BUS.post(event);
        return event.getValue();
    }

    public static Optional<Pair<Double, Double>> calculateRecoilRotations(Player player, PlayerAction capability, ItemStack stack)
    {
        float recoil = capability.getRecoil();
        float spread = capability.getSpread();
        float shake = capability.getShake();
//        RecoilRefundTypeCommand.RefundType refundType = WorldData.getCap(player.level()).getRecoilRefundType();
        RecoilRefundTypeCommand.RefundType refundType = AVACrossWorldData.getInstance().recoilRefundType;
        boolean isGun = stack.getItem() instanceof AVAItemGun;
        final float decr = 0.2F;
        float amount;
        boolean isLinear = refundType == RecoilRefundTypeCommand.RefundType.LINEAR;
        double p = 0.0D;
        double y = 0.0D;
        if (recoil != 0)
        {
            if (isLinear)
                amount = Math.min(recoil, isGun ? getGun(player).getRecoilCompensation(player, stack, true) : (decr + 0.225F));
            else
                amount = Math.min(recoil, Math.max(0.1F, recoil / 19.0F + 0.075F));
            capability.setRecoil(recoil - amount);
            if (refundType != RecoilRefundTypeCommand.RefundType.NONE) p = amount;
        }
        if (shake != 0)
        {
            if (isLinear)
            {
                //todo recoil compensation
                float stability = (isGun ? ((AVAItemGun) stack.getItem()).getStability(stack, null, true) : decr);
                if (shake > 0)
                    amount = Math.min(shake, stability + 2.5F);
                else
                    amount = Math.max(shake, -stability - 2.5F);
            }
            else
            {
                if (shake > 0)
                    amount = Math.min(shake, Math.max(0.075F, shake / 21.0F + 0.075F));
                else
                    amount = Math.max(shake, -Math.max(0.075F, shake / 21.0F + 0.075F));
            }
            capability.setShake(shake - amount);
            if (refundType != RecoilRefundTypeCommand.RefundType.NONE) y = -amount;
        }
        boolean notFiring = !isGun || (player.level().isClientSide() ? GunStatusManager.INSTANCE.idle : stack.getOrDefault(AVADataComponents.TAG_ITEM_IDLE, 0)) <= 0;
        if (spread >= 0.1F || notFiring)
        {
            float recovery = isGun ? getGun(player).getSpreadRecovery(stack, true) : decr;
            amount = spread - Math.min(spread, (notFiring ? (spread / 5.0F + recovery) : recovery * 0.25F) * (isGun ? getGun(player).getSpreadRecoveryFactor(stack, true) : 1.0F));
            capability.setSpread(amount);
        }
        return p != 0.0D || y != 0.0D ? Optional.of(Pair.of(y, p)) : Optional.empty();
    }

    public static boolean isInHorizontalArea(double x, double z, BlockPos from, BlockPos to)
    {
        return AVACommonUtil.between(x, from.getX(), to.getX()) && AVACommonUtil.between(z, from.getZ(), to.getZ());
    }

    public static boolean isInArea(Vec3 vec, BlockPos from, BlockPos to)
    {
        return AVACommonUtil.between(vec.x, from.getX(), to.getX()) && AVACommonUtil.between(vec.y, from.getY(), to.getY()) && AVACommonUtil.between(vec.z, from.getZ(), to.getZ());
    }

    public static void trackScoreboard(ServerLevel world, String name)
    {
        trackScoreboard(world.getServer(), name);
    }

    public static void trackScoreboard(MinecraftServer server, String name)
    {
        Objective obj = server.getScoreboard().getObjective(name);
        if (obj != null && !server.getScoreboard().trackedObjectives.contains(obj))
            server.getScoreboard().startTrackingObjective(obj);
    }

    public static void untrackScoreboard(ServerLevel world, String name)
    {
        untrackScoreboard(world.getServer(), name);
    }

    public static void untrackScoreboard(MinecraftServer server, String name)
    {
        if (server.getScoreboard().getObjective(name) != null)
            server.getScoreboard().stopTrackingObjective(server.getScoreboard().getObjective(name));
    }

    public static void removeRepairCost(ItemStack stack)
    {
        stack.remove(DataComponents.REPAIR_COST);
    }

    public static boolean isPlayerAirborneStrict(Player player, int tolerance)
    {
        return player.getPersistentData().getInt(TAG_ENTITY_AIRBORNE) >= tolerance;
    }

    public static <T> T notNullOr(T toCheck, T orElse)
    {
        return Optional.ofNullable(toCheck).orElse(orElse);
    }

    public static void forEachTeamMember(PlayerList list, Team team, Consumer<ServerPlayer> action)
    {
        forEachTeamMember(list, team, (p) -> {
            if (p != null)
                action.accept(p);
            return false;
        });
    }

    public static boolean forEachTeamMember(PlayerList list, Team team, Function<ServerPlayer, Boolean> action)
    {
        if (team == null)
            return false;
        for (String player : team.getPlayers())
        {
            ServerPlayer p = list.getPlayerByName(player);
            if (action.apply(p))
                return true;
        }
        return false;
    }

    public static boolean isInventoryArmourIndex(Inventory inventory, int index)
    {
        return index >= inventory.items.size() && index - inventory.items.size() < inventory.armor.size();
    }
}