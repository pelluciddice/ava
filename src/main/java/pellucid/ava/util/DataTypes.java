package pellucid.ava.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import org.apache.logging.log4j.util.TriConsumer;
import pellucid.ava.AVA;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/***
 * Version 1.20.2-2.6.21: 23/11/2023
 * Author: Dice
 */
public class DataTypes<T>
{
    public static final DataTypes<Boolean> BOOLEAN = new DataTypes<>(0, FriendlyByteBuf::writeBoolean, FriendlyByteBuf::readBoolean, CompoundTag::putBoolean, CompoundTag::getBoolean, JsonObject::addProperty, JsonElement::getAsBoolean);
    public static final DataTypes<Double> DOUBLE = new DataTypes<>(1, FriendlyByteBuf::writeDouble, FriendlyByteBuf::readDouble, CompoundTag::putDouble, CompoundTag::getDouble, JsonObject::addProperty, JsonElement::getAsDouble);
    public static final DataTypes<Float> FLOAT = new DataTypes<>(2, FriendlyByteBuf::writeFloat, FriendlyByteBuf::readFloat, CompoundTag::putFloat, CompoundTag::getFloat, JsonObject::addProperty, JsonElement::getAsFloat);
    public static final DataTypes<Integer> INT = new DataTypes<>(3, FriendlyByteBuf::writeInt, FriendlyByteBuf::readInt, CompoundTag::putInt, CompoundTag::getInt, JsonObject::addProperty, JsonElement::getAsInt);
    public static final DataTypes<Long> LONG = new DataTypes<>(4, FriendlyByteBuf::writeLong, FriendlyByteBuf::readLong, CompoundTag::putLong, CompoundTag::getLong, JsonObject::addProperty, JsonElement::getAsLong);
    public static final DataTypes<String> STRING = new DataTypes<>(5, FriendlyByteBuf::writeUtf, FriendlyByteBuf::readUtf, CompoundTag::putString, CompoundTag::getString, JsonObject::addProperty, JsonElement::getAsString);
    public static final DataTypes<java.util.UUID> UUID = new DataTypes<>(6, (k, v) -> k.writeUUID(v), (k) -> k.readUUID(), CompoundTag::putUUID, CompoundTag::getUUID, java.util.UUID::toString, java.util.UUID::fromString);
    public static final DataTypes<Item> ITEM = new DataTypes<>(7, (v) -> BuiltInRegistries.ITEM.getKey(v).toString(), (k) -> BuiltInRegistries.ITEM.get(new ResourceLocation(k)));
    public static final DataTypes<Supplier<Item>> ITEM_HOLDER = new DataTypes<>(8, (v) -> BuiltInRegistries.ITEM.getKey(v.get()).toString(), (k) -> () -> BuiltInRegistries.ITEM.get(new ResourceLocation(k)));
    public static final DataTypes<ItemStack> ITEMSTACK = new DataTypes<>(9, (k, v) -> ItemStack.OPTIONAL_STREAM_CODEC.encode((RegistryFriendlyByteBuf) k, v), (k) -> ItemStack.OPTIONAL_STREAM_CODEC.decode((RegistryFriendlyByteBuf) k), null, null, null, (Function<JsonElement, ItemStack>) null);
    public static final DataTypes<CompoundTag> COMPOUND = new DataTypes<>(10,
            (k, v) -> k.writeNbt(v),
            (k) -> k.readNbt(),
            CompoundTag::put,
            CompoundTag::getCompound,
            (json, key, tag) -> CompoundTag.CODEC.encodeStart(JsonOps.INSTANCE, tag).resultOrPartial(AVA.LOGGER::error).ifPresent((element) -> json.add(key, element)),
            (json) -> CompoundTag.CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(AVA.LOGGER::error).orElse(new CompoundTag()));
    public static final DataTypes<BlockPos> BLOCKPOS = new DataTypes<>(11,
            (k, v) -> k.writeBlockPos(v),
            (k) -> k.readBlockPos(),
            (compound, key, pos) -> compound.put(key, NbtUtils.writeBlockPos(pos)),
            (tag, key) -> NbtUtils.readBlockPos(tag, key).orElse(BlockPos.ZERO),
            (json, key, tag) -> BlockPos.CODEC.encodeStart(JsonOps.INSTANCE, tag).resultOrPartial(AVA.LOGGER::error).ifPresent((element) -> json.add(key, element)),
            (json) -> BlockPos.CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(AVA.LOGGER::error).orElse(BlockPos.ZERO));
    public static final DataTypes<Direction> DIRECTION = new DataTypes<>(12,
            FriendlyByteBuf::writeEnum,
            (buf) -> buf.readEnum(Direction.class),
            (compound, key, direction) -> Direction.CODEC.encodeStart(NbtOps.INSTANCE, direction).resultOrPartial(AVA.LOGGER::error).ifPresent((element) -> compound.put(key, element)),
            (tag, key) -> Direction.CODEC.parse(NbtOps.INSTANCE, tag.get(key)).resultOrPartial(AVA.LOGGER::error).orElse(Direction.NORTH),
            (json, key, direction) -> Direction.CODEC.encodeStart(JsonOps.INSTANCE, direction).resultOrPartial(AVA.LOGGER::error).ifPresent((element) -> json.add(key, element)),
            (json) -> Direction.CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(AVA.LOGGER::error).orElse(Direction.NORTH));
    public static final DataTypes<Short> SHORT = new DataTypes<>(13, (buf, v) -> buf.writeShort(v), FriendlyByteBuf::readShort, CompoundTag::putShort, CompoundTag::getShort, JsonObject::addProperty, JsonElement::getAsShort);
    public static final DataTypes<Byte> BYTE = new DataTypes<>(14, FriendlyByteBuf::writeByte, FriendlyByteBuf::readByte, CompoundTag::putByte, CompoundTag::getByte, JsonObject::addProperty, JsonElement::getAsByte);

    private static List<DataTypes<?>> DATA_TYPES;
    private final int index;
    private final BiConsumer<FriendlyByteBuf, T> packetWriter;
    private final Function<FriendlyByteBuf, T> packetReader;
    private final TriConsumer<CompoundTag, String, T> tagWriter;
    private final BiFunction<CompoundTag, String, T> tagReader;
    private final TriConsumer<JsonObject, String, T> jsonWriter;
    private final Function<JsonElement, T> jsonReader;

    public DataTypes(int index, Function<T, String> writer, Function<String, T> reader)
    {
        this(index, (buf, v) -> buf.writeUtf(writer.apply(v)), (buf) -> reader.apply(buf.readUtf()), (tag, key, v) -> tag.putString(key, writer.apply(v)), (tag, key) -> reader.apply(tag.getString(key)), writer, reader);
    }

    public DataTypes(int index, BiConsumer<FriendlyByteBuf, T> packetWriter, Function<FriendlyByteBuf, T> packetReader, TriConsumer<CompoundTag, String, T> tagWriter, BiFunction<CompoundTag, String, T> tagReader, Function<T, String> jsonWriter, Function<String, T> jsonReader)
    {
        this(index, packetWriter, packetReader, tagWriter, tagReader, (json, k, v) -> json.addProperty(k, jsonWriter.apply(v)), (json) -> jsonReader.apply(json.getAsString()));
    }

    public DataTypes(int index, BiConsumer<FriendlyByteBuf, T> packetWriter, Function<FriendlyByteBuf, T> packetReader, TriConsumer<CompoundTag, String, T> tagWriter, BiFunction<CompoundTag, String, T> tagReader, TriConsumer<JsonObject, String, T> jsonWriter, Function<JsonElement, T> jsonReader)
    {
        this.index = index;
        this.packetWriter = packetWriter;
        this.packetReader = packetReader;
        this.tagWriter = tagWriter;
        this.tagReader = tagReader;
        this.jsonWriter = jsonWriter;
        this.jsonReader = jsonReader;
        if (DATA_TYPES == null)
            DATA_TYPES = new ArrayList<>();
        DATA_TYPES.add(this);
    }

    public void accessTag(CompoundTag compound, String key, Function<T, T> action)
    {
        if (compound.contains(key))
        {
            T type = read(compound, key);
            write(compound, key, action.apply(type));
        }
    }

    public static DataTypes<?> fromIndex(int index)
    {
        return DATA_TYPES.get(index);
    }

    public int getIndex()
    {
        return index;
    }

    public void writeUnsafe(CompoundTag tag, String key, Object t)
    {
        write(tag, key, (T) t);
    }
    public void writeUnsafe(FriendlyByteBuf buf, Object t)
    {
        write(buf, (T) t);
    }
    public void writeUnsafe(JsonObject json, String key, Object t)
    {
        write(json, key, (T) t);
    }
    public void write(CompoundTag tag, String key, T t)
    {
        tagWriter.accept(tag, key, t);
    }
    public void write(FriendlyByteBuf buf, T t)
    {
        packetWriter.accept(buf, t);
    }
    public void write(JsonObject json, String key, T t)
    {
        jsonWriter.accept(json, key, t);
    }

    public T read(CompoundTag tag, String key)
    {
        return tagReader.apply(tag, key);
    }
    public T read(FriendlyByteBuf buf)
    {
        return packetReader.apply(buf);
    }
    public T read(JsonElement json)
    {
        return jsonReader.apply(json);
    }
    public T read(JsonObject obj, String name)
    {
        return jsonReader.apply(obj.get(name));
    }
    public T readSafe(CompoundTag tag, String key, T defaultValue)
    {
        try
        {
            return tag.contains(key) ? read(tag, key) : defaultValue;
        }
        catch (Exception e)
        {
            return defaultValue;
        }
    }
    public T readSafe(FriendlyByteBuf buf, T defaultValue)
    {
        try
        {
            return read(buf);
        }
        catch (Exception e)
        {
            return defaultValue;
        }
    }
    public T readSafe(JsonElement json, T defaultValue)
    {
        try
        {
            return read(json);
        }
        catch (Exception e)
        {
            return defaultValue;
        }
    }
}
