package pellucid.ava.util;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.items.IItemHandler;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredItem;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import org.apache.commons.lang3.StringUtils;
import org.joml.Vector3f;
import pellucid.ava.AVA;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.items.armours.AVAArmours;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.MeleeWeapons;
import pellucid.ava.items.init.MiscItems;
import pellucid.ava.items.init.Pistols;
import pellucid.ava.items.init.Projectiles;
import pellucid.ava.items.init.Rifles;
import pellucid.ava.items.init.Snipers;
import pellucid.ava.items.init.SpecialWeapons;
import pellucid.ava.items.init.SubmachineGuns;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.sounds.AVASounds;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class AVACommonUtil
{
    public static boolean absSimilar(double a, double b)
    {
        return Math.abs(a) - Math.abs(b) < 0.00001D;
    }

    public static <T extends Number> boolean absSimilar(T v, T v2)
    {
        return absSimilar(v.doubleValue(), v2.doubleValue());
    }

    public static boolean similar(double a, double b)
    {
        if (a == b)
            return true;
        return Math.abs(Math.abs(a) > Math.abs(b) ? a - b : b - a) < 0.00001D;
    }

    public static <T extends Number> boolean similar(T v, T v2)
    {
        return similar(v.doubleValue(), v2.doubleValue());
    }

    public static ResourceLocation modLoc(String path)
    {
        return new ResourceLocation(AVA.MODID, path);
    }

    public static ItemStack getHeldStack(LivingEntity entity)
    {
        return entity.getMainHandItem();
    }

    public static AVAItemGun getGun(LivingEntity entity)
    {
        return (AVAItemGun) getHeldStack(entity).getItem();
    }

    public static boolean isAvailable(Player player)
    {
        return (player != null && player.isAlive() && getHeldStack(player).getItem() instanceof AVAItemGun) && !player.isSpectator();
    }

    public static PlayerAction cap(Player player)
    {
        return PlayerAction.getCap(player);
    }

    public static boolean isFullEquipped(LivingEntity entity)
    {
        Item item = entity.getItemBySlot(EquipmentSlot.HEAD).getItem();
        Item item2 = entity.getItemBySlot(EquipmentSlot.CHEST).getItem();
        Item item3 = entity.getItemBySlot(EquipmentSlot.LEGS).getItem();
        Item item4 = entity.getItemBySlot(EquipmentSlot.FEET).getItem();
        return item instanceof AVAArmours && item2 instanceof AVAArmours && item3 instanceof AVAArmours && item4 instanceof AVAArmours;
    }

    public static boolean isFullEquippedSide(LivingEntity entity, AVAWeaponUtil.TeamSide side)
    {
        Item item = entity.getItemBySlot(EquipmentSlot.HEAD).getItem();
        Item item2 = entity.getItemBySlot(EquipmentSlot.CHEST).getItem();
        Item item3 = entity.getItemBySlot(EquipmentSlot.LEGS).getItem();
        Item item4 = entity.getItemBySlot(EquipmentSlot.FEET).getItem();
        return side.isSideArmour(item) && side.isSideArmour(item2) && side.isSideArmour(item3) && side.isSideArmour(item4);
    }

    public static boolean isMovingOnGroundServer(Player player)
    {
        CompoundTag compound = player.getPersistentData();
        double x = Math.abs(DataTypes.DOUBLE.read(compound, "lastposx") - player.getX());
        double y = Math.abs(DataTypes.DOUBLE.read(compound, "lastposy") - player.getY());
        double z = Math.abs(DataTypes.DOUBLE.read(compound, "lastposz") - player.getZ());
        return (x > 0.02F || y > 0.02F || z > 0.02F) && !AVAWeaponUtil.isPlayerAirborneStrict(player, 2) && !player.isPassenger();
    }

    public static boolean isMovingOnGroundClient(Player player)
    {
        return (isMovingClient(player) && AVAWeaponUtil.isOnGroundNotStrict(player));
    }

    public static boolean isMovingClient(Player player)
    {
        return (player.getX() != player.xOld || player.getZ() != player.zOld) && !player.isSwimming() && !player.isInWater();
    }

    public static boolean isAVADamageSource(DamageSource source)
    {
        return AVADamageSources.isAVADamageSource(source);
    }

    public static boolean hasHealthBoostModifier(Player player)
    {
        return player.getAttributes().hasModifier(Attributes.MAX_HEALTH, AVAConstants.HEALTH_BOOST_MODIFIER_ID);
    }

    public static AttributeModifier getHealthBoostModifier(Player player)
    {
        AttributeInstance attribute = player.getAttribute(Attributes.MAX_HEALTH);
        return attribute == null ? null : attribute.getModifier(AVAConstants.HEALTH_BOOST_MODIFIER_ID);
    }

    public static void removeHealthBoostModifier(Player player)
    {
        Objects.requireNonNull(player.getAttribute(Attributes.MAX_HEALTH)).removeModifier(AVAConstants.HEALTH_BOOST_MODIFIER_ID);
    }

    public static void applyHealthBoostModifier(Player player, AttributeModifier modifier)
    {
        player.getAttribute(Attributes.MAX_HEALTH).addPermanentModifier(modifier);
        player.setHealth((float) (player.getHealth() + modifier.amount()));
    }

    public static AttributeModifier createHealthBoostModifier(int amount)
    {
        return new AttributeModifier(AVAConstants.HEALTH_BOOST_MODIFIER_ID, "health_boost", amount, AttributeModifier.Operation.ADD_VALUE);
    }

    public static String toDisplayString(String toConvert)
    {
        String name = toConvert.replaceAll("_", " ");
        StringBuilder builder = new StringBuilder(name.length());
        String[] s = name.split(" ");
        for (String n : s)
        {
            n = StringUtils.capitalize(n.toLowerCase(Locale.ROOT));
            if (builder.length() != 0)
                builder.append(" ");
            builder.append(n);
        }
        return builder.toString();
    }

    public static double roundEntityDistance(Entity from, Entity to)
    {
        return round(from.position().distanceTo(to.position()), 3);
    }

    public static <T extends Number> float round(T num, int dp)
    {
        return round(num.floatValue(), dp);
    }

    public static float round(float num, int dp)
    {
        return (float) round((double) num, dp);
    }

    public static double round(double num, int dp)
    {
        return roundAndMin(num, dp, Integer.MIN_VALUE);
    }

    public static double roundAndMin(double num, int dp, int min)
    {
        double acc = Math.pow(10, dp);
        return Math.max(min, Math.round(num * acc) / acc);
    }

    public static BlockPos vecToPos(Vec3 vec)
    {
        return BlockPos.containing(vec.x, vec.y, vec.z);
    }

    public static Vec3 posToVec(BlockPos pos)
    {
        return new Vec3(pos.getX(), pos.getY(), pos.getZ());
    }

    @Nullable
    public static SoundEvent getStepSoundFor(Entity entity, boolean playRainStep)
    {
        SoundEvent sound = entity.level().getBlockState(new BlockPos(Mth.floor(entity.getX()), Mth.floor(entity.getY() - 0.2F), Mth.floor(entity.getZ()))).getSoundType().getStepSound();
        if (sound == SoundEvents.WOOD_STEP)
            sound = AVASounds.FOOTSTEP_WOOD.get();
        else if (sound == SoundEvents.METAL_STEP || sound == SoundEvents.LANTERN_STEP)
            sound = AVASounds.FOOTSTEP_METAL.get();
        else if (sound == SoundEvents.SAND_STEP || sound == SoundEvents.GRAVEL_STEP)
            sound = AVASounds.FOOTSTEP_SAND.get();
        else if (sound == SoundEvents.STONE_STEP)
            sound = AVASounds.FOOTSTEP_SOLID.get();
        else if (sound == SoundEvents.GRASS_STEP)
            sound = AVASounds.FOOTSTEP_GRASS.get();
        else if (sound == SoundEvents.WET_GRASS_STEP)
            sound = AVASounds.FOOTSTEP_FLOOD.get();
        else if (sound == SoundEvents.WOOL_STEP)
            sound = AVASounds.FOOTSTEP_WOOL.get();
        else
            sound = null;
        if (sound != null)
        {
            if (entity.level().isRainingAt(entity.blockPosition()) && playRainStep)
                entity.playSound(AVASounds.FOOTSTEP_FLOOD.get(), 1.0F, 1.0F);
        }
        return sound;
    }

    public static int getColourForName(LivingEntity target, LivingEntity local)
    {
//        return self(target, local) ? AVAConstants.AVA_NEUTRAL_COLOUR : AVAWeaponUtil.isSameSide(target, local) ? AVAConstants.AVA_FRIENDLY_COLOUR : AVAConstants.AVA_HOSTILE_COLOUR;
        if (self(target, local))
            return AVAConstants.AVA_NEUTRAL_COLOUR;
        if (local.isSpectator() || (local instanceof Player && ((Player) local).isCreative()))
        {
            AVAWeaponUtil.TeamSide side = AVAWeaponUtil.TeamSide.getSideFor(target);
            if (side != null)
                return side.getColour();
            return AVAConstants.AVA_NEUTRAL_COLOUR;
        }
        return AVAWeaponUtil.isSameSide(target, local) ? AVAConstants.AVA_HUD_TEXT_WHITE : AVAConstants.AVA_HUD_TEXT_RED;
    }

    public static boolean self(LivingEntity living, LivingEntity player)
    {
        return living == player || living.getUUID() == player.getUUID();
    }

    public static <T> int findFirstIndex(List<T> list, Predicate<T> target)
    {
        for (int i = 0; i < list.size(); i++)
            if (target.test(list.get(i)))
                return i;
        return -1;
    }

    public static float toRad(float deg)
    {
        return (float) (deg / 360.0F * Math.PI * 2.0F);
    }

    public static <T, V> Map<T, V> removeIf(Map<T, V> map, BiPredicate<T, V> condition)
    {
        List<T> toRemove = new ArrayList<>();
        map.forEach((t, v) -> {
            if (condition.test(t, v))
                toRemove.add(t);
        });
        for (T t : toRemove)
            map.remove(t);
        return map;
    }

    public static <C extends List<T>, T> C remove(C list, int index)
    {
        list.remove(index);
        return list;
    }

    public static <K> void decreaseAndRemove(Map<K, Integer> map)
    {
        List<K> toRemove = new ArrayList<>();
        HashMap<K, Integer> toAdd = new HashMap<>();
        map.forEach((key, ticks) -> {
            int next = ticks - 1;
            if (next <= 0)
                toRemove.add(key);
            else
                toAdd.put(key, next);
        });
        toRemove.forEach(map.keySet()::remove);
        map.putAll(toAdd);
    }

    public static <K, V> boolean containsKeys(Map<K, V> map, K... keys)
    {
        for (K k : keys)
            if (!map.containsKey(k))
                return false;
        return true;
    }


    public static String toDisplayStringForEnum(Enum<?> member)
    {
        String[] displayString = member.name().toLowerCase(Locale.ROOT).split("_");
        for (int i = 1; i < displayString.length; i++)
            displayString[i] = StringUtils.capitalize(displayString[i]);
        StringBuilder text = new StringBuilder();
        for (String s : displayString)
            text.append(s);
        return text.toString();
    }

    public static void broadcastSystemMessage(Component text)
    {
        MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
        if (server != null)
            server.getPlayerList().broadcastSystemMessage(text, false);
    }

    private static Map<Integer, List<Item>> ITEM_MAP;
    public static Map<Integer, List<Item>> getCraftableMap()
    {
        if (ITEM_MAP == null || ITEM_MAP.isEmpty())
        {
            Predicate<Item> con = (item) -> item instanceof Ammo || !(item instanceof IHasRecipe) || (item instanceof AVAItemGun && !((AVAItemGun) item).isMaster()) || ((IHasRecipe) item).getRecipe() == null;
            ITEM_MAP = new HashMap<>()
            {{
                put(0, removeIf(new ArrayList<>(Snipers.ITEM_SNIPERS), con));
                put(1, removeIf(new ArrayList<>(Rifles.ITEM_RIFLES), con));
                put(2, removeIf(new ArrayList<>(SubmachineGuns.ITEM_SUBMACHINE_GUNS), con));
                put(3, removeIf(new ArrayList<>(Pistols.ITEM_PISTOLS), con));
                put(4, removeIf(combine(MiscItems.ITEM_MISCS, Projectiles.ITEM_PROJECTILES, MeleeWeapons.ITEM_MELEE_WEAPONS, SpecialWeapons.ITEM_SPECIAL_WEAPONS), con));
                put(5, new ArrayList<>(Ammo.AMMO));
            }};
        }
        return ITEM_MAP;
    }

    private static List<? extends Item> PAINTABLES_LIST;
    public static List<? extends Item> getPaintables()
    {
        if (PAINTABLES_LIST == null || PAINTABLES_LIST.isEmpty())
            PAINTABLES_LIST = removeIf(AVAWeaponUtil.getAllGuns(), (item) -> !((AVAItemGun) item).isMaster() || ((AVAItemGun) item).getSubGuns().size() == 1);
        return PAINTABLES_LIST;
    }

    public static <T> List<T> removeIf(List<T> list, Predicate<T> condition)
    {
        list.removeIf(condition);
        return list;
    }

    public static <T, C extends Collection<? extends T>> List<T> combine(C... lists)
    {
        List<T> list = new ArrayList<>();
        for (C collection : lists)
            list.addAll(collection);
        return list;
    }

    public static <T, C extends Collection<T>> List<T> combine(List<T> list, C list2)
    {
        List<T> list3 = new ArrayList<>(list);
        list3.addAll(list2);
        return list3;
    }

    public static <T> List<T> combine(Collection<? extends Collection<T>> list)
    {
        List<T> toAdd = new ArrayList<>();
        for (Collection<T> element : list)
            toAdd.addAll(element);
        return toAdd;
    }

    public static <T, C extends Collection<T>> List<T> mergeNew(List<T> list, C list2)
    {
        return combine(removeIf(list, list2::contains), list2);
    }

    public static <T, C extends Collection<T>> List<T> merge(List<T> list, C list2)
    {
        List<T> temp = mergeNew(list, list2);
        list.clear();
        list.addAll(temp);
        return list;
    }

    public static <T> List<T> cast(List<?> list, Class<T> type)
    {
        return collect(list, type::cast);
    }

    public static <T, V> List<V> collect(Collection<T> list, Function<T, V> function)
    {
        return list.stream().collect(ArrayList::new, (list2, element) -> {
            V e = function.apply(element);
            if (e != null)
                list2.add(e);
        }, ArrayList::addAll);
    }

    public static <T extends Number> T clamp(T value, T min, T max)
    {
        double d = value.doubleValue();
        if (d > max.doubleValue())
            return max;
        if (d < min.doubleValue())
            return min;
        return value;
    }

    public static <T extends Number> T add(T v, T v2, Function<Double, T> caster)
    {
        return caster.apply(v.doubleValue() + v2.doubleValue());
    }

    public static void clearContainer(Player player, IItemHandler storage)
    {
        if (!player.isAlive() || player instanceof ServerPlayer && ((ServerPlayer) player).hasDisconnected())
            for (int j = 0; j < storage.getSlots(); ++j)
                player.drop(storage.extractItem(j, storage.getSlotLimit(j), false), false);
        else
            for (int i = 0; i < storage.getSlots(); ++i)
            {
                Inventory inventory = player.getInventory();
                if (inventory.player instanceof ServerPlayer)
                    inventory.placeItemBackInInventory(storage.extractItem(i, storage.getSlotLimit(i), false));
            }
    }

    public static String getDamageFloatingString(Pair<Float, Float> floating)
    {
        float left = round(floating.getA(), 1);
        float right = round(floating.getA() + floating.getB(), 1);
        return left + "~+" + right;
    }

    public static BlockState getCorrectStateForFluidBlock(Level world, BlockState state, BlockPos pos)
    {
        FluidState fluidstate = state.getFluidState();
        return !fluidstate.isEmpty() && !state.isFaceSturdy(world, pos, Direction.UP) ? fluidstate.createLegacyBlock() : state;
    }

    public static <T> List<T> toList(Iterable<T> iterable)
    {
        List<T> list = new ArrayList<>();
        iterable.forEach(list::add);
        return list;
    }

    public static <L extends Collection<T>, T> T append(L list, T t)
    {
        list.add(t);
        return t;
    }

    public static Pair<Double, Double> flip2D(double x, double y, double maxX, double maxY, Direction direction)
    {
        return flip2D(0, 0, x, y, maxX, maxY, direction);
    }

    public static Pair<Double, Double> flip2D(double oX, double oY, double x, double y, double maxX, double maxY, Direction direction)
    {
        x -= oX;
        y -= oY;
        Pair<Double, Double> pair = Pair.of(x, y);
        switch (direction)
        {
            case SOUTH -> {
                pair.setA(maxX - x);
                pair.setB(maxY - y);
            }
            case EAST -> {
                pair.setA(y);
                pair.setB(maxY - x);
            }
            case WEST -> {
                pair.setA(maxX - y);
                pair.setB(x);
            }
        }
        return pair.map((a) -> a + oX, (b) -> b + oY);
    }

    public static VoxelShape mergeShapes(VoxelShape... shapes)
    {
        return mergeShapes(Arrays.stream(shapes).toList());
    }

    public static VoxelShape mergeShapes(List<VoxelShape> shapes)
    {
        shapes = new ArrayList<>(shapes);
        if (shapes.size() > 1)
        {
            VoxelShape shape = shapes.get(0);
            shapes.remove(0);
            return Shapes.or(shape, shapes.toArray(new VoxelShape[0]));
        }
        return Shapes.empty();
    }

    public static VoxelShape slopeShape(float ySize, float zSize, float yOffset, float zOffset, int accuracy)
    {
        VoxelShape shape = Shapes.empty();
        float ySize2 = ySize / accuracy;
        float zSize2 = zSize / accuracy;
        for (int i = 0; i < accuracy; i++)
            shape = Shapes.or(shape, boxShapeShapes(0, yOffset, zOffset + zSize2 * i, 16, yOffset + ySize2 * i, zOffset + zSize2 * i + zSize2));
        return shape;
    }

    public static VoxelShape boxShapeBlock(double x, double y, double z, double x2, double y2, double z2)
    {
        return Block.box(Math.min(x, x2), Math.min(y, y2), Math.min(z, z2), Math.max(x, x2), Math.max(y, y2), Math.max(z, z2));
    }

    public static VoxelShape boxShapeShapes(double x, double y, double z, double x2, double y2, double z2)
    {
        return Shapes.box(Math.min(x, x2), Math.min(y, y2), Math.min(z, z2), Math.max(x, x2), Math.max(y, y2), Math.max(z, z2));
    }

    public static boolean between(double a, double bound, double bound2)
    {
        double min = Math.min(bound, bound2);
        double max = Math.max(bound, bound2);
        return min <= a && a <= max;
    }

    public static <K, V> void forEachSorted(Map<K, V> map, Comparator<K> comparator, BiConsumer<K, V> action)
    {
        List<K> keys = new ArrayList<>(map.keySet());
        keys.sort(comparator);
        for (K key : keys)
            action.accept(key, map.get(key));
    }

    public static BlockPos minPos(BlockPos pos, BlockPos pos2)
    {
        return alternatePos(pos, pos2, Math::min);
    }

    public static BlockPos maxPos(BlockPos pos, BlockPos pos2)
    {
        return alternatePos(pos, pos2, Math::max);
    }

    public static BlockPos alternatePos(BlockPos pos, BlockPos pos2, BiFunction<Integer, Integer, Integer> function)
    {
        return new BlockPos(function.apply(pos.getX(), pos2.getX()), function.apply(pos.getY(), pos2.getY()), function.apply(pos.getZ(), pos2.getZ()));
    }
    public static boolean isReplaceable(BlockState state)
    {
        return state.isAir() || state.is(BlockTags.REPLACEABLE);
    }

    public static Vector3f lerpVector3f(Vector3f from, Vector3f to, float f)
    {
        return new Vector3f(Mth.lerp(f, from.x(), to.x()), Mth.lerp(f, from.y(), to.y()), Mth.lerp(f, from.z(), to.z()));
    }

    public static Vector3f rotLerpVector3f(Vector3f from, Vector3f to, float f)
    {
        return new Vector3f(Mth.rotLerp(f, from.x(), to.x()), Mth.rotLerp(f, from.y(), to.y()), Mth.rotLerp(f, from.z(), to.z()));
    }

    public static Vec3 lerpVec3(Vec3 from, Vec3 to, float f)
    {
        return new Vec3(Mth.lerp(f, from.x(), to.x()), Mth.lerp(f, from.y(), to.y()), Mth.lerp(f, from.z(), to.z()));
    }

    public static double rotLerpD(double partialTicks, double from, double to)
    {
        return Mth.rotLerp((float) partialTicks, (float) from, (float) to);
    }

    public static float percentageF(float min, float to, float value)
    {
        return (float) percentageD(min, to, value);
    }

    public static double percentageD(double min, double to, double value)
    {
        return (value - min) / (to - min);
    }

    public static CompoundTag emptyTag()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.BOOLEAN.write(compound, AVAConstants.TAG_EMPTY, true);
        return compound;
    }

    public static boolean isEmptyTag(CompoundTag compound)
    {
        return compound.contains(AVAConstants.TAG_EMPTY) && DataTypes.BOOLEAN.read(compound, AVAConstants.TAG_EMPTY);
    }

    public static String fillTenth(String string)
    {
        StringBuilder builder = new StringBuilder(string);
        while (builder.length() < 2)
            builder.insert(0, "0");
        return builder.toString();
    }

    public static void putDirectionalShapes(Map<Direction, VoxelShape> shapes, VoxelShape north)
    {
        List<AABB> aabbs = north.toAabbs();
        Direction.Plane.HORIZONTAL.stream().forEach((direction) -> {
            shapes.put(direction, rotateBlockShape(aabbs, direction));
        });
    }

    public static VoxelShape rotateShape(List<AABB> list, Direction direction)
    {
        return rotateShapeInternal(list, direction, AVACommonUtil::boxShapeShapes, 1);
    }

    public static VoxelShape rotateBlockShape(List<AABB> list, Direction direction)
    {
        return rotateShapeInternal(list, direction, AVACommonUtil::boxShapeBlock, 16);
    }

    private static VoxelShape rotateShapeInternal(List<AABB> list, Direction direction, HexFunction<Double, Double, Double, Double, Double, Double, VoxelShape> shapeConstructor, int size)
    {
        AtomicObject<VoxelShape> shape2 = new AtomicObject<>();
        list.forEach((aabb) -> {
            double x = aabb.minX;
            double z = aabb.minZ;
            double y = aabb.minY;
            double x2 = aabb.maxX;
            double y2 = aabb.maxY;
            double z2 = aabb.maxZ;
            Pair<Double, Double> pair = AVACommonUtil.flip2D(x, z, size, size, direction);
            Pair<Double, Double> pair2 = AVACommonUtil.flip2D(x2, z2, size, size, direction);
            if (direction.getAxis() == Direction.Axis.X)
            {
                pair.map((a) -> size - a, (b) -> size - b);
                pair2.map((a) -> size - a, (b) -> size - b);
            }
            VoxelShape shape3 = shapeConstructor.apply(pair.getA(), y, pair.getB(), pair2.getA(), y2, pair2.getB());
            if (shape2.isEmpty())
                shape2.set(shape3);
            else
                shape2.set(Shapes.or(shape2.get(), shape3));
        });
        return shape2.get();
    }

    public static ResourceLocation getRegistryNameBlock(Supplier<? extends Block> entry)
    {
        return getRegistryName(entry, BuiltInRegistries.BLOCK);
    }

    public static ResourceLocation getRegistryNameBlock(Block entry)
    {
        return getRegistryName(entry, BuiltInRegistries.BLOCK);
    }

    public static ResourceLocation getRegistryNameItem(DeferredItem<Item> entry)
    {
        return getRegistryName(entry, BuiltInRegistries.ITEM);
    }

    public static ResourceLocation getRegistryNameItem(Item entry)
    {
        return getRegistryName(entry, BuiltInRegistries.ITEM);
    }

    public static <C extends T, T> ResourceLocation getRegistryName(Supplier<C> entry, Registry<T> registry)
    {
        return entry instanceof DeferredHolder<?,?> ? ((DeferredHolder<C, ?>) entry).getId() : registry.getKey(entry.get());
    }

    public static <C extends T, T> ResourceLocation getRegistryName(C entry, Registry<T> registry)
    {
        return registry.getKey(entry);
    }

    public static <T> T notNullOr(T toCheck, T orElse)
    {
        return Optional.ofNullable(toCheck).orElse(orElse);
    }

    public static void stacktrace(Object... objects)
    {
        try
        {
            throw new NullPointerException(Arrays.toString(objects));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static <T> T safePresentAndOr(Supplier<T> t, Predicate<T> test, Supplier<T> or)
    {
        try
        {
            T t2 = t.get();
            T t3 = presentOr(t2, or);
            return t3 == t2 && test.test(t3) ? t3 : or.get();
        }
        catch (Exception e)
        {
            return or.get();
        }
    }

    public static <T> T safePresentOr(Supplier<T> t, Supplier<T> or)
    {
        try
        {
            return presentOr(t.get(), or);
        }
        catch (Exception e)
        {
            return or.get();
        }
    }

    public static <T> T presentOr(T t, Supplier<T> supplier)
    {
        return t == null ? supplier.get() : t;
    }

    public static int[][] imageToArray(File file, boolean sizeConstrain) throws IOException
    {
        BufferedImage image = ImageIO.read(file);
        //        if (image.getWidth() * image.getHeight() >= AVAConstants.MAX_IMAGE_SIZE || !sizeConstrain)
        //        {
        int[][] array = new int[image.getWidth()][image.getHeight()];
        for (int x = 0; x < image.getWidth(); x++)
            for (int y = 0; y < image.getHeight(); y++)
                array[x][y] = image.getRGB(x, y);
        return array;
        //        }
        //        return null;
    }

    public static DataTypes getType(ModConfigSpec.ConfigValue field)
    {
        DataTypes type = DataTypes.STRING;
        if (field instanceof ModConfigSpec.IntValue)
            type = DataTypes.INT;
        else if (field instanceof ModConfigSpec.DoubleValue)
            type = DataTypes.DOUBLE;
        else if (field instanceof ModConfigSpec.BooleanValue)
            type = DataTypes.BOOLEAN;
        return type;
    }
}
