package pellucid.ava.util;

public class AtomicObject<T>
{
    private T t = null;

    public AtomicObject()
    {

    }

    public AtomicObject(T t)
    {
        this.t = t;
    }

    public boolean isEmpty()
    {
        return t == null;
    }

    public T get()
    {
        return t;
    }

    public void set(T t)
    {
        this.t = t;
    }
}
