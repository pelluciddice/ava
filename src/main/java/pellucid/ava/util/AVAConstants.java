package pellucid.ava.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.scores.PlayerTeam;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.AVAEntities;
import pellucid.ava.sounds.AVASounds;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;

import static net.minecraft.world.entity.EntityType.*;
import static net.minecraft.world.item.CreativeModeTabs.*;
import static net.minecraft.world.level.block.SoundType.*;

public class AVAConstants
{
    public static final String TAG_ENTITY_AIRBORNE = "airborne";
    public static final String TAG_ENTITY_PARACHUTED = "parachuted";

    public static final String FORGE_MOD_ID = "forge";
    public static final String NULL = "null";

    // Remember to change the model anim time too
    @Deprecated
    public static final int SILENCER_INSTALL_TIME = 20;
    @Deprecated
    public static final int BINOCULAR_AIM_TIME = 2;
    public static final int BINOCULAR_CD = 200;
    @Deprecated
    public static final int PROJECTILE_DRAW_TIME = 9;
    @Deprecated
    public static final int PROJECTILE_PIN_OFF_TIME = 9;
    @Deprecated
    public static final int PROJECTILE_TOSS_TIME = 12;
    @Deprecated
    public static final int PROJECTILE_THROW_TIME = 12;
    @Deprecated
    public static final int C4_SET_TIME = 70;

    @Deprecated
    public static final String OPTIFINE = "optifine";
    public static final String TAG_ENTITY_BLINDED = AVA.MODID + ":blinded";

    public static final String TAG_EMPTY = AVA.MODID + ":empty";

    public static final int RAY_TRACE_ACCURACY = 20;
    public static final int VANILLA_FULL_PACKED_LIGHT = 15728880;

    public static final int AVA_HUD_TEXT_ORANGE = 0xFFFF9700;
    public static final int AVA_HUD_TEXT_YELLOW = 0xFFFDC111;
    public static final int AVA_HUD_TEXT_GRAY = 0xFFCCCCCC;
    public static final int AVA_HUD_TEXT_WHITE = 0xFFFFFFFF;
    public static final int AVA_HUD_TEXT_RED = 0xFFFF704D;

    public static final int AVA_FRIENDLY_COLOUR = 17663;
    public static final int AVA_NEUTRAL_COLOUR = 16764928;
    public static final int AVA_HOSTILE_COLOUR = 16711680;

    public static final Color AVA_COMPETITIVE_UI_YELLOW_BG = new Color(255, 230, 70, 255);
    public static final Color AVA_COMPETITIVE_UI_YELLOW_BG_TRANSPARENT = new Color(255, 230, 70, 25);
    public static final Color AVA_COMPETITIVE_UI_GREEN_BG = new Color(50, 60, 50, (int) (0.4F * 255.0F));
    public static final Color AVA_COMPETITIVE_UI_GREEN_BG_TRANSPARENT = new Color(50, 60, 50, 1);
    public static final Color AVA_COMPETITIVE_UI_GRAY = new Color(70, 70, 70);
    public static final Color AVA_COMPETITIVE_UI_DARK_BLUE = new Color(6, 27, 26);

    public static final float MIN_BULLET_DAMAGE = 0.1F;

    public static final int DEFAULT_FONT_HEIGHT = 9;

    public static final Color AVA_HUD_COLOUR_HOVER_WHITE = new Color(1.0F, 1.0F, 1.0F, 0.225F);
    public static final Color AVA_HUD_COLOUR_WHITE = new Color(AVAConstants.AVA_HUD_TEXT_WHITE);
    public static final Color AVA_HUD_COLOUR_WHITE_TRANSPARENT = new Color(255, 255, 255, 1);
    public static final Color AVA_HUD_COLOUR_RED = new Color(160, 35, 40);
    public static final Color AVA_HUD_COLOUR_BLUE = new Color(55, 122, 189);

    public static final int AVA_HUD_ADS_SHAKE_WHITE = 0xDDDDDDDD;
    public static final int AVA_HUD_ADS_SHAKE_GREY = 0xDD888888;

    public static final Color AVA_HEALTH_BAR_COLOUR_DARK_GRAY = new Color(45, 45, 45);
    public static final Color AVA_HEALTH_BAR_COLOUR_GREEN = new Color(64, 213, 32);

    public static final Color AVA_SCOREBOARD_BG_BLACK = new Color(0.0F, 0.0F, 0.0F, 0.2F);
    public static final Color AVA_SCOREBOARD_TITLE_BLACK = new Color(0.0F, 0.0F, 0.0F, 0.35F);

    public static final Color AVA_GUN_BOOST_GREEN = new Color(0, 220, 50);

    public static final ResourceLocation AVA_JEI_ID = new ResourceLocation("jei:ava");

    public static final MutableComponent SHIFT_MORE_INFO = Component.translatable("ava.item.tips.more_info").withStyle(ChatFormatting.AQUA);
    public static final MutableComponent MAIN_HAND_EMPTY = Component.translatable("ava.chat.main_hand_empty").withStyle(ChatFormatting.RED);
    public static final MutableComponent SPACING = Component.literal("--------------------------").withStyle(ChatFormatting.GRAY);

    public static final RandomSource RAND = RandomSource.create();
    public static final UUID HEALTH_BOOST_MODIFIER_ID = UUID.fromString("da805c8e-28be-40aa-a6d3-7d1df657172f");
    public static final AttributeModifier SLOW_FALLING_MODIFIER = new AttributeModifier(UUID.fromString("994a6485-d5c7-4c4a-ac75-0634a02cc97d"), "Slow falling acceleration reduction", -0.07, AttributeModifier.Operation.ADD_VALUE);
    public static final SimpleCommandExceptionType INVALID_POSITION = new SimpleCommandExceptionType(Component.translatable("commands.summon.invalidPosition"));

    public static final float LEOPARD_WIDTH = 4.5F;
    public static final float LEOPARD_LENGTH = 6.6F;
    public static final float LEOPARD_HEIGHT = 2.65F;

    public static final float DEFAULT_SPREAD_RECOVERY_FACTOR = 1.5F;

    public static final int MAX_IMAGE_SIZE = 210000;

    @Deprecated
    public static final ResourceLocation ATTACHMENT_CAP = new ResourceLocation(AVA.MODID, "attachments");
    public static final ResourceLocation PLAYER_ACTION_CAP = new ResourceLocation(AVA.MODID, "player_action");
    @Deprecated
    public static final ResourceLocation WORLD_DATA_CAP = new ResourceLocation(AVA.MODID, "world_data");
    
    public static final Set<ResourceKey<CreativeModeTab>> VANILLA_TABS = ImmutableSet.of(   
//            BUILDING_BLOCKS,
            COLORED_BLOCKS,
//            NATURAL_BLOCKS,
//            FUNCTIONAL_BLOCKS,
//            REDSTONE_BLOCKS,
//            HOTBAR,
//            SEARCH,
//            TOOLS_AND_UTILITIES,
//            COMBAT,
            FOOD_AND_DRINKS,
            INGREDIENTS,
            SPAWN_EGGS
//            OP_BLOCKS,
//            INVENTORY
    );

    public static final Map<Integer, String> ROME_I_V = ImmutableMap.of(
            1, "I",
            2, "II",
            3, "III",
            4, "IV",
            5, "V"
    );

    private static int STRUCTURE_SEED_ID = 0;
    private static final List<Integer> STRUCTURE_SEEDS = ImmutableList.of(
            2063034179,
            1714209383,
            2032104170,
            536162899,
            1886228221,
            1954288338,
            2077655819,
            113574071,
            1212540747,
            1519387428,
            1445799531,
            1638145405,
            1074465098,
            1552758010,
            167490104
    );

    public static int nextStructureSeed()
    {
        return STRUCTURE_SEEDS.get(STRUCTURE_SEED_ID++);
    }

    private static final List<Block> SEE_THROUGH_BLOCKS = ImmutableList.of(
            Blocks.BARRIER
    );

    private static final List<Block> FAKE_BLOCKS = ImmutableList.of(
            Blocks.BARRIER,
            Blocks.LIGHT
    );

    public static boolean isFakeBLock(Block block)
    {
        return FAKE_BLOCKS.contains(block);
    }

    public static boolean isSeeThroughBlock(Level world, BlockPos pos)
    {
        BlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        if (SEE_THROUGH_BLOCKS.contains(block))
            return true;
        if (state.isAir())
            return true;
        return !state.canOcclude();
    }

    public static List<? extends Supplier<Block>> getClassicPlanks()
    {
        return ImmutableList.of(
                () -> Blocks.OAK_PLANKS,
                () -> Blocks.SPRUCE_PLANKS,
                () -> Blocks.BIRCH_PLANKS,
                () -> Blocks.JUNGLE_PLANKS,
                () -> Blocks.ACACIA_PLANKS,
                () -> Blocks.DARK_OAK_PLANKS,
                () -> Blocks.MANGROVE_PLANKS,
                () -> Blocks.CHERRY_PLANKS,
                () -> Blocks.CRIMSON_PLANKS,
                () -> Blocks.WARPED_PLANKS
        );
    }

    public static List<? extends Supplier<Block>> getClassicWoods()
    {
        return ImmutableList.of(
                () -> Blocks.OAK_WOOD,
                () -> Blocks.SPRUCE_WOOD,
                () -> Blocks.BIRCH_WOOD,
                () -> Blocks.JUNGLE_WOOD,
                () -> Blocks.ACACIA_WOOD,
                () -> Blocks.DARK_OAK_WOOD,
                () -> Blocks.MANGROVE_WOOD,
                () -> Blocks.CHERRY_WOOD,
                () -> Blocks.CRIMSON_HYPHAE,
                () -> Blocks.WARPED_HYPHAE
        );
    }

    public static List<? extends Supplier<Block>> getClassicStrippedWoods()
    {
        return ImmutableList.of(
                () -> Blocks.STRIPPED_OAK_WOOD,
                () -> Blocks.STRIPPED_SPRUCE_WOOD,
                () -> Blocks.STRIPPED_BIRCH_WOOD,
                () -> Blocks.STRIPPED_JUNGLE_WOOD,
                () -> Blocks.STRIPPED_ACACIA_WOOD,
                () -> Blocks.STRIPPED_DARK_OAK_WOOD,
                () -> Blocks.STRIPPED_CHERRY_WOOD,
                () -> Blocks.STRIPPED_CRIMSON_HYPHAE,
                () -> Blocks.STRIPPED_WARPED_HYPHAE
        );
    }

    public static List<? extends Supplier<Block>> getClassicRocks()
    {
        return ImmutableList.of(
                () -> Blocks.STONE,
                () -> Blocks.COBBLESTONE,
                () -> Blocks.MOSSY_COBBLESTONE,
                () -> Blocks.GRANITE,
                () -> Blocks.DIORITE,
                () -> Blocks.ANDESITE,
                () -> Blocks.SANDSTONE,
                () -> Blocks.RED_SANDSTONE,
                () -> Blocks.QUARTZ_BLOCK

        );
    }

    public static List<? extends Supplier<Block>> getClassicSmoothRocks()
    {
        return ImmutableList.of(
                () -> Blocks.SMOOTH_STONE,
                () -> Blocks.POLISHED_GRANITE,
                () -> Blocks.POLISHED_DIORITE,
                () -> Blocks.POLISHED_ANDESITE,
                () -> Blocks.SMOOTH_SANDSTONE,
                () -> Blocks.SMOOTH_RED_SANDSTONE,
                () -> Blocks.SMOOTH_QUARTZ,
                () -> Blocks.POLISHED_DEEPSLATE
        );
    }

    public static List<? extends Supplier<Block>> getClassicBricks()
    {
        return ImmutableList.of(
                () -> Blocks.STONE_BRICKS,
                () -> Blocks.BRICKS,
                () -> Blocks.CHISELED_STONE_BRICKS,
                () -> Blocks.MOSSY_STONE_BRICKS,
                () -> Blocks.DEEPSLATE_BRICKS,
                () -> Blocks.MUD_BRICKS
        );
    }

    public static List<? extends Supplier<Block>> getClassicWools()
    {
        return ImmutableList.of(
                () -> Blocks.BLACK_WOOL,
                () -> Blocks.CYAN_WOOL,
                () -> Blocks.BLUE_WOOL,
                () -> Blocks.LIGHT_BLUE_WOOL,
                () -> Blocks.BROWN_WOOL,
                () -> Blocks.GRAY_WOOL,
                () -> Blocks.LIGHT_GRAY_WOOL,
                () -> Blocks.GREEN_WOOL,
                () -> Blocks.LIME_WOOL,
                () -> Blocks.MAGENTA_WOOL,
                () -> Blocks.ORANGE_WOOL,
                () -> Blocks.PINK_WOOL,
                () -> Blocks.PURPLE_WOOL,
                () -> Blocks.RED_WOOL,
                () -> Blocks.WHITE_WOOL,
                () -> Blocks.YELLOW_WOOL
        );
    }

    public static List<? extends Supplier<Block>> getClassicConcretes()
    {
        return ImmutableList.of(
                () -> Blocks.BLACK_CONCRETE,
                () -> Blocks.CYAN_CONCRETE,
                () -> Blocks.BLUE_CONCRETE,
                () -> Blocks.LIGHT_BLUE_CONCRETE,
                () -> Blocks.BROWN_CONCRETE,
                () -> Blocks.GRAY_CONCRETE,
                () -> Blocks.LIGHT_GRAY_CONCRETE,
                () -> Blocks.GREEN_CONCRETE,
                () -> Blocks.LIME_CONCRETE,
                () -> Blocks.MAGENTA_CONCRETE,
                () -> Blocks.ORANGE_CONCRETE,
                () -> Blocks.PINK_CONCRETE,
                () -> Blocks.PURPLE_CONCRETE,
                () -> Blocks.RED_CONCRETE,
                () -> Blocks.WHITE_CONCRETE,
                () -> Blocks.YELLOW_CONCRETE
        );
    }

    public static List<? extends Supplier<Block>> getClassicTerracottas()
    {
        return ImmutableList.of(
                () -> Blocks.BLACK_TERRACOTTA,
                () -> Blocks.CYAN_TERRACOTTA,
                () -> Blocks.BLUE_TERRACOTTA,
                () -> Blocks.LIGHT_BLUE_TERRACOTTA,
                () -> Blocks.BROWN_TERRACOTTA,
                () -> Blocks.GRAY_TERRACOTTA,
                () -> Blocks.LIGHT_GRAY_TERRACOTTA,
                () -> Blocks.GREEN_TERRACOTTA,
                () -> Blocks.LIME_TERRACOTTA,
                () -> Blocks.MAGENTA_TERRACOTTA,
                () -> Blocks.ORANGE_TERRACOTTA,
                () -> Blocks.PINK_TERRACOTTA,
                () -> Blocks.PURPLE_TERRACOTTA,
                () -> Blocks.RED_TERRACOTTA,
                () -> Blocks.WHITE_TERRACOTTA,
                () -> Blocks.YELLOW_TERRACOTTA
        );
    }

    public static List<? extends Supplier<Block>> getGlazedTerracottas()
    {
        return ImmutableList.of(
                () -> Blocks.BLACK_GLAZED_TERRACOTTA,
                () -> Blocks.CYAN_GLAZED_TERRACOTTA,
                () -> Blocks.BLUE_GLAZED_TERRACOTTA,
                () -> Blocks.LIGHT_BLUE_GLAZED_TERRACOTTA,
                () -> Blocks.BROWN_GLAZED_TERRACOTTA,
                () -> Blocks.GRAY_GLAZED_TERRACOTTA,
                () -> Blocks.LIGHT_GRAY_GLAZED_TERRACOTTA,
                () -> Blocks.GREEN_GLAZED_TERRACOTTA,
                () -> Blocks.LIME_GLAZED_TERRACOTTA,
                () -> Blocks.MAGENTA_GLAZED_TERRACOTTA,
                () -> Blocks.ORANGE_GLAZED_TERRACOTTA,
                () -> Blocks.PINK_GLAZED_TERRACOTTA,
                () -> Blocks.PURPLE_GLAZED_TERRACOTTA,
                () -> Blocks.RED_GLAZED_TERRACOTTA,
                () -> Blocks.WHITE_GLAZED_TERRACOTTA,
                () -> Blocks.YELLOW_GLAZED_TERRACOTTA
        );
    }

    public static List<? extends Supplier<Block>> getClassicGemBlocks()
    {
        return ImmutableList.of(
                () -> Blocks.IRON_BLOCK,
                () -> Blocks.GOLD_BLOCK,
                () -> Blocks.DIAMOND_BLOCK,
                () -> Blocks.EMERALD_BLOCK,
                () -> Blocks.REDSTONE_BLOCK,
                () -> Blocks.LAPIS_BLOCK,
                () -> Blocks.AMETHYST_BLOCK
        );
    }

    public static List<? extends Supplier<Block>> getClassicCopperBlocks()
    {
        return ImmutableList.of(
                () -> Blocks.COPPER_BLOCK,
                () -> Blocks.EXPOSED_COPPER,
                () -> Blocks.WEATHERED_COPPER,
                () -> Blocks.OXIDIZED_COPPER,
                () -> Blocks.CUT_COPPER,
                () -> Blocks.EXPOSED_CUT_COPPER,
                () -> Blocks.WEATHERED_CUT_COPPER,
                () -> Blocks.OXIDIZED_CUT_COPPER
        );
    }


    public static List<? extends Supplier<Block>> getClassicBambooBlocks()
    {
        return ImmutableList.of(
                () -> Blocks.BAMBOO,
                () -> Blocks.BAMBOO_PLANKS,
                () -> Blocks.BAMBOO_MOSAIC
        );
    }

    public static List<? extends Supplier<Block>> getAVASolidBlocks()
    {
        return AVACommonUtil.combine(ImmutableList.of(
                AVABuildingBlocks.COBBLED_SANDSTONE_TILE
        ), AVABuildingBlocks.PLASTER_BLOCKS, AVABuildingBlocks.PLASTER_BLOCKS_2, AVABuildingBlocks.CONCRETE_BLOCKS, AVABuildingBlocks.CONCRETE_POWDER_BLOCKS, AVABuildingBlocks.WOOL_BLOCKS);
    }

    public static List<? extends Supplier<Block>> getAllCommonBlocks()
    {
        return AVACommonUtil.combine(getClassicPlanks(), getClassicWoods(), getClassicStrippedWoods(), getClassicRocks(), getClassicSmoothRocks(), getClassicWools(), getClassicConcretes(), getClassicTerracottas(), getGlazedTerracottas(), getClassicGemBlocks(), getClassicCopperBlocks(), getClassicBricks(), getClassicBambooBlocks(), getAVASolidBlocks());
    }

    public static List<? extends Supplier<Block>> getAllWallLightMaterials()
    {
        return AVACommonUtil.combine(getClassicRocks(), getClassicSmoothRocks(), getClassicConcretes(), getAVASolidBlocks());
    }

    public static final Map<ChatFormatting, Integer> FORMATTING_INTEGER_COLOURS = new HashMap<>()
    {{
        for (ChatFormatting value : ChatFormatting.values())
        {
            Integer colour = value.getColor();
            if (colour != null)
                put(value, new Color(colour).getRGB());
        }
    }};

    public static final int EU_NAMES = 8;

    public static final int NRF_NAMES = 16;

    public static Map<TagKey<Block>, Float> BLOCK_HARDNESS = new HashMap<>()
    {{
        put(Tags.Blocks.GLASS_BLOCKS, 0.01F);
        put(Tags.Blocks.GLASS_PANES, 0.01F);

        put(Tags.Blocks.FENCE_GATES_WOODEN, 0.25F);
        put(Tags.Blocks.FENCES_WOODEN, 0.25F);
        put(BlockTags.TRAPDOORS, 0.25F);

        put(BlockTags.DOORS, 0.5F);

        put(Tags.Blocks.BARRELS_WOODEN, 0.75F);
        put(Tags.Blocks.BOOKSHELVES, 0.75F);
        put(Tags.Blocks.CHESTS, 0.75F);

        put(Tags.Blocks.GRAVELS, 1.7F);
        put(Tags.Blocks.SANDS, 1.7F);

        put(Tags.Blocks.SANDSTONE_BLOCKS, 2.0F);

        put(Tags.Blocks.COBBLESTONES, 2.3F);
        put(Tags.Blocks.END_STONES, 2.3F);
        put(Tags.Blocks.FENCES_NETHER_BRICK, 2.3F);
        put(Tags.Blocks.NETHERRACKS, 2.3F);
        put(Tags.Blocks.ORES, 2.3F);
        put(Tags.Blocks.STONES, 2.3F);

        put(Tags.Blocks.OBSIDIANS, 4.0F);
    }};

    public static Lazy<Map<Block, Float>> BLOCK_HARDNESS_2 = Lazy.of(() -> new HashMap<>()
    {{
        put(Blocks.IRON_BARS, 0.01F);
        put(AVABuildingBlocks.GLASS_WALL.get(), 0.01F);
        put(AVABuildingBlocks.GLASS_FENCE.get(), 0.01F);
        put(AVABuildingBlocks.GLASS_FENCE_TALL.get(), 0.01F);
        put(AVABuildingBlocks.GLASS_TRIG_WALL.get(), 0.01F);
        put(AVABuildingBlocks.GLASS_TRIG_WALL_FLIPPED.get(), 0.01F);

        put(AVABuildingBlocks.HARDENED_IRON_BARS.get(), 4.0F);
    }});

    public static float getBlockHardness(Level world, BlockPos pos, BlockState block)
    {
        if (!AVAServerConfig.DO_BULLET_WALL_PENETRATION.get())
            return 300000.0F;
        AtomicObject<Float> hardness = new AtomicObject<>((float) Math.log(block.getDestroySpeed(world, pos) + 1));
        BLOCK_HARDNESS.keySet().stream().filter(block::is).findFirst().ifPresent((key) -> hardness.set(BLOCK_HARDNESS.get(key)));
        BLOCK_HARDNESS_2.get().keySet().stream().filter(block::is).findFirst().ifPresent((key) -> hardness.set(BLOCK_HARDNESS_2.get().get(key)));
        return hardness.get();
    }

    public static final float DEFAULT_ENTITY_HARDNESS = 0.75F;
    public static Lazy<Map<EntityType<?>, Float>> ENTITY_HARDNESS = Lazy.of(() -> new HashMap<EntityType<?>, Float>()
    {{
        put(BAT, 0.15F);
        put(BEE, 0.15F);
        put(BLAZE, 0.45F);
        put(CAT, 0.25F);
        put(CAVE_SPIDER, 0.55F);
        put(CHICKEN, 0.35F);
        put(COD, 0.1F);
        put(END_CRYSTAL, 0.125F);
        put(ENDERMITE, 0.125F);
        put(FOX, 0.45F);
        put(IRON_GOLEM, 0.9F);
        put(MAGMA_CUBE, 0.3F);
        put(OCELOT, 0.25F);
        put(PARROT, 0.35F);
        put(PHANTOM, 0.45F);
        put(POLAR_BEAR, 0.8F);
        put(PUFFERFISH, 0.125F);
        put(RABBIT, 0.35F);
        put(RAVAGER, 0.85f);
        put(SALMON, 0.1F);
        put(SILVERFISH, 0.125F);
        put(SKELETON, 0.725F);
        put(SKELETON_HORSE, 0.725F);
        put(SLIME, 0.3F);
        put(SNOW_GOLEM, 0.35F);
        put(SPIDER, 0.55F);
        put(SQUID, 0.65F);
        put(STRAY, 0.775F);
        put(STRIDER, 0.675F);
        put(TROPICAL_FISH, 0.1F);
        put(TURTLE, 0.95F);
        put(VEX, 0.5F);
        put(WITHER, 0.85F);
        put(PLAYER, 0.8F);

        put(AVAEntities.LEOPARD_DESERT.get(), 1.0F);
        put(AVAEntities.LEOPARD_FOREST.get(), 1.0F);
        put(AVAEntities.LEOPARD_SNOW.get(), 1.0F);

        put(AVAEntities.PLAIN_SMOKE.get(), 0.0F);
    }});

    public static float getEntityHardness(Entity entity)
    {
        return getEntityHardness(entity.getType());
    }

    public static float getEntityHardness(EntityType<?> entity)
    {
        return ENTITY_HARDNESS.get().getOrDefault(entity, DEFAULT_ENTITY_HARDNESS) * 4.0F;
    }

    private static final Map<DyeColor, MapColor> DYE_TO_MATERIAL_COLOUR = new HashMap<>()
    {{
        put(DyeColor.WHITE, MapColor.SNOW);
        put(DyeColor.ORANGE, MapColor.COLOR_ORANGE);
        put(DyeColor.MAGENTA, MapColor.COLOR_MAGENTA);
        put(DyeColor.LIGHT_BLUE, MapColor.COLOR_LIGHT_BLUE);
        put(DyeColor.YELLOW, MapColor.COLOR_YELLOW);
        put(DyeColor.LIME, MapColor.COLOR_LIGHT_GREEN);
        put(DyeColor.PINK, MapColor.COLOR_PINK);
        put(DyeColor.GRAY, MapColor.COLOR_GRAY);
        put(DyeColor.LIGHT_GRAY, MapColor.COLOR_LIGHT_GRAY);
        put(DyeColor.CYAN, MapColor.COLOR_CYAN);
        put(DyeColor.PURPLE, MapColor.COLOR_PURPLE);
        put(DyeColor.BLUE, MapColor.COLOR_BLUE);
        put(DyeColor.BROWN, MapColor.COLOR_BROWN);
        put(DyeColor.GREEN, MapColor.COLOR_GREEN);
        put(DyeColor.RED, MapColor.COLOR_RED);
        put(DyeColor.BLACK, MapColor.COLOR_BLACK);
    }};

    public static MapColor dyeToMaterialColour(DyeColor colour)
    {
        return DYE_TO_MATERIAL_COLOUR.get(colour);
    }

    public static final PlayerTeam EMPTY_TEAM = new PlayerTeam(null, "Empty");

    private static final Map<SoundType, Pair<Supplier<SoundEvent>, Supplier<SoundEvent>>> BLOCK_ONHIT_SOUNDS = new HashMap<>() {{
        put(SCULK, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(GRASS, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(SAND, Pair.of(AVASounds.COMMON_BLOCK_PLASTIC, AVASounds.COMMON_BLOCK_PLASTIC));
        put(WOOD, Pair.of(AVASounds.COMMON_BLOCK_PLASTIC, AVASounds.COMMON_BLOCK_PLASTIC));
        put(NETHER_WOOD , Pair.of(AVASounds.BULLET_BLOCK_WOOD, AVASounds.MELEE_BLOCK_WOOD));
        put(BAMBOO_SAPLING, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(BAMBOO, Pair.of(AVASounds.BULLET_BLOCK_WOOD, AVASounds.MELEE_BLOCK_WOOD));
        put(WOOL, Pair.of(AVASounds.BULLET_BLOCK_WOOD, AVASounds.MELEE_BLOCK_WOOD));
        put(GLASS, Pair.of(AVASounds.COMMON_BLOCK_GLASS, AVASounds.COMMON_BLOCK_GLASS));
        put(STONE, Pair.of(AVASounds.BULLET_BLOCK_ROCK, AVASounds.MELEE_BLOCK_ROCK));
        put(SNOW, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(MOSS, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(AMETHYST, Pair.of(AVASounds.COMMON_BLOCK_GLASS, AVASounds.COMMON_BLOCK_GLASS));
        put(POWDER_SNOW, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(FROGSPAWN, Pair.of(AVASounds.BULLET_BLOCK_ORGANIC, AVASounds.MELEE_BLOCK_ORGANIC));
        put(FROGLIGHT, Pair.of(AVASounds.COMMON_BLOCK_PLASTIC, AVASounds.COMMON_BLOCK_PLASTIC));
    }};

    public static Optional<SoundEvent> getBulletOnHitBlockSound(Level world, BlockState state, BlockPos pos)
    {
        Supplier<SoundEvent> s = getOnHitBlockSound(world, state, pos).getA();
        return Optional.ofNullable(s == null ? null : s.get());
    }

    public static Optional<SoundEvent> getMeleeOnHitBlockSound(Level world, BlockState state, BlockPos pos)
    {
        Supplier<SoundEvent> s = getOnHitBlockSound(world, state, pos).getB();
        return Optional.ofNullable(s == null ? null : s.get());
    }

    private static Pair<Supplier<SoundEvent>, Supplier<SoundEvent>> getOnHitBlockSound(Level world, BlockState state, BlockPos pos)
    {
        Pair<Supplier<SoundEvent>, Supplier<SoundEvent>> sound = null;
        SoundType material = state.getSoundType(world, pos, null);
        if ((material == METAL || material == ANVIL))
        {
            if (state.isCollisionShapeFullBlock(world, pos))
                sound = Pair.of(AVASounds.BULLET_BLOCK_METAL_THIN, AVASounds.MELEE_BLOCK_METAL_THIN);
            else
            {
                if (state.getCollisionShape(world, pos).bounds().getSize() >= 0.5F)
                    sound = Pair.of(AVASounds.BULLET_BLOCK_METAL_CRUNCHY, AVASounds.MELEE_BLOCK_METAL_CRUNCHY);
                else
                    sound = Pair.of(AVASounds.BULLET_BLOCK_METAL_EMPTY, AVASounds.MELEE_BLOCK_METAL_THIN);
            }
        }
        return sound == null ? BLOCK_ONHIT_SOUNDS.getOrDefault(material, Pair.empty()) : sound;
    }
}