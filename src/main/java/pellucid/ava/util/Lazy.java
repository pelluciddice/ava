package pellucid.ava.util;

import java.util.function.Supplier;

public class Lazy<T> implements Supplier<T>
{
    private final Supplier<T> supplier;
    private T value = null;

    public static <T> Lazy<T> of(Supplier<T> supplier)
    {
        return new Lazy<>(supplier);
    }

    private Lazy(Supplier<T> supplier)
    {
        this.supplier = supplier;
    }

    public void forget()
    {
        value = null;
    }

    @Override
    public T get()
    {
        return value == null ? value = supplier.get() : value;
    }
}
