package pellucid.ava.cap;

import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.datafix.DataFixTypes;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.saveddata.SavedData;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import pellucid.ava.AVA;
import pellucid.ava.blocks.repairable.RepairableTileEntity;
import pellucid.ava.client.renderers.BaseEffect;
import pellucid.ava.client.renderers.EntityEffect;
import pellucid.ava.client.renderers.environment.ActivePingEffect;
import pellucid.ava.client.renderers.environment.EnvironmentObjectEffect;
import pellucid.ava.client.renderers.environment.GrenadeTrailEffect;
import pellucid.ava.commands.RenderingAreaCommand;
import pellucid.ava.entities.base.ProjectileEntity;
import pellucid.ava.events.forge.AVAWorldDataEvent;
import pellucid.ava.events.forge.DataHolder;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.player.PositionWithRotation;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

public class AVAWorldData extends SavedData
{
    private static final Map<ResourceKey<Level>, AVAWorldData> INSTANCE = new HashMap<>();
    private static boolean SERVER_INITIALIZED = false;
    private static final Map<ResourceKey<Level>, AVAWorldData> SERVER_INSTANCE = new HashMap<>();
    public final Map<UUID, Integer> uavEscortS = new HashMap<>();
    public final Map<Integer, Integer> uavEscortC = new HashMap<>();
    public final Map<UUID, Integer> uavS = new HashMap<>();
    public final Map<Integer, Integer> uavC = new HashMap<>();
    public final HashMap<UUID, Integer> x9S = new HashMap<>();
    public final Map<Integer, Integer> x9C = new HashMap<>();
    public final List<EnvironmentObjectEffect> bulletHoles = new ArrayList<>();
    public final List<EnvironmentObjectEffect> bloods = new ArrayList<>();
    public final List<EnvironmentObjectEffect> knifeHoles = new ArrayList<>();
    public final List<EnvironmentObjectEffect> grenadeMarks = new ArrayList<>();
    public final Map<ProjectileEntity, List<GrenadeTrailEffect>> grenadeTrails = new HashMap<>();
    public final List<EntityEffect> hitMarks = new ArrayList<>();
    public final List<EntityEffect> killMarks = new ArrayList<>();
    public final List<ActivePingEffect> activePings = new ArrayList<>();
    public final Map<Pair<Vec3, Vec3>, Integer> bulletTrails = new HashMap<>();
    public final List<BlockPos> repairablePositions = new ArrayList<>();
    public final Map<String, List<PositionWithRotation>> teamSpawns = new HashMap<>();
    public final List<RenderingAreaCommand.Area> renderingArea = new ArrayList<>();
    public boolean requireRenderUpdate = false;
    public final Pair<BlockPos, BlockPos> sitesMark = Pair.of(null, null);

    private final Map<String, DataHolder<?>> externalData = new HashMap<>();
    private CompoundTag awaitForSerialization = null;

    public AVAWorldData()
    {
    }

    public AVAWorldData(CompoundTag compound, ServerLevel world)
    {
        load(compound, world);
    }

    public <T> DataHolder<T> computeExternalData(Pair<String, Supplier<DataHolder<T>>> dataHolder)
    {
        String name = dataHolder.getA();
        return (DataHolder<T>) externalData.computeIfAbsent(name, (s) -> {
            DataHolder<T> data = dataHolder.getB().get();
            if (awaitForSerialization != null && awaitForSerialization.contains(name))
                data.deserializeNBT(awaitForSerialization.getCompound(name));
            return data;
        });
    }

    public void repairAll(Level world, boolean repair)
    {
        repairablePositions.forEach((pos) -> {
            BlockEntity te = world.getBlockEntity(pos);
            if (te instanceof RepairableTileEntity repairable)
            {
                if (repair)
                    repairable.revive();
                else
                    repairable.destruct(null);
            }
        });
    }

    public void setRequireRenderUpdate(boolean requireRenderUpdate)
    {
        this.requireRenderUpdate = requireRenderUpdate;
    }

    public boolean hasRenderAreaEnabled()
    {
        return new ArrayList<>(renderingArea).stream().anyMatch(RenderingAreaCommand.Area::isEnabled);
    }

    public void load(CompoundTag compound, @Nullable Level world)
    {
        if (AVACommonUtil.isEmptyTag(compound))
            return;
        for (DataHolder<?> data : externalData.values())
        {
            String name = data.getName();
            try
            {
                data.deserializeNBT(DataTypes.COMPOUND.read(compound, name));
            }
            catch (Exception e)
            {
                AVA.LOGGER.error("Something went wrong while serializing external mod per-world data for name: " + name);
            }
        }
        Runnable post = () -> {};
        if (world == null)
        {
            readUAVs(compound);
            activePings.clear();
            compound.getList("pings", Tag.TAG_COMPOUND).forEach((nbt) -> {
                ActivePingEffect effect = new ActivePingEffect();
                effect.deserializeNBT((CompoundTag) nbt);
                activePings.add(effect);
            });
//            requireRenderUpdate = DataTypes.BOOLEAN.read(compound, "requireRenderUpdate");
        }

        teamSpawns.clear();
        ListTag list = compound.getList("teamspawns", Tag.TAG_COMPOUND);
        for (Tag nbt2 : list)
        {
            CompoundTag compound2 = (CompoundTag) nbt2;
            List<PositionWithRotation> spawns = new ArrayList<>();
            teamSpawns.put(compound2.getString("team"), spawns);
            compound2.getList("spawns", Tag.TAG_COMPOUND).forEach((nbt3) -> spawns.add(new PositionWithRotation((CompoundTag) nbt3)));
        }

        renderingArea.clear();
        list = compound.getList("renderingAreas", Tag.TAG_COMPOUND);
        for (Tag nbt2 : list)
            renderingArea.add(new RenderingAreaCommand.Area((CompoundTag) nbt2));

        if (compound.contains("siteAPos"))
            sitesMark.setA(DataTypes.BLOCKPOS.read(compound, "siteAPos"));
        if (compound.contains("siteBPos"))
            sitesMark.setB(DataTypes.BLOCKPOS.read(compound, "siteBPos"));

        post.run();

        awaitForSerialization = compound;

        if (world instanceof ServerLevel server)
            DataToClientMessage.world(this, server, PacketDistributor::sendToAllPlayers);
    }

    public void readUAVs(CompoundTag compound)
    {
        uavEscortC.clear();
        uavC.clear();
        x9C.clear();
        int[] keys = compound.getIntArray("uavkeys");
        int[] values = compound.getIntArray("uavvalues");
        for (int i = 0; i < keys.length; i++)
            uavC.put(keys[i], values[i]);
        keys = compound.getIntArray("x9keys");
        values = compound.getIntArray("x9values");
        for (int i = 0; i < keys.length; i++)
            x9C.put(keys[i], values[i]);
        keys = compound.getIntArray("uavEscortkeys");
        values = compound.getIntArray("uavEscortvalues");
        for (int i = 0; i < keys.length; i++)
            uavEscortC.put(keys[i], values[i]);
    }

    @Override
    public boolean isDirty()
    {
        return true;
    }

    public void tick(Level world)
    {
        EnvironmentObjectEffect.tickList(activePings);
        for (DataHolder<?> dataHolder : externalData.values())
            dataHolder.tick(world);
        if (world.isClientSide())
        {
            AVACommonUtil.decreaseAndRemove(uavEscortC);
            AVACommonUtil.decreaseAndRemove(uavC);
            AVACommonUtil.decreaseAndRemove(x9C);
            AVACommonUtil.decreaseAndRemove(bulletTrails);
            EnvironmentObjectEffect.tickList(bulletHoles);
            if (world.getGameTime() % 5 == 0)
                bulletHoles.removeIf((bulletHole) -> {
                    BlockPos pos = bulletHole.getPos();
                    return world.getBlockState(pos).getShape(world, pos).toAabbs().stream().noneMatch((bb) -> bb.inflate(0.01F).contains(bulletHole.getVec().subtract(pos.getX(), pos.getY(), pos.getZ())));
                });
            BaseEffect.tickList(bloods);
            BaseEffect.tickList(knifeHoles);
            BaseEffect.tickList(grenadeMarks);
            grenadeTrails.values().forEach(BaseEffect::tickList);
            AVACommonUtil.removeIf(grenadeTrails, (k, list) -> list.isEmpty());
            BaseEffect.tickList(hitMarks);
            BaseEffect.tickList(killMarks);
        }
        else
        {
            AVACommonUtil.decreaseAndRemove(uavEscortS);
            AVACommonUtil.decreaseAndRemove(uavS);
            AVACommonUtil.decreaseAndRemove(x9S);
        }
    }

    public static void init()
    {
        MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
        if (server != null)
        {
            server.getAllLevels().forEach((world) -> {
                AVAWorldData data = world.getDataStorage().computeIfAbsent(new Factory<>(AVAWorldData::new, (compound, provider) -> new AVAWorldData(compound, world), DataFixTypes.LEVEL), "ava_world_data");
                AVAWorldDataEvent.onWorldDataPopulate(world, data);
                SERVER_INSTANCE.put(world.dimension(), data);
            });
            SERVER_INITIALIZED = true;
        }
    }

    public static AVAWorldData getInstance(Level world)
    {
        return getInstance(world.dimension(), !world.isClientSide);
    }

    public static AVAWorldData getInstance(ResourceKey<Level> world, boolean server)
    {
        if (world == null)
            return new AVAWorldData();
        if (server && !SERVER_INITIALIZED)
            init();
        return (server ? SERVER_INSTANCE : INSTANCE).computeIfAbsent(world, (e) -> new AVAWorldData());
    }

    public static Map<ResourceKey<Level>, AVAWorldData> getInstances()
    {
        return INSTANCE;
    }

    public static void clearServerInstances()
    {
        SERVER_INSTANCE.clear();
        SERVER_INITIALIZED = false;
    }

    @Override
    public CompoundTag save(CompoundTag compound, HolderLookup.Provider lookup)
    {
        try
        {
            for (DataHolder<?> data : externalData.values())
                if (data.isPersistent())
                    DataTypes.COMPOUND.write(compound, data.getName(), data.serializeNBT());
            ListTag list = new ListTag();
            for (Map.Entry<String, List<PositionWithRotation>> entry : teamSpawns.entrySet())
            {
                CompoundTag teamSpawn = new CompoundTag();
                DataTypes.STRING.write(teamSpawn, "team", entry.getKey());
                ListTag list2 = new ListTag();
                for (PositionWithRotation pos : entry.getValue())
                    list2.add(pos.serializeNBT());
                teamSpawn.put("spawns", list2);
                list.add(teamSpawn);
            }
            compound.put("teamspawns", list);

            ListTag list2 = new ListTag();
            for (RenderingAreaCommand.Area area : renderingArea)
                list2.add(area.write());
            compound.put("renderingAreas", list2);
            if (sitesMark.hasA())
                DataTypes.BLOCKPOS.write(compound, "siteAPos", sitesMark.getA());
            if (sitesMark.hasB())
                DataTypes.BLOCKPOS.write(compound, "siteBPos", sitesMark.getB());
            return compound;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return AVACommonUtil.emptyTag();
    }

    public CompoundTag saveToClient(ServerLevel world, HolderLookup.Provider provider)
    {
        try
        {
            CompoundTag compound = new CompoundTag();
            for (DataHolder<?> data : externalData.values())
                if (!data.isPersistent())
                    DataTypes.COMPOUND.write(compound, data.getName(), compound);

            saveUAVs(world, compound);

            ListTag list = new ListTag();
            activePings.forEach((ping) -> list.add(ping.serializeNBT()));
            compound.put("pings", list);
            DataTypes.BOOLEAN.write(compound, "requireRenderUpdate", requireRenderUpdate);

            return save(compound, provider);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return AVACommonUtil.emptyTag();
    }

    public CompoundTag saveUAVs(ServerLevel world, CompoundTag compound)
    {
        uavEscortS.keySet().removeIf((uuid) -> !(world.getEntity(uuid) instanceof LivingEntity));
        List<Integer> keys = new ArrayList<>();
        uavEscortS.keySet().forEach((uuid) -> keys.add(world.getEntity(uuid).getId()));
        compound.putIntArray("uavEscortkeys", keys);
        compound.putIntArray("uavEscortvalues", new ArrayList<>(uavEscortS.values()));

        uavS.keySet().removeIf((uuid) -> !(world.getEntity(uuid) instanceof LivingEntity));
        List<Integer> keys2 = new ArrayList<>();
        uavS.keySet().forEach((uuid) -> keys2.add(world.getEntity(uuid).getId()));
        compound.putIntArray("uavkeys", keys2);
        compound.putIntArray("uavvalues", new ArrayList<>(uavS.values()));

        x9S.keySet().removeIf((uuid) -> !(world.getEntity(uuid) instanceof Player));
        List<Integer> keys3 = new ArrayList<>();
        x9S.keySet().forEach((uuid) -> keys3.add(world.getEntity(uuid).getId()));
        compound.putIntArray("x9keys", keys3);
        compound.putIntArray("x9values", new ArrayList<>(x9S.values()));
        return compound;
    }
}
