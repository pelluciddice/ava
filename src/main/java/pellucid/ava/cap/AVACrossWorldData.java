package pellucid.ava.cap;

import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.datafix.DataFixTypes;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.saveddata.SavedData;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import pellucid.ava.commands.PlayTypeCommand;
import pellucid.ava.commands.RecoilRefundTypeCommand;
import pellucid.ava.gamemodes.scoreboard.AVAScoreboardManager;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.DataTypes;

import java.util.ArrayList;
import java.util.List;

public class AVACrossWorldData extends SavedData
{
    private static AVACrossWorldData INSTANCE;
    private static boolean SERVER_INITIALIZED = false;
    private static AVACrossWorldData SERVER_INSTANCE;
    public boolean shouldRenderCrosshair = true;
    public boolean friendlyFire = true;
    public boolean reducedFriendlyFire = true;
    public boolean doGlassBreak = true;
    public float mobDropsKitsChance = 0.2F;
    public final List<Timer> timers = new ArrayList<>();
    public RecoilRefundTypeCommand.RefundType recoilRefundType = RecoilRefundTypeCommand.RefundType.EXPONENTIAL;
    public final PlayTypeCommand.PlayMode playMode = new PlayTypeCommand.PlayMode();
    public final AVAScoreboardManager scoreboardManager = new AVAScoreboardManager();

    public AVACrossWorldData()
    {

    }

    public AVACrossWorldData(CompoundTag compound, HolderLookup.Provider lookup)
    {
        load(compound);
    }

    public void load(CompoundTag compound)
    {
        if (AVACommonUtil.isEmptyTag(compound))
            return;
        shouldRenderCrosshair = DataTypes.BOOLEAN.read(compound, "shouldRenderCrosshair");
        friendlyFire = DataTypes.BOOLEAN.read(compound, "friendlyFire");
        reducedFriendlyFire = DataTypes.BOOLEAN.read(compound, "reducedFriendlyFire");
        doGlassBreak = DataTypes.BOOLEAN.read(compound, "doGlassBreak");
        mobDropsKitsChance = DataTypes.FLOAT.read(compound, "mobDropsKitsChance");
        timers.clear();
        timers.addAll(Timer.read(compound.getList("timers", Tag.TAG_COMPOUND)));
        try
        {
            if (compound.contains("recoilRefundType", Tag.TAG_INT))
                recoilRefundType = RecoilRefundTypeCommand.RefundType.values()[DataTypes.INT.read(compound, "recoilRefundType")];
        }
        catch (Exception ignore)
        {
            recoilRefundType = RecoilRefundTypeCommand.RefundType.EXPONENTIAL;
        }
        playMode.deserializeNBT(compound.getCompound("playMode"));
        scoreboardManager.deserializeNBT(compound.getCompound("scoreboard"));
    }

    public Timer getTimer(String name, String obj)
    {
        return timers.stream().filter((timer) -> timer.getName().equals(name) && timer.getStorage().equals(obj)).findFirst().orElse(null);
    }

    public Timer getDisplayTimer()
    {
        return timers.stream().filter(Timer::isDisplaying).findFirst().orElse(null);
    }

    @Override
    public boolean isDirty()
    {
        return true;
    }

    public void tick(Level world)
    {
        if (!world.isClientSide())
        {
            Timer.tick(world, timers);
            scoreboardManager.tick((ServerLevel) world);
        }
    }

    public static void init()
    {
        MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
        if (server != null)
        {
            ServerLevel world = server.overworld();
            SERVER_INSTANCE = world.getDataStorage().computeIfAbsent(new Factory<>(AVACrossWorldData::new, AVACrossWorldData::new, DataFixTypes.LEVEL), "ava_data");
        }
        SERVER_INITIALIZED = true;
    }

    public static AVACrossWorldData getInstance()
    {
        return getInstance(ServerLifecycleHooks.getCurrentServer());
    }

    public static AVACrossWorldData getInstance(MinecraftServer server)
    {
        // Client
        if (server == null)
        {
            if (INSTANCE == null)
                INSTANCE = new AVACrossWorldData();
            return INSTANCE;
        }
        if (!SERVER_INITIALIZED)
            init();
        return SERVER_INSTANCE;
    }

    public static void clearServerInstance()
    {
        SERVER_INITIALIZED = false;
    }

    @Override
    public CompoundTag save(CompoundTag compound, HolderLookup.Provider lookup)
    {
        try
        {
            DataTypes.BOOLEAN.write(compound, "shouldRenderCrosshair", shouldRenderCrosshair);
            DataTypes.BOOLEAN.write(compound, "friendlyFire", friendlyFire);
            DataTypes.BOOLEAN.write(compound, "reducedFriendlyFire", reducedFriendlyFire);
            DataTypes.BOOLEAN.write(compound, "doGlassBreak", doGlassBreak);
            DataTypes.FLOAT.write(compound, "mobDropsKitsChance", mobDropsKitsChance);
            compound.put("timers", Timer.write(timers));
            DataTypes.INT.write(compound, "recoilRefundType", recoilRefundType.ordinal());
            DataTypes.COMPOUND.write(compound, "playMode", playMode.serializeNBT());
            DataTypes.COMPOUND.write(compound, "scoreboard", scoreboardManager.serializeNBT());
            return compound;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return AVACommonUtil.emptyTag();
    }

    @Override
    public String toString()
    {
        return "AVACrossWorldData{" +
                "shouldRenderCrosshair=" + shouldRenderCrosshair +
                ", friendlyFire=" + friendlyFire +
                ", reducedFriendlyFire=" + reducedFriendlyFire +
                ", doGlassBreak=" + doGlassBreak +
                ", mobDropsKitsChance=" + mobDropsKitsChance +
                ", timers=" + timers +
                ", recoilRefundType=" + recoilRefundType +
                ", playMode=" + playMode +
                ", scoreboardManager=" + scoreboardManager +
                '}';
    }
}
