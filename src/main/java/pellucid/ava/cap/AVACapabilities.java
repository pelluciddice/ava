package pellucid.ava.cap;

import net.neoforged.neoforge.attachment.AttachmentType;
import net.neoforged.neoforge.registries.DeferredRegister;
import net.neoforged.neoforge.registries.NeoForgeRegistries;
import pellucid.ava.AVA;

import java.util.function.Supplier;

public class AVACapabilities
{
    public static final DeferredRegister<AttachmentType<?>> CAPABILITIES = DeferredRegister.create(NeoForgeRegistries.Keys.ATTACHMENT_TYPES, AVA.MODID);

    public static final Supplier<AttachmentType<PlayerAction>> PLAYER = CAPABILITIES.register("handler", () -> AttachmentType.serializable(PlayerAction::new).build());
}
