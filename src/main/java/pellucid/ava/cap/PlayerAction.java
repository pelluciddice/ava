package pellucid.ava.cap;

import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.util.INBTSerializable;
import pellucid.ava.client.overlay.HurtInfo;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.util.DataTypes;

public class PlayerAction implements INBTSerializable<CompoundTag>
{
    private float recoil;
    private float shake;
    private float spread;
    private int flashDuration;
    private int stepSoundCooldown;
    private final HurtInfo hurtInfo = new HurtInfo();
    private int attackDamageBoost = 0;
    private int healthBoost = 0;
    private boolean parachute;
    private float armourValue = 10;
    private boolean ads = false;

    public PlayerAction() {}

    public static PlayerAction getCap(Player player)
    {
        if (player == null)
            return new PlayerAction();
        return player.getData(AVACapabilities.PLAYER);
    }

    @Override
    public CompoundTag serializeNBT(HolderLookup.Provider provider)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.FLOAT.write(compound, "recoil", recoil);
        DataTypes.FLOAT.write(compound, "shake", shake);
        DataTypes.FLOAT.write(compound, "spread", spread);
        DataTypes.INT.write(compound, "flash", flashDuration);
        DataTypes.INT.write(compound, "step", stepSoundCooldown);
        writeBoosts(compound);
        DataTypes.BOOLEAN.write(compound, "parachute", parachute);
        DataTypes.FLOAT.write(compound, "armourValue", armourValue);
        return compound;
    }

    @Override
    public void deserializeNBT(HolderLookup.Provider provider, CompoundTag nbt)
    {
        readBoosts(nbt);
    }

    public CompoundTag writeBoosts(CompoundTag compound)
    {
        DataTypes.INT.write(compound, "attackdamage", attackDamageBoost);
        DataTypes.INT.write(compound, "health", healthBoost);
        return compound;
    }

    public void readBoosts(CompoundTag compound)
    {
        attackDamageBoost = DataTypes.INT.read(compound, "attackdamage");
        healthBoost = DataTypes.INT.read(compound, "health");
    }

    public void setRecoil(float value)
    {
        this.recoil = value;
    }

    public void setShake(float value)
    {
        this.shake = value;
    }

    public void setSpread(float value)
    {
        this.spread = value;
    }

    public void setFlashDuration(int value)
    {
        this.flashDuration = value;
    }

    public void setStepSoundCooldown(int cooldown)
    {
     this.stepSoundCooldown = cooldown;
    }

    public void addHurtInfo(Entity entity)
    {
        hurtInfo.add(new HurtInfo.Info(entity));
    }

    public void addHurtInfo(Vec3 pos)
    {
        hurtInfo.add(new HurtInfo.Info(pos));
    }

    public void setAttackDamageBoost(int amount)
    {
        this.attackDamageBoost = Math.max(0, Math.min(20, amount));
    }

    public void setHealthBoost(int amount)
    {
        this.healthBoost = Math.max(0, Math.min(20, amount));
    }

    public void setIsUsingParachute(boolean parachute)
    {
        setIsUsingParachute(null, parachute);
    }

    public void setIsUsingParachute(Player player, boolean parachute)
    {
        this.parachute = parachute;
        if (player instanceof ServerPlayer)
            DataToClientMessage.playerActivatedParachute((ServerPlayer) player, parachute);
    }

    public void setArmourValue(float armourValue)
    {
        setArmourValue(null, armourValue);
    }

    public void setArmourValue(Player player, float value)
    {
        value = Math.max(0, value);
        this.armourValue = value;
        if (player instanceof ServerPlayer)
            DataToClientMessage.armourValue((ServerPlayer) player);
    }

    public void setIsADS(Level world, boolean value)
    {
        if (ads != value)
        {
            this.ads = value;
            if (world.isClientSide)
                PlayerActionMessage.ads(value);
        }
    }

    public float getRecoil()
    {
        return this.recoil;
    }

    public float getShake()
    {
        return this.shake;
    }

    public float getSpread()
    {
        return this.spread;
    }

    public int getFlashDuration()
    {
        return this.flashDuration;
    }

    public int getStepSoundCooldown()
    {
        return this.stepSoundCooldown;
    }

    public HurtInfo getHurtInfo()
    {
        return hurtInfo;
    }

    public int getAttackDamageBoost()
    {
        return this.attackDamageBoost;
    }

    public int getHealthBoost()
    {
        return this.healthBoost;
    }

    public boolean getUsingParachute()
    {
        return parachute;
    }

    public float getArmourValue()
    {
        return armourValue;
    }

    public boolean isADS()
    {
        return ads;
    }
}
