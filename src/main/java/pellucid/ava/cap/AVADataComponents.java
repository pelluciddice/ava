package pellucid.ava.cap;

import com.mojang.serialization.Codec;
import net.minecraft.core.UUIDUtil;
import net.minecraft.core.component.DataComponentType;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.codec.ByteBufCodecs;
import net.minecraft.util.ExtraCodecs;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;

import java.util.UUID;

public class AVADataComponents
{
    public static final DeferredRegister.DataComponents DATA_COMPONENT_TYPES = DeferredRegister.createDataComponents(AVA.MODID);
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_AMMO = integer("tag_item_ammo");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_ATTACK_LIGHT = integer("tag_item_attack_light");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_ATTACK_HEAVY = integer("tag_item_attack_heavy");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_DRAW = integer("tag_item_draw");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_FIRE = integer("tag_item_fire");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<CompoundTag>> TAG_ITEM_GUN_ATTACHMENTS_MANAGER = compound("tag_item_gun_attachments_manager");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<CompoundTag>> TAG_ITEM_GUN_STAT_MODIFIERS_MANAGER = compound("tag_item_gun_stat_modifiers_manager");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_IDLE = integer("tag_item_idle");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_INNER_CAPACITY = integer("tag_item_inner_capacity");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<CompoundTag>> TAG_ITEM_MASTERY_MANAGER = compound("tag_item_mastery_manager");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_PRE_RELOAD = integer("tag_item_pre_reload");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_RELOAD = integer("tag_item_reload");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_POST_RELOAD = integer("tag_item_post_reload");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_SILENCER_INSTALL = integer("tag_item_silencer_install");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Boolean>> TAG_ITEM_SILENCER_INSTALLED = bool("tag_item_silencer_installed");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Boolean>> TAG_ITEM_SHAKE_TURN = bool("tag_item_shake_turn");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Boolean>> TAG_ITEM_SPECIAL = bool("tag_item_special");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Float>> TAG_ITEM_TICKS = decimal("tag_item_ticks");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<UUID>> TAG_ITEM_UUID = DATA_COMPONENT_TYPES.register("tag_item_uuid", () -> DataComponentType.<UUID>builder().persistent(UUIDUtil.CODEC).networkSynchronized(UUIDUtil.STREAM_CODEC).build());
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_GUN_WEAPON_CATEGORY_MAIN = integer("tag_item_weapon_category_main");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_GUN_WEAPON_CATEGORY_SECONDARY = integer("tag_item_weapon_category_secondary");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_GUN_WEAPON_CATEGORY_MELEE = integer("tag_item_weapon_category_melee");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_GUN_WEAPON_CATEGORY_PROJECTILE = integer("tag_item_weapon_category_projectile");
    public static final DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> TAG_ITEM_GUN_WEAPON_CATEGORY_SPECIAL = integer("tag_item_weapon_category_special");


    private static DeferredHolder<DataComponentType<?>, DataComponentType<Integer>> integer(String name)
    {
        return DATA_COMPONENT_TYPES.register(name, () -> DataComponentType.<Integer>builder().persistent(ExtraCodecs.NON_NEGATIVE_INT.orElse(0)).networkSynchronized(ByteBufCodecs.VAR_INT).build());
    }

    private static DeferredHolder<DataComponentType<?>, DataComponentType<Boolean>> bool(String name)
    {
        return DATA_COMPONENT_TYPES.register(name, () -> DataComponentType.<Boolean>builder().persistent(Codec.BOOL.orElse(false)).networkSynchronized(ByteBufCodecs.BOOL).build());
    }

    private static DeferredHolder<DataComponentType<?>, DataComponentType<Float>> decimal(String name)
    {
        return DATA_COMPONENT_TYPES.register(name, () -> DataComponentType.<Float>builder().persistent(Codec.FLOAT.orElse(0.0F)).networkSynchronized(ByteBufCodecs.FLOAT).build());
    }

    private static DeferredHolder<DataComponentType<?>, DataComponentType<CompoundTag>> compound(String name)
    {
        return DATA_COMPONENT_TYPES.register(name, () -> DataComponentType.<CompoundTag>builder().persistent(CompoundTag.CODEC.orElse(new CompoundTag())).networkSynchronized(ByteBufCodecs.COMPOUND_TAG).build());
    }
}
