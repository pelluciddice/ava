package pellucid.ava.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;

public class AVACraftingCategory extends AVARecipeCategory<IHasRecipe>
{
    public AVACraftingCategory(IGuiHelper helper)
    {
        super(IHasRecipe.class, "crafting", AVABlocks.GUN_CRAFTING_TABLE.get(), helper, 0, 0, 117, 17);
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder builder, IHasRecipe iRecipe, IFocusGroup focuses)
    {
        int x = 1;
        Recipe recipe = iRecipe.getRecipe();
        for (Ingredient ingredients : recipe.getIngredients())
        {
            builder.addSlot(RecipeIngredientRole.INPUT, x, 1).addIngredients(ingredients);
            x += 18;
        }
        builder.addSlot(RecipeIngredientRole.OUTPUT, 101, 1).addItemStack(recipe.getResultStack(iRecipe.asItem()));
    }

    //    @Override
//    public void setRecipe(IRecipeLayoutBuilder builder, IHasRecipe iRecipe, List<? extends IFocus<?>> focuses)
//    {
//        int x = 1;
//        Recipe recipe = iRecipe.getRecipe();
//        for (ItemStack stack : recipe.getInputStacks())
//        {
//            builder.addSlot(RecipeIngredientRole.INPUT, x, 1).addItemStack(stack);
//            x += 18;
//        }
//        builder.addSlot(RecipeIngredientRole.OUTPUT, 101, 1).addItemStack(recipe.getResultStack(iRecipe.asItem()));
//    }
}
