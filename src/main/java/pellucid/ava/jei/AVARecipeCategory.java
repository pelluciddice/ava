package pellucid.ava.jei;

import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;
import pellucid.ava.AVA;
import pellucid.ava.recipes.IHasRecipe;

public abstract class AVARecipeCategory<T extends IHasRecipe> implements IRecipeCategory<T>
{
    protected final static ResourceLocation TEXTURE = new ResourceLocation(AVA.MODID, "textures/jei/background.png");;
    protected final static ResourceLocation TEXTURE_2 = new ResourceLocation(AVA.MODID, "textures/jei/background_2.png");;
    private final Class<T> clazz;
    protected final Component localizedName;
    protected final ResourceLocation UID;
    private final IDrawable icon;
    protected final IDrawable background;

    public AVARecipeCategory(Class<T> clazz, String name, ItemLike icon, IGuiHelper helper, int u, int v, int width, int height)
    {
        this(clazz, name, icon, helper, u, v, width, height, 0);
    }

    public AVARecipeCategory(Class<T> clazz, String name, ItemLike icon, IGuiHelper helper, int u, int v, int width, int height, int type)
    {
        this.clazz = clazz;
        this.UID = new ResourceLocation(AVA.MODID, name);
        IDrawableBuilder builder = helper.drawableBuilder(type == 0 ? TEXTURE : TEXTURE_2, u, v, width, height);
        if (type == 0)
            builder.setTextureSize(512, 512);
        this.background = builder.build();
        this.icon = helper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(icon));
        this.localizedName = Component.translatable(AVA.MODID + ".gui.jei." + name);
    }

    @Override
    public RecipeType<T> getRecipeType()
    {
        return new RecipeType<>(UID, clazz);
    }

    @Override
    public Component getTitle()
    {
        return localizedName;
    }

    @Override
    public IDrawable getBackground()
    {
        return background;
    }

    @Override
    public IDrawable getIcon()
    {
        return icon;
    }
}
