package pellucid.ava.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAConstants;

import java.util.HashMap;
import java.util.Map;

public class AVAGunStatCategory extends AVARecipeCategory<AVAItemGun>
{
    public AVAGunStatCategory(IGuiHelper helper)
    {
        super(AVAItemGun.class, "stats", AVABlocks.GUN_MODIFYING_TABLE.get(), helper, 0, 0, 256, 136, 1);
    }

    private static final Map<Item, ItemStack> CACHED_STACK_MODELS = new HashMap<>();

    @Override
    public void draw(AVAItemGun gun, IRecipeSlotsView recipeSlotsView, GuiGraphics stack, double mouseX, double mouseY)
    {
        this.stack = stack;
        text(gun.getDescription().getString(), 24, 6, AVAConstants.AVA_HUD_TEXT_ORANGE, false);
        int x = 2;
        int y = 12;
        for (MutableComponent string : GunStatTypes.getStrings(gun.getStats(), (k, v) -> k.forJei()))
        {
            text(string.getString(), x, y, AVAConstants.AVA_HUD_TEXT_WHITE, true);
            y += 3;
            if (y >= 81)
            {
                y = 3;
                x = 80;
            }
        }
    }

    private GuiGraphics stack;
    private void text(String text, int x, int y, int colour, boolean scaledDown)
    {
        if (scaledDown)
            AVAClientUtil.scaleText(stack.pose(), x, y, 0.8F, 0.65F, () -> {
                stack.drawString(Minecraft.getInstance().font, text, x, y, colour, true);
            });
        else
            stack.drawString(Minecraft.getInstance().font, text, x, y, colour, true);
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder builder, AVAItemGun gun, IFocusGroup focuses)
    {
        builder.addSlot(RecipeIngredientRole.INPUT, 2, 2).addItemStack(CACHED_STACK_MODELS.computeIfAbsent(gun, (item) -> new ItemStack(gun)));
    }


}
