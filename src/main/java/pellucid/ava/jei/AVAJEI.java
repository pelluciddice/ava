package pellucid.ava.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import mezz.jei.api.registration.IRecipeCatalystRegistration;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import pellucid.ava.AVA;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.ItemRecipeHolder;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

@JeiPlugin
public class AVAJEI implements IModPlugin
{
    @Override
    public ResourceLocation getPluginUid()
    {
        return AVAConstants.AVA_JEI_ID;
    }

    private IRecipeCategory<IHasRecipe> colouringCategory;
    private IRecipeCategory<ItemRecipeHolder> buildersCategory;
    private IRecipeCategory<IHasRecipe> craftingCategory;
    private IRecipeCategory<AVAItemGun> gunStatCategory;

    @Override
    public void registerCategories(IRecipeCategoryRegistration registration)
    {
        IJeiHelpers helpers = registration.getJeiHelpers();
        IGuiHelper guiHelper = helpers.getGuiHelper();
        registration.addRecipeCategories(
                craftingCategory = new AVACraftingCategory(guiHelper),
                buildersCategory = new AVABuildersCategory(guiHelper),
                colouringCategory = new AVAColouringCategory(guiHelper),
                gunStatCategory = new AVAGunStatCategory(guiHelper)
        );
    }


    public static final RecipeType<IHasRecipe> GUN_CRAFTING = RecipeType.create(AVA.MODID, "crafting", IHasRecipe.class);
    public static final RecipeType<ItemRecipeHolder> BUILDERS = RecipeType.create(AVA.MODID, "builders", ItemRecipeHolder.class);
    public static final RecipeType<IHasRecipe> GUN_COLOURING = RecipeType.create(AVA.MODID, "colouring", IHasRecipe.class);
    public static final RecipeType<AVAItemGun> GUN_STATS = RecipeType.create(AVA.MODID, "stats", AVAItemGun.class);
    @Override
    public void registerRecipes(IRecipeRegistration registration)
    {
        registration.addRecipes(GUN_CRAFTING, AVACommonUtil.cast(AVACommonUtil.combine(AVACommonUtil.getCraftableMap().values()), IHasRecipe.class));
        registration.addRecipes(BUILDERS, AVABuildingBlocks.RECIPES);
        registration.addRecipes(GUN_COLOURING, AVACommonUtil.cast(AVACommonUtil.getPaintables(), IHasRecipe.class));
        registration.addRecipes(GUN_STATS, AVACommonUtil.cast(AVAWeaponUtil.getAllGunLikes((item) -> !(item instanceof AVAItemGun gun) || !gun.isMaster()), AVAItemGun.class));
    }

    @Override
    public void registerRecipeCatalysts(IRecipeCatalystRegistration registration)
    {
        registration.addRecipeCatalyst(new ItemStack(AVABlocks.GUN_CRAFTING_TABLE.get()), GUN_CRAFTING);
        registration.addRecipeCatalyst(new ItemStack(AVABlocks.BUILDERS_TABLE.get()), BUILDERS);
        registration.addRecipeCatalyst(new ItemStack(AVABlocks.GUN_COLOURING_TABLE.get()), GUN_COLOURING);
        registration.addRecipeCatalyst(new ItemStack(AVABlocks.GUN_MODIFYING_TABLE.get()), GUN_STATS);
    }
}
