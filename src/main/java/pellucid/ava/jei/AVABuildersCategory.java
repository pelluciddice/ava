package pellucid.ava.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.recipes.ItemRecipeHolder;
import pellucid.ava.recipes.Recipe;

public class AVABuildersCategory extends AVARecipeCategory<ItemRecipeHolder>
{
    public AVABuildersCategory(IGuiHelper helper)
    {
        super(ItemRecipeHolder.class, "builders", AVABlocks.BUILDERS_TABLE.get(), helper, 0, 0, 117, 17);
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder builder, ItemRecipeHolder iRecipe, IFocusGroup focuses)
    {
        int x = 1;
        Recipe recipe = iRecipe.getRecipe();
        for (Ingredient ingredients : recipe.getIngredients())
        {
            builder.addSlot(RecipeIngredientRole.INPUT, x, 1).addIngredients(ingredients);
            x += 18;
        }
        builder.addSlot(RecipeIngredientRole.OUTPUT, 101, 1).addItemStack(recipe.getResultStack(iRecipe.getItem()));
    }
}
