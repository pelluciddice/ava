package pellucid.ava.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVACommonUtil;

import java.util.ArrayList;
import java.util.Arrays;

public class AVAColouringCategory extends AVARecipeCategory<IHasRecipe>
{
    public AVAColouringCategory(IGuiHelper helper)
    {
        super(IHasRecipe.class, "colouring", AVABlocks.GUN_COLOURING_TABLE.get(), helper, 0, 18, 158, 118);
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder builder, IHasRecipe iRecipe, IFocusGroup focuses)
    {
        AVAItemGun gun = (AVAItemGun) iRecipe.asItem();
        builder.addSlot(RecipeIngredientRole.INPUT, 1, 51).addItemStack(new ItemStack(gun));
        int y = 1;
        for (AVAItemGun subGun : AVACommonUtil.removeIf(new ArrayList<>(gun.subGuns), (item) -> item == gun))
        {
            Recipe recipe = subGun.getRecipe();
            int x = 42;
            for (Ingredient ingredients : AVACommonUtil.removeIf(recipe.getIngredients(), (i) -> Arrays.stream(i.getItems()).anyMatch((stack) -> stack.getItem() instanceof AVAItemGun)))
            {
                builder.addSlot(RecipeIngredientRole.INPUT, x, y).addIngredients(ingredients);
                x += 18;
            }
            builder.addSlot(RecipeIngredientRole.OUTPUT, 142, y).addItemStack(new ItemStack(subGun));
            y += 20;
        }
    }

    //    @Override
//    public void setRecipe(IRecipeLayoutBuilder builder, IHasRecipe iRecipe, List<? extends IFocus<?>> focuses)
//    {
//        AVAItemGun gun = (AVAItemGun) iRecipe.asItem();
//        builder.addSlot(RecipeIngredientRole.INPUT, 1, 51).addItemStack(new ItemStack(gun));
//        int y = 1;
//        for (AVAItemGun subGun : AVACommonUtil.removeIf(gun.subGuns, (item) -> item == gun))
//        {
//            Recipe recipe = subGun.getRecipe();
//            int x = 42;
//            for (ItemStack stack : AVACommonUtil.removeIf(recipe.getInputStacks(), (stack) -> stack.getItem() instanceof AVAItemGun))
//            {
//                builder.addSlot(RecipeIngredientRole.INPUT, x, y).addItemStack(stack);
//                x += 18;
//            }
//            builder.addSlot(RecipeIngredientRole.OUTPUT, 142, y).addItemStack(new ItemStack(subGun));
//            y += 20;
//        }
//    }
}
