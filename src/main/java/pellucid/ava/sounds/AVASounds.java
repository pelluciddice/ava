package pellucid.ava.sounds;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.neoforged.neoforge.registries.DeferredHolder;
import net.neoforged.neoforge.registries.DeferredRegister;
import pellucid.ava.AVA;
import pellucid.ava.util.AVAWeaponUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;

public class AVASounds
{
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(BuiltInRegistries.SOUND_EVENT, AVA.MODID);
    public static DeferredHolder<SoundEvent, SoundEvent> AIM;
    public static DeferredHolder<SoundEvent, SoundEvent> GRENADE_PULL;
    public static DeferredHolder<SoundEvent, SoundEvent> SILENCER_INSTALLS;
    public static DeferredHolder<SoundEvent, SoundEvent> SILENCER_UNINSTALLS;
    public static DeferredHolder<SoundEvent, SoundEvent> SILENCED_SHOT;

    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_DRAW_LIGHT;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_DRAW_HEAVY;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_DRAW_SNIPER;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_DRAW;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_DRAW_OUT;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_EQUIP;

    public static DeferredHolder<SoundEvent, SoundEvent> PARACHUTE_OPEN;

    public static DeferredHolder<SoundEvent, SoundEvent> NIGHT_VISION_ACTIVATE;
    public static DeferredHolder<SoundEvent, SoundEvent> BIO_INDICATOR_BEEP;
    public static DeferredHolder<SoundEvent, SoundEvent> CHAT_MESSAGE;

    public static DeferredHolder<SoundEvent, SoundEvent> UAV_CAPTURED;
    public static DeferredHolder<SoundEvent, SoundEvent> UAV_CAPTURES;
    public static DeferredHolder<SoundEvent, SoundEvent> UAV_SUPPORT;

    public static DeferredHolder<SoundEvent, SoundEvent> AMMO_KIT_SUPPLIER_CONSUME;

    public static DeferredHolder<SoundEvent, SoundEvent> RPG_BOX_OPEN;
    public static DeferredHolder<SoundEvent, SoundEvent> RPG_BOX_CLOSE;

    public static DeferredHolder<SoundEvent, SoundEvent> HEADSHOT;
    public static DeferredHolder<SoundEvent, SoundEvent> HEADSHOT_HELMET;

    public static DeferredHolder<SoundEvent, SoundEvent> GRENADE_HIT;

    public static DeferredHolder<SoundEvent, SoundEvent> COLLECTS_PICKABLE;
    public static DeferredHolder<SoundEvent, SoundEvent> PICKUP_ITEM;

    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_FLY_BY;

    public static DeferredHolder<SoundEvent, SoundEvent> C4_LEVER_UP;
    public static DeferredHolder<SoundEvent, SoundEvent> C4_BUTTON_HIT;
    public static DeferredHolder<SoundEvent, SoundEvent> C4_BEEPS;
    public static DeferredHolder<SoundEvent, SoundEvent> C4_EXPLODE;
    public static DeferredHolder<SoundEvent, SoundEvent> C4_DRAW;

    public static DeferredHolder<SoundEvent, SoundEvent> GENERIC_GRENADE_EXPLODE;
    public static DeferredHolder<SoundEvent, SoundEvent> MK3A2_EXPLODE;
    public static DeferredHolder<SoundEvent, SoundEvent> FLASH_GRENADE_EXPLODE;
    public static DeferredHolder<SoundEvent, SoundEvent> ROCKET_EXPLODE;
    public static DeferredHolder<SoundEvent, SoundEvent> ROCKET_TRAVEL;
    public static DeferredHolder<SoundEvent, SoundEvent> SMOKE_GRENADE_ACTIVE;
    public static DeferredHolder<SoundEvent, SoundEvent> EXPLOSIVE_BARREL_EXPLODE;

    public static DeferredHolder<SoundEvent, SoundEvent> BLOCK_BOOSTS_PLAYER;

    public static DeferredHolder<SoundEvent, SoundEvent> GRENADE_EU;
    public static DeferredHolder<SoundEvent, SoundEvent> GRENADE_NRF;
    public static DeferredHolder<SoundEvent, SoundEvent> RELOAD_EU;
    public static DeferredHolder<SoundEvent, SoundEvent> RELOAD_NRF;
    public static DeferredHolder<SoundEvent, SoundEvent> ENEMY_GRENADE_EU;
    public static DeferredHolder<SoundEvent, SoundEvent> ENEMY_GRENADE_NRF;
    public static DeferredHolder<SoundEvent, SoundEvent> ALLY_DOWN_EU;
    public static DeferredHolder<SoundEvent, SoundEvent> ALLY_DOWN_NRF;
    public static DeferredHolder<SoundEvent, SoundEvent> ENEMY_DOWN_EU;
    public static DeferredHolder<SoundEvent, SoundEvent> ENEMY_DOWN_NRF;
    public static DeferredHolder<SoundEvent, SoundEvent> ON_HIT_EU;
    public static DeferredHolder<SoundEvent, SoundEvent> ON_HIT_NRF;

    public static DeferredHolder<SoundEvent, SoundEvent> SELECT_PRESET;

    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_WOOD;
    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_METAL;
    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_SAND;
    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_SOLID;
    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_GRASS;
    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_FLOOD;
    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_WOOL;
//    public static DeferredHolder<SoundEvent, SoundEvent> FOOTSTEP_DEFAULT = get("default_footstep");

    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_CHARGE_DEFUSED;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_CHARGE_SET;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_ENEMY_ELIMINATED;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_FRIENDLY_ELIMINATED;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_MISSION_TIME_COMPLETE;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_OPERATION_FAIL;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_OPERATION_SUCCESS;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_TARGET_DESTROYED;
    public static DeferredHolder<SoundEvent, SoundEvent> BROADCAST_APPEAR;

    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_DIRT;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_METAL_CRUNCHY;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_METAL_EMPTY;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_METAL_THIN;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_ORGANIC;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_ROCK;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_WATER;
    public static DeferredHolder<SoundEvent, SoundEvent> BULLET_BLOCK_WOOD;

    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_BLOCK_GLASS;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_BLOCK_PLASTIC;

    public static DeferredHolder<SoundEvent, SoundEvent> MELEE_BLOCK_METAL_CRUNCHY;
    public static DeferredHolder<SoundEvent, SoundEvent> MELEE_BLOCK_METAL_THIN;
    public static DeferredHolder<SoundEvent, SoundEvent> MELEE_BLOCK_ORGANIC;
    public static DeferredHolder<SoundEvent, SoundEvent> MELEE_BLOCK_ROCK;
    public static DeferredHolder<SoundEvent, SoundEvent> MELEE_BLOCK_WOOD;

    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_ROBOT_ATTACK;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_ROBOT_DEATH;
    public static DeferredHolder<SoundEvent, SoundEvent> COMMON_ROBOT_ONHIT;
    public static DeferredHolder<SoundEvent, SoundEvent> BLUE_ROBOT_AMBIENT;
    public static DeferredHolder<SoundEvent, SoundEvent> DARK_BLUE_ROBOT_AMBIENT;
    public static DeferredHolder<SoundEvent, SoundEvent> DARK_BLUE_ROBOT_ONHIT;
    public static DeferredHolder<SoundEvent, SoundEvent> DARK_BLUE_ROBOT_STEP;
    public static DeferredHolder<SoundEvent, SoundEvent> YELLOW_ROBOT_AMBIENT;
    public static DeferredHolder<SoundEvent, SoundEvent> LEOPARD_AMBIENT;
    public static DeferredHolder<SoundEvent, SoundEvent> LEOPARD_MOVING;

    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_OUT_OF_BORDER_WARNING;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_CAR_ALARM;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_AC;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_RADIO;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_RADIO_SHORT;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_RADIO_SHORT2;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_CHOPPER;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_CHOPPER2;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_ALARM;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_SIGNAL;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_WIND;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_PLANE;
    public static DeferredHolder<SoundEvent, SoundEvent> ENVIRONMENT_FAN;

    private static DeferredHolder<SoundEvent, SoundEvent> register(String name)
    {
        return SOUNDS.register(name, () -> SoundEvent.createVariableRangeEvent(new ResourceLocation(AVA.MODID, name)));
    }

    private static final AVASounds INSTANCE = new AVASounds();

    public static final List<DeferredHolder<SoundEvent, SoundEvent>> SHOT_SOUNDS = new ArrayList<>();
    public static final List<DeferredHolder<SoundEvent, SoundEvent>> RELOAD_SOUNDS = new ArrayList<>();
    public static final List<DeferredHolder<SoundEvent, SoundEvent>> DRAW_SOUNDS = new ArrayList<>();
    public static final List<DeferredHolder<SoundEvent, SoundEvent>> FOOTSTEP_SOUNDS = new ArrayList<>();
    public static final List<DeferredHolder<SoundEvent, SoundEvent>> RADIO_SOUNDS = new ArrayList<DeferredHolder<SoundEvent, SoundEvent>>();
    public static final List<DeferredHolder<SoundEvent, SoundEvent>> BROADCAST_SOUNDS = new ArrayList<>();
    public static final List<DeferredHolder<SoundEvent, SoundEvent>> ENVIRONMENT_SOUNDS = new ArrayList<>();

    static
    {
        getAllFields((field, sound) ->
        {
            try
            {
                field.set(INSTANCE, sound);
                String name = field.getName().toLowerCase(Locale.ROOT);
                if (name.contains("shoot"))
                    SHOT_SOUNDS.add(sound);
                else if (name.contains("reload"))
                    RELOAD_SOUNDS.add(sound);
                else if (name.contains("draw"))
                    DRAW_SOUNDS.add(sound);
                else if (name.contains("footstep"))
                    FOOTSTEP_SOUNDS.add(sound);
                else if (name.contains("broadcast"))
                    BROADCAST_SOUNDS.add(sound);
                else if (name.contains("environment"))
                    ENVIRONMENT_SOUNDS.add(sound);
            }
            catch (IllegalAccessException e)
            {
                AVA.LOGGER.error("An exception occurred during registering sounds!");
            }
        });
    }

    public static void registerAll()
    {
        for (AVAWeaponUtil.RadioMessage radio : AVAWeaponUtil.RadioMessage.values())
        {
            if (!radio.noAudio())
            {
                DeferredHolder<SoundEvent, SoundEvent> eu = register(radio.name().toLowerCase(Locale.ROOT) + "_eu");
                DeferredHolder<SoundEvent, SoundEvent> nrf = register(radio.name().toLowerCase(Locale.ROOT) + "_nrf");
                radio.setEuSound(eu).setNrfSound(nrf);
                RADIO_SOUNDS.add(eu);
                RADIO_SOUNDS.add(nrf);
            }
        }
        for (String name : AVASoundTracks.GROUP_GUNS.getB())
            register(name);
        for (String name : AVASoundTracks.GROUP_MELEES.getB())
            register(name);
    }

    private static void getAllFields(BiConsumer<Field, DeferredHolder<SoundEvent, SoundEvent>> action)
    {
        for (Field field : AVASounds.class.getFields())
        {
            if (!Modifier.isFinal(field.getModifiers()))
                action.accept(field, register(field.getName().toLowerCase(Locale.ROOT)));
        }
    }
}
