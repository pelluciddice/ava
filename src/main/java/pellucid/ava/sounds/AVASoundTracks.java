package pellucid.ava.sounds;

import pellucid.ava.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static pellucid.ava.sounds.SoundTrack.Types;
import static pellucid.ava.sounds.SoundTrack.of;

public class AVASoundTracks
{
    public static final Pair<List<SoundTrack>, Set<String>> GROUP_GUNS = Pair.of(new ArrayList<>(), new TreeSet<>());
    public static final Pair<List<SoundTrack>, Set<String>> GROUP_MELEES = Pair.of(new ArrayList<>(), new TreeSet<>());

    public static final SoundTrack SILENCED_FIRE = of().add(0, AVASounds.SILENCED_SHOT);
    public static final SoundTrack AIM = of().add(0, AVASounds.AIM);
    public static final SoundTrack PIN_OFF = of().add(9, AVASounds.GRENADE_PULL);
    public static final SoundTrack C4_SET = of().add(8, AVASounds.C4_LEVER_UP).add(20, AVASounds.C4_BUTTON_HIT).add(30, AVASounds.C4_BUTTON_HIT).add(35, AVASounds.C4_BUTTON_HIT).add(40, AVASounds.C4_BUTTON_HIT).add(47, AVASounds.C4_BUTTON_HIT);
    public static final SoundTrack C4_DRAW = of().add(2, AVASounds.C4_DRAW);

    // Do not directly reference items here in case of cyclic dependencies
    public static final SoundTrack AK12_DRAW = of("ak12").add(12, Types.BOLT_OUT).add(16, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack AK12_FIRE = fire("ak12").build(GROUP_GUNS);
    public static final SoundTrack AK12_RELOAD = of("ak12").add(7, Types.MAG_OUT).add(25, Types.MAG_IN).add(33, Types.MAG_IN_2).add(45, Types.BOLT_OUT).add(49, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack AK47_DRAW = of("ak47").add(6, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack AK47_FIRE = fire("ak47").build(GROUP_GUNS);
    public static final SoundTrack AK47_RELOAD = of("ak47").add(7, Types.MAG_OUT).add(22, Types.SLAP).add(32, Types.MAG_IN).build(GROUP_GUNS);
    public static final SoundTrack AKS74U_DRAW = of("aks74u").add(8, Types.BOLT_OUT).add(15, Types.BOLT_IN).add(23, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack AKS74U_FIRE = fire("aks74u").build(GROUP_GUNS);
    public static final SoundTrack AKS74U_RELOAD = of("aks74u").add(6, Types.MAG_OUT).add(9, Types.MAG_OUT_2).add(23, Types.MAG_IN).add(36, Types.MAG_IN_2).add(45, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack AWM_DRAW = drawSniper(11).build(GROUP_GUNS);
    public static final SoundTrack AWM_FIRE = of("awm").add(0, Types.FIRE).add(7, Types.BOLT_OUT).add(15, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack AWM_RELOAD = of("awm").add(5, Types.MAG_OUT).add(16, Types.MAG_IN).add(29, Types.BOLT_OUT).add(33, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack BARRETT_DRAW = of("barrett").add(12, Types.BOLT_OUT).add(17, Types.BOLT_IN).add(31, AVASounds.COMMON_DRAW_SNIPER).build(GROUP_GUNS);
    public static final SoundTrack BARRETT_FIRE = fire("barrett").build(GROUP_GUNS);
    public static final SoundTrack BARRETT_RELOAD = of("barrett").add(14, Types.MAG_OUT).add(21, Types.MAG_OUT_2).add(43, Types.MAG_IN).add(61, Types.BOLT_OUT).add(67, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack BERETTA_92FS_DRAW = drawLight(5).build(GROUP_GUNS);
    public static final SoundTrack BERETTA_92FS_FIRE = fire("beretta_92fs").build(GROUP_GUNS);
    public static final SoundTrack BERETTA_92FS_RELOAD = of("beretta_92fs").add(3, Types.MAG_OUT).add(18, Types.MAG_IN).add(30, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack COLT_SAA_DRAW = drawLight(5).build(GROUP_GUNS);
    public static final SoundTrack COLT_SAA_FIRE = fire("colt_saa").build(GROUP_GUNS);
    public static final SoundTrack COLT_SAA_RELOAD = of("colt_saa").add(14, Types.MAG_OUT).add(20, Types.MAG_OUT).add(26, Types.MAG_OUT).add(32, Types.MAG_OUT).add(38, Types.MAG_OUT).add(44, Types.MAG_OUT).add(53, Types.MAG_IN).add(59, Types.MAG_IN).add(65, Types.MAG_IN).add(71, Types.MAG_IN).add(77, Types.MAG_IN).add(83, Types.MAG_IN).add(86, Types.MAG_ROLL).build(GROUP_GUNS);
    public static final SoundTrack CZ_EVO3_DRAW = equip(9).build(GROUP_GUNS);
    public static final SoundTrack CZ_EVO3_FIRE = fire("cz_evo3").build(GROUP_GUNS);
    public static final SoundTrack CZ_EVO3_RELOAD = of("cz_evo3").add(7, Types.MAG_OUT).add(26, Types.MAG_IN).add(34, Types.SLAP).add(42, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack D_DEFENSE_10GA_DRAW = of("d_defense_10ga").add(9, Types.OPEN).add(21, Types.CLOSE).build(GROUP_GUNS);
    public static final SoundTrack D_DEFENSE_10GA_FIRE = fire("d_defense_10ga").build(GROUP_GUNS);
    public static final SoundTrack D_DEFENSE_10GA_RELOAD = of("d_defense_10ga").add(5, Types.OPEN_WIDE).add(23, Types.MAG_IN).add(37, Types.CLOSE).build(GROUP_GUNS);
    public static final SoundTrack DESERT_EAGLE_DRAW = drawLight(5).build(GROUP_GUNS);
    public static final SoundTrack DESERT_EAGLE_FIRE = fire("desert_eagle").build(GROUP_GUNS);
    public static final SoundTrack DESERT_EAGLE_RELOAD = of("desert_eagle").add(4, Types.MAG_OUT).add(24, Types.MAG_IN).add(33, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack DSR1_DRAW = equip(1).add(8, AVASounds.COMMON_DRAW_SNIPER);
    public static final SoundTrack DSR1_FIRE = fire("dsr1").add(6, Types.BOLT_OUT).add(14, Types.BOLT_OUT_2).add(18, Types.BOLT_IN).add(22, Types.BOLT_IN_2).add(24, AVASounds.COMMON_DRAW_SNIPER).build(GROUP_GUNS);
    public static final SoundTrack DSR1_RELOAD = of("dsr1").add(8, Types.MAG_OUT).add(30, Types.MAG_IN).add(46, Types.BOLT_OUT).add(54, Types.BOLT_OUT_2).add(60, Types.BOLT_IN).add(63, Types.BOLT_IN_2).add(76, AVASounds.COMMON_DRAW_SNIPER).build(GROUP_GUNS);
    public static final SoundTrack FG42_DRAW = of("fg42").add(3, AVASounds.COMMON_DRAW_OUT).add(11, Types.BOLT_OUT).add(17, Types.BOLT_IN).add(20, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack FG42_FIRE = fire("fg42").build(GROUP_GUNS);
    public static final SoundTrack FG42_RELOAD = of("fg42").add(5, Types.MAG_OUT).add(23, Types.MAG_IN).add(42, Types.BOLT_OUT).add(48, Types.BOLT_IN).add(51, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack FN57_DRAW = drawLight(5).build(GROUP_GUNS);
    public static final SoundTrack FN57_FIRE = fire("fn57").build(GROUP_GUNS);
    public static final SoundTrack FN57_RELOAD = of("fn57").add(7, Types.MAG_OUT).add(25, Types.MAG_IN).add(33, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack FN_FNC_DRAW = of("fn_fnc").add(1, Types.STOCK_FOLD).add(6, Types.STOCK_FOLD_2).add(10, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack FN_FNC_FIRE = fire("fn_fnc").build(GROUP_GUNS);
    public static final SoundTrack FN_FNC_RELOAD = of("fn_fnc").add(5, Types.MAG_OUT).add(19, Types.MAG_IN).add(35, Types.BOLT_OUT).add(38, Types.BOLT_IN).add(43, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack FN_TPS_DRAW = of("fn_tps").add(11, Types.SLIDE_OUT).add(16, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack FN_TPS_FIRE = of("fn_tps").add(0, Types.FIRE).add(6, Types.SLIDE_OUT).add(11, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack FN_TPS_RELOAD = of("fn_tps").add(6, Types.MAG_IN).build(GROUP_GUNS);
    public static final SoundTrack FN_TPS_POST_RELOAD = of("fn_tps").add(5, Types.SLIDE_OUT).add(10, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack FR_F2_DRAW = of("fr_f2").add(1, AVASounds.COMMON_DRAW_OUT).add(14, AVASounds.COMMON_DRAW_SNIPER).build(GROUP_GUNS);
    public static final SoundTrack FR_F2_FIRE = of("fr_f2").add(0, Types.FIRE).add(6, Types.BOLT_OUT).add(14, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack FR_F2_RELOAD = of("fr_f2").add(7, Types.MAG_OUT).add(18, Types.MAG_IN).add(32, Types.BOLT_OUT).add(40, Types.BOLT_IN).add(41, AVASounds.COMMON_DRAW_SNIPER).build(GROUP_GUNS);
    public static final SoundTrack G36KA1_DRAW = of("g36ka1").add(2, AVASounds.COMMON_EQUIP).add(10, Types.STOCK_FOLD).add(20, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack G36KA1_FIRE = fire("g36ka1").build(GROUP_GUNS);
    public static final SoundTrack G36KA1_RELOAD = of("g36ka1").add(7, Types.MAG_OUT).add(20, Types.MAG_IN).add(32, Types.BOLT_OUT).add(37, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack GLOCK_DRAW = drawLight(5).build(GROUP_GUNS);
    public static final SoundTrack GLOCK_FIRE = fire("glock").build(GROUP_GUNS);
    public static final SoundTrack GLOCK_RELOAD = of("glock").add(2, Types.MAG_OUT).add(13, Types.MAG_IN).add(16, Types.MAG_IN_2).add(26, Types.SLIDE_OUT).add(29, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack GM94_DRAW = equip(12).build(GROUP_GUNS);
    public static final SoundTrack GM94_FIRE = of("gm94").add(0, Types.FIRE).add(3, Types.BOLT_OUT).add(11, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack GM94_RELOAD = of("gm94").add(4, Types.OPEN).add(17, Types.MAG_IN).add(17, Types.MAG_IN).add(30, Types.MAG_IN).add(37, Types.OPEN).add(41, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack K1A1_DRAW = of("k1a1").add(14, Types.BOLT_OUT).add(19, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack K1A1_FIRE = fire("k1a1").build(GROUP_GUNS);
    public static final SoundTrack K1A1_RELOAD = of("k1a1").add(4, Types.MAG_OUT).add(17, Types.MAG_IN).add(32, Types.BOLT_OUT).add(35, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack KELTEC_DRAW = of("keltec").add(1, AVASounds.COMMON_DRAW_OUT).add(14, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack KELTEC_FIRE = fire("keltec").build(GROUP_GUNS);
    public static final SoundTrack KELTEC_RELOAD = of("keltec").add(7, Types.MAG_OUT).add(22, Types.MAG_IN).add(24, Types.MAG_IN_2).add(40, Types.BOLT_OUT).add(43, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack KRISS_SUPER_V_DRAW = of("kriss_super_v").add(1, AVASounds.COMMON_DRAW_OUT).add(8, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack KRISS_SUPER_V_FIRE = fire("kriss_super_v").build(GROUP_GUNS);
    public static final SoundTrack KRISS_SUPER_V_RELOAD = of("kriss_super_v").add(10, Types.MAG_OUT).add(21, Types.MAG_IN).add(36, Types.BOLT_OUT).add(42, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M16_VN_DRAW = of("m16_vn").add(7, Types.BOLT_OUT).add(11, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M16_VN_FIRE = fire("m16_vn").build(GROUP_GUNS);
    public static final SoundTrack M16_VN_RELOAD = of("m16_vn").add(3, Types.MAG_OUT).add(19, Types.MAG_IN).add(31, Types.BOLT_OUT).add(34, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M1_GARAND_DRAW = of("m1_garand").add(14, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M1_GARAND_FIRE = fire("m1_garand").build(GROUP_GUNS);
    public static final SoundTrack M1_GARAND_RELOAD = of("m1_garand").add(3, Types.MAG_OUT).add(19, Types.MAG_IN).add(35, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M24_DRAW = drawSniper(6).build(GROUP_GUNS);
    public static final SoundTrack M24_FIRE = of("m24").add(0, Types.FIRE).add(11, Types.BOLT_OUT).add(15, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M24_RELOAD = of("m24").add(11, Types.MAG_OUT).add(23, Types.BOLT_OUT).add(27, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack M202_DRAW = equip(5).build(GROUP_GUNS);
    public static final SoundTrack M202_FIRE = fire("m202").build(GROUP_GUNS);
    public static final SoundTrack M202_RELOAD = of("m202").add(4, Types.RELOAD).build(GROUP_GUNS);
    public static final SoundTrack M4A1_DRAW = equip(7).build(GROUP_GUNS);
    public static final SoundTrack M4A1_FIRE = fire("m4a1").build(GROUP_GUNS);
    public static final SoundTrack M4A1_RELOAD = of("m4a1").add(4, Types.MAG_OUT).add(26, Types.MAG_IN).add(38, Types.BOLT_OUT).add(45, Types.BOLT_IN).add(47, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack M4A1_XPLORER_DRAW = of("m4a1_xplorer").add(7, Types.DRAW).add(12, Types.DRAW_2).build(GROUP_GUNS);
    public static final SoundTrack M4A1_XPLORER_FIRE = fire("m4a1_xplorer").build(GROUP_GUNS);
    public static final SoundTrack MAUSER_C96_DRAW = of("mauser_c96").add(0, Types.DRAW).build(GROUP_GUNS);
    public static final SoundTrack MAUSER_C96_FIRE = fire("mauser_c96").build(GROUP_GUNS);
    public static final SoundTrack MAUSER_C96_RELOAD = of("mauser_c96").add(0, Types.RELOAD).add(22, Types.MAG_IN).add(29, Types.MAG_IN_2).add(35, Types.MAG_OUT).build(GROUP_GUNS);
    public static final SoundTrack MK18_DRAW = of("mk18").add(12, Types.BOLT_OUT).add(16, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MK18_FIRE = fire("mk18").build(GROUP_GUNS);
    public static final SoundTrack MK18_RELOAD = of("mk18").add(6, Types.MAG_OUT).add(19, Types.MAG_IN).add(33, Types.BOLT_OUT).add(36, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MK20_DRAW = of("mk20").add(11, Types.BOLT_OUT).add(17, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MK20_FIRE = fire("mk20").build(GROUP_GUNS);
    public static final SoundTrack MK20_RELOAD = of("mk20").add(6, Types.MAG_OUT).add(22, Types.MAG_IN).add(29, Types.MAG_IN_2).add(37, Types.BOLT_OUT).add(45, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MOSIN_NAGANT_DRAW = drawSniper(3).build(GROUP_GUNS);
    public static final SoundTrack MOSIN_NAGANT_FIRE = of("mosin_nagant").add(0, Types.FIRE).add(11, Types.BOLT_OUT).add(17, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MOSIN_NAGANT_PRE_RELOAD = of("mosin_nagant").add(6, Types.OPEN).build(GROUP_GUNS);
    public static final SoundTrack MOSIN_NAGANT_RELOAD = of("mosin_nagant").add(6, Types.MAG_IN).build(GROUP_GUNS);
    public static final SoundTrack MOSIN_NAGANT_POST_RELOAD = of("mosin_nagant").add(1, Types.CLOSE).build(GROUP_GUNS);
    public static final SoundTrack MP5K_DRAW = of("mp5k").add(5, Types.BOLT_OUT).add(9, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MP5K_FIRE = fire("mp5k").build(GROUP_GUNS);
    public static final SoundTrack MP5K_RELOAD = of("mp5k").add(5, Types.MAG_OUT).add(20, Types.MAG_IN).add(29, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MP5SD5_DRAW = of("mp5sd5").add(8, Types.BOLT_OUT).add(12, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MP5SD5_FIRE = fire("mp5sd5").build(GROUP_GUNS);
    public static final SoundTrack MP5SD5_RELOAD = of("mp5sd5").add(6, Types.MAG_OUT).add(21, Types.MAG_IN).add(23, Types.MAG_IN_2).add(33, Types.BOLT_OUT).add(36, Types.BOLT_IN).add(40, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack MP7A1_DRAW = of("mp7a1").add(7, Types.BOLT_OUT).add(9, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack MP7A1_FIRE = fire("mp7a1").build(GROUP_GUNS);
    public static final SoundTrack MP7A1_RELOAD = of("mp7a1").add(5, Types.MAG_OUT).add(18, Types.MAG_IN).add(27, Types.BOLT_OUT).add(29, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack P90_DRAW = of("p90").add(1, AVASounds.COMMON_DRAW_OUT).add(6, AVASounds.COMMON_DRAW_LIGHT).build(GROUP_GUNS);
    public static final SoundTrack P90_FIRE = fire("p90").build(GROUP_GUNS);
    public static final SoundTrack P90_RELOAD = of("p90").add(6, Types.MAG_OUT).add(8, Types.MAG_OUT_2).add(26, Types.MAG_IN).add(32, Types.MAG_IN_2).add(42, Types.BOLT_OUT).add(44, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack P226_DRAW = of("p226").add(5, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack P226_FIRE = fire("p226").build(GROUP_GUNS);
    public static final SoundTrack P226_RELOAD = of("p226").add(10, Types.MAG_OUT).add(22, Types.MAG_IN).add(33, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack PYTHON357_DRAW = of("python357").add(3, Types.OPEN).add(13, Types.CLOSE).build(GROUP_GUNS);
    public static final SoundTrack PYTHON357_FIRE = fire("python357").build(GROUP_GUNS);
    public static final SoundTrack PYTHON357_RELOAD = of("python357").add(5, Types.OPEN).add(14, Types.MAG_OUT).add(41, Types.MAG_IN).add(57, Types.CLOSE).add(65, Types.MAG_ROLL).build(GROUP_GUNS);
    public static final SoundTrack REMINGTON870_DRAW = of("remington870").add(11, Types.SLIDE_OUT).add(16, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack REMINGTON870_FIRE = of("remington870").add(0, Types.FIRE).add(6, Types.SLIDE_OUT).add(11, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack REMINGTON870_RELOAD = of("remington870").add(6, Types.MAG_IN).build(GROUP_GUNS);
    public static final SoundTrack REMINGTON870_POST_RELOAD = of("remington870").add(5, Types.SLIDE_OUT).add(10, Types.SLIDE_IN).build(GROUP_GUNS);
    public static final SoundTrack RK95_DRAW = of("rk95").add(1, AVASounds.COMMON_DRAW_OUT).add(13, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack RK95_FIRE = fire("rk95").build(GROUP_GUNS);
    public static final SoundTrack RK95_RELOAD = of("rk95").add(13, Types.MAG_OUT).add(15, Types.MAG_OUT_2).add(28, Types.MAG_IN).build(GROUP_GUNS);
    public static final SoundTrack RPG7_DRAW = drawHeavy(18).build(GROUP_GUNS);
    public static final SoundTrack RPG7_FIRE = of("rpg7").build(GROUP_GUNS);
    public static final SoundTrack RPG7_RELOAD = of("rpg7").add(7, Types.MAG_OUT).add(10, Types.MAG_OUT_2).add(30, Types.MAG_IN).add(43, Types.MAG_IN_2).add(50, Types.SLIDE_OUT).add(53, Types.SLIDE_IN).add(78, AVASounds.COMMON_DRAW_HEAVY).build(GROUP_GUNS);
    public static final SoundTrack SA58_DRAW = of("sa58").add(8, Types.BOLT_OUT).add(14, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack SA58_FIRE = fire("sa58").build(GROUP_GUNS);
    public static final SoundTrack SA58_RELOAD = of("sa58").add(5, Types.MAG_OUT).add(26, Types.MAG_IN).add(40, Types.SLAP).build(GROUP_GUNS);
    public static final SoundTrack SCAR_L_RELOAD = of("scar_l").add(6, Types.MAG_OUT).add(17, Types.DRAW_2).add(22, Types.MAG_IN).add(29, Types.SLAP).add(37, Types.BOLT_OUT).add(41, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack SCAR_L_DRAW = of("scar_l").add(3, Types.DRAW).add(11, Types.BOLT_OUT).add(14, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack SCAR_L_FIRE = fire("scar_l").build(GROUP_GUNS);
    public static final SoundTrack SG556_DRAW = of("sg556").add(12, Types.SLAP).add(17, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack SG556_FIRE = fire("sg556").build(GROUP_GUNS);
    public static final SoundTrack SG556_RELOAD = of("sg556").add(11, Types.MAG_OUT).add(17, Types.MAG_OUT_2).add(24, Types.MAG_IN).add(28, Types.MAG_IN_2).add(38, Types.BOLT_OUT).add(40, Types.BOLT_IN).add(44, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack SR_2M_VERESK_DRAW = draw(2).build(GROUP_GUNS);
    public static final SoundTrack SR_2M_VERESK_FIRE = fire("sr_2m_veresk").build(GROUP_GUNS);
    public static final SoundTrack SR_2M_VERESK_RELOAD = of("sr_2m_veresk").add(4, Types.MAG_OUT).add(16, Types.MAG_IN).add(29, Types.BOLT_OUT).add(32, Types.BOLT_IN).add(39, AVASounds.COMMON_DRAW).build(GROUP_GUNS);
    public static final SoundTrack SR_25_DRAW = drawSniper(7).build(GROUP_GUNS);
    public static final SoundTrack SR_25_FIRE = fire("sr_25").build(GROUP_GUNS);
    public static final SoundTrack SR_25_RELOAD = of("sr_25").add(3, Types.MAG_OUT).add(14, Types.MAG_IN).add(30, Types.BOLT_OUT).add(32, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack SW1911_COLT_DRAW = of("sw1911_colt").add(3, Types.SLIDE_IN).add(9, Types.SLIDE_OUT).add(11, AVASounds.COMMON_DRAW_LIGHT).build(GROUP_GUNS);
    public static final SoundTrack SW1911_COLT_FIRE = fire("sw1911_colt").build(GROUP_GUNS);
    public static final SoundTrack SW1911_COLT_RELOAD = of("sw1911_colt").add(5, Types.MAG_OUT).add(23, Types.MAG_IN).add(33, Types.CLOSE).add(36, AVASounds.COMMON_DRAW_LIGHT).build(GROUP_GUNS);
    public static final SoundTrack X95R_DRAW = of("x95r").add(1, AVASounds.COMMON_DRAW_OUT).add(8, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack X95R_FIRE = fire("x95r").build(GROUP_GUNS);
    public static final SoundTrack X95R_RELOAD = of("x95r").add(4, Types.MAG_OUT).add(14, Types.MAG_IN).add(32, Types.BOLT_OUT).add(37, Types.BOLT_IN).add(39, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack XCR_DRAW = of("xcr").add(7, Types.BOLT_OUT).add(15, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack XCR_FIRE = fire("xcr").build(GROUP_GUNS);
    public static final SoundTrack XCR_RELOAD = of("xcr").add(7, Types.MAG_OUT).add(25, Types.MAG_IN).add(37, Types.BOLT_OUT).add(44, Types.BOLT_IN).build(GROUP_GUNS);
    public static final SoundTrack XM8_DRAW = of("xm8").add(7, Types.BOLT_OUT).add(14, Types.BOLT_IN).add(20, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);
    public static final SoundTrack XM8_FIRE = fire("xm8").build(GROUP_GUNS);
    public static final SoundTrack XM8_RELOAD = of("xm8").add(6, Types.MAG_OUT).add(23, Types.MAG_IN).add(42, Types.BOLT_OUT).add(46, Types.BOLT_IN).add(50, AVASounds.COMMON_EQUIP).build(GROUP_GUNS);



    public static final SoundTrack FIELD_KNIFE_DRAW = of("field_knife").add(6, Types.DRAW).build(GROUP_MELEES);
    public static final SoundTrack FIELD_KNIFE_ATTACK_LIGHT = of("field_knife").add(0, Types.ATTACK_LIGHT).build(GROUP_MELEES);
    public static final SoundTrack FIELD_KNIFE_ATTACK_HEAVY = of("field_knife").add(7, Types.ATTACK_HEAVY).build(GROUP_MELEES);
    public static final SoundTrack SCYTHE_IGNIS_DRAW = of("scythe_ignis").add(6, Types.DRAW).build(GROUP_MELEES);
    public static final SoundTrack SCYTHE_IGNIS_ATTACK_LIGHT = of("scythe_ignis").add(0, Types.ATTACK_LIGHT).build(GROUP_MELEES);
    public static final SoundTrack SCYTHE_IGNIS_ATTACK_HEAVY = of("scythe_ignis").add(7, Types.ATTACK_HEAVY).build(GROUP_MELEES);

    private static SoundTrack fire(String name)
    {
        return of(name).add(0, Types.FIRE);
    }

    private static SoundTrack draw(int ticks)
    {
        return of().add(ticks, AVASounds.COMMON_DRAW);
    }

    private static SoundTrack drawLight(int ticks)
    {
        return of().add(ticks, AVASounds.COMMON_DRAW_LIGHT);
    }

    private static SoundTrack drawHeavy(int ticks)
    {
        return of().add(ticks, AVASounds.COMMON_DRAW_HEAVY);
    }

    private static SoundTrack drawSniper(int ticks)
    {
        return of().add(ticks, AVASounds.COMMON_DRAW_SNIPER);
    }

    private static SoundTrack equip(int ticks)
    {
        return of().add(ticks, AVASounds.COMMON_EQUIP);
    }

    public static void registerAll()
    {

    }
}
