package pellucid.ava.sounds;

import com.google.common.collect.ImmutableList;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.sounds.SoundEvent;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.PlayLevelSoundEvent;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.Lazy;

import java.util.List;
import java.util.function.Supplier;

import static pellucid.ava.sounds.AVASounds.*;

@EventBusSubscriber(Dist.CLIENT)
public class AVASoundManager
{
    private static final Lazy<List<SoundEvent>> PASSIVE_RADIO_SOUNDS = Lazy.of(() -> AVACommonUtil.collect(ImmutableList.of(
            GRENADE_EU,
            GRENADE_NRF,
            RELOAD_EU,
            RELOAD_NRF,
            ENEMY_GRENADE_EU,
            ENEMY_GRENADE_NRF,
            ALLY_DOWN_EU,
            ALLY_DOWN_NRF,
            ENEMY_DOWN_EU,
            ENEMY_DOWN_NRF,
            ON_HIT_EU,
            ON_HIT_NRF
    ), Supplier::get));

    @SubscribeEvent
    public static void onSoundPlay(PlayLevelSoundEvent event)
    {
        if (!AVAClientConfig.ENABLE_PASSIVE_RADIO_VOICE.get() && PASSIVE_RADIO_SOUNDS.get().stream().anyMatch((sound) -> BuiltInRegistries.SOUND_EVENT.getKey(sound).equals(BuiltInRegistries.SOUND_EVENT.getKey(event.getSound().value()))))
            event.setCanceled(true);
    }
}
