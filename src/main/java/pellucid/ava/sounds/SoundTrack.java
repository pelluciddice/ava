package pellucid.ava.sounds;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.neoforged.neoforge.registries.DeferredItem;
import pellucid.ava.AVA;
import pellucid.ava.util.Lazy;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public class SoundTrack
{
    private final String name;
    private final Map<Integer, Track> soundTrack = new HashMap<>();
    private final Set<Types> typesUsed = new HashSet<>();
    private final Set<String> constructedSounds = new HashSet<>();

    private SoundTrack(String name)
    {
        this.name = name;
    }

    public boolean hasSoundAt(int ticks)
    {
        return soundTrack.keySet().contains(ticks);
    }

    public Set<Types> getTypesUsed()
    {
        return typesUsed;
    }

    public Set<String> getConstructedSounds()
    {
        return constructedSounds;
    }

    public boolean isEmpty()
    {
        return soundTrack.isEmpty();
    }

    public SoundTrack build(Pair<List<SoundTrack>, Set<String>> group)
    {
        group.getA().add(this);
        group.getB().addAll(getConstructedSounds());
        return this;
    }

    public SoundTrack add(int ticks, Supplier<SoundEvent> sound)
    {
        return add(ticks, sound, 1.0F);
    }

    public SoundTrack add(int ticks, Supplier<SoundEvent> sound, float pitch)
    {
        return add(ticks, sound, pitch, 1.0F);
    }

    public SoundTrack add(int ticks, Supplier<SoundEvent> sound, float pitch, float volume)
    {
        soundTrack.put(ticks, new Track(sound, pitch, volume));
        return this;
    }

    public SoundTrack add(int ticks, Types type)
    {
        return add(ticks, type, 1.0F);
    }

    public SoundTrack add(int ticks, Types type, float pitch)
    {
        return add(ticks, type, pitch, 1.0F);
    }

    public SoundTrack add(int ticks, Types type, float pitch, float volume)
    {
        typesUsed.add(type);
        String soundName = name + "_" + type.name().toLowerCase(Locale.ROOT);
        constructedSounds.add(soundName);
        soundTrack.put(ticks, new Track(Lazy.of(() -> BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(AVA.MODID, soundName))), pitch, volume));
        return this;
    }

    @Nullable
    public Track get(int ticks)
    {
        return soundTrack.get(ticks);
    }

    public static SoundTrack of(DeferredItem<?> item, String name)
    {
        return new SoundTrack(item.getId().getPath() + "_" + name);
    }

    public static SoundTrack of(DeferredItem<?> item)
    {
        return new SoundTrack(item.getId().getPath());
    }

    public static SoundTrack of(String name)
    {
        return new SoundTrack(name);
    }

    public static SoundTrack of()
    {
        return new SoundTrack("");
    }

    @Override
    public String toString()
    {
        return "SoundTrack{" +
                "name='" + name + '\'' +
                ", soundTrack=" + soundTrack +
                ", typesUsed=" + typesUsed +
                ", constructedSounds=" + constructedSounds +
                '}';
    }

    public enum Types
    {
        MAG_IN,
        MAG_IN_2,
        MAG_OUT,
        MAG_OUT_2,
        MAG_ROLL,
        BOLT_OUT,
        BOLT_OUT_2,
        BOLT_IN,
        BOLT_IN_2,
        SLAP,
        SLIDE_OUT,
        SLIDE_IN,
        FIRE,
        COMMON_DRAW,
        OPEN,
        OPEN_WIDE,
        CLOSE,
        STOCK_FOLD,
        STOCK_FOLD_2,
        DRAW,
        DRAW_2,
        RELOAD,
        ATTACK_LIGHT,
        ATTACK_HEAVY;
    }

    public static class Track
    {
        public final Supplier<SoundEvent> sound;
        public final float pitch;
        public final float volume;

        public Track(Supplier<SoundEvent> sound, float pitch, float volume)
        {
            this.sound = sound;
            this.pitch = pitch;
            this.volume = volume;
        }
    }
}