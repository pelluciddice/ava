package pellucid.ava.recipes;

import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.blocks.AVABuildingBlocks;
import pellucid.ava.events.data.tags.AVAItemTagsProvider;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.init.MiscItems;

public class AVAGunRecipes
{
    public static final Recipe REGULAR_PISTOL_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(2);
    public static final Recipe SMALL_PISTOL_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(4);
    public static final Recipe LARGE_PISTOL_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 4).addItem(Materials.PACKED_GUNPOWDER).setResultCount(2);
    public static final Recipe PISTOL_AMMO = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(16);
    public static final Recipe REGULAR_RIFLE_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER);
    public static final Recipe SMALL_RIFLE_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 2).addItem(Materials.PACKED_GUNPOWDER);
    public static final Recipe SNIPER_AMMO = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(10);
    public static final Recipe SMALL_SNIPER_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(2);
    public static final Recipe REGULAR_SNIPER_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 5).addItem(Materials.PACKED_GUNPOWDER);
    public static final Recipe REGULAR_SUB_MACHINEGUN_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 2).addItem(Materials.PACKED_GUNPOWDER);
    public static final Recipe SMALL_SUB_MACHINEGUN_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(2);
    public static final Recipe SHOTGUN_AMMO = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 3).addItem(Materials.PACKED_GUNPOWDER).setResultCount(10);
    public static final Recipe HEAVY_AP_MAGAZINE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 12).addItem(Materials.PACKED_GUNPOWDER);


    public static final Recipe FIELD_KNIFE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 10).addItem(AVAItemTagsProvider.SILICON, 8);
    public static final Recipe SCYTHE_IGNIS = new Recipe().addItem(ItemTags.ACACIA_LOGS, 12).addItem(Items.PUMPKIN, 16);


    public static final Recipe EU_STANDARD_BOOTS = new Recipe().addItem(Materials.CERAMIC, 4).addItem(AVAItemTagsProvider.FIBRE, 12).addItem(Items.YELLOW_DYE, 2).addItem(Items.LEATHER_BOOTS);
    public static final Recipe EU_STANDARD_TROUSERS = new Recipe().addItem(Materials.CERAMIC, 7).addItem(AVAItemTagsProvider.FIBRE, 21).addItem(Items.YELLOW_DYE, 3).addItem(Items.LEATHER_LEGGINGS);
    public static final Recipe EU_STANDARD_KEVLAR = new Recipe().addItem(Materials.CERAMIC, 8).addItem(AVAItemTagsProvider.FIBRE, 24).addItem(Items.YELLOW_DYE, 4).addItem(Items.LEATHER_CHESTPLATE);
    public static final Recipe EU_STANDARD_HELMET = new Recipe().addItem(Materials.CERAMIC, 5).addItem(AVAItemTagsProvider.FIBRE, 10).addItem(Items.YELLOW_DYE, 3).addItem(Items.LEATHER_HELMET);
    public static final Recipe NRF_STANDARD_BOOTS = new Recipe().addItem(Materials.CERAMIC, 4).addItem(AVAItemTagsProvider.FIBRE, 12).addItem(Items.BLUE_DYE, 2).addItem(Items.LEATHER_BOOTS);
    public static final Recipe NRF_STANDARD_TROUSERS = new Recipe().addItem(Materials.CERAMIC, 7).addItem(AVAItemTagsProvider.FIBRE, 21).addItem(Items.BLUE_DYE, 3).addItem(Items.LEATHER_LEGGINGS);
    public static final Recipe NRF_STANDARD_KEVLAR = new Recipe().addItem(Materials.CERAMIC, 8).addItem(AVAItemTagsProvider.FIBRE, 24).addItem(Items.BLUE_DYE, 4).addItem(Items.LEATHER_CHESTPLATE);
    public static final Recipe NRF_STANDARD_HELMET = new Recipe().addItem(Materials.CERAMIC, 5).addItem(AVAItemTagsProvider.FIBRE, 10).addItem(Items.BLUE_DYE, 3).addItem(Items.LEATHER_HELMET);
    public static final Recipe AMMO_KIT = new Recipe().addItem(AVAItemTagsProvider.FIBRE, 30).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 10);
    public static final Recipe AMMO_KIT_I = new Recipe().addItem(AVAItemTagsProvider.FIBRE, 64).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 64).addItem(MiscItems.AMMO_KIT);
    public static final Recipe C4 = new Recipe().addItem(Materials.PACKED_GUNPOWDER).addItem(Tags.Items.NUGGETS_IRON, 8).addItem(Materials.FUSE);
    public final static Recipe PARACHUTE = new Recipe().addItem(Items.STRING, 32).addItem(AVAItemTagsProvider.FIBRE, 20).addItem(Items.GREEN_DYE, 8);


    public static final Recipe P226 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 20).addItem(AVAItemTagsProvider.SILICON, 10).addItem(AVAItemTagsProvider.PLASTIC, 10);
    public static final Recipe SW1911_COLT = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(AVAItemTagsProvider.SILICON, 4).addItem(AVAItemTagsProvider.PLASTIC, 6);
    public static final Recipe PYTHON357 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 24).addItem(AVAItemTagsProvider.COMPRESSED_WOOD, 16);
    public static final Recipe MAUSER_C96 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(AVAItemTagsProvider.COMPRESSED_WOOD, 10);
    public static final Recipe COLT_SAA = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 36).addItem(Tags.Items.BONES, 4);
    public static final Recipe BERETTA_92FS = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 20).addItem(AVAItemTagsProvider.SILICON, 10).addItem(AVAItemTagsProvider.PLASTIC, 10);
    public static final Recipe FN57 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 16).addItem(AVAItemTagsProvider.SILICON, 12).addItem(AVAItemTagsProvider.PLASTIC, 12);
    public static final Recipe DESERT_EAGLE = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 24).addItem(AVAItemTagsProvider.SILICON, 8).addItem(AVAItemTagsProvider.FIBRE, 12);
    public static final Recipe GLOCK = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 14).addItem(AVAItemTagsProvider.SILICON, 16).addItem(AVAItemTagsProvider.PLASTIC, 14);

    public static final Recipe M67 = new Recipe().addItem(Items.IRON_NUGGET, 12).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 2).addItem(Materials.PACKED_GUNPOWDER).addItem(Materials.FUSE, 3).setResultCount(3);
    public static final Recipe M67_SPORTS = new Recipe().addItem(Items.IRON_NUGGET, 12).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 2).addItem(Materials.PACKED_GUNPOWDER).addItem(Materials.FUSE, 3).setResultCount(3);
    public static final Recipe MK3A2 = new Recipe().addItem(Items.GLOWSTONE_DUST, 10).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 2).addItem(Materials.PACKED_GUNPOWDER).addItem(Materials.FUSE, 2).setResultCount(2).addDescription("mk3a2").addDescription("mk3a2_2");
    public static final Recipe M116A1 = new Recipe().addItem(Items.GLOWSTONE_DUST, 16).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 1).addItem(Materials.PACKED_GUNPOWDER).addItem(Materials.FUSE, 2).setResultCount(2).addDescription("m116a1");
    public static final Recipe M18_GREY = new Recipe().addItem(Tags.Items.DYES_LIGHT_GRAY, 2).addDescription("general_smoke");
    public static final Recipe M18_GREY_II = new Recipe().addItem(Tags.Items.DYES_LIGHT_GRAY, 2).addDescription("average_smoke");
    public static final Recipe M18_GREY_III = new Recipe().addItem(Tags.Items.DYES_LIGHT_GRAY, 2).addDescription("fast_smoke");
    public static final Recipe M18_RED = new Recipe().addItem(Tags.Items.DYES_RED, 2).addDescription("general_smoke");
    public static final Recipe M18_RED_II = new Recipe().addItem(Tags.Items.DYES_RED, 2).addDescription("average_smoke");
    public static final Recipe M18_RED_III = new Recipe().addItem(Tags.Items.DYES_RED, 2).addDescription("fast_smoke");
    public static final Recipe M18_PURPLE = new Recipe().addItem(Tags.Items.DYES_PURPLE, 2).addDescription("slow_smoke");
    public static final Recipe M18_TOXIC = new Recipe().addItem(Items.SPIDER_EYE, 16).addDescription("toxic_smoke");
    public static final Recipe M18_YELLOW = new Recipe().addItem(Tags.Items.DYES_YELLOW, 2).addDescription("slow_smoke");
    public static final Recipe M18_BLUE = new Recipe().addItem(Tags.Items.DYES_BLUE, 2).addDescription("slow_smoke");


    public static final Recipe M4A1 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(AVAItemTagsProvider.SILICON, 14).addItem(AVAItemTagsProvider.PLASTIC, 16).addItem(Materials.LENS);
    public static final Recipe MK20 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 38).addItem(AVAItemTagsProvider.SILICON, 16).addItem(AVAItemTagsProvider.PLASTIC, 6).addItem(Materials.LENS);
    public static final Recipe FN_FNC = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 40).addItem(AVAItemTagsProvider.SILICON, 10).addItem(AVAItemTagsProvider.PLASTIC, 10);
    public static final Recipe XM8 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 24).addItem(AVAItemTagsProvider.SILICON, 16).addItem(AVAItemTagsProvider.PLASTIC, 20).addItem(Materials.LENS);
    public static final Recipe FG42 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 42).addItem(Materials.COMPRESSED_WOOD, 18);
    public static final Recipe SG556 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 24).addItem(AVAItemTagsProvider.SILICON, 22).addItem(AVAItemTagsProvider.PLASTIC, 14).addItem(Materials.LENS);
    public static final Recipe AK12 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 34).addItem(AVAItemTagsProvider.SILICON, 18).addItem(AVAItemTagsProvider.PLASTIC, 8);
    public static final Recipe M16_VN = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(AVAItemTagsProvider.SILICON, 8).addItem(AVAItemTagsProvider.PLASTIC, 22);
    public static final Recipe RK95 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(AVAItemTagsProvider.SILICON, 20).addItem(AVAItemTagsProvider.PLASTIC, 20);
    public static final Recipe SA58 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 40).addItem(AVAItemTagsProvider.PLASTIC, 15);
    public static final Recipe KELTEC = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 32).addItem(AVAItemTagsProvider.SILICON, 25);
    public static final Recipe AK47 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 40).addItem(ItemTags.PLANKS, 30);
    public static final Recipe SCAR_L = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 28).addItem(AVAItemTagsProvider.SILICON, 14).addItem(AVAItemTagsProvider.PLASTIC, 20);
    public static final Recipe M1_GARAND = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 14).addItem(Materials.COMPRESSED_WOOD, 40);
    public static final Recipe G36KA1 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 10).addItem(AVAItemTagsProvider.SILICON, 22).addItem(AVAItemTagsProvider.PLASTIC, 25).addItem(Materials.LENS);
    public static final Recipe XCR = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 32).addItem(AVAItemTagsProvider.SILICON, 16).addItem(AVAItemTagsProvider.PLASTIC, 10).addItem(Materials.LENS);

    public static final Recipe MOSIN_NAGANT = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 26).addItem(Materials.COMPRESSED_WOOD, 34).addItem(Materials.LENS, 2);
    public static final Recipe M24 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 40).addItem(AVAItemTagsProvider.SILICON, 10).addItem(AVAItemTagsProvider.PLASTIC, 10).addItem(Materials.LENS, 2);
    public static final Recipe SR_25 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 40).addItem(AVAItemTagsProvider.SILICON, 10).addItem(AVAItemTagsProvider.PLASTIC, 10).addItem(Materials.LENS, 2);
    public static final Recipe FR_F2 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 36).addItem(AVAItemTagsProvider.COMPRESSED_WOOD, 30).addItem(Materials.LENS, 2);
    public static final Recipe BARRETT = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 64).addItem(AVAItemTagsProvider.SILICON, 32).addItem(Materials.LENS, 2);
    public static final Recipe DSR1 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 24).addItem(AVAItemTagsProvider.SILICON, 24).addItem(Materials.LENS, 2);
    public static final Recipe AWM = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 32).addItem(AVAItemTagsProvider.SILICON, 12).addItem(Materials.LENS, 2);


    public static final Recipe M202_ROCKET = new Recipe().addItem(Materials.FUSE).addItem(Materials.PACKED_GUNPOWDER).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON);
    public static final Recipe M202 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 40).addItem(AVAItemTagsProvider.SILICON, 20).addItem(Tags.Items.DYES_GREEN, 16);
    public static final Recipe GM94_GRENADE = new Recipe().addItem(Materials.FUSE).addItem(Materials.PACKED_GUNPOWDER).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON).setResultCount(2);
    public static final Recipe GM94 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(AVAItemTagsProvider.SILICON, 30);
    public static final Recipe BINOCULAR = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 20).addItem(Materials.LENS, 4).addItem(Tags.Items.DYES_GREEN, 8).addDescription("binocular");
    public static final Recipe RPG7 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 30).addItem(Materials.COMPRESSED_WOOD, 40).addItem(AVAItemTagsProvider.SILICON, 20);


    public static final Recipe X95R = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 28).addItem(AVAItemTagsProvider.SILICON, 20).addItem(AVAItemTagsProvider.PLASTIC, 12).addItem(Materials.LENS);
    public static final Recipe MP5SD5 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 22).addItem(AVAItemTagsProvider.SILICON, 22).addItem(AVAItemTagsProvider.PLASTIC, 16);
    public static final Recipe MK18 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 38).addItem(AVAItemTagsProvider.SILICON, 14).addItem(AVAItemTagsProvider.PLASTIC, 8);
    public static final Recipe REMINGTON870 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 34).addItem(AVAItemTagsProvider.SILICON, 22).addItem(AVAItemTagsProvider.PLASTIC, 4);
    public static final Recipe FN_TPS = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 32).addItem(AVAItemTagsProvider.SILICON, 16).addItem(AVAItemTagsProvider.PLASTIC, 6);
    public static final Recipe MP5K = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 20).addItem(AVAItemTagsProvider.SILICON, 20).addItem(AVAItemTagsProvider.PLASTIC, 20).addItem(Materials.LENS);
    public static final Recipe SR_2M_VERESK = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 18).addItem(AVAItemTagsProvider.SILICON, 20).addItem(AVAItemTagsProvider.PLASTIC, 22).addItem(Materials.LENS);
    public static final Recipe D_DEFENSE_10GA = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 36).addItem(AVAItemTagsProvider.COMPRESSED_WOOD, 24);
    public static final Recipe KRISS = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 16).addItem(AVAItemTagsProvider.SILICON, 24).addItem(AVAItemTagsProvider.PLASTIC, 20).addItem(Materials.LENS);
    public static final Recipe CZ_EVO3 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 32).addItem(AVAItemTagsProvider.SILICON, 8).addItem(AVAItemTagsProvider.PLASTIC, 4);
    public static final Recipe K1A1 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 22).addItem(AVAItemTagsProvider.SILICON, 36).addItem(Materials.LENS);
    public static final Recipe MP7A1 = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 26).addItem(AVAItemTagsProvider.SILICON, 12).addItem(AVAItemTagsProvider.PLASTIC, 12).addItem(Materials.LENS);
    public static final Recipe AKS74U = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 28).addItem(AVAItemTagsProvider.SILICON, 8).addItem(AVAItemTagsProvider.COMPRESSED_WOOD, 8);


    public static final Recipe AMMO_KIT_SUPPLIER = new Recipe().addItem(MiscItems.AMMO_KIT_I, 8).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 64).addItem(AVAItemTagsProvider.PACKED_GUNPOWDER, 64);
    public static final Recipe EXPLOSIVE_BARREL = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 4).addItem(Materials.PACKED_GUNPOWDER, 3).setResultCount(2);
    public static final Recipe CONTROLLER = new Recipe().addItem(AVAItemTagsProvider.WORK_HARDENED_IRON, 4).addItem(Tags.Items.INGOTS_GOLD, 4).addItem(Items.REPEATER, 3).addItem(Items.LEVER, 3).setResultCount(3);


    public static final Recipe SMOOTH_STONE_STAIRS = new Recipe().addItem(Blocks.SMOOTH_STONE, 6).setResultCount(4);
    public static final Recipe COBBLED_SANDSTONE_TILE = new Recipe().addItem(Blocks.SANDSTONE);
    public static final Recipe COBBLED_SANDSTONE_TILE_SLAB = new Recipe().addItem(Blocks.SANDSTONE, 3).setResultCount(6);
    public static final Recipe COBBLED_SANDSTONE_TILE_STAIRS = new Recipe().addItem(() -> AVABuildingBlocks.COBBLED_SANDSTONE_TILE.get().asItem(), 6).setResultCount(4);
    public static final Recipe GLASS_FENCE = new Recipe().addItem(Blocks.IRON_BARS).addItem(Blocks.GLASS).setResultCount(4);
    public static final Recipe GLASS_FENCE_TALL = new Recipe().addItem(Blocks.IRON_BARS).addItem(Blocks.GLASS, 2).setResultCount(4);
    public static final Recipe GLASS_WALL = new Recipe().addItem(Blocks.GLASS).setResultCount(2);
    public static final Recipe GLASS_TRIG_WALL = new Recipe().addItem(() -> AVABuildingBlocks.GLASS_WALL.get().asItem()).setResultCount(2);
    public static final Recipe IRON_GRID = new Recipe().addItem(Blocks.IRON_BARS);
    public static final Recipe HARDENED_IRON_GRID = new Recipe().addItem(Blocks.IRON_BARS, 2).addItem(AVAItemTagsProvider.WORK_HARDENED_IRON).setResultCount(2);

}
