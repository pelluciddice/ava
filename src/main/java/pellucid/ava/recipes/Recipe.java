package pellucid.ava.recipes;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import pellucid.ava.blocks.AVABlocks;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Recipe
{
    private final List<Pair<Supplier<Ingredient>, Integer>> ingredients = new ArrayList<>();
    private final List<String> description = new ArrayList<>();
    private int resultCount = 1;

    public Recipe()
    {
    }

    public static Item pickFromIngredient(Ingredient ingredient, Item defaultItem)
    {
        ItemStack[] ingredients = ingredient.getItems();
        return (ingredients.length == 0 || ingredient.isEmpty()) ? defaultItem : ingredients[0].getItem();
    }

    public boolean isEmpty()
    {
        return ingredients.stream().noneMatch((ingredient) -> ingredient.getA().get().getItems().length > 0 && ingredient.getB() > 0);
    }

    public Recipe addItem(Block item)
    {
        this.addItem(item, 1);
        return this;
    }

    public Recipe addItem(Block item, int count)
    {
        return addItem(item.asItem(), count);
    }

    public Recipe addItem(Item item)
    {
        this.addItem(item, 1);
        return this;
    }

    public Recipe addItem(Item item, int count)
    {
        return putIngredient(() -> Ingredient.of(item), count);
    }

    public Recipe addItem(Supplier<Item> item)
    {
        this.addItem(item, 1);
        return this;
    }

    public Recipe addItem(Supplier<Item> item, int count)
    {
        putIngredient(() -> Ingredient.of(item.get()), count);
        return this;
    }

    public Recipe addItem(TagKey<Item> item)
    {
        this.addItem(item, 1);
        return this;
    }

    public Recipe addItem(TagKey<Item> item, int count)
    {
        return putIngredient(() -> Ingredient.of(item), count);
    }

    private Recipe putIngredient(Supplier<Ingredient> ingredient, int count)
    {
        ingredients.add(Pair.of(ingredient, count));
        return this;
    }

    public Recipe mergeIngredients(Recipe recipe)
    {
        for (Pair<Supplier<Ingredient>, Integer> ingredient : recipe.ingredients)
            this.putIngredient(ingredient.getA(), ingredient.getB());
        if (recipe.hasDescription())
            for (String s : recipe.getDescription())
                addPlainDescription(s);
        return this;
    }

    public ItemStack getResultStack(Item result)
    {
        return new ItemStack(result, getResultCount());
    }

    public boolean canCraft(Player player, Item result)
    {
        return canCraft(player, result, 1);
    }

    public boolean canCraft(Player player, Item result, int count)
    {
        if (player.isCreative())
            return true;
        if (!player.level().isClientSide() && !isItemCraftable(result))
            return false;
        Inventory inventory = player.getInventory();
        for (Ingredient ingredient : getIngredients())
            if (getItemCount(inventory, ingredient) < getCount(ingredient) * count)
                return false;
        return true;
    }

    private boolean isItemCraftable(Item item)
    {
        return item != AVABlocks.AMMO_KIT_SUPPLIER.get().asItem() || AVAServerConfig.IS_AMMO_KIT_SUPPLIER_CRAFTABLE.get();
    }

    public void craft(ServerPlayer player, Item result)
    {
        craft(player, result, null);
    }

    public void craft(ServerPlayer player, Item result, @Nullable Predicate<ItemStack> toFind)
    {
        ItemStack found = consume(player, toFind);
        ItemStack stack = getResultStack(result);
        if (!found.isEmpty() && !found.isComponentsPatchEmpty())
            stack.applyComponents(found.getComponents());
        if (!player.getInventory().add(stack))
            player.getCommandSenderWorld().addFreshEntity(new ItemEntity(player.getCommandSenderWorld(), player.getX(), player.getY(), player.getZ(), stack));
    }

    public ItemStack consume(ServerPlayer player, @Nullable Predicate<ItemStack> toFind)
    {
        Inventory inventory = player.getInventory();
        ItemStack found = ItemStack.EMPTY;
        if (!player.isCreative())
        {
            List<ItemStack> remainder = new ArrayList<>();
            for (Ingredient ingredient : getIngredients())
            {
                int count = getCount(ingredient);
                while (count > 0)
                    for (int slot=0;slot<inventory.getContainerSize();slot++)
                    {
                        ItemStack stack = inventory.getItem(slot);
                        if (testIngredient(stack, ingredient))
                        {
                            if (toFind != null && toFind.test(stack))
                                found = stack.copy();
                            if (stack.getCount() >= count)
                            {
                                if (stack.hasCraftingRemainingItem())
                                    remainder.add(stack.getCraftingRemainingItem());
                                else
                                    stack.shrink(count);
                                count = 0;
                                break;
                            }
                            else
                            {
                                count -= stack.getCount();
                                inventory.removeItemNoUpdate(slot);
                            }
                        }
                    }
            }
            for (ItemStack stack : remainder)
                inventory.add(stack);
        }
        return found;
    }

    public boolean testIngredient(ItemStack stack, Ingredient ingredient)
    {
        return ingredient.test(stack) && stack.getCount() >= getCount(ingredient);
    }

    protected int getItemCount(Inventory inventory, Ingredient ingredient)
    {
        int count = 0;
        for (int slot=0;slot<inventory.getContainerSize();slot++)
        {
            ItemStack stack = inventory.getItem(slot);
            if (testIngredient(stack, ingredient))
                count += stack.getCount();
        }
        return count;
    }

    public List<Ingredient> getIngredients()
    {
        return AVACommonUtil.collect(this.ingredients, (pair) -> pair.getA().get());
    }

    public int getCount(Ingredient item)
    {
        return ingredients.stream().filter((ingredient) -> ingredient.getA().get().equals(item)).findFirst().orElse(Pair.of(null, 0)).getB();
    }

    public Recipe setResultCount(int count)
    {
        this.resultCount = count;
        return this;
    }

    public int getResultCount()
    {
        return this.resultCount;
    }

    public Recipe addDescription(String text)
    {
        return addPlainDescription("ava.gui.description." + text);
    }

    public Recipe addPlainDescription(String text)
    {
        this.description.add(text);
        return this;
    }

    public boolean hasDescription()
    {
        return !this.description.isEmpty();
    }

    public List<String> getDescription()
    {
        return this.description;
    }

    public MutableComponent getCombinedDescription()
    {
        MutableComponent text = Component.empty();
        for (String s : getDescription())
        {
            text.append(Component.translatable(s));
            text.append(" ");
        }
        return text;
    }

    public Recipe copy()
    {
        return copyOf(this);
    }

    public static Recipe copyOf(Recipe recipe)
    {
        Recipe newRecipe = new Recipe();
        for (Pair<Supplier<Ingredient>, Integer> ingredient : recipe.ingredients)
            newRecipe.putIngredient(ingredient.getA(), ingredient.getB());
        if (recipe.hasDescription())
            for (String description : recipe.getDescription())
                newRecipe.addDescription(description);
        newRecipe.setResultCount(recipe.getResultCount());
        return newRecipe;
    }
}
