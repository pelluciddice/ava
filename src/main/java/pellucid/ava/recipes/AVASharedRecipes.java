package pellucid.ava.recipes;

import net.minecraft.world.item.Items;
import net.neoforged.neoforge.common.Tags;
import pellucid.ava.items.init.Materials;

public class AVASharedRecipes
{
    public static final Recipe FLEUR_DE_LYS = skin().addItem(Items.LIGHT_GRAY_DYE, 4).addItem(Items.BLUE_DYE, 2).addItem(Items.YELLOW_DYE, 4);
    public static final Recipe SUMIRE = skin().addItem(Items.PURPLE_DYE, 5).addItem(Items.WHITE_DYE, 2).addItem(Items.YELLOW_DYE, 3);
    public static final Recipe DREAMCATCHER = skin().addItem(Items.BLACK_DYE, 7).addItem(Items.YELLOW_DYE, 7).addItem(Items.WHITE_DYE, 4);
    public static final Recipe FULLMOON = skin().addItem(Items.BROWN_DYE, 10).addItem(Items.YELLOW_DYE, 7);
    public static final Recipe FROST = skin().addItem(Items.WHITE_DYE, 7).addItem(Items.SNOW_BLOCK, 2);
    public static final Recipe SNOWFALL = skin().addItem(Items.WHITE_DYE, 5).addItem(Items.ICE);
    public static final Recipe AIR_WARFARE = skin().addItem(Items.LIGHT_BLUE_DYE, 7).addItem(Items.WHITE_DYE, 3);
    public static final Recipe KUYO_MON = skin().addItem(Items.BROWN_DYE, 7).addItem(Items.BLUE_DYE, 2).addItem(Items.GOLD_NUGGET, 2);
    public static final Recipe KNUT = skin().addItem(Items.LIGHT_BLUE_DYE, 5).addItem(Items.BLUE_DYE, 5).addItem(Items.WHITE_DYE, 5).addItem(Items.SNOWBALL, 4);
    public static final Recipe OVERRIDER = skin().addItem(Items.GOLD_INGOT, 5).addItem(Items.DIAMOND);
    public static final Recipe XPLORER = skin().addItem(Items.EMERALD, 35).addItem(Items.IRON_INGOT, 20);
    public static final Recipe AUBE = skin().addItem(Items.PACKED_ICE, 5).addItem(Items.SNOW_BLOCK, 5).addItem(Items.BLACK_DYE, 5);
    public static final Recipe BALD_EAGLE = skin().addItem(Items.IRON_INGOT, 5).addItem(Items.WHITE_DYE, 4).addItem(Items.LIGHT_GRAY_DYE, 4);
    public static final Recipe BLACK_WIDOW = skin().addItem(Items.DIAMOND, 1).addItem(Items.COBWEB, 3).addItem(Items.IRON_INGOT, 3);
    public static final Recipe UNIT_01 = skin().addItem(Items.GOLD_INGOT, 5).addItem(Items.PURPLE_DYE, 4).addItem(Items.LIME_DYE, 3);
    public static final Recipe FROST_SNOW = skin().addItem(Items.DIAMOND, 2).addItem(Items.GOLD_INGOT, 7).addItem(Items.BLUE_ICE, 5);
    public static final Recipe SPORTS = skin().addItem(Items.DIAMOND).addItem(Items.COAL, 5);
    public static final Recipe BARBATOS = skin().addItem(Items.REDSTONE, 20).addItem(Items.COAL, 5);
    public static final Recipe GOLD = skin().addItem(Items.GOLD_INGOT, 6).addItem(Items.GOLD_NUGGET, 5).addItem(Items.COAL, 5);
    public static final Recipe CHRISTMAS = skin().addItem(Items.LIME_DYE, 6).addItem(Items.RED_DYE, 4).addItem(Items.SWEET_BERRIES, 4);
    public static final Recipe COTTON_CANDY = skin().addItem(Items.WHITE_DYE, 6).addItem(Items.PINK_DYE, 4).addItem(Items.SUGAR, 4);
    public static final Recipe RED_SECTOR = skin().addItem(Items.GLOWSTONE_DUST, 8).addItem(Items.ORANGE_DYE, 8).addItem(Items.COAL, 4);
    public static final Recipe SKILLED = skin().addItem(Items.REDSTONE, 2).addItem(Items.GOLD_INGOT, 4).addItem(Items.COAL, 4);
    public static final Recipe LIGHT = skin().addItem(Items.BLUE_DYE, 2).addItem(Items.WHITE_DYE, 4).addItem(Items.LIGHT_BLUE_DYE, 8);
    public static final Recipe VALKYRIE = skin().addItem(Items.GLOWSTONE_DUST, 4).addItem(Items.ORANGE_DYE, 8);
    public static final Recipe COSMIC = skin().addItem(Items.QUARTZ, 12).addItem(Items.GLOWSTONE_DUST, 4).addItem(Items.LIGHT_BLUE_DYE, 8);
    public static final Recipe PREDATOR = skin().addItem(Items.CLAY_BALL, 24).addItem(Items.GRAY_DYE, 6).addItem(Items.LIGHT_GRAY_DYE, 6);
    public static final Recipe ARGENTO = skin().addItem(Items.REDSTONE, 2).addItem(Items.IRON_INGOT, 8).addItem(Items.INK_SAC, 6);
    public static final Recipe SILVER = skin().addItem(Items.IRON_INGOT, 16).addItem(Items.SAND, 8);
    public static final Recipe BLACK = skin().addItem(Items.COAL, 16).addItem(Items.GRAY_DYE, 8);
    public static final Recipe THE_ARGUS = skin().addItem(Items.WHITE_DYE, 12).addItem(Items.BLACK_DYE, 8).addItem(Items.LIGHT_BLUE_DYE, 8);
    public static final Recipe R_LAB = skin().addItem(Tags.Items.INGOTS_COPPER, 10).addItem(Items.BLACK_DYE, 8);
    public static final Recipe BLITZ = skin().addItem(Tags.Items.INGOTS_COPPER, 10).addItem(Items.BLACK_DYE, 8);

    private static Recipe skin()
    {
        return new Recipe().addItem(Materials.ACETONE_SOLUTION);
    }
}
