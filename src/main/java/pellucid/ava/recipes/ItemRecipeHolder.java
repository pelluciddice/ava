package pellucid.ava.recipes;

import net.minecraft.world.item.Item;
import pellucid.ava.util.Lazy;

import java.util.function.Supplier;

public class ItemRecipeHolder implements IHasRecipe
{
    private final Lazy<Item> item;
    private final Recipe recipe;

    public ItemRecipeHolder(Supplier<Item> item, Recipe recipe)
    {
        this.item = Lazy.of(item);
        this.recipe = recipe;
    }

    public Item getItem()
    {
        return item.get();
    }

    @Override
    public Recipe getRecipe()
    {
        return recipe;
    }

    @Override
    public Item asItem()
    {
        return getItem();
    }
}
