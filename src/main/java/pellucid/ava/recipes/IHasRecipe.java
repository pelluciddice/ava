package pellucid.ava.recipes;

import net.minecraft.world.level.ItemLike;

public interface IHasRecipe extends ItemLike
{
    Recipe getRecipe();
}
