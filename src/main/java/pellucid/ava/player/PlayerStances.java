package pellucid.ava.player;

import net.minecraft.world.entity.player.Player;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAWeaponUtil;

import javax.annotation.Nullable;

import static pellucid.ava.gun.stats.GunStatTypes.*;

public enum PlayerStances
{
    STANDING(ACCURACY, ACCURACY_AIM, INITIAL_ACCURACY, INITIAL_ACCURACY_AIM, RECOIL_COMPENSATION, RECOIL_COMPENSATION_AIM, STABILITY, STABILITY_AIM),
    CROUCHING(ACCURACY_CROUCH, ACCURACY_AIM_CROUCH, INITIAL_ACCURACY_CROUCH, INITIAL_ACCURACY_AIM_CROUCH, RECOIL_COMPENSATION_CROUCH, RECOIL_COMPENSATION_AIM_CROUCH, STABILITY_CROUCH, STABILITY_AIM_CROUCH),
    MOVING(ACCURACY_MOVING, ACCURACY_AIM_MOVING, INITIAL_ACCURACY_MOVING, INITIAL_ACCURACY_AIM_MOVING, RECOIL_COMPENSATION_MOVING, RECOIL_COMPENSATION_AIM_MOVING, STABILITY_MOVING, STABILITY_AIM_MOVING),
    JUMPING(ACCURACY_JUMPING, ACCURACY_AIM_JUMPING, INITIAL_ACCURACY_JUMPING, INITIAL_ACCURACY_AIM_JUMPING, RECOIL_COMPENSATION_JUMPING, RECOIL_COMPENSATION_AIM_JUMPING, STABILITY_JUMPING, STABILITY_AIM_JUMPING),

    ;
    private final GunStatTypes accuracy;
    private final GunStatTypes accuracyAim;
    private final GunStatTypes initAccuracy;
    private final GunStatTypes initAccuracyAim;
    private final GunStatTypes recoilCompensation;
    private final GunStatTypes recoilAimingCompensation;
    private final GunStatTypes stability;
    private final GunStatTypes stabilityAim;

    PlayerStances(GunStatTypes accuracy, GunStatTypes accuracyAim, GunStatTypes initAccuracy, GunStatTypes initAccuracyAim, GunStatTypes recoilCompensation, GunStatTypes recoilAimingCompensation, GunStatTypes stability, GunStatTypes stabilityAim)
    {
        this.accuracy = accuracy;
        this.accuracyAim = accuracyAim;
        this.initAccuracy = initAccuracy;
        this.initAccuracyAim = initAccuracyAim;
        this.recoilCompensation = recoilCompensation;
        this.recoilAimingCompensation = recoilAimingCompensation;
        this.stability = stability;
        this.stabilityAim = stabilityAim;
    }

    public static GunStatTypes accuracyOf(@Nullable Player player)
    {
        return of(player).accuracy;
    }

    public static GunStatTypes aimAccuracyOf(@Nullable Player player)
    {
        return of(player).accuracyAim;
    }

    public static GunStatTypes initialAccuracyOf(@Nullable Player player)
    {
        return of(player).initAccuracy;
    }

    public static GunStatTypes initialAimAccuracyOf(@Nullable Player player)
    {
        return of(player).initAccuracyAim;
    }

    public static GunStatTypes recoilCompensationOf(@Nullable Player player)
    {
        return of(player).recoilCompensation;
    }

    public static GunStatTypes aimRecoilCompensationOf(@Nullable Player player)
    {
        return of(player).recoilAimingCompensation;
    }

    public static GunStatTypes stabilityOf(@Nullable Player player)
    {
        return of(player).stability;
    }

    public static GunStatTypes aimStabilityOf(@Nullable Player player)
    {
        return of(player).stabilityAim;
    }

    public static PlayerStances of(@Nullable Player player)
    {
        if (player != null)
        {
            if (!AVAWeaponUtil.isOnGroundNotStrict(player))
                return JUMPING;
            if (player.isCrouching())
                return CROUCHING;
            if (player.level().isClientSide ? AVACommonUtil.isMovingOnGroundClient(player) : AVACommonUtil.isMovingOnGroundServer(player))
                return MOVING;
        }
        return STANDING;
    }
}
