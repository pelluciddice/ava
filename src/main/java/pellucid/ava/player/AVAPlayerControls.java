package pellucid.ava.player;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.DistExecutor;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.event.entity.living.LivingDamageEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;
import net.neoforged.neoforge.event.tick.PlayerTickEvent;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.AVADamageSources;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.items.functionalities.AVAAnimatedItem;
import pellucid.ava.items.functionalities.ICustomModel;
import pellucid.ava.packets.DataToClientMessage;
import pellucid.ava.packets.LivingDamageMessage;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

import static pellucid.ava.util.AVACommonUtil.*;

@EventBusSubscriber(modid = AVA.MODID)
public class AVAPlayerControls
{
    @SubscribeEvent
    public static void onPlayerLeftClickBlock(PlayerInteractEvent.LeftClickBlock event)
    {
        Item item = event.getEntity().getMainHandItem().getItem();
        if (item instanceof ICustomModel)
            event.setCanceled(true);
    }

    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event)
    {
        if (event.getEntity() instanceof ServerPlayer player)
        {
            syncCrossWorldCapWithClient(player);
            syncWorldCapWithClient(player);
            syncGunStatsWithClient(player);
            syncPlayerCapWithClient(player);
            DataToClientMessage.serverConfig(player);
            DataToClientMessage.scoreboardMap(player);
            for (Timer timer : AVACrossWorldData.getInstance().timers)
                AVAWeaponUtil.trackScoreboard(ServerLifecycleHooks.getCurrentServer(), timer.getStorage());
        }
    }

    @SubscribeEvent
    public static void onPlayerChangedDimension(PlayerEvent.PlayerChangedDimensionEvent event)
    {
        if (event.getEntity() instanceof ServerPlayer player && player.level() instanceof ServerLevel)
            syncWorldCapWithClient(player);
    }

    @SubscribeEvent
    public static void onPlayerTick(PlayerTickEvent.Pre event)
    {
        Player player = event.getEntity();

        if (!player.isAlive())
            return;
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            if (event.getEntity() == Minecraft.getInstance().player)
                AVAClientUtil.clientPlayerTick(event);
        });
        CompoundTag playerData = player.getPersistentData();
        playerData.putInt(AVAConstants.TAG_ENTITY_AIRBORNE, player.onGround() ? 0 : (playerData.getInt(AVAConstants.TAG_ENTITY_AIRBORNE) + 1));
        PlayerAction capability = cap(player);
        if (!player.level().isClientSide())
        {
            AttributeInstance attribute = player.getAttribute(Attributes.GRAVITY);
            boolean usingParachute = capability.getUsingParachute();
            if (usingParachute)
            {
                player.fallDistance /= 1.15F;
                player.fallDistance -= 1.25F;
            }
            if (player.onGround())
            {
                if (usingParachute)
                {
                    capability.setIsUsingParachute(player, false);
                    if (attribute.hasModifier(AVAConstants.SLOW_FALLING_MODIFIER))
                        attribute.removeModifier(AVAConstants.SLOW_FALLING_MODIFIER.id());
                }
            }
            else if (!attribute.hasModifier(AVAConstants.SLOW_FALLING_MODIFIER) && usingParachute)
                attribute.addTransientModifier(AVAConstants.SLOW_FALLING_MODIFIER);
        }
        ItemStack stack = getHeldStack(player);
        Level world = player.level();
        if (world.isClientSide && PlayerAction.getCap(player).isADS())
        {
            if (player.isSprinting())
                player.setSprinting(false);
            if (!(stack.getItem() instanceof AVAAnimatedItem))
                PlayerAction.getCap(player).setIsADS(world, false);
        }
        if (isFullEquipped(player) && !world.isClientSide)
        {
            capability.setStepSoundCooldown(capability.getStepSoundCooldown() - 1);
            if (isMovingOnGroundServer(player) && !player.isShiftKeyDown() && capability.getStepSoundCooldown() <= 0)
            {
                SoundEvent sound = AVACommonUtil.getStepSoundFor(player, true);
                if (sound != null)
                {
                    AVAWeaponUtil.playAttenuableSoundToClientMoving(sound, player, 1.3F);
                    capability.setStepSoundCooldown(player.isSprinting() ? 5 : 8);
                }
            }
        }
        if (!world.isClientSide)
            AVAWeaponUtil.calculateRecoilRotations(player, capability, stack);
        int healthBoost = capability.getHealthBoost() * 3;
        if (!player.level().isClientSide())
        {
            boolean shouldApply = false;
            if (hasHealthBoostModifier(player) && getHealthBoostModifier(player).amount() != healthBoost)
            {
                removeHealthBoostModifier(player);
                shouldApply = true;
            }
            else if (!hasHealthBoostModifier(player) && healthBoost != 0)
                shouldApply = true;
            if (shouldApply)
                applyHealthBoostModifier(player, createHealthBoostModifier(healthBoost));
        }
        DataTypes.DOUBLE.write(playerData, "lastposx", player.getX());
        DataTypes.DOUBLE.write(playerData, "lastposy", player.getY());
        DataTypes.DOUBLE.write(playerData, "lastposz", player.getZ());
    }

    @SubscribeEvent
    public static void onLivingDeath(LivingDeathEvent event)
    {
        LivingEntity target = event.getEntity();
        DamageSource source = event.getSource();
        Entity attacker = source.getEntity();
        if (attacker instanceof LivingEntity && source instanceof AVADamageSources.IAVAWeaponDamageSource)
            PacketDistributor.sendToAllPlayers(new LivingDamageMessage(attacker.getId(), LivingDamageMessage.DEATH, isAVADamageSource(source) ? ((AVADamageSources.IAVAWeaponDamageSource) source).getWeapon() : null, target.getId()));
        if (!target.level().isClientSide() && source.is(AVADamageSources.SYSTEM_REMOVE))
        {
            AVACrossWorldData data = AVACrossWorldData.getInstance();
            boolean isTargetPlayer = target instanceof Player;
            if (isTargetPlayer)
            {
                PlayerAction cap = PlayerAction.getCap((Player) target);
                cap.setIsUsingParachute((Player) target, false);
            }
            if (isTargetPlayer || (target instanceof SidedSmartAIEntity && AVAServerConfig.isCompetitiveModeActivated()))
            {
                if (!AVAServerConfig.isCompetitiveModeActivated() || !AVAWeaponUtil.isSameSide(attacker, target))
                {
                    boolean changed = false;
                    if (attacker instanceof Player player)
                    {
                        data.scoreboardManager.getPlayerStat(player).addScore(1);
                        changed = true;
                    }
                    if (isTargetPlayer)
                    {
                        data.scoreboardManager.getPlayerStat((Player) target).addDeath();
                        changed = true;
                    }
                    if (changed)
                        DataToClientMessage.scoreboardScore();
                }
            }
        }
    }

    @SubscribeEvent
    public static void onPlayerHurt(LivingDamageEvent event)
    {
        LivingEntity target = event.getEntity();
        if (event.getSource() instanceof AVADamageSources.IAVAWeaponDamageSource)
        {
            Entity attacker = event.getSource().getEntity();
            if (target instanceof ServerPlayer player && attacker != null)
                PacketDistributor.sendToPlayer(player, new LivingDamageMessage(attacker.getId(), LivingDamageMessage.HURT, null, -1));
            if (attacker instanceof Player player && (target instanceof Player || (target instanceof SidedSmartAIEntity && AVAServerConfig.isCompetitiveModeActivated())) && attacker != target)
            {
                if (!AVAServerConfig.isCompetitiveModeActivated() || !AVAWeaponUtil.isSameSide(player, target))
                {
                    AVACrossWorldData.getInstance().scoreboardManager.getPlayerStat(player).addDamage(Math.min(target.getHealth(), event.getAmount()));
                    DataToClientMessage.scoreboardScore();
                }
            }
            if (attacker instanceof ServerPlayer player && attacker != target)
                PacketDistributor.sendToPlayer(player, new LivingDamageMessage(player.getId(), LivingDamageMessage.TARGET_HURT, null, target.getId()));
        }
    }

    public static void syncWorldCapWithClient(ServerPlayer player)
    {
        DataToClientMessage.world(player.serverLevel(), (p) -> PacketDistributor.sendToPlayer(player, p));
    }

    public static void syncCrossWorldCapWithClient(ServerPlayer player)
    {
        DataToClientMessage.crossWorld(player);
    }

    public static void syncGunStatsWithClient(ServerPlayer player)
    {
        DataToClientMessage.weaponStats((p) -> PacketDistributor.sendToPlayer(player, p));
    }

    public static void syncPlayerCapWithClient(ServerPlayer player)
    {
        DataToClientMessage.armourValue(player);
        DataToClientMessage.playerBoosts(player);
    }
}
