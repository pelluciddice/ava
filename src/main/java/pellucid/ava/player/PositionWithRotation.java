package pellucid.ava.player;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.phys.Vec3;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

public class PositionWithRotation
{
    private final Vec3 vec;
    private final int yaw;
    private final int pitch;
    private final int radius;

    public PositionWithRotation(CompoundTag compound)
    {
        this(AVAWeaponUtil.readVec(compound), DataTypes.INT.read(compound, "yaw"), DataTypes.INT.read(compound, "pitch"), DataTypes.INT.read(compound, "radius"));
    }

    public PositionWithRotation(Vec3 vec, int yaw, int pitch, int radius)
    {
        this.vec = vec;
        this.yaw = yaw;
        this.pitch = pitch;
        this.radius = radius;
    }

    public Vec3 getVec()
    {
        return vec;
    }

    public int getYaw()
    {
        return yaw;
    }

    public int getPitch()
    {
        return pitch;
    }

    public int getRadius()
    {
        return radius;
    }

    public CompoundTag serializeNBT()
    {
        CompoundTag compound = AVAWeaponUtil.writeVec(vec);
        DataTypes.INT.write(compound, "yaw", getYaw());
        DataTypes.INT.write(compound, "pitch", getPitch());
        DataTypes.INT.write(compound, "radius", getRadius());
        return compound;
    }

    @Override
    public String toString()
    {
        return "PositionWithRotation{" +
                "vec=" + vec +
                ", yaw=" + yaw +
                ", pitch=" + pitch +
                ", radius=" + radius +
                '}';
    }
}
