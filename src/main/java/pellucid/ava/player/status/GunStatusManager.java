package pellucid.ava.player.status;

import net.minecraft.core.component.DataComponents;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.Loop;

import static pellucid.ava.events.ClientModEventBus.INSTALL_SILENCER;
import static pellucid.ava.events.ClientModEventBus.RELOAD;

public class GunStatusManager extends ItemStatusManager<AVAItemGun>
{
    public static final GunStatusManager INSTANCE = new GunStatusManager();

    public Loop zoomLevel = new Loop(2);

    public boolean pendingReload = false;

    public float fireInterval = 0.0F;
    public int idle = 0;

    public GunStatusManager()
    {
        super(() -> AVAModelTypes.GUNS);
    }

    @Override
    public void tickItem(AVAItemGun item, Entity entity, ItemStack stack, Level world, int itemSlot, boolean isSelected)
    {
        super.tickItem(item, entity, stack, world, itemSlot, isSelected);
        if (!isSelected)
        {
            stack.set(AVADataComponents.TAG_ITEM_RELOAD, 0);
            stack.set(AVADataComponents.TAG_ITEM_DRAW, 0);
            stack.set(AVADataComponents.TAG_ITEM_TICKS, 0.0F);
            stack.set(AVADataComponents.TAG_ITEM_FIRE, 0);
            stack.set(AVADataComponents.TAG_ITEM_IDLE, 0);
            stack.set(AVADataComponents.TAG_ITEM_SILENCER_INSTALL, 0);
        }
        else
        {
            countDown(stack, AVADataComponents.TAG_ITEM_RELOAD);
            countDown(stack, AVADataComponents.TAG_ITEM_FIRE);
            countDown(stack, AVADataComponents.TAG_ITEM_DRAW);
            countDown(stack, AVADataComponents.TAG_ITEM_IDLE);
            countDown(stack, AVADataComponents.TAG_ITEM_SILENCER_INSTALL);
            countDown(stack, AVADataComponents.TAG_ITEM_PRE_RELOAD);
            countDown(stack, AVADataComponents.TAG_ITEM_POST_RELOAD);
            if (stack.has(DataComponents.DAMAGE))
                stack.remove(DataComponents.DAMAGE);
        }
    }

    @Override
    protected Runnable onComplete(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (status == GunStatus.ADS_IN)
            PlayerAction.getCap(player).setIsADS(player.level(), true);
        else if (status == GunStatus.INSTALL_SILENCER || status == GunStatus.UNINSTALL_SILENCER)
        {
            PlayerActionMessage.silencer();
            if (status == GunStatus.INSTALL_SILENCER)
                stack.set(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, true);
        }
        else if (status == GunStatus.RELOAD)
        {
            gun.reload(player, stack, AVAServerConfig.isCompetitiveModeActivated());
            PlayerActionMessage.reload();
            if (gun.hasExtraReloadSteps(stack))
            {
                if (gun.reloadable(player, stack, AVAServerConfig.isCompetitiveModeActivated()) && stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) < gun.getCapacity(stack, true))
                    return () ->
                    {
                        start(player, gun, stack, GunStatus.RELOAD, gun.getReloadTime(stack));
                    };
                else
                {
                    return () ->
                    {
                        start(player, gun, stack, GunStatus.RELOAD_POST, gun.getPostReloadTime(stack));
                    };
                }
            }
        }
        else if (status == GunStatus.RELOAD_PRE)
        {
            return () ->
            {
                start(player, gun, stack, GunStatus.RELOAD, gun.getReloadTime(stack));
            };
        }
        return null;
    }

    @Override
    protected void tick(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (isActive(GunStatus.DRAW))
            playSound(player, gun.getDrawSound(stack), progress);
        else if (isActive(GunStatus.FIRE))
            playSound(player, gun.getShootSound(stack), progress, 1.0F, 0.85F + RANDOM.nextFloat() * 0.3F);
        else if (isActive(GunStatus.RELOAD_PRE))
            playSound(player, gun.getPreReloadSound(stack), progress);
        else if (isActive(GunStatus.RELOAD))
            playSound(player, gun.getReloadSound(stack), progress);
        else if (isActive(GunStatus.RELOAD_POST))
            playSound(player, gun.getPostReloadSound(stack), progress);

        super.tick(player, gun, stack);
        if (isActive(GunStatus.ADS_IN, GunStatus.ADS_OUT))
        {
            run = 0;
            walk = 0;
            steady = 0;
        }
        if (matchesID(stack, id))
        {
            if (fireInterval > 0)
                fireInterval--;
            if (idle > 0)
                idle--;
            if (fireInterval > 0 && gun.isAuto(stack))
                idle = 2;

            if (pendingReload)
                tryReload(player, gun, stack);

            if (status == GunStatus.UNINSTALL_SILENCER && progress == 14)
                stack.set(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, false);
        }
        else
        {
            clear(player);
            if (isRightItem(stack))
            {
                id = getID(stack);
                tryDraw(player, gun, stack);
            }
        }
    }

    public boolean tryFire(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(GunStatus.ADS_IN, GunStatus.ADS_OUT, GunStatus.DRAW, GunStatus.UNINSTALL_SILENCER, GunStatus.INSTALL_SILENCER, GunStatus.RELOAD_PRE, GunStatus.RELOAD_POST, GunStatus.MELEE))
                return false;
            if (isReloading() && !gun.isReloadInteractable(stack))
                return false;
            if (gun.firable(player, stack))
            {
                if (fireInterval <= 0)
                {
                    PlayerActionMessage.fire();
                    gun.fire(player.getCommandSenderWorld(), player, stack,
                            (cap, p, s, m) -> AVAItemGun.recoil(cap, p, s, m, (p2, pitch) -> AVAClientUtil.rotateTowards(player, 0.0F, pitch)),
                            (cap, p, s, m) -> AVAItemGun.shake(cap, p, s, m, (p2, yaw) -> AVAClientUtil.rotateTowards(player, yaw, 0.0F)), AVAServerConfig.isCompetitiveModeActivated()
                    );
                    if (AVAServerConfig.STABLE_FIRERATE.get())
                        GunStatusManager.INSTANCE.fireInterval = 20.0F / gun.getFireRate(stack, true);
                    else
                        GunStatusManager.INSTANCE.fireInterval += 20.0F / gun.getFireRate(stack, true);
                    idle = 2;
                    start(player, gun, stack, GunStatus.FIRE, gun.getFireAnimation(stack));

                    if (gun.exitAimOnFire(stack))
                        zoomLevel.set(0);

                    if (stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) <= 1)
                        pendingReload = true;

                    return true;
                }
            }
        }
        return false;
    }

    public boolean tryDraw(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            PlayerAction.getCap(player).setIsADS(player.level(), false);
            start(player, gun, stack, GunStatus.DRAW, gun.getDrawSpeed(stack, true));
            if (stack.getOrDefault(AVADataComponents.TAG_ITEM_AMMO, 0) <= 0)
                pendingReload = true;
            return true;
        }
        return false;
    }

    public boolean tryADS(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            AVAItemGun.IScopeType scope = gun.getScopeType(stack, true);
            if (scope.noAim(stack))
                return false;
            if (scope.isIronSight())
                PlayerAction.getCap(player).setIsADS(player.level(), true);
            else if (!scope.isDoubleZoom() || zoomLevel.current() != 2)
                start(player, gun, stack, GunStatus.ADS_IN, gun.getAimTime(stack, true));
            playSound(player, AVASoundTracks.AIM, 0);
            return true;
        }
        return false;
    }

    public boolean tryExitADS(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (!isADS(player))
                return false;
            AVAItemGun.IScopeType scope = gun.getScopeType(stack, true);
            if (scope.noAim(stack))
                return false;
            if (!scope.isIronSight())
                start(player, gun, stack, GunStatus.ADS_OUT, gun.getAimTime(stack, true));
            zoomLevel.set(0);
            PlayerAction.getCap(player).setIsADS(player.level(), false);
            playSound(player, AVASoundTracks.AIM);
            return true;
        }
        return false;
    }

    public boolean tryReload(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(GunStatus.ADS_IN, GunStatus.ADS_OUT, GunStatus.RELOAD, GunStatus.DRAW, GunStatus.FIRE, GunStatus.UNINSTALL_SILENCER, GunStatus.INSTALL_SILENCER, GunStatus.RELOAD, GunStatus.RELOAD_PRE, GunStatus.RELOAD_POST, GunStatus.MELEE))
                return false;
            if (gun.reloadable(player, stack, AVAServerConfig.isCompetitiveModeActivated()))
            {
                if (isADS(player))
                {
                    tryExitADS(player, gun, stack);
                    pendingReload = true;
                    return false;
                }
                if (gun.hasExtraReloadSteps(stack))
                {
                    start(player, gun, stack, GunStatus.RELOAD_PRE, gun.getPreReloadTime(stack));
                    if (AVACommonUtil.isFullEquipped(player) && player.getRandom().nextFloat() <= 0.25F)
                        AVAClientUtil.playSoundOnServer(true, AVAWeaponUtil.eitherSound(player, AVASounds.RELOAD_EU.get(), AVASounds.RELOAD_NRF.get()));
                }
                else
                {
                    start(player, gun, stack, GunStatus.RELOAD, gun.getReloadTime(stack));
                }
                pendingReload = false;
            }
        }
        return false;
    }

    public boolean tryInstallSilencer(Player player, AVAItemGun gun, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (!gun.hasOptionalSilencer(stack, true) || isADS(player) || isActive(GunStatus.ADS_IN, GunStatus.ADS_OUT, GunStatus.DRAW, GunStatus.FIRE, GunStatus.UNINSTALL_SILENCER, GunStatus.INSTALL_SILENCER, GunStatus.RELOAD, GunStatus.RELOAD_PRE, GunStatus.RELOAD_POST, GunStatus.MELEE))
                return false;
            boolean installed = stack.getOrDefault(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, false);
            start(player, gun, stack, installed ? GunStatus.UNINSTALL_SILENCER : GunStatus.INSTALL_SILENCER, AVAConstants.SILENCER_INSTALL_TIME);
            AVAClientUtil.playSoundOnServer(true, (installed ? AVASounds.SILENCER_UNINSTALLS : AVASounds.SILENCER_INSTALLS).get());
            return true;
        }
        return false;
    }

    @Override
    public void start(Player player, AVAItemGun item, ItemStack stack, IStatus status, int duration)
    {
        super.start(player, item, stack, status, duration);
        if (status == GunStatus.RELOAD_PRE)
            PlayerActionMessage.statusReload();
        else if (status == GunStatus.RELOAD)
            PlayerActionMessage.statusPreReload();
        else if (status == GunStatus.RELOAD_POST)
            PlayerActionMessage.statusPostReload();
        else if (status == GunStatus.DRAW)
            PlayerActionMessage.statusDraw();
    }

    public boolean isReloading()
    {
        return isActive(GunStatus.RELOAD, GunStatus.RELOAD_PRE, GunStatus.RELOAD_POST);
    }

    @Override
    protected void clear(Player player)
    {
        super.clear(player);
        zoomLevel.set(0);
        fireInterval = 0;
        idle = 0;
        pendingReload = false;
    }

    @Override
    protected GunInput createInputManager()
    {
        return new GunInput();
    }

    public class GunInput extends Input<AVAItemGun, GunStatusManager>
    {
        public GunInput()
        {
            super(INSTANCE);
        }

        @Override
        public void renderTick(Player player, AVAItemGun gun, ItemStack stack)
        {
            while (RELOAD.consumeClick())
                tryReload(player, gun, stack);
            while (INSTALL_SILENCER.consumeClick())
                tryInstallSilencer(player, gun, stack);
        }

        @Override
        public void whileLeftDown(Player player, AVAItemGun gun, ItemStack stack)
        {
            if (gun.isAuto(stack))
                tryFire(player, gun, stack);
        }

        @Override
        public boolean onLeftClick(Player player, AVAItemGun gun, ItemStack stack)
        {
            return !gun.isAuto(stack) && tryFire(player, gun, stack);
        }

        @Override
        public boolean onRightClick(Player player, AVAItemGun gun, ItemStack stack)
        {
            if (isActive(GunStatus.ADS_IN, GunStatus.ADS_OUT, GunStatus.DRAW, GunStatus.FIRE, GunStatus.UNINSTALL_SILENCER, GunStatus.INSTALL_SILENCER, GunStatus.RELOAD_PRE, GunStatus.RELOAD_POST, GunStatus.MELEE))
                return false;
            if (isReloading() && !gun.isReloadInteractable(stack))
                return false;
            AVAItemGun.IScopeType scope = gun.getScopeType(stack, true);
            if (scope.noAim(stack))
                return false;
            int level = zoomLevel.next();
            if (level == 2)
            {
                if (!gun.getScopeType(stack, true).isDoubleZoom())
                {
                    zoomLevel.set(0);
                    level = 0;
                }
            }
            if (level == 1 || level == 2)
                return tryADS(player, gun, stack);
            else if (level == 0)
                return tryExitADS(player, gun, stack);
            return false;
        }

        @Override
        public void tick(Player player)
        {
            if (isActive(GunStatus.ADS_IN, GunStatus.ADS_OUT, GunStatus.DRAW, GunStatus.FIRE, GunStatus.UNINSTALL_SILENCER, GunStatus.INSTALL_SILENCER, GunStatus.RELOAD, GunStatus.RELOAD_PRE, GunStatus.RELOAD_POST, GunStatus.MELEE) && player.isSprinting())
                cancelSprint(player);
        }
    }

    public enum GunStatus implements IStatus
    {
        ADS_IN,
        ADS_OUT,
        DRAW,
        FIRE,
        INSTALL_SILENCER,
        UNINSTALL_SILENCER,
        RELOAD,
        RELOAD_PRE,
        RELOAD_POST,
        MELEE,

        IDLE,
        RUN,
        WALK,
        ;
    }
}
