package pellucid.ava.player.status;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.util.AVAConstants;

import static pellucid.ava.util.AVAConstants.*;

public class ProjectileStatusManager extends ItemStatusManager<ThrowableItem>
{
    public static final ProjectileStatusManager INSTANCE = new ProjectileStatusManager();

    public boolean cooking = false;
    public boolean left = false;
    public boolean holdingLeft = false;
    public boolean holdingRight = false;

    public ProjectileStatusManager()
    {
        super(() -> AVAModelTypes.PROJECTILE);
    }

    @Override
    protected Runnable onComplete(Player player, ThrowableItem grenade, ItemStack stack)
    {
        if (isActive(ProjectileStatus.PIN_OFF))
            cooking = true;
        return null;
    }

    @Override
    protected void tickItem(ThrowableItem item, Entity player, ItemStack stack, Level world, int itemSlot, boolean isSelected)
    {
        super.tickItem(item, player, stack, world, itemSlot, isSelected);
        countDown(stack, AVADataComponents.TAG_ITEM_FIRE);
        if (stack.has(AVADataComponents.TAG_ITEM_FIRE) && stack.getOrDefault(AVADataComponents.TAG_ITEM_FIRE, 0) == 0)
        {
            if (player instanceof Player && !((Player) player).isCreative())
                stack.shrink(1);
        }
    }

    @Override
    protected void tick(Player player, ThrowableItem grenade, ItemStack stack)
    {
        if (isActive(ProjectileStatus.DRAW))
            playSound(player, AVASoundTracks.SR_2M_VERESK_DRAW, progress);
        else if (isActive(ProjectileStatus.PIN_OFF))
            playSound(player, AVASoundTracks.PIN_OFF, progress);

        super.tick(player, grenade, stack);
        if (matchesID(stack, id))
        {
            if (isActive(ProjectileStatus.TOSS, ProjectileStatus.THROW) && progress == 1)
            {
                PlayerActionMessage.tossGrenade(status != ProjectileStatus.THROW);
                stack.set(AVADataComponents.TAG_ITEM_FIRE, AVAConstants.PROJECTILE_THROW_TIME - 1);
            }
        }
        else
        {
            clear(player);
            if (isRightItem(stack))
            {
                id = getID(stack);
                tryDraw(player, grenade, stack);
            }
        }
    }

    @Override
    protected void clear(Player player)
    {
        super.clear(player);
        cooking = false;
    }

    public boolean tryChuck(Player player, ThrowableItem grenade, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(ProjectileStatus.DRAW, ProjectileStatus.PIN_OFF, ProjectileStatus.TOSS, ProjectileStatus.THROW))
                return false;
            if (!cooking)
                return false;
            this.cooking = false;
            start(player, grenade, stack, left ? ProjectileStatus.THROW : ProjectileStatus.TOSS, left ? PROJECTILE_THROW_TIME : PROJECTILE_TOSS_TIME);
            return true;
        }
        return false;
    }

    public boolean tryPinOff(Player player, ThrowableItem grenade, ItemStack stack, boolean left)
    {
        if (matchesID(stack, id))
        {
            if (isActive(ProjectileStatus.DRAW, ProjectileStatus.PIN_OFF, ProjectileStatus.TOSS, ProjectileStatus.THROW))
                return false;
            if (cooking)
                return false;
            this.left = left;
            start(player, grenade, stack, ProjectileStatus.PIN_OFF, PROJECTILE_PIN_OFF_TIME);
            return true;
        }
        return false;
    }

    public boolean tryDraw(Player player, ThrowableItem grenade, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            start(player, grenade, stack, ProjectileStatus.DRAW, PROJECTILE_DRAW_TIME);
            return true;
        }
        return false;
    }

    @Override
    protected ProjectileInput createInputManager()
    {
        return new ProjectileInput();
    }

    public static class ProjectileInput extends Input<ThrowableItem, ProjectileStatusManager>
    {
        public ProjectileInput()
        {
            super(INSTANCE);
        }

        @Override
        public void whileLeftDown(Player player, ThrowableItem grenade, ItemStack stack)
        {
            manager.holdingLeft = true;
            manager.tryPinOff(player, grenade, stack, true);
        }

        @Override
        public void whileRightDown(Player player, ThrowableItem grenade, ItemStack stack)
        {
            manager.holdingRight = true;
            manager.tryPinOff(player, grenade, stack, false);
        }

        @Override
        public void whileLeftUp(Player player, ThrowableItem grenade, ItemStack stack)
        {
            if (manager.cooking)
            {
                if (manager.holdingLeft)
                    manager.tryChuck(player, grenade, stack);
                manager.holdingLeft = false;
            }
        }

        @Override
        public void whileRightUp(Player player, ThrowableItem grenade, ItemStack stack)
        {
            if (manager.cooking)
            {
                if (manager.holdingRight)
                    manager.tryChuck(player, grenade, stack);
                manager.holdingRight = false;
            }
        }

        @Override
        public void tick(Player player)
        {
            if (manager.cooking || manager.isActive(ProjectileStatus.DRAW, ProjectileStatus.PIN_OFF, ProjectileStatus.TOSS, ProjectileStatus.THROW))
                cancelSprint(player);
        }
    }

    public enum ProjectileStatus implements IStatus
    {
        DRAW,
        IDLE,
        RUN,
        PIN_OFF,
        THROW,
        TOSS,
        ;
    }
}
