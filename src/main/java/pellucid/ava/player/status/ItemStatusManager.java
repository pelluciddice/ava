package pellucid.ava.player.status;

import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.core.component.DataComponentType;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import net.neoforged.neoforge.client.event.InputEvent;
import org.lwjgl.glfw.GLFW;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.sounds.SoundTrack;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.Lazy;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

public abstract class ItemStatusManager<I extends Item>
{
    public static final List<ItemStatusManager<?>> MANAGERS = new ArrayList<>();
    public final Lazy<AVAModelTypes<?, ?, ?, ?, ?, ?>> type;
    public UUID id;
    public IStatus status = null;
    protected int progress;
    protected int duration;

    public int steady = 0;
    public int run = 0;
    public int walk = 0;

    public ItemStatusManager(Supplier<AVAModelTypes<?, ?, ?, ?, ?, ?>> type)
    {
        this.type = Lazy.of(type);
        MANAGERS.add(this);
    }

    public final void tick(Player player)
    {
        if (!AVAClientUtil.isFocused())
            return;
        ItemStack stack = player.getMainHandItem();
        if (!isRightItem(stack))
        {
            clear(player);
            return;
        }
        tick(player, (I) stack.getItem(), stack);
    }

    protected void playSound(Player player, SoundTrack sound)
    {
        playSound(player, sound, 0);
    }

    protected void playSound(Player player, SoundTrack sound, int ticks)
    {
        playSound(player, sound, ticks, 1.0F, 1.0F);
    }

    protected static final RandomSource RANDOM = RandomSource.create();
    protected void playSound(Player player, SoundTrack sound, int ticks, float volume, float pitch)
    {
        if (sound != null && sound.hasSoundAt(ticks))
        {
            SoundTrack.Track track = sound.get(ticks);
            player.level().playSound(player, player, track.sound.get(), player.getSoundSource(), volume, pitch);
            AVAClientUtil.playSoundOnServer(false, track.sound.get());
        }
    }

    protected void tick(Player player, I item, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (status != null)
            {
                if (progress >= duration)
                {
                    Runnable action = onComplete(player, item, stack);
                    reset();
                    if (action != null)
                        action.run();
                }
                else
                    progress++;
            }
        }
        if (AVACommonUtil.isMovingOnGroundClient(player))
        {
            if (player.isSprinting())
            {
                run++;
                walk = 0;
            }
            else
            {
                walk++;
                run = 0;
            }
            steady = 0;
        }
        else
            steady++;
    }

    protected void countDown(ItemStack stack, Supplier<DataComponentType<Integer>> key)
    {
        if (stack.has(key))
        {
            int value = stack.getOrDefault(key, 0);
            if (value > 0)
                stack.set(key, value - 1);
        }
    }

    protected abstract Runnable onComplete(Player player, I item, ItemStack stack);

    protected boolean isADS(Player player)
    {
        return PlayerAction.getCap(player).isADS();
    }

    protected void clear(Player player)
    {
        id = null;
        reset();
    }

    public void reset()
    {
        status = null;
        progress = 0;
        duration = 0;
    }

    public void start(Player player, I item, ItemStack stack, IStatus status, int duration)
    {
        this.id = getID(stack);
        this.status = status;
        this.progress = 0;
        this.duration = duration;
    }

    public int getProgress()
    {
        return progress;
    }

    public int getProgressFor(ItemStack stack, IStatus status)
    {
        return isActive(status) && matchesID(stack, id) ? progress : 0;
    }

    public float getProgressFactor()
    {
        return (float) progress / duration;
    }

    public boolean isActive()
    {
        return this.status != null;
    }

    public boolean isActive(IStatus... status)
    {
        return status != null && Arrays.stream(status).anyMatch((action) -> action == this.status);
    }

    @Nullable
    public UUID getID(ItemStack stack)
    {
        if (isRightItem(stack))
            return stack.get(AVADataComponents.TAG_ITEM_UUID.get());
        return null;
    }

    public boolean matchesID(ItemStack stack, ItemStack stack2)
    {
        return matchesID(stack, getID(stack2));
    }

    public boolean matchesID(ItemStack stack, @Nullable UUID uuid)
    {
        return matchesID(getID(stack), uuid);
    }

    public boolean matchesID(@Nullable UUID uuid, @Nullable UUID uuid2)
    {
        if (uuid == null)
            return false;
        return uuid.equals(uuid2);
    }

    public boolean isRightItem(ItemStack stack)
    {
        return isRightItem(stack.getItem());
    }

    public boolean isRightItem(Item item)
    {
        return type.get().isRightItem(item);
    }

    public final void tickItem(I i, ItemStack stack, Level worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if (!entityIn.isAlive())
            return;
        tickItem(i, entityIn, stack, worldIn, itemSlot, isSelected);
    }

    protected void  tickItem(I item, Entity player, ItemStack stack, Level world, int itemSlot, boolean isSelected)
    {
        if (stack.getOrDefault(AVADataComponents.TAG_ITEM_UUID.get(), Util.NIL_UUID).equals(Util.NIL_UUID))
            stack.set(AVADataComponents.TAG_ITEM_UUID.get(), UUID.randomUUID());
    }

    public final Lazy<Input> input = Lazy.of(this::createInputManager);

    protected abstract Input createInputManager();

    @EventBusSubscriber(Dist.CLIENT)
    public static class Input<I extends Item, M extends ItemStatusManager<I>>
    {
        protected final M manager;
        private static int UNFOCUSED_DELAY = 0;

        public Input(M manager)
        {
            this.manager = manager;
        }

        protected void cancelSprint(Player player)
        {
            player.setSprinting(false);
            AVAClientUtil.stopSprinting(20);
        }

        @OnlyIn(Dist.CLIENT)
        @SubscribeEvent
        public static void onClientTick(ClientTickEvent.Pre event)
        {
            Minecraft minecraft = Minecraft.getInstance();
            LocalPlayer player = minecraft.player;
            if (player == null || !player.isAlive() || player.isSpectator() || !AVAClientUtil.isFocused())
            {
                UNFOCUSED_DELAY = 10;
                return;
            }
            if (UNFOCUSED_DELAY > 0)
            {
                UNFOCUSED_DELAY--;
                return;
            }
            ItemStack stack = player.getMainHandItem();
            for (ItemStatusManager<?> manager : MANAGERS)
            {
                if (!manager.isRightItem(stack))
                    continue;
                Item item = stack.getItem();
                long window = minecraft.getWindow().getWindow();
                manager.input.get().renderTick(player, item, stack);
                if (GLFW.glfwGetMouseButton(window, GLFW.GLFW_MOUSE_BUTTON_LEFT) == GLFW.GLFW_PRESS)
                    manager.input.get().whileLeftDown(player, item, stack);
                else
                    manager.input.get().whileLeftUp(player, item, stack);
                if (GLFW.glfwGetMouseButton(window, GLFW.GLFW_MOUSE_BUTTON_RIGHT) == GLFW.GLFW_PRESS)
                    manager.input.get().whileRightDown(player, item, stack);
                else
                    manager.input.get().whileRightUp(player, item, stack);
            }
        }

        @SubscribeEvent
        @OnlyIn(Dist.CLIENT)
        public static void onMouseClick(InputEvent.MouseButton.Pre event)
        {
            Minecraft minecraft = Minecraft.getInstance();
            LocalPlayer player = minecraft.player;
            if (player == null || !player.isAlive() || player.isSpectator() || !AVAClientUtil.isFocused() || UNFOCUSED_DELAY > 0)
                return;
            if (event.getAction() == GLFW.GLFW_PRESS)
            {
                ItemStack stack = player.getMainHandItem();
                for (ItemStatusManager<?> manager : MANAGERS)
                {
                    if (!manager.isRightItem(stack))
                        continue;
                    if (event.getButton() == GLFW.GLFW_MOUSE_BUTTON_LEFT && manager.input.get().onLeftClick(player, stack.getItem(), stack))
                        event.setCanceled(true);
                    if (event.getButton() == GLFW.GLFW_MOUSE_BUTTON_RIGHT && manager.input.get().onRightClick(player, stack.getItem(), stack))
                        event.setCanceled(true);
                }
            }
        }

        public void renderTick(Player player, I i, ItemStack stack)
        {

        }

        @OnlyIn(Dist.CLIENT)
        public void tick(Player player)
        {

        }

        public boolean onLeftClick(Player player, I i, ItemStack stack)
        {
            return false;
        }

        public void whileLeftDown(Player player, I i, ItemStack stack)
        {

        }

        public void whileLeftUp(Player player, I i, ItemStack stack)
        {

        }

        public boolean onRightClick(Player player, I i, ItemStack stack)
        {
            return false;
        }

        public void whileRightDown(Player player, I i, ItemStack stack)
        {

        }

        public void whileRightUp(Player player, I i, ItemStack stack)
        {

        }
    }

    public interface IStatus
    {

    }
}
