package pellucid.ava.player.status;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVAConstants;

import static pellucid.ava.util.AVAConstants.BINOCULAR_AIM_TIME;

public class BinocularStatusManager extends ItemStatusManager<BinocularItem>
{
    public static final BinocularStatusManager INSTANCE = new BinocularStatusManager();

    public int fireInterval = 0;

    public BinocularStatusManager()
    {
        super(() -> AVAModelTypes.BINOCULAR);
    }

    @Override
    protected void tickItem(BinocularItem item, Entity player, ItemStack stack, Level world, int itemSlot, boolean isSelected)
    {
        super.tickItem(item, player, stack, world, itemSlot, isSelected);
        if (!isSelected)
        {
            stack.set(AVADataComponents.TAG_ITEM_DRAW, 0);
            stack.set(AVADataComponents.TAG_ITEM_FIRE, 0);
        }
        else
        {
            countDown(stack, AVADataComponents.TAG_ITEM_DRAW);
            countDown(stack, AVADataComponents.TAG_ITEM_FIRE);
        }
    }

    @Override
    protected Runnable onComplete(Player player, BinocularItem binocular, ItemStack stack)
    {
        if (status == BinocularStatus.ADS_IN)
            PlayerAction.getCap(player).setIsADS(player.level(), true);
        return null;
    }

    @Override
    protected void tick(Player player, BinocularItem binocular, ItemStack stack)
    {
        if (isActive(BinocularStatus.DRAW))
            playSound(player, AVASoundTracks.M4A1_DRAW, progress);

        super.tick(player, binocular, stack);
        if (isActive(BinocularStatus.ADS_IN, BinocularStatus.ADS_OUT))
        {
            run = 0;
            walk = 0;
            steady = 0;
        }
        if (matchesID(stack, id))
        {
            if (fireInterval > 0)
                fireInterval--;
        }
        else
        {
            clear(player);
            if (isRightItem(stack))
            {
                id = getID(stack);
                tryDraw(player, binocular, stack);
            }
        }
    }

    public boolean tryFire(Player player, BinocularItem binocular, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(BinocularStatus.DRAW, BinocularStatus.ADS_IN, BinocularStatus.ADS_OUT))
                return false;
            if (!isADS(player))
                return false;
            if (fireInterval <= 0)
            {
                fireInterval = AVAConstants.BINOCULAR_CD;
                PlayerActionMessage.binocular();
                return true;
            }
        }
        return false;
    }

    public boolean tryDraw(Player player, BinocularItem binocular, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            PlayerAction.getCap(player).setIsADS(player.level(), false);
            PlayerActionMessage.statusMeleeDraw();
            start(player, binocular, stack, BinocularStatus.DRAW, 20);
            return true;
        }
        return false;
    }

    public boolean tryADS(Player player, BinocularItem binocular, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(BinocularStatus.DRAW, BinocularStatus.ADS_IN, BinocularStatus.ADS_OUT))
                return false;
            start(player, binocular, stack, BinocularStatus.ADS_IN, BINOCULAR_AIM_TIME);
            AVAClientUtil.playSoundOnServer(true, AVASounds.AIM.get());
            return true;
        }
        return false;
    }

    public boolean tryExitADS(Player player, BinocularItem binocular, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(BinocularStatus.DRAW, BinocularStatus.ADS_IN, BinocularStatus.ADS_OUT))
                return false;
            if (!isADS(player))
                return false;
            start(player, binocular, stack, BinocularStatus.ADS_OUT, BINOCULAR_AIM_TIME);
            PlayerAction.getCap(player).setIsADS(player.level(), false);
            AVAClientUtil.playSoundOnServer(true, AVASounds.AIM.get());
            return true;
        }
        return false;
    }

    @Override
    protected void clear(Player player)
    {
        super.clear(player);
        fireInterval = 0;
    }

    @Override
    protected BinoInput createInputManager()
    {
        return new BinoInput();
    }

    public static class BinoInput extends Input<BinocularItem, BinocularStatusManager>
    {
        public BinoInput()
        {
            super(INSTANCE);
        }

        @Override
        public boolean onLeftClick(Player player, BinocularItem binocularItem, ItemStack stack)
        {
            return manager.tryFire(player, binocularItem, stack);
        }

        @Override
        public boolean onRightClick(Player player, BinocularItem binocularItem, ItemStack stack)
        {
            if (manager.isADS(player))
                return manager.tryExitADS(player, binocularItem, stack);
            return manager.tryADS(player, binocularItem, stack);
        }

        @Override
        public void tick(Player player)
        {
            if (manager.isActive(BinocularStatus.DRAW, BinocularStatus.ADS_IN, BinocularStatus.ADS_OUT))
                cancelSprint(player);
        }
    }

    public enum BinocularStatus implements IStatus
    {
        IDLE,
        DRAW,
        RUN,
        ADS_IN,
        ADS_OUT
        ;
    }
}
