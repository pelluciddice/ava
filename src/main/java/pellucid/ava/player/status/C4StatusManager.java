package pellucid.ava.player.status;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.packets.PlayerActionMessage;
import pellucid.ava.sounds.AVASoundTracks;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

public class C4StatusManager extends ItemStatusManager<C4Item>
{
    public static final C4StatusManager INSTANCE = new C4StatusManager();

    public C4StatusManager()
    {
        super(() -> AVAModelTypes.C4);
    }

    @Override
    protected void tickItem(C4Item item, Entity player, ItemStack stack, Level world, int itemSlot, boolean isSelected)
    {
        super.tickItem(item, player, stack, world, itemSlot, isSelected);
        if (!isSelected)
        {
            stack.set(AVADataComponents.TAG_ITEM_DRAW, 0);
            stack.set(AVADataComponents.TAG_ITEM_FIRE, 0);
        }
        else
        {
            countDown(stack, AVADataComponents.TAG_ITEM_DRAW);
            countDown(stack, AVADataComponents.TAG_ITEM_FIRE);
        }
    }

    @Override
    protected Runnable onComplete(Player player, C4Item c4, ItemStack stack)
    {
        if (isActive(C4Status.SET))
            PlayerActionMessage.plantC4();
        return null;
    }

    @Override
    protected void tick(Player player, C4Item c4, ItemStack stack)
    {
        if (isActive(C4Status.SET))
            playSound(player, AVASoundTracks.C4_SET, progress);
        else if (isActive(C4Status.DRAW))
            playSound(player, AVASoundTracks.C4_DRAW, progress);
        super.tick(player, c4, stack);
        if (matchesID(stack, id))
        {
        }
        else
        {
            clear(player);
            if (isRightItem(stack))
            {
                id = getID(stack);
                tryDraw(player, c4, stack);
            }
        }
    }

    public boolean trySet(Player player, C4Item c4, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            start(player, c4, stack, C4Status.SET, AVAConstants.C4_SET_TIME);
            return true;
        }
        return false;
    }

    public boolean tryDraw(Player player, C4Item c4, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            start(player, c4, stack, C4Status.DRAW, 20);
            return true;
        }
        return false;
    }

    public static boolean unplantable(Player player)
    {
        return AVAServerConfig.isCompetitiveModeActivated() && !AVAWeaponUtil.isOnSite(player);
    }

    @Override
    protected C4Input createInputManager()
    {
        return new C4Input();
    }

    public static class C4Input extends Input<C4Item, C4StatusManager>
    {
        private int delay = 0;

        public C4Input()
        {
            super(INSTANCE);
        }

        @Override
        public void whileLeftDown(Player player, C4Item c4Item, ItemStack stack)
        {
            if (delay > 0)
                delay--;
            if (!manager.isActive(C4Status.SET))
            {
                if (unplantable(player))
                {
                    if (delay <= 0)
                    {
                        player.sendSystemMessage(Component.translatable("ava.chat.c4_plant_restricted"));
                        delay = 200;
                    }
                    return;
                }
                manager.trySet(player, c4Item, stack);
            }
        }

        @Override
        public void whileLeftUp(Player player, C4Item c4Item, ItemStack stack)
        {
            if (manager.isActive(C4Status.SET))
                manager.reset();
        }

        @Override
        public void tick(Player player)
        {
            if (manager.isActive(C4Status.DRAW, C4Status.SET))
                cancelSprint(player);
        }
    }

    public enum C4Status implements IStatus
    {
        IDLE,
        DRAW,
        RUN,
        SET,
        ;
    }
}
