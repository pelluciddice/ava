package pellucid.ava.player.status;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.client.renderers.models.AVAModelTypes;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.packets.PlayerActionMessage;

public class MeleeStatusManager extends ItemStatusManager<AVAMeleeItem>
{
    public static final MeleeStatusManager INSTANCE = new MeleeStatusManager();

    public int fireInterval = 0;

    public MeleeStatusManager()
    {
        super(() -> AVAModelTypes.MELEES);
    }

    @Override
    protected void tickItem(AVAMeleeItem item, Entity player, ItemStack stack, Level world, int itemSlot, boolean isSelected)
    {
        super.tickItem(item, player, stack, world, itemSlot, isSelected);
        if (!isSelected)
        {
            stack.set(AVADataComponents.TAG_ITEM_DRAW, 0);
            stack.set(AVADataComponents.TAG_ITEM_ATTACK_LIGHT, 0);
            stack.set(AVADataComponents.TAG_ITEM_ATTACK_HEAVY, 0);
        }
        else
        {
            countDown(stack, AVADataComponents.TAG_ITEM_DRAW);
            countDown(stack, AVADataComponents.TAG_ITEM_ATTACK_LIGHT);
            countDown(stack, AVADataComponents.TAG_ITEM_ATTACK_HEAVY);
        }
    }

    @Override
    protected Runnable onComplete(Player player, AVAMeleeItem knife, ItemStack stack)
    {
        return null;
    }

    @Override
    protected void tick(Player player, AVAMeleeItem knife, ItemStack stack)
    {
        if (isActive(MeleeStatus.DRAW))
            playSound(player, knife.getDrawSound(), progress);
        else if (isActive(MeleeStatus.ATTACK_LIGHT))
            playSound(player, knife.getAttackLightSound(), progress, 1.0F, 0.85F + RANDOM.nextFloat() * 0.3F);
        else if (isActive(MeleeStatus.ATTACK_HEAVY))
            playSound(player, knife.getAttackHeavySound(), progress, 1.0F, 0.85F + RANDOM.nextFloat() * 0.3F);

        super.tick(player, knife, stack);
        if (matchesID(stack, id))
        {
            if (fireInterval > 0)
                fireInterval--;
            if (isActive(MeleeStatus.ATTACK_LIGHT) && progress == 3)
                PlayerActionMessage.melee(true);
            if (isActive(MeleeStatus.ATTACK_HEAVY) && progress == 7)
                PlayerActionMessage.melee(false);
        }
        else
        {
            clear(player);
            if (isRightItem(stack))
            {
                id = getID(stack);
                tryDraw(player, knife, stack);
            }
        }
    }

    public boolean tryDraw(Player player, AVAMeleeItem knife, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            start(player, knife, stack, MeleeStatus.DRAW, knife.getDrawSpeed());
            PlayerActionMessage.statusMeleeDraw();
            return true;
        }
        return false;
    }

    public boolean tryAttackLeft(Player player, AVAMeleeItem knife, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(MeleeStatus.DRAW, MeleeStatus.ATTACK_LIGHT, MeleeStatus.ATTACK_HEAVY))
                return false;
            start(player, knife, stack, MeleeStatus.ATTACK_LIGHT, knife.getSpeedL());
            PlayerActionMessage.statusMelee(true);
        }
        return false;
    }

    public boolean tryAttackRight(Player player, AVAMeleeItem knife, ItemStack stack)
    {
        if (matchesID(stack, id))
        {
            if (isActive(MeleeStatus.DRAW, MeleeStatus.ATTACK_LIGHT, MeleeStatus.ATTACK_HEAVY))
                return false;
            start(player, knife, stack, MeleeStatus.ATTACK_HEAVY, knife.getSpeedR());
            PlayerActionMessage.statusMelee(false);
        }
        return false;
    }

    @Override
    protected MeleeInput createInputManager()
    {
        return new MeleeInput();
    }

    public static class MeleeInput extends Input<AVAMeleeItem, MeleeStatusManager>
    {
        public MeleeInput()
        {
            super(INSTANCE);
        }

        @Override
        public void whileLeftDown(Player player, AVAMeleeItem knife, ItemStack stack)
        {
            manager.tryAttackLeft(player, knife, stack);
        }

        @Override
        public void whileRightDown(Player player, AVAMeleeItem knife, ItemStack stack)
        {
            manager.tryAttackRight(player, knife, stack);
        }

        @Override
        public void tick(Player player)
        {
            if (manager.isActive(MeleeStatus.DRAW, MeleeStatus.ATTACK_LIGHT, MeleeStatus.ATTACK_HEAVY))
                cancelSprint(player);
        }
    }

    public enum MeleeStatus implements IStatus
    {
        IDLE,
        DRAW,
        RUN,
        ATTACK_LIGHT,
        ATTACK_HEAVY,
        ;
    }
}
