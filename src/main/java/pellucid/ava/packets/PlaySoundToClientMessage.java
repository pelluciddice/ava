package pellucid.ava.packets;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.util.DataTypes;

public class PlaySoundToClientMessage implements CustomPacketPayload
{
    public static final Type<PlaySoundToClientMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "play_sound_to_client"));

    private final String sound;
    private final int category;
    private final float x, y, z;
    private final float volume, pitch;
    private int moving = -1;

    public PlaySoundToClientMessage(RegistryFriendlyByteBuf buffer)
    {
        this(DataTypes.STRING.read(buffer), DataTypes.INT.read(buffer), DataTypes.FLOAT.read(buffer), DataTypes.FLOAT.read(buffer), DataTypes.FLOAT.read(buffer), DataTypes.FLOAT.read(buffer), DataTypes.FLOAT.read(buffer), DataTypes.INT.read(buffer));
    }

    public PlaySoundToClientMessage(SoundEvent sound, Entity source)
    {
        this(sound, source.getX(), source.getY(), source.getZ(), 1.0F);
    }

    public PlaySoundToClientMessage(SoundEvent sound, double x, double y, double z)
    {
        this(sound, x, y, z, 1.0F);
    }

    public PlaySoundToClientMessage(SoundEvent sound, double x, double y, double z, float volume)
    {
        this(BuiltInRegistries.SOUND_EVENT.getKey(sound).toString(), x, y, z, volume, 1.0F);
    }

    public PlaySoundToClientMessage(SoundEvent sound, double x, double y, double z, float volume, float pitch)
    {
        this(BuiltInRegistries.SOUND_EVENT.getKey(sound).toString(), x, y, z, volume, pitch);
    }

    public PlaySoundToClientMessage(String sound, double x, double y, double z, float volume, float pitch)
    {
        this(sound, SoundSource.PLAYERS, x, y, z, volume, pitch);
    }

    public PlaySoundToClientMessage(String sound, SoundSource category, double x, double y, double z, float volume, float pitch)
    {
        this(sound, category.ordinal(), x, y, z, volume, pitch);
    }

    public PlaySoundToClientMessage(String sound, int category, double x, double y, double z, float volume, float pitch)
    {
        this(sound, category, x, y, z, volume, pitch, -1);
    }

    public PlaySoundToClientMessage(String sound, int category, double x, double y, double z, float volume, float pitch, int moving)
    {
        this.sound = sound;
        this.category = category;
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
        this.volume = volume;
        this.pitch = pitch;
        this.moving = moving;
    }

    public void write(RegistryFriendlyByteBuf buffer)
    {
        DataTypes.STRING.write(buffer, sound);
        DataTypes.INT.write(buffer, category);
        DataTypes.FLOAT.write(buffer, x);
        DataTypes.FLOAT.write(buffer, y);
        DataTypes.FLOAT.write(buffer, z);
        DataTypes.FLOAT.write(buffer, volume);
        DataTypes.FLOAT.write(buffer, pitch);
        DataTypes.INT.write(buffer, moving);
    }

    public PlaySoundToClientMessage setMoving(int entityID)
    {
        this.moving = entityID;
        return this;
    }

    public static void handle(PlaySoundToClientMessage message, IPayloadContext ctx)
    {
        if (ctx.flow().isClientbound())
        {
            ctx.enqueueWork(new Runnable() {
                @Override
                public void run()
                {
                    Player player = ctx.player();
                    SoundEvent sound = BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(message.sound));
                    if (sound != null)
                    {
                        Entity entity = player.level().getEntity(message.moving);
                        SoundSource category = SoundSource.values()[message.category];
                        if (entity == null)
                            player.level().playLocalSound(message.x, message.y, message.z, sound, category, message.volume, message.pitch, false);
                        else
                            player.level().playSound(player, entity, sound, category, message.volume, message.pitch);
                    }
                }
            });
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
