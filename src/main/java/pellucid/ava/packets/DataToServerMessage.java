package pellucid.ava.packets;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.blocks.modifying_table.GunModifyingTableContainer;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.client.renderers.environment.ActivePingEffect;
import pellucid.ava.competitive_mode.CompetitiveInventory;
import pellucid.ava.competitive_mode.Preset;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.smart.SidedSmartAIEntity;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.gun.attachments.GunAttachmentManager;
import pellucid.ava.gun.attachments.GunAttachmentTypes;
import pellucid.ava.gun.gun_mastery.GunMastery;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.init.Materials;
import pellucid.ava.items.weapon_chest.WeaponChest;
import pellucid.ava.recipes.IHasRecipe;
import pellucid.ava.recipes.Recipe;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import java.util.List;

public class DataToServerMessage extends TypedMessage
{
    public static final Type<DataToServerMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "data_to_server"));

    private static final int GUN_CRAFTING = 0;
    private static final int GUN_PAINTING = 1;
    private static final int BUILDER_CRAFTING = 2;
    private static final int PICK_WEAPON_CHEST = 3;
    private static final int PING = 4;
    private static final int GUN_MODIFYING = 5;
    private static final int ASSIGN_MASTERY = 6;
    private static final int SELECT_WEAPON = 7;
    private static final int SEND_PRESET = 8;

    private final CompoundTag data;

    public static void gunCrafting(Item item, boolean forGun)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.ITEM.write(compound, "item", item);
        DataTypes.BOOLEAN.write(compound, "forGun", forGun);
        sendToServer(GUN_CRAFTING, compound);
    }

    public static void gunPainting(Item from, Item to)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.ITEM.write(compound, "from", from);
        DataTypes.ITEM.write(compound, "to", to);
        sendToServer(GUN_PAINTING, compound);
    }

    public static void builderCrafting(Item item, int count)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.ITEM.write(compound, "item", item);
        DataTypes.INT.write(compound, "count", count);
        sendToServer(BUILDER_CRAFTING, compound);
    }

    public static void pickWeaponChest(int slot)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "slot", slot);
        sendToServer(PICK_WEAPON_CHEST, compound);
    }

    public static void ping(Vec3 vec, int type)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.FLOAT.write(compound, "x", (float) vec.x);
        DataTypes.FLOAT.write(compound, "y", (float) vec.y);
        DataTypes.FLOAT.write(compound, "z", (float) vec.z);
        DataTypes.INT.write(compound, "type", type);
        sendToServer(PING, compound);
    }

    public static void gunModifying(boolean install, GunAttachmentTypes type)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.BOOLEAN.write(compound, "install", install);
        DataTypes.INT.write(compound, "type", type.ordinal());
        sendToServer(GUN_MODIFYING, compound);
    }

    public static void assignMastery(String mastery)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.STRING.write(compound, "mastery", mastery);
        sendToServer(ASSIGN_MASTERY, compound);
    }

    public static void selectWeapon(int index, int index2)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "index", index);
        DataTypes.INT.write(compound, "index2", index2);
        sendToServer(SELECT_WEAPON, compound);
    }

    public static void sendPreset(CompoundTag compound)
    {
        sendToServer(SEND_PRESET, compound);
    }

    private static void sendToServer(int type, CompoundTag data)
    {
        PacketDistributor.sendToServer(new DataToServerMessage(type, data));
    }

    public DataToServerMessage(FriendlyByteBuf buffer)
    {
        this(DataTypes.INT.read(buffer), DataTypes.COMPOUND.read(buffer));
    }

    private DataToServerMessage(int type, CompoundTag data)
    {
        super(type);
        this.data = data;
    }

    @Override
    public void write(FriendlyByteBuf buffer)
    {
        super.write(buffer);
        DataTypes.COMPOUND.write(buffer, data);
    }

    public static void handle(DataToServerMessage message, IPayloadContext ctx)
    {
        if (ctx.flow().isServerbound())
        {
            ctx.enqueueWork(() ->
            {
                Player player = ctx.player();
                switch (message.type)
                {
                    case GUN_CRAFTING -> {
                        Item item = DataTypes.ITEM.read(message.data, "item");
                        if (item instanceof IHasRecipe)
                        {
                            Item item2 = DataTypes.BOOLEAN.read(message.data, "forGun") ? item : ((AVAItemGun) item).getAmmoType(new ItemStack(item));
                            Recipe recipe = ((IHasRecipe) item2).getRecipe();
                            if (recipe.canCraft(player, item2))
                                recipe.craft((ServerPlayer) player, item2);
                        }
                    }
                    case GUN_PAINTING -> {
                        Item from = DataTypes.ITEM.read(message.data, "from");
                        Item to = DataTypes.ITEM.read(message.data, "to");
                        if (to instanceof AVAItemGun && !player.isCreative())
                        {
                            if (((AVAItemGun) to).isMaster())
                            {
                                Recipe recipe = new Recipe().addItem(from).addItem(Materials.ACETONE_SOLUTION.get());
                                if (recipe.canCraft(player, to))
                                    recipe.craft((ServerPlayer) player, to, (stack) -> stack.getItem() instanceof AVAItemGun);
                            }
                            else
                            {
                                Recipe recipe = ((IHasRecipe) to).getRecipe();
                                if (recipe.canCraft(player, to))
                                    recipe.craft((ServerPlayer) player, to, (stack) -> stack.getItem() instanceof AVAItemGun);
                            }
                        }
                        else if (player.isCreative())
                        {
                            ItemStack result = new ItemStack(to);
                            result.setDamageValue(result.getMaxDamage());
                            if (!player.getInventory().add(result))
                                player.level().addFreshEntity(new ItemEntity(player.level(), player.getX(), player.getY(), player.getZ(), result));
                        }
                    }
                    case BUILDER_CRAFTING -> {
                        Item item = DataTypes.ITEM.read(message.data, "item");
                        int count = DataTypes.INT.read(message.data, "count");
                        if (item instanceof IHasRecipe)
                        {
                            Recipe recipe = ((IHasRecipe) item).getRecipe();
                            if (recipe.canCraft(player, item, count))
                                for (int i = 0; i < count; i++)
                                    recipe.craft((ServerPlayer) player, item);
                        }
                    }
                    case PICK_WEAPON_CHEST -> {
                        ItemStack stack = player.getMainHandItem();
                        Item item = stack.getItem();
                        if (item instanceof WeaponChest)
                        {
                            player.level().playSound(null, player.blockPosition().getX(), player.blockPosition().getY(), player.blockPosition().getZ(), SoundEvents.CHEST_CLOSE, SoundSource.PLAYERS, 1.0F, 1.0F);
                            player.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(((WeaponChest) stack.getItem()).getContents().get(DataTypes.INT.read(message.data, "slot"))));
                            stack.shrink(1);
                        }
                    }
                    case PING -> {
                        Vec3 vec = new Vec3(DataTypes.FLOAT.read(message.data, "x"), DataTypes.FLOAT.read(message.data, "y"), DataTypes.FLOAT.read(message.data, "z"));
                        ActivePingEffect.Type type = ActivePingEffect.Type.values()[DataTypes.INT.read(message.data, "type")];
                        AVAWorldData.getInstance(player.level()).activePings.add(new ActivePingEffect(vec, type));
                        DataToClientMessage.ping((p) -> PacketDistributor.sendToPlayersInDimension((ServerLevel) player.level(), p), vec, type);
                        player.level().getEntitiesOfClass(SidedSmartAIEntity.class, player.getBoundingBox().inflate(10), (e) -> e.getLeader() == player).forEach((entity) -> {
                            entity.moveToPing(vec);
                            entity.playSound(AVAWeaponUtil.RadioMessage.X1.getSound(entity), 1.0F, 1.0F);
                        });
                    }
                    case GUN_MODIFYING -> {
                        boolean install = DataTypes.BOOLEAN.read(message.data, "install");
                        GunAttachmentTypes type = GunAttachmentTypes.values()[DataTypes.INT.read(message.data, "type")];
                        if (player.containerMenu instanceof GunModifyingTableContainer container)
                        {
                            ItemStack stack = container.handler.getStackInSlot(0);
                            if (stack.getItem() instanceof AVAItemGun gun && gun.getAttachmentCategories().get(type.getCategory()).contains(type))
                            {
                                GunAttachmentManager manager = AVAItemGun.manager(stack);
                                Recipe recipe = type.getRecipe();
                                if (install && recipe.canCraft(player, Items.AIR))
                                {
                                    recipe.craft((ServerPlayer) player, Items.AIR);
                                    manager.install(type);
                                }
                                else
                                    manager.uninstall(type);
                            }
                        }
                    }
                    case ASSIGN_MASTERY -> {
                        ItemStack stack = player.getMainHandItem();
                        if (stack.getItem() instanceof AVAItemGun)
                            GunMasteryManager.ofUnsafe(stack).setMastery(GunMastery.MASTERIES.get(DataTypes.STRING.read(message.data, "mastery")));
                    }
                    case SELECT_WEAPON -> {
                        int index = DataTypes.INT.read(message.data, "index");
                        int index2 = DataTypes.INT.read(message.data, "index2");
                        AVAWeaponUtil.WeaponCategory category = AVAWeaponUtil.WeaponCategory.fromIndex(index);
                        List<Pair<ItemStack, Integer>> stacks = category.getStacksAndSlot(player.getInventory());
                        try
                        {
                            Pair<ItemStack, Integer> stack = stacks.get(index2);
                            ItemStack held = player.getMainHandItem();
                            player.getInventory().setItem(player.getInventory().selected, stack.getA());
                            player.getInventory().setItem(stack.getB(), ItemStack.EMPTY);
                            player.getInventory().add(held);
                        }
                        catch (Exception ignored)
                        {

                        }
                    }
                    case SEND_PRESET -> {
                        if (AVAServerConfig.isCompetitiveModeActivated())
                        {
                            Preset p = new Preset(message.data);
                            CompetitiveInventory.giveIfExist(player, AVAWeaponUtil.clearPlayerInventory(player), new int[]{0, 0, 1, 2, 3, 3, 3, 4}, p.getPrimaryModification(), p.getPrimaryModification2(), p.getPrimary(), p.getPrimary2(), p.getSecondary(), p.getMelee(), p.getProjectile(), p.getProjectile2(), p.getProjectile3(), p.getSpecial());
                            if (!CompetitiveInventory.giveArmour(player, "eu"))
                                CompetitiveInventory.giveArmour(player, "nrf");
                            GameModes.getRunningAVAGamemode().ifPresent((gamemode) -> {
                                player.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, gamemode.getRespawnInvincibleTime(), 999, true, false, false));
                            });
                        }
                    }
                }
            });
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
