package pellucid.ava.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.StringUtil;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BaseCommandBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.blocks.command_executor_block.CommandExecutorTE;
import pellucid.ava.util.DataTypes;

public class UpdateCommandExecutorMessage implements CustomPacketPayload
{
    public static final Type<UpdateCommandExecutorMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "update_command_executor"));

    private final BlockPos pos;
    private final String command;
    private final boolean trackOutput;
    private final int[] delays;

    public UpdateCommandExecutorMessage(FriendlyByteBuf buffer)
    {
        this(buffer.readBlockPos(), DataTypes.STRING.read(buffer), DataTypes.BOOLEAN.read(buffer), buffer.readVarIntArray());
    }

    public UpdateCommandExecutorMessage(BlockPos pos, String command, boolean trackOutput, int... delays)
    {
        this.pos = pos;
        this.command = command;
        this.trackOutput = trackOutput;
        this.delays = delays;
    }

    public void write(FriendlyByteBuf buffer)
    {
        buffer.writeBlockPos(pos);
        DataTypes.STRING.write(buffer, command);
        DataTypes.BOOLEAN.write(buffer, trackOutput);
        buffer.writeVarIntArray(delays);
    }

    public static void handle(UpdateCommandExecutorMessage message, IPayloadContext ctx)
    {
        if (ctx.flow().isServerbound())
        {
            ctx.enqueueWork(() ->
            {
                Player player = ctx.player();
                MinecraftServer server = player.getServer();
                if (!server.isCommandBlockEnabled())
                    player.sendSystemMessage(Component.translatable("advMode.notEnabled"));
                else if (!player.canUseGameMasterBlocks())
                    player.sendSystemMessage(Component.translatable("advMode.notAllowed"));
                else
                {
                    BlockEntity be = player.level().getBlockEntity(message.pos);
                    if (be instanceof CommandExecutorTE)
                    {
                        CommandExecutorTE executor = (CommandExecutorTE) be;
                        BaseCommandBlock command = executor.getCommandBlock();
                        int[] delays = message.delays;
                        try
                        {
                            executor.constantDelay = delays[0];
                            executor.minRandDelay = delays[1];
                            executor.maxRandDelay = delays[2];
                        }
                        catch (Exception e)
                        {
                            AVA.LOGGER.error("Player " + player + " is trying sending invalid command executor arguments");
                        }
                        command.setCommand(message.command);
                        command.setTrackOutput(message.trackOutput);
                        command.onUpdated();
                        if (!StringUtil.isNullOrEmpty(message.command))
                            player.sendSystemMessage(Component.translatable("advMode.setCommand.success", message.command));
                        be.setChanged();
                    }
                }
            });
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
