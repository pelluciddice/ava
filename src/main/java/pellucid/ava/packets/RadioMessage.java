package pellucid.ava.packets;

import net.minecraft.ChatFormatting;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;


public class RadioMessage implements CustomPacketPayload
{
    public static final Type<RadioMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "radio"));

    private final int category;
    private final int index;

    public RadioMessage(FriendlyByteBuf buffer)
    {
        this(AVAWeaponUtil.RadioCategory.values()[DataTypes.INT.read(buffer)], DataTypes.INT.read(buffer));
    }

    public RadioMessage(AVAWeaponUtil.RadioCategory category, int index)
    {
        this.category = category.ordinal();
        this.index = index;
    }

    public void write(FriendlyByteBuf buffer)
    {
        DataTypes.INT.write(buffer, category);
        DataTypes.INT.write(buffer, index);
    }

    public static void handle(RadioMessage message, IPayloadContext ctx)
    {
        ctx.enqueueWork(() -> {
            Player player = ctx.player();
            AVAWeaponUtil.RadioMessage radio = AVAWeaponUtil.RadioCategory.values()[message.category].getMessages().get(message.index - 1);
            MutableComponent text = Component.literal("<" + player.getDisplayName().getString() + ">" + " [RADIO]: ");
            if (radio.isX9())
            {
                text.append(radio.getDisplayName()).append(" at " + player.blockPosition()).withStyle(ChatFormatting.RED);
                player.addEffect(new MobEffectInstance(MobEffects.GLOWING, 100, 0, false, false));
                AVAWorldData.getInstance(player.level()).x9S.put(player.getUUID(), 80);
                DataToClientMessage.world((ServerLevel) player.level(), (p) -> PacketDistributor.sendToPlayersTrackingEntity(player, p));
            }
            else
                text.append(radio.getDisplayName().withStyle(ChatFormatting.GOLD));
            player.level().players().forEach((player2) ->
            {
                if (AVAWeaponUtil.isSameSide(player, player2) || radio.isX9())
                    send(player2, player, text, radio);
            });
        });
    }

    public static void send(Player player, Player sender, Component text, AVAWeaponUtil.RadioMessage radio)
    {
        SoundEvent sound = radio.getSound(sender);
        if (sound != null)
        {
            player.sendSystemMessage(text);
            PacketDistributor.sendToPlayer((ServerPlayer) player, new PlaySoundToClientMessage(sound, player.getX(), player.getEyeY(), player.getZ()).setMoving(player.getId()));
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
