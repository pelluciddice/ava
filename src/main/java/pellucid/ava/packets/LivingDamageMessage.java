package pellucid.ava.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.client.overlay.KillTips;
import pellucid.ava.client.renderers.HUDIndicators;
import pellucid.ava.client.renderers.KillEffect;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.functionalities.ICustomOnHitEffect;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.DataTypes;

import javax.annotation.Nullable;

import static pellucid.ava.util.AVACommonUtil.cap;

public class LivingDamageMessage implements CustomPacketPayload
{
    public static final Type<LivingDamageMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "living_damage"));

    private final int killerID;
    private final int type;
    private final Item weapon;
    private final int targetID;
    public static final int HURT = 0;
    public static final int DEATH = 1;
    public static final int TARGET_HURT = 2;

    public LivingDamageMessage(FriendlyByteBuf buffer)
    {
        this(DataTypes.INT.read(buffer), DataTypes.INT.read(buffer), BuiltInRegistries.ITEM.get(new ResourceLocation(DataTypes.STRING.read(buffer))), DataTypes.INT.read(buffer));
    }

    public LivingDamageMessage(int killerID, int type, @Nullable Item weapon, int targetID)
    {
        this.killerID = killerID;
        this.type = type;
        this.weapon = weapon == null ? Items.AIR : weapon;
        this.targetID = targetID;
    }

    public void write(FriendlyByteBuf buffer)
    {
        DataTypes.INT.write(buffer, killerID);
        DataTypes.INT.write(buffer, type);
        DataTypes.STRING.write(buffer, BuiltInRegistries.ITEM.getKey(weapon).toString());
        DataTypes.INT.write(buffer, targetID);
    }

    public static void handle(LivingDamageMessage message, IPayloadContext ctx)
    {
        if (ctx.flow().isClientbound())
        {
            Player player = ctx.player();
            Minecraft minecraft = Minecraft.getInstance();
            if (minecraft.level == null)
                return;
            Entity entity = minecraft.level.getEntity(message.killerID);
            Entity target = minecraft.level.getEntity(message.targetID);
            if (entity instanceof LivingEntity)
            {
                if (message.type == HURT)
                {
                    cap(player).addHurtInfo(entity);
                    HUDIndicators.Hurt.onHurt();
                }
                else if (message.type == DEATH && target instanceof LivingEntity)
                {
                    ICustomOnHitEffect effect = target instanceof ICustomOnHitEffect effector ? effector : null;
                    if ((effect == null || effect.onKillTip()) && AVAServerConfig.ANNOUNCE_KILL_TIPS.get())
                        AVAClientUtil.KILLTIPS.add(new KillTips.KillTip((LivingEntity) entity, message.weapon, (LivingEntity) target, player));
                    if (entity == player && AVAClientConfig.ENABLE_KILL_EFFECT.get() && (effect == null || effect.onKillEffect()))
                        AVAWorldData.getInstance(player.level()).killMarks.add(new KillEffect(target));
                }
                else if (message.type == TARGET_HURT && target instanceof LivingEntity && entity == player)
                {
                    if (AVAClientConfig.ENABLE_HIT_EFFECT.get())
                        AVAClientUtil.addHitMark(player, target);
                }
            }
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
