package pellucid.ava.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.blocks.IInteractable;
import pellucid.ava.blocks.rpg_box.RPGBoxBlock;
import pellucid.ava.blocks.rpg_box.RPGBoxTE;
import pellucid.ava.cap.AVADataComponents;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.config.AVAServerConfig;
import pellucid.ava.entities.objects.c4.C4Entity;
import pellucid.ava.entities.objects.item.GunItemEntity;
import pellucid.ava.entities.objects.leopard.LeopardEntity;
import pellucid.ava.gun.gun_mastery.GunMasteryManager;
import pellucid.ava.gun.stats.GunStatTypes;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.AVAMeleeItem;
import pellucid.ava.items.miscs.BinocularItem;
import pellucid.ava.items.miscs.C4Item;
import pellucid.ava.items.miscs.GunStatModifierManager;
import pellucid.ava.items.miscs.ParachuteItem;
import pellucid.ava.items.throwables.ThrowableItem;
import pellucid.ava.player.status.C4StatusManager;
import pellucid.ava.sounds.AVASounds;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;

import java.util.concurrent.atomic.AtomicReference;

public class PlayerActionMessage extends TypedMessage
{
    public static final Type<PlayerActionMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "player_action"));

    private static final int FIRE = 0;
    private static final int RELOAD = 1;
    private static final int GRENADE_THROW = 2;
    private static final int GRENADE_TOSS = 3;
    private static final int ADS_IN = 4;
    private static final int ADS_OUT = 5;
    private static final int BINOCULAR_USE = 6;
    private static final int MELEE_LIGHT = 7;
    private static final int MELEE_HEAVY = 8;
    private static final int SILENCER = 9;
    private static final int STATUS_PRE_RELOAD = 10;
    private static final int STATUS_RELOAD = 11;
    private static final int STATUS_POST_RELOAD = 12;
    private static final int STATUS_DRAW = 13;
    private static final int ROLL_BOOST = 14;
    private static final int PLANT_C4 = 15;
    private static final int STATUS_MELEE_ATTACK_LIGHT = 16;
    private static final int STATUS_MELEE_ATTACK_HEAVY = 17;
    private static final int STATUS_MELEE_DRAW = 18;
    private static final int PICKUP_WEAPON = 19;
    private static final int INTERACTION_COMPLETE_BLOCK = 20;
    private static final int INTERACTION_COMPLETE_ENTITY = 21;
    private static final int ACTIVATE_PARACHUTE = 22;
    private static final int REPAIR_LEOPARD = 23;

    public static void fire()
    {
        sendToServer(FIRE);
    }

    public static void reload()
    {
        sendToServer(RELOAD);
    }

    public static void tossGrenade(boolean toss)
    {
        sendToServer(toss ? GRENADE_TOSS : GRENADE_THROW);
    }

    public static void ads(boolean in)
    {
        sendToServer(in ? ADS_IN : ADS_OUT);
    }

    public static void binocular()
    {
        sendToServer(BINOCULAR_USE);
    }

    public static void melee(boolean light)
    {
        sendToServer(light ? MELEE_LIGHT : MELEE_HEAVY);
    }

    public static void silencer()
    {
        sendToServer(SILENCER);
    }

    public static void statusPreReload()
    {
        sendToServer(STATUS_PRE_RELOAD);
    }

    public static void statusReload()
    {
        sendToServer(STATUS_RELOAD);
    }

    public static void statusPostReload()
    {
        sendToServer(STATUS_POST_RELOAD);
    }

    public static void statusDraw()
    {
        sendToServer(STATUS_DRAW);
    }

    public static void rollBoost()
    {
        sendToServer(ROLL_BOOST);
    }

    public static void plantC4()
    {
        sendToServer(PLANT_C4);
    }

    public static void statusMelee(boolean light)
    {
        sendToServer(light ? STATUS_MELEE_ATTACK_LIGHT : STATUS_MELEE_ATTACK_HEAVY);
    }

    public static void statusMeleeDraw()
    {
        sendToServer(STATUS_MELEE_DRAW);
    }

    public static void pickupWeapon()
    {
        sendToServer(PICKUP_WEAPON);
    }

    public static void interactionCompletionBlock()
    {
        sendToServer(INTERACTION_COMPLETE_BLOCK);
    }

    public static void interactionCompletionEntity()
    {
        sendToServer(INTERACTION_COMPLETE_ENTITY);
    }

    public static void activateParachute()
    {
        sendToServer(ACTIVATE_PARACHUTE);
    }

    public static void repairLeopard()
    {
        sendToServer(REPAIR_LEOPARD);
    }

    protected static void sendToServer(int type)
    {
        PacketDistributor.sendToServer(new PlayerActionMessage(type));
    }

    public PlayerActionMessage(FriendlyByteBuf buffer)
    {
        super(buffer);
    }

    private PlayerActionMessage(int type)
    {
        super(type);
    }

    public static void handle(PlayerActionMessage message, IPayloadContext ctx)
    {
        if (!ctx.flow().isClientbound())
        {
            ctx.enqueueWork(() -> {
                Player player = ctx.player();
                ItemStack stack = player.getMainHandItem();
                Item item = stack.getItem();
                boolean competitive = AVAServerConfig.isCompetitiveModeActivated();
                ServerLevel world = (ServerLevel) player.level();
                switch (message.type)
                {
                    case FIRE -> {
                        if (item instanceof AVAItemGun gun)
                        {
                            if (gun.firable(player, stack))
                                gun.fire(world, player, stack, competitive);
                        }
                    }
                    case RELOAD -> {
                        if (item instanceof AVAItemGun gun)
                        {
                            if (gun.reloadable(player, stack, competitive))
                                gun.reload(player, stack, competitive);
                        }
                    }
                    case GRENADE_THROW, GRENADE_TOSS -> {
                        if (item instanceof ThrowableItem grenade)
                            grenade.toss(world, player, stack, message.type == GRENADE_TOSS);
                    }
                    case ADS_IN, ADS_OUT -> {
                        PlayerAction.getCap(player).setIsADS(player.level(), message.type == ADS_IN);
                    }
                    case BINOCULAR_USE -> {
                        if (item instanceof BinocularItem binocular)
                        {
                            if (binocular.firable(player, stack))
                                binocular.fire(world, player, stack);
                        }
                    }
                    case MELEE_LIGHT, MELEE_HEAVY -> {
                        if (item instanceof AVAMeleeItem knife)
                            knife.meleeAttack(player, message.type == MELEE_LIGHT);
                    }
                    case SILENCER -> {
                        if (item instanceof AVAItemGun gun && gun.hasOptionalSilencer(stack, true))
                        {
                            stack.set(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, !stack.getOrDefault(AVADataComponents.TAG_ITEM_SILENCER_INSTALLED, false));
                            player.gameEvent(GameEvent.EQUIP, player);
                        }
                    }
                    case STATUS_PRE_RELOAD -> {
                        if (item instanceof AVAItemGun gun)
                            gun.startPreReload(stack);
                    }
                    case STATUS_RELOAD -> {
                        if (item instanceof AVAItemGun gun)
                            gun.startReload(stack);
                    }
                    case STATUS_POST_RELOAD -> {
                        if (item instanceof AVAItemGun gun)
                            gun.startPostReload(stack);
                    }
                    case STATUS_DRAW -> {
                        if (item instanceof AVAItemGun gun)
                            gun.startDraw(stack);
                    }
                    case ROLL_BOOST -> {
                        if (player.experienceLevel > 0)
                        {
                            GunMasteryManager.of(stack).ifPresent((manager) -> {
                                int[] boosts = new int[5];
                                int masteryLevel = manager.masteryLevel;
                                if (masteryLevel > 0)
                                {
                                    GunStatModifierManager.of(stack).ifPresent((modifiers) -> {
                                        modifiers.removeModifier(GunStatModifierManager.Source.GUN_MASTERY_BOOSTS);
                                        player.giveExperienceLevels(-1);
                                        for (int i = 0; i < masteryLevel; i++)
                                        {
                                            int r = AVAConstants.RAND.nextInt(5);
                                            int f = boosts[r] + 1;
                                            for (GunStatTypes type : GunMasteryManager.SUBS.get(GunMasteryManager.BOOSTS.get(r)))
                                            {
                                                modifiers.removeModifier(type, GunStatModifierManager.Source.GUN_MASTERY_BOOSTS);
                                                if (f > 0)
                                                    modifiers.addModifier(type, GunStatModifierManager.Source.GUN_MASTERY_BOOSTS, f * 2.0D);
                                            }
                                            boosts[r] = f;
                                        }
                                        manager.boosts = boosts;
                                        manager.save();
                                    });

                                }
                            });
                        }
                    }
                    case PLANT_C4 ->
                    {
                        if (stack.getItem() instanceof C4Item)
                        {
                            if (C4StatusManager.unplantable(player))
                                return;
                            C4Entity c4 = new C4Entity(player.level(), player);
                            c4.setYRot(player.getYRot());
                            world.addFreshEntity(c4);
                            if (!player.getAbilities().instabuild)
                                stack.shrink(1);
                        }
                    }
                    case STATUS_MELEE_ATTACK_LIGHT -> {
                        if (item instanceof AVAMeleeItem knife)
                            knife.startAttackLight(stack);
                    }
                    case STATUS_MELEE_ATTACK_HEAVY -> {
                        if (item instanceof AVAMeleeItem knife)
                            knife.startAttackHeavy(stack);
                    }
                    case STATUS_MELEE_DRAW -> {
                        if (item instanceof AVAMeleeItem knife)
                            knife.startDraw(stack);
                    }
                    case PICKUP_WEAPON -> {
                        if (AVAWeaponUtil.isPrimaryWeapon(stack.getItem()))
                        {
                            AtomicReference<ItemStack> toReplace = new AtomicReference<>(null);
                            if (AVAWeaponUtil.rayTrace(player, 1.0F, false) instanceof BlockHitResult result)
                            {
                                BlockPos pos = result.getBlockPos();
                                BlockState state = player.level().getBlockState(pos);
                                if (state.getBlock() instanceof RPGBoxBlock)
                                {
                                    if (state.getValue(RPGBoxBlock.HALF) == DoubleBlockHalf.UPPER)
                                        pos = pos.below();
                                    if (world.getBlockEntity(pos) instanceof RPGBoxTE box)
                                    {
                                        if (box.hasWeapon && box.isOpened())
                                        {
                                            box.pickUp();
                                            toReplace.set(RPGBoxTE.createRPGStack());
                                        }
                                    }
                                }
                            }
                            if (toReplace.get() == null)
                                player.level().getEntitiesOfClass(GunItemEntity.class, player.getBoundingBox().inflate(0.15F)).stream().findFirst().ifPresent((entity) -> {
                                    toReplace.set(entity.getStack().copy());
                                    entity.remove(Entity.RemovalReason.DISCARDED);
                                });
                            if (toReplace.get() != null)
                            {
                                player.level().addFreshEntity(new GunItemEntity(player, stack));
                                toReplace.get().set(AVAWeaponUtil.WeaponCategory.MAIN.getTag(), stack.getOrDefault(AVAWeaponUtil.WeaponCategory.MAIN.getTag(), 0));
                                AVAWeaponUtil.clearAnimationData(stack);
                                player.getInventory().setItem(player.getInventory().selected, toReplace.get());
                                player.level().playSound(null, player, AVASounds.PICKUP_ITEM.get(), SoundSource.PLAYERS, 1.0F, 1.0F);
                            }
                        }
                    }
                    case INTERACTION_COMPLETE_BLOCK -> {
                        HitResult result = AVAWeaponUtil.rayTrace(player, player.blockInteractionRange(), false);
                        if (result != null && result.getType() == HitResult.Type.BLOCK)
                        {
                            BlockPos pos = BlockPos.containing(result.getLocation());
                            if (world.getBlockState(pos).getBlock() instanceof IInteractable)
                                ((IInteractable) world.getBlockState(pos).getBlock()).interact(world, result, pos, result.getLocation(), player);
                        }
                    }
                    case INTERACTION_COMPLETE_ENTITY -> {
                        HitResult result = AVAWeaponUtil.rayTrace(player, player.entityInteractionRange(), false);
                        if (result != null && result.getType() == HitResult.Type.ENTITY)
                        {
                            Entity entity = ((EntityHitResult) result).getEntity();
                            if (entity instanceof IInteractable)
                                ((IInteractable) entity).interact(world, result, null, result.getLocation(), player);
                        }
                    }
                    case ACTIVATE_PARACHUTE -> {
                        ParachuteItem.activateParachute(player);
                    }
                    case REPAIR_LEOPARD -> {
                        HitResult result = AVAWeaponUtil.rayTrace(player, player.entityInteractionRange(), false);
                        if (result != null && result.getType() == HitResult.Type.ENTITY)
                        {
                            Entity entity = ((EntityHitResult) result).getEntity();
                            if (entity instanceof LeopardEntity leopard)
                                leopard.repair(player.getName().getString());
                        }
                    }
                }
            });
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
