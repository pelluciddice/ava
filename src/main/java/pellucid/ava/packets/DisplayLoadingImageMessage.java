package pellucid.ava.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.config.AVAClientConfig;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesClientManager;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesManager;
import pellucid.ava.gamemodes.loading_screen.LoadingImagesServerManager;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Lazy;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.nio.file.Path;

public class DisplayLoadingImageMessage implements CustomPacketPayload
{
    public static final Type<DisplayLoadingImageMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "display_loading_image"));

    public static final int SERVER_ASK_CLIENT = 0;
    public static final int SERVER_IMAGE_TO_CLIENT = 1;
    public static final int SERVER_ALL_READY = 2;
    public static final int CLIENT_REQUIRES_IMAGE = 3;
    public static final int CLIENT_IS_READY = 4;
    private final int flag;
    private final String name;
    private final int[][] pixels;
    private final int duration;

    public DisplayLoadingImageMessage(FriendlyByteBuf buffer)
    {
        this.flag = DataTypes.BYTE.read(buffer);
        AVA.LOGGER.info("Image transfer, decode operation " + flag);
        this.name = DataTypes.STRING.read(buffer);
        int width = DataTypes.SHORT.read(buffer);
        if (width > 0)
        {
            this.pixels = new int[width][];
            for (int col = 0; col < pixels.length; col++)
                this.pixels[col] = buffer.readVarIntArray();
        }
        else
            this.pixels = null;
        this.duration = DataTypes.INT.read(buffer);
        AVA.LOGGER.info("Image transfer, finished decode operation " + flag);
    }

    public DisplayLoadingImageMessage(int flag, String name, int[][] pixels, int duration)
    {
        this.flag = flag;
        this.name = name;
        this.pixels = pixels;
        this.duration = duration;
    }

    public void write(FriendlyByteBuf buffer)
    {
        AVA.LOGGER.info("Image transfer, encode operation " + flag);
        DataTypes.BYTE.write(buffer, (byte) flag);
        DataTypes.STRING.write(buffer, name);
        DataTypes.SHORT.write(buffer, (short) (pixels == null ? -1 : pixels.length));
        if (pixels != null)
        {
            for (int[] col : pixels)
                buffer.writeVarIntArray(col);
        }
        DataTypes.INT.write(buffer, duration);
        AVA.LOGGER.info("Image transfer, finished encode operation " + flag);
    }

    public static void handle(DisplayLoadingImageMessage message, IPayloadContext ctx)
    {
        if (!ctx.flow().isClientbound())
        {
            ctx.enqueueWork(() ->
            {
                Player player = ctx.player();
                if (message.flag == CLIENT_REQUIRES_IMAGE)
                    LoadingImagesServerManager.INSTANCE.sendImageToClient((ServerPlayer) player, message.name, message.duration);
                else if (message.flag == CLIENT_IS_READY)
                    LoadingImagesServerManager.INSTANCE.checkAndSendReadyToClients((ServerPlayer) player, false);
            });
        }
        else
        {
            ctx.enqueueWork(() ->
            {
                try
                {
                    boolean rejectImages = AVAClientConfig.REJECT_SERVER_DISPLAY_IMAGE.get();
                    LoadingImagesClientManager manager = LoadingImagesClientManager.INSTANCE;
                    if (message.flag == SERVER_ASK_CLIENT && !rejectImages)
                    {
                        AVA.LOGGER.info("Server tried to display image: " + message.name);
                        if (!manager.images.containsKey(message.name))
                        {
                            if (message.pixels == null)
                            {
                                AVA.LOGGER.info("No image found locally for: " + message.name + ", requesting from server");
                                PacketDistributor.sendToServer(new DisplayLoadingImageMessage(CLIENT_REQUIRES_IMAGE, message.name, null, message.duration));
                                return;
                            }
                        }
                    }
                    else if (message.flag == SERVER_IMAGE_TO_CLIENT && !rejectImages)
                    {
                        AVA.LOGGER.info("Received image sent from server: " + message.name + " caching locally");
                        Path download = LoadingImagesManager.loadingImagesDirectory(Minecraft.getInstance().gameDirectory.toPath()).resolve(message.name + ".png");
                        BufferedImage image = new BufferedImage(message.pixels.length, message.pixels[0].length, BufferedImage.TYPE_INT_ARGB);
                        for (int row = 0; row < message.pixels.length; row++)
                            for (int col = 0; col < message.pixels[row].length; col++)
                                image.setRGB(row, col, message.pixels[row][col]);
                        ImageIO.write(image, "png", download.toFile());
                        manager.images.put(message.name, Lazy.of(() -> message.pixels));
                    }
                    if (message.name != null && !message.name.isEmpty())
                    {
                        AVA.LOGGER.info("Displaying image: " + message.name);
                        manager.display(message.name, Integer.MAX_VALUE);
                    }
                    if (message.flag == SERVER_ALL_READY)
                    {
                        AVA.LOGGER.info("All players are ready, counting down from " + (message.duration / 20) + "s");
                        manager.displayingDuration = message.duration;
                    }
                    else
                    {
                        AVA.LOGGER.info("Client is ready, notifying to server");
                        PacketDistributor.sendToServer(new DisplayLoadingImageMessage(CLIENT_IS_READY, message.name, null, -1));
                    }
                }
                catch (Exception e)
                {
                    AVA.LOGGER.error("Error happened during loading image: " + message.name + ", operation: " + message.flag);
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
