package pellucid.ava.packets;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import pellucid.ava.AVA;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;

public class PlaySoundMessage implements CustomPacketPayload
{
    public static final Type<PlaySoundMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "play_sound"));

    private final String[] sounds;
    private final boolean includeLocal;

    public PlaySoundMessage(FriendlyByteBuf buffer)
    {
        int length = DataTypes.INT.read(buffer);
        this.sounds = new String[length];
        for (int i = 0; i < length; i++)
            sounds[i] = DataTypes.STRING.read(buffer);
        this.includeLocal = DataTypes.BOOLEAN.read(buffer);
    }

    public PlaySoundMessage(boolean includeLocal, String... sounds)
    {
        this.sounds = sounds;
        this.includeLocal = includeLocal;
    }

    public void write(FriendlyByteBuf buffer)
    {
        DataTypes.INT.write(buffer, sounds.length);
        for (String sound : sounds)
            DataTypes.STRING.write(buffer, sound);
        DataTypes.BOOLEAN.write(buffer, includeLocal);
    }
    
    public static PlaySoundMessage decode(RegistryFriendlyByteBuf packetBuffer)
    {
        int length = DataTypes.INT.read(packetBuffer);
        String[] sounds = new String[length];
        for (int i = 0; i < length; i++)
            sounds[i] = DataTypes.STRING.read(packetBuffer);
        return new PlaySoundMessage(DataTypes.BOOLEAN.read(packetBuffer), sounds);
    }

    public static void handle(PlaySoundMessage message, IPayloadContext ctx)
    {
        ctx.enqueueWork(() -> {
            Player player = ctx.player();
            try
            {
                for (String sound : message.sounds)
                    AVAWeaponUtil.playAttenuableSoundToClient(BuiltInRegistries.SOUND_EVENT.get(new ResourceLocation(sound)), player, true, (p) -> !message.includeLocal && p == player);
            }
            catch (Exception e)
            {
                AVA.LOGGER.error("Sound does not exist, this should not happen!");
            }
        });
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
