package pellucid.ava.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import pellucid.ava.util.DataTypes;

public abstract class TypedMessage implements CustomPacketPayload
{
    protected final int type;

    public TypedMessage(FriendlyByteBuf buffer)
    {
        this(DataTypes.INT.read(buffer));
    }

    protected TypedMessage(int type)
    {
        this.type = type;
    }

    public void write(FriendlyByteBuf buffer)
    {
        DataTypes.INT.write(buffer, type);
    }
}
