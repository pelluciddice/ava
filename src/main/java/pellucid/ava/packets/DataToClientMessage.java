package pellucid.ava.packets;

import com.mojang.blaze3d.platform.NativeImage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.network.PacketDistributor;
import net.neoforged.neoforge.network.handling.IPayloadContext;
import net.neoforged.neoforge.server.ServerLifecycleHooks;
import pellucid.ava.AVA;
import pellucid.ava.blocks.command_executor_block.CommandExecutorScreen;
import pellucid.ava.blocks.command_executor_block.CommandExecutorTE;
import pellucid.ava.cap.AVACrossWorldData;
import pellucid.ava.cap.AVAWorldData;
import pellucid.ava.cap.PlayerAction;
import pellucid.ava.client.renderers.AVARenderer;
import pellucid.ava.client.renderers.HUDIndicators;
import pellucid.ava.client.renderers.environment.ActivePingEffect;
import pellucid.ava.commands.RecoilRefundTypeCommand;
import pellucid.ava.competitive_mode.CompetitiveModeClient;
import pellucid.ava.config.AVAConfig;
import pellucid.ava.gamemodes.modes.GameMode;
import pellucid.ava.gamemodes.modes.GameModes;
import pellucid.ava.gamemodes.scoreboard.AVAPlayerStat;
import pellucid.ava.gamemodes.scoreboard.AVAScoreboardManager;
import pellucid.ava.gamemodes.scoreboard.Timer;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;
import pellucid.ava.util.AVAConstants;
import pellucid.ava.util.AVAWeaponUtil;
import pellucid.ava.util.DataTypes;
import pellucid.ava.util.Pair;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public class DataToClientMessage extends TypedMessage
{
    public static final Type<DataToClientMessage> TYPE = new Type<>(new ResourceLocation(AVA.MODID, "data_to_client"));
    public static final int GAMEMODE = 0;
    public static final int TIMER = 1;
    public static final int SCOREBOARD_SCORE = 2;
    public static final int SCREEN_SHAKE = 3;
    public static final int BROADCAST_TEXT = 4;
    public static final int CROSS_WORLD = 5;
    public static final int FLASH = 6;
    public static final int SITE_POS = 7;
    public static final int WEAPON_STATS = 8;
    public static final int SERVER_CONFIG = 9;
    public static final int ARMOUR_VALUE = 10;
    public static final int WORLD = 11;
    public static final int SCOREBOARD_MAP = 12;
    public static final int PING = 13;
    public static final int OPEN_SCREEN_COMMAND_EXECUTOR = 14;
    public static final int REQUEST_CLIENT_PRESET = 15;
    public static final int PLAYER_ACTVIATED_PARACHUTE = 16;
    public static final int SCOREBOARD = 17;
    public static final int PLAYMODE = 18;
    public static final int PLAYER_BOOSTS = 19;
    public static final int RECOIL_REFUND_TYPE = 20;
    public static final int CROSSHAIR = 21;
    public static final int UAV = 22;

    private final CompoundTag data;

    public static void broadcastText(String text, boolean key, int duration, @Nullable Collection<ServerPlayer> players, String... args)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.STRING.write(compound, "text", text);
        DataTypes.BOOLEAN.write(compound, "key", key);
        DataTypes.INT.write(compound, "duration", duration);
        if (args != null)
        {
            ListTag list = new ListTag();
            for (String arg : args)
                list.add(StringTag.valueOf(arg));
            compound.put("args", list);
        }
        if (players != null)
        {
            for (ServerPlayer player : players)
                PacketDistributor.sendToPlayer(player, new DataToClientMessage(BROADCAST_TEXT, compound));
        }
        else
            PacketDistributor.sendToAllPlayers(new DataToClientMessage(BROADCAST_TEXT, compound));
    }

    public static void screenShake(ServerPlayer player, float weakness)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.FLOAT.write(compound, "weakness", weakness);
        PacketDistributor.sendToPlayer(player, new DataToClientMessage(SCREEN_SHAKE, compound));
    }

    public static void scoreboardScore()
    {
        CompoundTag compound = new CompoundTag();
        ListTag list = new ListTag();
        AVACrossWorldData.getInstance().scoreboardManager.getStats().forEach((id, stat) -> {
            CompoundTag compound2 = new CompoundTag();
            DataTypes.UUID.write(compound2, "id", id);
            compound2.put("stat", stat.write());
            list.add(compound2);
        });
        compound.put("stats", list);
        PacketDistributor.sendToAllPlayers(new DataToClientMessage(SCOREBOARD_SCORE, compound));
    }

    public static void timer()
    {
        CompoundTag compound = new CompoundTag();
        compound.put("timers", Timer.write(AVACrossWorldData.getInstance().timers));
        PacketDistributor.sendToAllPlayers(new DataToClientMessage(TIMER, compound));
    }

    public static void gamemode()
    {
        GameMode mode = AVACrossWorldData.getInstance().scoreboardManager.getGamemode();
        CompoundTag compound = mode.serializeNBT();
        DataTypes.STRING.write(compound, "mode", mode.getName());
        PacketDistributor.sendToAllPlayers(new DataToClientMessage(GAMEMODE, compound));
    }

    public static void crossWorld(ServerPlayer player)
    {
        Level world = ServerLifecycleHooks.getCurrentServer().overworld();
        if (player == null)
            PacketDistributor.sendToAllPlayers(new DataToClientMessage(CROSS_WORLD, AVACrossWorldData.getInstance().save(new CompoundTag(), world.registryAccess())));
        else
            PacketDistributor.sendToPlayer(player, new DataToClientMessage(CROSS_WORLD, AVACrossWorldData.getInstance().save(new CompoundTag(), world.registryAccess())));
    }

    public static void flash(ServerPlayer player, int duration)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "duration", duration);
        PacketDistributor.sendToPlayer(player, new DataToClientMessage(FLASH, compound));
    }

    public static void sitePos(ServerLevel world)
    {
        CompoundTag compound = new CompoundTag();
        Pair<BlockPos, BlockPos> pos = AVAWorldData.getInstance(world).sitesMark;
        if (pos.hasA())
            DataTypes.BLOCKPOS.write(compound, "aPos", pos.getA());
        if (pos.hasB())
            DataTypes.BLOCKPOS.write(compound, "bPos", pos.getB());
        PacketDistributor.sendToPlayersInDimension(world, new DataToClientMessage(SITE_POS, compound));
    }

    public static void weaponStats(Consumer<DataToClientMessage> packet)
    {
        CompoundTag compound = new CompoundTag();
        ListTag list = new ListTag();
        for (AVAItemGun item : AVAWeaponUtil.getAllGuns())
        {
            CompoundTag compound2 = new CompoundTag();
            DataTypes.STRING.write(compound2, "item", BuiltInRegistries.ITEM.getKey(item).toString());
            DataTypes.COMPOUND.write(compound2, "stats", item.getStats().serializeNBT());
            list.add(compound2);
        }
        compound.put("list", list);
        packet.accept(new DataToClientMessage(WEAPON_STATS, compound));
    }

    public static void serverConfig(ServerPlayer player)
    {
        ListTag list = new ListTag();
        for (ModConfigSpec.ConfigValue<?> field : AVAConfig.getServerFields())
        {
            CompoundTag compound = new CompoundTag();
            AVACommonUtil.getType(field).write(compound, "value", field.get());
            list.add(compound);
        }
        CompoundTag compound = new CompoundTag();
        compound.put("list", list);
        if (player == null)
            PacketDistributor.sendToAllPlayers(new DataToClientMessage(SERVER_CONFIG, compound));
        else
            PacketDistributor.sendToPlayer(player, new DataToClientMessage(SERVER_CONFIG, compound));
    }

    public static void armourValue(ServerPlayer player)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.FLOAT.write(compound, "value", PlayerAction.getCap(player).getArmourValue());
        PacketDistributor.sendToPlayer(player, new DataToClientMessage(ARMOUR_VALUE, compound));
    }


    public static void world(ServerLevel world, Consumer<DataToClientMessage> packet)
    {
        world(AVAWorldData.getInstance(world), world, packet);
    }

    public static void world(AVAWorldData data, ServerLevel world, Consumer<DataToClientMessage> packet)
    {
        CompoundTag compound = data.saveToClient(world, world.registryAccess());
        DataTypes.STRING.write(compound, "world", world.dimension().location().toString());
        packet.accept(new DataToClientMessage(WORLD, compound));
    }

    public static void scoreboardMap(ServerPlayer player)
    {
        if (player == null)
            PacketDistributor.sendToAllPlayers(new DataToClientMessage(SCOREBOARD_MAP, AVACrossWorldData.getInstance().scoreboardManager.writeMap()));
        else
            PacketDistributor.sendToPlayer(player, new DataToClientMessage(SCOREBOARD_MAP, AVACrossWorldData.getInstance().scoreboardManager.writeMap()));
    }

    public static void ping(Consumer<DataToClientMessage> packet, Vec3 pos, ActivePingEffect.Type type)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.FLOAT.write(compound, "x", (float) pos.x);
        DataTypes.FLOAT.write(compound, "y", (float) pos.y);
        DataTypes.FLOAT.write(compound, "z", (float) pos.z);
        DataTypes.INT.write(compound, "type", type.ordinal());
        packet.accept(new DataToClientMessage(PING, compound));
    }

    public static void openCommandExecutorScreen(ServerPlayer player, BlockPos pos)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.BLOCKPOS.write(compound, "pos", pos);
        PacketDistributor.sendToPlayer(player, new DataToClientMessage(OPEN_SCREEN_COMMAND_EXECUTOR, compound));
    }

    public static void requestClientPreset(ServerPlayer player)
    {
        PacketDistributor.sendToPlayer(player, new DataToClientMessage(REQUEST_CLIENT_PRESET, new CompoundTag()));
    }

    public static void playerActivatedParachute(ServerPlayer player, boolean parachuted)
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "id", player.getId());
        DataTypes.BOOLEAN.write(compound, "parachuted", parachuted);
        PacketDistributor.sendToPlayersTrackingEntity(player, new DataToClientMessage(PLAYER_ACTVIATED_PARACHUTE, compound));
    }

    public static void scoreboard()
    {
        PacketDistributor.sendToAllPlayers(new DataToClientMessage(SCOREBOARD, AVACrossWorldData.getInstance().scoreboardManager.serializeNBT()));
    }

    public static void playMode(Consumer<DataToClientMessage> packet)
    {
        packet.accept(new DataToClientMessage(PLAYMODE, AVACrossWorldData.getInstance().playMode.serializeNBT()));
    }

    public static void playerBoosts(ServerPlayer player)
    {
        PacketDistributor.sendToPlayer(player, new DataToClientMessage(PLAYER_BOOSTS, PlayerAction.getCap(player).writeBoosts(new CompoundTag())));
    }

    public static void recoilRefundType()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.INT.write(compound, "type", AVACrossWorldData.getInstance().recoilRefundType.ordinal());
        PacketDistributor.sendToAllPlayers(new DataToClientMessage(RECOIL_REFUND_TYPE, compound));
    }

    public static void crosshair()
    {
        CompoundTag compound = new CompoundTag();
        DataTypes.BOOLEAN.write(compound, "crosshair", AVACrossWorldData.getInstance().shouldRenderCrosshair);
        PacketDistributor.sendToAllPlayers(new DataToClientMessage(CROSSHAIR, compound));
    }

    public static void uav(ServerLevel world)
    {
        PacketDistributor.sendToPlayersInDimension(world, new DataToClientMessage(UAV, AVAWorldData.getInstance(world).saveUAVs(world, new CompoundTag())));
    }

    public DataToClientMessage(RegistryFriendlyByteBuf buffer)
    {
        this(DataTypes.INT.read(buffer), DataTypes.COMPOUND.read(buffer));
    }

    private DataToClientMessage(int type, CompoundTag data)
    {
        super(type);
        this.data = data;
    }

    @Override
    public void write(FriendlyByteBuf buffer)
    {
        super.write(buffer);
        DataTypes.COMPOUND.write(buffer, data);
    }

    public static void handle(DataToClientMessage message, IPayloadContext ctx)
    {
        if (ctx.flow().isClientbound())
        {
            // Prevent sided crash
            ctx.enqueueWork(new Runnable() {
                @Override
                public void run()
                {
                    AVACrossWorldData data = AVACrossWorldData.getInstance();
                    AVAScoreboardManager scoreboard = data.scoreboardManager;
                    Player player = ctx.player();
                    Level world = player.level();
                    switch (message.type)
                    {
                        case GAMEMODE ->
                        {
                            scoreboard.setGamemode(GameModes.GAMEMODES.get(DataTypes.STRING.read(message.data, "mode")));
                            scoreboard.getGamemode().deserializeNBT(message.data);
                        }
                        case TIMER ->
                        {
                            AVACrossWorldData crossData = AVACrossWorldData.getInstance();
                            crossData.timers.clear();
                            crossData.timers.addAll(Timer.read(message.data.getList("timers", Tag.TAG_COMPOUND)));
                        }
                        case SCOREBOARD_SCORE ->
                        {
                            AVACrossWorldData crossData = AVACrossWorldData.getInstance();
                            Map<UUID, AVAPlayerStat> stats = crossData.scoreboardManager.getStats();
                            stats.clear();
                            for (Tag tag : message.data.getList("stats", Tag.TAG_COMPOUND))
                            {
                                CompoundTag c = (CompoundTag) tag;
                                stats.put(DataTypes.UUID.read(c, "id"), new AVAPlayerStat(DataTypes.COMPOUND.read(c, "stat")));
                            }
                        }
                        case SCREEN_SHAKE ->
                        {
                            AVAClientUtil.CAMERA_VERTICAL_SHAKE_TIME = AVAClientUtil.CAMERA_VERTICAL_SHAKE_TIME_MAX;
                            AVAClientUtil.CAMERA_VERTICAL_SHAKE_WEAKNESS = message.data.getFloat("weakness");
                        }
                        case BROADCAST_TEXT ->
                        {
                            String text = DataTypes.STRING.read(message.data, "text");
                            List<String> args = new ArrayList<>();
                            if (message.data.contains("args"))
                            {
                                ListTag list = message.data.getList("args", Tag.TAG_STRING);
                                for (Tag tag : list)
                                    args.add(tag.getAsString());
                            }
                            HUDIndicators.BroadCastText.setKey(DataTypes.BOOLEAN.read(message.data, "key") ? Component.translatable(text, (Object[]) args.toArray(new String[0])).getString() : text, DataTypes.INT.read(message.data, "duration"));
                        }
                        case CROSS_WORLD ->
                        {
                            AVACrossWorldData.getInstance().load(message.data);
                        }
                        case WEAPON_STATS ->
                        {
                            ListTag list = message.data.getList("list", Tag.TAG_COMPOUND);
                            for (Tag tag : list)
                            {
                                CompoundTag compound = (CompoundTag) tag;
                                if (BuiltInRegistries.ITEM.get(new ResourceLocation(compound.getString("item"))) instanceof AVAItemGun gun)
                                    gun.getStats().deserializeNBT(compound.getCompound("stats"));
                                else
                                    AVA.LOGGER.error("Received incomplete gun stat from server...... this should not happen!");
                            }
                        }
                        case SERVER_CONFIG ->
                        {
                            try
                            {
                                ListTag list = message.data.getList("list", Tag.TAG_COMPOUND);
                                List<ModConfigSpec.ConfigValue<?>> fields = AVAConfig.getServerFields();
                                for (int i = 0; i < list.size(); i++)
                                {
                                    CompoundTag compound = list.getCompound(i);
                                    ModConfigSpec.ConfigValue field = fields.get(i);
                                    field.set(AVACommonUtil.getType(field).read(compound, "value"));
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        case SCOREBOARD_MAP ->
                        {
                            AVAScoreboardManager manager = AVACrossWorldData.getInstance().scoreboardManager;
                            manager.updateMap(message.data);
                            if (AVARenderer.MAP_TEXTURE != null)
                                AVARenderer.MAP_TEXTURE.close();
                            if (manager.hasMap())
                            {
                                AVARenderer.MAP_TEXTURE = new DynamicTexture(manager.getMapWidth(), manager.getMapHeight(), true);
                                NativeImage pixels = AVARenderer.MAP_TEXTURE.getPixels();
                                if (pixels != null)
                                {
                                    for (int y = 0; y < manager.getMapHeight(); y++)
                                        for (int x = 0; x < manager.getMapWidth(); x++)
                                            pixels.setPixelRGBA(x, y, MapColor.getColorFromPackedId(manager.getMapColours().get(x + y * manager.getMapWidth())));
                                    AVARenderer.MAP_TEXTURE.upload();
                                    AVARenderer.MAP_RENDER_TYPE = RenderType.textSeeThrough(Minecraft.getInstance().textureManager.register(AVA.MODID + "_" + "scoreboard_map", AVARenderer.MAP_TEXTURE));
                                }
                            }
                        }
                        case WORLD ->
                        {
                            AVAWorldData.getInstance(ResourceKey.create(Registries.DIMENSION, new ResourceLocation(DataTypes.STRING.read(message.data, "world"))), false).load(message.data, null);
                        }
                        case SCOREBOARD ->
                        {
                            AVACrossWorldData.getInstance().scoreboardManager.deserializeNBT(message.data);
                        }
                        case PLAYMODE ->
                        {
                            AVACrossWorldData.getInstance().playMode.deserializeNBT(message.data);
                        }
                        case RECOIL_REFUND_TYPE ->
                        {
                            AVACrossWorldData.getInstance().recoilRefundType = RecoilRefundTypeCommand.RefundType.values()[DataTypes.INT.read(message.data, "type")];
                        }
                        case CROSSHAIR ->
                        {
                            AVACrossWorldData.getInstance().shouldRenderCrosshair = DataTypes.BOOLEAN.read(message.data, "crosshair");
                        }
                        case UAV ->
                        {
                            AVAWorldData.getInstance(world).readUAVs(message.data);
                        }
                    }
                    switch (message.type)
                    {
                        case FLASH ->
                        {
                            PlayerAction.getCap(player).setFlashDuration(DataTypes.INT.read(message.data, "duration"));
                        }
                        case SITE_POS ->
                        {
                            AVAWorldData.getInstance(player.level()).sitesMark.set(DataTypes.BLOCKPOS.readSafe(message.data, "aPos", null), DataTypes.BLOCKPOS.readSafe(message.data, "bPos", null));
                        }
                        case ARMOUR_VALUE ->
                        {
                            PlayerAction.getCap(player).setArmourValue(DataTypes.FLOAT.read(message.data, "value"));
                        }
                        case PING ->
                        {
                            Vec3 pos = new Vec3(DataTypes.FLOAT.read(message.data, "x"), DataTypes.FLOAT.read(message.data, "y"), DataTypes.FLOAT.read(message.data, "z"));
                            int type = DataTypes.INT.read(message.data, "type");
                            AVAWorldData.getInstance(player.level()).activePings.add(new ActivePingEffect(pos, ActivePingEffect.Type.values()[type]));
                        }
                        case OPEN_SCREEN_COMMAND_EXECUTOR ->
                        {
                            BlockPos pos = DataTypes.BLOCKPOS.read(message.data, "pos");
                            if (player.level().getBlockEntity(pos) instanceof CommandExecutorTE te)
                                Minecraft.getInstance().setScreen(new CommandExecutorScreen(te));
                        }
                        case REQUEST_CLIENT_PRESET ->
                        {
                            CompetitiveModeClient.calculateAndSendPreset();
                            player.getPersistentData().putBoolean(AVAConstants.TAG_ENTITY_PARACHUTED, false);
                        }
                        case PLAYER_ACTVIATED_PARACHUTE ->
                        {
                            Entity entity = player.level().getEntity(DataTypes.INT.read(message.data, "id"));
                            if (entity instanceof Player)
                                PlayerAction.getCap((Player) entity).setIsUsingParachute(DataTypes.BOOLEAN.read(message.data, "parachuted"));
                        }
                        case PLAYER_BOOSTS ->
                        {
                            PlayerAction.getCap(player).readBoosts(message.data);
                        }
                    }
                }
            });
        }
    }

    @Override
    public Type<? extends CustomPacketPayload> type()
    {
        return TYPE;
    }
}
