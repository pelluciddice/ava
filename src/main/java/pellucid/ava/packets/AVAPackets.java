package pellucid.ava.packets;

import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.neoforged.neoforge.network.event.RegisterPayloadHandlersEvent;
import net.neoforged.neoforge.network.registration.PayloadRegistrar;
import pellucid.ava.AVA;

public class AVAPackets
{
    public static void registerMessages(RegisterPayloadHandlersEvent event)
    {
        PayloadRegistrar registrar = event.registrar(AVA.MODID).versioned("1.0.0").optional();
        registrar.playToClient(DataToClientMessage.TYPE, CustomPacketPayload.codec(DataToClientMessage::write, DataToClientMessage::new), DataToClientMessage::handle);
        registrar.playToClient(PlaySoundToClientMessage.TYPE, CustomPacketPayload.codec(PlaySoundToClientMessage::write, PlaySoundToClientMessage::new), PlaySoundToClientMessage::handle);
        registrar.commonBidirectional(DataToServerMessage.TYPE, CustomPacketPayload.codec(DataToServerMessage::write, DataToServerMessage::new), DataToServerMessage::handle);
        registrar.commonBidirectional(PlayerActionMessage.TYPE, CustomPacketPayload.codec(PlayerActionMessage::write, PlayerActionMessage::new), PlayerActionMessage::handle);
        registrar.commonBidirectional(PlaySoundMessage.TYPE, CustomPacketPayload.codec(PlaySoundMessage::write, PlaySoundMessage::new), PlaySoundMessage::handle);
        registrar.commonBidirectional(RadioMessage.TYPE, CustomPacketPayload.codec(RadioMessage::write, RadioMessage::new), RadioMessage::handle);
        registrar.commonBidirectional(LivingDamageMessage.TYPE, CustomPacketPayload.codec(LivingDamageMessage::write, LivingDamageMessage::new), LivingDamageMessage::handle);
        registrar.commonBidirectional(UpdateCommandExecutorMessage.TYPE, CustomPacketPayload.codec(UpdateCommandExecutorMessage::write, UpdateCommandExecutorMessage::new), UpdateCommandExecutorMessage::handle);
        registrar.commonBidirectional(DisplayLoadingImageMessage.TYPE, CustomPacketPayload.codec(DisplayLoadingImageMessage::write, DisplayLoadingImageMessage::new), DisplayLoadingImageMessage::handle);
    }
}
