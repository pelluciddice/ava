// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


import pellucid.ava.client.renderers.ModelRenderer;

public class custom_model extends EntityModel<Entity> {
	private final ModelRenderer bipedHead;
	private final ModelRenderer bipedBody;
	private final ModelRenderer bipedLeftArm;
	private final ModelRenderer bipedRightArm;
	private final ModelRenderer bipedLeftLeg;
	private final ModelRenderer bipedRightLeg;

	public custom_model() {
		textureWidth = 64;
		textureHeight = 64;

		bipedHead = new ModelRenderer(this);
		bipedHead.setRotationPoint(0.0F, -1.5293F, -0.9603F);
		bipedHead.setTextureOffset(24, 0).addBox(-3.0F, -3.4707F, -2.0397F, 6.0F, 3.0F, 5.0F, 0.0F, false);
		bipedHead.setTextureOffset(42, 8).addBox(-1.5F, -3.4707F, -1.0397F, 3.0F, 5.0F, 3.0F, 0.0F, false);
		bipedHead.setTextureOffset(12, 41).addBox(-2.5F, -6.6465F, -0.0397F, 5.0F, 4.0F, 3.0F, 0.0F, false);
		bipedHead.setTextureOffset(44, 36).addBox(-1.0F, -6.4707F, -2.9421F, 2.0F, 3.0F, 3.0F, 0.0F, false);
		bipedHead.setTextureOffset(19, 0).addBox(-1.5F, -3.4707F, -3.3991F, 3.0F, 3.0F, 2.0F, 0.0F, false);
		bipedHead.setTextureOffset(32, 27).addBox(-2.0F, -5.4707F, -1.0397F, 4.0F, 4.0F, 5.0F, 0.0F, false);

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, -1.1667F, -0.5833F);
		bipedBody.setTextureOffset(0, 0).addBox(-3.5F, 1.1667F, -2.4167F, 7.0F, 11.0F, 5.0F, 0.0F, false);
		bipedBody.setTextureOffset(44, 16).addBox(-1.5F, 12.1667F, -2.4167F, 3.0F, 3.0F, 4.0F, 0.0F, false);
		bipedBody.setTextureOffset(22, 10).addBox(-3.0F, 3.1667F, -2.9167F, 6.0F, 4.0F, 6.0F, 0.0F, false);
		bipedBody.setTextureOffset(0, 16).addBox(-4.0F, 1.1667F, -2.9167F, 8.0F, 2.0F, 6.0F, 0.0F, false);
		bipedBody.setTextureOffset(12, 48).addBox(-3.5F, -0.3333F, -2.4167F, 7.0F, 2.0F, 5.0F, 0.0F, false);
		bipedBody.setTextureOffset(28, 20).addBox(-2.5F, 7.1667F, -2.9167F, 5.0F, 1.0F, 6.0F, 0.0F, false);

		bipedLeftArm = new ModelRenderer(this);
		bipedLeftArm.setRotationPoint(5.0F, 2.75F, -0.25F);
		bipedLeftArm.setTextureOffset(24, 12).addBox(-0.5F, 9.25F, -1.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(12, 24).addBox(0.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(0, 0).addBox(-1.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(44, 49).addBox(-1.5F, 5.25F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(32, 40).addBox(-1.0F, 2.25F, -1.75F, 3.0F, 3.0F, 3.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(32, 46).addBox(-1.5F, -1.75F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);

		bipedRightArm = new ModelRenderer(this);
		bipedRightArm.setRotationPoint(-5.0F, 2.75F, -0.25F);
		bipedRightArm.setTextureOffset(44, 49).addBox(-1.5F, 5.25F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(32, 46).addBox(-1.5F, -1.75F, -1.75F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(32, 40).addBox(-2.0F, 2.25F, -1.75F, 3.0F, 3.0F, 3.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(0, 24).addBox(-0.5F, 9.25F, -1.75F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(24, 8).addBox(-1.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(0, 16).addBox(0.5F, 9.25F, 0.25F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		bipedLeftLeg = new ModelRenderer(this);
		bipedLeftLeg.setRotationPoint(3.5F, 11.5F, -0.5F);
		bipedLeftLeg.setTextureOffset(16, 24).addBox(-2.0F, -0.5F, -2.0F, 4.0F, 13.0F, 4.0F, 0.0F, false);

		bipedRightLeg = new ModelRenderer(this);
		bipedRightLeg.setRotationPoint(-3.5F, 11.5F, -0.5F);
		bipedRightLeg.setTextureOffset(0, 24).addBox(-2.0F, -0.5F, -2.0F, 4.0F, 13.0F, 4.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bipedHead.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedBody.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedLeftArm.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedRightArm.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedLeftLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedRightLeg.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}