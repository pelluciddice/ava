// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


import pellucid.ava.client.renderers.ModelRenderer;

public class custom_model extends EntityModel<Entity> {
	private final ModelRenderer bipedHead;
	private final ModelRenderer bipedBody;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer bipedLeftArm;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer bipedRightArm;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer bipedLeftLeg;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer bipedRightLeg;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;

	public custom_model() {
		textureWidth = 128;
		textureHeight = 128;

		bipedHead = new ModelRenderer(this);
		bipedHead.setRotationPoint(0.0F, -33.5F, 1.0F);
		bipedHead.setTextureOffset(16, 85).addBox(-3.0F, -8.5F, -10.0F, 6.0F, 11.0F, 2.0F, 0.0F, false);
		bipedHead.setTextureOffset(66, 22).addBox(-4.0F, -9.5F, -8.0F, 8.0F, 13.0F, 6.0F, 0.0F, false);
		bipedHead.setTextureOffset(32, 12).addBox(-3.0F, -2.5F, -2.0F, 6.0F, 5.0F, 2.0F, 0.0F, false);

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, -33.5016F, 0.6738F);
		bipedBody.setTextureOffset(0, 0).addBox(-15.0F, -2.4984F, 0.2871F, 30.0F, 5.0F, 7.0F, 0.0F, false);
		bipedBody.setTextureOffset(0, 56).addBox(-6.0F, 2.5016F, -0.6738F, 12.0F, 21.0F, 8.0F, 0.0F, false);
		bipedBody.setTextureOffset(72, 41).addBox(-2.0F, 23.5016F, -0.6738F, 4.0F, 5.0F, 8.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(0.0F, 57.5016F, -0.6738F);
		bipedBody.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, 0.0F, 0.1222F);
		cube_r1.setTextureOffset(0, 12).addBox(-10.176F, -56.4906F, 0.0F, 12.0F, 22.0F, 8.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(0.0F, 57.5016F, -0.6738F);
		bipedBody.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, 0.0F, 0.7854F);
		cube_r2.setTextureOffset(68, 57).addBox(-33.2772F, -49.4641F, 0.9648F, 5.0F, 11.0F, 7.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(0.0F, 57.5016F, -0.6738F);
		bipedBody.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.0F, 0.0F, -0.7854F);
		cube_r3.setTextureOffset(74, 0).addBox(28.2772F, -49.4641F, 0.9648F, 5.0F, 11.0F, 7.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(0.0F, 57.5016F, -0.6738F);
		bipedBody.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 0.0F, -0.1222F);
		cube_r4.setTextureOffset(32, 34).addBox(-1.824F, -56.4906F, 0.0F, 12.0F, 22.0F, 8.0F, 0.0F, false);

		bipedLeftArm = new ModelRenderer(this);
		bipedLeftArm.setRotationPoint(15.5F, -33.6799F, 3.4163F);
		bipedLeftArm.setTextureOffset(35, 83).addBox(-0.5F, -5.3201F, -1.4163F, 5.0F, 10.0F, 5.0F, 0.0F, true);
		bipedLeftArm.setTextureOffset(86, 75).addBox(0.5F, 4.6799F, -0.4163F, 3.0F, 12.0F, 3.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-15.5F, 57.6799F, -3.4163F);
		bipedLeftArm.addChild(cube_r5);
		setRotationAngle(cube_r5, -2.0508F, 0.0F, 0.0F);
		cube_r5.setTextureOffset(86, 90).addBox(17.0F, 27.0835F, -40.7932F, 1.0F, 17.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(92, 54).addBox(17.0F, 27.0835F, -36.7932F, 1.0F, 11.0F, 3.0F, 0.0F, false);
		cube_r5.setTextureOffset(55, 86).addBox(17.0F, 22.0835F, -40.7932F, 1.0F, 5.0F, 7.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 85).addBox(15.5F, 13.2793F, -39.2059F, 4.0F, 9.0F, 4.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(-15.5F, 57.6799F, -3.4163F);
		bipedLeftArm.addChild(cube_r6);
		setRotationAngle(cube_r6, -2.7053F, 0.0F, 0.0F);
		cube_r6.setTextureOffset(92, 90).addBox(17.0F, 50.7714F, -5.6132F, 1.0F, 9.0F, 2.0F, 0.0F, false);

		bipedRightArm = new ModelRenderer(this);
		bipedRightArm.setRotationPoint(-15.5F, -33.6799F, 4.4163F);
		bipedRightArm.setTextureOffset(35, 83).addBox(-4.5F, -5.3201F, -2.4163F, 5.0F, 10.0F, 5.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(86, 75).addBox(-3.5F, 4.6799F, -1.4163F, 3.0F, 12.0F, 3.0F, 0.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(15.5F, 57.6799F, -4.4163F);
		bipedRightArm.addChild(cube_r7);
		setRotationAngle(cube_r7, -2.0508F, 0.0F, 0.0F);
		cube_r7.setTextureOffset(86, 90).addBox(-18.0F, 27.0183F, -40.7848F, 1.0F, 17.0F, 2.0F, 0.0F, false);
		cube_r7.setTextureOffset(92, 54).addBox(-18.0F, 27.0183F, -36.7848F, 1.0F, 11.0F, 3.0F, 0.0F, false);
		cube_r7.setTextureOffset(55, 86).addBox(-18.0F, 22.0183F, -40.7848F, 1.0F, 5.0F, 7.0F, 0.0F, false);
		cube_r7.setTextureOffset(0, 85).addBox(-19.5F, 13.2141F, -39.1976F, 4.0F, 9.0F, 4.0F, 0.0F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(15.5F, 57.6799F, -4.4163F);
		bipedRightArm.addChild(cube_r8);
		setRotationAngle(cube_r8, -2.7053F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(92, 90).addBox(-18.0F, 50.6977F, -5.6281F, 1.0F, 9.0F, 2.0F, 0.0F, false);

		bipedLeftLeg = new ModelRenderer(this);
		bipedLeftLeg.setRotationPoint(-6.0F, -10.0407F, 2.39F);
		bipedLeftLeg.setTextureOffset(40, 12).addBox(-4.0F, 0.0407F, -2.39F, 8.0F, 8.0F, 8.0F, 0.0F, true);
		bipedLeftLeg.setTextureOffset(0, 42).addBox(-3.0F, 30.9626F, -6.5033F, 6.0F, 3.0F, 9.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(6.0F, 34.0407F, -2.39F);
		bipedLeftLeg.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.0873F, 0.0F, 0.0F);
		cube_r9.setTextureOffset(68, 75).addBox(-8.5F, -14.8802F, 0.6693F, 5.0F, 14.0F, 4.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(6.0F, 34.0407F, -2.39F);
		bipedLeftLeg.addChild(cube_r10);
		setRotationAngle(cube_r10, -0.1309F, 0.0F, 0.0F);
		cube_r10.setTextureOffset(40, 64).addBox(-9.5F, -26.7437F, -3.0016F, 7.0F, 12.0F, 7.0F, 0.0F, false);

		bipedRightLeg = new ModelRenderer(this);
		bipedRightLeg.setRotationPoint(6.0F, -10.0407F, 2.39F);
		bipedRightLeg.setTextureOffset(40, 12).addBox(-4.0F, 0.0407F, -2.39F, 8.0F, 8.0F, 8.0F, 0.0F, false);
		bipedRightLeg.setTextureOffset(0, 42).addBox(-3.0F, 30.9626F, -6.5033F, 6.0F, 3.0F, 9.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(0.0F, 19.0F, 0.0F);
		bipedRightLeg.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.0873F, 0.0F, 0.0F);
		cube_r11.setTextureOffset(68, 75).addBox(-2.5F, -0.104F, -3.0211F, 5.0F, 14.0F, 4.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(0.0F, 19.0F, 0.0F);
		bipedRightLeg.addChild(cube_r12);
		setRotationAngle(cube_r12, -0.1309F, 0.0F, 0.0F);
		cube_r12.setTextureOffset(40, 64).addBox(-3.5F, -11.521F, -3.4103F, 7.0F, 12.0F, 7.0F, 0.0F, true);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bipedHead.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedBody.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedLeftArm.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedRightArm.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedLeftLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedRightLeg.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}