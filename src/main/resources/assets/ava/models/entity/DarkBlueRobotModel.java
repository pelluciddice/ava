// Made with Blockbench 4.0.5
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports


import pellucid.ava.client.renderers.ModelRenderer;

public class custom_model extends EntityModel<Entity> {
	private final ModelRenderer bipedHead;
	private final ModelRenderer bipedBody;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer bipedLeftArm;
	private final ModelRenderer cube_r8;
	private final ModelRenderer rotatable2;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer bipedRightArm;
	private final ModelRenderer cube_r3;
	private final ModelRenderer rotatable;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer bipedLeftLeg;
	private final ModelRenderer cube_r13;
	private final ModelRenderer cube_r14;
	private final ModelRenderer bipedRightLeg;
	private final ModelRenderer cube_r15;
	private final ModelRenderer cube_r16;

	public custom_model() {
		textureWidth = 256;
		textureHeight = 256;

		bipedHead = new ModelRenderer(this);
		bipedHead.setRotationPoint(0.0F, -61.0F, -2.3516F);
		bipedHead.setTextureOffset(129, 79).addBox(-7.0F, -14.0F, -7.0F, 14.0F, 14.0F, 14.0F, 0.0F, false);

		bipedBody = new ModelRenderer(this);
		bipedBody.setRotationPoint(0.0F, -41.4167F, -5.2527F);
		bipedBody.setTextureOffset(0, 0).addBox(-22.0F, -19.5833F, -1.7473F, 44.0F, 19.0F, 17.0F, 0.0F, false);
		bipedBody.setTextureOffset(0, 36).addBox(-14.0F, -0.5833F, -6.7473F, 28.0F, 23.0F, 18.0F, 0.0F, false);
		bipedBody.setTextureOffset(0, 110).addBox(-7.0F, 22.4167F, -4.7473F, 14.0F, 14.0F, 16.0F, 0.0F, false);
		bipedBody.setTextureOffset(108, 22).addBox(-7.5F, -19.5833F, -14.0989F, 15.0F, 19.0F, 14.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(10.2016F, -12.0833F, -1.0002F);
		bipedBody.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, -0.6981F, 0.0F);
		cube_r1.setTextureOffset(0, 77).addBox(-10.5F, -7.5F, -8.2227F, 19.0F, 19.0F, 14.0F, 0.0F, true);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-10.2016F, -12.0833F, -1.0002F);
		bipedBody.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, 0.6981F, 0.0F);
		cube_r2.setTextureOffset(0, 77).addBox(-8.5F, -7.5F, -8.2227F, 19.0F, 19.0F, 14.0F, 0.0F, false);

		bipedLeftArm = new ModelRenderer(this);
		bipedLeftArm.setRotationPoint(19.5F, -57.4846F, -1.3998F);
		bipedLeftArm.setTextureOffset(111, 118).addBox(-0.5F, 7.4846F, -3.6002F, 13.0F, 24.0F, 13.0F, 0.0F, false);
		bipedLeftArm.setTextureOffset(75, 60).addBox(-2.5F, -8.5154F, -5.6002F, 17.0F, 16.0F, 17.0F, 0.0F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(6.0F, 26.9846F, -6.1002F);
		bipedLeftArm.addChild(cube_r8);
		setRotationAngle(cube_r8, -0.3054F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(105, 0).addBox(-3.5F, -4.5F, -3.5F, 7.0F, 7.0F, 7.0F, 0.0F, false);

		rotatable2 = new ModelRenderer(this);
		rotatable2.setRotationPoint(6.0F, 20.382F, -23.7382F);
		bipedLeftArm.addChild(rotatable2);
		setRotationAngle(rotatable2, -0.3054F, 0.0F, 0.0F);
		rotatable2.setTextureOffset(47, 131).addBox(-5.5F, -5.7679F, 2.5F, 11.0F, 11.0F, 13.0F, 0.0F, false);
		rotatable2.setTextureOffset(126, 55).addBox(-3.5F, -3.7679F, -10.5F, 7.0F, 7.0F, 13.0F, 0.0F, false);
		rotatable2.setTextureOffset(153, 42).addBox(-2.0F, -10.3929F, -7.5F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable2.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.0F, 0.0F, -1.2566F);
		cube_r9.setTextureOffset(74, 36).addBox(-2.1337F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable2.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.0F, 0.0F, -2.5133F);
		cube_r10.setTextureOffset(82, 142).addBox(-2.0827F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable2.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.0F, 0.0F, 2.5133F);
		cube_r11.setTextureOffset(150, 107).addBox(-1.9173F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable2.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.0F, 0.0F, 1.2566F);
		cube_r12.setTextureOffset(150, 142).addBox(-1.8663F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		bipedRightArm = new ModelRenderer(this);
		bipedRightArm.setRotationPoint(-19.5F, -57.4846F, -1.3998F);
		bipedRightArm.setTextureOffset(75, 60).addBox(-14.5F, -8.5154F, -5.6002F, 17.0F, 16.0F, 17.0F, 0.0F, false);
		bipedRightArm.setTextureOffset(111, 118).addBox(-12.5F, 7.4846F, -3.6002F, 13.0F, 24.0F, 13.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-6.0F, 26.9846F, -6.1002F);
		bipedRightArm.addChild(cube_r3);
		setRotationAngle(cube_r3, -0.3054F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(105, 0).addBox(-3.5F, -4.5F, -3.5F, 7.0F, 7.0F, 7.0F, 0.0F, false);

		rotatable = new ModelRenderer(this);
		rotatable.setRotationPoint(-6.0F, 20.382F, -23.7382F);
		bipedRightArm.addChild(rotatable);
		setRotationAngle(rotatable, -0.3054F, 0.0F, 0.0F);
		rotatable.setTextureOffset(47, 131).addBox(-5.5F, -5.7679F, 2.5F, 11.0F, 11.0F, 13.0F, 0.0F, false);
		rotatable.setTextureOffset(126, 55).addBox(-3.5F, -3.7679F, -10.5F, 7.0F, 7.0F, 13.0F, 0.0F, false);
		rotatable.setTextureOffset(153, 42).addBox(-2.0F, -10.3929F, -7.5F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 0.0F, -1.2566F);
		cube_r4.setTextureOffset(74, 36).addBox(-2.1337F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, 0.0F, -2.5133F);
		cube_r5.setTextureOffset(82, 142).addBox(-2.0827F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.0F, 0.0F, 2.5133F);
		cube_r6.setTextureOffset(150, 107).addBox(-1.9173F, -10.6138F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(0.0F, -0.0335F, 2.25F);
		rotatable.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.0F, 0.0F, 1.2566F);
		cube_r7.setTextureOffset(150, 142).addBox(-1.8663F, -10.4565F, -9.75F, 4.0F, 5.0F, 13.0F, 0.0F, false);

		bipedLeftLeg = new ModelRenderer(this);
		bipedLeftLeg.setRotationPoint(13.5F, -17.6296F, -3.9585F);
		bipedLeftLeg.setTextureOffset(122, 0).addBox(-5.5F, 36.8132F, -8.3384F, 11.0F, 5.0F, 16.0F, 0.0F, false);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(0.5F, 31.6296F, 1.9585F);
		bipedLeftLeg.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.1745F, 0.0F, 0.0F);
		cube_r13.setTextureOffset(0, 140).addBox(-5.0F, -15.8164F, -6.2969F, 9.0F, 23.0F, 10.0F, 0.0F, false);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(0.0F, 9.6296F, 1.9585F);
		bipedLeftLeg.addChild(cube_r14);
		setRotationAngle(cube_r14, -0.1745F, 0.0F, 0.0F);
		cube_r14.setTextureOffset(66, 93).addBox(-6.5F, -12.2539F, -9.7969F, 13.0F, 22.0F, 16.0F, 0.0F, false);

		bipedRightLeg = new ModelRenderer(this);
		bipedRightLeg.setRotationPoint(-13.5F, -17.6296F, -3.9585F);
		bipedRightLeg.setTextureOffset(122, 0).addBox(-5.5F, 36.8132F, -8.3384F, 11.0F, 5.0F, 16.0F, 0.0F, false);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(0.5F, 31.6296F, 1.9585F);
		bipedRightLeg.addChild(cube_r15);
		setRotationAngle(cube_r15, 0.1745F, 0.0F, 0.0F);
		cube_r15.setTextureOffset(0, 140).addBox(-5.0F, -15.8164F, -6.2969F, 9.0F, 23.0F, 10.0F, 0.0F, false);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(0.0F, 9.6296F, 1.9585F);
		bipedRightLeg.addChild(cube_r16);
		setRotationAngle(cube_r16, -0.1745F, 0.0F, 0.0F);
		cube_r16.setTextureOffset(66, 93).addBox(-6.5F, -12.2539F, -9.7969F, 13.0F, 22.0F, 16.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		bipedHead.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedBody.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedLeftArm.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedRightArm.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedLeftLeg.render(matrixStack, buffer, packedLight, packedOverlay);
		bipedRightLeg.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}